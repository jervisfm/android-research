.class public Lnet/flixster/android/TicketInfoPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "TicketInfoPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final DIALOGKEY_LOADING_SHOWTIMES:I = 0x1

.field public static final DIALOGKEY_NETWORKFAIL:I = 0x2


# instance fields
.field private mAdView:Lnet/flixster/android/ads/AdView;

.field private mExtras:Landroid/os/Bundle;

.field private mListingLayout:Landroid/widget/LinearLayout;

.field private mListingPastLayout:Landroid/widget/LinearLayout;

.field private mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

.field private mMovie:Lnet/flixster/android/model/Movie;

.field private mMovieId:J

.field private mNetworkTrys:I

.field private mPosterView:Landroid/widget/ImageView;

.field private mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

.field private mShowtimesTitle:Ljava/lang/String;

.field private mStartLoadingShowtimesDialogHandler:Landroid/os/Handler;

.field private mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;

.field private mTheater:Lnet/flixster/android/model/Theater;

.field private mTheaterId:J

.field private mTicketInfoPage:Lnet/flixster/android/TicketInfoPage;

.field private mTimer:Ljava/util/Timer;

.field private postShowtimesLoadHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/TicketInfoPage;->mNetworkTrys:I

    .line 205
    new-instance v0, Lnet/flixster/android/TicketInfoPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketInfoPage$1;-><init>(Lnet/flixster/android/TicketInfoPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mStartLoadingShowtimesDialogHandler:Landroid/os/Handler;

    .line 217
    new-instance v0, Lnet/flixster/android/TicketInfoPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketInfoPage$2;-><init>(Lnet/flixster/android/TicketInfoPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;

    .line 236
    new-instance v0, Lnet/flixster/android/TicketInfoPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketInfoPage$3;-><init>(Lnet/flixster/android/TicketInfoPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketInfoPage;->postShowtimesLoadHandler:Landroid/os/Handler;

    .line 40
    return-void
.end method

.method private ScheduleLoadShowtimesTask()V
    .locals 4

    .prologue
    .line 148
    new-instance v0, Lnet/flixster/android/TicketInfoPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketInfoPage$4;-><init>(Lnet/flixster/android/TicketInfoPage;)V

    .line 177
    .local v0, loadShowtimesTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 178
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/TicketInfoPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 56
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/TicketInfoPage;)Lnet/flixster/android/model/Theater;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/TicketInfoPage;Lnet/flixster/android/model/Theater;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 47
    iput-object p1, p0, Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    return-void
.end method

.method static synthetic access$11(Lnet/flixster/android/TicketInfoPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 217
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mStopLoadingShowtimesDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$12(Lnet/flixster/android/TicketInfoPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 236
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->postShowtimesLoadHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$13(Lnet/flixster/android/TicketInfoPage;)I
    .locals 1
    .parameter

    .prologue
    .line 57
    iget v0, p0, Lnet/flixster/android/TicketInfoPage;->mNetworkTrys:I

    return v0
.end method

.method static synthetic access$14(Lnet/flixster/android/TicketInfoPage;)V
    .locals 0
    .parameter

    .prologue
    .line 147
    invoke-direct {p0}, Lnet/flixster/android/TicketInfoPage;->ScheduleLoadShowtimesTask()V

    return-void
.end method

.method static synthetic access$15(Lnet/flixster/android/TicketInfoPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    iput p1, p0, Lnet/flixster/android/TicketInfoPage;->mNetworkTrys:I

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/TicketInfoPage;)J
    .locals 2
    .parameter

    .prologue
    .line 51
    iget-wide v0, p0, Lnet/flixster/android/TicketInfoPage;->mMovieId:J

    return-wide v0
.end method

.method static synthetic access$3(Lnet/flixster/android/TicketInfoPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mShowtimesTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/TicketInfoPage;)Lnet/flixster/android/TicketInfoPage;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mTicketInfoPage:Lnet/flixster/android/TicketInfoPage;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mListingLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 50
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mListingPastLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/TicketInfoPage;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 58
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/TicketInfoPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 205
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mStartLoadingShowtimesDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Lnet/flixster/android/TicketInfoPage;)J
    .locals 2
    .parameter

    .prologue
    .line 53
    iget-wide v0, p0, Lnet/flixster/android/TicketInfoPage;->mTheaterId:J

    return-wide v0
.end method

.method private populateDetails()V
    .locals 7

    .prologue
    .line 127
    const v5, 0x7f07026f

    invoke-virtual {p0, v5}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 128
    .local v4, theaterName:Landroid/widget/TextView;
    const v5, 0x7f07026c

    invoke-virtual {p0, v5}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 129
    .local v1, movieActors:Landroid/widget/TextView;
    const v5, 0x7f07026d

    invoke-virtual {p0, v5}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 130
    .local v2, movieMeta:Landroid/widget/TextView;
    const v5, 0x7f070270

    invoke-virtual {p0, v5}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 132
    .local v3, theaterAddress:Landroid/widget/TextView;
    iget-object v5, p0, Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v6, "name"

    invoke-virtual {v5, v6}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    iget-object v5, p0, Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v6, "address"

    invoke-virtual {v5, v6}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 134
    iget-object v5, p0, Lnet/flixster/android/TicketInfoPage;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v5, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    iget-object v5, p0, Lnet/flixster/android/TicketInfoPage;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v6, "meta"

    invoke-virtual {v5, v6}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 137
    iget-object v5, p0, Lnet/flixster/android/TicketInfoPage;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getThumbnailPoster()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    .line 138
    iget-object v5, p0, Lnet/flixster/android/TicketInfoPage;->mPosterView:Landroid/widget/ImageView;

    const v6, 0x7f02014f

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 140
    :cond_1
    iget-object v5, p0, Lnet/flixster/android/TicketInfoPage;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v6, p0, Lnet/flixster/android/TicketInfoPage;->mPosterView:Landroid/widget/ImageView;

    invoke-virtual {v5, v6}, Lnet/flixster/android/model/Movie;->getThumbnailBackedProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 141
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 142
    iget-object v5, p0, Lnet/flixster/android/TicketInfoPage;->mPosterView:Landroid/widget/ImageView;

    invoke-virtual {v5, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "v"

    .prologue
    .line 297
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Landroid/widget/TextView;

    if-ne v1, v2, :cond_1

    .line 298
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Listing;

    .line 299
    .local v0, l:Lnet/flixster/android/model/Listing;
    iget-object v1, v0, Lnet/flixster/android/model/Listing;->ticketUrl:Ljava/lang/String;

    invoke-static {v1, p0}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    .line 318
    .end local v0           #l:Lnet/flixster/android/model/Listing;
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f070273

    if-ne v1, v2, :cond_0

    .line 313
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mListingPastLayout:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 314
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .parameter "savedInstanceState"

    .prologue
    .line 63
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 64
    iput-object p0, p0, Lnet/flixster/android/TicketInfoPage;->mTicketInfoPage:Lnet/flixster/android/TicketInfoPage;

    .line 65
    invoke-virtual {p0}, Lnet/flixster/android/TicketInfoPage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mExtras:Landroid/os/Bundle;

    .line 66
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mTimer:Ljava/util/Timer;

    if-nez v1, :cond_0

    .line 67
    new-instance v1, Ljava/util/Timer;

    invoke-direct {v1}, Ljava/util/Timer;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mTimer:Ljava/util/Timer;

    .line 70
    :cond_0
    const v1, 0x7f030087

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->setContentView(I)V

    .line 71
    invoke-virtual {p0}, Lnet/flixster/android/TicketInfoPage;->createActionBar()V

    .line 72
    const v1, 0x7f0c0096

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 74
    const v1, 0x7f07026b

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    .local v0, movieTitle:Landroid/widget/TextView;
    const v1, 0x7f070269

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mPosterView:Landroid/widget/ImageView;

    .line 80
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mExtras:Landroid/os/Bundle;

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lnet/flixster/android/TicketInfoPage;->mTheaterId:J

    .line 81
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mExtras:Landroid/os/Bundle;

    const-string v2, "MOVIE_ID"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lnet/flixster/android/TicketInfoPage;->mMovieId:J

    .line 82
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mExtras:Landroid/os/Bundle;

    const-string v2, "SHOWTIMES_TITLE"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mShowtimesTitle:Ljava/lang/String;

    .line 83
    iget-wide v1, p0, Lnet/flixster/android/TicketInfoPage;->mTheaterId:J

    invoke-static {v1, v2}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    .line 84
    iget-wide v1, p0, Lnet/flixster/android/TicketInfoPage;->mMovieId:J

    invoke-static {v1, v2}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mMovie:Lnet/flixster/android/model/Movie;

    .line 85
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TicketInfoPage.onCreate mThaterId:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v3, p0, Lnet/flixster/android/TicketInfoPage;->mTheaterId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mMovieId:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lnet/flixster/android/TicketInfoPage;->mMovieId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 86
    const-string v3, " mShowtimesTitle:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/TicketInfoPage;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 85
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const v1, 0x7f07002a

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/ads/AdView;

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mAdView:Lnet/flixster/android/ads/AdView;

    .line 89
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    invoke-direct {p0}, Lnet/flixster/android/TicketInfoPage;->populateDetails()V

    .line 96
    const v1, 0x7f070277

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mListingLayout:Landroid/widget/LinearLayout;

    .line 97
    const v1, 0x7f070276

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mListingPastLayout:Landroid/widget/LinearLayout;

    .line 98
    const v1, 0x7f070273

    invoke-virtual {p0, v1}, Lnet/flixster/android/TicketInfoPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    .line 99
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 100
    iget-object v1, p0, Lnet/flixster/android/TicketInfoPage;->mShowElapsedShowtimesPanel:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 4
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 182
    packed-switch p1, :pswitch_data_0

    .line 202
    :goto_0
    return-object v0

    .line 184
    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    .line 185
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lnet/flixster/android/TicketInfoPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 186
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 187
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 188
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mLoadingShowtimesDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 190
    :pswitch_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 191
    const-string v2, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 192
    const-string v2, "Network Error"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 193
    const-string v2, "Retry"

    new-instance v3, Lnet/flixster/android/TicketInfoPage$5;

    invoke-direct {v3, p0}, Lnet/flixster/android/TicketInfoPage$5;-><init>(Lnet/flixster/android/TicketInfoPage;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 200
    invoke-virtual {p0}, Lnet/flixster/android/TicketInfoPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c004a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 182
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 322
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mAdView:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 123
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onDestroy()V

    .line 124
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 105
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 106
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mTimer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 107
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mTimer:Ljava/util/Timer;

    .line 109
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/tickets/info"

    const-string v2, "TicketInfoPage"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v0}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v0

    if-nez v0, :cond_1

    .line 112
    invoke-direct {p0}, Lnet/flixster/android/TicketInfoPage;->ScheduleLoadShowtimesTask()V

    .line 117
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->mAdView:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 118
    return-void

    .line 114
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/TicketInfoPage;->postShowtimesLoadHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
