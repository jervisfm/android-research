.class Lnet/flixster/android/TicketSelectPage$1;
.super Landroid/os/Handler;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TicketSelectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 376
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 14
    .parameter "msg"

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 381
    const-string v8, "FlxMain"

    const-string v9, "TicketSelectPage.addTicketBars()"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v8}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 386
    .local v2, li:Landroid/view/LayoutInflater;
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketBarLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v8}, Lnet/flixster/android/TicketSelectPage;->access$1(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 387
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v8}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v8

    invoke-virtual {v8}, Lnet/flixster/android/model/Performance;->getCategoryTally()I

    move-result v1

    .line 388
    .local v1, length:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v1, :cond_0

    .line 411
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v8}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v8

    const v9, 0x7f07028c

    invoke-virtual {v8, v9}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 412
    .local v3, perTicketSurcharge:Landroid/widget/TextView;
    const-string v8, "$%.2f"

    new-array v9, v13, [Ljava/lang/Object;

    iget-object v10, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mSurcharge:F
    invoke-static {v10}, Lnet/flixster/android/TicketSelectPage;->access$5(Lnet/flixster/android/TicketSelectPage;)F

    move-result v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 413
    .local v5, surchargeUnitPrice:Ljava/lang/String;
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    invoke-virtual {v8}, Lnet/flixster/android/TicketSelectPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0c0103

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-array v9, v13, [Ljava/lang/Object;

    .line 414
    aput-object v5, v9, v12

    .line 413
    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 415
    .local v4, surchargeNote:Ljava/lang/String;
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 416
    return-void

    .line 389
    .end local v3           #perTicketSurcharge:Landroid/widget/TextView;
    .end local v4           #surchargeNote:Ljava/lang/String;
    .end local v5           #surchargeUnitPrice:Ljava/lang/String;
    :cond_0
    const v8, 0x7f030089

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketBarLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v9}, Lnet/flixster/android/TicketSelectPage;->access$1(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v9

    invoke-virtual {v2, v8, v9, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout;

    .line 390
    .local v7, ticketBar:Landroid/widget/RelativeLayout;
    new-instance v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;

    invoke-direct {v6}, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;-><init>()V

    .line 391
    .local v6, tbh:Lnet/flixster/android/TicketSelectPage$TicketbarHolder;
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    iput-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->index:Ljava/lang/Integer;

    .line 393
    const v8, 0x7f0702c2

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->incButton:Landroid/widget/ImageButton;

    .line 394
    iget-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->incButton:Landroid/widget/ImageButton;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 395
    iget-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->incButton:Landroid/widget/ImageButton;

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v9}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 396
    const v8, 0x7f0702c4

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageButton;

    iput-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->decButton:Landroid/widget/ImageButton;

    .line 397
    iget-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->decButton:Landroid/widget/ImageButton;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 398
    iget-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->decButton:Landroid/widget/ImageButton;

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v9}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 399
    const v8, 0x7f0702c7

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->label:Landroid/widget/TextView;

    .line 400
    iget-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->label:Landroid/widget/TextView;

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategoryLabels:[Ljava/lang/String;
    invoke-static {v9}, Lnet/flixster/android/TicketSelectPage;->access$3(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;

    move-result-object v9

    aget-object v9, v9, v0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 401
    const v8, 0x7f0702c6

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->price:Landroid/widget/TextView;

    .line 402
    iget-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->price:Landroid/widget/TextView;

    const-string v9, "$%.2f"

    new-array v10, v13, [Ljava/lang/Object;

    iget-object v11, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F
    invoke-static {v11}, Lnet/flixster/android/TicketSelectPage;->access$4(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v11

    aget v11, v11, v0

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 403
    const v8, 0x7f0702c3

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->counter:Landroid/widget/TextView;

    .line 404
    const v8, 0x7f0702c5

    invoke-virtual {v7, v8}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, v6, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->subTotal:Landroid/widget/TextView;

    .line 406
    invoke-virtual {v7, v6}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 408
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage$1;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketBarLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v8}, Lnet/flixster/android/TicketSelectPage;->access$1(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 388
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0
.end method
