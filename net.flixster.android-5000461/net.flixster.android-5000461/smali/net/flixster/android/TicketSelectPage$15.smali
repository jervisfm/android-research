.class Lnet/flixster/android/TicketSelectPage$15;
.super Landroid/os/Handler;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TicketSelectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 1243
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1246
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mState:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$45(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1283
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mScrollView:Landroid/widget/ScrollView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$55(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    .line 1285
    return-void

    .line 1248
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mSelectTicketsLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$46(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1249
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mConfirmReservationLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$47(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1250
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorTicketText:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$48(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1251
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mReceiptInfoLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$49(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1252
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/tickets/select"

    const-string v2, "Select"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1256
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorTicketText:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$48(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 1257
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #calls: Lnet/flixster/android/TicketSelectPage;->hardValidateAll()V
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$50(Lnet/flixster/android/TicketSelectPage;)V

    .line 1258
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->updatePrice:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$51(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1259
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$52(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1260
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mConfirmReservationLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$47(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1261
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mReceiptInfoLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$49(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1262
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/tickets/fix"

    const-string v2, "Fix"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1265
    :pswitch_2
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mSelectToReservationFieldHandler:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$53(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1267
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mSelectTicketsLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$46(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1268
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mConfirmReservationLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$47(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1269
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mReceiptInfoLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$49(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1270
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/tickets/confirm"

    const-string v2, "Confirm"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1273
    :pswitch_3
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v0, v0, Lnet/flixster/android/TicketSelectPage;->mCompletePurchaseButton:Landroid/widget/Button;

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v1

    invoke-virtual {v1}, Lnet/flixster/android/TicketSelectPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00c6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1274
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mSelectTicketsLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$46(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1275
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketsConfirmMessage:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$54(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1276
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mConfirmReservationLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$47(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1277
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$15;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mReceiptInfoLayout:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$49(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1278
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/tickets/receipt"

    const-string v2, "Receipt"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1246
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
