.class Lnet/flixster/android/TicketSelectPage$16;
.super Landroid/os/Handler;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TicketSelectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 1288
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 20
    .parameter "msg"

    .prologue
    .line 1291
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702bf

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 1292
    .local v5, confirmEmail:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$40(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-virtual {v5, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1294
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702b3

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1295
    .local v7, confirmMovieTitle:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    #getter for: Lnet/flixster/android/TicketSelectPage;->mMovieTitle:Ljava/lang/String;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$56(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v7, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1297
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702b5

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 1298
    .local v8, confirmTheaterName:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTheaterName:Ljava/lang/String;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$57(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1300
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702b8

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 1301
    .local v9, confirmTickets:Landroid/widget/TextView;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 1302
    .local v13, tickets:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$11(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v14

    if-eqz v14, :cond_0

    .line 1303
    const/4 v11, 0x0

    .local v11, i:I
    :goto_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategories:I
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$58(Lnet/flixster/android/TicketSelectPage;)I

    move-result v14

    if-lt v11, v14, :cond_2

    .line 1312
    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1315
    .end local v11           #i:I
    :cond_0
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702b9

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1316
    .local v6, confirmFee:Landroid/widget/TextView;
    const-string v14, "$%.2f"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    move-object/from16 v17, v0

    #getter for: Lnet/flixster/android/TicketSelectPage;->mConvenienceTotal:F
    invoke-static/range {v17 .. v17}, Lnet/flixster/android/TicketSelectPage;->access$15(Lnet/flixster/android/TicketSelectPage;)F

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v6, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1318
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702ba

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1319
    .local v10, confirmTotal:Landroid/widget/TextView;
    const-string v14, "$%.2f"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    move-object/from16 v17, v0

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTotal:F
    invoke-static/range {v17 .. v17}, Lnet/flixster/android/TicketSelectPage;->access$12(Lnet/flixster/android/TicketSelectPage;)F

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v10, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1321
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mMethodNames:[Ljava/lang/String;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$6(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;

    move-result-object v14

    if-eqz v14, :cond_1

    .line 1322
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702bb

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1323
    .local v4, confirmCardType:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mMethodNames:[Ljava/lang/String;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$6(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardTypeIndex:I
    invoke-static {v15}, Lnet/flixster/android/TicketSelectPage;->access$7(Lnet/flixster/android/TicketSelectPage;)I

    move-result v15

    aget-object v14, v14, v15

    invoke-virtual {v4, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1326
    .end local v4           #confirmCardType:Landroid/widget/TextView;
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702bc

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1327
    .local v3, confirmCardNumber:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$21(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v14

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1328
    .local v1, cardNumber:Ljava/lang/String;
    const/4 v14, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v15

    add-int/lit8 v15, v15, -0x4

    invoke-static {v14, v15}, Ljava/lang/Math;->max(II)I

    move-result v14

    invoke-virtual {v1, v14}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v12

    .line 1329
    .local v12, subCard:Ljava/lang/String;
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "************"

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v14, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1331
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v14

    const v15, 0x7f0702be

    invoke-virtual {v14, v15}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1332
    .local v2, confirmCardExp:Landroid/widget/TextView;
    new-instance v14, Ljava/lang/StringBuilder;

    const-string v15, "%02d"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    move-object/from16 v18, v0

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardMonth:I
    invoke-static/range {v18 .. v18}, Lnet/flixster/android/TicketSelectPage;->access$31(Lnet/flixster/android/TicketSelectPage;)I

    move-result v18

    add-int/lit8 v18, v18, 0x1

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v14, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 1333
    const-string v15, "%4d"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    move-object/from16 v18, v0

    #getter for: Lnet/flixster/android/TicketSelectPage;->mThisYear:I
    invoke-static/range {v18 .. v18}, Lnet/flixster/android/TicketSelectPage;->access$35(Lnet/flixster/android/TicketSelectPage;)I

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    move-object/from16 v19, v0

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardYear:I
    invoke-static/range {v19 .. v19}, Lnet/flixster/android/TicketSelectPage;->access$33(Lnet/flixster/android/TicketSelectPage;)I

    move-result v19

    add-int v18, v18, v19

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1332
    invoke-virtual {v2, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1334
    return-void

    .line 1304
    .end local v1           #cardNumber:Ljava/lang/String;
    .end local v2           #confirmCardExp:Landroid/widget/TextView;
    .end local v3           #confirmCardNumber:Landroid/widget/TextView;
    .end local v6           #confirmFee:Landroid/widget/TextView;
    .end local v10           #confirmTotal:Landroid/widget/TextView;
    .end local v12           #subCard:Ljava/lang/String;
    .restart local v11       #i:I
    :cond_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$11(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v14

    aget v14, v14, v11

    const/4 v15, 0x0

    cmpl-float v14, v14, v15

    if-lez v14, :cond_4

    .line 1305
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->length()I

    move-result v14

    if-lez v14, :cond_3

    .line 1306
    const-string v14, "\n"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1308
    :cond_3
    move-object/from16 v0, p0

    iget-object v14, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$11(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v14

    aget v14, v14, v11

    float-to-int v14, v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "\u00d7 "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategoryLabels:[Ljava/lang/String;
    invoke-static {v15}, Lnet/flixster/android/TicketSelectPage;->access$3(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;

    move-result-object v15

    aget-object v15, v15, v11

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    .line 1309
    const-string v15, " / "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "$%.2f"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/TicketSelectPage$16;->this$0:Lnet/flixster/android/TicketSelectPage;

    move-object/from16 v18, v0

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F
    invoke-static/range {v18 .. v18}, Lnet/flixster/android/TicketSelectPage;->access$4(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v18

    aget v18, v18, v11

    invoke-static/range {v18 .. v18}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1303
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0
.end method
