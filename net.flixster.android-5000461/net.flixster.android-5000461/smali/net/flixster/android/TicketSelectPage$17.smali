.class Lnet/flixster/android/TicketSelectPage$17;
.super Ljava/util/TimerTask;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TicketSelectPage;->ScheduleLoadPerformanceTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 491
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 494
    const-string v1, "FlxMain"

    const-string v2, "TicketSelectPage.ScheduleLoadPerformanceTask() run"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mParamTheater:Ljava/lang/String;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$59(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mParamListing:Ljava/lang/String;
    invoke-static {v3}, Lnet/flixster/android/TicketSelectPage;->access$60(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mParamDate:Ljava/lang/String;
    invoke-static {v4}, Lnet/flixster/android/TicketSelectPage;->access$61(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mParamTime:Ljava/lang/String;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$62(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lnet/flixster/android/data/TicketsDao;->getMoviePerformance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Performance;

    move-result-object v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$63(Lnet/flixster/android/TicketSelectPage;Lnet/flixster/android/model/Performance;)V

    .line 498
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TicketSelectPage.ScheduleLoadPerformanceTask() mPerformance:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v3}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 500
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/Performance;->getCategoryTally()I

    move-result v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mCategories:I
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$64(Lnet/flixster/android/TicketSelectPage;I)V

    .line 501
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/Performance;->getLabels()[Ljava/lang/String;

    move-result-object v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mCategoryLabels:[Ljava/lang/String;
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$65(Lnet/flixster/android/TicketSelectPage;[Ljava/lang/String;)V

    .line 502
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/Performance;->getPrices()[F

    move-result-object v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$66(Lnet/flixster/android/TicketSelectPage;[F)V

    .line 503
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategories:I
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$58(Lnet/flixster/android/TicketSelectPage;)I

    move-result v2

    new-array v2, v2, [F

    #setter for: Lnet/flixster/android/TicketSelectPage;->mTicketTotals:[F
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$67(Lnet/flixster/android/TicketSelectPage;[F)V

    .line 504
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategories:I
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$58(Lnet/flixster/android/TicketSelectPage;)I

    move-result v2

    new-array v2, v2, [F

    #setter for: Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$68(Lnet/flixster/android/TicketSelectPage;[F)V

    .line 506
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/Performance;->getSurcharge()F

    move-result v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mSurcharge:F
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$69(Lnet/flixster/android/TicketSelectPage;F)V

    .line 509
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/Performance;->getMethodIds()[Ljava/lang/String;

    move-result-object v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mMethodIds:[Ljava/lang/String;
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$70(Lnet/flixster/android/TicketSelectPage;[Ljava/lang/String;)V

    .line 510
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/Performance;->getMethodNames()[Ljava/lang/String;

    move-result-object v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mMethodNames:[Ljava/lang/String;
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$71(Lnet/flixster/android/TicketSelectPage;[Ljava/lang/String;)V

    .line 512
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mMethodIds:[Ljava/lang/String;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$72(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/data/TicketsDao;->makeCardTypeMap([Ljava/lang/String;)[I

    move-result-object v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mCardMapType:[I
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$73(Lnet/flixster/android/TicketSelectPage;[I)V

    .line 513
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardMapType:[I
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$74(Lnet/flixster/android/TicketSelectPage;)[I

    move-result-object v2

    const/4 v3, 0x0

    aget v2, v2, v3

    #setter for: Lnet/flixster/android/TicketSelectPage;->mCardType:I
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$75(Lnet/flixster/android/TicketSelectPage;I)V

    .line 515
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->addTicketBars:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$76(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 517
    const-string v1, "FlxMain"

    .line 518
    const-string v2, "TicketSelectPage.ScheduleLoadPerformanceTask() time to call updatePrice/updateCardStatus"

    .line 517
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->setupCardSpinner:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$77(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 521
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->updatePrice:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$51(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 522
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$52(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 523
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStopLoadPricingDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$78(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/model/MovieTicketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 541
    :goto_0
    return-void

    .line 525
    :catch_0
    move-exception v0

    .line 526
    .local v0, e:Lnet/flixster/android/model/MovieTicketException;
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    invoke-virtual {v0}, Lnet/flixster/android/model/MovieTicketException;->getMessage()Ljava/lang/String;

    move-result-object v2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorMessage:Ljava/lang/String;
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$79(Lnet/flixster/android/TicketSelectPage;Ljava/lang/String;)V

    .line 527
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStopLoadPricingDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$78(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 528
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mShowPurchaseTimeoutErrorHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$80(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 530
    .end local v0           #e:Lnet/flixster/android/model/MovieTicketException;
    :catch_1
    move-exception v0

    .line 531
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "FlxMain"

    const-string v2, "TicketSelectPage.ScheduleLoadPerformanceTask() IOException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 532
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mFetchPerformanceTrys:I
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$81(Lnet/flixster/android/TicketSelectPage;)I

    move-result v1

    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 533
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mFetchPerformanceTrys:I
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$81(Lnet/flixster/android/TicketSelectPage;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    #setter for: Lnet/flixster/android/TicketSelectPage;->mFetchPerformanceTrys:I
    invoke-static {v1, v2}, Lnet/flixster/android/TicketSelectPage;->access$82(Lnet/flixster/android/TicketSelectPage;I)V

    .line 534
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #calls: Lnet/flixster/android/TicketSelectPage;->ScheduleLoadPerformanceTask()V
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$83(Lnet/flixster/android/TicketSelectPage;)V

    goto :goto_0

    .line 536
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStopLoadPricingDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$78(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 537
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$17;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mShowNetorkErrorDialogHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$84(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
