.class Lnet/flixster/android/TicketSelectPage$18;
.super Ljava/util/TimerTask;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TicketSelectPage;->ScheduleGetReservation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 552
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 555
    const-string v0, "FlxMain"

    const-string v1, "TicketSelectPage.ScheduleTicketAvailabilityCheck() run"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mParamTheater:Ljava/lang/String;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$59(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v0

    .line 559
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mParamListing:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$60(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v1

    .line 560
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mParamDate:Ljava/lang/String;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$61(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v2

    .line 561
    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mParamTime:Ljava/lang/String;
    invoke-static {v3}, Lnet/flixster/android/TicketSelectPage;->access$62(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;

    move-result-object v3

    .line 562
    iget-object v4, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mMethodIds:[Ljava/lang/String;
    invoke-static {v4}, Lnet/flixster/android/TicketSelectPage;->access$72(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardTypeIndex:I
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$7(Lnet/flixster/android/TicketSelectPage;)I

    move-result v5

    aget-object v4, v4, v5

    .line 563
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$21(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 564
    iget-object v6, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;
    invoke-static {v6}, Lnet/flixster/android/TicketSelectPage;->access$26(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 565
    iget-object v7, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;
    invoke-static {v7}, Lnet/flixster/android/TicketSelectPage;->access$29(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lnet/flixster/android/util/PostalCodeUtils;->parseZipcodeForShowtimes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 566
    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "%4d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v13, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardYear:I
    invoke-static {v13}, Lnet/flixster/android/TicketSelectPage;->access$33(Lnet/flixster/android/TicketSelectPage;)I

    move-result v13

    iget-object v14, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mThisYear:I
    invoke-static {v14}, Lnet/flixster/android/TicketSelectPage;->access$35(Lnet/flixster/android/TicketSelectPage;)I

    move-result v14

    add-int/2addr v13, v14

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 567
    const-string v9, "%02d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v13, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardMonth:I
    invoke-static {v13}, Lnet/flixster/android/TicketSelectPage;->access$31(Lnet/flixster/android/TicketSelectPage;)I

    move-result v13

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 566
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 567
    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;
    invoke-static {v9}, Lnet/flixster/android/TicketSelectPage;->access$40(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    .line 568
    iget-object v10, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v10}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v10

    iget-object v11, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F
    invoke-static {v11}, Lnet/flixster/android/TicketSelectPage;->access$11(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v11

    .line 557
    invoke-static/range {v0 .. v11}, Lnet/flixster/android/data/TicketsDao;->getReservation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Performance;[F)Lnet/flixster/android/model/Performance;

    .line 570
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStopGetReservationDialogHandler:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$85(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 571
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v0

    invoke-virtual {v0}, Lnet/flixster/android/model/Performance;->getError()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mEmailFieldState:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$39(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    .line 572
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    const/4 v1, 0x1

    #setter for: Lnet/flixster/android/TicketSelectPage;->mState:I
    invoke-static {v0, v1}, Lnet/flixster/android/TicketSelectPage;->access$86(Lnet/flixster/android/TicketSelectPage;I)V

    .line 573
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStateLayout:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$87(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 585
    :goto_0
    return-void

    .line 576
    :cond_1
    const-string v0, "FlxMain"

    const-string v1, "TicketSelectPage.ScheduleGetReservation() run post dao"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    const/4 v1, 0x2

    #setter for: Lnet/flixster/android/TicketSelectPage;->mState:I
    invoke-static {v0, v1}, Lnet/flixster/android/TicketSelectPage;->access$86(Lnet/flixster/android/TicketSelectPage;I)V

    .line 578
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStateLayout:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$87(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 579
    :catch_0
    move-exception v12

    .line 580
    .local v12, e:Ljava/lang/Exception;
    const-string v0, "FlxMain"

    const-string v1, "TicketSelectPage.ScheduleTicketAvailabilityCheck() Exception"

    invoke-static {v0, v1, v12}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 581
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$18;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStopGetReservationDialogHandler:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$85(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
