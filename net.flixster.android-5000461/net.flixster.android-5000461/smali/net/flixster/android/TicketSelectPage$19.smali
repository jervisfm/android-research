.class Lnet/flixster/android/TicketSelectPage$19;
.super Ljava/util/TimerTask;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TicketSelectPage;->SchedulePurchase()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 596
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 599
    const-string v2, "FlxMain"

    const-string v3, "TicketSelectPage.ScheduleTicketAvailabilityCheck() run"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v2

    invoke-virtual {v2}, Lnet/flixster/android/model/Performance;->getReservationId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;
    invoke-static {v3}, Lnet/flixster/android/TicketSelectPage;->access$40(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v3

    .line 602
    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 601
    invoke-static {v2, v3}, Lnet/flixster/android/data/TicketsDao;->purchaseTickets(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 604
    .local v0, confirmation:Ljava/lang/String;
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketConfirmationNumber:Landroid/widget/TextView;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$88(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 605
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStopPurchaseDialogHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$89(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 607
    if-nez v0, :cond_0

    .line 608
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$90(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 632
    .end local v0           #confirmation:Ljava/lang/String;
    :goto_0
    return-void

    .line 611
    .restart local v0       #confirmation:Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    const/4 v3, 0x3

    #setter for: Lnet/flixster/android/TicketSelectPage;->mState:I
    invoke-static {v2, v3}, Lnet/flixster/android/TicketSelectPage;->access$86(Lnet/flixster/android/TicketSelectPage;I)V

    .line 612
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStateLayout:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$87(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/model/MovieTicketPurchaseException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lnet/flixster/android/model/MovieTicketException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 615
    .end local v0           #confirmation:Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 617
    .local v1, e:Lnet/flixster/android/model/MovieTicketPurchaseException;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TicketsDao. throw timeout error e:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    invoke-virtual {v1}, Lnet/flixster/android/model/MovieTicketPurchaseException;->getMessage()Ljava/lang/String;

    move-result-object v3

    #setter for: Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorMessage:Ljava/lang/String;
    invoke-static {v2, v3}, Lnet/flixster/android/TicketSelectPage;->access$79(Lnet/flixster/android/TicketSelectPage;Ljava/lang/String;)V

    .line 619
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mShowPurchaseTimeoutErrorHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$80(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 621
    .end local v1           #e:Lnet/flixster/android/model/MovieTicketPurchaseException;
    :catch_1
    move-exception v1

    .line 623
    .local v1, e:Lnet/flixster/android/model/MovieTicketException;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "TicketsDao. throw standard error e:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    invoke-virtual {v1}, Lnet/flixster/android/model/MovieTicketException;->getMessage()Ljava/lang/String;

    move-result-object v3

    #setter for: Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorMessage:Ljava/lang/String;
    invoke-static {v2, v3}, Lnet/flixster/android/TicketSelectPage;->access$79(Lnet/flixster/android/TicketSelectPage;Ljava/lang/String;)V

    .line 625
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mShowPurchaseErrorHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$91(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 627
    .end local v1           #e:Lnet/flixster/android/model/MovieTicketException;
    :catch_2
    move-exception v1

    .line 628
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "FlxMain"

    const-string v3, "TicketSelectPage.ScheduleTicketAvailabilityCheck() Exception"

    invoke-static {v2, v3, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 629
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$19;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStopPurchaseDialogHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$89(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method
