.class Lnet/flixster/android/TicketSelectPage$2;
.super Landroid/os/Handler;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TicketSelectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$2;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 419
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .parameter "msg"

    .prologue
    .line 422
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$2;->this$0:Lnet/flixster/android/TicketSelectPage;

    const v3, 0x7f07028f

    invoke-virtual {v2, v3}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    .line 423
    .local v1, spinner:Landroid/widget/Spinner;
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$2;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v2

    .line 424
    const v3, 0x1090008

    iget-object v4, p0, Lnet/flixster/android/TicketSelectPage$2;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mMethodNames:[Ljava/lang/String;
    invoke-static {v4}, Lnet/flixster/android/TicketSelectPage;->access$6(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;

    move-result-object v4

    .line 423
    invoke-direct {v0, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 430
    .local v0, adapter:Landroid/widget/ArrayAdapter;,"Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v2, 0x1090009

    invoke-virtual {v0, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 431
    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 432
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$2;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 433
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$2;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardTypeIndex:I
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$7(Lnet/flixster/android/TicketSelectPage;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 434
    return-void
.end method
