.class Lnet/flixster/android/TicketSelectPage$20;
.super Ljava/lang/Object;
.source "TicketSelectPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TicketSelectPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$20;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 926
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .parameter "dialog"
    .parameter "which"

    .prologue
    .line 928
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$20;->this$0:Lnet/flixster/android/TicketSelectPage;

    #setter for: Lnet/flixster/android/TicketSelectPage;->mCardMonth:I
    invoke-static {v0, p2}, Lnet/flixster/android/TicketSelectPage;->access$92(Lnet/flixster/android/TicketSelectPage;I)V

    .line 929
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$20;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$20;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardMonth:I
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$31(Lnet/flixster/android/TicketSelectPage;)I

    move-result v1

    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage$20;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardYear:I
    invoke-static {v2}, Lnet/flixster/android/TicketSelectPage;->access$33(Lnet/flixster/android/TicketSelectPage;)I

    move-result v2

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage$20;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mThisMonth:I
    invoke-static {v3}, Lnet/flixster/android/TicketSelectPage;->access$93(Lnet/flixster/android/TicketSelectPage;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lnet/flixster/android/data/TicketsDao;->validateDate(III)I

    move-result v1

    #setter for: Lnet/flixster/android/TicketSelectPage;->mExpDateFieldState:I
    invoke-static {v0, v1}, Lnet/flixster/android/TicketSelectPage;->access$94(Lnet/flixster/android/TicketSelectPage;I)V

    .line 930
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$20;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$52(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 931
    return-void
.end method
