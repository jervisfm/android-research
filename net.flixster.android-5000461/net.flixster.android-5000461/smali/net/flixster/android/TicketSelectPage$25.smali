.class Lnet/flixster/android/TicketSelectPage$25;
.super Ljava/lang/Object;
.source "TicketSelectPage.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TicketSelectPage;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$25;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 1002
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .parameter "dialog"
    .parameter "whichButton"

    .prologue
    const/4 v1, 0x0

    .line 1004
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$25;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mState:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$45(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1011
    :goto_0
    return-void

    .line 1006
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$25;->this$0:Lnet/flixster/android/TicketSelectPage;

    #setter for: Lnet/flixster/android/TicketSelectPage;->mFetchPerformanceTrys:I
    invoke-static {v0, v1}, Lnet/flixster/android/TicketSelectPage;->access$82(Lnet/flixster/android/TicketSelectPage;I)V

    .line 1007
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$25;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mStartLoadPricingDialogHandler:Landroid/os/Handler;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$96(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1008
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$25;->this$0:Lnet/flixster/android/TicketSelectPage;

    #calls: Lnet/flixster/android/TicketSelectPage;->ScheduleLoadPerformanceTask()V
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$83(Lnet/flixster/android/TicketSelectPage;)V

    goto :goto_0

    .line 1004
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
