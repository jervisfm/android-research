.class Lnet/flixster/android/TicketSelectPage$3;
.super Landroid/os/Handler;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TicketSelectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 705
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .parameter "msg"

    .prologue
    const/4 v10, 0x0

    .line 708
    const-string v5, "FlxMain"

    const-string v6, "TicketSelectPage.updatePrice:"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 711
    :try_start_0
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    const/4 v6, 0x0

    #setter for: Lnet/flixster/android/TicketSelectPage;->mTotal:F
    invoke-static {v5, v6}, Lnet/flixster/android/TicketSelectPage;->access$8(Lnet/flixster/android/TicketSelectPage;F)V

    .line 712
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    const/4 v6, 0x0

    #setter for: Lnet/flixster/android/TicketSelectPage;->mTicketCount:F
    invoke-static {v5, v6}, Lnet/flixster/android/TicketSelectPage;->access$9(Lnet/flixster/android/TicketSelectPage;F)V

    .line 713
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;

    move-result-object v5

    invoke-virtual {v5}, Lnet/flixster/android/model/Performance;->getCategoryTally()I

    move-result v2

    .line 714
    .local v2, length:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 720
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    iget-object v6, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCount:F
    invoke-static {v6}, Lnet/flixster/android/TicketSelectPage;->access$13(Lnet/flixster/android/TicketSelectPage;)F

    move-result v6

    iget-object v7, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mSurcharge:F
    invoke-static {v7}, Lnet/flixster/android/TicketSelectPage;->access$5(Lnet/flixster/android/TicketSelectPage;)F

    move-result v7

    mul-float/2addr v6, v7

    #setter for: Lnet/flixster/android/TicketSelectPage;->mConvenienceTotal:F
    invoke-static {v5, v6}, Lnet/flixster/android/TicketSelectPage;->access$14(Lnet/flixster/android/TicketSelectPage;F)V

    .line 721
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTotal:F
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$12(Lnet/flixster/android/TicketSelectPage;)F

    move-result v6

    iget-object v7, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mConvenienceTotal:F
    invoke-static {v7}, Lnet/flixster/android/TicketSelectPage;->access$15(Lnet/flixster/android/TicketSelectPage;)F

    move-result v7

    add-float/2addr v6, v7

    #setter for: Lnet/flixster/android/TicketSelectPage;->mTotal:F
    invoke-static {v5, v6}, Lnet/flixster/android/TicketSelectPage;->access$8(Lnet/flixster/android/TicketSelectPage;F)V

    .line 727
    const/4 v1, 0x0

    :goto_1
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$4(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v5

    array-length v5, v5

    if-lt v1, v5, :cond_1

    .line 736
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mConvenienceTotalView:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$16(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v5

    const-string v6, "$%.2f"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mConvenienceTotal:F
    invoke-static {v9}, Lnet/flixster/android/TicketSelectPage;->access$15(Lnet/flixster/android/TicketSelectPage;)F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 737
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTotalView:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$17(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v5

    const-string v6, "$%.2f"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTotal:F
    invoke-static {v9}, Lnet/flixster/android/TicketSelectPage;->access$12(Lnet/flixster/android/TicketSelectPage;)F

    move-result v9

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 739
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCountState:I
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$18(Lnet/flixster/android/TicketSelectPage;)I

    move-result v5

    const/4 v6, 0x3

    if-ne v5, v6, :cond_3

    .line 740
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCount:F
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$13(Lnet/flixster/android/TicketSelectPage;)F

    move-result v5

    cmpl-float v5, v5, v10

    if-nez v5, :cond_2

    .line 741
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorTicketCountText:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$19(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v5

    const v6, 0x7f0c00c7

    sget-object v7, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setText(ILandroid/widget/TextView$BufferType;)V

    .line 746
    :goto_2
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorTicketCountText:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$19(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 754
    .end local v1           #i:I
    .end local v2           #length:I
    :goto_3
    return-void

    .line 715
    .restart local v1       #i:I
    .restart local v2       #length:I
    :cond_0
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketTotals:[F
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$10(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v5

    iget-object v6, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F
    invoke-static {v6}, Lnet/flixster/android/TicketSelectPage;->access$4(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v6

    aget v6, v6, v1

    iget-object v7, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F
    invoke-static {v7}, Lnet/flixster/android/TicketSelectPage;->access$11(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v7

    aget v7, v7, v1

    mul-float/2addr v6, v7

    aput v6, v5, v1

    .line 716
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTotal:F
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$12(Lnet/flixster/android/TicketSelectPage;)F

    move-result v6

    iget-object v7, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketTotals:[F
    invoke-static {v7}, Lnet/flixster/android/TicketSelectPage;->access$10(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v7

    aget v7, v7, v1

    add-float/2addr v6, v7

    #setter for: Lnet/flixster/android/TicketSelectPage;->mTotal:F
    invoke-static {v5, v6}, Lnet/flixster/android/TicketSelectPage;->access$8(Lnet/flixster/android/TicketSelectPage;F)V

    .line 717
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCount:F
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$13(Lnet/flixster/android/TicketSelectPage;)F

    move-result v6

    iget-object v7, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F
    invoke-static {v7}, Lnet/flixster/android/TicketSelectPage;->access$11(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v7

    aget v7, v7, v1

    add-float/2addr v6, v7

    #setter for: Lnet/flixster/android/TicketSelectPage;->mTicketCount:F
    invoke-static {v5, v6}, Lnet/flixster/android/TicketSelectPage;->access$9(Lnet/flixster/android/TicketSelectPage;F)V

    .line 714
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 730
    :cond_1
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketBarLinearLayout:Landroid/widget/LinearLayout;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$1(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    .line 731
    .local v4, ticketBar:Landroid/widget/RelativeLayout;
    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;

    .line 732
    .local v3, tbh:Lnet/flixster/android/TicketSelectPage$TicketbarHolder;
    iget-object v5, v3, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->counter:Landroid/widget/TextView;

    iget-object v6, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F
    invoke-static {v6}, Lnet/flixster/android/TicketSelectPage;->access$11(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v6

    aget v6, v6, v1

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 733
    iget-object v5, v3, Lnet/flixster/android/TicketSelectPage$TicketbarHolder;->subTotal:Landroid/widget/TextView;

    const-string v6, "$%.2f"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketTotals:[F
    invoke-static {v9}, Lnet/flixster/android/TicketSelectPage;->access$10(Lnet/flixster/android/TicketSelectPage;)[F

    move-result-object v9

    aget v9, v9, v1

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 727
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 743
    .end local v3           #tbh:Lnet/flixster/android/TicketSelectPage$TicketbarHolder;
    .end local v4           #ticketBar:Landroid/widget/RelativeLayout;
    :cond_2
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorTicketCountText:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$19(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v5

    const v6, 0x7f0c00c8

    sget-object v7, Landroid/widget/TextView$BufferType;->NORMAL:Landroid/widget/TextView$BufferType;

    invoke-virtual {v5, v6, v7}, Landroid/widget/TextView;->setText(ILandroid/widget/TextView$BufferType;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 751
    .end local v1           #i:I
    .end local v2           #length:I
    :catch_0
    move-exception v0

    .line 752
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "FlxMain"

    const-string v6, "TicketSelectPage updatePrice problem"

    invoke-static {v5, v6, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 748
    .end local v0           #e:Ljava/lang/Exception;
    .restart local v1       #i:I
    .restart local v2       #length:I
    :cond_3
    :try_start_1
    iget-object v5, p0, Lnet/flixster/android/TicketSelectPage$3;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorTicketCountText:Landroid/widget/TextView;
    invoke-static {v5}, Lnet/flixster/android/TicketSelectPage;->access$19(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method
