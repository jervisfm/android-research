.class Lnet/flixster/android/TicketSelectPage$4;
.super Landroid/os/Handler;
.source "TicketSelectPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TicketSelectPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TicketSelectPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    .line 767
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .parameter "msg"

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 770
    const-string v0, "FlxMain"

    const-string v1, "TicketSelectPage.updateCardStatus"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 772
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$20(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 781
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$21(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 782
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$24(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 786
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$25(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 795
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$26(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 796
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorCodenumberText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$27(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 800
    :goto_1
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$28(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    packed-switch v0, :pswitch_data_2

    .line 809
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$29(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 810
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorPostalnumberText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$30(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 817
    :goto_2
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardMonth:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$31(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 818
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v0

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketExpMonth:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$32(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "%02d"

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardMonth:I
    invoke-static {v3}, Lnet/flixster/android/TicketSelectPage;->access$31(Lnet/flixster/android/TicketSelectPage;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 820
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardYear:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$33(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    const/16 v1, -0x64

    if-eq v0, v1, :cond_1

    .line 822
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;

    move-result-object v0

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketExpYear:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$34(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, "%4d"

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mCardYear:I
    invoke-static {v3}, Lnet/flixster/android/TicketSelectPage;->access$33(Lnet/flixster/android/TicketSelectPage;)I

    move-result v3

    iget-object v4, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mThisYear:I
    invoke-static {v4}, Lnet/flixster/android/TicketSelectPage;->access$35(Lnet/flixster/android/TicketSelectPage;)I

    move-result v4

    add-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 824
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mExpDateFieldState:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$36(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    packed-switch v0, :pswitch_data_3

    .line 833
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketExpDateLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$37(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 834
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorExpDateText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$38(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 838
    :goto_3
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mEmailFieldState:I
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$39(Lnet/flixster/android/TicketSelectPage;)I

    move-result v0

    packed-switch v0, :pswitch_data_4

    .line 847
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$40(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 848
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorEmailText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$41(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 852
    :goto_4
    return-void

    .line 774
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$21(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$22(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 777
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$21(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$23(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 778
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$24(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 788
    :pswitch_2
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$26(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$22(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    .line 791
    :pswitch_3
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$26(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$23(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 792
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorCodenumberText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$27(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 802
    :pswitch_4
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$29(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$22(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    .line 805
    :pswitch_5
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$29(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$23(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 806
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorPostalnumberText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$30(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 826
    :pswitch_6
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketExpDateLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$37(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$22(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    .line 829
    :pswitch_7
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketExpDateLabel:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$37(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$23(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 830
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorExpDateText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$38(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 840
    :pswitch_8
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$40(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$22(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 843
    :pswitch_9
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$40(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lnet/flixster/android/TicketSelectPage;->access$23(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 844
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage$4;->this$0:Lnet/flixster/android/TicketSelectPage;

    #getter for: Lnet/flixster/android/TicketSelectPage;->mErrorEmailText:Landroid/widget/TextView;
    invoke-static {v0}, Lnet/flixster/android/TicketSelectPage;->access$41(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 772
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 786
    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_2
    .end packed-switch

    .line 800
    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 824
    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_7
        :pswitch_6
    .end packed-switch

    .line 838
    :pswitch_data_4
    .packed-switch 0x2
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method
