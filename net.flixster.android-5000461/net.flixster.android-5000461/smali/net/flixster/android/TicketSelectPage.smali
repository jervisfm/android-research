.class public Lnet/flixster/android/TicketSelectPage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "TicketSelectPage.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/TicketSelectPage$TicketbarHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/flixster/android/activity/common/DecoratedSherlockActivity;",
        "Landroid/view/View$OnKeyListener;",
        "Landroid/view/View$OnClickListener;",
        "Landroid/view/View$OnFocusChangeListener;",
        "Landroid/widget/AdapterView$OnItemSelectedListener;"
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final BUNDLE_DATE:Ljava/lang/String; = "net.android.flixster.TicketSelectPage.BUNDLE_DATE"

.field public static final BUNDLE_DISPLAY_DATETIME:Ljava/lang/String; = "net.android.flixster.TicketSelectPage.BUNDLE_DISPLAY_DATETIME"

.field public static final BUNDLE_LISTING:Ljava/lang/String; = "net.android.flixster.TicketSelectPage.BUNDLE_LISTING"

.field public static final BUNDLE_MIDNIGHT_DISCLAIMER:Ljava/lang/String; = "net.android.flixster.TicketSelectPage.BUNDLE_MIDNIGHT_DISCLAIMER"

.field public static final BUNDLE_THEATER_ID:Ljava/lang/String; = "net.android.flixster.TicketSelectPage.BUNDLE_THEATER_ID"

.field public static final BUNDLE_TIME:Ljava/lang/String; = "net.android.flixster.TicketSelectPage.BUNDLE_TIME"

.field public static final DIALOGKEY_LOADING_PRICING:I = 0x4

.field public static final DIALOGKEY_NETWORKFAIL:I = 0x8

.field public static final DIALOGKEY_PURCHASE_ERROR:I = 0x6

.field public static final DIALOGKEY_PURCHASE_TIMEOUT_ERROR:I = 0x7

.field public static final DIALOGKEY_PURCHASING:I = 0x5

.field public static final DIALOGKEY_SELECT_EXPMONTH:I = 0x1

.field public static final DIALOGKEY_SELECT_EXPYEAR:I = 0x2

.field public static final DIALOGKEY_TICKET_AVAILABILITY:I = 0x3

.field public static final FLOAT2PRICE_FORMAT:Ljava/lang/String; = "$%.2f"

.field public static final MAXIMUM_TICKETS:I = 0xa

.field public static final MONTH_FORMAT:Ljava/lang/String; = "%02d"

.field public static final STATE_CONFIRM_RESERVATION:I = 0x2

.field public static final STATE_DISPLAY_RECEIPT:I = 0x3

.field public static final STATE_FIX_FIELDS:I = 0x1

.field public static final STATE_SELECT_TICKETS:I = 0x0

.field private static final YEAROPTIONS:I = 0xa

.field public static final YEAR_FORMAT:Ljava/lang/String; = "%4d"


# instance fields
.field public final INT:I

.field public final PRICE:I

.field private addTicketBars:Landroid/os/Handler;

.field private mAlertIconCheck:Landroid/graphics/drawable/Drawable;

.field private mAlertIconError:Landroid/graphics/drawable/Drawable;

.field private mCardMapType:[I

.field private mCardMonth:I

.field private mCardNumberFieldState:I

.field private mCardType:I

.field private mCardTypeIndex:I

.field private mCardYear:I

.field private mCardYearOptions:[Ljava/lang/CharSequence;

.field private mCategories:I

.field private mCategoryLabels:[Ljava/lang/String;

.field private mCategoryPrices:[F

.field private mCodeFieldState:I

.field mCompletePurchaseButton:Landroid/widget/Button;

.field private mConfirmReservationLayout:Landroid/widget/LinearLayout;

.field mContinueButton:Landroid/widget/Button;

.field private mConvenienceTotal:F

.field private mConvenienceTotalView:Landroid/widget/TextView;

.field private mEmailFieldState:I

.field mEmailPattern:Ljava/util/regex/Pattern;

.field private mErrorCardnumberText:Landroid/widget/TextView;

.field private mErrorCodenumberText:Landroid/widget/TextView;

.field private mErrorEmailText:Landroid/widget/TextView;

.field private mErrorExpDateText:Landroid/widget/TextView;

.field private mErrorPostalnumberText:Landroid/widget/TextView;

.field private mErrorTicketCountText:Landroid/widget/TextView;

.field private mErrorTicketText:Landroid/widget/RelativeLayout;

.field private mExpDateFieldState:I

.field private mExtras:Landroid/os/Bundle;

.field private mFetchPerformanceTrys:I

.field private mLoadingPricingDialog:Landroid/app/ProgressDialog;

.field private mMethodIds:[Ljava/lang/String;

.field private mMethodNames:[Ljava/lang/String;

.field private mMovie:Lnet/flixster/android/model/Movie;

.field private mMovieId:J

.field private mMovieTitle:Ljava/lang/String;

.field private mParamDate:Ljava/lang/String;

.field private mParamDisplayDatetime:Ljava/lang/String;

.field private mParamListing:Ljava/lang/String;

.field private mParamTheater:Ljava/lang/String;

.field private mParamTime:Ljava/lang/String;

.field private mPerformance:Lnet/flixster/android/model/Performance;

.field private mPostalFieldState:I

.field private mPosterView:Landroid/widget/ImageView;

.field private mPurchaseDialog:Landroid/app/ProgressDialog;

.field private mPurchaseError:Landroid/app/Dialog;

.field private mPurchaseErrorHandler:Landroid/os/Handler;

.field private volatile mPurchaseErrorMessage:Ljava/lang/String;

.field private mReceiptInfoLayout:Landroid/widget/LinearLayout;

.field private mScrollAd:Lnet/flixster/android/ads/AdView;

.field private mScrollView:Landroid/widget/ScrollView;

.field private mSelectTicketsLayout:Landroid/widget/LinearLayout;

.field private mSelectToReservationFieldHandler:Landroid/os/Handler;

.field private mShowNetorkErrorDialogHandler:Landroid/os/Handler;

.field private mShowPurchaseErrorHandler:Landroid/os/Handler;

.field private mShowPurchaseTimeoutErrorHandler:Landroid/os/Handler;

.field private mShowtimeTextView:Landroid/widget/TextView;

.field private mStartGetReservationDialogHandler:Landroid/os/Handler;

.field private mStartLoadPricingDialogHandler:Landroid/os/Handler;

.field private mStartPurchaseDialogHandler:Landroid/os/Handler;

.field private mState:I

.field private mStateLayout:Landroid/os/Handler;

.field private mStopGetReservationDialogHandler:Landroid/os/Handler;

.field private mStopLoadPricingDialogHandler:Landroid/os/Handler;

.field private mStopPurchaseDialogHandler:Landroid/os/Handler;

.field private mSurcharge:F

.field private mTheaterAddress:Ljava/lang/String;

.field private mTheaterName:Ljava/lang/String;

.field private mThisMonth:I

.field private mThisYear:I

.field private mTicketAvailabilityDialog:Landroid/app/ProgressDialog;

.field private mTicketBarLinearLayout:Landroid/widget/LinearLayout;

.field private mTicketCardNumber:Landroid/widget/TextView;

.field private mTicketCodeNumber:Landroid/widget/TextView;

.field private mTicketConfirmationNumber:Landroid/widget/TextView;

.field private mTicketCount:F

.field private mTicketCountState:I

.field private mTicketCounts:[F

.field private mTicketEmail:Landroid/widget/TextView;

.field private mTicketExpDateLabel:Landroid/widget/TextView;

.field private mTicketExpMonth:Landroid/widget/TextView;

.field private mTicketExpYear:Landroid/widget/TextView;

.field private mTicketPostalNumber:Landroid/widget/TextView;

.field private mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;

.field private mTicketSelectShowtime:Landroid/widget/TextView;

.field private mTicketSelectTheaterAddress:Landroid/widget/TextView;

.field private mTicketSelectTheaterName:Landroid/widget/TextView;

.field private mTicketSelectTitle:Landroid/widget/TextView;

.field private mTicketTotals:[F

.field private mTicketsConfirmMessage:Landroid/widget/TextView;

.field mTimer:Ljava/util/Timer;

.field private mTotal:F

.field private mTotalView:Landroid/widget/TextView;

.field private setupCardSpinner:Landroid/os/Handler;

.field private updateCardStatus:Landroid/os/Handler;

.field private updatePrice:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 78
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mFetchPerformanceTrys:I

    .line 91
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    .line 140
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    .line 141
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    .line 142
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    .line 143
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mExpDateFieldState:I

    .line 144
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mEmailFieldState:I

    .line 146
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCountState:I

    .line 150
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCount:F

    .line 159
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketTotals:[F

    .line 161
    const/high16 v0, 0x3f80

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mSurcharge:F

    .line 162
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mConvenienceTotal:F

    .line 163
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mTotal:F

    .line 168
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->INT:I

    .line 169
    const/4 v0, 0x1

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->PRICE:I

    .line 186
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardTypeIndex:I

    .line 187
    iput v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    .line 188
    iput v3, p0, Lnet/flixster/android/TicketSelectPage;->mCardMonth:I

    .line 189
    const/16 v0, -0x64

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardYear:I

    .line 191
    iput v3, p0, Lnet/flixster/android/TicketSelectPage;->mThisYear:I

    .line 192
    iput v3, p0, Lnet/flixster/android/TicketSelectPage;->mThisMonth:I

    .line 376
    new-instance v0, Lnet/flixster/android/TicketSelectPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$1;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->addTicketBars:Landroid/os/Handler;

    .line 419
    new-instance v0, Lnet/flixster/android/TicketSelectPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$2;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->setupCardSpinner:Landroid/os/Handler;

    .line 705
    new-instance v0, Lnet/flixster/android/TicketSelectPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$3;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updatePrice:Landroid/os/Handler;

    .line 767
    new-instance v0, Lnet/flixster/android/TicketSelectPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$4;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    .line 1131
    new-instance v0, Lnet/flixster/android/TicketSelectPage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$5;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStartGetReservationDialogHandler:Landroid/os/Handler;

    .line 1142
    new-instance v0, Lnet/flixster/android/TicketSelectPage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$6;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStopGetReservationDialogHandler:Landroid/os/Handler;

    .line 1153
    new-instance v0, Lnet/flixster/android/TicketSelectPage$7;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$7;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStartLoadPricingDialogHandler:Landroid/os/Handler;

    .line 1165
    new-instance v0, Lnet/flixster/android/TicketSelectPage$8;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$8;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStopLoadPricingDialogHandler:Landroid/os/Handler;

    .line 1181
    new-instance v0, Lnet/flixster/android/TicketSelectPage$9;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$9;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStartPurchaseDialogHandler:Landroid/os/Handler;

    .line 1192
    new-instance v0, Lnet/flixster/android/TicketSelectPage$10;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$10;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStopPurchaseDialogHandler:Landroid/os/Handler;

    .line 1203
    new-instance v0, Lnet/flixster/android/TicketSelectPage$11;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$11;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorHandler:Landroid/os/Handler;

    .line 1212
    new-instance v0, Lnet/flixster/android/TicketSelectPage$12;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$12;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mShowNetorkErrorDialogHandler:Landroid/os/Handler;

    .line 1221
    new-instance v0, Lnet/flixster/android/TicketSelectPage$13;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$13;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mShowPurchaseErrorHandler:Landroid/os/Handler;

    .line 1232
    new-instance v0, Lnet/flixster/android/TicketSelectPage$14;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$14;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mShowPurchaseTimeoutErrorHandler:Landroid/os/Handler;

    .line 1243
    new-instance v0, Lnet/flixster/android/TicketSelectPage$15;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$15;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStateLayout:Landroid/os/Handler;

    .line 1288
    new-instance v0, Lnet/flixster/android/TicketSelectPage$16;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$16;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mSelectToReservationFieldHandler:Landroid/os/Handler;

    .line 54
    return-void

    .line 159
    :array_0
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private ScheduleGetReservation()V
    .locals 4

    .prologue
    .line 551
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TicketSelectPage.ScheduleTicketAvailabilityCheck() define mTimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    new-instance v0, Lnet/flixster/android/TicketSelectPage$18;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$18;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    .line 589
    .local v0, performanceLoadTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 590
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 592
    :cond_0
    return-void
.end method

.method private ScheduleLoadPerformanceTask()V
    .locals 4

    .prologue
    .line 490
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TicketSelectPage.ScheduleLoadPerformanceTask() define mTimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    new-instance v0, Lnet/flixster/android/TicketSelectPage$17;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$17;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    .line 545
    .local v0, performanceLoadTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 546
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 548
    :cond_0
    return-void
.end method

.method private SchedulePurchase()V
    .locals 4

    .prologue
    .line 595
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TicketSelectPage.SchedulePurchase define mTimer:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    new-instance v0, Lnet/flixster/android/TicketSelectPage$19;

    invoke-direct {v0, p0}, Lnet/flixster/android/TicketSelectPage$19;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    .line 636
    .local v0, performancePurchaseTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 637
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 639
    :cond_0
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/TicketSelectPage;
    .locals 1
    .parameter

    .prologue
    .line 59
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 149
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketBarLinearLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$10(Lnet/flixster/android/TicketSelectPage;)[F
    .locals 1
    .parameter

    .prologue
    .line 159
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketTotals:[F

    return-object v0
.end method

.method static synthetic access$11(Lnet/flixster/android/TicketSelectPage;)[F
    .locals 1
    .parameter

    .prologue
    .line 151
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F

    return-object v0
.end method

.method static synthetic access$12(Lnet/flixster/android/TicketSelectPage;)F
    .locals 1
    .parameter

    .prologue
    .line 163
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mTotal:F

    return v0
.end method

.method static synthetic access$13(Lnet/flixster/android/TicketSelectPage;)F
    .locals 1
    .parameter

    .prologue
    .line 150
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCount:F

    return v0
.end method

.method static synthetic access$14(Lnet/flixster/android/TicketSelectPage;F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 162
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mConvenienceTotal:F

    return-void
.end method

.method static synthetic access$15(Lnet/flixster/android/TicketSelectPage;)F
    .locals 1
    .parameter

    .prologue
    .line 162
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mConvenienceTotal:F

    return v0
.end method

.method static synthetic access$16(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 155
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mConvenienceTotalView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$17(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 156
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTotalView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$18(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 146
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCountState:I

    return v0
.end method

.method static synthetic access$19(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 138
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorTicketCountText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/TicketSelectPage;)Lnet/flixster/android/model/Performance;
    .locals 1
    .parameter

    .prologue
    .line 66
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;

    return-object v0
.end method

.method static synthetic access$20(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 140
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    return v0
.end method

.method static synthetic access$21(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 173
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$22(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .parameter

    .prologue
    .line 165
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$23(Lnet/flixster/android/TicketSelectPage;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .parameter

    .prologue
    .line 166
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$24(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 133
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$25(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 141
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    return v0
.end method

.method static synthetic access$26(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 177
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$27(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 134
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCodenumberText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$28(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 142
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    return v0
.end method

.method static synthetic access$29(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 178
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 153
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mCategoryLabels:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$30(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 135
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorPostalnumberText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$31(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 188
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardMonth:I

    return v0
.end method

.method static synthetic access$32(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 175
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpMonth:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$33(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 189
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardYear:I

    return v0
.end method

.method static synthetic access$34(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 176
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpYear:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$35(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 191
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mThisYear:I

    return v0
.end method

.method static synthetic access$36(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 143
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mExpDateFieldState:I

    return v0
.end method

.method static synthetic access$37(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 174
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpDateLabel:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$38(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 136
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorExpDateText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$39(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 144
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mEmailFieldState:I

    return v0
.end method

.method static synthetic access$4(Lnet/flixster/android/TicketSelectPage;)[F
    .locals 1
    .parameter

    .prologue
    .line 154
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F

    return-object v0
.end method

.method static synthetic access$40(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 179
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$41(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 137
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorEmailText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$42(Lnet/flixster/android/TicketSelectPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 102
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketAvailabilityDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$43(Lnet/flixster/android/TicketSelectPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 103
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mLoadingPricingDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$44(Lnet/flixster/android/TicketSelectPage;)Landroid/app/ProgressDialog;
    .locals 1
    .parameter

    .prologue
    .line 104
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$45(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 91
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    return v0
.end method

.method static synthetic access$46(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 73
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mSelectTicketsLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$47(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 74
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mConfirmReservationLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$48(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorTicketText:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$49(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 75
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mReceiptInfoLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/TicketSelectPage;)F
    .locals 1
    .parameter

    .prologue
    .line 161
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mSurcharge:F

    return v0
.end method

.method static synthetic access$50(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1337
    invoke-direct {p0}, Lnet/flixster/android/TicketSelectPage;->hardValidateAll()V

    return-void
.end method

.method static synthetic access$51(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 705
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updatePrice:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$52(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 767
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$53(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1288
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mSelectToReservationFieldHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$54(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 76
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketsConfirmMessage:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$55(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/ScrollView;
    .locals 1
    .parameter

    .prologue
    .line 72
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mScrollView:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$56(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 68
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mMovieTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$57(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 69
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTheaterName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$58(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 152
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    return v0
.end method

.method static synthetic access$59(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 61
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mParamTheater:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 183
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mMethodNames:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$60(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mParamListing:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$61(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 63
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mParamDate:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$62(Lnet/flixster/android/TicketSelectPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 64
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mParamTime:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$63(Lnet/flixster/android/TicketSelectPage;Lnet/flixster/android/model/Performance;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 66
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;

    return-void
.end method

.method static synthetic access$64(Lnet/flixster/android/TicketSelectPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 152
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    return-void
.end method

.method static synthetic access$65(Lnet/flixster/android/TicketSelectPage;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 153
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mCategoryLabels:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$66(Lnet/flixster/android/TicketSelectPage;[F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 154
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F

    return-void
.end method

.method static synthetic access$67(Lnet/flixster/android/TicketSelectPage;[F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 159
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mTicketTotals:[F

    return-void
.end method

.method static synthetic access$68(Lnet/flixster/android/TicketSelectPage;[F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 151
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F

    return-void
.end method

.method static synthetic access$69(Lnet/flixster/android/TicketSelectPage;F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 161
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mSurcharge:F

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 186
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardTypeIndex:I

    return v0
.end method

.method static synthetic access$70(Lnet/flixster/android/TicketSelectPage;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 182
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mMethodIds:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$71(Lnet/flixster/android/TicketSelectPage;[Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 183
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mMethodNames:[Ljava/lang/String;

    return-void
.end method

.method static synthetic access$72(Lnet/flixster/android/TicketSelectPage;)[Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 182
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mMethodIds:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$73(Lnet/flixster/android/TicketSelectPage;[I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 185
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mCardMapType:[I

    return-void
.end method

.method static synthetic access$74(Lnet/flixster/android/TicketSelectPage;)[I
    .locals 1
    .parameter

    .prologue
    .line 185
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardMapType:[I

    return-object v0
.end method

.method static synthetic access$75(Lnet/flixster/android/TicketSelectPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 187
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    return-void
.end method

.method static synthetic access$76(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 376
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->addTicketBars:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$77(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 419
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->setupCardSpinner:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$78(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1165
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStopLoadPricingDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$79(Lnet/flixster/android/TicketSelectPage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 106
    iput-object p1, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorMessage:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$8(Lnet/flixster/android/TicketSelectPage;F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 163
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mTotal:F

    return-void
.end method

.method static synthetic access$80(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1232
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mShowPurchaseTimeoutErrorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$81(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 78
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mFetchPerformanceTrys:I

    return v0
.end method

.method static synthetic access$82(Lnet/flixster/android/TicketSelectPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 78
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mFetchPerformanceTrys:I

    return-void
.end method

.method static synthetic access$83(Lnet/flixster/android/TicketSelectPage;)V
    .locals 0
    .parameter

    .prologue
    .line 489
    invoke-direct {p0}, Lnet/flixster/android/TicketSelectPage;->ScheduleLoadPerformanceTask()V

    return-void
.end method

.method static synthetic access$84(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1212
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mShowNetorkErrorDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$85(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1142
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStopGetReservationDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$86(Lnet/flixster/android/TicketSelectPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 91
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    return-void
.end method

.method static synthetic access$87(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1243
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStateLayout:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$88(Lnet/flixster/android/TicketSelectPage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 198
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketConfirmationNumber:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$89(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1192
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStopPurchaseDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$9(Lnet/flixster/android/TicketSelectPage;F)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 150
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCount:F

    return-void
.end method

.method static synthetic access$90(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1203
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$91(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1221
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mShowPurchaseErrorHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$92(Lnet/flixster/android/TicketSelectPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 188
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mCardMonth:I

    return-void
.end method

.method static synthetic access$93(Lnet/flixster/android/TicketSelectPage;)I
    .locals 1
    .parameter

    .prologue
    .line 192
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mThisMonth:I

    return v0
.end method

.method static synthetic access$94(Lnet/flixster/android/TicketSelectPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 143
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mExpDateFieldState:I

    return-void
.end method

.method static synthetic access$95(Lnet/flixster/android/TicketSelectPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 189
    iput p1, p0, Lnet/flixster/android/TicketSelectPage;->mCardYear:I

    return-void
.end method

.method static synthetic access$96(Lnet/flixster/android/TicketSelectPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 1153
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStartLoadPricingDialogHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private hardValidateAll()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x3

    .line 1338
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    if-eq v0, v1, :cond_0

    .line 1339
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    .line 1341
    :cond_0
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    if-eq v0, v1, :cond_1

    .line 1342
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    .line 1344
    :cond_1
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    if-eq v0, v1, :cond_2

    .line 1345
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    .line 1347
    :cond_2
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mExpDateFieldState:I

    if-eq v0, v1, :cond_3

    .line 1348
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mExpDateFieldState:I

    .line 1350
    :cond_3
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mEmailFieldState:I

    if-eq v0, v1, :cond_4

    .line 1351
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mEmailFieldState:I

    .line 1353
    :cond_4
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCount:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_5

    .line 1354
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCountState:I

    .line 1356
    :cond_5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .parameter "v"

    .prologue
    const/4 v4, 0x3

    const/high16 v7, 0x3f80

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 644
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 703
    :cond_0
    :goto_0
    return-void

    .line 646
    :sswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 647
    .local v0, i:Ljava/lang/Integer;
    if-nez v0, :cond_1

    .line 648
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "TicketSelect.onClick inc button without tag"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 650
    :cond_1
    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCount:F

    const/high16 v3, 0x4120

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    .line 651
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget v4, v2, v3

    add-float/2addr v4, v7

    aput v4, v2, v3

    .line 652
    iput v6, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCountState:I

    .line 656
    :goto_1
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->updatePrice:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 654
    :cond_2
    iput v4, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCountState:I

    goto :goto_1

    .line 659
    .end local v0           #i:Ljava/lang/Integer;
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 660
    .restart local v0       #i:Ljava/lang/Integer;
    if-nez v0, :cond_3

    .line 661
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "TicketSelect.onClick dec button without tag"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 663
    :cond_3
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget v2, v2, v3

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    .line 664
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget v4, v2, v3

    sub-float/2addr v4, v7

    aput v4, v2, v3

    .line 665
    iput v6, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCountState:I

    .line 667
    :cond_4
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->updatePrice:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 670
    .end local v0           #i:Ljava/lang/Integer;
    :sswitch_2
    const-string v2, "FlxMain"

    const-string v3, "TicketSelectPage.onClick ticket_expmonth"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 671
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lnet/flixster/android/TicketSelectPage;->showDialog(I)V

    goto :goto_0

    .line 674
    :sswitch_3
    const-string v2, "FlxMain"

    const-string v3, "TicketSelectPage.onClick ticket_expyear"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    invoke-virtual {p0, v6}, Lnet/flixster/android/TicketSelectPage;->showDialog(I)V

    goto :goto_0

    .line 678
    :sswitch_4
    const-string v2, "FlxMain"

    const-string v3, "TicketSelectPage.onClick ticket_continue_button"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 679
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixsterApplication;->setTicketEmail(Ljava/lang/String;)V

    .line 680
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;

    if-eqz v2, :cond_5

    .line 681
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;

    invoke-virtual {v2}, Lnet/flixster/android/model/Performance;->clearError()V

    .line 684
    :cond_5
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/data/TicketsDao;->validateEmail(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mEmailFieldState:I

    .line 686
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mStartGetReservationDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 687
    const-string v2, "input_method"

    invoke-virtual {p0, v2}, Lnet/flixster/android/TicketSelectPage;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 688
    .local v1, imm:Landroid/view/inputmethod/InputMethodManager;
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 690
    invoke-direct {p0}, Lnet/flixster/android/TicketSelectPage;->ScheduleGetReservation()V

    goto/16 :goto_0

    .line 693
    .end local v1           #imm:Landroid/view/inputmethod/InputMethodManager;
    :sswitch_5
    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    if-ne v2, v6, :cond_6

    .line 694
    const-string v2, "FlxMain"

    const-string v3, "TicketSelectPage.onClick ticket_completepurchase_button"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mStartPurchaseDialogHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 696
    invoke-direct {p0}, Lnet/flixster/android/TicketSelectPage;->SchedulePurchase()V

    goto/16 :goto_0

    .line 697
    :cond_6
    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    if-ne v2, v4, :cond_0

    .line 698
    invoke-virtual {p0}, Lnet/flixster/android/TicketSelectPage;->finish()V

    goto/16 :goto_0

    .line 644
    :sswitch_data_0
    .sparse-switch
        0x7f070294 -> :sswitch_2
        0x7f070295 -> :sswitch_3
        0x7f0702a3 -> :sswitch_4
        0x7f0702c0 -> :sswitch_5
        0x7f0702c2 -> :sswitch_0
        0x7f0702c4 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 13
    .parameter "savedInstanceState"

    .prologue
    .line 202
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 203
    iput-object p0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectPage:Lnet/flixster/android/TicketSelectPage;

    .line 205
    const v8, 0x7f030088

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->setContentView(I)V

    .line 206
    invoke-virtual {p0}, Lnet/flixster/android/TicketSelectPage;->createActionBar()V

    .line 207
    const v8, 0x7f0c0098

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 209
    const v8, 0x7f07002a

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/ads/AdView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mScrollAd:Lnet/flixster/android/ads/AdView;

    .line 210
    invoke-virtual {p0}, Lnet/flixster/android/TicketSelectPage;->getIntent()Landroid/content/Intent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    .line 211
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "TicketSelectPage.onCreate mExtras:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    if-eqz v8, :cond_0

    .line 213
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "net.android.flixster.TicketSelectPage.BUNDLE_THEATER_ID"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mParamTheater:Ljava/lang/String;

    .line 214
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "net.android.flixster.TicketSelectPage.BUNDLE_LISTING"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mParamListing:Ljava/lang/String;

    .line 215
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "net.android.flixster.TicketSelectPage.BUNDLE_DATE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mParamDate:Ljava/lang/String;

    .line 216
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "net.android.flixster.TicketSelectPage.BUNDLE_TIME"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mParamTime:Ljava/lang/String;

    .line 217
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "net.android.flixster.TicketSelectPage.BUNDLE_DISPLAY_DATETIME"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mParamDisplayDatetime:Ljava/lang/String;

    .line 219
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "SHOWTIMES_TITLE"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mMovieTitle:Ljava/lang/String;

    .line 220
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "name"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTheaterName:Ljava/lang/String;

    .line 221
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "address"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTheaterAddress:Ljava/lang/String;

    .line 222
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "MOVIE_ID"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    iput-wide v8, p0, Lnet/flixster/android/TicketSelectPage;->mMovieId:J

    .line 224
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "TicketSelectPage.onCreate mParamTime:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lnet/flixster/android/TicketSelectPage;->mParamTime:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    :cond_0
    const v8, 0x7f07027a

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ScrollView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mScrollView:Landroid/widget/ScrollView;

    .line 229
    const v8, 0x7f07027b

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mSelectTicketsLayout:Landroid/widget/LinearLayout;

    .line 230
    const v8, 0x7f0702a4

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mConfirmReservationLayout:Landroid/widget/LinearLayout;

    .line 231
    const v8, 0x7f0702a6

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mReceiptInfoLayout:Landroid/widget/LinearLayout;

    .line 232
    const v8, 0x7f0702af

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketsConfirmMessage:Landroid/widget/TextView;

    .line 234
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mStateLayout:Landroid/os/Handler;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 237
    const v8, 0x7f07026b

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectTitle:Landroid/widget/TextView;

    .line 238
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectTitle:Landroid/widget/TextView;

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mMovieTitle:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    const v8, 0x7f07026f

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectTheaterName:Landroid/widget/TextView;

    .line 240
    const v8, 0x7f0702b6

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectShowtime:Landroid/widget/TextView;

    .line 241
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectShowtime:Landroid/widget/TextView;

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mParamDisplayDatetime:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectTheaterName:Landroid/widget/TextView;

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mTheaterName:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    const v8, 0x7f070270

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectTheaterAddress:Landroid/widget/TextView;

    .line 244
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketSelectTheaterAddress:Landroid/widget/TextView;

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mTheaterAddress:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    const v8, 0x7f070282

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mShowtimeTextView:Landroid/widget/TextView;

    .line 246
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mShowtimeTextView:Landroid/widget/TextView;

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mParamDisplayDatetime:Ljava/lang/String;

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 247
    const v8, 0x7f070283

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 248
    .local v7, ticketSelectShowtimeFootnote:Landroid/widget/TextView;
    const v8, 0x7f0702b7

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 249
    .local v1, completePurchaseShowtimeFootnote:Landroid/widget/TextView;
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mExtras:Landroid/os/Bundle;

    const-string v9, "net.android.flixster.TicketSelectPage.BUNDLE_MIDNIGHT_DISCLAIMER"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 250
    .local v5, midnightDisclaimer:Ljava/lang/String;
    if-nez v5, :cond_3

    .line 251
    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 252
    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    :goto_0
    const v8, 0x7f070280

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/ImageView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mPosterView:Landroid/widget/ImageView;

    .line 265
    iget-wide v8, p0, Lnet/flixster/android/TicketSelectPage;->mMovieId:J

    invoke-static {v8, v9}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mMovie:Lnet/flixster/android/model/Movie;

    .line 266
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v8, v8, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v8}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v8

    if-eqz v8, :cond_1

    .line 267
    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mPosterView:Landroid/widget/ImageView;

    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v8, v8, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v8}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    invoke-virtual {v9, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 271
    :cond_1
    invoke-virtual {p0}, Lnet/flixster/android/TicketSelectPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020061

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;

    .line 272
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget-object v11, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    iget-object v12, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconError:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 273
    invoke-virtual {p0}, Lnet/flixster/android/TicketSelectPage;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020060

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;

    .line 274
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;

    const/4 v9, 0x0

    const/4 v10, 0x0

    iget-object v11, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v11}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v11

    iget-object v12, p0, Lnet/flixster/android/TicketSelectPage;->mAlertIconCheck:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v12}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v12

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 276
    const v8, 0x7f07027c

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorTicketText:Landroid/widget/RelativeLayout;

    .line 277
    const v8, 0x7f07029a

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;

    .line 278
    const v8, 0x7f07029c

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCodenumberText:Landroid/widget/TextView;

    .line 279
    const v8, 0x7f07029d

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorPostalnumberText:Landroid/widget/TextView;

    .line 280
    const v8, 0x7f07029b

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorExpDateText:Landroid/widget/TextView;

    .line 281
    const v8, 0x7f0702a1

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorEmailText:Landroid/widget/TextView;

    .line 283
    const v8, 0x7f070288

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorTicketCountText:Landroid/widget/TextView;

    .line 285
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorTicketText:Landroid/widget/RelativeLayout;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 286
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mErrorTicketCountText:Landroid/widget/TextView;

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    const v8, 0x7f070284

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketBarLinearLayout:Landroid/widget/LinearLayout;

    .line 291
    const v8, 0x7f070286

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mConvenienceTotalView:Landroid/widget/TextView;

    .line 292
    const v8, 0x7f07028a

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTotalView:Landroid/widget/TextView;

    .line 294
    const v8, 0x7f070292

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;

    .line 295
    const v8, 0x7f070293

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpDateLabel:Landroid/widget/TextView;

    .line 296
    const v8, 0x7f070294

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpMonth:Landroid/widget/TextView;

    .line 297
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpMonth:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    .line 298
    const v8, 0x7f070295

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpYear:Landroid/widget/TextView;

    .line 299
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpYear:Landroid/widget/TextView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setFocusableInTouchMode(Z)V

    .line 300
    const v8, 0x7f070297

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;

    .line 301
    const v8, 0x7f070299

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;

    .line 302
    const v8, 0x7f070299

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;

    .line 303
    const v8, 0x7f0702a0

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;

    .line 305
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTicketEmail()Ljava/lang/String;

    move-result-object v6

    .line 306
    .local v6, tempEmail:Ljava/lang/String;
    if-eqz v6, :cond_2

    .line 307
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 310
    :cond_2
    const/4 v8, 0x6

    new-array v0, v8, [Landroid/widget/TextView;

    const/4 v8, 0x0

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;

    aput-object v9, v0, v8

    const/4 v8, 0x1

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpMonth:Landroid/widget/TextView;

    aput-object v9, v0, v8

    const/4 v8, 0x2

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpYear:Landroid/widget/TextView;

    aput-object v9, v0, v8

    const/4 v8, 0x3

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;

    aput-object v9, v0, v8

    const/4 v8, 0x4

    .line 311
    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;

    aput-object v9, v0, v8

    const/4 v8, 0x5

    iget-object v9, p0, Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;

    aput-object v9, v0, v8

    .line 312
    .local v0, cardFieldViews:[Landroid/widget/TextView;
    array-length v9, v0

    const/4 v8, 0x0

    :goto_1
    if-lt v8, v9, :cond_4

    .line 319
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 320
    .local v3, date:Ljava/util/Calendar;
    const/4 v8, 0x2

    invoke-virtual {v3, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    iput v8, p0, Lnet/flixster/android/TicketSelectPage;->mThisMonth:I

    .line 321
    const/4 v8, 0x1

    invoke-virtual {v3, v8}, Ljava/util/Calendar;->get(I)I

    move-result v8

    iput v8, p0, Lnet/flixster/android/TicketSelectPage;->mThisYear:I

    .line 323
    const/16 v8, 0xa

    new-array v8, v8, [Ljava/lang/CharSequence;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mCardYearOptions:[Ljava/lang/CharSequence;

    .line 324
    const/4 v4, 0x0

    .local v4, i:I
    :goto_2
    const/16 v8, 0xa

    if-lt v4, v8, :cond_5

    .line 328
    const-string v8, "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$"

    invoke-static {v8}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v8

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mEmailPattern:Ljava/util/regex/Pattern;

    .line 330
    new-instance v8, Ljava/util/Timer;

    invoke-direct {v8}, Ljava/util/Timer;-><init>()V

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTimer:Ljava/util/Timer;

    .line 332
    const v8, 0x7f0702a3

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mContinueButton:Landroid/widget/Button;

    .line 333
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mContinueButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 335
    const v8, 0x7f0702c0

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/Button;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mCompletePurchaseButton:Landroid/widget/Button;

    .line 336
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mCompletePurchaseButton:Landroid/widget/Button;

    invoke-virtual {v8, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 338
    const v8, 0x7f0702ac

    invoke-virtual {p0, v8}, Lnet/flixster/android/TicketSelectPage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    iput-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mTicketConfirmationNumber:Landroid/widget/TextView;

    .line 339
    return-void

    .line 254
    .end local v0           #cardFieldViews:[Landroid/widget/TextView;
    .end local v3           #date:Ljava/util/Calendar;
    .end local v4           #i:I
    .end local v6           #tempEmail:Ljava/lang/String;
    :cond_3
    invoke-virtual {v7, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 255
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 312
    .restart local v0       #cardFieldViews:[Landroid/widget/TextView;
    .restart local v6       #tempEmail:Ljava/lang/String;
    :cond_4
    aget-object v2, v0, v8

    .line 313
    .local v2, cv:Landroid/widget/TextView;
    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 314
    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 315
    invoke-virtual {v2, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 312
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 325
    .end local v2           #cv:Landroid/widget/TextView;
    .restart local v3       #date:Ljava/util/Calendar;
    .restart local v4       #i:I
    :cond_5
    iget-object v8, p0, Lnet/flixster/android/TicketSelectPage;->mCardYearOptions:[Ljava/lang/CharSequence;

    const-string v9, "%4d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v12, p0, Lnet/flixster/android/TicketSelectPage;->mThisYear:I

    add-int/2addr v12, v4

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    .line 324
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .parameter "id"

    .prologue
    const/4 v0, 0x0

    const v5, 0x7f0c004a

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 923
    packed-switch p1, :pswitch_data_0

    .line 1014
    :goto_0
    return-object v0

    .line 925
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c00ac

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 926
    const v1, 0x7f0e000a

    new-instance v2, Lnet/flixster/android/TicketSelectPage$20;

    invoke-direct {v2, p0}, Lnet/flixster/android/TicketSelectPage$20;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 932
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 934
    :pswitch_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0c00ad

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 935
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardYearOptions:[Ljava/lang/CharSequence;

    new-instance v2, Lnet/flixster/android/TicketSelectPage$21;

    invoke-direct {v2, p0}, Lnet/flixster/android/TicketSelectPage$21;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 941
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 943
    :pswitch_2
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketAvailabilityDialog:Landroid/app/ProgressDialog;

    .line 944
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketAvailabilityDialog:Landroid/app/ProgressDialog;

    const-string v1, "Checking Ticket Availability..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 945
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketAvailabilityDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 946
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketAvailabilityDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 947
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketAvailabilityDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 949
    :pswitch_3
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mLoadingPricingDialog:Landroid/app/ProgressDialog;

    .line 950
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mLoadingPricingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lnet/flixster/android/TicketSelectPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0135

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 951
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mLoadingPricingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 952
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mLoadingPricingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 953
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mLoadingPricingDialog:Landroid/app/ProgressDialog;

    goto :goto_0

    .line 955
    :pswitch_4
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseDialog:Landroid/app/ProgressDialog;

    .line 956
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseDialog:Landroid/app/ProgressDialog;

    const-string v1, "Purchasing tickets..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 957
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 958
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 959
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 962
    :pswitch_5
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 963
    const-string v1, "Purchase Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 964
    const-string v1, "OK"

    new-instance v2, Lnet/flixster/android/TicketSelectPage$22;

    invoke-direct {v2, p0}, Lnet/flixster/android/TicketSelectPage$22;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 971
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 962
    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseError:Landroid/app/Dialog;

    .line 972
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseError:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 975
    :pswitch_6
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 976
    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseErrorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 977
    const-string v1, "Purchase Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 978
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 979
    invoke-virtual {p0}, Lnet/flixster/android/TicketSelectPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 980
    new-instance v2, Lnet/flixster/android/TicketSelectPage$23;

    invoke-direct {v2, p0}, Lnet/flixster/android/TicketSelectPage$23;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    .line 979
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 987
    const-string v1, "Call"

    new-instance v2, Lnet/flixster/android/TicketSelectPage$24;

    invoke-direct {v2, p0}, Lnet/flixster/android/TicketSelectPage$24;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 996
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 975
    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseError:Landroid/app/Dialog;

    .line 997
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPurchaseError:Landroid/app/Dialog;

    goto/16 :goto_0

    .line 999
    :pswitch_7
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1000
    const-string v2, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1001
    const-string v2, "Network Error"

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1002
    const-string v2, "Retry"

    new-instance v3, Lnet/flixster/android/TicketSelectPage$25;

    invoke-direct {v3, p0}, Lnet/flixster/android/TicketSelectPage$25;-><init>(Lnet/flixster/android/TicketSelectPage;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1012
    invoke-virtual {p0}, Lnet/flixster/android/TicketSelectPage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    .line 923
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 1360
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mScrollAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 373
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onDestroy()V

    .line 374
    return-void
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 6
    .parameter "v"
    .parameter "hasFocus"

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 856
    const-string v0, "FlxMain"

    const-string v1, "TicketSelectPage.onFocusChange"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 918
    :cond_0
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 919
    return-void

    .line 859
    :sswitch_0
    if-eqz p2, :cond_1

    .line 860
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    if-ne v0, v3, :cond_0

    .line 861
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 864
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 865
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 866
    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    .line 865
    invoke-static {v0, v1}, Lnet/flixster/android/data/TicketsDao;->validateCreditCard(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    .line 867
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    if-ne v0, v4, :cond_0

    .line 868
    iput v3, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    goto :goto_0

    .line 874
    :sswitch_1
    if-nez p2, :cond_0

    .line 875
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 879
    :sswitch_2
    if-eqz p2, :cond_2

    .line 880
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    if-ne v0, v3, :cond_0

    .line 881
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCodenumberText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 884
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCodenumberText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 885
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    invoke-static {v0, v1}, Lnet/flixster/android/data/TicketsDao;->validateCardCode(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    .line 886
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    if-ne v0, v4, :cond_3

    .line 887
    iput v3, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    .line 889
    :cond_3
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 893
    :sswitch_3
    if-eqz p2, :cond_4

    .line 894
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    if-ne v0, v3, :cond_0

    .line 895
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorPostalnumberText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 898
    :cond_4
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mErrorPostalnumberText:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 899
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/data/TicketsDao;->validatePostal(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    .line 900
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    if-ne v0, v4, :cond_5

    .line 901
    iput v3, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    .line 903
    :cond_5
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 907
    :sswitch_4
    if-nez p2, :cond_0

    .line 908
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 912
    :sswitch_5
    if-nez p2, :cond_0

    .line 913
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketEmail:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/data/TicketsDao;->validateEmail(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mEmailFieldState:I

    .line 914
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 857
    :sswitch_data_0
    .sparse-switch
        0x7f070292 -> :sswitch_0
        0x7f070294 -> :sswitch_1
        0x7f070295 -> :sswitch_4
        0x7f070297 -> :sswitch_2
        0x7f070299 -> :sswitch_3
        0x7f0702a0 -> :sswitch_5
    .end sparse-switch
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1115
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardMapType:[I

    if-eqz v0, :cond_0

    .line 1116
    iput p3, p0, Lnet/flixster/android/TicketSelectPage;->mCardTypeIndex:I

    .line 1117
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardMapType:[I

    aget v0, v0, p3

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    .line 1118
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1119
    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    .line 1118
    invoke-static {v0, v1}, Lnet/flixster/android/data/TicketsDao;->validateCreditCard(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    .line 1120
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    invoke-static {v0, v1}, Lnet/flixster/android/data/TicketsDao;->validateCardCode(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    .line 1121
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/data/TicketsDao;->validatePostal(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    .line 1122
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1124
    :cond_0
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 6
    .parameter "v"
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v5, 0x3

    const/16 v4, 0x16

    const/16 v3, 0x13

    const/4 v1, 0x0

    .line 1039
    const/16 v2, 0x3e

    if-ne p2, v2, :cond_1

    .line 1040
    const/4 v1, 0x1

    .line 1111
    :cond_0
    :goto_0
    return v1

    .line 1043
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1047
    :pswitch_1
    if-lt p2, v3, :cond_2

    if-le p2, v4, :cond_3

    .line 1048
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCardNumber:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1049
    iget v3, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    .line 1048
    invoke-static {v2, v3}, Lnet/flixster/android/data/TicketsDao;->validateCreditCard(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    .line 1050
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1052
    :cond_3
    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mCardNumberFieldState:I

    if-ne v2, v5, :cond_0

    .line 1053
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1059
    :pswitch_2
    if-lt p2, v3, :cond_4

    if-le p2, v4, :cond_5

    .line 1060
    :cond_4
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCodeNumber:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1061
    iget v3, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    .line 1060
    invoke-static {v2, v3}, Lnet/flixster/android/data/TicketsDao;->validateCardCode(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    .line 1062
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1064
    :cond_5
    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mCodeFieldState:I

    if-ne v2, v5, :cond_0

    .line 1065
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mErrorCardnumberText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1072
    :pswitch_3
    if-lt p2, v3, :cond_6

    if-le p2, v4, :cond_7

    .line 1073
    :cond_6
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketPostalNumber:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/data/TicketsDao;->validatePostal(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    .line 1074
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1076
    :cond_7
    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mPostalFieldState:I

    if-ne v2, v5, :cond_0

    .line 1077
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mErrorPostalnumberText:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1082
    :pswitch_4
    if-lt p2, v3, :cond_8

    if-le p2, v4, :cond_0

    .line 1083
    :cond_8
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->mTicketExpMonth:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1084
    .local v0, monthText:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_9

    .line 1085
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mCardMonth:I

    .line 1087
    :cond_9
    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mCardMonth:I

    iget v3, p0, Lnet/flixster/android/TicketSelectPage;->mCardYear:I

    iget v4, p0, Lnet/flixster/android/TicketSelectPage;->mThisMonth:I

    invoke-static {v2, v3, v4}, Lnet/flixster/android/data/TicketsDao;->validateDate(III)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mExpDateFieldState:I

    .line 1088
    iget-object v2, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0

    .line 1043
    :pswitch_data_0
    .packed-switch 0x7f070292
        :pswitch_1
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .parameter "keyCode"
    .parameter "event"

    .prologue
    const/4 v2, 0x0

    .line 1028
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1029
    iput v2, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    .line 1030
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStateLayout:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1031
    const/4 v0, 0x1

    .line 1033
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 2
    .parameter "keyCode"
    .parameter "event"

    .prologue
    .line 1019
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1020
    const/4 v0, 0x1

    .line 1022
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1129
    .local p1, arg0:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedInstanceState"

    .prologue
    .line 464
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 466
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TicketSelectPage.onRestoreInstanceState(.) pre  mCategories:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    const-string v0, "mState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    .line 468
    const-string v0, "mTicketCount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCount:F

    .line 469
    const-string v0, "mTicketCounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F

    .line 470
    const-string v0, "mCategories"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    .line 471
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TicketSelectPage.onRestoreInstanceState(.) post  mCategories:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const-string v0, "mCategoryLabels"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mCategoryLabels:[Ljava/lang/String;

    .line 474
    const-string v0, "mCategoryPrices"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getFloatArray(Ljava/lang/String;)[F

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F

    .line 476
    const-string v0, "mMethodIds"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mMethodIds:[Ljava/lang/String;

    .line 477
    const-string v0, "mMethodNames"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mMethodNames:[Ljava/lang/String;

    .line 479
    const-string v0, "mCardMapType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardMapType:[I

    .line 480
    const-string v0, "mCardTypeIndex"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardTypeIndex:I

    .line 481
    const-string v0, "mCardType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    .line 482
    const-string v0, "mCardMonth"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardMonth:I

    .line 483
    const-string v0, "mCardYear"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TicketSelectPage;->mCardYear:I

    .line 485
    const-string v0, "mPerformance"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Performance;

    iput-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;

    .line 487
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 343
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 345
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TicketSelectPage.onResume() mCategories:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    .line 349
    iget v0, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    .line 350
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "TicketSelectPage.onCreate() mCategories:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStartLoadPricingDialogHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 352
    invoke-direct {p0}, Lnet/flixster/android/TicketSelectPage;->ScheduleLoadPerformanceTask()V

    .line 361
    :cond_0
    :goto_0
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mSelectToReservationFieldHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 362
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mStateLayout:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 364
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mScrollAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 368
    return-void

    .line 354
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->mTicketBarLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 355
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->addTicketBars:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 356
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updatePrice:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 357
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->updateCardStatus:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 358
    iget-object v0, p0, Lnet/flixster/android/TicketSelectPage;->setupCardSpinner:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 439
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 441
    const-string v0, "FlxMain"

    const-string v1, "TicketSelectPage.onSaveInstanceState(.) "

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 442
    const-string v0, "mState"

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mState:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 443
    const-string v0, "mTicketCount"

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCount:F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 444
    const-string v0, "mTicketCounts"

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mTicketCounts:[F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 445
    const-string v0, "mCategories"

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCategories:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 446
    const-string v0, "mCategoryLabels"

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mCategoryLabels:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 447
    const-string v0, "mCategoryPrices"

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mCategoryPrices:[F

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloatArray(Ljava/lang/String;[F)V

    .line 449
    const-string v0, "mMethodIds"

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mMethodIds:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 450
    const-string v0, "mMethodNames"

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mMethodNames:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 452
    const-string v0, "mCardMapType"

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardMapType:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 453
    const-string v0, "mCardTypeIndex"

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardTypeIndex:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 454
    const-string v0, "mCardType"

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 455
    const-string v0, "mCardMonth"

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardMonth:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 456
    const-string v0, "mCardYear"

    iget v1, p0, Lnet/flixster/android/TicketSelectPage;->mCardYear:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 458
    const-string v0, "mPerformance"

    iget-object v1, p0, Lnet/flixster/android/TicketSelectPage;->mPerformance:Lnet/flixster/android/model/Performance;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 460
    return-void
.end method
