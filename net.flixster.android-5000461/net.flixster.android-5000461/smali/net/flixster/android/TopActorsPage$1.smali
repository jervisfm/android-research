.class Lnet/flixster/android/TopActorsPage$1;
.super Ljava/lang/Object;
.source "TopActorsPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TopActorsPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TopActorsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TopActorsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TopActorsPage$1;->this$0:Lnet/flixster/android/TopActorsPage;

    .line 171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .parameter "v"

    .prologue
    .line 174
    new-instance v1, Landroid/content/Intent;

    const-string v2, "TOP_ACTOR"

    const/4 v3, 0x0

    iget-object v4, p0, Lnet/flixster/android/TopActorsPage$1;->this$0:Lnet/flixster/android/TopActorsPage;

    const-class v5, Lnet/flixster/android/ActorPage;

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 176
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 178
    .local v0, actor:Lnet/flixster/android/model/Actor;
    const-string v2, "ACTOR_ID"

    iget-wide v3, v0, Lnet/flixster/android/model/Actor;->id:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 179
    const-string v2, "ACTOR_NAME"

    iget-object v3, v0, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 180
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage$1;->this$0:Lnet/flixster/android/TopActorsPage;

    invoke-virtual {v2, v1}, Lnet/flixster/android/TopActorsPage;->startActivity(Landroid/content/Intent;)V

    .line 182
    .end local v0           #actor:Lnet/flixster/android/model/Actor;
    :cond_0
    return-void
.end method
