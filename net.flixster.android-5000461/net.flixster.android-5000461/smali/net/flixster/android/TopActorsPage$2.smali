.class Lnet/flixster/android/TopActorsPage$2;
.super Ljava/lang/Object;
.source "TopActorsPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TopActorsPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TopActorsPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TopActorsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TopActorsPage$2;->this$0:Lnet/flixster/android/TopActorsPage;

    .line 186
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .parameter "view"

    .prologue
    .line 188
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage$2;->this$0:Lnet/flixster/android/TopActorsPage;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    #setter for: Lnet/flixster/android/TopActorsPage;->mNavSelect:I
    invoke-static {v0, v1}, Lnet/flixster/android/TopActorsPage;->access$0(Lnet/flixster/android/TopActorsPage;I)V

    .line 189
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage$2;->this$0:Lnet/flixster/android/TopActorsPage;

    #calls: Lnet/flixster/android/TopActorsPage;->trackHelper()V
    invoke-static {v0}, Lnet/flixster/android/TopActorsPage;->access$1(Lnet/flixster/android/TopActorsPage;)V

    .line 190
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage$2;->this$0:Lnet/flixster/android/TopActorsPage;

    iget-object v0, v0, Lnet/flixster/android/TopActorsPage;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$2;->this$0:Lnet/flixster/android/TopActorsPage;

    invoke-virtual {v1}, Lnet/flixster/android/TopActorsPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 191
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage$2;->this$0:Lnet/flixster/android/TopActorsPage;

    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$2;->this$0:Lnet/flixster/android/TopActorsPage;

    #getter for: Lnet/flixster/android/TopActorsPage;->mNavSelect:I
    invoke-static {v1}, Lnet/flixster/android/TopActorsPage;->access$2(Lnet/flixster/android/TopActorsPage;)I

    move-result v1

    const-wide/16 v2, 0x64

    #calls: Lnet/flixster/android/TopActorsPage;->ScheduleLoadItemsTask(IJ)V
    invoke-static {v0, v1, v2, v3}, Lnet/flixster/android/TopActorsPage;->access$3(Lnet/flixster/android/TopActorsPage;IJ)V

    .line 192
    return-void
.end method
