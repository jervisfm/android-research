.class Lnet/flixster/android/TopActorsPage$3;
.super Ljava/util/TimerTask;
.source "TopActorsPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TopActorsPage;->ScheduleLoadItemsTask(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TopActorsPage;

.field private final synthetic val$navSelection:I


# direct methods
.method constructor <init>(Lnet/flixster/android/TopActorsPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iput p2, p0, Lnet/flixster/android/TopActorsPage$3;->val$navSelection:I

    .line 94
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 98
    :try_start_0
    iget v1, p0, Lnet/flixster/android/TopActorsPage$3;->val$navSelection:I

    packed-switch v1, :pswitch_data_0

    .line 113
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget-object v1, v1, Lnet/flixster/android/TopActorsPage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget-object v1, v1, Lnet/flixster/android/TopActorsPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 129
    :goto_1
    return-void

    .line 100
    :pswitch_0
    :try_start_1
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    #getter for: Lnet/flixster/android/TopActorsPage;->mTopThisWeek:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/TopActorsPage;->access$4(Lnet/flixster/android/TopActorsPage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 101
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    const-string v2, "popular"

    const/16 v3, 0x19

    invoke-static {v2, v3}, Lnet/flixster/android/data/ActorDao;->getTopActors(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v2

    #setter for: Lnet/flixster/android/TopActorsPage;->mTopThisWeek:Ljava/util/ArrayList;
    invoke-static {v1, v2}, Lnet/flixster/android/TopActorsPage;->access$5(Lnet/flixster/android/TopActorsPage;Ljava/util/ArrayList;)V

    .line 103
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    #calls: Lnet/flixster/android/TopActorsPage;->setTopThisWeekLviList()V
    invoke-static {v1}, Lnet/flixster/android/TopActorsPage;->access$6(Lnet/flixster/android/TopActorsPage;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 114
    :catch_0
    move-exception v0

    .line 117
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "TopActorsPage.ScheduleLoadItemsTask.run() mRetryCount:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget v3, v3, Lnet/flixster/android/TopActorsPage;->mRetryCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v0}, Lnet/flixster/android/data/DaoException;->printStackTrace()V

    .line 119
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget v2, v1, Lnet/flixster/android/TopActorsPage;->mRetryCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lnet/flixster/android/TopActorsPage;->mRetryCount:I

    .line 120
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget v1, v1, Lnet/flixster/android/TopActorsPage;->mRetryCount:I

    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    .line 121
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget v2, p0, Lnet/flixster/android/TopActorsPage$3;->val$navSelection:I

    const-wide/16 v3, 0x3e8

    #calls: Lnet/flixster/android/TopActorsPage;->ScheduleLoadItemsTask(IJ)V
    invoke-static {v1, v2, v3, v4}, Lnet/flixster/android/TopActorsPage;->access$3(Lnet/flixster/android/TopActorsPage;IJ)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 127
    :goto_2
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget-object v1, v1, Lnet/flixster/android/TopActorsPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 106
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_1
    :try_start_3
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    #getter for: Lnet/flixster/android/TopActorsPage;->mRandom:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/TopActorsPage;->access$7(Lnet/flixster/android/TopActorsPage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 107
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    const-string v2, "random"

    const/16 v3, 0x19

    invoke-static {v2, v3}, Lnet/flixster/android/data/ActorDao;->getTopActors(Ljava/lang/String;I)Ljava/util/ArrayList;

    move-result-object v2

    #setter for: Lnet/flixster/android/TopActorsPage;->mRandom:Ljava/util/ArrayList;
    invoke-static {v1, v2}, Lnet/flixster/android/TopActorsPage;->access$8(Lnet/flixster/android/TopActorsPage;Ljava/util/ArrayList;)V

    .line 109
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    #calls: Lnet/flixster/android/TopActorsPage;->setRandomLviList()V
    invoke-static {v1}, Lnet/flixster/android/TopActorsPage;->access$9(Lnet/flixster/android/TopActorsPage;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    .line 126
    :catchall_0
    move-exception v1

    .line 127
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget-object v2, v2, Lnet/flixster/android/TopActorsPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v2, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 128
    throw v1

    .line 123
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    :cond_2
    :try_start_4
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    const/4 v2, 0x0

    iput v2, v1, Lnet/flixster/android/TopActorsPage;->mRetryCount:I

    .line 124
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage$3;->this$0:Lnet/flixster/android/TopActorsPage;

    iget-object v1, v1, Lnet/flixster/android/TopActorsPage;->mShowDialogHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 98
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
