.class public Lnet/flixster/android/TopActorsPage;
.super Lnet/flixster/android/LviActivity;
.source "TopActorsPage.java"


# static fields
.field private static final LIMIT_ACTORS:I = 0x19


# instance fields
.field private mActorListener:Landroid/view/View$OnClickListener;

.field private mNavListener:Landroid/view/View$OnClickListener;

.field private mNavSelect:I

.field private mRandom:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private mTopThisWeek:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private navBar:Lcom/flixster/android/view/SubNavBar;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Lnet/flixster/android/LviActivity;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/TopActorsPage;->mTopThisWeek:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/TopActorsPage;->mRandom:Ljava/util/ArrayList;

    .line 171
    new-instance v0, Lnet/flixster/android/TopActorsPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/TopActorsPage$1;-><init>(Lnet/flixster/android/TopActorsPage;)V

    iput-object v0, p0, Lnet/flixster/android/TopActorsPage;->mActorListener:Landroid/view/View$OnClickListener;

    .line 186
    new-instance v0, Lnet/flixster/android/TopActorsPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/TopActorsPage$2;-><init>(Lnet/flixster/android/TopActorsPage;)V

    iput-object v0, p0, Lnet/flixster/android/TopActorsPage;->mNavListener:Landroid/view/View$OnClickListener;

    .line 23
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(IJ)V
    .locals 3
    .parameter "navSelection"
    .parameter "delay"

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->throbberHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 94
    new-instance v0, Lnet/flixster/android/TopActorsPage$3;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/TopActorsPage$3;-><init>(Lnet/flixster/android/TopActorsPage;I)V

    .line 132
    .local v0, loadMoviesTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 133
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v1, v0, p2, p3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    :cond_0
    monitor-exit p0

    return-void

    .line 92
    .end local v0           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method static synthetic access$0(Lnet/flixster/android/TopActorsPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 28
    iput p1, p0, Lnet/flixster/android/TopActorsPage;->mNavSelect:I

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/TopActorsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 80
    invoke-direct {p0}, Lnet/flixster/android/TopActorsPage;->trackHelper()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/TopActorsPage;)I
    .locals 1
    .parameter

    .prologue
    .line 28
    iget v0, p0, Lnet/flixster/android/TopActorsPage;->mNavSelect:I

    return v0
.end method

.method static synthetic access$3(Lnet/flixster/android/TopActorsPage;IJ)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 91
    invoke-direct {p0, p1, p2, p3}, Lnet/flixster/android/TopActorsPage;->ScheduleLoadItemsTask(IJ)V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/TopActorsPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage;->mTopThisWeek:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/TopActorsPage;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    iput-object p1, p0, Lnet/flixster/android/TopActorsPage;->mTopThisWeek:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$6(Lnet/flixster/android/TopActorsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 137
    invoke-direct {p0}, Lnet/flixster/android/TopActorsPage;->setTopThisWeekLviList()V

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/TopActorsPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage;->mRandom:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/TopActorsPage;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 27
    iput-object p1, p0, Lnet/flixster/android/TopActorsPage;->mRandom:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/TopActorsPage;)V
    .locals 0
    .parameter

    .prologue
    .line 154
    invoke-direct {p0}, Lnet/flixster/android/TopActorsPage;->setRandomLviList()V

    return-void
.end method

.method private setRandomLviList()V
    .locals 4

    .prologue
    .line 156
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 157
    invoke-virtual {p0}, Lnet/flixster/android/TopActorsPage;->destroyExistingLviAd()V

    .line 158
    new-instance v2, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v2, p0, Lnet/flixster/android/TopActorsPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 159
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v3, "TopActors"

    iput-object v3, v2, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 160
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v3, p0, Lnet/flixster/android/TopActorsPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->mRandom:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 169
    return-void

    .line 162
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 163
    .local v0, actor:Lnet/flixster/android/model/Actor;
    new-instance v1, Lnet/flixster/android/lvi/LviActor;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviActor;-><init>()V

    .line 164
    .local v1, lviActor:Lnet/flixster/android/lvi/LviActor;
    iput-object v0, v1, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    .line 165
    iget-object v3, p0, Lnet/flixster/android/TopActorsPage;->mActorListener:Landroid/view/View$OnClickListener;

    iput-object v3, v1, Lnet/flixster/android/lvi/LviActor;->mActorClick:Landroid/view/View$OnClickListener;

    .line 166
    iget-object v3, p0, Lnet/flixster/android/TopActorsPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private setTopThisWeekLviList()V
    .locals 4

    .prologue
    .line 139
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 140
    invoke-virtual {p0}, Lnet/flixster/android/TopActorsPage;->destroyExistingLviAd()V

    .line 141
    new-instance v2, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v2, p0, Lnet/flixster/android/TopActorsPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 142
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v3, "TopActors"

    iput-object v3, v2, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 143
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v3, p0, Lnet/flixster/android/TopActorsPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->mTopThisWeek:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    .line 152
    return-void

    .line 145
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Actor;

    .line 146
    .local v0, actor:Lnet/flixster/android/model/Actor;
    new-instance v1, Lnet/flixster/android/lvi/LviActor;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviActor;-><init>()V

    .line 147
    .local v1, lviActor:Lnet/flixster/android/lvi/LviActor;
    iput-object v0, v1, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    .line 148
    iget-object v3, p0, Lnet/flixster/android/TopActorsPage;->mActorListener:Landroid/view/View$OnClickListener;

    iput-object v3, v1, Lnet/flixster/android/lvi/LviActor;->mActorClick:Landroid/view/View$OnClickListener;

    .line 149
    iget-object v3, p0, Lnet/flixster/android/TopActorsPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private trackHelper()V
    .locals 3

    .prologue
    .line 81
    iget v0, p0, Lnet/flixster/android/TopActorsPage;->mNavSelect:I

    packed-switch v0, :pswitch_data_0

    .line 89
    :goto_0
    return-void

    .line 83
    :pswitch_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/actor/top"

    const-string v2, "Top Actors Page for TopThisWeek"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :pswitch_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/actor/random"

    const-string v2, "Top Actors Page for Random"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedState"

    .prologue
    const v5, 0x7f070258

    .line 33
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onCreate(Landroid/os/Bundle;)V

    .line 34
    invoke-virtual {p0}, Lnet/flixster/android/TopActorsPage;->createActionBar()V

    .line 35
    const v1, 0x7f0c001b

    invoke-virtual {p0, v1}, Lnet/flixster/android/TopActorsPage;->setActionBarTitle(I)V

    .line 37
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/TopActorsPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 38
    const v1, 0x7f0700f3

    invoke-virtual {p0, v1}, Lnet/flixster/android/TopActorsPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 40
    .local v0, mNavbarHolder:Landroid/widget/LinearLayout;
    new-instance v1, Lcom/flixster/android/view/SubNavBar;

    invoke-direct {v1, p0}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lnet/flixster/android/TopActorsPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    .line 41
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget-object v2, p0, Lnet/flixster/android/TopActorsPage;->mNavListener:Landroid/view/View$OnClickListener;

    const v3, 0x7f0c001d

    const v4, 0x7f0c0032

    invoke-virtual {v1, v2, v3, v4}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;II)V

    .line 42
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    invoke-virtual {v1, v5}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 43
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 45
    iput v5, p0, Lnet/flixster/android/TopActorsPage;->mNavSelect:I

    .line 47
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v2, "TopActorsStickyTop"

    invoke-virtual {v1, v2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 48
    iget-object v1, p0, Lnet/flixster/android/TopActorsPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v2, "TopActorsStickyBottom"

    invoke-virtual {v1, v2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 61
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onPause()V

    .line 62
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage;->mTopThisWeek:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 63
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage;->mRandom:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 64
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedInstanceState"

    .prologue
    .line 75
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 76
    const-string v0, "FlxMain"

    const-string v1, "TopActorsPage.onRestoreInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    const-string v0, "LISTSTATE_NAV"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TopActorsPage;->mNavSelect:I

    .line 78
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 53
    invoke-super {p0}, Lnet/flixster/android/LviActivity;->onResume()V

    .line 54
    invoke-direct {p0}, Lnet/flixster/android/TopActorsPage;->trackHelper()V

    .line 55
    iget-object v0, p0, Lnet/flixster/android/TopActorsPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget v1, p0, Lnet/flixster/android/TopActorsPage;->mNavSelect:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 56
    iget v0, p0, Lnet/flixster/android/TopActorsPage;->mNavSelect:I

    const-wide/16 v1, 0x64

    invoke-direct {p0, v0, v1, v2}, Lnet/flixster/android/TopActorsPage;->ScheduleLoadItemsTask(IJ)V

    .line 57
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 68
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 69
    const-string v0, "FlxMain"

    const-string v1, "TopActorsPage.onSaveInstanceState()"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v0, "LISTSTATE_NAV"

    iget v1, p0, Lnet/flixster/android/TopActorsPage;->mNavSelect:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 71
    return-void
.end method
