.class Lnet/flixster/android/TopPhotosPage$1;
.super Landroid/os/Handler;
.source "TopPhotosPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/TopPhotosPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TopPhotosPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TopPhotosPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TopPhotosPage$1;->this$0:Lnet/flixster/android/TopPhotosPage;

    .line 140
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 143
    const-string v0, "FlxMain"

    const-string v1, "PhotoGalleryPage.updateHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage$1;->this$0:Lnet/flixster/android/TopPhotosPage;

    #getter for: Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;
    invoke-static {v0}, Lnet/flixster/android/TopPhotosPage;->access$0(Lnet/flixster/android/TopPhotosPage;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage$1;->this$0:Lnet/flixster/android/TopPhotosPage;

    #calls: Lnet/flixster/android/TopPhotosPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/TopPhotosPage;->access$1(Lnet/flixster/android/TopPhotosPage;)V

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage$1;->this$0:Lnet/flixster/android/TopPhotosPage;

    invoke-virtual {v0}, Lnet/flixster/android/TopPhotosPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage$1;->this$0:Lnet/flixster/android/TopPhotosPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/TopPhotosPage;->showDialog(I)V

    goto :goto_0
.end method
