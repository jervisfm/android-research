.class Lnet/flixster/android/TopPhotosPage$6;
.super Ljava/util/TimerTask;
.source "TopPhotosPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/TopPhotosPage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/TopPhotosPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/TopPhotosPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/TopPhotosPage$6;->this$0:Lnet/flixster/android/TopPhotosPage;

    .line 122
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 124
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage$6;->this$0:Lnet/flixster/android/TopPhotosPage;

    #getter for: Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/TopPhotosPage;->access$0(Lnet/flixster/android/TopPhotosPage;)Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_0

    .line 126
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage$6;->this$0:Lnet/flixster/android/TopPhotosPage;

    iget-object v2, p0, Lnet/flixster/android/TopPhotosPage$6;->this$0:Lnet/flixster/android/TopPhotosPage;

    #getter for: Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;
    invoke-static {v2}, Lnet/flixster/android/TopPhotosPage;->access$3(Lnet/flixster/android/TopPhotosPage;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/data/PhotoDao;->getTopPhotos(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    #setter for: Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;
    invoke-static {v1, v2}, Lnet/flixster/android/TopPhotosPage;->access$6(Lnet/flixster/android/TopPhotosPage;Ljava/util/ArrayList;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 132
    :cond_0
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage$6;->this$0:Lnet/flixster/android/TopPhotosPage;

    #getter for: Lnet/flixster/android/TopPhotosPage;->updateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/TopPhotosPage;->access$7(Lnet/flixster/android/TopPhotosPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 133
    return-void

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "PhotoGalleryPage.scheduleUpdatePageTask:failed to get photos"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 129
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage$6;->this$0:Lnet/flixster/android/TopPhotosPage;

    const/4 v2, 0x0

    #setter for: Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;
    invoke-static {v1, v2}, Lnet/flixster/android/TopPhotosPage;->access$6(Lnet/flixster/android/TopPhotosPage;Ljava/util/ArrayList;)V

    goto :goto_0
.end method
