.class public Lnet/flixster/android/TopPhotosPage;
.super Lnet/flixster/android/FlixsterActivity;
.source "TopPhotosPage.java"


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field public static final KEY_PHOTO_FILTER:Ljava/lang/String; = "KEY_PHOTO_FILTER"

.field private static final MAX_PHOTOS:I = 0x64


# instance fields
.field private filter:Ljava/lang/String;

.field private headerClickListener:Landroid/view/View$OnClickListener;

.field private mDefaultAd:Lnet/flixster/android/ads/AdView;

.field private mNavSelect:I

.field private navBar:Lcom/flixster/android/view/SubNavBar;

.field private photoClickListener:Landroid/view/View$OnClickListener;

.field private photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private photos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private topPhotos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final updateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 140
    new-instance v0, Lnet/flixster/android/TopPhotosPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/TopPhotosPage$1;-><init>(Lnet/flixster/android/TopPhotosPage;)V

    iput-object v0, p0, Lnet/flixster/android/TopPhotosPage;->updateHandler:Landroid/os/Handler;

    .line 185
    new-instance v0, Lnet/flixster/android/TopPhotosPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/TopPhotosPage$2;-><init>(Lnet/flixster/android/TopPhotosPage;)V

    iput-object v0, p0, Lnet/flixster/android/TopPhotosPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 191
    new-instance v0, Lnet/flixster/android/TopPhotosPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/TopPhotosPage$3;-><init>(Lnet/flixster/android/TopPhotosPage;)V

    iput-object v0, p0, Lnet/flixster/android/TopPhotosPage;->photoClickListener:Landroid/view/View$OnClickListener;

    .line 210
    new-instance v0, Lnet/flixster/android/TopPhotosPage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/TopPhotosPage$4;-><init>(Lnet/flixster/android/TopPhotosPage;)V

    iput-object v0, p0, Lnet/flixster/android/TopPhotosPage;->headerClickListener:Landroid/view/View$OnClickListener;

    .line 28
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/TopPhotosPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/TopPhotosPage;)V
    .locals 0
    .parameter

    .prologue
    .line 154
    invoke-direct {p0}, Lnet/flixster/android/TopPhotosPage;->updatePage()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/TopPhotosPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage;->topPhotos:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/TopPhotosPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/TopPhotosPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36
    iput p1, p0, Lnet/flixster/android/TopPhotosPage;->mNavSelect:I

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/TopPhotosPage;)V
    .locals 0
    .parameter

    .prologue
    .line 121
    invoke-direct {p0}, Lnet/flixster/android/TopPhotosPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$6(Lnet/flixster/android/TopPhotosPage;Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 30
    iput-object p1, p0, Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/TopPhotosPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 140
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 122
    new-instance v0, Lnet/flixster/android/TopPhotosPage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/TopPhotosPage$6;-><init>(Lnet/flixster/android/TopPhotosPage;)V

    .line 135
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 136
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage;->mPageTimer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 138
    :cond_0
    return-void
.end method

.method private updateFilter()V
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lnet/flixster/android/TopPhotosPage;->mNavSelect:I

    packed-switch v0, :pswitch_data_0

    .line 183
    :goto_0
    return-void

    .line 174
    :pswitch_0
    const-string v0, "top-actresses"

    iput-object v0, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    goto :goto_0

    .line 177
    :pswitch_1
    const-string v0, "top-actors"

    iput-object v0, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    goto :goto_0

    .line 180
    :pswitch_2
    const-string v0, "top-movies"

    iput-object v0, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    goto :goto_0

    .line 172
    nop

    :pswitch_data_0
    .packed-switch 0x7f070258
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private updatePage()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 155
    iget-object v3, p0, Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 156
    const v3, 0x7f0702cf

    invoke-virtual {p0, v3}, Lnet/flixster/android/TopPhotosPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    .line 157
    .local v0, galleryView:Landroid/widget/GridView;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lnet/flixster/android/TopPhotosPage;->topPhotos:Ljava/util/ArrayList;

    .line 158
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    const/16 v3, 0x64

    if-lt v1, v3, :cond_2

    .line 162
    :cond_0
    new-instance v3, Lnet/flixster/android/PhotosListAdapter;

    iget-object v4, p0, Lnet/flixster/android/TopPhotosPage;->topPhotos:Ljava/util/ArrayList;

    iget-object v5, p0, Lnet/flixster/android/TopPhotosPage;->photoClickListener:Landroid/view/View$OnClickListener;

    invoke-direct {v3, p0, v4, v5}, Lnet/flixster/android/PhotosListAdapter;-><init>(Landroid/content/Context;Ljava/util/List;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 163
    iget-object v3, p0, Lnet/flixster/android/TopPhotosPage;->photoItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 164
    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setClickable(Z)V

    .line 165
    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setFocusable(Z)V

    .line 166
    invoke-virtual {v0, v6}, Landroid/widget/GridView;->setFocusableInTouchMode(Z)V

    .line 167
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "/photos/"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Top Photos Page for filter:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .end local v0           #galleryView:Landroid/widget/GridView;
    .end local v1           #i:I
    :cond_1
    return-void

    .line 159
    .restart local v0       #galleryView:Landroid/widget/GridView;
    .restart local v1       #i:I
    :cond_2
    iget-object v3, p0, Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .line 160
    .local v2, photo:Ljava/lang/Object;
    iget-object v3, p0, Lnet/flixster/android/TopPhotosPage;->topPhotos:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 158
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .parameter "savedState"

    .prologue
    .line 42
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    const-string v1, "FlxMain"

    const-string v2, "PhotoGalleryPage.onCreate"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const v1, 0x7f03008b

    invoke-virtual {p0, v1}, Lnet/flixster/android/TopPhotosPage;->setContentView(I)V

    .line 45
    const v1, 0x7f0702ce

    invoke-virtual {p0, v1}, Lnet/flixster/android/TopPhotosPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 46
    invoke-virtual {p0}, Lnet/flixster/android/TopPhotosPage;->createActionBar()V

    .line 47
    const v1, 0x7f0c001c

    invoke-virtual {p0, v1}, Lnet/flixster/android/TopPhotosPage;->setActionBarTitle(I)V

    .line 49
    invoke-virtual {p0}, Lnet/flixster/android/TopPhotosPage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 50
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_2

    .line 51
    const-string v1, "KEY_PHOTO_FILTER"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    .line 52
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-gtz v1, :cond_1

    .line 53
    :cond_0
    const-string v1, "random"

    iput-object v1, p0, Lnet/flixster/android/TopPhotosPage;->filter:Ljava/lang/String;

    .line 56
    :cond_1
    new-instance v1, Lcom/flixster/android/view/SubNavBar;

    invoke-direct {v1, p0}, Lcom/flixster/android/view/SubNavBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lnet/flixster/android/TopPhotosPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    .line 57
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget-object v2, p0, Lnet/flixster/android/TopPhotosPage;->headerClickListener:Landroid/view/View$OnClickListener;

    const v3, 0x7f0c002f

    const v4, 0x7f0c0030

    .line 58
    const v5, 0x7f0c0031

    .line 57
    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/flixster/android/view/SubNavBar;->load(Landroid/view/View$OnClickListener;III)V

    .line 59
    iget-object v1, p0, Lnet/flixster/android/TopPhotosPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const v2, 0x7f070258

    invoke-virtual {v1, v2}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 60
    const v1, 0x7f0702cd

    invoke-virtual {p0, v1}, Lnet/flixster/android/TopPhotosPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iget-object v2, p0, Lnet/flixster/android/TopPhotosPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 66
    :cond_2
    const v1, 0x7f07002a

    invoke-virtual {p0, v1}, Lnet/flixster/android/TopPhotosPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/ads/AdView;

    iput-object v1, p0, Lnet/flixster/android/TopPhotosPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    .line 68
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "dialogId"

    .prologue
    .line 102
    packed-switch p1, :pswitch_data_0

    .line 117
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 104
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 105
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 106
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 107
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 108
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/TopPhotosPage$5;

    invoke-direct {v2, p0}, Lnet/flixster/android/TopPhotosPage$5;-><init>(Lnet/flixster/android/TopPhotosPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 113
    invoke-virtual {p0}, Lnet/flixster/android/TopPhotosPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 114
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 97
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 98
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 90
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 91
    const-string v0, "LISTSTATE_NAV"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/TopPhotosPage;->mNavSelect:I

    .line 92
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 73
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage;->navBar:Lcom/flixster/android/view/SubNavBar;

    iget v1, p0, Lnet/flixster/android/TopPhotosPage;->mNavSelect:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubNavBar;->setSelectedButton(I)V

    .line 74
    invoke-direct {p0}, Lnet/flixster/android/TopPhotosPage;->updateFilter()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/TopPhotosPage;->photos:Ljava/util/ArrayList;

    .line 76
    invoke-direct {p0}, Lnet/flixster/android/TopPhotosPage;->scheduleUpdatePageTask()V

    .line 79
    iget-object v0, p0, Lnet/flixster/android/TopPhotosPage;->mDefaultAd:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 80
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .parameter "outState"

    .prologue
    .line 84
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 85
    const-string v0, "LISTSTATE_NAV"

    iget v1, p0, Lnet/flixster/android/TopPhotosPage;->mNavSelect:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    return-void
.end method
