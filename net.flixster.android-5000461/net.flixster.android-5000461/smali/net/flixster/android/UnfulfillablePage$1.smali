.class Lnet/flixster/android/UnfulfillablePage$1;
.super Landroid/os/Handler;
.source "UnfulfillablePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/UnfulfillablePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UnfulfillablePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/UnfulfillablePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    .line 57
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 59
    const/4 v0, 0x0

    .line 60
    .local v0, insertionIndex:I
    iget-object v3, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->unfulfillableRights:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/UnfulfillablePage;->access$0(Lnet/flixster/android/UnfulfillablePage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 62
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v3

    invoke-virtual {v3}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v2

    .line 63
    .local v2, rights:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 70
    iget-object v3, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->learnMoreView:Landroid/widget/TextView;
    invoke-static {v3}, Lnet/flixster/android/UnfulfillablePage;->access$1(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    invoke-virtual {v4}, Lnet/flixster/android/UnfulfillablePage;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0077

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-object v3, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->learnMoreView:Landroid/widget/TextView;
    invoke-static {v3}, Lnet/flixster/android/UnfulfillablePage;->access$1(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/TextView;

    move-result-object v3

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 73
    iget-object v3, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->gridView:Landroid/widget/GridView;
    invoke-static {v3}, Lnet/flixster/android/UnfulfillablePage;->access$2(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/GridView;

    move-result-object v3

    new-instance v4, Lnet/flixster/android/LockerRightGridViewAdapter;

    iget-object v5, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    iget-object v6, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->unfulfillableRights:Ljava/util/List;
    invoke-static {v6}, Lnet/flixster/android/UnfulfillablePage;->access$0(Lnet/flixster/android/UnfulfillablePage;)Ljava/util/List;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lnet/flixster/android/LockerRightGridViewAdapter;-><init>(Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 74
    iget-object v3, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->gridView:Landroid/widget/GridView;
    invoke-static {v3}, Lnet/flixster/android/UnfulfillablePage;->access$2(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/GridView;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setClickable(Z)V

    .line 75
    iget-object v3, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->gridView:Landroid/widget/GridView;
    invoke-static {v3}, Lnet/flixster/android/UnfulfillablePage;->access$2(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/GridView;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->unfulfillableMovieClickListener:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v4}, Lnet/flixster/android/UnfulfillablePage;->access$3(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 76
    return-void

    .line 63
    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/LockerRight;

    .line 64
    .local v1, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight;->isUnfulfillable()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    iget-object v4, p0, Lnet/flixster/android/UnfulfillablePage$1;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->unfulfillableRights:Ljava/util/List;
    invoke-static {v4}, Lnet/flixster/android/UnfulfillablePage;->access$0(Lnet/flixster/android/UnfulfillablePage;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
