.class Lnet/flixster/android/UnfulfillablePage$3;
.super Ljava/lang/Object;
.source "UnfulfillablePage.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/UnfulfillablePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/widget/AdapterView$OnItemClickListener;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UnfulfillablePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/UnfulfillablePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UnfulfillablePage$3;->this$0:Lnet/flixster/android/UnfulfillablePage;

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5
    .parameter
    .parameter "v"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v3, p0, Lnet/flixster/android/UnfulfillablePage$3;->this$0:Lnet/flixster/android/UnfulfillablePage;

    #getter for: Lnet/flixster/android/UnfulfillablePage;->unfulfillableRights:Ljava/util/List;
    invoke-static {v3}, Lnet/flixster/android/UnfulfillablePage;->access$0(Lnet/flixster/android/UnfulfillablePage;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v3}, Lnet/flixster/android/model/LockerRight;->getAsset()Lnet/flixster/android/model/Movie;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Unfulfillable;

    .line 90
    .local v2, unfulfillableMovie:Lnet/flixster/android/model/Unfulfillable;
    invoke-virtual {v2}, Lnet/flixster/android/model/Unfulfillable;->getAlternateUrl()Ljava/lang/String;

    move-result-object v0

    .line 91
    .local v0, alternateProviderUrl:Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 92
    .local v1, i:Landroid/content/Intent;
    iget-object v3, p0, Lnet/flixster/android/UnfulfillablePage$3;->this$0:Lnet/flixster/android/UnfulfillablePage;

    invoke-virtual {v3, v1}, Lnet/flixster/android/UnfulfillablePage;->startActivity(Landroid/content/Intent;)V

    .line 93
    return-void
.end method
