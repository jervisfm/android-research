.class public Lnet/flixster/android/UnfulfillablePage;
.super Lcom/flixster/android/activity/common/DecoratedSherlockActivity;
.source "UnfulfillablePage.java"


# instance fields
.field private final errorHandler:Landroid/os/Handler;

.field private gridView:Landroid/widget/GridView;

.field private learnMoreView:Landroid/widget/TextView;

.field private final successHandler:Landroid/os/Handler;

.field private final unfulfillableMovieClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private unfulfillableRights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;-><init>()V

    .line 57
    new-instance v0, Lnet/flixster/android/UnfulfillablePage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/UnfulfillablePage$1;-><init>(Lnet/flixster/android/UnfulfillablePage;)V

    iput-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->successHandler:Landroid/os/Handler;

    .line 79
    new-instance v0, Lnet/flixster/android/UnfulfillablePage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/UnfulfillablePage$2;-><init>(Lnet/flixster/android/UnfulfillablePage;)V

    iput-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->errorHandler:Landroid/os/Handler;

    .line 87
    new-instance v0, Lnet/flixster/android/UnfulfillablePage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/UnfulfillablePage$3;-><init>(Lnet/flixster/android/UnfulfillablePage;)V

    iput-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->unfulfillableMovieClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 29
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/UnfulfillablePage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 32
    iget-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->unfulfillableRights:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/TextView;
    .locals 1
    .parameter

    .prologue
    .line 30
    iget-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->learnMoreView:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/GridView;
    .locals 1
    .parameter

    .prologue
    .line 31
    iget-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->gridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/UnfulfillablePage;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1
    .parameter

    .prologue
    .line 87
    iget-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->unfulfillableMovieClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .parameter "savedInstanceState"

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onCreate(Landroid/os/Bundle;)V

    .line 37
    const v0, 0x7f03008c

    invoke-virtual {p0, v0}, Lnet/flixster/android/UnfulfillablePage;->setContentView(I)V

    .line 38
    invoke-virtual {p0}, Lnet/flixster/android/UnfulfillablePage;->createActionBar()V

    .line 39
    const v0, 0x7f0c0075

    invoke-virtual {p0, v0}, Lnet/flixster/android/UnfulfillablePage;->setActionBarTitle(I)V

    .line 41
    const v0, 0x7f0702d0

    invoke-virtual {p0, v0}, Lnet/flixster/android/UnfulfillablePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->learnMoreView:Landroid/widget/TextView;

    .line 42
    const v0, 0x7f07010f

    invoke-virtual {p0, v0}, Lnet/flixster/android/UnfulfillablePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->gridView:Landroid/widget/GridView;

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->unfulfillableRights:Ljava/util/List;

    .line 44
    return-void
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 98
    const/4 v0, 0x1

    return v0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 48
    invoke-super {p0}, Lcom/flixster/android/activity/common/DecoratedSherlockActivity;->onResume()V

    .line 50
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->hasUserSession()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lnet/flixster/android/UnfulfillablePage;->successHandler:Landroid/os/Handler;

    iget-object v1, p0, Lnet/flixster/android/UnfulfillablePage;->errorHandler:Landroid/os/Handler;

    invoke-static {v0, v1}, Lnet/flixster/android/data/ProfileDao;->getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V

    .line 54
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/mymovies/unfulfillables"

    const-string v2, "My Movies - Unfulfillables"

    invoke-interface {v0, v1, v2}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method
