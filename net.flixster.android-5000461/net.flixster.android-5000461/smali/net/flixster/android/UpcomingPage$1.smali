.class Lnet/flixster/android/UpcomingPage$1;
.super Ljava/util/TimerTask;
.source "UpcomingPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/UpcomingPage;->ScheduleLoadItemsTask(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UpcomingPage;

.field private final synthetic val$currResumeCtr:I


# direct methods
.method constructor <init>(Lnet/flixster/android/UpcomingPage;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    iput p2, p0, Lnet/flixster/android/UpcomingPage$1;->val$currResumeCtr:I

    .line 56
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 59
    const-string v1, "FlxMain"

    const-string v3, "UpcomingPage.ScheduleLoadItemsTask.run"

    invoke-static {v1, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    #getter for: Lnet/flixster/android/UpcomingPage;->mUpcoming:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/UpcomingPage;->access$0(Lnet/flixster/android/UpcomingPage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 62
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    #getter for: Lnet/flixster/android/UpcomingPage;->mUpcomingFeatured:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/UpcomingPage;->access$1(Lnet/flixster/android/UpcomingPage;)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    #getter for: Lnet/flixster/android/UpcomingPage;->mUpcoming:Ljava/util/ArrayList;
    invoke-static {v1}, Lnet/flixster/android/UpcomingPage;->access$0(Lnet/flixster/android/UpcomingPage;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    iget v1, v1, Lnet/flixster/android/UpcomingPage;->mRetryCount:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v3, v4, v1}, Lnet/flixster/android/data/MovieDao;->fetchUpcoming(Ljava/util/List;Ljava/util/List;Z)V

    .line 65
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    iget v3, p0, Lnet/flixster/android/UpcomingPage$1;->val$currResumeCtr:I

    invoke-virtual {v1, v3}, Lnet/flixster/android/UpcomingPage;->shouldSkipBackgroundTask(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_2

    .line 77
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    iget-object v1, v1, Lnet/flixster/android/UpcomingPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 79
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 62
    goto :goto_0

    .line 69
    :cond_2
    :try_start_1
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    #calls: Lnet/flixster/android/UpcomingPage;->setUpcomingLviList()V
    invoke-static {v1}, Lnet/flixster/android/UpcomingPage;->access$2(Lnet/flixster/android/UpcomingPage;)V

    .line 71
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    invoke-virtual {v1}, Lnet/flixster/android/UpcomingPage;->trackPage()V

    .line 72
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    iget-object v1, v1, Lnet/flixster/android/UpcomingPage;->mUpdateHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_0

    .line 77
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    iget-object v1, v1, Lnet/flixster/android/UpcomingPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :try_start_2
    const-string v1, "FlxMain"

    const-string v3, "UpcomingPage.ScheduleLoadItemsTask.run DaoException"

    invoke-static {v1, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    invoke-virtual {v1, v0}, Lnet/flixster/android/UpcomingPage;->retryLogic(Lnet/flixster/android/data/DaoException;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 77
    iget-object v1, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    iget-object v1, v1, Lnet/flixster/android/UpcomingPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 76
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :catchall_0
    move-exception v1

    .line 77
    iget-object v3, p0, Lnet/flixster/android/UpcomingPage$1;->this$0:Lnet/flixster/android/UpcomingPage;

    iget-object v3, v3, Lnet/flixster/android/UpcomingPage;->throbberHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 78
    throw v1
.end method
