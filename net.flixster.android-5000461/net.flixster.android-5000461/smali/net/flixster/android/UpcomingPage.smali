.class public Lnet/flixster/android/UpcomingPage;
.super Lnet/flixster/android/LviActivityCopy;
.source "UpcomingPage.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final mUpcoming:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field private final mUpcomingFeatured:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lnet/flixster/android/LviActivityCopy;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/UpcomingPage;->mUpcoming:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/UpcomingPage;->mUpcomingFeatured:Ljava/util/ArrayList;

    .line 24
    return-void
.end method

.method private declared-synchronized ScheduleLoadItemsTask(J)V
    .locals 4
    .parameter "delay"

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lnet/flixster/android/UpcomingPage;->throbberHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 55
    invoke-static {}, Lcom/flixster/android/activity/decorator/TopLevelDecorator;->getResumeCtr()I

    move-result v0

    .line 56
    .local v0, currResumeCtr:I
    new-instance v1, Lnet/flixster/android/UpcomingPage$1;

    invoke-direct {v1, p0, v0}, Lnet/flixster/android/UpcomingPage$1;-><init>(Lnet/flixster/android/UpcomingPage;I)V

    .line 82
    .local v1, loadMoviesTask:Ljava/util/TimerTask;
    iget-object v2, p0, Lnet/flixster/android/UpcomingPage;->mPageTimer:Ljava/util/Timer;

    if-eqz v2, :cond_0

    .line 83
    iget-object v2, p0, Lnet/flixster/android/UpcomingPage;->mPageTimer:Ljava/util/Timer;

    invoke-virtual {v2, v1, p1, p2}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :cond_0
    monitor-exit p0

    return-void

    .line 53
    .end local v0           #currResumeCtr:I
    .end local v1           #loadMoviesTask:Ljava/util/TimerTask;
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method static synthetic access$0(Lnet/flixster/android/UpcomingPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 26
    iget-object v0, p0, Lnet/flixster/android/UpcomingPage;->mUpcoming:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/UpcomingPage;)Ljava/util/ArrayList;
    .locals 1
    .parameter

    .prologue
    .line 27
    iget-object v0, p0, Lnet/flixster/android/UpcomingPage;->mUpcomingFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/UpcomingPage;)V
    .locals 0
    .parameter

    .prologue
    .line 92
    invoke-direct {p0}, Lnet/flixster/android/UpcomingPage;->setUpcomingLviList()V

    return-void
.end method

.method private setUpcomingLviList()V
    .locals 11

    .prologue
    .line 93
    const-string v9, "FlxMain"

    const-string v10, "UpcomingPage.setUpcomingLviList"

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v9, p0, Lnet/flixster/android/UpcomingPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    .line 99
    invoke-virtual {p0}, Lnet/flixster/android/UpcomingPage;->destroyExistingLviAd()V

    .line 100
    new-instance v9, Lnet/flixster/android/lvi/LviAd;

    invoke-direct {v9}, Lnet/flixster/android/lvi/LviAd;-><init>()V

    iput-object v9, p0, Lnet/flixster/android/UpcomingPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    .line 101
    iget-object v9, p0, Lnet/flixster/android/UpcomingPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    const-string v10, "UpcomingTab"

    iput-object v10, v9, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    .line 102
    iget-object v9, p0, Lnet/flixster/android/UpcomingPage;->mDataHolder:Ljava/util/ArrayList;

    iget-object v10, p0, Lnet/flixster/android/UpcomingPage;->lviAd:Lnet/flixster/android/lvi/LviAd;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v9, p0, Lnet/flixster/android/UpcomingPage;->mUpcomingFeatured:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    .line 106
    .local v4, mUpcomingFeaturedCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 107
    new-instance v6, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 108
    .local v6, subHead:Lnet/flixster/android/lvi/LviSubHeader;
    invoke-virtual {p0}, Lnet/flixster/android/UpcomingPage;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c00be

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 109
    iget-object v9, p0, Lnet/flixster/android/UpcomingPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_1

    .line 120
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_0
    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 121
    .local v0, date:Ljava/util/Date;
    iget-object v9, p0, Lnet/flixster/android/UpcomingPage;->mUpcoming:Ljava/util/ArrayList;

    invoke-static {v9}, Lcom/flixster/android/utils/ListHelper;->clone(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    .line 122
    .local v3, mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-nez v10, :cond_2

    .line 137
    new-instance v1, Lnet/flixster/android/lvi/LviFooter;

    invoke-direct {v1}, Lnet/flixster/android/lvi/LviFooter;-><init>()V

    .line 138
    .local v1, footer:Lnet/flixster/android/lvi/LviFooter;
    iget-object v9, p0, Lnet/flixster/android/UpcomingPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    return-void

    .line 110
    .end local v0           #date:Ljava/util/Date;
    .end local v1           #footer:Lnet/flixster/android/lvi/LviFooter;
    .end local v3           #mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .restart local v6       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_1
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 111
    .local v5, movie:Lnet/flixster/android/model/Movie;
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 112
    .local v2, lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 113
    invoke-virtual {p0}, Lnet/flixster/android/UpcomingPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 114
    iget-object v10, p0, Lnet/flixster/android/UpcomingPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 122
    .end local v2           #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    .end local v5           #movie:Lnet/flixster/android/model/Movie;
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    .restart local v0       #date:Ljava/util/Date;
    .restart local v3       #mUpcomingCopy:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    :cond_2
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Movie;

    .line 123
    .restart local v5       #movie:Lnet/flixster/android/model/Movie;
    const-string v10, "theaterReleaseDate"

    invoke-virtual {v5, v10}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 124
    .local v8, theaterReleaseString:Ljava/lang/String;
    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v7

    .line 125
    .local v7, theaterRelease:Ljava/util/Date;
    if-eqz v7, :cond_3

    invoke-virtual {v0, v7}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 126
    new-instance v6, Lnet/flixster/android/lvi/LviSubHeader;

    invoke-direct {v6}, Lnet/flixster/android/lvi/LviSubHeader;-><init>()V

    .line 127
    .restart local v6       #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    iput-object v8, v6, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    .line 128
    iget-object v10, p0, Lnet/flixster/android/UpcomingPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    move-object v0, v7

    .line 131
    .end local v6           #subHead:Lnet/flixster/android/lvi/LviSubHeader;
    :cond_3
    new-instance v2, Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviMovie;-><init>()V

    .line 132
    .restart local v2       #lviMovie:Lnet/flixster/android/lvi/LviMovie;
    iput-object v5, v2, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 133
    invoke-virtual {p0}, Lnet/flixster/android/UpcomingPage;->getTrailerOnClickListener()Landroid/view/View$OnClickListener;

    move-result-object v10

    iput-object v10, v2, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    .line 134
    iget-object v10, p0, Lnet/flixster/android/UpcomingPage;->mDataHolder:Ljava/util/ArrayList;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method


# virtual methods
.method protected getAnalyticsAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    const-string v0, "Logo"

    return-object v0
.end method

.method protected getAnalyticsCategory()Ljava/lang/String;
    .locals 1

    .prologue
    .line 155
    const-string v0, "WidgetEntrance"

    return-object v0
.end method

.method protected getAnalyticsTag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    const-string v0, "/upcoming"

    return-object v0
.end method

.method protected getAnalyticsTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    const-string v0, "Upcoming Movies"

    return-object v0
.end method

.method protected getFeaturedMovies()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation

    .prologue
    .line 165
    iget-object v0, p0, Lnet/flixster/android/UpcomingPage;->mUpcomingFeatured:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedState"

    .prologue
    .line 31
    invoke-super {p0, p1}, Lnet/flixster/android/LviActivityCopy;->onCreate(Landroid/os/Bundle;)V

    .line 32
    iget-object v0, p0, Lnet/flixster/android/UpcomingPage;->mListView:Landroid/widget/ListView;

    invoke-virtual {p0}, Lnet/flixster/android/UpcomingPage;->getMovieItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 34
    iget-object v0, p0, Lnet/flixster/android/UpcomingPage;->mStickyTopAd:Lnet/flixster/android/ads/AdView;

    const-string v1, "UpcomingTabStickyTop"

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lnet/flixster/android/UpcomingPage;->mStickyBottomAd:Lnet/flixster/android/ads/AdView;

    const-string v1, "UpcomingTabStickyBottom"

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 37
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0}, Lnet/flixster/android/LviActivityCopy;->onPause()V

    .line 48
    iget-object v0, p0, Lnet/flixster/android/UpcomingPage;->mUpcoming:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 49
    iget-object v0, p0, Lnet/flixster/android/UpcomingPage;->mUpcomingFeatured:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 50
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0}, Lnet/flixster/android/LviActivityCopy;->onResume()V

    .line 42
    const-wide/16 v0, 0x64

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/UpcomingPage;->ScheduleLoadItemsTask(J)V

    .line 43
    return-void
.end method

.method protected retryAction()V
    .locals 2

    .prologue
    .line 89
    const-wide/16 v0, 0x3e8

    invoke-direct {p0, v0, v1}, Lnet/flixster/android/UpcomingPage;->ScheduleLoadItemsTask(J)V

    .line 90
    return-void
.end method
