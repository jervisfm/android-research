.class Lnet/flixster/android/UserProfilePage$1;
.super Landroid/os/Handler;
.source "UserProfilePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/UserProfilePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UserProfilePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/UserProfilePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UserProfilePage$1;->this$0:Lnet/flixster/android/UserProfilePage;

    .line 158
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .parameter "message"

    .prologue
    .line 162
    const-string v0, "FlxMain"

    const-string v1, "UserProfilePage.updateHandler"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage$1;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/UserProfilePage$1;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v0

    iget-object v0, v0, Lnet/flixster/android/model/User;->reviews:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/UserProfilePage$1;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v0

    iget-object v0, v0, Lnet/flixster/android/model/User;->reviews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/UserProfilePage$1;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v0

    iget v0, v0, Lnet/flixster/android/model/User;->reviewCount:I

    if-gtz v0, :cond_2

    .line 164
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage$1;->this$0:Lnet/flixster/android/UserProfilePage;

    #calls: Lnet/flixster/android/UserProfilePage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/UserProfilePage;->access$1(Lnet/flixster/android/UserProfilePage;)V

    .line 170
    :cond_1
    :goto_0
    return-void

    .line 166
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage$1;->this$0:Lnet/flixster/android/UserProfilePage;

    invoke-virtual {v0}, Lnet/flixster/android/UserProfilePage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 167
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage$1;->this$0:Lnet/flixster/android/UserProfilePage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/UserProfilePage;->showDialog(I)V

    goto :goto_0
.end method
