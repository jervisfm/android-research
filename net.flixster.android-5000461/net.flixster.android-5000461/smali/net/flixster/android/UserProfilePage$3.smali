.class Lnet/flixster/android/UserProfilePage$3;
.super Ljava/lang/Object;
.source "UserProfilePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/UserProfilePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UserProfilePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/UserProfilePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .parameter "view"

    .prologue
    .line 193
    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->ratedLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$5(Lnet/flixster/android/UserProfilePage;)Landroid/widget/LinearLayout;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 194
    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x3

    if-le v3, v4, :cond_0

    .line 196
    const/4 v2, 0x0

    .line 197
    .local v2, reviewView:Landroid/view/View;
    const/4 v0, 0x3

    .local v0, i:I
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v0, v3, :cond_2

    .line 203
    .end local v0           #i:I
    .end local v2           #reviewView:Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$7(Lnet/flixster/android/UserProfilePage;)Landroid/widget/RelativeLayout;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 204
    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$7(Lnet/flixster/android/UserProfilePage;)Landroid/widget/RelativeLayout;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 206
    :cond_1
    return-void

    .line 198
    .restart local v0       #i:I
    .restart local v2       #reviewView:Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Review;

    .line 199
    .local v1, review:Lnet/flixster/android/model/Review;
    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #calls: Lnet/flixster/android/UserProfilePage;->getReviewView(Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;
    invoke-static {v3, v1, v2}, Lnet/flixster/android/UserProfilePage;->access$6(Lnet/flixster/android/UserProfilePage;Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;

    move-result-object v2

    .line 200
    iget-object v3, p0, Lnet/flixster/android/UserProfilePage$3;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->ratedLayout:Landroid/widget/LinearLayout;
    invoke-static {v3}, Lnet/flixster/android/UserProfilePage;->access$5(Lnet/flixster/android/UserProfilePage;)Landroid/widget/LinearLayout;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 197
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
