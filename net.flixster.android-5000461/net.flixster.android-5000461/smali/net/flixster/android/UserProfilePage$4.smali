.class Lnet/flixster/android/UserProfilePage$4;
.super Ljava/lang/Object;
.source "UserProfilePage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/UserProfilePage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UserProfilePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/UserProfilePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UserProfilePage$4;->this$0:Lnet/flixster/android/UserProfilePage;

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .parameter "view"

    .prologue
    .line 211
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Review;

    .line 212
    .local v3, review:Lnet/flixster/android/model/Review;
    if-eqz v3, :cond_1

    iget-object v5, p0, Lnet/flixster/android/UserProfilePage$4;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v5}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 213
    invoke-virtual {v3}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v2

    .line 214
    .local v2, movie:Lnet/flixster/android/model/Movie;
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v5

    const-string v6, "/mymovies/profile"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "User Review - "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "title"

    invoke-virtual {v2, v8}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v5, p0, Lnet/flixster/android/UserProfilePage$4;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v5}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v5

    iget-object v5, v5, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 216
    .local v0, index:I
    if-gez v0, :cond_0

    .line 217
    iget-object v5, p0, Lnet/flixster/android/UserProfilePage$4;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v5}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v5

    iget-object v5, v5, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 218
    if-gez v0, :cond_2

    .line 219
    const/4 v0, 0x0

    .line 224
    :cond_0
    :goto_0
    iget-object v5, p0, Lnet/flixster/android/UserProfilePage$4;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v5}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v5

    iget-object v5, v5, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 225
    .local v4, userId:Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v5, "PROFILE_REVIEW_PAGE"

    const/4 v6, 0x0

    iget-object v7, p0, Lnet/flixster/android/UserProfilePage$4;->this$0:Lnet/flixster/android/UserProfilePage;

    invoke-virtual {v7}, Lnet/flixster/android/UserProfilePage;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 226
    const-class v8, Lnet/flixster/android/ProfileReviewPage;

    .line 225
    invoke-direct {v1, v5, v6, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 227
    .local v1, intent:Landroid/content/Intent;
    const-string v5, "REVIEW_INDEX"

    invoke-virtual {v1, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 228
    const-string v5, "PLATFORM_ID"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    iget-object v5, p0, Lnet/flixster/android/UserProfilePage$4;->this$0:Lnet/flixster/android/UserProfilePage;

    invoke-virtual {v5, v1}, Lnet/flixster/android/UserProfilePage;->startActivity(Landroid/content/Intent;)V

    .line 231
    .end local v0           #index:I
    .end local v1           #intent:Landroid/content/Intent;
    .end local v2           #movie:Lnet/flixster/android/model/Movie;
    .end local v4           #userId:Ljava/lang/String;
    :cond_1
    return-void

    .line 221
    .restart local v0       #index:I
    .restart local v2       #movie:Lnet/flixster/android/model/Movie;
    :cond_2
    iget-object v5, p0, Lnet/flixster/android/UserProfilePage$4;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v5}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v5

    iget-object v5, v5, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int/2addr v0, v5

    goto :goto_0
.end method
