.class Lnet/flixster/android/UserProfilePage$8;
.super Ljava/util/TimerTask;
.source "UserProfilePage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/UserProfilePage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UserProfilePage;


# direct methods
.method constructor <init>(Lnet/flixster/android/UserProfilePage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    .line 115
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 118
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    iget-object v7, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    invoke-virtual {v7}, Lnet/flixster/android/UserProfilePage;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    const-string v8, "PLATFORM_ID"

    invoke-virtual {v7, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    #setter for: Lnet/flixster/android/UserProfilePage;->platformId:Ljava/lang/String;
    invoke-static {v6, v7}, Lnet/flixster/android/UserProfilePage;->access$11(Lnet/flixster/android/UserProfilePage;Ljava/lang/String;)V

    .line 119
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->platformId:Ljava/lang/String;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$12(Lnet/flixster/android/UserProfilePage;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    .line 120
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformId()Ljava/lang/String;

    move-result-object v7

    #setter for: Lnet/flixster/android/UserProfilePage;->platformId:Ljava/lang/String;
    invoke-static {v6, v7}, Lnet/flixster/android/UserProfilePage;->access$11(Lnet/flixster/android/UserProfilePage;Ljava/lang/String;)V

    .line 122
    :cond_0
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    iget-object v6, v6, Lnet/flixster/android/model/User;->reviews:Ljava/util/List;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    iget-object v6, v6, Lnet/flixster/android/model/User;->reviews:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    iget v6, v6, Lnet/flixster/android/model/User;->reviewCount:I

    if-lez v6, :cond_2

    .line 124
    :cond_1
    :try_start_0
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    iget-object v7, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->platformId:Ljava/lang/String;
    invoke-static {v7}, Lnet/flixster/android/UserProfilePage;->access$12(Lnet/flixster/android/UserProfilePage;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lnet/flixster/android/data/ProfileDao;->fetchUser(Ljava/lang/String;)Lnet/flixster/android/model/User;

    move-result-object v7

    #setter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6, v7}, Lnet/flixster/android/UserProfilePage;->access$13(Lnet/flixster/android/UserProfilePage;Lnet/flixster/android/model/User;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :goto_0
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    iget-object v6, v6, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-eqz v6, :cond_2

    .line 132
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v2, ratedReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v5, wantToSeeReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    iget-object v6, v6, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v7, 0x19

    invoke-static {v6, v7}, Lnet/flixster/android/data/ProfileDao;->getUserReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v4

    .line 135
    .local v4, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-lt v1, v6, :cond_3

    .line 143
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    iput-object v2, v6, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    .line 144
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;

    move-result-object v6

    iput-object v5, v6, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_1

    .line 150
    .end local v1           #i:I
    .end local v2           #ratedReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .end local v4           #reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .end local v5           #wantToSeeReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    :cond_2
    :goto_2
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    #getter for: Lnet/flixster/android/UserProfilePage;->updateHandler:Landroid/os/Handler;
    invoke-static {v6}, Lnet/flixster/android/UserProfilePage;->access$14(Lnet/flixster/android/UserProfilePage;)Landroid/os/Handler;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 151
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v6, "FlxMain"

    const-string v7, "UserProfilePage.scheduleUpdatePageTask (failed to get user data)"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 127
    iget-object v6, p0, Lnet/flixster/android/UserProfilePage$8;->this$0:Lnet/flixster/android/UserProfilePage;

    const/4 v7, 0x0

    #setter for: Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;
    invoke-static {v6, v7}, Lnet/flixster/android/UserProfilePage;->access$13(Lnet/flixster/android/UserProfilePage;Lnet/flixster/android/model/User;)V

    goto :goto_0

    .line 136
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    .restart local v1       #i:I
    .restart local v2       #ratedReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .restart local v4       #reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .restart local v5       #wantToSeeReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    :cond_3
    :try_start_2
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Review;

    .line 137
    .local v3, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v3}, Lnet/flixster/android/model/Review;->isWantToSee()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 138
    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    :cond_4
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 139
    :cond_5
    invoke-virtual {v3}, Lnet/flixster/android/model/Review;->isNotInterested()Z

    move-result v6

    if-nez v6, :cond_4

    .line 140
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 145
    .end local v1           #i:I
    .end local v2           #ratedReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .end local v3           #review:Lnet/flixster/android/model/Review;
    .end local v4           #reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .end local v5           #wantToSeeReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    :catch_1
    move-exception v0

    .line 146
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    const-string v6, "FlxMain"

    const-string v7, "UserProfilePage.scheduleUpdatePageTask (failed to get review data)"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
