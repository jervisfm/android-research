.class public Lnet/flixster/android/UserProfilePage;
.super Lnet/flixster/android/FlixsterActivity;
.source "UserProfilePage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/UserProfilePage$MovieViewHolder;,
        Lnet/flixster/android/UserProfilePage$ProfileViewHolder;,
        Lnet/flixster/android/UserProfilePage$ReviewViewHolder;
    }
.end annotation


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field protected static final HIDE_THROBBER:I = 0x0

.field private static final LIMIT_RATING:I = 0x3

.field private static final LIMIT_WANT_TO_SEE:I = 0x3

.field protected static final SHOW_THROBBER:I = 0x1


# instance fields
.field private inflater:Landroid/view/LayoutInflater;

.field private moreRatedLayout:Landroid/widget/RelativeLayout;

.field private moreRatedListener:Landroid/view/View$OnClickListener;

.field private moreWantToSeeLayout:Landroid/widget/RelativeLayout;

.field private moreWantToSeeListener:Landroid/view/View$OnClickListener;

.field private final movieThumbnailHandler:Landroid/os/Handler;

.field private platformId:Ljava/lang/String;

.field private ratedLayout:Landroid/widget/LinearLayout;

.field private resources:Landroid/content/res/Resources;

.field private reviewClickListener:Landroid/view/View$OnClickListener;

.field private throbber:Landroid/view/View;

.field protected final throbberHandler:Landroid/os/Handler;

.field private timer:Ljava/util/Timer;

.field private updateHandler:Landroid/os/Handler;

.field private user:Lnet/flixster/android/model/User;

.field private wantToSeeLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 158
    new-instance v0, Lnet/flixster/android/UserProfilePage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserProfilePage$1;-><init>(Lnet/flixster/android/UserProfilePage;)V

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->updateHandler:Landroid/os/Handler;

    .line 173
    new-instance v0, Lnet/flixster/android/UserProfilePage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserProfilePage$2;-><init>(Lnet/flixster/android/UserProfilePage;)V

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeListener:Landroid/view/View$OnClickListener;

    .line 191
    new-instance v0, Lnet/flixster/android/UserProfilePage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserProfilePage$3;-><init>(Lnet/flixster/android/UserProfilePage;)V

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->moreRatedListener:Landroid/view/View$OnClickListener;

    .line 209
    new-instance v0, Lnet/flixster/android/UserProfilePage$4;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserProfilePage$4;-><init>(Lnet/flixster/android/UserProfilePage;)V

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->reviewClickListener:Landroid/view/View$OnClickListener;

    .line 568
    new-instance v0, Lnet/flixster/android/UserProfilePage$5;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserProfilePage$5;-><init>(Lnet/flixster/android/UserProfilePage;)V

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->movieThumbnailHandler:Landroid/os/Handler;

    .line 615
    new-instance v0, Lnet/flixster/android/UserProfilePage$6;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserProfilePage$6;-><init>(Lnet/flixster/android/UserProfilePage;)V

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->throbberHandler:Landroid/os/Handler;

    .line 37
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/UserProfilePage;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 42
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/UserProfilePage;)V
    .locals 0
    .parameter

    .prologue
    .line 234
    invoke-direct {p0}, Lnet/flixster/android/UserProfilePage;->updatePage()V

    return-void
.end method

.method static synthetic access$10(Lnet/flixster/android/UserProfilePage;)V
    .locals 0
    .parameter

    .prologue
    .line 114
    invoke-direct {p0}, Lnet/flixster/android/UserProfilePage;->scheduleUpdatePageTask()V

    return-void
.end method

.method static synthetic access$11(Lnet/flixster/android/UserProfilePage;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 44
    iput-object p1, p0, Lnet/flixster/android/UserProfilePage;->platformId:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$12(Lnet/flixster/android/UserProfilePage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 44
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->platformId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$13(Lnet/flixster/android/UserProfilePage;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 42
    iput-object p1, p0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$14(Lnet/flixster/android/UserProfilePage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 158
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/UserProfilePage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->wantToSeeLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/UserProfilePage;Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 401
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/UserProfilePage;->getMovieView(Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/UserProfilePage;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 47
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$5(Lnet/flixster/android/UserProfilePage;)Landroid/widget/LinearLayout;
    .locals 1
    .parameter

    .prologue
    .line 48
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->ratedLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/UserProfilePage;Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;
    .locals 1
    .parameter
    .parameter
    .parameter

    .prologue
    .line 356
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/UserProfilePage;->getReviewView(Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7(Lnet/flixster/android/UserProfilePage;)Landroid/widget/RelativeLayout;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/UserProfilePage;)V
    .locals 0
    .parameter

    .prologue
    .line 630
    invoke-direct {p0}, Lnet/flixster/android/UserProfilePage;->showLoading()V

    return-void
.end method

.method static synthetic access$9(Lnet/flixster/android/UserProfilePage;)V
    .locals 0
    .parameter

    .prologue
    .line 634
    invoke-direct {p0}, Lnet/flixster/android/UserProfilePage;->hideLoading()V

    return-void
.end method

.method private addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V
    .locals 4
    .parameter "score"
    .parameter "scoreIcon"
    .parameter "scoreView"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 562
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p2, v2, v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 563
    invoke-virtual {p3, p2, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 564
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 565
    invoke-virtual {p3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 566
    return-void
.end method

.method private getMovieView(Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;
    .locals 30
    .parameter "review"
    .parameter "movieView"

    .prologue
    .line 403
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, Lnet/flixster/android/UserProfilePage$MovieViewHolder;

    if-nez v4, :cond_1

    .line 404
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->inflater:Landroid/view/LayoutInflater;

    const v7, 0x7f03005a

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 406
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lnet/flixster/android/UserProfilePage$MovieViewHolder;

    .line 407
    .local v25, viewHolder:Lnet/flixster/android/UserProfilePage$MovieViewHolder;
    if-nez v25, :cond_2

    .line 408
    new-instance v25, Lnet/flixster/android/UserProfilePage$MovieViewHolder;

    .end local v25           #viewHolder:Lnet/flixster/android/UserProfilePage$MovieViewHolder;
    const/4 v4, 0x0

    move-object/from16 v0, v25

    invoke-direct {v0, v4}, Lnet/flixster/android/UserProfilePage$MovieViewHolder;-><init>(Lnet/flixster/android/UserProfilePage$MovieViewHolder;)V

    .line 409
    .restart local v25       #viewHolder:Lnet/flixster/android/UserProfilePage$MovieViewHolder;
    const v4, 0x7f070108

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->titleView:Landroid/widget/TextView;

    .line 410
    const v4, 0x7f07012e

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 411
    const v4, 0x7f070132

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    .line 412
    const v4, 0x7f070133

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    .line 413
    const v4, 0x7f07010b

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->actorsView:Landroid/widget/TextView;

    .line 414
    const v4, 0x7f07010c

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->metaView:Landroid/widget/TextView;

    .line 415
    const v4, 0x7f07010d

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    move-object/from16 v0, v25

    iput-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->releaseView:Landroid/widget/TextView;

    .line 416
    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 420
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v5

    .line 421
    .local v5, movie:Lnet/flixster/android/model/Movie;
    const-string v4, "title"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 422
    .local v22, title:Ljava/lang/String;
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 424
    iget-object v4, v5, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_8

    .line 425
    move-object/from16 v0, v25

    iget-object v7, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    iget-object v4, v5, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v4}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v7, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 443
    :cond_3
    :goto_0
    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v4

    if-eqz v4, :cond_9

    .line 444
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v5}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-nez v4, :cond_9

    const/4 v14, 0x0

    .line 445
    .local v14, isReleased:Z
    :goto_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v18

    .line 446
    .local v18, ratingType:I
    packed-switch v18, :pswitch_data_0

    .line 479
    :cond_4
    :goto_2
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_e

    .line 480
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 526
    :goto_3
    const-string v4, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 527
    .local v9, actors:Ljava/lang/String;
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_5

    .line 528
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->actorsView:Landroid/widget/TextView;

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 529
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->actorsView:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 532
    :cond_5
    const-string v4, "meta"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 533
    .local v16, meta:Ljava/lang/String;
    if-eqz v16, :cond_6

    .line 534
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->metaView:Landroid/widget/TextView;

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 535
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->metaView:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 538
    :cond_6
    const-string v4, "Opening This Week"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 539
    const-string v4, "theaterReleaseDate"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 540
    .local v19, release:Ljava/lang/String;
    if-eqz v19, :cond_7

    .line 541
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->releaseView:Landroid/widget/TextView;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 542
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->releaseView:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 546
    .end local v19           #release:Ljava/lang/String;
    :cond_7
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 547
    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setFocusable(Z)V

    .line 548
    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setClickable(Z)V

    .line 549
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->reviewClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 550
    return-object p2

    .line 427
    .end local v9           #actors:Ljava/lang/String;
    .end local v14           #isReleased:Z
    .end local v16           #meta:Ljava/lang/String;
    .end local v18           #ratingType:I
    :cond_8
    const-string v4, "thumbnail"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 428
    .local v6, thumbnailUrl:Ljava/lang/String;
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_3

    .line 429
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 430
    new-instance v3, Lnet/flixster/android/model/ImageOrder;

    const/4 v4, 0x0

    .line 431
    move-object/from16 v0, v25

    iget-object v7, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->thumbnailView:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lnet/flixster/android/UserProfilePage;->movieThumbnailHandler:Landroid/os/Handler;

    .line 430
    invoke-direct/range {v3 .. v8}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 432
    .local v3, imageOrder:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/UserProfilePage;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto/16 :goto_0

    .line 444
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .end local v6           #thumbnailUrl:Ljava/lang/String;
    :cond_9
    const/4 v14, 0x1

    goto/16 :goto_1

    .line 450
    .restart local v14       #isReleased:Z
    .restart local v18       #ratingType:I
    :pswitch_0
    const-string v4, "popcornScore"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 451
    const-string v4, "popcornScore"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 452
    .local v10, flixsterScore:I
    if-lez v10, :cond_4

    .line 453
    const/16 v4, 0x3c

    if-ge v10, v4, :cond_a

    const/4 v15, 0x1

    .line 454
    .local v15, isSpilled:Z
    :goto_4
    if-nez v14, :cond_b

    const v13, 0x7f0200f6

    .line 456
    .local v13, iconId:I
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    invoke-virtual {v4, v13}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v21

    .line 457
    .local v21, scoreIcon:Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    invoke-direct {v0, v10, v1, v4}, Lnet/flixster/android/UserProfilePage;->addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V

    goto/16 :goto_2

    .line 453
    .end local v13           #iconId:I
    .end local v15           #isSpilled:Z
    .end local v21           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_a
    const/4 v15, 0x0

    goto :goto_4

    .line 454
    .restart local v15       #isSpilled:Z
    :cond_b
    if-eqz v15, :cond_c

    const v13, 0x7f0200ef

    goto :goto_5

    .line 455
    :cond_c
    const v13, 0x7f0200ea

    goto :goto_5

    .line 464
    .end local v10           #flixsterScore:I
    .end local v15           #isSpilled:Z
    :pswitch_1
    const-string v4, "rottenTomatoes"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 465
    const-string v4, "rottenTomatoes"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v24

    .line 466
    .local v24, tomatoScore:I
    if-lez v24, :cond_4

    .line 468
    const/16 v4, 0x3c

    move/from16 v0, v24

    if-ge v0, v4, :cond_d

    .line 469
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0200ed

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .line 473
    .local v23, tomatoIcon:Landroid/graphics/drawable/Drawable;
    :goto_6
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->scoreView:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move/from16 v1, v24

    move-object/from16 v2, v23

    invoke-direct {v0, v1, v2, v4}, Lnet/flixster/android/UserProfilePage;->addScore(ILandroid/graphics/drawable/Drawable;Landroid/widget/TextView;)V

    goto/16 :goto_2

    .line 471
    .end local v23           #tomatoIcon:Landroid/graphics/drawable/Drawable;
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0200de

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .restart local v23       #tomatoIcon:Landroid/graphics/drawable/Drawable;
    goto :goto_6

    .line 481
    .end local v23           #tomatoIcon:Landroid/graphics/drawable/Drawable;
    .end local v24           #tomatoScore:I
    :cond_e
    const-string v4, "FRIENDS_RATED_COUNT"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_10

    .line 482
    const-string v4, "FRIENDS_RATED_COUNT"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_10

    .line 483
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0200eb

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v20

    .line 484
    .local v20, reviewIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v28

    move-object/from16 v0, v20

    move/from16 v1, v28

    invoke-virtual {v0, v4, v7, v8, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 485
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v28

    invoke-virtual {v4, v0, v7, v8, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 486
    const-string v4, "FRIENDS_RATED_COUNT"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 487
    .local v11, friendsRatedCount:I
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    .line 488
    .local v17, ratingCountBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 489
    const/4 v4, 0x1

    if-le v11, v4, :cond_f

    .line 490
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0c0090

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 494
    :goto_7
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 492
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0c008f

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 496
    .end local v11           #friendsRatedCount:I
    .end local v17           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v20           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_10
    const-string v4, "FRIENDS_WTS_COUNT"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 497
    const-string v4, "FRIENDS_WTS_COUNT"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_14

    .line 498
    const-string v4, "FRIENDS_WTS_COUNT"

    invoke-virtual {v5, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v12

    .line 499
    .local v12, friendsWantToSeeCount:I
    new-instance v26, Ljava/lang/StringBuilder;

    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    .line 500
    .local v26, wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    if-nez v14, :cond_12

    if-nez v18, :cond_12

    .line 501
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v28, 0x0

    const/16 v29, 0x0

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    invoke-virtual {v4, v7, v8, v0, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 502
    const-string v4, "("

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 503
    const/4 v4, 0x1

    if-le v12, v4, :cond_11

    .line 504
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0c008e

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    :goto_8
    const-string v4, ")"

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 520
    :goto_9
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 521
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 506
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0c008d

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_8

    .line 510
    :cond_12
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0200f7

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v27

    .line 511
    .local v27, wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    invoke-virtual/range {v27 .. v27}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v28

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v0, v4, v7, v8, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 512
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/16 v28, 0x0

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    invoke-virtual {v4, v0, v7, v8, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 513
    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    const/4 v4, 0x1

    if-le v12, v4, :cond_13

    .line 515
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0c0094

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 517
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v7, 0x7f0c0093

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v26

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    .line 523
    .end local v12           #friendsWantToSeeCount:I
    .end local v26           #wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    .end local v27           #wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    :cond_14
    move-object/from16 v0, v25

    iget-object v4, v0, Lnet/flixster/android/UserProfilePage$MovieViewHolder;->friendScore:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 446
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getProfileView(Lnet/flixster/android/model/User;)Landroid/view/View;
    .locals 14
    .parameter "user"

    .prologue
    const/4 v11, 0x0

    const/4 v13, 0x0

    .line 306
    iget-object v9, p0, Lnet/flixster/android/UserProfilePage;->inflater:Landroid/view/LayoutInflater;

    const v10, 0x7f03006e

    invoke-virtual {v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 307
    .local v5, profileView:Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;

    .line 308
    .local v7, viewHolder:Lnet/flixster/android/UserProfilePage$ProfileViewHolder;
    if-nez v7, :cond_0

    .line 309
    new-instance v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;

    .end local v7           #viewHolder:Lnet/flixster/android/UserProfilePage$ProfileViewHolder;
    invoke-direct {v7, v11}, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;-><init>(Lnet/flixster/android/UserProfilePage$ProfileViewHolder;)V

    .line 310
    .restart local v7       #viewHolder:Lnet/flixster/android/UserProfilePage$ProfileViewHolder;
    const v9, 0x7f07020a

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 311
    const v9, 0x7f07020b

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/ImageView;

    iput-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    .line 312
    const v9, 0x7f07020c

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    .line 313
    const v9, 0x7f0700a5

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->friendView:Landroid/widget/TextView;

    .line 314
    const v9, 0x7f0700a4

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->ratingView:Landroid/widget/TextView;

    .line 315
    const v9, 0x7f0700a3

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->wtsView:Landroid/widget/TextView;

    .line 316
    const v9, 0x7f0700a6

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    iput-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    .line 317
    invoke-virtual {v5, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 321
    :cond_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v4

    .line 322
    .local v4, platform:Ljava/lang/String;
    const-string v9, "FLX"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 323
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    const v10, 0x7f0200bc

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 328
    :cond_1
    :goto_0
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {p1, v9}, Lnet/flixster/android/model/User;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 329
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 330
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 333
    :cond_2
    iget-object v2, p1, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    .line 334
    .local v2, displayName:Ljava/lang/String;
    if-eqz v2, :cond_3

    .line 335
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v9, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 338
    :cond_3
    iget v3, p1, Lnet/flixster/android/model/User;->friendCount:I

    .line 339
    .local v3, friendCount:I
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->friendView:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, ""

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 340
    iget-object v11, p0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v12, 0x7f0c0082

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 339
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    iget v6, p1, Lnet/flixster/android/model/User;->ratingCount:I

    .line 342
    .local v6, ratingCount:I
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->ratingView:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, ""

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 343
    iget-object v11, p0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v12, 0x7f0c0083

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 342
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 344
    iget v8, p1, Lnet/flixster/android/model/User;->wtsCount:I

    .line 345
    .local v8, wtsCount:I
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->wtsView:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, ""

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 346
    iget-object v11, p0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v12, 0x7f0c0084

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 345
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 347
    iget v1, p1, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 348
    .local v1, collectionsCount:I
    if-lez v1, :cond_4

    .line 349
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, ""

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 350
    iget-object v11, p0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    const v12, 0x7f0c0085

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    .line 349
    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 351
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    invoke-virtual {v9, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 353
    :cond_4
    return-object v5

    .line 324
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v1           #collectionsCount:I
    .end local v2           #displayName:Ljava/lang/String;
    .end local v3           #friendCount:I
    .end local v6           #ratingCount:I
    .end local v8           #wtsCount:I
    :cond_5
    const-string v9, "FBK"

    invoke-virtual {v9, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 325
    iget-object v9, v7, Lnet/flixster/android/UserProfilePage$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    const v10, 0x7f020149

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method private getReviewView(Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;
    .locals 12
    .parameter "review"
    .parameter "reviewView"

    .prologue
    .line 358
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;

    if-nez v1, :cond_1

    .line 359
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/UserProfilePage;->inflater:Landroid/view/LayoutInflater;

    const v4, 0x7f030059

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 361
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;

    .line 362
    .local v7, viewHolder:Lnet/flixster/android/UserProfilePage$ReviewViewHolder;
    if-nez v7, :cond_2

    .line 363
    new-instance v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;

    .end local v7           #viewHolder:Lnet/flixster/android/UserProfilePage$ReviewViewHolder;
    const/4 v1, 0x0

    invoke-direct {v7, v1}, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;-><init>(Lnet/flixster/android/UserProfilePage$ReviewViewHolder;)V

    .line 364
    .restart local v7       #viewHolder:Lnet/flixster/android/UserProfilePage$ReviewViewHolder;
    const v1, 0x7f07013d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->starsView:Landroid/widget/ImageView;

    .line 365
    const v1, 0x7f07013f

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->commentView:Landroid/widget/TextView;

    .line 366
    const v1, 0x7f07013b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->titleView:Landroid/widget/TextView;

    .line 367
    const v1, 0x7f07013a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    .line 368
    invoke-virtual {p2, v7}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 372
    :cond_2
    iget-object v1, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->starsView:Landroid/widget/ImageView;

    sget-object v4, Lnet/flixster/android/Flixster;->RATING_SMALL_R:[I

    iget-wide v8, p1, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v10, 0x4000

    mul-double/2addr v8, v10

    double-to-int v5, v8

    aget v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 373
    iget-object v1, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->commentView:Landroid/widget/TextView;

    iget-object v4, p1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 375
    invoke-virtual {p1}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v2

    .line 376
    .local v2, movie:Lnet/flixster/android/model/Movie;
    if-eqz v2, :cond_3

    .line 377
    const-string v1, "title"

    invoke-virtual {v2, v1}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 378
    .local v6, title:Ljava/lang/String;
    iget-object v1, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    iget-object v1, v2, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 380
    iget-object v4, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    iget-object v1, v2, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 392
    .end local v6           #title:Ljava/lang/String;
    :cond_3
    :goto_0
    invoke-virtual {p2, p1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 393
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 394
    const/4 v1, 0x1

    invoke-virtual {p2, v1}, Landroid/view/View;->setClickable(Z)V

    .line 395
    const v1, 0x7f030059

    invoke-virtual {p2, v1}, Landroid/view/View;->setId(I)V

    .line 396
    iget-object v1, p0, Lnet/flixster/android/UserProfilePage;->reviewClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 397
    return-object p2

    .line 382
    .restart local v6       #title:Ljava/lang/String;
    :cond_4
    const-string v1, "thumbnail"

    invoke-virtual {v2, v1}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 383
    .local v3, thumbnailUrl:Ljava/lang/String;
    if-eqz v3, :cond_3

    .line 384
    iget-object v1, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 385
    new-instance v0, Lnet/flixster/android/model/ImageOrder;

    const/4 v1, 0x0

    .line 386
    iget-object v4, v7, Lnet/flixster/android/UserProfilePage$ReviewViewHolder;->movieView:Landroid/widget/ImageView;

    iget-object v5, p0, Lnet/flixster/android/UserProfilePage;->movieThumbnailHandler:Landroid/os/Handler;

    .line 385
    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 387
    .local v0, imageOrder:Lnet/flixster/android/model/ImageOrder;
    invoke-virtual {p0, v0}, Lnet/flixster/android/UserProfilePage;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_0
.end method

.method private getTextView(Ljava/lang/String;)Landroid/view/View;
    .locals 4
    .parameter "text"

    .prologue
    const/16 v3, 0xa

    .line 554
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 555
    .local v0, textView:Landroid/widget/TextView;
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 556
    invoke-virtual {p0}, Lnet/flixster/android/UserProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f09000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 557
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 558
    return-object v0
.end method

.method private hideLoading()V
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->throbber:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 636
    return-void
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 115
    new-instance v0, Lnet/flixster/android/UserProfilePage$8;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserProfilePage$8;-><init>(Lnet/flixster/android/UserProfilePage;)V

    .line 153
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/UserProfilePage;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 154
    iget-object v1, p0, Lnet/flixster/android/UserProfilePage;->timer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 156
    :cond_0
    return-void
.end method

.method private showLoading()V
    .locals 2

    .prologue
    .line 631
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->throbber:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 632
    return-void
.end method

.method private updatePage()V
    .locals 19

    .prologue
    .line 235
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    if-eqz v15, :cond_6

    .line 236
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lnet/flixster/android/UserProfilePage;->inflater:Landroid/view/LayoutInflater;

    .line 237
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/UserProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    .line 238
    const v15, 0x7f070209

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Lnet/flixster/android/UserProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 240
    .local v8, profileLayout:Landroid/widget/LinearLayout;
    const v15, 0x7f0702d1

    invoke-virtual {v8, v15}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 241
    .local v7, profileHeaderLayout:Landroid/widget/LinearLayout;
    invoke-virtual {v7}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v15

    if-gtz v15, :cond_0

    .line 242
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lnet/flixster/android/UserProfilePage;->getProfileView(Lnet/flixster/android/model/User;)Landroid/view/View;

    move-result-object v9

    .line 243
    .local v9, profileView:Landroid/view/View;
    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 245
    .end local v9           #profileView:Landroid/view/View;
    :cond_0
    const v15, 0x7f0702d3

    invoke-virtual {v8, v15}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v15, v0, Lnet/flixster/android/UserProfilePage;->wantToSeeLayout:Landroid/widget/LinearLayout;

    .line 246
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_2

    .line 247
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->wantToSeeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v15}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 248
    const v15, 0x7f0702d2

    invoke-virtual {v8, v15}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 249
    .local v14, wantToSeeText:Landroid/widget/TextView;
    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    const/4 v6, 0x0

    .line 251
    .local v6, movieView:Landroid/view/View;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-ge v3, v15, :cond_1

    const/4 v15, 0x3

    if-lt v3, v15, :cond_7

    .line 256
    :cond_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x3

    move/from16 v0, v16

    if-le v15, v0, :cond_2

    .line 257
    const v15, 0x7f0702d4

    invoke-virtual {v8, v15}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeLayout:Landroid/widget/RelativeLayout;

    .line 258
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeLayout:Landroid/widget/RelativeLayout;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 259
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 260
    .local v13, totalBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f0c0080

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeLayout:Landroid/widget/RelativeLayout;

    const v16, 0x7f0702d6

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 263
    .local v5, moreWantToSeeLabel:Landroid/widget/TextView;
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v5, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 264
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeLayout:Landroid/widget/RelativeLayout;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 265
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeLayout:Landroid/widget/RelativeLayout;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 266
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/UserProfilePage;->moreWantToSeeListener:Landroid/view/View$OnClickListener;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 269
    .end local v3           #i:I
    .end local v5           #moreWantToSeeLabel:Landroid/widget/TextView;
    .end local v6           #movieView:Landroid/view/View;
    .end local v13           #totalBuilder:Ljava/lang/StringBuilder;
    .end local v14           #wantToSeeText:Landroid/widget/TextView;
    :cond_2
    const v15, 0x7f0702d8

    invoke-virtual {v8, v15}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v15, v0, Lnet/flixster/android/UserProfilePage;->ratedLayout:Landroid/widget/LinearLayout;

    .line 270
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v15, :cond_4

    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->isEmpty()Z

    move-result v15

    if-nez v15, :cond_4

    .line 271
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->ratedLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v15}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 272
    const v15, 0x7f0702d7

    invoke-virtual {v8, v15}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 273
    .local v10, ratedText:Landroid/widget/TextView;
    const/4 v15, 0x0

    invoke-virtual {v10, v15}, Landroid/widget/TextView;->setVisibility(I)V

    .line 274
    const/4 v12, 0x0

    .line 275
    .local v12, reviewView:Landroid/view/View;
    const/4 v3, 0x0

    .restart local v3       #i:I
    :goto_1
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    if-ge v3, v15, :cond_3

    const/4 v15, 0x3

    if-lt v3, v15, :cond_8

    .line 280
    :cond_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    const/16 v16, 0x3

    move/from16 v0, v16

    if-le v15, v0, :cond_4

    .line 281
    const v15, 0x7f0702d9

    invoke-virtual {v8, v15}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iput-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;

    .line 282
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 283
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 284
    .restart local v13       #totalBuilder:Ljava/lang/StringBuilder;
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v15

    invoke-virtual {v13, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 285
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f0c0080

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;

    const v16, 0x7f0702db

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 287
    .local v4, moreRatedLabel:Landroid/widget/TextView;
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v4, v15}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 288
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 289
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 290
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->moreRatedLayout:Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/UserProfilePage;->moreRatedListener:Landroid/view/View$OnClickListener;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    .end local v3           #i:I
    .end local v4           #moreRatedLabel:Landroid/widget/TextView;
    .end local v10           #ratedText:Landroid/widget/TextView;
    .end local v12           #reviewView:Landroid/view/View;
    .end local v13           #totalBuilder:Ljava/lang/StringBuilder;
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->wantToSeeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v15}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v15

    if-gtz v15, :cond_5

    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->ratedLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v15}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v15

    if-gtz v15, :cond_5

    .line 294
    new-instance v15, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lnet/flixster/android/model/User;->firstName:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    .line 295
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/UserProfilePage;->resources:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f0c008b

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 294
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 296
    .local v1, emptyBuilder:Ljava/lang/StringBuilder;
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    invoke-direct {v0, v15}, Lnet/flixster/android/UserProfilePage;->getTextView(Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 297
    .local v2, emptyView:Landroid/view/View;
    invoke-virtual {v8, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 299
    .end local v1           #emptyBuilder:Ljava/lang/StringBuilder;
    .end local v2           #emptyView:Landroid/view/View;
    :cond_5
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v15

    const-string v16, "/mymovies/profile"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "User Profile Page for user:"

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-interface/range {v15 .. v17}, Lcom/flixster/android/analytics/Tracker;->track(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->throbberHandler:Landroid/os/Handler;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 302
    .end local v7           #profileHeaderLayout:Landroid/widget/LinearLayout;
    .end local v8           #profileLayout:Landroid/widget/LinearLayout;
    :cond_6
    return-void

    .line 252
    .restart local v3       #i:I
    .restart local v6       #movieView:Landroid/view/View;
    .restart local v7       #profileHeaderLayout:Landroid/widget/LinearLayout;
    .restart local v8       #profileLayout:Landroid/widget/LinearLayout;
    .restart local v14       #wantToSeeText:Landroid/widget/TextView;
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    invoke-interface {v15, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lnet/flixster/android/model/Review;

    .line 253
    .local v11, review:Lnet/flixster/android/model/Review;
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v6}, Lnet/flixster/android/UserProfilePage;->getMovieView(Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;

    move-result-object v6

    .line 254
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->wantToSeeLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v15, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 251
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 276
    .end local v6           #movieView:Landroid/view/View;
    .end local v11           #review:Lnet/flixster/android/model/Review;
    .end local v14           #wantToSeeText:Landroid/widget/TextView;
    .restart local v10       #ratedText:Landroid/widget/TextView;
    .restart local v12       #reviewView:Landroid/view/View;
    :cond_8
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->user:Lnet/flixster/android/model/User;

    iget-object v15, v15, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    invoke-interface {v15, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lnet/flixster/android/model/Review;

    .line 277
    .restart local v11       #review:Lnet/flixster/android/model/Review;
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v12}, Lnet/flixster/android/UserProfilePage;->getReviewView(Lnet/flixster/android/model/Review;Landroid/view/View;)Landroid/view/View;

    move-result-object v12

    .line 278
    move-object/from16 v0, p0

    iget-object v15, v0, Lnet/flixster/android/UserProfilePage;->ratedLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v15, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 275
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .parameter "savedState"

    .prologue
    .line 55
    const-string v0, "FlxMain"

    const-string v1, "UserProfilePage.onCreate"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const v0, 0x7f03008d

    invoke-virtual {p0, v0}, Lnet/flixster/android/UserProfilePage;->setContentView(I)V

    .line 58
    invoke-virtual {p0}, Lnet/flixster/android/UserProfilePage;->createActionBar()V

    .line 59
    const v0, 0x7f0c007c

    invoke-virtual {p0, v0}, Lnet/flixster/android/UserProfilePage;->setActionBarTitle(I)V

    .line 61
    const v0, 0x7f070039

    invoke-virtual {p0, v0}, Lnet/flixster/android/UserProfilePage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->throbber:Landroid/view/View;

    .line 62
    invoke-direct {p0}, Lnet/flixster/android/UserProfilePage;->updatePage()V

    .line 63
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "dialogId"

    .prologue
    .line 78
    packed-switch p1, :pswitch_data_0

    .line 93
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 80
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 81
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 82
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 83
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 84
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/UserProfilePage$7;

    invoke-direct {v2, p0}, Lnet/flixster/android/UserProfilePage$7;-><init>(Lnet/flixster/android/UserProfilePage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 90
    invoke-virtual {p0}, Lnet/flixster/android/UserProfilePage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 91
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 1
    .parameter "menu"

    .prologue
    .line 99
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 104
    const-string v0, "FlxMain"

    const-string v1, "UserProfilePage.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 107
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 109
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 111
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->timer:Ljava/util/Timer;

    .line 112
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 67
    const-string v0, "FlxMain"

    const-string v1, "UserProfilePage.onResume"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 69
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 70
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/UserProfilePage;->timer:Ljava/util/Timer;

    .line 72
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/UserProfilePage;->throbberHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 73
    invoke-direct {p0}, Lnet/flixster/android/UserProfilePage;->scheduleUpdatePageTask()V

    .line 74
    return-void
.end method
