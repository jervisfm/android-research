.class Lnet/flixster/android/UserReviewPage$1;
.super Landroid/os/Handler;
.source "UserReviewPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/UserReviewPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UserReviewPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/UserReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    .line 211
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .parameter "message"

    .prologue
    .line 215
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    invoke-virtual {v0}, Lnet/flixster/android/UserReviewPage;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 219
    :cond_1
    const-string v0, "FlxMain"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "UserReviewPage.updateHandler - mReviewsList:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v2}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    #calls: Lnet/flixster/android/UserReviewPage;->hideLoading()V
    invoke-static {v0}, Lnet/flixster/android/UserReviewPage;->access$1(Lnet/flixster/android/UserReviewPage;)V

    .line 221
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v0}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v0}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    #calls: Lnet/flixster/android/UserReviewPage;->updatePage()V
    invoke-static {v0}, Lnet/flixster/android/UserReviewPage;->access$3(Lnet/flixster/android/UserReviewPage;)V

    goto :goto_0

    .line 224
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    invoke-virtual {v0}, Lnet/flixster/android/UserReviewPage;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 225
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage$1;->this$0:Lnet/flixster/android/UserReviewPage;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lnet/flixster/android/UserReviewPage;->showDialog(I)V

    goto :goto_0
.end method
