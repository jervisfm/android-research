.class Lnet/flixster/android/UserReviewPage$3;
.super Ljava/util/TimerTask;
.source "UserReviewPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/UserReviewPage;->scheduleUpdatePageTask()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/UserReviewPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/UserReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    .line 98
    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 100
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    :cond_0
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;

    move-result-object v2

    #setter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1, v2}, Lnet/flixster/android/UserReviewPage;->access$4(Lnet/flixster/android/UserReviewPage;Lnet/flixster/android/model/User;)V
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :cond_1
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget-object v1, v1, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 110
    :try_start_1
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviewType:I
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$5(Lnet/flixster/android/UserReviewPage;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 124
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    iget-object v2, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->reviews:Ljava/util/List;

    #setter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1, v2}, Lnet/flixster/android/UserReviewPage;->access$6(Lnet/flixster/android/UserReviewPage;Ljava/util/List;)V

    .line 125
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget v1, v1, Lnet/flixster/android/model/User;->reviewCount:I

    if-lez v1, :cond_3

    .line 126
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    iget-object v2, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v3, 0x32

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getUserReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    #setter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1, v2}, Lnet/flixster/android/UserReviewPage;->access$6(Lnet/flixster/android/UserReviewPage;Ljava/util/List;)V
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_1

    .line 134
    :cond_3
    :goto_1
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->updateHandler:Landroid/os/Handler;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$7(Lnet/flixster/android/UserReviewPage;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 135
    return-void

    .line 103
    :catch_0
    move-exception v0

    .line 104
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "UserReviewPage.scheduleUpdatePageTask (failed to get user data)"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 105
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    const/4 v2, 0x0

    #setter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1, v2}, Lnet/flixster/android/UserReviewPage;->access$4(Lnet/flixster/android/UserReviewPage;Lnet/flixster/android/model/User;)V

    goto :goto_0

    .line 112
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_0
    :try_start_2
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    iget-object v2, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    #setter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1, v2}, Lnet/flixster/android/UserReviewPage;->access$6(Lnet/flixster/android/UserReviewPage;Ljava/util/List;)V

    .line 113
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget v1, v1, Lnet/flixster/android/model/User;->wtsCount:I

    if-lez v1, :cond_3

    .line 114
    :cond_4
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    iget-object v2, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v3, 0x32

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getWantToSeeReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    #setter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1, v2}, Lnet/flixster/android/UserReviewPage;->access$6(Lnet/flixster/android/UserReviewPage;Ljava/util/List;)V
    :try_end_2
    .catch Lnet/flixster/android/data/DaoException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 130
    :catch_1
    move-exception v0

    .line 131
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "UserReviewPage.scheduleUpdatePageTask (failed to get review data)"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 118
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :pswitch_1
    :try_start_3
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    iget-object v2, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    #setter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1, v2}, Lnet/flixster/android/UserReviewPage;->access$6(Lnet/flixster/android/UserReviewPage;Ljava/util/List;)V

    .line 119
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v1

    iget v1, v1, Lnet/flixster/android/model/User;->ratingCount:I

    if-lez v1, :cond_3

    .line 120
    :cond_5
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    iget-object v2, p0, Lnet/flixster/android/UserReviewPage$3;->this$0:Lnet/flixster/android/UserReviewPage;

    #getter for: Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;
    invoke-static {v2}, Lnet/flixster/android/UserReviewPage;->access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v3, 0x32

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getUserRatedReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    #setter for: Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;
    invoke-static {v1, v2}, Lnet/flixster/android/UserReviewPage;->access$6(Lnet/flixster/android/UserReviewPage;Ljava/util/List;)V
    :try_end_3
    .catch Lnet/flixster/android/data/DaoException; {:try_start_3 .. :try_end_3} :catch_1

    goto/16 :goto_1

    .line 110
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
