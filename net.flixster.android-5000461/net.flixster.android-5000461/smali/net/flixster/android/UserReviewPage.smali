.class public Lnet/flixster/android/UserReviewPage;
.super Lnet/flixster/android/FlixsterActivity;
.source "UserReviewPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final DIALOG_NETWORK_FAIL:I = 0x1

.field public static final KEY_REVIEW_INDEX:Ljava/lang/String; = "REVIEW_INDEX"

.field public static final KEY_REVIEW_TYPE:Ljava/lang/String; = "REVIEW_TYPE"

.field public static final REVIEW_TYPE_MY_RATED:I = 0x1

.field public static final REVIEW_TYPE_MY_REVIEWS:I = 0x2

.field public static final REVIEW_TYPE_MY_WANT_TO_SEE:I


# instance fields
.field private final movieImageHandler:Landroid/os/Handler;

.field private reviewIndex:I

.field private reviewType:I

.field private reviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field private throbber:Landroid/view/View;

.field private timer:Ljava/util/Timer;

.field private final updateHandler:Landroid/os/Handler;

.field private user:Lnet/flixster/android/model/User;

.field private viewFlipper:Landroid/widget/ViewFlipper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lnet/flixster/android/FlixsterActivity;-><init>()V

    .line 211
    new-instance v0, Lnet/flixster/android/UserReviewPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserReviewPage$1;-><init>(Lnet/flixster/android/UserReviewPage;)V

    iput-object v0, p0, Lnet/flixster/android/UserReviewPage;->updateHandler:Landroid/os/Handler;

    .line 231
    new-instance v0, Lnet/flixster/android/UserReviewPage$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserReviewPage$2;-><init>(Lnet/flixster/android/UserReviewPage;)V

    iput-object v0, p0, Lnet/flixster/android/UserReviewPage;->movieImageHandler:Landroid/os/Handler;

    .line 38
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/UserReviewPage;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/UserReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 207
    invoke-direct {p0}, Lnet/flixster/android/UserReviewPage;->hideLoading()V

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/UserReviewPage;)Lnet/flixster/android/model/User;
    .locals 1
    .parameter

    .prologue
    .line 46
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/UserReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 142
    invoke-direct {p0}, Lnet/flixster/android/UserReviewPage;->updatePage()V

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/UserReviewPage;Lnet/flixster/android/model/User;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 46
    iput-object p1, p0, Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/UserReviewPage;)I
    .locals 1
    .parameter

    .prologue
    .line 47
    iget v0, p0, Lnet/flixster/android/UserReviewPage;->reviewType:I

    return v0
.end method

.method static synthetic access$6(Lnet/flixster/android/UserReviewPage;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 49
    iput-object p1, p0, Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;

    return-void
.end method

.method static synthetic access$7(Lnet/flixster/android/UserReviewPage;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 211
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->updateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lnet/flixster/android/UserReviewPage;)V
    .locals 0
    .parameter

    .prologue
    .line 97
    invoke-direct {p0}, Lnet/flixster/android/UserReviewPage;->scheduleUpdatePageTask()V

    return-void
.end method

.method private getPageTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    const v1, 0x7f0c005b

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->getDisplayedChild()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " of "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 300
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 299
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private hideLoading()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->throbber:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 209
    return-void
.end method

.method private scheduleUpdatePageTask()V
    .locals 4

    .prologue
    .line 98
    new-instance v0, Lnet/flixster/android/UserReviewPage$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/UserReviewPage$3;-><init>(Lnet/flixster/android/UserReviewPage;)V

    .line 137
    .local v0, updatePageTask:Ljava/util/TimerTask;
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->timer:Ljava/util/Timer;

    if-eqz v1, :cond_0

    .line 138
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->timer:Ljava/util/Timer;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v0, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 140
    :cond_0
    return-void
.end method

.method private showLoading()V
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->throbber:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 205
    return-void
.end method

.method private updatePage()V
    .locals 26

    .prologue
    .line 143
    const-string v3, "FlxMain"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v21, "UserReviewPage.updatePage reviewIndex:"

    move-object/from16 v0, v21

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v0, v0, Lnet/flixster/android/UserReviewPage;->reviewIndex:I

    move/from16 v21, v0

    move/from16 v0, v21

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v3, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;

    if-eqz v3, :cond_1

    .line 145
    const v3, 0x7f0701ca

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/UserReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/ImageView;

    .line 146
    .local v20, userImageView:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Lnet/flixster/android/model/User;->getThumbnailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 147
    .local v8, bitmap:Landroid/graphics/Bitmap;
    if-eqz v8, :cond_0

    .line 148
    move-object/from16 v0, v20

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 151
    :cond_0
    const v3, 0x7f0701cb

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/UserReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 152
    .local v17, nameView:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->user:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 155
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 156
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3}, Landroid/widget/ViewFlipper;->removeAllViews()V

    .line 157
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/UserReviewPage;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v11

    .line 158
    .local v11, inflater:Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v21

    :goto_0
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 197
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    move-object/from16 v0, p0

    iget v7, v0, Lnet/flixster/android/UserReviewPage;->reviewIndex:I

    invoke-virtual {v3, v7}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    .line 198
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/UserReviewPage;->getPageTitle()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lnet/flixster/android/UserReviewPage;->setActionBarTitle(Ljava/lang/String;)V

    .line 201
    .end local v8           #bitmap:Landroid/graphics/Bitmap;
    .end local v11           #inflater:Landroid/view/LayoutInflater;
    .end local v17           #nameView:Landroid/widget/TextView;
    .end local v20           #userImageView:Landroid/widget/ImageView;
    :cond_1
    return-void

    .line 158
    .restart local v8       #bitmap:Landroid/graphics/Bitmap;
    .restart local v11       #inflater:Landroid/view/LayoutInflater;
    .restart local v17       #nameView:Landroid/widget/TextView;
    .restart local v20       #userImageView:Landroid/widget/ImageView;
    :cond_2
    invoke-interface/range {v21 .. v21}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lnet/flixster/android/model/Review;

    .line 159
    .local v19, review:Lnet/flixster/android/model/Review;
    invoke-virtual/range {v19 .. v19}, Lnet/flixster/android/model/Review;->getMovie()Lnet/flixster/android/model/Movie;

    move-result-object v4

    .line 160
    .local v4, movie:Lnet/flixster/android/model/Movie;
    const v3, 0x7f030060

    move-object/from16 v0, p0

    iget-object v7, v0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    const/16 v22, 0x0

    move/from16 v0, v22

    invoke-virtual {v11, v3, v7, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ScrollView;

    .line 161
    .local v12, item:Landroid/widget/ScrollView;
    const v3, 0x7f0701bd

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/RelativeLayout;

    .line 162
    .local v13, movieLayout:Landroid/widget/RelativeLayout;
    invoke-virtual {v13, v4}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 163
    const/4 v3, 0x1

    invoke-virtual {v13, v3}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 164
    move-object/from16 v0, p0

    invoke-virtual {v13, v0}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    const v3, 0x7f0701be

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 166
    .local v6, posterView:Landroid/widget/ImageView;
    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 167
    iget-object v3, v4, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 168
    iget-object v3, v4, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 179
    :goto_1
    const/4 v3, 0x3

    new-array v14, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v7, "title"

    aput-object v7, v14, v3

    const/4 v3, 0x1

    const-string v7, "MOVIE_ACTORS_SHORT"

    aput-object v7, v14, v3

    const/4 v3, 0x2

    const-string v7, "meta"

    aput-object v7, v14, v3

    .line 180
    .local v14, movieProperties:[Ljava/lang/String;
    const/4 v3, 0x3

    new-array v0, v3, [I

    move-object/from16 v16, v0

    fill-array-data v16, :array_0

    .line 182
    .local v16, movieTextViewIds:[I
    const/4 v10, 0x0

    .local v10, i:I
    :goto_2
    array-length v3, v14

    if-lt v10, v3, :cond_5

    .line 191
    const v3, 0x7f0701c4

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    .line 192
    .local v18, ratingView:Landroid/widget/ImageView;
    sget-object v3, Lnet/flixster/android/Flixster;->RATING_LARGE_R:[I

    move-object/from16 v0, v19

    iget-wide v0, v0, Lnet/flixster/android/model/Review;->stars:D

    move-wide/from16 v22, v0

    const-wide/high16 v24, 0x4000

    mul-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-int v7, v0

    aget v3, v3, v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 193
    const v3, 0x7f0701c5

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 194
    .local v9, commentView:Landroid/widget/TextView;
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v3, v12}, Landroid/widget/ViewFlipper;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 170
    .end local v9           #commentView:Landroid/widget/TextView;
    .end local v10           #i:I
    .end local v14           #movieProperties:[Ljava/lang/String;
    .end local v16           #movieTextViewIds:[I
    .end local v18           #ratingView:Landroid/widget/ImageView;
    :cond_3
    const-string v3, "thumbnail"

    invoke-virtual {v4, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 171
    .local v5, thumbnailUrl:Ljava/lang/String;
    if-eqz v5, :cond_4

    const-string v3, "http"

    invoke-virtual {v5, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 172
    new-instance v2, Lnet/flixster/android/model/ImageOrder;

    const/4 v3, 0x0

    .line 173
    move-object/from16 v0, p0

    iget-object v7, v0, Lnet/flixster/android/UserReviewPage;->movieImageHandler:Landroid/os/Handler;

    .line 172
    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 174
    .local v2, imageOrder:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lnet/flixster/android/UserReviewPage;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_1

    .line 176
    .end local v2           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_4
    const v3, 0x7f02014f

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 183
    .end local v5           #thumbnailUrl:Ljava/lang/String;
    .restart local v10       #i:I
    .restart local v14       #movieProperties:[Ljava/lang/String;
    .restart local v16       #movieTextViewIds:[I
    :cond_5
    aget v3, v16, v10

    invoke-virtual {v12, v3}, Landroid/widget/ScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v15

    check-cast v15, Landroid/widget/TextView;

    .line 184
    .local v15, movieTextView:Landroid/widget/TextView;
    aget-object v3, v14, v10

    invoke-virtual {v4, v3}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 185
    aget-object v3, v14, v10

    invoke-virtual {v4, v3}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    const/4 v3, 0x0

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 182
    :goto_3
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_2

    .line 188
    :cond_6
    const/16 v3, 0x8

    invoke-virtual {v15, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    .line 180
    nop

    :array_0
    .array-data 0x4
        0xbft 0x1t 0x7t 0x7ft
        0xc0t 0x1t 0x7t 0x7ft
        0xc1t 0x1t 0x7t 0x7ft
    .end array-data
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .parameter "view"

    .prologue
    .line 304
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 306
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 307
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_0

    .line 308
    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v1

    invoke-static {v1, v2, p0}, Lnet/flixster/android/Starter;->launchMovieDetail(JLandroid/content/Context;)V

    goto :goto_0

    .line 304
    :pswitch_data_0
    .packed-switch 0x7f0701bd
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .parameter "savedState"

    .prologue
    const/16 v2, 0x8

    .line 56
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lnet/flixster/android/UserReviewPage;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 59
    const-string v1, "REVIEW_TYPE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lnet/flixster/android/UserReviewPage;->reviewType:I

    .line 60
    const-string v1, "REVIEW_INDEX"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lnet/flixster/android/UserReviewPage;->reviewIndex:I

    .line 62
    :cond_0
    const v1, 0x7f030061

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->setContentView(I)V

    .line 63
    const v1, 0x7f0701c6

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 64
    const v1, 0x7f0701c8

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 65
    const v1, 0x7f0701c7

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 66
    const v1, 0x7f0701c9

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 67
    invoke-virtual {p0}, Lnet/flixster/android/UserReviewPage;->createActionBar()V

    .line 68
    const v1, 0x7f0c005b

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->setActionBarTitle(I)V

    .line 70
    const v1, 0x7f0701cc

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ViewFlipper;

    iput-object v1, p0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    .line 71
    const v1, 0x7f070039

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/UserReviewPage;->throbber:Landroid/view/View;

    .line 72
    invoke-direct {p0}, Lnet/flixster/android/UserReviewPage;->updatePage()V

    .line 73
    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 3
    .parameter "dialogId"

    .prologue
    .line 249
    packed-switch p1, :pswitch_data_0

    .line 268
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v1

    :goto_0
    return-object v1

    .line 251
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 252
    .local v0, alertBuilder:Landroid/app/AlertDialog$Builder;
    const-string v1, "The network connection failed. Press Retry to make another attempt."

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 253
    const-string v1, "Network Error"

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 254
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 255
    const-string v1, "Retry"

    new-instance v2, Lnet/flixster/android/UserReviewPage$4;

    invoke-direct {v2, p0}, Lnet/flixster/android/UserReviewPage$4;-><init>(Lnet/flixster/android/UserReviewPage;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 260
    invoke-virtual {p0}, Lnet/flixster/android/UserReviewPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c004a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 261
    new-instance v2, Lnet/flixster/android/UserReviewPage$5;

    invoke-direct {v2, p0}, Lnet/flixster/android/UserReviewPage$5;-><init>(Lnet/flixster/android/UserReviewPage;)V

    .line 260
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 266
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Lcom/actionbarsherlock/view/Menu;)Z
    .locals 2
    .parameter "menu"

    .prologue
    .line 274
    invoke-virtual {p0}, Lnet/flixster/android/UserReviewPage;->getSupportMenuInflater()Lcom/actionbarsherlock/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0f000e

    invoke-virtual {v0, v1, p1}, Lcom/actionbarsherlock/view/MenuInflater;->inflate(ILcom/actionbarsherlock/view/Menu;)V

    .line 275
    const/4 v0, 0x1

    return v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onDestroy()V

    .line 90
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 92
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->purge()I

    .line 94
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/UserReviewPage;->timer:Ljava/util/Timer;

    .line 95
    return-void
.end method

.method public onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z
    .locals 3
    .parameter "item"

    .prologue
    const/4 v0, 0x1

    .line 280
    invoke-interface {p1}, Lcom/actionbarsherlock/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 294
    invoke-super {p0, p1}, Lnet/flixster/android/FlixsterActivity;->onOptionsItemSelected(Lcom/actionbarsherlock/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 282
    :pswitch_0
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    const v2, 0x7f040008

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 283
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    const v2, 0x7f040009

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 284
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->showPrevious()V

    .line 285
    invoke-direct {p0}, Lnet/flixster/android/UserReviewPage;->getPageTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->setActionBarTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 288
    :pswitch_1
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    const v2, 0x7f040006

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 289
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    const v2, 0x7f040007

    invoke-static {p0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ViewFlipper;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 290
    iget-object v1, p0, Lnet/flixster/android/UserReviewPage;->viewFlipper:Landroid/widget/ViewFlipper;

    invoke-virtual {v1}, Landroid/widget/ViewFlipper;->showNext()V

    .line 291
    invoke-direct {p0}, Lnet/flixster/android/UserReviewPage;->getPageTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/UserReviewPage;->setActionBarTitle(Ljava/lang/String;)V

    goto :goto_0

    .line 280
    nop

    :pswitch_data_0
    .packed-switch 0x7f07030b
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Lnet/flixster/android/FlixsterActivity;->onResume()V

    .line 78
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->timer:Ljava/util/Timer;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/UserReviewPage;->timer:Ljava/util/Timer;

    .line 81
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnet/flixster/android/UserReviewPage;->reviews:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 82
    :cond_1
    invoke-direct {p0}, Lnet/flixster/android/UserReviewPage;->showLoading()V

    .line 83
    invoke-direct {p0}, Lnet/flixster/android/UserReviewPage;->scheduleUpdatePageTask()V

    .line 85
    :cond_2
    return-void
.end method
