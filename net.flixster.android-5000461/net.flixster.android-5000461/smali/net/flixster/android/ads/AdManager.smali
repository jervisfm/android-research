.class public Lnet/flixster/android/ads/AdManager;
.super Ljava/lang/Object;
.source "AdManager.java"


# static fields
.field private static synthetic $SWITCH_TABLE$net$flixster$android$ads$model$FlixsterAd$AdTarget:[I = null

.field public static final ADCONTEXT_ID:Ljava/lang/String; = "ADCONTEXT_ID"

.field public static final CM_ACTOR_DETAIL:Ljava/lang/String; = "ActorDetails"

.field public static final CM_BOX_OFFICE:Ljava/lang/String; = "MoviesTab"

.field public static final CM_DVD:Ljava/lang/String; = "DVDTab"

.field public static final CM_DVD_CATEGORY:Ljava/lang/String; = "Category"

.field public static final CM_FRIENDRATINGS:Ljava/lang/String; = "FriendRatings"

.field public static final CM_HOMEPAGE:Ljava/lang/String; = "Homepage"

.field public static final CM_INTERSTITIALTRAILER:Ljava/lang/String; = "InterstitialTrailer"

.field public static final CM_LAUNCHAD:Ljava/lang/String; = "LaunchAd"

.field public static final CM_MOVIESHOWTIMES:Ljava/lang/String; = "MovieShowtimes"

.field public static final CM_MOVIESIVERATED:Ljava/lang/String; = "MoviesIveRated"

.field public static final CM_MOVIESIWTS:Ljava/lang/String; = "MoviesIWantToSee"

.field public static final CM_MOVIE_DETAIL_DVD:Ljava/lang/String; = "MovieDetailsDvd"

.field public static final CM_MOVIE_DETAIL_PLAYING:Ljava/lang/String; = "MovieDetailsInTheaters"

.field public static final CM_MOVIE_DETAIL_UPCOMING:Ljava/lang/String; = "MovieDetailsUpcoming"

.field public static final CM_MYFRIENDS:Ljava/lang/String; = "MyFriends"

.field public static final CM_PREROLL:Ljava/lang/String; = "Preroll"

.field public static final CM_SEARCHRESULTS:Ljava/lang/String; = "SearchResults"

.field public static final CM_THEATERINFO:Ljava/lang/String; = "TheaterInfo"

.field public static final CM_THEATERS:Ljava/lang/String; = "TheatersTab"

.field public static final CM_TOPACTORS:Ljava/lang/String; = "TopActors"

.field public static final CM_TOPNEWS:Ljava/lang/String; = "TopNews"

.field public static final CM_TOPPHOTOS:Ljava/lang/String; = "TopPhotos"

.field public static final CM_TRAILERBG:Ljava/lang/String; = "TrailerBG"

.field public static final CM_UPCOMING:Ljava/lang/String; = "UpcomingTab"

.field public static final EVENT_CLICK:Ljava/lang/String; = "Click"

.field public static final EVENT_SKIP:Ljava/lang/String; = "Skip"

.field public static final EVENT_VIEW:Ljava/lang/String; = "Impression"

.field private static instance:Lnet/flixster/android/ads/AdManager;


# instance fields
.field private final adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

.field private final isDisabled:Z


# direct methods
.method static synthetic $SWITCH_TABLE$net$flixster$android$ads$model$FlixsterAd$AdTarget()[I
    .locals 3

    .prologue
    .line 23
    sget-object v0, Lnet/flixster/android/ads/AdManager;->$SWITCH_TABLE$net$flixster$android$ads$model$FlixsterAd$AdTarget:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->values()[Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->HTML:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    invoke-virtual {v1}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->MOVIE:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    invoke-virtual {v1}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->PRODUCT:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    invoke-virtual {v1}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->TRAILER:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    invoke-virtual {v1}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->URL:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    invoke-virtual {v1}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lnet/flixster/android/ads/AdManager;->$SWITCH_TABLE$net$flixster$android$ads$model$FlixsterAd$AdTarget:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    .line 73
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/ads/AdManager;->isDisabled:Z

    .line 78
    :goto_0
    return-void

    .line 75
    :cond_1
    new-instance v0, Lnet/flixster/android/ads/data/GreedyAdsDao;

    invoke-direct {v0}, Lnet/flixster/android/ads/data/GreedyAdsDao;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    .line 76
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/ads/AdManager;->isDisabled:Z

    goto :goto_0
.end method

.method public static instance()Lnet/flixster/android/ads/AdManager;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lnet/flixster/android/ads/AdManager;->instance:Lnet/flixster/android/ads/AdManager;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lnet/flixster/android/ads/AdManager;

    invoke-direct {v0}, Lnet/flixster/android/ads/AdManager;-><init>()V

    sput-object v0, Lnet/flixster/android/ads/AdManager;->instance:Lnet/flixster/android/ads/AdManager;

    .line 84
    :cond_0
    sget-object v0, Lnet/flixster/android/ads/AdManager;->instance:Lnet/flixster/android/ads/AdManager;

    return-object v0
.end method

.method public static onFlixsterAdClick(Lnet/flixster/android/ads/model/FlixsterAd;Landroid/content/Context;)V
    .locals 2
    .parameter "ad"
    .parameter "context"

    .prologue
    .line 181
    iget-object v0, p0, Lnet/flixster/android/ads/model/FlixsterAd;->targetType:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    if-eqz v0, :cond_1

    .line 182
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v0

    const-string v1, "Click"

    invoke-virtual {v0, p0, v1}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    .line 183
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->$SWITCH_TABLE$net$flixster$android$ads$model$FlixsterAd$AdTarget()[I

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/ads/model/FlixsterAd;->targetType:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    invoke-virtual {v1}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 202
    :goto_0
    return-void

    .line 185
    :pswitch_0
    iget-object v0, p0, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1, p1}, Lnet/flixster/android/Starter;->launchMovieDetail(JLandroid/content/Context;)V

    goto :goto_0

    .line 188
    :pswitch_1
    iget-object v0, p0, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    invoke-static {v0, p1}, Lnet/flixster/android/Starter;->launchAdFreeTrailer(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 192
    :pswitch_2
    iget-object v0, p0, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    invoke-static {v0}, Lcom/flixster/android/activity/DeepLink;->isValid(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/flixster/android/activity/DeepLink;->launch(Ljava/lang/String;Landroid/content/Context;)Z

    goto :goto_0

    .line 195
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    invoke-static {v0, p1}, Lnet/flixster/android/Starter;->launchBrowser(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_0

    .line 200
    :cond_1
    const-string v0, "FlxAd"

    const-string v1, "AdManager.onFlixsterAdClick target null"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public addDaoListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 92
    iget-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    invoke-interface {v0, p1}, Lnet/flixster/android/ads/data/GenericAdsDao;->addListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V

    .line 95
    :cond_0
    return-void
.end method

.method public getAd(Ljava/lang/String;)Lnet/flixster/android/ads/model/Ad;
    .locals 1
    .parameter "placement"

    .prologue
    const/4 v0, 0x0

    .line 104
    invoke-virtual {p0, p1, v0, v0}, Lnet/flixster/android/ads/AdManager;->getAd(Ljava/lang/String;Landroid/os/Handler;Ljava/lang/Long;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    return-object v0
.end method

.method public getAd(Ljava/lang/String;Landroid/os/Handler;Ljava/lang/Long;)Lnet/flixster/android/ads/model/Ad;
    .locals 7
    .parameter "placement"
    .parameter "handler"
    .parameter "contextId"

    .prologue
    const/4 v2, 0x0

    .line 109
    iget-boolean v3, p0, Lnet/flixster/android/ads/AdManager;->isDisabled:Z

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 126
    :cond_0
    :goto_0
    return-object v0

    .line 113
    :cond_1
    const/4 v0, 0x0

    .line 114
    .local v0, ad:Lnet/flixster/android/ads/model/Ad;
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-eqz v3, :cond_2

    .line 115
    iget-object v3, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4, p2}, Lnet/flixster/android/ads/data/GenericAdsDao;->find(Ljava/lang/String;Landroid/os/Handler;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    .line 117
    :cond_2
    if-nez v0, :cond_3

    .line 118
    iget-object v3, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    invoke-interface {v3, p1, p2}, Lnet/flixster/android/ads/data/GenericAdsDao;->find(Ljava/lang/String;Landroid/os/Handler;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    .line 120
    :cond_3
    if-eqz v0, :cond_4

    .line 121
    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    .line 122
    .local v1, now:Ljava/util/Date;
    iget-object v3, v0, Lnet/flixster/android/ads/model/Ad;->start:Ljava/util/Date;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lnet/flixster/android/ads/model/Ad;->end:Ljava/util/Date;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lnet/flixster/android/ads/model/Ad;->start:Ljava/util/Date;

    invoke-virtual {v3, v1}, Ljava/util/Date;->before(Ljava/util/Date;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, v0, Lnet/flixster/android/ads/model/Ad;->end:Ljava/util/Date;

    invoke-virtual {v3, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-nez v3, :cond_0

    .end local v1           #now:Ljava/util/Date;
    :cond_4
    move-object v0, v2

    .line 126
    goto :goto_0
.end method

.method public getPayloadDump()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-boolean v0, p0, Lnet/flixster/android/ads/AdManager;->isDisabled:Z

    if-eqz v0, :cond_0

    .line 168
    const-string v0, "Ads disabled"

    .line 171
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    invoke-interface {v0}, Lnet/flixster/android/ads/data/GenericAdsDao;->getPayloadDump()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isActiveAd(Ljava/lang/String;Ljava/lang/Long;)Z
    .locals 3
    .parameter "placement"
    .parameter "contextId"

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-boolean v2, p0, Lnet/flixster/android/ads/AdManager;->isDisabled:Z

    if-eqz v2, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v1

    .line 135
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v2, p2}, Lnet/flixster/android/ads/AdManager;->getAd(Ljava/lang/String;Landroid/os/Handler;Ljava/lang/Long;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    .line 136
    .local v0, ad:Lnet/flixster/android/ads/model/Ad;
    if-eqz v0, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    invoke-interface {v0}, Lnet/flixster/android/ads/data/GenericAdsDao;->isInitialized()Z

    move-result v0

    goto :goto_0
.end method

.method protected isTabPlacement(Ljava/lang/String;)Z
    .locals 1
    .parameter "p"

    .prologue
    .line 175
    const-string v0, "Homepage"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "MoviesTab"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "TheatersTab"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "UpcomingTab"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    const-string v0, "DVDTab"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Category"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    .line 175
    goto :goto_0
.end method

.method public removeDaoListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V
    .locals 1
    .parameter "listener"

    .prologue
    .line 98
    iget-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    invoke-interface {v0, p1}, Lnet/flixster/android/ads/data/GenericAdsDao;->removeListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V

    .line 101
    :cond_0
    return-void
.end method

.method public trackEvent(ILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .parameter "tag"
    .parameter "event"
    .parameter "placement"

    .prologue
    .line 157
    iget-boolean v1, p0, Lnet/flixster/android/ads/AdManager;->isDisabled:Z

    if-eqz v1, :cond_0

    .line 164
    :goto_0
    return-void

    .line 161
    :cond_0
    invoke-static {p1, p2, p3}, Lnet/flixster/android/data/ApiBuilder;->adTrack(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, url:Ljava/lang/String;
    const-string v1, "FlxAd"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AdManager.trackEvent tag:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " event:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " placement:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method public trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V
    .locals 2
    .parameter "ad"
    .parameter "event"

    .prologue
    .line 141
    iget-boolean v0, p0, Lnet/flixster/android/ads/AdManager;->isDisabled:Z

    if-eqz v0, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    if-eqz p1, :cond_0

    iget-object v0, p1, Lnet/flixster/android/ads/model/TaggableAd;->tag:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p1, Lnet/flixster/android/ads/model/TaggableAd;->cap:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p1, Lnet/flixster/android/ads/model/Ad;->cap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p1, Lnet/flixster/android/ads/model/TaggableAd;->cap:Ljava/lang/Integer;

    .line 148
    iget-object v0, p1, Lnet/flixster/android/ads/model/TaggableAd;->cap:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    .line 149
    iget-object v0, p0, Lnet/flixster/android/ads/AdManager;->adsDao:Lnet/flixster/android/ads/data/GenericAdsDao;

    invoke-interface {v0, p1}, Lnet/flixster/android/ads/data/GenericAdsDao;->delete(Lnet/flixster/android/ads/model/Ad;)V

    .line 151
    :cond_2
    iget-object v0, p1, Lnet/flixster/android/ads/model/TaggableAd;->tag:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p1, Lnet/flixster/android/ads/model/TaggableAd;->placement:Ljava/lang/String;

    invoke-virtual {p0, v0, p2, v1}, Lnet/flixster/android/ads/AdManager;->trackEvent(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
