.class public Lnet/flixster/android/ads/AdUtils;
.super Ljava/lang/Object;
.source "AdUtils.java"


# static fields
.field private static final DEFAULT_KEYWORDS:Ljava/lang/String; = "movie,watch+movies,movie+releases,free+movies,buy+movies,dvd+releases,films"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static parseAd(Lorg/json/JSONObject;)Lnet/flixster/android/ads/model/Ad;
    .locals 28
    .parameter "jsonObject"

    .prologue
    .line 47
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 48
    .local v14, otherHash:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v23, "other"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 49
    const-string v23, "other"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 50
    .local v13, other:Ljava/lang/String;
    const-string v23, "&"

    move-object/from16 v0, v23

    invoke-virtual {v13, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    array-length v0, v0

    move/from16 v25, v0

    const/16 v23, 0x0

    :goto_0
    move/from16 v0, v23

    move/from16 v1, v25

    if-lt v0, v1, :cond_3

    .line 59
    .end local v13           #other:Ljava/lang/String;
    :cond_0
    const-wide/32 v10, 0xa4cb800

    .line 60
    .local v10, installDelay:J
    const-string v23, "installdelay"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1

    .line 61
    const-string v23, "installdelay"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    invoke-static/range {v23 .. v23}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 63
    :cond_1
    sget-wide v23, Lnet/flixster/android/FlixsterApplication;->sInstallMsPosixTime:J

    add-long v23, v23, v10

    sget-object v25, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual/range {v25 .. v25}, Ljava/util/Date;->getTime()J

    move-result-wide v25

    cmp-long v23, v23, v25

    if-lez v23, :cond_5

    .line 64
    const/4 v3, 0x0

    .line 221
    :cond_2
    :goto_1
    return-object v3

    .line 50
    .end local v10           #installDelay:J
    .restart local v13       #other:Ljava/lang/String;
    :cond_3
    aget-object v16, v24, v23

    .line 51
    .local v16, param:Ljava/lang/String;
    const-string v26, "="

    move-object/from16 v0, v16

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 52
    .local v15, pair:[Ljava/lang/String;
    array-length v0, v15

    move/from16 v26, v0

    const/16 v27, 0x2

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_4

    const/16 v26, 0x0

    aget-object v26, v15, v26

    if-eqz v26, :cond_4

    const/16 v26, 0x1

    aget-object v26, v15, v26

    if-eqz v26, :cond_4

    .line 53
    const/16 v26, 0x0

    aget-object v26, v15, v26

    const/16 v27, 0x1

    aget-object v27, v15, v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v14, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    :cond_4
    add-int/lit8 v23, v23, 0x1

    goto :goto_0

    .line 69
    .end local v13           #other:Ljava/lang/String;
    .end local v15           #pair:[Ljava/lang/String;
    .end local v16           #param:Ljava/lang/String;
    .restart local v10       #installDelay:J
    :cond_5
    const-string v23, "placement"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 70
    .local v17, placement:Ljava/lang/String;
    const-string v23, "target"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 71
    .local v22, target:Ljava/lang/String;
    const-string v23, "AdMob"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_c

    .line 72
    new-instance v12, Lnet/flixster/android/ads/model/AdMobAd;

    invoke-direct {v12}, Lnet/flixster/android/ads/model/AdMobAd;-><init>()V

    .line 74
    .local v12, mad:Lnet/flixster/android/ads/model/AdMobAd;
    const-string v23, "adSenseChannel"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_6

    .line 75
    const-string v23, "adSenseChannel"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v12, Lnet/flixster/android/ads/model/AdMobAd;->channelId:Ljava/lang/String;

    .line 78
    :cond_6
    const-string v23, "adSenseKeywords"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_7

    .line 79
    const-string v23, "adSenseKeywords"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v12, Lnet/flixster/android/ads/model/AdMobAd;->keywords:Ljava/lang/String;

    .line 82
    :cond_7
    iget-object v0, v12, Lnet/flixster/android/ads/model/AdMobAd;->keywords:Ljava/lang/String;

    move-object/from16 v23, v0

    if-eqz v23, :cond_8

    iget-object v0, v12, Lnet/flixster/android/ads/model/AdMobAd;->keywords:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v23

    if-gtz v23, :cond_9

    .line 83
    :cond_8
    const-string v23, "movie,watch+movies,movie+releases,free+movies,buy+movies,dvd+releases,films"

    move-object/from16 v0, v23

    iput-object v0, v12, Lnet/flixster/android/ads/model/AdMobAd;->keywords:Ljava/lang/String;

    .line 85
    :cond_9
    move-object v3, v12

    .line 189
    .end local v12           #mad:Lnet/flixster/android/ads/model/AdMobAd;
    .local v3, ad:Lnet/flixster/android/ads/model/Ad;
    :goto_2
    move-object/from16 v0, v22

    iput-object v0, v3, Lnet/flixster/android/ads/model/Ad;->target:Ljava/lang/String;

    .line 190
    move-object/from16 v0, v17

    iput-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    .line 194
    const-string v23, "cap"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 195
    .local v5, cap:Ljava/lang/String;
    if-eqz v5, :cond_a

    const-string v23, "LaunchAd"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_a

    .line 196
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v3, Lnet/flixster/android/ads/model/Ad;->cap:Ljava/lang/Integer;

    .line 199
    :cond_a
    const-string v23, "sticky"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    move-object/from16 v0, v23

    iput-object v0, v3, Lnet/flixster/android/ads/model/Ad;->sticky:Ljava/lang/String;

    if-eqz v23, :cond_b

    .line 201
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "MoviePhotos"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "PhotoGallery"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    .line 202
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "TicketInfo"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "ActorDetails"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    .line 203
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "ActorBio"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "BuyTickets"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    .line 204
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "TopPhotos"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "FriendRatings"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    .line 205
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "MoviesIveRated"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "MyFriends"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    .line 206
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "FriendReview"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "MyReview"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    .line 207
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "TrailerBG"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_b

    .line 208
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->sticky:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "top"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_24

    .line 209
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v23, "StickyTop"

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    .line 216
    :cond_b
    :goto_3
    const-string v23, "contextId"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_2

    .line 217
    const-string v23, "contextId"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v23

    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v3, Lnet/flixster/android/ads/model/Ad;->contextId:Ljava/lang/Long;

    .line 218
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->contextId:Ljava/lang/Long;

    move-object/from16 v23, v0

    invoke-virtual/range {v23 .. v23}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    goto/16 :goto_1

    .line 86
    .end local v3           #ad:Lnet/flixster/android/ads/model/Ad;
    .end local v5           #cap:Ljava/lang/String;
    :cond_c
    const-string v23, "DART"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_12

    .line 87
    const-string v23, "Preroll"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_e

    .line 88
    new-instance v20, Lnet/flixster/android/ads/model/DfpVideoAd;

    invoke-direct/range {v20 .. v20}, Lnet/flixster/android/ads/model/DfpVideoAd;-><init>()V

    .line 89
    .local v20, tad:Lnet/flixster/android/ads/model/DfpVideoAd;
    const-string v23, "tag"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 90
    .local v21, tag:Ljava/lang/String;
    if-eqz v21, :cond_d

    .line 91
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    move-object/from16 v1, v20

    iput-object v0, v1, Lnet/flixster/android/ads/model/DfpVideoAd;->tag:Ljava/lang/Integer;

    .line 93
    :cond_d
    move-object/from16 v3, v20

    .restart local v3       #ad:Lnet/flixster/android/ads/model/Ad;
    goto/16 :goto_2

    .line 94
    .end local v3           #ad:Lnet/flixster/android/ads/model/Ad;
    .end local v20           #tad:Lnet/flixster/android/ads/model/DfpVideoAd;
    .end local v21           #tag:Ljava/lang/String;
    :cond_e
    const-string v23, "LaunchAd"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-nez v23, :cond_f

    .line 95
    const-string v23, "InterstitialTrailer"

    move-object/from16 v0, v17

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_10

    .line 96
    :cond_f
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 98
    :cond_10
    new-instance v6, Lnet/flixster/android/ads/model/DfpAd;

    invoke-direct {v6}, Lnet/flixster/android/ads/model/DfpAd;-><init>()V

    .line 99
    .local v6, dfp:Lnet/flixster/android/ads/model/DfpAd;
    const-string v23, "tag"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 100
    .restart local v21       #tag:Ljava/lang/String;
    if-eqz v21, :cond_11

    .line 101
    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v6, Lnet/flixster/android/ads/model/DfpAd;->tag:Ljava/lang/Integer;

    .line 104
    :cond_11
    move-object v3, v6

    .restart local v3       #ad:Lnet/flixster/android/ads/model/Ad;
    goto/16 :goto_2

    .line 106
    .end local v3           #ad:Lnet/flixster/android/ads/model/Ad;
    .end local v6           #dfp:Lnet/flixster/android/ads/model/DfpAd;
    .end local v21           #tag:Ljava/lang/String;
    :cond_12
    const-string v23, "AdSense"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_18

    .line 107
    new-instance v9, Lnet/flixster/android/ads/model/GoogleAd;

    invoke-direct {v9}, Lnet/flixster/android/ads/model/GoogleAd;-><init>()V

    .line 109
    .local v9, gad:Lnet/flixster/android/ads/model/GoogleAd;
    const-string v23, "adSenseKeywords"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_14

    .line 110
    const-string v23, "adSenseKeywords"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->keywords:Ljava/lang/String;

    .line 117
    :goto_4
    const-string v23, "adSenseChannel"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_16

    .line 118
    const-string v23, "adSenseChannel"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->channelId:Ljava/lang/String;

    .line 124
    :goto_5
    :try_start_0
    const-string v23, "bcolor"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_17

    .line 125
    const-string v23, "bcolor"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    const-string v24, "UTF-8"

    invoke-static/range {v23 .. v24}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->bcolor:Ljava/lang/String;

    .line 130
    :goto_6
    const-string v23, "fcolor"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_13

    .line 131
    const-string v23, "fcolor"

    move-object/from16 v0, v23

    invoke-virtual {v14, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/String;

    const-string v24, "UTF-8"

    invoke-static/range {v23 .. v24}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->fcolor:Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 137
    :cond_13
    :goto_7
    const-string v23, "adSenseWebURL"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->webUrl:Ljava/lang/String;

    .line 139
    move-object v3, v9

    .restart local v3       #ad:Lnet/flixster/android/ads/model/Ad;
    goto/16 :goto_2

    .line 111
    .end local v3           #ad:Lnet/flixster/android/ads/model/Ad;
    :cond_14
    const-string v23, "adsenseKeywords"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_15

    .line 112
    const-string v23, "adsenseKeywords"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->keywords:Ljava/lang/String;

    goto/16 :goto_4

    .line 114
    :cond_15
    const-string v23, "movie,watch+movies,movie+releases,free+movies,buy+movies,dvd+releases,films"

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->keywords:Ljava/lang/String;

    goto/16 :goto_4

    .line 120
    :cond_16
    const-string v23, "520867217"

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->channelId:Ljava/lang/String;

    goto/16 :goto_5

    .line 127
    :cond_17
    :try_start_1
    const-string v23, "#525152"

    move-object/from16 v0, v23

    iput-object v0, v9, Lnet/flixster/android/ads/model/GoogleAd;->bcolor:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_6

    .line 133
    :catch_0
    move-exception v7

    .line 135
    .local v7, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v7}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_7

    .line 140
    .end local v7           #e:Ljava/io/UnsupportedEncodingException;
    .end local v9           #gad:Lnet/flixster/android/ads/model/GoogleAd;
    :cond_18
    const-string v23, "MobClix"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_19

    .line 141
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 142
    :cond_19
    const-string v23, "web"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_1a

    .line 143
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 144
    :cond_1a
    const-string v23, "Medialets"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_1b

    .line 145
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 146
    :cond_1b
    const-string v23, "transpera"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_1c

    .line 147
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 149
    :cond_1c
    new-instance v8, Lnet/flixster/android/ads/model/FlixsterAd;

    invoke-direct {v8}, Lnet/flixster/android/ads/model/FlixsterAd;-><init>()V

    .line 151
    .local v8, fad:Lnet/flixster/android/ads/model/FlixsterAd;
    const-string v23, "\\|"

    invoke-virtual/range {v22 .. v23}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 152
    .local v4, attr:[Ljava/lang/String;
    if-eqz v4, :cond_1e

    array-length v0, v4

    move/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_1e

    .line 153
    const/16 v23, 0x1

    aget-object v23, v4, v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    .line 154
    iget-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "www"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v23

    if-eqz v23, :cond_1d

    .line 155
    new-instance v23, Ljava/lang/StringBuilder;

    const-string v24, "http://"

    invoke-direct/range {v23 .. v24}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    move-object/from16 v24, v0

    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    .line 157
    :cond_1d
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->MOVIE:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->literal:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v24, v4, v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_20

    .line 158
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->MOVIE:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->targetType:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    .line 171
    :cond_1e
    :goto_8
    const-string v23, "tag"

    const-string v24, "0"

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->tag:Ljava/lang/Integer;

    .line 172
    const-string v23, "image"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->imageUrl:Ljava/lang/String;

    .line 174
    const-string v23, "size"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 175
    .local v18, size:Ljava/lang/String;
    if-eqz v18, :cond_1f

    const-string v23, "x"

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_1f

    .line 176
    const-string v23, "x"

    move-object/from16 v0, v18

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 177
    .local v19, sizes:[Ljava/lang/String;
    const/16 v23, 0x0

    aget-object v23, v19, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->width:Ljava/lang/Integer;

    .line 178
    const/16 v23, 0x1

    aget-object v23, v19, v23

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->height:Ljava/lang/Integer;

    .line 182
    .end local v19           #sizes:[Ljava/lang/String;
    :cond_1f
    const-string v23, "button1"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->button1:Ljava/lang/String;

    .line 183
    const-string v23, "button2"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->button2:Ljava/lang/String;

    .line 185
    move-object v3, v8

    .restart local v3       #ad:Lnet/flixster/android/ads/model/Ad;
    goto/16 :goto_2

    .line 159
    .end local v3           #ad:Lnet/flixster/android/ads/model/Ad;
    .end local v18           #size:Ljava/lang/String;
    :cond_20
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->URL:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->literal:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v24, v4, v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_21

    .line 160
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->URL:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->targetType:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    goto/16 :goto_8

    .line 161
    :cond_21
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->TRAILER:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->literal:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v24, v4, v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_22

    .line 162
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->TRAILER:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->targetType:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    goto/16 :goto_8

    .line 163
    :cond_22
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->PRODUCT:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->literal:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v24, v4, v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_23

    .line 164
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->PRODUCT:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->targetType:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    goto/16 :goto_8

    .line 165
    :cond_23
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->HTML:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->literal:Ljava/lang/String;

    move-object/from16 v23, v0

    const/16 v24, 0x0

    aget-object v24, v4, v24

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_1e

    .line 166
    sget-object v23, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->HTML:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->targetType:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    .line 167
    const-string v23, "title"

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v8, Lnet/flixster/android/ads/model/FlixsterAd;->title:Ljava/lang/String;

    goto/16 :goto_8

    .line 210
    .end local v4           #attr:[Ljava/lang/String;
    .end local v8           #fad:Lnet/flixster/android/ads/model/FlixsterAd;
    .restart local v3       #ad:Lnet/flixster/android/ads/model/Ad;
    .restart local v5       #cap:Ljava/lang/String;
    :cond_24
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->sticky:Ljava/lang/String;

    move-object/from16 v23, v0

    const-string v24, "bottom"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v23

    if-eqz v23, :cond_b

    .line 211
    iget-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    move-object/from16 v23, v0

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-static/range {v23 .. v23}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v23, "StickyBottom"

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, v23

    iput-object v0, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    goto/16 :goto_3
.end method

.method public static parseAds(Lorg/json/JSONArray;)Ljava/util/List;
    .locals 7
    .parameter "adsArray"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/ads/model/Ad;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 32
    .local v1, ads:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/ads/model/Ad;>;"
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 33
    .local v3, numOfAds:I
    const-string v4, "FlxAd"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "AdUtils.parseAds size:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-lt v2, v3, :cond_0

    .line 40
    return-object v1

    .line 35
    :cond_0
    invoke-virtual {p0, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-static {v4}, Lnet/flixster/android/ads/AdUtils;->parseAd(Lorg/json/JSONObject;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    .line 36
    .local v0, ad:Lnet/flixster/android/ads/model/Ad;
    if-eqz v0, :cond_1

    .line 37
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 34
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static splitKeywords(Ljava/lang/String;)Ljava/util/Set;
    .locals 6
    .parameter "keywords"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 225
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 226
    .local v1, set:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v4, :cond_0

    .line 229
    return-object v1

    .line 226
    :cond_0
    aget-object v0, v3, v2

    .line 227
    .local v0, keyword:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 226
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
