.class Lnet/flixster/android/ads/AdView$1;
.super Landroid/os/Handler;
.source "AdView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ads/AdView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 305
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "message"

    .prologue
    .line 309
    :try_start_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/widget/ImageView;

    .line 310
    .local v1, adView:Landroid/widget/ImageView;
    const-string v4, "FlxAd"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "AdView.onImageLoad called! adView:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    if-eqz v1, :cond_0

    .line 312
    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/FlixsterAd;

    .line 313
    .local v0, ad:Lnet/flixster/android/ads/model/FlixsterAd;
    const-string v4, "FlxAd"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "AdView.onImageLoad adView.getTag():"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    iget-object v4, v0, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    .line 315
    const-string v4, "FlxAd"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "AdView.onImageLoad ad.bitmap:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v4, v0, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 317
    sget v4, Lnet/flixster/android/FlixsterApplication;->sRelativePixel:F

    iget-object v5, v0, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    .line 318
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    float-to-int v4, v4

    .line 317
    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 319
    iget-object v4, v0, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    iget-object v5, v0, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    iget-object v6, v0, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    .line 320
    .local v2, color:I
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 321
    invoke-virtual {v1}, Landroid/widget/ImageView;->invalidate()V

    .line 322
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v4

    const-string v5, "Impression"

    invoke-virtual {v4, v0, v5}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 329
    .end local v0           #ad:Lnet/flixster/android/ads/model/FlixsterAd;
    .end local v1           #adView:Landroid/widget/ImageView;
    .end local v2           #color:I
    :cond_0
    :goto_0
    return-void

    .line 325
    :catch_0
    move-exception v3

    .line 326
    .local v3, e:Ljava/lang/Exception;
    const-string v4, "FlxAd"

    const-string v5, "Couldn\'t set image for ad."

    invoke-static {v4, v5, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
