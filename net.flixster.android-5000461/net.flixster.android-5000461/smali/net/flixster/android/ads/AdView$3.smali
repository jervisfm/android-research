.class Lnet/flixster/android/ads/AdView$3;
.super Ljava/lang/Object;
.source "AdView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ads/AdView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ads/AdView;


# direct methods
.method constructor <init>(Lnet/flixster/android/ads/AdView;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ads/AdView$3;->this$0:Lnet/flixster/android/ads/AdView;

    .line 343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .parameter "v"

    .prologue
    .line 345
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/FlixsterAd;

    .line 346
    .local v0, ad:Lnet/flixster/android/ads/model/FlixsterAd;
    iget-object v1, p0, Lnet/flixster/android/ads/AdView$3;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lnet/flixster/android/ads/AdView;->access$0(Lnet/flixster/android/ads/AdView;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lnet/flixster/android/ads/AdManager;->onFlixsterAdClick(Lnet/flixster/android/ads/model/FlixsterAd;Landroid/content/Context;)V

    .line 347
    return-void
.end method
