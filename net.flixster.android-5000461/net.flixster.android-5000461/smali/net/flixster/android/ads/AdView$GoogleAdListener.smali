.class Lnet/flixster/android/ads/AdView$GoogleAdListener;
.super Ljava/lang/Object;
.source "AdView.java"

# interfaces
.implements Lcom/google/ads/AdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ads/AdView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GoogleAdListener"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ads/AdView;


# direct methods
.method private constructor <init>(Lnet/flixster/android/ads/AdView;)V
    .locals 0
    .parameter

    .prologue
    .line 358
    iput-object p1, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/ads/AdView;Lnet/flixster/android/ads/AdView$GoogleAdListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 358
    invoke-direct {p0, p1}, Lnet/flixster/android/ads/AdView$GoogleAdListener;-><init>(Lnet/flixster/android/ads/AdView;)V

    return-void
.end method


# virtual methods
.method public onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 2
    .parameter "gad"

    .prologue
    .line 398
    const-string v0, "FlxAd"

    const-string v1, "AdView.GoogleAdListener.onDismissScreen"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 399
    return-void
.end method

.method public onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 3
    .parameter "gad"
    .parameter "errorCode"

    .prologue
    .line 385
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AdView.GoogleAdListener.onFailedToReceiveAd errorCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->sw(Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->isTabPlacement:Z
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$2(Lnet/flixster/android/ads/AdView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->isDfpAdRetry:Z
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$3(Lnet/flixster/android/ads/AdView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    instance-of v0, v0, Lnet/flixster/android/ads/model/DfpAd;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    const/4 v1, 0x1

    #setter for: Lnet/flixster/android/ads/AdView;->isDfpAdRetry:Z
    invoke-static {v0, v1}, Lnet/flixster/android/ads/AdView;->access$4(Lnet/flixster/android/ads/AdView;Z)V

    .line 388
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 394
    :goto_0
    return-void

    .line 390
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setMinimumHeight(I)V

    .line 391
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->removeAllViews()V

    .line 392
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lnet/flixster/android/ads/AdView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 4
    .parameter "gad"

    .prologue
    .line 377
    const-string v0, "FlxAd"

    const-string v1, "AdView.GoogleAdListener.onLeaveApplication"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 378
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    instance-of v0, v0, Lnet/flixster/android/ads/model/DfpAd;

    if-eqz v0, :cond_0

    .line 379
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v1

    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/DfpAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/DfpAd;->tag:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v2, "Click"

    iget-object v3, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v3}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lnet/flixster/android/ads/AdManager;->trackEvent(ILjava/lang/String;Ljava/lang/String;)V

    .line 381
    :cond_0
    return-void
.end method

.method public onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 4
    .parameter "gad"

    .prologue
    .line 369
    const-string v0, "FlxAd"

    const-string v1, "AdView.GoogleAdListener.onPresentScreen"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    instance-of v0, v0, Lnet/flixster/android/ads/model/DfpAd;

    if-eqz v0, :cond_0

    .line 371
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v1

    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/DfpAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/DfpAd;->tag:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v2, "Click"

    iget-object v3, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v3}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lnet/flixster/android/ads/AdManager;->trackEvent(ILjava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_0
    return-void
.end method

.method public onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 4
    .parameter "gad"

    .prologue
    .line 361
    const-string v0, "FlxAd"

    const-string v1, "AdView.GoogleAdListener.onReceiveAd"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    instance-of v0, v0, Lnet/flixster/android/ads/model/DfpAd;

    if-eqz v0, :cond_0

    .line 363
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v1

    iget-object v0, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v0}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/DfpAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/DfpAd;->tag:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const-string v2, "Impression"

    iget-object v3, p0, Lnet/flixster/android/ads/AdView$GoogleAdListener;->this$0:Lnet/flixster/android/ads/AdView;

    #getter for: Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;
    invoke-static {v3}, Lnet/flixster/android/ads/AdView;->access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    invoke-virtual {v1, v0, v2, v3}, Lnet/flixster/android/ads/AdManager;->trackEvent(ILjava/lang/String;Ljava/lang/String;)V

    .line 365
    :cond_0
    return-void
.end method
