.class public Lnet/flixster/android/ads/AdView;
.super Landroid/widget/LinearLayout;
.source "AdView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/ads/AdView$GoogleAdListener;
    }
.end annotation


# static fields
.field private static final LAYOUT:Landroid/widget/LinearLayout$LayoutParams;

.field private static final onImageLoad:Landroid/os/Handler;


# instance fields
.field private volatile ad:Lnet/flixster/android/ads/model/Ad;

.field public dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

.field private googleAdListener:Lcom/google/ads/AdListener;

.field private isDfpAdRetry:Z

.field private isTabPlacement:Z

.field public mAdContextId:Ljava/lang/Long;

.field private final mContext:Landroid/content/Context;

.field private mView:Landroid/view/View;

.field private final onAdLoad:Landroid/os/Handler;

.field private final onClickListener:Landroid/view/View$OnClickListener;

.field private placement:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    sput-object v0, Lnet/flixster/android/ads/AdView;->LAYOUT:Landroid/widget/LinearLayout$LayoutParams;

    .line 305
    new-instance v0, Lnet/flixster/android/ads/AdView$1;

    invoke-direct {v0}, Lnet/flixster/android/ads/AdView$1;-><init>()V

    sput-object v0, Lnet/flixster/android/ads/AdView;->onImageLoad:Landroid/os/Handler;

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .parameter "context"

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 47
    iput-object v1, p0, Lnet/flixster/android/ads/AdView;->mAdContextId:Ljava/lang/Long;

    .line 332
    new-instance v0, Lnet/flixster/android/ads/AdView$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/ads/AdView$2;-><init>(Lnet/flixster/android/ads/AdView;)V

    iput-object v0, p0, Lnet/flixster/android/ads/AdView;->onAdLoad:Landroid/os/Handler;

    .line 343
    new-instance v0, Lnet/flixster/android/ads/AdView$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/ads/AdView$3;-><init>(Lnet/flixster/android/ads/AdView;)V

    iput-object v0, p0, Lnet/flixster/android/ads/AdView;->onClickListener:Landroid/view/View$OnClickListener;

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lnet/flixster/android/ads/AdView;->setOrientation(I)V

    .line 63
    iput-object p1, p0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    .line 64
    invoke-virtual {p0, v1}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .parameter "context"
    .parameter "attr"

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    const/4 v1, 0x0

    iput-object v1, p0, Lnet/flixster/android/ads/AdView;->mAdContextId:Ljava/lang/Long;

    .line 332
    new-instance v1, Lnet/flixster/android/ads/AdView$2;

    invoke-direct {v1, p0}, Lnet/flixster/android/ads/AdView$2;-><init>(Lnet/flixster/android/ads/AdView;)V

    iput-object v1, p0, Lnet/flixster/android/ads/AdView;->onAdLoad:Landroid/os/Handler;

    .line 343
    new-instance v1, Lnet/flixster/android/ads/AdView$3;

    invoke-direct {v1, p0}, Lnet/flixster/android/ads/AdView$3;-><init>(Lnet/flixster/android/ads/AdView;)V

    iput-object v1, p0, Lnet/flixster/android/ads/AdView;->onClickListener:Landroid/view/View$OnClickListener;

    .line 76
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lnet/flixster/android/ads/AdView;->setOrientation(I)V

    .line 77
    iput-object p1, p0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    .line 78
    sget-object v1, Lnet/flixster/android/R$styleable;->AdView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 79
    .local v0, result:Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .parameter "context"
    .parameter "placement"

    .prologue
    .line 68
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/ads/AdView;->mAdContextId:Ljava/lang/Long;

    .line 332
    new-instance v0, Lnet/flixster/android/ads/AdView$2;

    invoke-direct {v0, p0}, Lnet/flixster/android/ads/AdView$2;-><init>(Lnet/flixster/android/ads/AdView;)V

    iput-object v0, p0, Lnet/flixster/android/ads/AdView;->onAdLoad:Landroid/os/Handler;

    .line 343
    new-instance v0, Lnet/flixster/android/ads/AdView$3;

    invoke-direct {v0, p0}, Lnet/flixster/android/ads/AdView$3;-><init>(Lnet/flixster/android/ads/AdView;)V

    iput-object v0, p0, Lnet/flixster/android/ads/AdView;->onClickListener:Landroid/view/View$OnClickListener;

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lnet/flixster/android/ads/AdView;->setOrientation(I)V

    .line 70
    iput-object p1, p0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    .line 71
    invoke-virtual {p0, p2}, Lnet/flixster/android/ads/AdView;->setSlot(Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/ads/AdView;)Landroid/content/Context;
    .locals 1
    .parameter

    .prologue
    .line 49
    iget-object v0, p0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/ads/AdView;)Lnet/flixster/android/ads/model/Ad;
    .locals 1
    .parameter

    .prologue
    .line 52
    iget-object v0, p0, Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/ads/AdView;)Z
    .locals 1
    .parameter

    .prologue
    .line 53
    iget-boolean v0, p0, Lnet/flixster/android/ads/AdView;->isTabPlacement:Z

    return v0
.end method

.method static synthetic access$3(Lnet/flixster/android/ads/AdView;)Z
    .locals 1
    .parameter

    .prologue
    .line 58
    iget-boolean v0, p0, Lnet/flixster/android/ads/AdView;->isDfpAdRetry:Z

    return v0
.end method

.method static synthetic access$4(Lnet/flixster/android/ads/AdView;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 58
    iput-boolean p1, p0, Lnet/flixster/android/ads/AdView;->isDfpAdRetry:Z

    return-void
.end method

.method private getAdView(Lnet/flixster/android/ads/model/Ad;)Landroid/view/View;
    .locals 25
    .parameter "ad"

    .prologue
    .line 134
    const-string v2, "FlxAd"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "AdView.getAdView ad:"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " mView:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    move-object/from16 v0, p1

    instance-of v2, v0, Lnet/flixster/android/ads/model/FlixsterAd;

    if-eqz v2, :cond_3

    move-object/from16 v4, p1

    .line 137
    check-cast v4, Lnet/flixster/android/ads/model/FlixsterAd;

    .line 138
    .local v4, fad:Lnet/flixster/android/ads/model/FlixsterAd;
    iget-object v2, v4, Lnet/flixster/android/ads/model/FlixsterAd;->targetType:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    sget-object v3, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->HTML:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    if-ne v2, v3, :cond_0

    .line 139
    const-string v2, "FlxAd"

    const-string v3, "AdView.getAdView new HtmlAdBannerView"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    new-instance v23, Lcom/flixster/android/ads/HtmlAdBannerView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    move-object/from16 v0, v23

    invoke-direct {v0, v2, v4}, Lcom/flixster/android/ads/HtmlAdBannerView;-><init>(Landroid/content/Context;Lnet/flixster/android/ads/model/FlixsterAd;)V

    .line 141
    .local v23, webView:Landroid/webkit/WebView;
    iget-object v2, v4, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 142
    move-object/from16 v0, v23

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    .line 176
    .end local v23           #webView:Landroid/webkit/WebView;
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    .line 298
    .end local v4           #fad:Lnet/flixster/android/ads/model/FlixsterAd;
    .end local p1
    :goto_1
    return-object v2

    .line 144
    .restart local v4       #fad:Lnet/flixster/android/ads/model/FlixsterAd;
    .restart local p1
    :cond_0
    new-instance v6, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    invoke-direct {v6, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 145
    .local v6, adView:Landroid/widget/ImageView;
    const-string v2, "FlxAd"

    const-string v3, "AdView.getAdView new ImageView"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 147
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 149
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 151
    const/4 v2, 0x1

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 152
    iget-object v2, v4, Lnet/flixster/android/ads/model/FlixsterAd;->width:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 153
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v3, v4, Lnet/flixster/android/ads/model/FlixsterAd;->width:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v5, v4, Lnet/flixster/android/ads/model/FlixsterAd;->height:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    :cond_1
    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 159
    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 161
    iget-object v2, v4, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_2

    .line 162
    invoke-static {}, Lnet/flixster/android/ads/ImageManager;->getInstance()Lnet/flixster/android/ads/ImageManager;

    move-result-object v24

    new-instance v2, Lnet/flixster/android/model/ImageOrder;

    const/4 v3, 0x0

    iget-object v5, v4, Lnet/flixster/android/ads/model/FlixsterAd;->imageUrl:Ljava/lang/String;

    sget-object v7, Lnet/flixster/android/ads/AdView;->onImageLoad:Landroid/os/Handler;

    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    move-object/from16 v0, v24

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/ImageManager;->enqueImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 173
    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->onClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 174
    move-object/from16 v0, p0

    iput-object v6, v0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    goto :goto_0

    .line 164
    :cond_2
    iget-object v2, v4, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 165
    sget v2, Lnet/flixster/android/FlixsterApplication;->sRelativePixel:F

    iget-object v3, v4, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    float-to-int v2, v2

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 166
    iget-object v2, v4, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    iget-object v3, v4, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    iget-object v5, v4, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v12

    .line 167
    .local v12, color:I
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lnet/flixster/android/ads/AdView;->setBackgroundColor(I)V

    .line 171
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v2

    const-string v3, "Impression"

    invoke-virtual {v2, v4, v3}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    goto :goto_2

    .line 178
    .end local v4           #fad:Lnet/flixster/android/ads/model/FlixsterAd;
    .end local v6           #adView:Landroid/widget/ImageView;
    .end local v12           #color:I
    :cond_3
    move-object/from16 v0, p1

    instance-of v2, v0, Lnet/flixster/android/ads/model/AdMobAd;

    if-eqz v2, :cond_8

    .line 179
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v2

    const/4 v3, 0x4

    if-lt v2, v3, :cond_7

    .line 180
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_6

    move-object/from16 v2, p1

    .line 181
    check-cast v2, Lnet/flixster/android/ads/model/AdMobAd;

    iget-object v10, v2, Lnet/flixster/android/ads/model/AdMobAd;->channelId:Ljava/lang/String;

    .line 182
    .local v10, channelId:Ljava/lang/String;
    check-cast p1, Lnet/flixster/android/ads/model/AdMobAd;

    .end local p1
    move-object/from16 v0, p1

    iget-object v0, v0, Lnet/flixster/android/ads/model/AdMobAd;->keywords:Ljava/lang/String;

    move-object/from16 v17, v0

    .line 183
    .local v17, keywords:Ljava/lang/String;
    new-instance v8, Lcom/google/ads/AdView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    sget-object v3, Lcom/google/ads/AdSize;->BANNER:Lcom/google/ads/AdSize;

    .line 184
    if-eqz v10, :cond_5

    .line 183
    .end local v10           #channelId:Ljava/lang/String;
    :goto_3
    invoke-direct {v8, v2, v3, v10}, Lcom/google/ads/AdView;-><init>(Landroid/app/Activity;Lcom/google/ads/AdSize;Ljava/lang/String;)V

    .line 185
    .local v8, adMob:Lcom/google/ads/AdView;
    new-instance v19, Lcom/google/ads/AdRequest;

    invoke-direct/range {v19 .. v19}, Lcom/google/ads/AdRequest;-><init>()V

    .line 186
    .local v19, request:Lcom/google/ads/AdRequest;
    if-eqz v17, :cond_4

    .line 187
    invoke-static/range {v17 .. v17}, Lnet/flixster/android/ads/AdUtils;->splitKeywords(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/google/ads/AdRequest;->setKeywords(Ljava/util/Set;)Lcom/google/ads/AdRequest;

    .line 189
    :cond_4
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/ads/AdView;->getGoogleAdListener()Lcom/google/ads/AdListener;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/ads/AdView;->setAdListener(Lcom/google/ads/AdListener;)V

    .line 190
    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Lcom/google/ads/AdView;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 191
    move-object/from16 v0, p0

    iput-object v8, v0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    goto/16 :goto_1

    .line 184
    .end local v8           #adMob:Lcom/google/ads/AdView;
    .end local v19           #request:Lcom/google/ads/AdRequest;
    .restart local v10       #channelId:Ljava/lang/String;
    :cond_5
    const-string v10, "a14b625b592ec46"

    goto :goto_3

    .line 194
    .end local v10           #channelId:Ljava/lang/String;
    .end local v17           #keywords:Ljava/lang/String;
    .restart local p1
    :cond_6
    const-string v2, "FlxAd"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Activity context needed for AdMob: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    :cond_7
    :goto_4
    const-string v2, "FlxAd"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "AdView.getAdView unsupported ad type: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 197
    :cond_8
    move-object/from16 v0, p1

    instance-of v2, v0, Lnet/flixster/android/ads/model/DfpAd;

    if-eqz v2, :cond_f

    .line 198
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v2

    const/4 v3, 0x4

    if-lt v2, v3, :cond_7

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    instance-of v2, v2, Landroid/app/Activity;

    if-eqz v2, :cond_e

    .line 200
    new-instance v21, Lcom/google/ads/AdSize;

    const/16 v3, 0x140

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lnet/flixster/android/ads/AdView;->isTabPlacement:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lnet/flixster/android/ads/AdView;->isDfpAdRetry:Z

    if-eqz v2, :cond_c

    :cond_9
    const/16 v2, 0x32

    :goto_5
    move-object/from16 v0, v21

    invoke-direct {v0, v3, v2}, Lcom/google/ads/AdSize;-><init>(II)V

    .line 201
    .local v21, size:Lcom/google/ads/AdSize;
    move-object/from16 v0, p1

    iget-object v2, v0, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    invoke-static {v2}, Lnet/flixster/android/ads/model/DfpAd;->buildAdUnitId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 202
    .local v9, adUnitId:Ljava/lang/String;
    new-instance v14, Lcom/google/ads/AdView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    move-object/from16 v0, v21

    invoke-direct {v14, v2, v0, v9}, Lcom/google/ads/AdView;-><init>(Landroid/app/Activity;Lcom/google/ads/AdSize;Ljava/lang/String;)V

    .line 203
    .local v14, dfp:Lcom/google/ads/AdView;
    invoke-direct/range {p0 .. p0}, Lnet/flixster/android/ads/AdView;->getGoogleAdListener()Lcom/google/ads/AdListener;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/google/ads/AdView;->setAdListener(Lcom/google/ads/AdListener;)V

    .line 204
    new-instance v19, Lcom/google/ads/AdRequest;

    invoke-direct/range {v19 .. v19}, Lcom/google/ads/AdRequest;-><init>()V

    .line 205
    .restart local v19       #request:Lcom/google/ads/AdRequest;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLocation()Landroid/location/Location;

    move-result-object v18

    .line 206
    .local v18, loc:Landroid/location/Location;
    if-eqz v18, :cond_a

    .line 207
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/ads/AdRequest;->setLocation(Landroid/location/Location;)Lcom/google/ads/AdRequest;

    .line 209
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    if-eqz v2, :cond_d

    .line 210
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->addCustomKeyValuePairsTo(Lcom/google/ads/AdRequest;)V

    .line 214
    :cond_b
    :goto_6
    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Lcom/google/ads/AdView;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 215
    move-object/from16 v0, p0

    iput-object v14, v0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    .line 216
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    goto/16 :goto_1

    .line 200
    .end local v9           #adUnitId:Ljava/lang/String;
    .end local v14           #dfp:Lcom/google/ads/AdView;
    .end local v18           #loc:Landroid/location/Location;
    .end local v19           #request:Lcom/google/ads/AdRequest;
    .end local v21           #size:Lcom/google/ads/AdSize;
    :cond_c
    const/16 v2, 0x96

    goto :goto_5

    .line 211
    .restart local v9       #adUnitId:Ljava/lang/String;
    .restart local v14       #dfp:Lcom/google/ads/AdView;
    .restart local v18       #loc:Landroid/location/Location;
    .restart local v19       #request:Lcom/google/ads/AdRequest;
    .restart local v21       #size:Lcom/google/ads/AdSize;
    :cond_d
    invoke-static {}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->getTestFlag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_b

    .line 212
    invoke-static/range {v19 .. v19}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->addCustomTargetPairTo(Lcom/google/ads/AdRequest;)V

    goto :goto_6

    .line 218
    .end local v9           #adUnitId:Ljava/lang/String;
    .end local v14           #dfp:Lcom/google/ads/AdView;
    .end local v18           #loc:Landroid/location/Location;
    .end local v19           #request:Lcom/google/ads/AdRequest;
    .end local v21           #size:Lcom/google/ads/AdSize;
    :cond_e
    const-string v2, "FlxAd"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Activity context needed for DFP: "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 221
    :cond_f
    move-object/from16 v0, p1

    instance-of v2, v0, Lnet/flixster/android/ads/model/GoogleAd;

    if-eqz v2, :cond_7

    .line 222
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v2

    const/4 v3, 0x4

    if-ge v2, v3, :cond_10

    .line 223
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 225
    :cond_10
    new-instance v20, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 226
    .local v20, rl:Landroid/widget/RelativeLayout;
    const/4 v2, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setHorizontalGravity(I)V

    .line 227
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    .line 228
    const/4 v5, -0x2

    invoke-direct {v2, v3, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 227
    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v16, p1

    .line 230
    check-cast v16, Lnet/flixster/android/ads/model/GoogleAd;

    .line 231
    .local v16, gad:Lnet/flixster/android/ads/model/GoogleAd;
    new-instance v6, Lcom/google/ads/GoogleAdView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mContext:Landroid/content/Context;

    invoke-direct {v6, v2}, Lcom/google/ads/GoogleAdView;-><init>(Landroid/content/Context;)V

    .line 232
    .local v6, adView:Lcom/google/ads/GoogleAdView;
    const-string v2, "FlxAd"

    const-string v3, "AdView.getAdView new GoogleAdView"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ads/AdView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v13

    .line 235
    .local v13, country:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserZip()Ljava/lang/String;

    move-result-object v11

    .line 237
    .local v11, cityzip:Ljava/lang/String;
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v2

    if-eqz v2, :cond_15

    .line 238
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentZip()Ljava/lang/String;

    move-result-object v11

    .line 239
    if-eqz v11, :cond_11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_12

    .line 240
    :cond_11
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentCity()Ljava/lang/String;

    move-result-object v11

    .line 241
    if-eqz v11, :cond_12

    .line 242
    const-string v2, "UTF-8"

    invoke-static {v11, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v11

    .line 258
    :cond_12
    :goto_7
    const-string v2, "FlxAd"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "AdView.getAdView country:"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " city:"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    new-instance v22, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;

    const-string v2, "ca-mb-app-pub-7518399278719852"

    move-object/from16 v0, v22

    invoke-direct {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;-><init>(Ljava/lang/String;)V

    .line 261
    .local v22, spec:Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;
    move-object/from16 v0, v22

    invoke-virtual {v0, v13}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setUserCountry(Ljava/lang/String;)Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;

    .line 262
    move-object/from16 v0, v22

    invoke-virtual {v0, v11}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setUserCity(Ljava/lang/String;)Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;

    .line 263
    const-string v2, "Flixster Inc."

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setCompanyName(Ljava/lang/String;)Lcom/google/ads/AdSenseSpec;

    .line 264
    const-string v2, "Movies"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setAppName(Ljava/lang/String;)Lcom/google/ads/AdSenseSpec;

    .line 265
    sget-object v2, Lcom/google/ads/AdSenseSpec$AdType;->TEXT_IMAGE:Lcom/google/ads/AdSenseSpec$AdType;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setAdType(Lcom/google/ads/AdSenseSpec$AdType;)Lcom/google/ads/AdSenseSpec;

    .line 266
    move-object/from16 v0, v16

    iget-object v2, v0, Lnet/flixster/android/ads/model/GoogleAd;->keywords:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setKeywords(Ljava/lang/String;)Lcom/google/ads/AdSenseSpec;

    .line 267
    move-object/from16 v0, v16

    iget-object v2, v0, Lnet/flixster/android/ads/model/GoogleAd;->channelId:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setChannel(Ljava/lang/String;)Lcom/google/ads/AdSenseSpec;

    .line 268
    const/4 v2, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setAdTestEnabled(Z)Lcom/google/ads/AdSenseSpec;

    .line 270
    move-object/from16 v0, v16

    iget-object v2, v0, Lnet/flixster/android/ads/model/GoogleAd;->sticky:Ljava/lang/String;

    if-eqz v2, :cond_17

    move-object/from16 v0, v16

    iget-object v2, v0, Lnet/flixster/android/ads/model/GoogleAd;->sticky:Ljava/lang/String;

    const-string v3, "bottom"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 271
    sget-object v2, Lcom/google/ads/AdSenseSpec$ExpandDirection;->TOP:Lcom/google/ads/AdSenseSpec$ExpandDirection;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setExpandDirection(Lcom/google/ads/AdSenseSpec$ExpandDirection;)Lcom/google/ads/AdSenseSpec;

    .line 276
    :goto_8
    move-object/from16 v0, v16

    iget-object v2, v0, Lnet/flixster/android/ads/model/GoogleAd;->bcolor:Ljava/lang/String;

    if-eqz v2, :cond_13

    .line 277
    move-object/from16 v0, v16

    iget-object v2, v0, Lnet/flixster/android/ads/model/GoogleAd;->bcolor:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setColorBackground(Ljava/lang/String;)Lcom/google/ads/AdSenseSpec;

    .line 279
    :cond_13
    move-object/from16 v0, v16

    iget-object v2, v0, Lnet/flixster/android/ads/model/GoogleAd;->fcolor:Ljava/lang/String;

    if-eqz v2, :cond_14

    .line 280
    move-object/from16 v0, v16

    iget-object v2, v0, Lnet/flixster/android/ads/model/GoogleAd;->fcolor:Ljava/lang/String;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setColorText(Ljava/lang/String;)Lcom/google/ads/AdSenseSpec;

    .line 282
    :cond_14
    move-object/from16 v0, v22

    invoke-virtual {v6, v0}, Lcom/google/ads/GoogleAdView;->showAds(Lcom/google/ads/AdSpec;)V

    .line 287
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/google/ads/GoogleAdView;->setFocusable(Z)V

    .line 288
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/google/ads/GoogleAdView;->setFocusableInTouchMode(Z)V

    .line 290
    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 291
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/ads/AdView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f090017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    .line 293
    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    .line 294
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    goto/16 :goto_1

    .line 246
    .end local v22           #spec:Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;
    :cond_15
    :try_start_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserZip()Ljava/lang/String;

    move-result-object v11

    .line 247
    if-eqz v11, :cond_16

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_12

    .line 248
    :cond_16
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserCity()Ljava/lang/String;

    move-result-object v11

    .line 249
    if-eqz v11, :cond_12

    .line 250
    const-string v2, "UTF-8"

    invoke-static {v11, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v11

    goto/16 :goto_7

    .line 254
    :catch_0
    move-exception v15

    .line 256
    .local v15, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v15}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_7

    .line 273
    .end local v15           #e:Ljava/io/UnsupportedEncodingException;
    .restart local v22       #spec:Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;
    :cond_17
    sget-object v2, Lcom/google/ads/AdSenseSpec$ExpandDirection;->BOTTOM:Lcom/google/ads/AdSenseSpec$ExpandDirection;

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->setExpandDirection(Lcom/google/ads/AdSenseSpec$ExpandDirection;)Lcom/google/ads/AdSenseSpec;

    goto :goto_8
.end method

.method private getGoogleAdListener()Lcom/google/ads/AdListener;
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lnet/flixster/android/ads/AdView;->googleAdListener:Lcom/google/ads/AdListener;

    if-nez v0, :cond_0

    .line 353
    new-instance v0, Lnet/flixster/android/ads/AdView$GoogleAdListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lnet/flixster/android/ads/AdView$GoogleAdListener;-><init>(Lnet/flixster/android/ads/AdView;Lnet/flixster/android/ads/AdView$GoogleAdListener;)V

    iput-object v0, p0, Lnet/flixster/android/ads/AdView;->googleAdListener:Lcom/google/ads/AdListener;

    .line 355
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/ads/AdView;->googleAdListener:Lcom/google/ads/AdListener;

    return-object v0
.end method


# virtual methods
.method public destroy()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    instance-of v0, v0, Lcom/google/ads/AdView;

    if-eqz v0, :cond_1

    .line 123
    const-string v0, "FlxAd"

    const-string v1, "AdView.destroy AdMob"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    check-cast v0, Lcom/google/ads/AdView;

    invoke-virtual {v0}, Lcom/google/ads/AdView;->destroy()V

    .line 130
    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    .line 131
    return-void

    .line 128
    :cond_1
    const-string v0, "FlxAd"

    const-string v1, "AdView.destroy null/na"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public refreshAds()V
    .locals 6

    .prologue
    const v5, 0x7f0a0024

    .line 91
    iget-object v1, p0, Lnet/flixster/android/ads/AdView;->mView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 92
    invoke-virtual {p0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 95
    :cond_0
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/ads/AdView;->placement:Ljava/lang/String;

    iget-object v3, p0, Lnet/flixster/android/ads/AdView;->onAdLoad:Landroid/os/Handler;

    iget-object v4, p0, Lnet/flixster/android/ads/AdView;->mAdContextId:Ljava/lang/Long;

    invoke-virtual {v1, v2, v3, v4}, Lnet/flixster/android/ads/AdManager;->getAd(Ljava/lang/String;Landroid/os/Handler;Ljava/lang/Long;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;

    .line 96
    const-string v1, "FlxAd"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AdView.refreshAds slot:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/ads/AdView;->placement:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ad:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 97
    iget-object v1, p0, Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;

    if-eqz v1, :cond_3

    .line 98
    iget-object v1, p0, Lnet/flixster/android/ads/AdView;->ad:Lnet/flixster/android/ads/model/Ad;

    invoke-direct {p0, v1}, Lnet/flixster/android/ads/AdView;->getAdView(Lnet/flixster/android/ads/model/Ad;)Landroid/view/View;

    move-result-object v0

    .line 99
    .local v0, view:Landroid/view/View;
    if-eqz v0, :cond_1

    .line 100
    invoke-virtual {p0}, Lnet/flixster/android/ads/AdView;->removeAllViews()V

    .line 101
    sget-object v1, Lnet/flixster/android/ads/AdView;->LAYOUT:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/ads/AdView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 102
    invoke-virtual {p0}, Lnet/flixster/android/ads/AdView;->invalidate()V

    .line 107
    :cond_1
    iget-boolean v1, p0, Lnet/flixster/android/ads/AdView;->isTabPlacement:Z

    if-eqz v1, :cond_2

    .line 108
    invoke-virtual {p0}, Lnet/flixster/android/ads/AdView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/ads/AdView;->setMinimumHeight(I)V

    .line 113
    :goto_0
    invoke-virtual {p0}, Lnet/flixster/android/ads/AdView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/ads/AdView;->setBackgroundColor(I)V

    .line 114
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lnet/flixster/android/ads/AdView;->setVisibility(I)V

    .line 118
    .end local v0           #view:Landroid/view/View;
    :goto_1
    return-void

    .line 111
    .restart local v0       #view:Landroid/view/View;
    :cond_2
    invoke-virtual {p0}, Lnet/flixster/android/ads/AdView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lnet/flixster/android/ads/AdView;->setMinimumHeight(I)V

    goto :goto_0

    .line 116
    .end local v0           #view:Landroid/view/View;
    :cond_3
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lnet/flixster/android/ads/AdView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setSlot(Ljava/lang/String;)V
    .locals 3
    .parameter "placement"

    .prologue
    .line 85
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AdView.setPlacement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    iput-object p1, p0, Lnet/flixster/android/ads/AdView;->placement:Ljava/lang/String;

    .line 87
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lnet/flixster/android/ads/AdManager;->isTabPlacement(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lnet/flixster/android/ads/AdView;->isTabPlacement:Z

    .line 88
    return-void
.end method
