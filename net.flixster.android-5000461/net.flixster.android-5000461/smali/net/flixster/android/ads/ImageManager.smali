.class public Lnet/flixster/android/ads/ImageManager;
.super Ljava/lang/Thread;
.source "ImageManager.java"


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mInstance:Lnet/flixster/android/ads/ImageManager;


# instance fields
.field private mQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Lnet/flixster/android/model/ImageOrder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lnet/flixster/android/ads/ImageManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/ads/ImageManager;->TAG:Ljava/lang/String;

    .line 20
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "name"

    .prologue
    .line 26
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public static getInstance()Lnet/flixster/android/ads/ImageManager;
    .locals 2

    .prologue
    .line 30
    sget-object v0, Lnet/flixster/android/ads/ImageManager;->mInstance:Lnet/flixster/android/ads/ImageManager;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lnet/flixster/android/ads/ImageManager;

    sget-object v1, Lnet/flixster/android/ads/ImageManager;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1}, Lnet/flixster/android/ads/ImageManager;-><init>(Ljava/lang/String;)V

    sput-object v0, Lnet/flixster/android/ads/ImageManager;->mInstance:Lnet/flixster/android/ads/ImageManager;

    .line 33
    :cond_0
    sget-object v0, Lnet/flixster/android/ads/ImageManager;->mInstance:Lnet/flixster/android/ads/ImageManager;

    return-object v0
.end method


# virtual methods
.method public enqueImage(Lnet/flixster/android/model/ImageOrder;)V
    .locals 3
    .parameter "io"

    .prologue
    .line 37
    iget-object v1, p0, Lnet/flixster/android/ads/ImageManager;->mQueue:Ljava/util/concurrent/BlockingQueue;

    if-nez v1, :cond_0

    .line 38
    new-instance v1, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v1, p0, Lnet/flixster/android/ads/ImageManager;->mQueue:Ljava/util/concurrent/BlockingQueue;

    .line 39
    invoke-virtual {p0}, Lnet/flixster/android/ads/ImageManager;->start()V

    .line 43
    :cond_0
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/ads/ImageManager;->mQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v1, p1}, Ljava/util/concurrent/BlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    return-void

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, e:Ljava/lang/InterruptedException;
    sget-object v1, Lnet/flixster/android/ads/ImageManager;->TAG:Ljava/lang/String;

    const-string v2, "Error queueing image"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    .line 52
    :cond_0
    :goto_0
    :try_start_0
    iget-object v5, p0, Lnet/flixster/android/ads/ImageManager;->mQueue:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v5}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/ImageOrder;

    .line 53
    .local v3, imageOrder:Lnet/flixster/android/model/ImageOrder;
    new-instance v5, Ljava/net/URL;

    iget-object v6, v3, Lnet/flixster/android/model/ImageOrder;->urlString:Ljava/lang/String;

    invoke-direct {v5, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {v5}, Lnet/flixster/android/util/HttpImageHelper;->fetchImage(Ljava/net/URL;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 55
    .local v0, bitmap:Landroid/graphics/Bitmap;
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ImageManager.run() imageOrder:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v5, v3, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    if-eqz v5, :cond_1

    .line 57
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ImageManager.run() imageOrder.tag:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v3, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v4, v3, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    check-cast v4, Lnet/flixster/android/ads/model/BitmapModel;

    .line 59
    .local v4, view:Lnet/flixster/android/ads/model/BitmapModel;
    iput-object v0, v4, Lnet/flixster/android/ads/model/BitmapModel;->bitmap:Landroid/graphics/Bitmap;

    .line 62
    .end local v4           #view:Lnet/flixster/android/ads/model/BitmapModel;
    :cond_1
    iget-object v5, v3, Lnet/flixster/android/model/ImageOrder;->refreshHandler:Landroid/os/Handler;

    if-eqz v5, :cond_0

    .line 63
    iget-object v5, v3, Lnet/flixster/android/model/ImageOrder;->refreshHandler:Landroid/os/Handler;

    iget-object v6, v3, Lnet/flixster/android/model/ImageOrder;->refreshHandler:Landroid/os/Handler;

    const/4 v7, 0x0

    .line 64
    iget-object v8, v3, Lnet/flixster/android/model/ImageOrder;->movieView:Landroid/view/View;

    .line 63
    invoke-static {v6, v7, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 66
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v3           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :catch_0
    move-exception v1

    .line 67
    .local v1, e:Ljava/lang/InterruptedException;
    sget-object v5, Lnet/flixster/android/ads/ImageManager;->TAG:Ljava/lang/String;

    const-string v6, "Image queue interrupted exception"

    invoke-static {v5, v6, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 68
    .end local v1           #e:Ljava/lang/InterruptedException;
    :catch_1
    move-exception v2

    .line 69
    .local v2, ie:Ljava/io/IOException;
    sget-object v5, Lnet/flixster/android/ads/ImageManager;->TAG:Ljava/lang/String;

    const-string v6, "Error getting image"

    invoke-static {v5, v6, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
