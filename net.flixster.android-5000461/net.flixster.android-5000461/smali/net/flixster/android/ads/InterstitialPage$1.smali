.class Lnet/flixster/android/ads/InterstitialPage$1;
.super Landroid/os/Handler;
.source "InterstitialPage.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ads/InterstitialPage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ads/InterstitialPage;


# direct methods
.method constructor <init>(Lnet/flixster/android/ads/InterstitialPage;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    .line 132
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/ads/InterstitialPage$1;)Lnet/flixster/android/ads/InterstitialPage;
    .locals 1
    .parameter

    .prologue
    .line 132
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    return-object v0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 7
    .parameter "msg"

    .prologue
    .line 135
    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    invoke-virtual {v2}, Lnet/flixster/android/ads/InterstitialPage;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    const-string v2, "FlxAd"

    const-string v3, "InterstitialPage.imageLoaded skipped"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    const-string v2, "FlxAd"

    const-string v3, "InterstitialPage.imageLoaded"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 140
    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;
    invoke-static {v2}, Lnet/flixster/android/ads/InterstitialPage;->access$0(Lnet/flixster/android/ads/InterstitialPage;)Landroid/widget/ImageView;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;
    invoke-static {v3}, Lnet/flixster/android/ads/InterstitialPage;->access$1(Lnet/flixster/android/ads/InterstitialPage;)Lnet/flixster/android/ads/model/FlixsterAd;

    move-result-object v3

    iget-object v3, v3, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 141
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;
    invoke-static {v3}, Lnet/flixster/android/ads/InterstitialPage;->access$1(Lnet/flixster/android/ads/InterstitialPage;)Lnet/flixster/android/ads/model/FlixsterAd;

    move-result-object v3

    const-string v4, "Impression"

    invoke-virtual {v2, v3, v4}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    .line 142
    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;
    invoke-static {v2}, Lnet/flixster/android/ads/InterstitialPage;->access$2(Lnet/flixster/android/ads/InterstitialPage;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "CustomInterstitial"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 143
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v2

    const-string v3, "/launch/interstitial"

    const-string v4, "launch"

    const-string v5, "LaunchInterstitial"

    const-string v6, "Impression"

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    :goto_1
    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;
    invoke-static {v2}, Lnet/flixster/android/ads/InterstitialPage;->access$1(Lnet/flixster/android/ads/InterstitialPage;)Lnet/flixster/android/ads/model/FlixsterAd;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;
    invoke-static {v2}, Lnet/flixster/android/ads/InterstitialPage;->access$1(Lnet/flixster/android/ads/InterstitialPage;)Lnet/flixster/android/ads/model/FlixsterAd;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    .line 155
    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;
    invoke-static {v2}, Lnet/flixster/android/ads/InterstitialPage;->access$1(Lnet/flixster/android/ads/InterstitialPage;)Lnet/flixster/android/ads/model/FlixsterAd;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 156
    .local v1, height:I
    const/16 v2, 0x64

    if-le v1, v2, :cond_0

    .line 157
    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;
    invoke-static {v2}, Lnet/flixster/android/ads/InterstitialPage;->access$1(Lnet/flixster/android/ads/InterstitialPage;)Lnet/flixster/android/ads/model/FlixsterAd;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/ads/model/FlixsterAd;->bitmap:Landroid/graphics/Bitmap;

    const/16 v3, 0xa0

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    .line 158
    .local v0, color:I
    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;
    invoke-static {v2}, Lnet/flixster/android/ads/InterstitialPage;->access$0(Lnet/flixster/android/ads/InterstitialPage;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0

    .line 145
    .end local v0           #color:I
    .end local v1           #height:I
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage$1;->this$0:Lnet/flixster/android/ads/InterstitialPage;

    #getter for: Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;
    invoke-static {v2}, Lnet/flixster/android/ads/InterstitialPage;->access$0(Lnet/flixster/android/ads/InterstitialPage;)Landroid/widget/ImageView;

    move-result-object v2

    new-instance v3, Lnet/flixster/android/ads/InterstitialPage$1$1;

    invoke-direct {v3, p0}, Lnet/flixster/android/ads/InterstitialPage$1$1;-><init>(Lnet/flixster/android/ads/InterstitialPage$1;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
