.class public Lnet/flixster/android/ads/InterstitialPage;
.super Landroid/app/Activity;
.source "InterstitialPage.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final CUSTOM_INTERSTITIAL:Ljava/lang/String; = "CustomInterstitial"

.field public static final SLOT_TYPE:Ljava/lang/String; = "SLOT_TYPE"


# instance fields
.field public imageLoaded:Landroid/os/Handler;

.field private mButton1:Landroid/widget/Button;

.field private mButton2:Landroid/widget/Button;

.field private mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

.field private mImageView:Landroid/widget/ImageView;

.field private mSlotType:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 132
    new-instance v0, Lnet/flixster/android/ads/InterstitialPage$1;

    invoke-direct {v0, p0}, Lnet/flixster/android/ads/InterstitialPage$1;-><init>(Lnet/flixster/android/ads/InterstitialPage;)V

    iput-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->imageLoaded:Landroid/os/Handler;

    .line 29
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/ads/InterstitialPage;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/ads/InterstitialPage;)Lnet/flixster/android/ads/model/FlixsterAd;
    .locals 1
    .parameter

    .prologue
    .line 34
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/ads/InterstitialPage;)Ljava/lang/String;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .parameter "v"

    .prologue
    const/4 v6, 0x0

    .line 179
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InterstitialPage.onClick view id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0700d4

    if-ne v0, v1, :cond_1

    .line 181
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    const-string v2, "Skip"

    invoke-virtual {v0, v1, v2}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    .line 182
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;

    const-string v1, "CustomInterstitial"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/7eleven"

    const-string v2, "7-Eleven"

    const-string v3, "7Eleven"

    const-string v4, "Interstitial"

    const-string v5, "Skip"

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 187
    :goto_0
    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->finish()V

    .line 197
    :goto_1
    return-void

    .line 185
    :cond_0
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/launch/interstitial"

    const-string v2, "launch"

    const-string v3, "LaunchInterstitial"

    const-string v4, "Skip"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 189
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;

    const-string v1, "CustomInterstitial"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 190
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/7eleven"

    const-string v2, "7-Eleven"

    const-string v3, "7Eleven"

    const-string v4, "Interstitial"

    const-string v5, "Click"

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 191
    const-string v0, "7 eleven"

    invoke-static {v0, p0}, Lnet/flixster/android/Starter;->launchMaps(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_1

    .line 193
    :cond_2
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/launch/interstitial"

    const-string v2, "launch"

    const-string v3, "LaunchInterstitial"

    const-string v4, "Click"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    invoke-static {v0, p0}, Lnet/flixster/android/ads/AdManager;->onFlixsterAdClick(Lnet/flixster/android/ads/model/FlixsterAd;Landroid/content/Context;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12
    .parameter "savedInstanceState"

    .prologue
    .line 41
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 42
    sget v0, Lcom/flixster/android/utils/F;->API_LEVEL:I

    const/16 v1, 0x9

    if-lt v0, v1, :cond_0

    const/4 v0, 0x7

    :goto_0
    invoke-virtual {p0, v0}, Lnet/flixster/android/ads/InterstitialPage;->setRequestedOrientation(I)V

    .line 44
    const v0, 0x7f030037

    invoke-virtual {p0, v0}, Lnet/flixster/android/ads/InterstitialPage;->setContentView(I)V

    .line 46
    const v0, 0x7f0700d3

    invoke-virtual {p0, v0}, Lnet/flixster/android/ads/InterstitialPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    .line 47
    const v0, 0x7f0700d4

    invoke-virtual {p0, v0}, Lnet/flixster/android/ads/InterstitialPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton2:Landroid/widget/Button;

    .line 48
    const v0, 0x7f0700d1

    invoke-virtual {p0, v0}, Lnet/flixster/android/ads/InterstitialPage;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    .line 49
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton2:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 52
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v7

    .line 55
    .local v7, extras:Landroid/os/Bundle;
    const-string v0, "SLOT_TYPE"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;

    .line 57
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InterstitialPage.onCreate SLOT_TYPE:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 59
    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->finish()V

    .line 109
    :goto_1
    return-void

    .line 43
    .end local v7           #extras:Landroid/os/Bundle;
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 62
    .restart local v7       #extras:Landroid/os/Bundle;
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;

    const-string v1, "CustomInterstitial"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    invoke-static {}, Lcom/flixster/android/utils/ObjectHolder;->instance()Lcom/flixster/android/utils/ObjectHolder;

    move-result-object v0

    const-string v1, "CustomInterstitial"

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/ObjectHolder;->remove(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/FlixsterAd;

    iput-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    .line 64
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/7eleven"

    const-string v2, "7-Eleven"

    const-string v3, "7Eleven"

    const-string v4, "Interstitial"

    const-string v5, "Impression"

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 66
    :cond_2
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "InterstitialPage.onCreate mFlixsterAd:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    if-nez v0, :cond_3

    .line 68
    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->finish()V

    goto :goto_1

    .line 72
    :cond_3
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd;->imageUrl:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 73
    invoke-static {}, Lnet/flixster/android/ads/ImageManager;->getInstance()Lnet/flixster/android/ads/ImageManager;

    move-result-object v6

    .line 74
    new-instance v0, Lnet/flixster/android/model/ImageOrder;

    const/4 v1, 0x0

    iget-object v2, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v3, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v3, v3, Lnet/flixster/android/ads/model/FlixsterAd;->imageUrl:Ljava/lang/String;

    iget-object v4, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    iget-object v5, p0, Lnet/flixster/android/ads/InterstitialPage;->imageLoaded:Landroid/os/Handler;

    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 73
    invoke-virtual {v6, v0}, Lnet/flixster/android/ads/ImageManager;->enqueImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 77
    :cond_4
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd;->button1:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 78
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 79
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd;->url:Ljava/lang/String;

    const-string v1, "flixster://flixster.com/mobile/android/facebookauth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 80
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    const-string v1, "Login with Facebook"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 81
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextSize(F)V

    .line 83
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    const v1, 0x7f02006b

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setBackgroundResource(I)V

    .line 84
    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 85
    .local v11, vPad:I
    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 86
    .local v9, leftPad:I
    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 90
    .local v10, rightPad:I
    invoke-virtual {p0}, Lnet/flixster/android/ads/InterstitialPage;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 91
    .local v8, fbicon:Landroid/graphics/drawable/Drawable;
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 92
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/widget/Button;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 93
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    invoke-virtual {v0, v9, v11, v10, v11}, Landroid/widget/Button;->setPadding(IIII)V

    .line 95
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    const/high16 v1, 0x4000

    const/4 v2, 0x0

    const/high16 v3, -0x4000

    const v4, 0x7f090023

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/Button;->setShadowLayer(FFFI)V

    .line 104
    .end local v8           #fbicon:Landroid/graphics/drawable/Drawable;
    .end local v9           #leftPad:I
    .end local v10           #rightPad:I
    .end local v11           #vPad:I
    :goto_2
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v0, v0, Lnet/flixster/android/ads/model/FlixsterAd;->button2:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 105
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton2:Landroid/widget/Button;

    iget-object v1, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v1, v1, Lnet/flixster/android/ads/model/FlixsterAd;->button2:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 98
    :cond_5
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    iget-object v1, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    iget-object v1, v1, Lnet/flixster/android/ads/model/FlixsterAd;->button1:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 101
    :cond_6
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton1:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_2

    .line 107
    :cond_7
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mButton2:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 128
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 129
    const-string v0, "FlxAd"

    const-string v1, "InterstitialPage.onDestroy"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 7
    .parameter "keycode"
    .parameter "event"

    .prologue
    .line 166
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    .line 167
    const-string v0, "FlxMain"

    const-string v1, "InterstitialPage.onKeyDown KEYCODE_BACK"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    invoke-static {}, Lnet/flixster/android/ads/AdManager;->instance()Lnet/flixster/android/ads/AdManager;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/ads/InterstitialPage;->mFlixsterAd:Lnet/flixster/android/ads/model/FlixsterAd;

    const-string v2, "Skip"

    invoke-virtual {v0, v1, v2}, Lnet/flixster/android/ads/AdManager;->trackEvent(Lnet/flixster/android/ads/model/TaggableAd;Ljava/lang/String;)V

    .line 169
    iget-object v0, p0, Lnet/flixster/android/ads/InterstitialPage;->mSlotType:Ljava/lang/String;

    const-string v1, "CustomInterstitial"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/7eleven"

    const-string v2, "7-Eleven"

    const-string v3, "7Eleven"

    const-string v4, "Interstitial"

    const-string v5, "Skip"

    const/4 v6, 0x0

    invoke-interface/range {v0 .. v6}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 175
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 172
    :cond_1
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/launch/interstitial"

    const-string v2, "launch"

    const-string v3, "LaunchInterstitial"

    const-string v4, "Skip"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 7
    .parameter "hasFocus"

    .prologue
    .line 113
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 114
    iget-object v3, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    int-to-float v0, v3

    .line 115
    .local v0, height:F
    iget-object v3, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    int-to-float v2, v3

    .line 116
    .local v2, width:F
    div-float v1, v2, v0

    .line 117
    .local v1, ratio:F
    const-string v3, "FlxAd"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "InterstitialPage.onWindowFocusChanged width:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " height:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ratio:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 118
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 117
    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    float-to-double v3, v1

    const-wide v5, 0x3fe6425aee631f8aL

    cmpg-double v3, v3, v5

    if-gez v3, :cond_0

    .line 120
    iget-object v3, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_START:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 124
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v3, p0, Lnet/flixster/android/ads/InterstitialPage;->mImageView:Landroid/widget/ImageView;

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0
.end method
