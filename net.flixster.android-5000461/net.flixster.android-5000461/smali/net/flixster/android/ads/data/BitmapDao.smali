.class public Lnet/flixster/android/ads/data/BitmapDao;
.super Ljava/lang/Object;
.source "BitmapDao.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lnet/flixster/android/ads/data/BitmapDao;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/ads/data/BitmapDao;->TAG:Ljava/lang/String;

    .line 18
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .parameter "c"

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lnet/flixster/android/ads/data/BitmapDao;->context:Landroid/content/Context;

    .line 25
    return-void
.end method


# virtual methods
.method public create(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3
    .parameter "key"
    .parameter "bit"

    .prologue
    .line 39
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/ads/data/BitmapDao;->context:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v0

    .line 40
    .local v0, stream:Ljava/io/FileOutputStream;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p2, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 43
    .end local v0           #stream:Ljava/io/FileOutputStream;
    :goto_0
    return-void

    .line 41
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public find(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .parameter "key"

    .prologue
    .line 29
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/ads/data/BitmapDao;->context:Landroid/content/Context;

    invoke-virtual {v1, p1}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 34
    :goto_0
    return-object v1

    .line 30
    :catch_0
    move-exception v0

    .line 31
    .local v0, e:Ljava/io/FileNotFoundException;
    sget-object v1, Lnet/flixster/android/ads/data/BitmapDao;->TAG:Ljava/lang/String;

    const-string v2, "Couldn\'t get image."

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 34
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 1
    .parameter "key"

    .prologue
    .line 46
    iget-object v0, p0, Lnet/flixster/android/ads/data/BitmapDao;->context:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 47
    return-void
.end method
