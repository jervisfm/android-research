.class public interface abstract Lnet/flixster/android/ads/data/GenericAdsDao;
.super Ljava/lang/Object;
.source "GenericAdsDao.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;
    }
.end annotation


# virtual methods
.method public abstract addListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V
.end method

.method public abstract create(Lnet/flixster/android/ads/model/Ad;)V
.end method

.method public abstract delete(Lnet/flixster/android/ads/model/Ad;)V
.end method

.method public abstract find(Ljava/lang/String;)Lnet/flixster/android/ads/model/Ad;
.end method

.method public abstract find(Ljava/lang/String;Landroid/os/Handler;)Lnet/flixster/android/ads/model/Ad;
.end method

.method public abstract getPayloadDump()Ljava/lang/String;
.end method

.method public abstract isInitialized()Z
.end method

.method public abstract removeListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V
.end method
