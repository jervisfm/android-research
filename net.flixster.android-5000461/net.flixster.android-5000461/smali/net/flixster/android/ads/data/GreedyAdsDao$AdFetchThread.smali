.class final Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;
.super Ljava/lang/Thread;
.source "GreedyAdsDao.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ads/data/GreedyAdsDao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AdFetchThread"
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;


# direct methods
.method private constructor <init>(Lnet/flixster/android/ads/data/GreedyAdsDao;)V
    .locals 0
    .parameter

    .prologue
    .line 104
    iput-object p1, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/ads/data/GreedyAdsDao;Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;-><init>(Lnet/flixster/android/ads/data/GreedyAdsDao;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 107
    const-class v2, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->setName(Ljava/lang/String;)V

    .line 109
    :try_start_0
    const-string v2, "FlxAd"

    const-string v3, "GreedyAdsDao.AdFetchThread.run"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    #calls: Lnet/flixster/android/ads/data/GreedyAdsDao;->getAds()Ljava/util/Map;
    invoke-static {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$0(Lnet/flixster/android/ads/data/GreedyAdsDao;)Ljava/util/Map;

    .line 111
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    const/4 v3, 0x1

    #setter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->isInitialized:Z
    invoke-static {v2, v3}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$1(Lnet/flixster/android/ads/data/GreedyAdsDao;Z)V

    .line 112
    const-string v2, "FlxAd"

    const-string v3, "GreedyAdsDao.AdFetchThread.run completed"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :try_start_1
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    #getter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$2(Lnet/flixster/android/ads/data/GreedyAdsDao;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 115
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    #getter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastHandler:Landroid/os/Handler;
    invoke-static {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$2(Lnet/flixster/android/ads/data/GreedyAdsDao;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 116
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    const/4 v3, 0x0

    #setter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastHandler:Landroid/os/Handler;
    invoke-static {v2, v3}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$3(Lnet/flixster/android/ads/data/GreedyAdsDao;Landroid/os/Handler;)V

    .line 113
    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 119
    :try_start_2
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    const/4 v3, 0x0

    #setter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->mNetworkRetryCount:I
    invoke-static {v2, v3}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$4(Lnet/flixster/android/ads/data/GreedyAdsDao;I)V

    .line 122
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    #getter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->listeners:Ljava/util/List;
    invoke-static {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$5(Lnet/flixster/android/ads/data/GreedyAdsDao;)Ljava/util/List;

    move-result-object v3

    monitor-enter v3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 123
    :try_start_3
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    #getter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->listeners:Ljava/util/List;
    invoke-static {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$5(Lnet/flixster/android/ads/data/GreedyAdsDao;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 122
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 136
    :goto_1
    return-void

    .line 113
    :catchall_0
    move-exception v2

    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v2
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 127
    :catch_0
    move-exception v0

    .line 128
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "FlxAd"

    const-string v3, "GreedyAdsDao.AdFetchThread failed to fetch ads"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 129
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    #getter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->mNetworkRetryCount:I
    invoke-static {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$6(Lnet/flixster/android/ads/data/GreedyAdsDao;)I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_2

    .line 130
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    #getter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->mNetworkRetryCount:I
    invoke-static {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$6(Lnet/flixster/android/ads/data/GreedyAdsDao;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    #setter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->mNetworkRetryCount:I
    invoke-static {v2, v3}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$4(Lnet/flixster/android/ads/data/GreedyAdsDao;I)V

    .line 131
    new-instance v2, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;

    iget-object v3, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    invoke-direct {v2, v3}, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;-><init>(Lnet/flixster/android/ads/data/GreedyAdsDao;)V

    invoke-virtual {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->start()V

    goto :goto_1

    .line 123
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1
    :try_start_6
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;

    .line 124
    .local v1, listener:Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;
    invoke-interface {v1}, Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;->onAdsLoaded()V

    goto :goto_0

    .line 122
    .end local v1           #listener:Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v2
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 133
    .restart local v0       #e:Ljava/lang/Exception;
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->this$0:Lnet/flixster/android/ads/data/GreedyAdsDao;

    #setter for: Lnet/flixster/android/ads/data/GreedyAdsDao;->mNetworkRetryCount:I
    invoke-static {v2, v5}, Lnet/flixster/android/ads/data/GreedyAdsDao;->access$4(Lnet/flixster/android/ads/data/GreedyAdsDao;I)V

    goto :goto_1
.end method
