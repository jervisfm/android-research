.class public Lnet/flixster/android/ads/data/GreedyAdsDao;
.super Ljava/lang/Object;
.source "GreedyAdsDao.java"

# interfaces
.implements Lnet/flixster/android/ads/data/GenericAdsDao;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;
    }
.end annotation


# static fields
.field private static final MAX_RETRYS:I = 0x2


# instance fields
.field private adMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lnet/flixster/android/ads/model/Ad;",
            ">;"
        }
    .end annotation
.end field

.field private volatile isInitialized:Z

.field private final listeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mLastHandler:Landroid/os/Handler;

.field private mLastUpdate:J

.field private mNetworkRetryCount:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->listeners:Ljava/util/List;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->adMap:Ljava/util/HashMap;

    .line 39
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastUpdate:J

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mNetworkRetryCount:I

    .line 43
    invoke-direct {p0}, Lnet/flixster/android/ads/data/GreedyAdsDao;->refreshAdsIfNeeded()V

    .line 44
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/ads/data/GreedyAdsDao;)Ljava/util/Map;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 87
    invoke-direct {p0}, Lnet/flixster/android/ads/data/GreedyAdsDao;->getAds()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/ads/data/GreedyAdsDao;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 36
    iput-boolean p1, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->isInitialized:Z

    return-void
.end method

.method static synthetic access$2(Lnet/flixster/android/ads/data/GreedyAdsDao;)Landroid/os/Handler;
    .locals 1
    .parameter

    .prologue
    .line 35
    iget-object v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lnet/flixster/android/ads/data/GreedyAdsDao;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 35
    iput-object p1, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$4(Lnet/flixster/android/ads/data/GreedyAdsDao;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40
    iput p1, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mNetworkRetryCount:I

    return-void
.end method

.method static synthetic access$5(Lnet/flixster/android/ads/data/GreedyAdsDao;)Ljava/util/List;
    .locals 1
    .parameter

    .prologue
    .line 33
    iget-object v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->listeners:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$6(Lnet/flixster/android/ads/data/GreedyAdsDao;)I
    .locals 1
    .parameter

    .prologue
    .line 40
    iget v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mNetworkRetryCount:I

    return v0
.end method

.method private getAds()Ljava/util/Map;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lnet/flixster/android/ads/model/Ad;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 88
    iget-object v5, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->adMap:Ljava/util/HashMap;

    if-eqz v5, :cond_0

    .line 89
    iget-object v5, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->adMap:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    .line 91
    :cond_0
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->ads()Ljava/lang/String;

    move-result-object v4

    .line 92
    .local v4, url:Ljava/lang/String;
    new-instance v5, Ljava/net/URL;

    invoke-direct {v5, v4}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6, v6}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;

    move-result-object v3

    .line 96
    .local v3, response:Ljava/lang/String;
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v3}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 97
    .local v1, array:Lorg/json/JSONArray;
    invoke-static {v1}, Lnet/flixster/android/ads/AdUtils;->parseAds(Lorg/json/JSONArray;)Ljava/util/List;

    move-result-object v2

    .line 98
    .local v2, payload:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/ads/model/Ad;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-nez v6, :cond_1

    .line 101
    iget-object v5, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->adMap:Ljava/util/HashMap;

    return-object v5

    .line 98
    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/Ad;

    .line 99
    .local v0, ad:Lnet/flixster/android/ads/model/Ad;
    invoke-virtual {p0, v0}, Lnet/flixster/android/ads/data/GreedyAdsDao;->create(Lnet/flixster/android/ads/model/Ad;)V

    goto :goto_0
.end method

.method private refreshAdsIfNeeded()V
    .locals 6

    .prologue
    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 81
    .local v0, now:J
    iget-wide v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastUpdate:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x1b7740

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 82
    iput-wide v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastUpdate:J

    .line 83
    new-instance v2, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;-><init>(Lnet/flixster/android/ads/data/GreedyAdsDao;Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;)V

    invoke-virtual {v2}, Lnet/flixster/android/ads/data/GreedyAdsDao$AdFetchThread;->start()V

    .line 85
    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V
    .locals 2
    .parameter "listener"

    .prologue
    .line 162
    iget-object v1, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 163
    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    monitor-exit v1

    .line 165
    return-void

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public create(Lnet/flixster/android/ads/model/Ad;)V
    .locals 2
    .parameter "ad"

    .prologue
    .line 53
    iget-object v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->adMap:Ljava/util/HashMap;

    iget-object v1, p1, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public delete(Lnet/flixster/android/ads/model/Ad;)V
    .locals 2
    .parameter "ad"

    .prologue
    .line 58
    iget-object v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->adMap:Ljava/util/HashMap;

    iget-object v1, p1, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method

.method public find(Ljava/lang/String;)Lnet/flixster/android/ads/model/Ad;
    .locals 4
    .parameter "id"

    .prologue
    .line 63
    iget-object v1, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->adMap:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/Ad;

    .line 64
    .local v0, result:Lnet/flixster/android/ads/model/Ad;
    const-string v1, "FlxAd"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GreedyAdsDao.find "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-object v0
.end method

.method public find(Ljava/lang/String;Landroid/os/Handler;)Lnet/flixster/android/ads/model/Ad;
    .locals 1
    .parameter "placement"
    .parameter "handler"

    .prologue
    .line 70
    invoke-direct {p0}, Lnet/flixster/android/ads/data/GreedyAdsDao;->refreshAdsIfNeeded()V

    .line 71
    if-eqz p2, :cond_0

    .line 72
    monitor-enter p0

    .line 73
    :try_start_0
    iput-object p2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->mLastHandler:Landroid/os/Handler;

    .line 72
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_0
    invoke-virtual {p0, p1}, Lnet/flixster/android/ads/data/GreedyAdsDao;->find(Ljava/lang/String;)Lnet/flixster/android/ads/model/Ad;

    move-result-object v0

    return-object v0

    .line 72
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getPayloadDump()Ljava/lang/String;
    .locals 5

    .prologue
    .line 141
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 142
    .local v1, dump:Ljava/lang/StringBuilder;
    iget-object v2, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->adMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    .line 157
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 142
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/Ad;

    .line 143
    .local v0, ad:Lnet/flixster/android/ads/model/Ad;
    iget-object v2, v0, Lnet/flixster/android/ads/model/Ad;->placement:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    instance-of v2, v0, Lnet/flixster/android/ads/model/TaggableAd;

    if-eqz v2, :cond_1

    .line 145
    const-string v2, "  tag:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object v2, v0

    check-cast v2, Lnet/flixster/android/ads/model/TaggableAd;

    iget-object v2, v2, Lnet/flixster/android/ads/model/TaggableAd;->tag:Ljava/lang/Integer;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 147
    :cond_1
    const-string v2, "  target:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v0, Lnet/flixster/android/ads/model/Ad;->target:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    iget-object v2, v0, Lnet/flixster/android/ads/model/Ad;->contextId:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 149
    const-string v2, "  ctxtId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, v0, Lnet/flixster/android/ads/model/Ad;->contextId:Ljava/lang/Long;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 152
    :cond_2
    instance-of v2, v0, Lnet/flixster/android/ads/model/AdMobAd;

    if-eqz v2, :cond_3

    .line 153
    const-string v2, "  chId:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    check-cast v0, Lnet/flixster/android/ads/model/AdMobAd;

    .end local v0           #ad:Lnet/flixster/android/ads/model/Ad;
    iget-object v4, v0, Lnet/flixster/android/ads/model/AdMobAd;->channelId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 155
    :cond_3
    const-string v2, "\n\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->isInitialized:Z

    return v0
.end method

.method public removeListener(Lnet/flixster/android/ads/data/GenericAdsDao$AdsListener;)V
    .locals 2
    .parameter "listener"

    .prologue
    .line 169
    iget-object v1, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->listeners:Ljava/util/List;

    monitor-enter v1

    .line 170
    :try_start_0
    iget-object v0, p0, Lnet/flixster/android/ads/data/GreedyAdsDao;->listeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 169
    monitor-exit v1

    .line 172
    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
