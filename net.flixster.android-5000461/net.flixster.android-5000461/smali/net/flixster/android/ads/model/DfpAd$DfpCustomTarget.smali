.class public Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;
.super Ljava/lang/Object;
.source "DfpAd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ads/model/DfpAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DfpCustomTarget"
.end annotation


# static fields
.field public static final CUST_PARAMS_KEY:Ljava/lang/String; = "cust_params"


# instance fields
.field public celebrityId:Ljava/lang/String;

.field public certifiedFresh:Ljava/lang/Boolean;

.field public freshRotten:Ljava/lang/String;

.field public genres:[Ljava/lang/String;

.field public movieId:Ljava/lang/String;

.field public mpaa:Ljava/lang/String;


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .parameter "actorId"

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->celebrityId:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public constructor <init>(Lnet/flixster/android/model/Movie;)V
    .locals 2
    .parameter "m"

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->movieId:Ljava/lang/String;

    .line 72
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getGenres()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->genres:[Ljava/lang/String;

    .line 73
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->getMpaa()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->mpaa:Ljava/lang/String;

    .line 74
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->isFresh()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "fresh"

    :goto_0
    iput-object v0, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->freshRotten:Ljava/lang/String;

    .line 75
    invoke-virtual {p1}, Lnet/flixster/android/model/Movie;->isCertifiedFresh()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->certifiedFresh:Ljava/lang/Boolean;

    .line 76
    return-void

    .line 74
    :cond_0
    const-string v0, "rotten"

    goto :goto_0
.end method

.method protected static addCustomKeyValuePairsTo(Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;Lcom/google/ads/AdRequest;)V
    .locals 7
    .parameter "target"
    .parameter "request"

    .prologue
    .line 88
    invoke-static {}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->getTestFlag()Ljava/lang/String;

    move-result-object v4

    .line 89
    .local v4, testFlag:Ljava/lang/String;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    .line 90
    const-string v5, "target"

    invoke-virtual {p1, v5, v4}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 92
    :cond_0
    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->movieId:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 93
    const-string v5, "movie_id"

    iget-object v6, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->movieId:Ljava/lang/String;

    invoke-virtual {p1, v5, v6}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 95
    :cond_1
    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->mpaa:Ljava/lang/String;

    if-eqz v5, :cond_2

    .line 96
    const-string v5, "mpaa"

    iget-object v6, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->mpaa:Ljava/lang/String;

    invoke-virtual {p1, v5, v6}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 98
    :cond_2
    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->freshRotten:Ljava/lang/String;

    if-eqz v5, :cond_3

    .line 99
    const-string v5, "fresh_rotten"

    iget-object v6, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->freshRotten:Ljava/lang/String;

    invoke-virtual {p1, v5, v6}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 101
    :cond_3
    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->certifiedFresh:Ljava/lang/Boolean;

    if-eqz v5, :cond_4

    .line 102
    const-string v6, "certified_fresh"

    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->certifiedFresh:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_7

    const-string v5, "1"

    :goto_0
    invoke-virtual {p1, v6, v5}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 104
    :cond_4
    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->celebrityId:Ljava/lang/String;

    if-eqz v5, :cond_5

    .line 105
    const-string v5, "celebrity_id"

    iget-object v6, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->celebrityId:Ljava/lang/String;

    invoke-virtual {p1, v5, v6}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 107
    :cond_5
    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->genres:[Ljava/lang/String;

    if-eqz v5, :cond_6

    .line 108
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 109
    .local v3, sb:Ljava/lang/StringBuilder;
    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->genres:[Ljava/lang/String;

    array-length v2, v5

    .line 110
    .local v2, numOfGenres:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    if-lt v1, v2, :cond_8

    .line 119
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_6

    .line 120
    const-string v5, "genre"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->formatGenres(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 123
    .end local v1           #i:I
    .end local v2           #numOfGenres:I
    .end local v3           #sb:Ljava/lang/StringBuilder;
    :cond_6
    const-string v5, "app_version"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v5, v6}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 124
    return-void

    .line 102
    :cond_7
    const-string v5, "0"

    goto :goto_0

    .line 111
    .restart local v1       #i:I
    .restart local v2       #numOfGenres:I
    .restart local v3       #sb:Ljava/lang/StringBuilder;
    :cond_8
    iget-object v5, p0, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->genres:[Ljava/lang/String;

    aget-object v0, v5, v1

    .line 112
    .local v0, genre:Ljava/lang/String;
    if-eqz v0, :cond_9

    .line 113
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    add-int/lit8 v5, v1, 0x1

    if-ge v5, v2, :cond_9

    .line 115
    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static addCustomTargetPairTo(Ljava/lang/String;Lnet/flixster/android/model/Movie;Lcom/google/ads/interactivemedia/api/SimpleAdsRequest;)Ljava/lang/String;
    .locals 12
    .parameter "adTagUrl"
    .parameter "movie"
    .parameter "imaRequest"

    .prologue
    .line 145
    new-instance v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    invoke-direct {v6, p1}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;-><init>(Lnet/flixster/android/model/Movie;)V

    .line 146
    .local v6, target:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;
    invoke-static {}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->getTestFlag()Ljava/lang/String;

    move-result-object v8

    .line 147
    .local v8, testFlag:Ljava/lang/String;
    const-string v0, "%26"

    .line 148
    .local v0, delimiter:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 149
    .local v4, pairs:Ljava/lang/StringBuilder;
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "app_version%3D"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 150
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_0

    .line 151
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "target%3D"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 153
    :cond_0
    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->movieId:Ljava/lang/String;

    if-eqz v9, :cond_1

    .line 154
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "movie_id%3D"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->movieId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    :cond_1
    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->mpaa:Ljava/lang/String;

    if-eqz v9, :cond_2

    .line 157
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "mpaa%3D"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->mpaa:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    :cond_2
    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->freshRotten:Ljava/lang/String;

    if-eqz v9, :cond_3

    .line 160
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "fresh_rotten%3D"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->freshRotten:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 162
    :cond_3
    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->certifiedFresh:Ljava/lang/Boolean;

    if-eqz v9, :cond_4

    .line 163
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "certified_fresh%3D"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->certifiedFresh:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_8

    const-string v9, "1"

    :goto_0
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 165
    :cond_4
    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->celebrityId:Ljava/lang/String;

    if-eqz v9, :cond_5

    .line 166
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "celebrity_id%3D"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->celebrityId:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 168
    :cond_5
    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->genres:[Ljava/lang/String;

    if-eqz v9, :cond_6

    .line 169
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 170
    .local v5, sb:Ljava/lang/StringBuilder;
    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->genres:[Ljava/lang/String;

    array-length v3, v9

    .line 171
    .local v3, numOfGenres:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-lt v2, v3, :cond_9

    .line 180
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-lez v9, :cond_6

    .line 181
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v10, "genre%3D"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->formatGenres(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 185
    .end local v2           #i:I
    .end local v3           #numOfGenres:I
    .end local v5           #sb:Ljava/lang/StringBuilder;
    :cond_6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v9

    if-lez v9, :cond_7

    .line 186
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "&t="

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 187
    .local v7, targeting:Ljava/lang/String;
    const-string v9, "FlxAd"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "DfpCustomTarget.addCustomTargetPairTo "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 190
    .end local v7           #targeting:Ljava/lang/String;
    :cond_7
    return-object p0

    .line 163
    :cond_8
    const-string v9, "0"

    goto/16 :goto_0

    .line 172
    .restart local v2       #i:I
    .restart local v3       #numOfGenres:I
    .restart local v5       #sb:Ljava/lang/StringBuilder;
    :cond_9
    iget-object v9, v6, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->genres:[Ljava/lang/String;

    aget-object v1, v9, v2

    .line 173
    .local v1, genre:Ljava/lang/String;
    if-eqz v1, :cond_a

    .line 174
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 175
    add-int/lit8 v9, v2, 0x1

    if-ge v9, v3, :cond_a

    .line 176
    const-string v9, ","

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 171
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1
.end method

.method public static addCustomTargetPairTo(Lcom/google/ads/AdRequest;)V
    .locals 3
    .parameter "request"

    .prologue
    .line 128
    invoke-static {}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->getTestFlag()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, testFlag:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 130
    const-string v1, "target"

    invoke-virtual {p0, v1, v0}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 132
    :cond_0
    const-string v1, "app_version"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/google/ads/AdRequest;->addExtra(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/ads/AdRequest;

    .line 133
    return-void
.end method

.method private static formatGenres(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "genres"

    .prologue
    .line 199
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, " &"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v1, " "

    const-string v2, "-"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getTestFlag()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdAdminDoubleClickTest()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public addCustomKeyValuePairsTo(Lcom/google/ads/AdRequest;)V
    .locals 0
    .parameter "request"

    .prologue
    .line 83
    invoke-static {p0, p1}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->addCustomKeyValuePairsTo(Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;Lcom/google/ads/AdRequest;)V

    .line 84
    return-void
.end method
