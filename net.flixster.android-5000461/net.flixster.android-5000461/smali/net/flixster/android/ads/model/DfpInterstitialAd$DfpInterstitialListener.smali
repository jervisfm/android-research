.class Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;
.super Ljava/lang/Object;
.source "DfpInterstitialAd.java"

# interfaces
.implements Lcom/google/ads/AdListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ads/model/DfpInterstitialAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DfpInterstitialListener"
.end annotation


# instance fields
.field private isAdClicked:Z

.field final synthetic this$0:Lnet/flixster/android/ads/model/DfpInterstitialAd;


# direct methods
.method private constructor <init>(Lnet/flixster/android/ads/model/DfpInterstitialAd;)V
    .locals 1
    .parameter

    .prologue
    .line 57
    iput-object p1, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->this$0:Lnet/flixster/android/ads/model/DfpInterstitialAd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->isAdClicked:Z

    return-void
.end method

.method synthetic constructor <init>(Lnet/flixster/android/ads/model/DfpInterstitialAd;Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;-><init>(Lnet/flixster/android/ads/model/DfpInterstitialAd;)V

    return-void
.end method


# virtual methods
.method public onDismissScreen(Lcom/google/ads/Ad;)V
    .locals 5
    .parameter "ad"

    .prologue
    .line 99
    const-string v0, "FlxAd"

    const-string v1, "DfpInterstitialListener.onDismissScreen"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    iget-boolean v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->isAdClicked:Z

    if-nez v0, :cond_0

    .line 101
    iget-object v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->this$0:Lnet/flixster/android/ads/model/DfpInterstitialAd;

    #getter for: Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z
    invoke-static {v0}, Lnet/flixster/android/ads/model/DfpInterstitialAd;->access$0(Lnet/flixster/android/ads/model/DfpInterstitialAd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/launch/interstitial"

    const-string v2, "launch"

    const-string v3, "LaunchInterstitial"

    const-string v4, "Skip"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :cond_0
    return-void
.end method

.method public onFailedToReceiveAd(Lcom/google/ads/Ad;Lcom/google/ads/AdRequest$ErrorCode;)V
    .locals 3
    .parameter "ad"
    .parameter "errorCode"

    .prologue
    .line 94
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DfpInterstitialListener.onFailedToReceiveAd errorCode: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->sw(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    return-void
.end method

.method public onLeaveApplication(Lcom/google/ads/Ad;)V
    .locals 5
    .parameter "ad"

    .prologue
    .line 85
    const-string v0, "FlxAd"

    const-string v1, "DfpInterstitialListener.onLeaveApplication"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->isAdClicked:Z

    .line 87
    iget-object v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->this$0:Lnet/flixster/android/ads/model/DfpInterstitialAd;

    #getter for: Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z
    invoke-static {v0}, Lnet/flixster/android/ads/model/DfpInterstitialAd;->access$0(Lnet/flixster/android/ads/model/DfpInterstitialAd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/launch/interstitial"

    const-string v2, "launch"

    const-string v3, "LaunchInterstitial"

    const-string v4, "Click"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_0
    return-void
.end method

.method public onPresentScreen(Lcom/google/ads/Ad;)V
    .locals 5
    .parameter "ad"

    .prologue
    .line 77
    const-string v0, "FlxAd"

    const-string v1, "DfpInterstitialListener.onPresentScreen"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    iget-object v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->this$0:Lnet/flixster/android/ads/model/DfpInterstitialAd;

    #getter for: Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z
    invoke-static {v0}, Lnet/flixster/android/ads/model/DfpInterstitialAd;->access$0(Lnet/flixster/android/ads/model/DfpInterstitialAd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    invoke-static {}, Lcom/flixster/android/analytics/Trackers;->instance()Lcom/flixster/android/analytics/Tracker;

    move-result-object v0

    const-string v1, "/launch/interstitial"

    const-string v2, "launch"

    const-string v3, "LaunchInterstitial"

    const-string v4, "Impression"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/flixster/android/analytics/Tracker;->trackEvent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    :cond_0
    return-void
.end method

.method public onReceiveAd(Lcom/google/ads/Ad;)V
    .locals 3
    .parameter "ad"

    .prologue
    .line 62
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->isTopLevelActivityOkayToShowInterstitial()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    const-string v1, "FlxAd"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "DfpInterstitialListener.onReceiveAd showing "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->this$0:Lnet/flixster/android/ads/model/DfpInterstitialAd;

    #getter for: Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z
    invoke-static {v0}, Lnet/flixster/android/ads/model/DfpInterstitialAd;->access$0(Lnet/flixster/android/ads/model/DfpInterstitialAd;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "launch"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " interstitial"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 63
    invoke-static {v1, v0}, Lcom/flixster/android/utils/Logger;->si(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    check-cast p1, Lcom/google/ads/InterstitialAd;

    .end local p1
    invoke-virtual {p1}, Lcom/google/ads/InterstitialAd;->show()V

    .line 73
    :cond_0
    :goto_1
    return-void

    .line 64
    .restart local p1
    :cond_1
    const-string v0, "post-trailer"

    goto :goto_0

    .line 67
    :cond_2
    const-string v1, "FlxAd"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "DfpInterstitialListener.onReceiveAd skipping "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 68
    iget-object v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->this$0:Lnet/flixster/android/ads/model/DfpInterstitialAd;

    #getter for: Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z
    invoke-static {v0}, Lnet/flixster/android/ads/model/DfpInterstitialAd;->access$0(Lnet/flixster/android/ads/model/DfpInterstitialAd;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "launch"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " interstitial"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 67
    invoke-static {v1, v0}, Lcom/flixster/android/utils/Logger;->sw(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    iget-object v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;->this$0:Lnet/flixster/android/ads/model/DfpInterstitialAd;

    #getter for: Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z
    invoke-static {v0}, Lnet/flixster/android/ads/model/DfpInterstitialAd;->access$0(Lnet/flixster/android/ads/model/DfpInterstitialAd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-static {}, Lcom/flixster/android/ads/AdsArbiter;->launch()Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/ads/AdsArbiter$LaunchAdsArbiter;->reshowLater()V

    goto :goto_1

    .line 68
    :cond_3
    const-string v0, "post-trailer"

    goto :goto_2
.end method
