.class public Lnet/flixster/android/ads/model/DfpInterstitialAd;
.super Ljava/lang/Object;
.source "DfpInterstitialAd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;
    }
.end annotation


# static fields
.field private static final DFP_AD_UNIT_LAUNCH:Ljava/lang/String; = "launch"

.field private static final DFP_AD_UNIT_POST_TRAILER:Ljava/lang/String; = "post_trailer"

.field private static final DFP_NETWORK_CODE:Ljava/lang/String; = "6327"

.field private static final DFP_PUBLISHER_CODE:Ljava/lang/String; = "mob.flix.android"


# instance fields
.field private final isLaunchInterstitial:Z


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    instance-of v0, p0, Lcom/flixster/android/ads/DfpLaunchInterstitial;

    if-eqz v0, :cond_0

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z

    .line 35
    :goto_0
    return-void

    .line 30
    :cond_0
    instance-of v0, p0, Lcom/flixster/android/ads/DfpPostTrailerInterstitial;

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method static synthetic access$0(Lnet/flixster/android/ads/model/DfpInterstitialAd;)Z
    .locals 1
    .parameter

    .prologue
    .line 25
    iget-boolean v0, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z

    return v0
.end method

.method private buildAdUnitId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 50
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "6327"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 51
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "mob.flix.android"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 52
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 53
    iget-boolean v1, p0, Lnet/flixster/android/ads/model/DfpInterstitialAd;->isLaunchInterstitial:Z

    if-eqz v1, :cond_0

    const-string v1, "launch"

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 54
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 53
    :cond_0
    const-string v1, "post_trailer"

    goto :goto_0
.end method


# virtual methods
.method public loadAd(Landroid/app/Activity;Lnet/flixster/android/model/Movie;)V
    .locals 4
    .parameter "activity"
    .parameter "targetMovie"

    .prologue
    .line 38
    new-instance v1, Lcom/google/ads/InterstitialAd;

    invoke-direct {p0}, Lnet/flixster/android/ads/model/DfpInterstitialAd;->buildAdUnitId()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/google/ads/InterstitialAd;-><init>(Landroid/app/Activity;Ljava/lang/String;)V

    .line 39
    .local v1, interstitial:Lcom/google/ads/InterstitialAd;
    new-instance v2, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;-><init>(Lnet/flixster/android/ads/model/DfpInterstitialAd;Lnet/flixster/android/ads/model/DfpInterstitialAd$DfpInterstitialListener;)V

    invoke-virtual {v1, v2}, Lcom/google/ads/InterstitialAd;->setAdListener(Lcom/google/ads/AdListener;)V

    .line 40
    new-instance v0, Lcom/google/ads/AdRequest;

    invoke-direct {v0}, Lcom/google/ads/AdRequest;-><init>()V

    .line 41
    .local v0, adRequest:Lcom/google/ads/AdRequest;
    invoke-static {v0}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->addCustomTargetPairTo(Lcom/google/ads/AdRequest;)V

    .line 42
    if-eqz p2, :cond_0

    .line 43
    new-instance v2, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    invoke-direct {v2, p2}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;-><init>(Lnet/flixster/android/model/Movie;)V

    invoke-static {v2, v0}, Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;->addCustomKeyValuePairsTo(Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;Lcom/google/ads/AdRequest;)V

    .line 45
    :cond_0
    invoke-virtual {v1, v0}, Lcom/google/ads/InterstitialAd;->loadAd(Lcom/google/ads/AdRequest;)V

    .line 46
    return-void
.end method
