.class public Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;
.super Lcom/google/ads/AdSenseSpec;
.source "ExtendedAdSenseSpec.java"


# instance fields
.field private mUserCity:Ljava/lang/String;

.field private mUserCountry:Ljava/lang/String;

.field private mUserRegion:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "clientId"

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/ads/AdSenseSpec;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method


# virtual methods
.method public generateParameters(Landroid/content/Context;)Ljava/util/List;
    .locals 4
    .parameter "context"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/google/ads/AdSpec$Parameter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-super {p0, p1}, Lcom/google/ads/AdSenseSpec;->generateParameters(Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 51
    .local v0, parameters:Ljava/util/List;,"Ljava/util/List<Lcom/google/ads/AdSpec$Parameter;>;"
    iget-object v1, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserCountry:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 52
    new-instance v1, Lcom/google/ads/AdSpec$Parameter;

    const-string v2, "gl"

    iget-object v3, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserCountry:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/ads/AdSpec$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 54
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserCity:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 55
    new-instance v1, Lcom/google/ads/AdSpec$Parameter;

    const-string v2, "gcs"

    iget-object v3, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserCity:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/ads/AdSpec$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserRegion:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 58
    new-instance v1, Lcom/google/ads/AdSpec$Parameter;

    const-string v2, "gr"

    iget-object v3, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserRegion:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/ads/AdSpec$Parameter;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 61
    :cond_2
    return-object v0
.end method

.method public getUserCity()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserCity:Ljava/lang/String;

    return-object v0
.end method

.method public getUserCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserCountry:Ljava/lang/String;

    return-object v0
.end method

.method public setUserCity(Ljava/lang/String;)Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;
    .locals 0
    .parameter "userCity"

    .prologue
    .line 43
    iput-object p1, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserCity:Ljava/lang/String;

    .line 44
    return-object p0
.end method

.method public setUserCountry(Ljava/lang/String;)Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;
    .locals 0
    .parameter "userCountry"

    .prologue
    .line 35
    iput-object p1, p0, Lnet/flixster/android/ads/model/ExtendedAdSenseSpec;->mUserCountry:Ljava/lang/String;

    .line 36
    return-object p0
.end method
