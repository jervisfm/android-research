.class public final enum Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;
.super Ljava/lang/Enum;
.source "FlixsterAd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/ads/model/FlixsterAd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AdTarget"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

.field public static final enum HTML:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

.field public static final enum MOVIE:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

.field public static final enum PRODUCT:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

.field public static final enum TRAILER:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

.field public static final enum URL:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;


# instance fields
.field public final literal:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 5
    new-instance v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    const-string v1, "TRAILER"

    const-string v2, "trailer"

    invoke-direct {v0, v1, v3, v2}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->TRAILER:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    new-instance v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    const-string v1, "URL"

    const-string v2, "url"

    invoke-direct {v0, v1, v4, v2}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->URL:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    new-instance v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    const-string v1, "MOVIE"

    const-string v2, "movie"

    invoke-direct {v0, v1, v5, v2}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->MOVIE:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    new-instance v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    const-string v1, "PRODUCT"

    const-string v2, "product"

    invoke-direct {v0, v1, v6, v2}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->PRODUCT:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    new-instance v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    const-string v1, "HTML"

    const-string v2, "html"

    invoke-direct {v0, v1, v7, v2}, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->HTML:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    .line 4
    const/4 v0, 0x5

    new-array v0, v0, [Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->TRAILER:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    aput-object v1, v0, v3

    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->URL:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    aput-object v1, v0, v4

    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->MOVIE:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    aput-object v1, v0, v5

    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->PRODUCT:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    aput-object v1, v0, v6

    sget-object v1, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->HTML:Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    aput-object v1, v0, v7

    sput-object v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->ENUM$VALUES:[Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter "literal"

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 10
    iput-object p3, p0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->literal:Ljava/lang/String;

    .line 11
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    return-object v0
.end method

.method public static values()[Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;->ENUM$VALUES:[Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    array-length v1, v0

    new-array v2, v1, [Lnet/flixster/android/ads/model/FlixsterAd$AdTarget;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
