.class public Lnet/flixster/android/data/ActorDao;
.super Ljava/lang/Object;
.source "ActorDao.java"


# static fields
.field public static final FILTER_POPULAR:Ljava/lang/String; = "popular"

.field public static final FILTER_RANDOM:Ljava/lang/String; = "random"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getActor(J)Lnet/flixster/android/model/Actor;
    .locals 8
    .parameter "actorId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 39
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-static {p0, p1}, Lnet/flixster/android/data/ApiBuilder;->actor(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 47
    .local v4, response:Ljava/lang/String;
    :try_start_1
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 48
    .local v0, actorObject:Lorg/json/JSONObject;
    invoke-static {v0}, Lnet/flixster/android/data/ActorDao;->parseActor(Lorg/json/JSONObject;)Lnet/flixster/android/model/Actor;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v5

    return-object v5

    .line 40
    .end local v0           #actorObject:Lorg/json/JSONObject;
    .end local v4           #response:Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 41
    .local v3, mue:Ljava/net/MalformedURLException;
    new-instance v5, Ljava/lang/RuntimeException;

    invoke-direct {v5, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v5

    .line 42
    .end local v3           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 43
    .local v1, ie:Ljava/io/IOException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v5

    throw v5

    .line 49
    .end local v1           #ie:Ljava/io/IOException;
    .restart local v4       #response:Ljava/lang/String;
    :catch_2
    move-exception v2

    .line 50
    .local v2, je:Lorg/json/JSONException;
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v5

    throw v5
.end method

.method public static getTopActors(Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 1
    .parameter "filter"
    .parameter "limit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 32
    .local v0, actors:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Actor;>;"
    invoke-static {p0, p1, v0}, Lnet/flixster/android/data/ActorDao;->getTopActors(Ljava/lang/String;ILjava/util/List;)V

    .line 33
    return-object v0
.end method

.method private static getTopActors(Ljava/lang/String;ILjava/util/List;)V
    .locals 11
    .parameter "filter"
    .parameter "limit"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 66
    .local p2, actors:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Actor;>;"
    :try_start_0
    new-instance v8, Ljava/net/URL;

    invoke-static {p0, p1}, Lnet/flixster/android/data/ApiBuilder;->topActors(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 74
    .local v7, response:Ljava/lang/String;
    :try_start_1
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 75
    .local v2, actorsArray:Lorg/json/JSONArray;
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v8

    if-lt v3, v8, :cond_0

    .line 83
    return-void

    .line 67
    .end local v2           #actorsArray:Lorg/json/JSONArray;
    .end local v3           #i:I
    .end local v7           #response:Ljava/lang/String;
    :catch_0
    move-exception v6

    .line 68
    .local v6, mue:Ljava/net/MalformedURLException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 69
    .end local v6           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v4

    .line 70
    .local v4, ie:Ljava/io/IOException;
    invoke-static {v4}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v8

    throw v8

    .line 76
    .end local v4           #ie:Ljava/io/IOException;
    .restart local v2       #actorsArray:Lorg/json/JSONArray;
    .restart local v3       #i:I
    .restart local v7       #response:Ljava/lang/String;
    :cond_0
    :try_start_2
    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 77
    .local v1, actorObject:Lorg/json/JSONObject;
    invoke-static {v1}, Lnet/flixster/android/data/ActorDao;->parseActor(Lorg/json/JSONObject;)Lnet/flixster/android/model/Actor;

    move-result-object v0

    .line 78
    .local v0, actor:Lnet/flixster/android/model/Actor;
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 75
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 80
    .end local v0           #actor:Lnet/flixster/android/model/Actor;
    .end local v1           #actorObject:Lorg/json/JSONObject;
    .end local v2           #actorsArray:Lorg/json/JSONArray;
    .end local v3           #i:I
    :catch_2
    move-exception v5

    .line 81
    .local v5, je:Lorg/json/JSONException;
    invoke-static {v5}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v8

    throw v8
.end method

.method private static parseActor(Lorg/json/JSONObject;)Lnet/flixster/android/model/Actor;
    .locals 23
    .parameter "actorObject"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 86
    new-instance v3, Lnet/flixster/android/model/Actor;

    invoke-direct {v3}, Lnet/flixster/android/model/Actor;-><init>()V

    .line 87
    .local v3, actor:Lnet/flixster/android/model/Actor;
    const-string v20, "id"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v20

    move-wide/from16 v0, v20

    iput-wide v0, v3, Lnet/flixster/android/model/Actor;->id:J

    .line 88
    const-string v20, "name"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    .line 89
    const-string v20, "url"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 90
    const-string v20, "url"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->flixsterUrl:Ljava/lang/String;

    .line 92
    :cond_0
    const-string v20, "dob"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 93
    const-string v20, "dob"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 94
    .local v5, birthdayObject:Lorg/json/JSONObject;
    const-string v20, "year"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v19

    .line 95
    .local v19, year:I
    const-string v20, "month"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 96
    .local v8, month:I
    const-string v20, "day"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 97
    .local v6, day:I
    if-lez v6, :cond_1

    .line 98
    new-instance v20, Ljava/util/GregorianCalendar;

    add-int/lit8 v21, v8, -0x1

    move-object/from16 v0, v20

    move/from16 v1, v19

    move/from16 v2, v21

    invoke-direct {v0, v1, v2, v6}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual/range {v20 .. v20}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->birthDate:Ljava/util/Date;

    .line 99
    iget-object v0, v3, Lnet/flixster/android/model/Actor;->birthDate:Ljava/util/Date;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/flixster/android/utils/DateTimeHelper;->formatMonthDayYear(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->birthDay:Ljava/lang/String;

    .line 102
    .end local v5           #birthdayObject:Lorg/json/JSONObject;
    .end local v6           #day:I
    .end local v8           #month:I
    .end local v19           #year:I
    :cond_1
    const-string v20, "pob"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 103
    const-string v20, "FlxMain"

    const-string v21, "ActorDao.parseActor() has pob"

    invoke-static/range {v20 .. v21}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v20, "pob"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    .line 105
    const-string v20, "FlxMain"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "ActorDao.parseActor() pob:"

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lnet/flixster/android/model/Actor;->birthplace:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_2
    const-string v20, "bio"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 108
    const-string v20, "FlxMain"

    const-string v21, "ActorDao.parseActor() has bio"

    invoke-static/range {v20 .. v21}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v20, "bio"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 110
    .local v4, bio:Ljava/lang/String;
    const-string v20, "<br />"

    const-string v21, "\n"

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v4, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    .line 112
    const-string v20, "FlxMain"

    new-instance v21, Ljava/lang/StringBuilder;

    const-string v22, "ActorDao.parseActor() has bio:"

    invoke-direct/range {v21 .. v22}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v3, Lnet/flixster/android/model/Actor;->biography:Ljava/lang/String;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    .end local v4           #bio:Ljava/lang/String;
    :cond_3
    const-string v20, "photoCount"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 115
    const-string v20, "photoCount"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v20

    move/from16 v0, v20

    iput v0, v3, Lnet/flixster/android/model/Actor;->photoCount:I

    .line 117
    :cond_4
    const-string v20, "photo"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 118
    const-string v20, "photo"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v16

    .line 119
    .local v16, photoObject:Lorg/json/JSONObject;
    const-string v20, "thumbnail"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_5

    .line 120
    const-string v20, "thumbnail"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    .line 122
    :cond_5
    const-string v20, "lthumbnail"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 123
    const-string v20, "lthumbnail"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->largeUrl:Ljava/lang/String;

    .line 126
    .end local v16           #photoObject:Lorg/json/JSONObject;
    :cond_6
    const-string v20, "movies"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_7

    .line 127
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v13, movies:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    const-string v20, "movies"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v14

    .line 129
    .local v14, moviesArray:Lorg/json/JSONArray;
    const/4 v7, 0x0

    .local v7, i:I
    :goto_0
    invoke-virtual {v14}, Lorg/json/JSONArray;->length()I

    move-result v20

    move/from16 v0, v20

    if-lt v7, v0, :cond_9

    .line 136
    iput-object v13, v3, Lnet/flixster/android/model/Actor;->movies:Ljava/util/ArrayList;

    .line 138
    .end local v7           #i:I
    .end local v13           #movies:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    .end local v14           #moviesArray:Lorg/json/JSONArray;
    :cond_7
    const-string v20, "photos"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 139
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 140
    .local v17, photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    const-string v20, "photos"

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v18

    .line 141
    .local v18, photosArray:Lorg/json/JSONArray;
    const/4 v7, 0x0

    .restart local v7       #i:I
    :goto_1
    invoke-virtual/range {v18 .. v18}, Lorg/json/JSONArray;->length()I

    move-result v20

    move/from16 v0, v20

    if-lt v7, v0, :cond_a

    .line 147
    move-object/from16 v0, v17

    iput-object v0, v3, Lnet/flixster/android/model/Actor;->photos:Ljava/util/ArrayList;

    .line 149
    .end local v7           #i:I
    .end local v17           #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .end local v18           #photosArray:Lorg/json/JSONArray;
    :cond_8
    return-object v3

    .line 130
    .restart local v7       #i:I
    .restart local v13       #movies:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    .restart local v14       #moviesArray:Lorg/json/JSONArray;
    :cond_9
    invoke-virtual {v14, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v12

    .line 131
    .local v12, movieObject:Lorg/json/JSONObject;
    const-string v20, "id"

    move-object/from16 v0, v20

    invoke-virtual {v12, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 132
    .local v10, movieId:J
    invoke-static {v10, v11}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v9

    .line 133
    .local v9, movie:Lnet/flixster/android/model/Movie;
    invoke-virtual {v9, v12}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 134
    invoke-virtual {v13, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 142
    .end local v9           #movie:Lnet/flixster/android/model/Movie;
    .end local v10           #movieId:J
    .end local v12           #movieObject:Lorg/json/JSONObject;
    .end local v13           #movies:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    .end local v14           #moviesArray:Lorg/json/JSONArray;
    .restart local v17       #photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    .restart local v18       #photosArray:Lorg/json/JSONArray;
    :cond_a
    new-instance v15, Lnet/flixster/android/model/Photo;

    invoke-direct {v15}, Lnet/flixster/android/model/Photo;-><init>()V

    .line 143
    .local v15, photo:Lnet/flixster/android/model/Photo;
    move-object/from16 v0, v18

    invoke-virtual {v0, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v16

    .line 144
    .restart local v16       #photoObject:Lorg/json/JSONObject;
    const-string v20, "url"

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    iput-object v0, v15, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    .line 145
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    add-int/lit8 v7, v7, 0x1

    goto :goto_1
.end method

.method public static searchActors(Ljava/lang/String;Ljava/util/List;)V
    .locals 3
    .parameter "query"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 56
    .local p1, results:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Actor;>;"
    :try_start_0
    const-string v1, "UTF-8"

    invoke-static {p0, v1}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x19

    invoke-static {v1, v2, p1}, Lnet/flixster/android/data/ActorDao;->getTopActors(Ljava/lang/String;ILjava/util/List;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 61
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 59
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method
