.class public Lnet/flixster/android/data/ApiBuilder;
.super Ljava/lang/Object;
.source "ApiBuilder.java"


# static fields
.field private static final FLX_API_ENDPOINTS:[Ljava/lang/String; = null

.field private static final LOCKER_API_ENDPOINTS:[Ljava/lang/String; = null

.field private static final MAX_PHOTO:I = 0x32

.field private static final NETFLIX_USERS_URL:Ljava/lang/String; = "http://api-public.netflix.com/users/"

.field private static final SECURE_BASE_URL:Ljava/lang/String; = "https://secure.flixster.com/android/api/v1/"

.field private static final URI_DATE_FORMATTER:Ljava/text/SimpleDateFormat;

.field private static platformBasedDeviceType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lnet/flixster/android/data/ApiBuilder;->URI_DATE_FORMATTER:Ljava/text/SimpleDateFormat;

    .line 34
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "api.flixster.com"

    aput-object v1, v0, v2

    const-string v1, "staging.flixster.com"

    aput-object v1, v0, v3

    .line 35
    const-string v1, "community.flixsterqa.com"

    aput-object v1, v0, v4

    const-string v1, "communitypreview.flixsterqa.com"

    aput-object v1, v0, v5

    .line 34
    sput-object v0, Lnet/flixster/android/data/ApiBuilder;->FLX_API_ENDPOINTS:[Ljava/lang/String;

    .line 37
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "ld.flixster.com"

    aput-object v1, v0, v2

    const-string v1, "lockerstaging5.flixster.com"

    aput-object v1, v0, v3

    .line 38
    const-string v1, "lockerstaging5.flixster.com"

    aput-object v1, v0, v4

    const-string v1, "lockerstaging5.flixster.com"

    aput-object v1, v0, v5

    .line 37
    sput-object v0, Lnet/flixster/android/data/ApiBuilder;->LOCKER_API_ENDPOINTS:[Ljava/lang/String;

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static actor(J)Ljava/lang/String;
    .locals 3
    .parameter "actorId"

    .prologue
    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 313
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "actors/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 315
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static adTrack(ILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "tag"
    .parameter "event"
    .parameter "placement"

    .prologue
    .line 719
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 720
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "analytics/ads.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 721
    const-string v1, "?udt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 722
    const-string v1, "&pid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 723
    const-string v1, "&event="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 724
    const-string v1, "&placement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 725
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static ads()Ljava/lang/String;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 696
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 697
    .local v5, urlBuilder:Ljava/lang/StringBuilder;
    const-string v6, "ads.json?version="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 698
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 699
    const-string v6, "&model="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getDeviceModel()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 700
    const-string v6, "&osVersion="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAndroidBuildInt()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 702
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->getAvailableLatitude()D

    move-result-wide v0

    .line 703
    .local v0, lat:D
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->getAvailableLongitude()D

    move-result-wide v2

    .line 704
    .local v2, lng:D
    cmpl-double v6, v0, v8

    if-eqz v6, :cond_0

    cmpl-double v6, v2, v8

    if-eqz v6, :cond_0

    .line 705
    const-string v6, "&latitude="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "&longitude="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 707
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/LocationFacade;->getAvailableZip()Ljava/lang/String;

    move-result-object v4

    .line 708
    .local v4, postal:Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 709
    const-string v6, "&postal="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 711
    :cond_1
    const-string v6, "&conn="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isWifi()Z

    move-result v6

    if-eqz v6, :cond_2

    const-string v6, "2"

    :goto_0
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 712
    const-string v6, "&udt="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 713
    invoke-static {v5}, Lnet/flixster/android/data/ApiBuilder;->appendAdsParams(Ljava/lang/StringBuilder;)V

    .line 714
    invoke-static {v5}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 715
    invoke-static {v5}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6

    .line 711
    :cond_2
    const-string v6, "1"

    goto :goto_0
.end method

.method private static appendAdsParams(Ljava/lang/StringBuilder;)V
    .locals 9
    .parameter "builder"

    .prologue
    .line 670
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTicketEmail()Ljava/lang/String;

    move-result-object v1

    .line 671
    .local v1, esig:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/flixster/android/utils/CryptHelper;->encryptEsig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 672
    const-string v6, "&esig="

    invoke-virtual {p0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 675
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/OsInfo;->instance()Lcom/flixster/android/utils/OsInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flixster/android/utils/OsInfo;->getAdu()Ljava/lang/String;

    move-result-object v0

    .line 676
    .local v0, adu:Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 677
    const-string v6, "&adu="

    invoke-virtual {p0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 681
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getReferrer()Ljava/lang/String;

    move-result-object v2

    .line 682
    .local v2, referrer:Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 683
    invoke-static {v2}, Lcom/flixster/android/utils/UrlHelper;->getQueries(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 684
    .local v5, vars:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const-string v6, "utm_source"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 685
    .local v3, source:Ljava/lang/String;
    const-string v6, "tracking"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 686
    .local v4, tracking:Ljava/lang/String;
    const-string v6, "trialpay"

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v4, :cond_2

    .line 687
    const-string v6, "FlxAd"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "AdUtils.appendParams tp_sid="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "&tp_sid="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 691
    .end local v3           #source:Ljava/lang/String;
    .end local v4           #tracking:Ljava/lang/String;
    .end local v5           #vars:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_2
    return-void
.end method

.method private static appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 2
    .parameter "urlBuilder"

    .prologue
    .line 774
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    .line 775
    .local v0, countryCode:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 776
    const-string v1, "?"

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    const-string v1, "?"

    :goto_0
    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    const-string v1, "country="

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 779
    :cond_0
    return-object p0

    .line 776
    :cond_1
    const-string v1, "&"

    goto :goto_0
.end method

.method private static appendCountryParam(Ljava/util/ArrayList;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 661
    .local p0, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "country"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCountryCode()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 662
    return-void
.end method

.method private static appendCpiParams(Ljava/util/ArrayList;)V
    .locals 7
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 640
    .local p0, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getReferrer()Ljava/lang/String;

    move-result-object v0

    .line 641
    .local v0, referrer:Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 642
    invoke-static {v0}, Lcom/flixster/android/utils/UrlHelper;->getQueries(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v3

    .line 643
    .local v3, vars:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    const-string v4, "utm_source"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 644
    .local v1, source:Ljava/lang/String;
    const-string v4, "tracking"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/util/Map;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 647
    .local v2, tracking:Ljava/lang/String;
    const-string v4, "trialpay"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-eqz v2, :cond_0

    .line 648
    const-string v4, "FlxAd"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ApiBuilder.appendCpiParams tp_sid="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "tp_sid"

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    :cond_0
    const-string v4, "utm_medium"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "mdotm"

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 654
    const-string v4, "FlxAd"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ApiBuilder.appendCpiParams mdotm="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "mdotm"

    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 658
    .end local v1           #source:Ljava/lang/String;
    .end local v2           #tracking:Ljava/lang/String;
    .end local v3           #vars:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Ljava/lang/String;>;>;"
    :cond_1
    return-void
.end method

.method private static appendEmailParam(Ljava/util/ArrayList;)V
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 610
    .local p0, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTicketEmail()Ljava/lang/String;

    move-result-object v0

    .line 611
    .local v0, esig:Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/flixster/android/utils/CryptHelper;->encryptEsig(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 612
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "esig"

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 614
    :cond_0
    return-void
.end method

.method private static appendIdentifierParams(Ljava/util/ArrayList;)V
    .locals 6
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 617
    .local p0, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    invoke-static {}, Lcom/flixster/android/utils/OsInfo;->instance()Lcom/flixster/android/utils/OsInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/OsInfo;->getEncryptedTelephonyDeviceId()Ljava/lang/String;

    move-result-object v3

    .line 618
    .local v3, tdi:Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 619
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "tdi"

    invoke-direct {v4, v5, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/OsInfo;->instance()Lcom/flixster/android/utils/OsInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/OsInfo;->getEncryptedAndroidId()Ljava/lang/String;

    move-result-object v2

    .line 623
    .local v2, sai:Ljava/lang/String;
    if-eqz v2, :cond_1

    .line 624
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "sai"

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 627
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/OsInfo;->instance()Lcom/flixster/android/utils/OsInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/OsInfo;->getEncryptedSerialNumber()Ljava/lang/String;

    move-result-object v1

    .line 628
    .local v1, bsn:Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 629
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "bsn"

    invoke-direct {v4, v5, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 633
    :cond_2
    invoke-static {}, Lcom/flixster/android/utils/OsInfo;->instance()Lcom/flixster/android/utils/OsInfo;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/utils/OsInfo;->getAdu()Ljava/lang/String;

    move-result-object v0

    .line 634
    .local v0, adu:Ljava/lang/String;
    if-eqz v0, :cond_3

    .line 635
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "adu"

    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    :cond_3
    return-void
.end method

.method private static appendUserSessionParam(Ljava/util/ArrayList;)V
    .locals 0
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 665
    .local p0, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    invoke-static {p0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserPairs(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 666
    return-void
.end method

.method protected static boxOffice()Ljava/lang/String;
    .locals 3

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 115
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "movies.json?filter=iphone-popular&view=long"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 117
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    const-string v1, "&exp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 120
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 121
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static device()Ljava/lang/String;
    .locals 2

    .prologue
    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 576
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "device.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 588
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static deviceParams()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 592
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 593
    .local v0, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "visitType"

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/utils/Properties;->getVisitType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "version"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "deviceType"

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "deviceModel"

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getDeviceModel()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "osVersion"

    sget v3, Lcom/flixster/android/utils/F;->API_LEVEL:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 598
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "locale"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getLocale()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "conn"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isWifi()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "2"

    :goto_0
    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 600
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "udt"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendIdentifierParams(Ljava/util/ArrayList;)V

    .line 602
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendEmailParam(Ljava/util/ArrayList;)V

    .line 603
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCpiParams(Ljava/util/ArrayList;)V

    .line 604
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendUserSessionParam(Ljava/util/ArrayList;)V

    .line 605
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/util/ArrayList;)V

    .line 606
    return-object v0

    .line 599
    :cond_0
    const-string v1, "1"

    goto :goto_0
.end method

.method protected static dvdCategory(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "params"

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 156
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "movies.json?view=long"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 157
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 158
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    const-string v1, "&exp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 160
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 161
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static dvdComingSoon()Ljava/lang/String;
    .locals 3

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 146
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "movies.json?filter=coming-soon-dvds&view=long"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 148
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 149
    const-string v1, "&exp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 150
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 151
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static dvdNewRelease()Ljava/lang/String;
    .locals 3

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 136
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "movies.json?filter=new-release-dvds&view=long"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 138
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    const-string v1, "&exp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 140
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 141
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static faq()Ljava/lang/String;
    .locals 1

    .prologue
    .line 752
    const-string v0, "http://flixster.desk.com/customer/portal/topics/28130-movies-on-android"

    return-object v0
.end method

.method protected static fbInvites(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "userId"
    .parameter "requestId"

    .prologue
    .line 388
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 389
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 390
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/msk/fb-invites"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 391
    const-string v1, "?reqs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 392
    const-string v1, "&requestTextCode=mb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 393
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 394
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static feedback()Ljava/lang/String;
    .locals 1

    .prologue
    .line 756
    const-string v0, "http://flixster.desk.com/customer/widget/emails/new"

    return-object v0
.end method

.method protected static friendReviews(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "userId"

    .prologue
    .line 511
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 512
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 513
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/friends/activity.json?limit=50"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 514
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 515
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 516
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static friends(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .parameter "userId"
    .parameter "limit"

    .prologue
    .line 365
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 366
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 367
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/friends.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 368
    if-lez p1, :cond_0

    .line 369
    const-string v1, "?limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 373
    :goto_0
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 374
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 375
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 371
    :cond_0
    const-string v1, "?limit=50"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected static friendsIds(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "userId"

    .prologue
    .line 379
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 380
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 381
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/friends/msk/fb.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 382
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 383
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 384
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getBaseUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lnet/flixster/android/data/ApiBuilder;->FLX_API_ENDPOINTS:[Ljava/lang/String;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdminApiSource()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/android/api/v1/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getDeviceModel()Ljava/lang/String;
    .locals 2

    .prologue
    .line 886
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/flixster/android/utils/UrlHelper;->urlEncode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getGlue(Lnet/flixster/android/model/Movie;)Ljava/lang/String;
    .locals 5
    .parameter "m"

    .prologue
    .line 892
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://m.getglue.com"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 893
    .local v2, urlBuilder:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/data/ApiBuilder;->getGlueMovieName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 894
    .local v1, normalizedMovieName:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 895
    iget-object v3, p0, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 896
    iget-object v3, p0, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/Actor;

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    invoke-static {v3}, Lnet/flixster/android/data/ApiBuilder;->getGlueDirectorName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 897
    .local v0, normalizedDirectorName:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 898
    const-string v3, "/checkin/movies/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 899
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 903
    .end local v0           #normalizedDirectorName:Ljava/lang/String;
    :cond_0
    const-string v3, "?s=flixster"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 904
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static getGlueDirectorName(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "director"

    .prologue
    .line 936
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 937
    .local v1, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/flixster/android/utils/StringHelper;->replaceNonAlphaNumeric(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 938
    .local v2, tokens:[Ljava/lang/String;
    const/4 v3, 0x0

    aget-object v3, v2, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v2

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v2, v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 939
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 942
    .end local v1           #sb:Ljava/lang/StringBuilder;
    .end local v2           #tokens:[Ljava/lang/String;
    :goto_0
    return-object v3

    .line 940
    :catch_0
    move-exception v0

    .line 941
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "FlxMain"

    const-string v4, "ApiBuilder.getGlueDirectorName"

    invoke-static {v3, v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 942
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private static getGlueMovieName(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .parameter "title"

    .prologue
    const/4 v4, 0x0

    .line 913
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 914
    .local v2, sb:Ljava/lang/StringBuilder;
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    const-string v6, "\'"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/flixster/android/utils/StringHelper;->replaceNonAlphaNumeric(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "_"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    array-length v6, v5

    :goto_0
    if-lt v4, v6, :cond_1

    .line 919
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 920
    .local v1, result:Ljava/lang/String;
    const-string v4, "_"

    invoke-virtual {v1, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 921
    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 926
    .end local v1           #result:Ljava/lang/String;
    .end local v2           #sb:Ljava/lang/StringBuilder;
    :cond_0
    :goto_1
    return-object v1

    .line 914
    .restart local v2       #sb:Ljava/lang/StringBuilder;
    :cond_1
    aget-object v3, v5, v4

    .line 915
    .local v3, token:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_2

    const-string v7, "the"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "a"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "and"

    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 916
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 914
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 924
    .end local v2           #sb:Ljava/lang/StringBuilder;
    .end local v3           #token:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 925
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "FlxMain"

    const-string v5, "ApiBuilder.getGlueMovieName"

    invoke-static {v4, v5, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 926
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static getPlatformBasedDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 860
    sget-object v0, Lnet/flixster/android/data/ApiBuilder;->platformBasedDeviceType:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 861
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 862
    const-string v0, "Android_Tablet"

    sput-object v0, Lnet/flixster/android/data/ApiBuilder;->platformBasedDeviceType:Ljava/lang/String;

    .line 869
    :cond_0
    :goto_0
    sget-object v0, Lnet/flixster/android/data/ApiBuilder;->platformBasedDeviceType:Ljava/lang/String;

    return-object v0

    .line 863
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 864
    const-string v0, "Android_Gtv"

    sput-object v0, Lnet/flixster/android/data/ApiBuilder;->platformBasedDeviceType:Ljava/lang/String;

    goto :goto_0

    .line 866
    :cond_2
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getDeviceModel()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/data/ApiBuilder;->platformBasedDeviceType:Ljava/lang/String;

    goto :goto_0
.end method

.method private static getPlatformBasedDeviceType(Z)Ljava/lang/String;
    .locals 3
    .parameter "isSonicProxyMode"

    .prologue
    .line 874
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v0

    .line 875
    .local v0, customDeviceType:Ljava/lang/String;
    if-eqz p0, :cond_1

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/flixster/android/utils/F;->IS_NATIVE_HC_DRM_ENABLED:Z

    if-nez v1, :cond_1

    .line 876
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getDeviceModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 881
    :cond_0
    :goto_0
    return-object v0

    .line 877
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 878
    sget-boolean v1, Lcom/flixster/android/utils/F;->IS_NATIVE_HC_DRM_ENABLED:Z

    if-eqz v1, :cond_0

    .line 879
    const-string v0, "Android_Tablet"

    goto :goto_0
.end method

.method private static getSecureBaseUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lnet/flixster/android/data/ApiBuilder;->FLX_API_ENDPOINTS:[Ljava/lang/String;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdminApiSource()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/android/api/v1/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getSecureMskBaseUrl()Ljava/lang/String;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lnet/flixster/android/data/ApiBuilder;->LOCKER_API_ENDPOINTS:[Ljava/lang/String;

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdminApiSource()I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/msk/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getTheaterDistance()F
    .locals 3

    .prologue
    .line 951
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getTheaterDistance()I

    move-result v1

    int-to-float v0, v1

    .line 952
    .local v0, distance:F
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getDistanceType()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 953
    const v1, 0x3fcdf3b6

    div-float/2addr v0, v1

    .line 955
    :cond_0
    return v0
.end method

.method public static isAdTrack(Ljava/lang/String;)Z
    .locals 1
    .parameter "url"

    .prologue
    .line 729
    const-string v0, "analytics/ads.json"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method private static isQaApiServer()Z
    .locals 2

    .prologue
    .line 560
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getAdminApiSource()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSecureFlxUrl(Ljava/lang/String;)Z
    .locals 1
    .parameter "url"

    .prologue
    .line 291
    const-string v0, "https://secure.flixster.com/android/api/v1/"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getSecureBaseUrl()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static final isTrailerUrl(Ljava/lang/String;)Z
    .locals 1
    .parameter "url"

    .prologue
    .line 848
    const-string v0, "videodetective.net/player.aspx"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public static isVideoLink(Ljava/lang/String;)Z
    .locals 1
    .parameter "url"

    .prologue
    .line 852
    const-string v0, ".mp4"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".m4a"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, ".3gp"

    invoke-virtual {p0, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected static location(DD)Ljava/lang/String;
    .locals 2
    .parameter "latitude"
    .parameter "longitude"

    .prologue
    .line 341
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 342
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "cities.json?latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 343
    const-string v1, "&longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 344
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 345
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static location(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "query"

    .prologue
    .line 329
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 331
    .local v1, urlBuilder:Ljava/lang/StringBuilder;
    :try_start_0
    const-string v2, "cities.json?q="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-static {p0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 336
    :goto_0
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 337
    invoke-static {v1}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 332
    :catch_0
    move-exception v0

    .line 334
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method public static mobileUpsell()Ljava/lang/String;
    .locals 1

    .prologue
    .line 768
    const-string v0, "http://tmto.es/go2flix"

    return-object v0
.end method

.method protected static movie(J)Ljava/lang/String;
    .locals 3
    .parameter "movieId"

    .prologue
    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 180
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "movies/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 181
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 182
    const-string v1, "&exp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 183
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 184
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 185
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static netflixDeleteQueue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "queue"
    .parameter "titleRef"

    .prologue
    .line 839
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://api-public.netflix.com/users/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 840
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 841
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 842
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 844
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static netflixGetQueue(Ljava/lang/String;II)Ljava/lang/String;
    .locals 2
    .parameter "queue"
    .parameter "startIndex"
    .parameter "maxResults"

    .prologue
    .line 803
    const-string v1, "/queues/disc/available"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 804
    const-string p0, "/queues/disc"

    .line 807
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://api-public.netflix.com/users/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 808
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 809
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 810
    const-string v1, "?max_results="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 811
    const-string v1, "&start_index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 812
    const-string v1, "&output=json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 813
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static netflixMovies()Ljava/lang/String;
    .locals 1

    .prologue
    .line 785
    const-string v0, "http://api.flixster.com/android/api/v1/netflix/movies.json"

    return-object v0
.end method

.method protected static netflixPostQueue(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 5
    .parameter "queue"
    .parameter "titleRef"
    .parameter "nPosition"

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    .line 819
    const-string v2, "/available"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .local v0, end:I
    if-le v0, v3, :cond_2

    .line 820
    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 825
    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "http://api-public.netflix.com/users/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 826
    .local v1, urlBuilder:Ljava/lang/StringBuilder;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 827
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 828
    const-string v2, "?title_ref="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 829
    if-eqz p2, :cond_1

    .line 830
    const-string v2, "&position="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 832
    :cond_1
    const-string v2, "&output=json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 834
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 821
    .end local v1           #urlBuilder:Ljava/lang/StringBuilder;
    :cond_2
    const-string v2, "/saved"

    invoke-virtual {p0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    if-le v0, v3, :cond_0

    .line 822
    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method protected static netflixTitleState(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "titleRef"

    .prologue
    .line 791
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://api-public.netflix.com/users/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 792
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixUserId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793
    const-string v1, "/title_states?title_refs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 794
    const-string v1, "&output=json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 795
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static photoInfo(J)Ljava/lang/String;
    .locals 2
    .parameter "photoId"

    .prologue
    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "http://flixster.com/mobile/support/flag-photo?image="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static photos(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "args"

    .prologue
    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 298
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "photos.json?limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x32

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 299
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 300
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 301
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 302
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static promos()Ljava/lang/String;
    .locals 3

    .prologue
    .line 565
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 566
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "promos.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 567
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 568
    const-string v1, "&exp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 569
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static properties()Ljava/lang/String;
    .locals 3

    .prologue
    .line 735
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 736
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "properties.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 737
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 738
    const-string v1, "&udt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 739
    const-string v1, "&telephonySupported="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 740
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->isTelephonySupported()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 739
    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 742
    const-string v1, "&osVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/flixster/android/utils/F;->API_LEVEL:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 743
    const-string v1, "&deviceManufacturer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/flixster/android/utils/F;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 744
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 740
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected static quickRate(Z)Ljava/lang/String;
    .locals 3
    .parameter "isWts"

    .prologue
    .line 198
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 199
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "quickrate/movies.json?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    const-string v1, "&type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p0, :cond_0

    const-string v1, "wts"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 202
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 203
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 200
    :cond_0
    const-string v1, "rate"

    goto :goto_0
.end method

.method public static registerAccount()Ljava/lang/String;
    .locals 2

    .prologue
    .line 507
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getSecureMskBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "mobile0/flixsterReg/?standalone=true&native=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static rightDownload(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3
    .parameter "rightId"
    .parameter "reason"
    .parameter "sonicId"
    .parameter "playPosition"
    .parameter "isSonicProxyMode"

    .prologue
    .line 438
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 439
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "rights/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 440
    const-string v1, "/download.json?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 441
    if-eqz p2, :cond_0

    .line 442
    const-string v1, "&reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 444
    :cond_0
    if-eqz p3, :cond_1

    .line 445
    const-string v1, "&sonicQueueId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 447
    :cond_1
    if-eqz p4, :cond_2

    .line 448
    const-string v1, "&playPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 450
    :cond_2
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 451
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p5}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 452
    const-string v1, "&udt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 453
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 454
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static rightEpisodes(J)Ljava/lang/String;
    .locals 3
    .parameter "seasonRightId"

    .prologue
    .line 485
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 486
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "rights/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 487
    const-string v1, "/episodes.json?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 488
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 489
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 490
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 491
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static rightLicenseStats(JLjava/lang/String;ZZ)Ljava/lang/String;
    .locals 3
    .parameter "rightId"
    .parameter "streamId"
    .parameter "isSuccess"
    .parameter "isSonicProxyMode"

    .prologue
    .line 458
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 459
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "rights/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 460
    const-string v1, "/license/stats.json?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 461
    const-string v1, "&mediaTransfer="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackManager;->isStreamingMode()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "streaming"

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 462
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v1

    invoke-interface {v1}, Lcom/flixster/android/drm/PlaybackManager;->isStreamingMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 463
    const-string v1, "&streamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 465
    :cond_0
    const-string v1, "&status="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz p3, :cond_2

    const-string v1, "200"

    :goto_1
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 466
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 467
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p4}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 468
    const-string v1, "&udt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 469
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 470
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 461
    :cond_1
    const-string v1, "download"

    goto :goto_0

    .line 465
    :cond_2
    const-string v1, "500"

    goto :goto_1
.end method

.method protected static rightPosition(JLjava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "rightId"
    .parameter "playPosition"

    .prologue
    .line 474
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 475
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "rights/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 476
    const-string v1, "/position.json?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 477
    const-string v1, "&playPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 478
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 479
    const-string v1, "&udt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 480
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 481
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static rightStream(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3
    .parameter "rightId"
    .parameter "reason"
    .parameter "streamId"
    .parameter "playPosition"
    .parameter "isSonicProxyMode"

    .prologue
    .line 417
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 418
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "rights/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 419
    const-string v1, "/stream.json?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 420
    if-eqz p2, :cond_0

    .line 421
    const-string v1, "&reason="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    :cond_0
    if-eqz p3, :cond_1

    .line 424
    const-string v1, "&streamId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 426
    :cond_1
    if-eqz p4, :cond_2

    .line 427
    const-string v1, "&playPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 429
    :cond_2
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 430
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p5}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 431
    const-string v1, "&udt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 432
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 433
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static searchMovies(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "query"

    .prologue
    .line 165
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 167
    .local v1, urlBuilder:Ljava/lang/StringBuilder;
    :try_start_0
    const-string v2, "movies.json?filter="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-static {p0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    :goto_0
    const-string v2, "&view=long&limit=25"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    const-string v2, "&version="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 173
    const-string v2, "&deviceType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 174
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 175
    invoke-static {v1}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 168
    :catch_0
    move-exception v0

    .line 169
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0
.end method

.method protected static season(J)Ljava/lang/String;
    .locals 3
    .parameter "seasonId"

    .prologue
    .line 189
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 190
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "tv/seasons/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    const-string v1, "&exp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 193
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 194
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static sonicAckLicense()Ljava/lang/String;
    .locals 1

    .prologue
    .line 495
    const-string v0, "https://www.cinemanow.com/pox/LicenseBusinessLogic.asmx/StreamKeyServerAck"

    return-object v0
.end method

.method protected static sonicAckLicenseParams(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .parameter "customData"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 499
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 500
    .local v0, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "strCustomData"

    invoke-direct {v1, v2, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    return-object v0
.end method

.method protected static streamCreate(Lnet/flixster/android/model/LockerRight;)Ljava/lang/String;
    .locals 4
    .parameter "right"

    .prologue
    .line 57
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isEpisode()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v0, "episodes/"

    .line 58
    .local v0, type:Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getSecureBaseUrl()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 59
    .local v1, urlBuilder:Ljava/lang/StringBuilder;
    const-string v2, "uv/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 60
    iget-wide v2, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/stream/create"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 61
    const-string v2, "?deviceType="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 57
    .end local v0           #type:Ljava/lang/String;
    .end local v1           #urlBuilder:Ljava/lang/StringBuilder;
    :cond_0
    const-string v0, "movies/"

    goto :goto_0
.end method

.method protected static streamDelete(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "streamId"

    .prologue
    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 68
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "uv/stream/delete?streamHandleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static supportEmail()Ljava/lang/String;
    .locals 1

    .prologue
    .line 760
    const-string v0, "android@flixster-inc.com"

    return-object v0
.end method

.method protected static theater(JLjava/util/Date;)Ljava/lang/String;
    .locals 3
    .parameter "theaterId"
    .parameter "date"

    .prologue
    .line 242
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 243
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "theaters/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 244
    const-string v1, "?date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lnet/flixster/android/data/ApiBuilder;->URI_DATE_FORMATTER:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    const-string v1, "&showtimes=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 247
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 248
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static theaterMoviePair(JJLjava/util/Date;)Ljava/lang/String;
    .locals 3
    .parameter "theaterId"
    .parameter "movieId"
    .parameter "date"

    .prologue
    .line 252
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 253
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "theaters.json?theater="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 254
    const-string v1, "&movie="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 255
    const-string v1, "&showtimes=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 256
    const-string v1, "&date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lnet/flixster/android/data/ApiBuilder;->URI_DATE_FORMATTER:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 258
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static theaters(DD)Ljava/lang/String;
    .locals 3
    .parameter "latitude"
    .parameter "longitude"

    .prologue
    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 224
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "theaters.json?latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&longitude="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 225
    const-string v1, "&radius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getTheaterDistance()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&limit=100"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 227
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static theaters(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .parameter "postal"

    .prologue
    .line 211
    :try_start_0
    const-string v3, "ISO-8859-1"

    invoke-static {p0, v3}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 215
    .local v0, encodedPostal:Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 216
    .local v2, urlBuilder:Ljava/lang/StringBuilder;
    const-string v3, "theaters.json?postal="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 217
    const-string v3, "&radius="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getTheaterDistance()F

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 218
    const-string v3, "&version="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 219
    invoke-static {v2}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3

    .line 212
    .end local v0           #encodedPostal:Ljava/lang/String;
    .end local v2           #urlBuilder:Ljava/lang/StringBuilder;
    :catch_0
    move-exception v1

    .line 213
    .local v1, uee:Ljava/io/UnsupportedEncodingException;
    move-object v0, p0

    .restart local v0       #encodedPostal:Ljava/lang/String;
    goto :goto_0
.end method

.method protected static theatersByMovie(DDJLjava/util/Date;)Ljava/lang/String;
    .locals 3
    .parameter "latitude"
    .parameter "longitude"
    .parameter "movieId"
    .parameter "date"

    .prologue
    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 232
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "theaters.json?latitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 233
    const-string v1, "&longitude="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    .line 234
    const-string v1, "&showtimes=true&movie="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 235
    const-string v1, "&date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lnet/flixster/android/data/ApiBuilder;->URI_DATE_FORMATTER:Ljava/text/SimpleDateFormat;

    invoke-virtual {v2, p6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    const-string v1, "&radius="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getTheaterDistance()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "&limit=100"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 238
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static ticketAvailability(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "theaterId"
    .parameter "listingId"
    .parameter "date"
    .parameter "time"

    .prologue
    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://secure.flixster.com/android/api/v1/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 267
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "tickets.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    const-string v1, "?theater="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    const-string v1, "&listing="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string v1, "&date="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    const-string v1, "&time="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 273
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static ticketPurchase()Ljava/lang/String;
    .locals 3

    .prologue
    .line 284
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://secure.flixster.com/android/api/v1/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 285
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "tickets/purchases.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 286
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 287
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static ticketReservation()Ljava/lang/String;
    .locals 3

    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "https://secure.flixster.com/android/api/v1/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 278
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "tickets/reservations.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 279
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 280
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static tokens()Ljava/lang/String;
    .locals 2

    .prologue
    .line 553
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->isQaApiServer()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 554
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "tokens.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 555
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 553
    .end local v0           #urlBuilder:Ljava/lang/StringBuilder;
    :cond_0
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getSecureBaseUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected static topActors(Ljava/lang/String;I)Ljava/lang/String;
    .locals 2
    .parameter "filter"
    .parameter "limit"

    .prologue
    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 320
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "actors.json?limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 321
    const-string v1, "&filter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 323
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static upcoming()Ljava/lang/String;
    .locals 3

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 126
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "movies.json?filter=theater-upcoming&view=long"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 128
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    const-string v1, "&exp="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lnet/flixster/android/FlixsterApplication;->sDay:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 130
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 131
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static user(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "userId"

    .prologue
    .line 536
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 537
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 538
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json?locale=en_US&country=us"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 539
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 540
    invoke-static {p0, v0}, Lnet/flixster/android/FlixsterApplication;->addUserParameters(Ljava/lang/String;Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 541
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static userMovieReview(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "flixsterId"
    .parameter "movieId"

    .prologue
    .line 527
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 528
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 529
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ratings/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 530
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 531
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 532
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static userMovieReviewShort(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "flixsterId"
    .parameter "movieId"

    .prologue
    .line 520
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 521
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 522
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ratings/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 523
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected static userReviews(Ljava/lang/String;I)Ljava/lang/String;
    .locals 3
    .parameter "userId"
    .parameter "limit"

    .prologue
    .line 351
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 352
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/ratings.json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 354
    if-gtz p1, :cond_0

    .line 355
    const-string v1, "?limit=50"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 359
    :goto_0
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 360
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 361
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 357
    :cond_0
    const-string v1, "?limit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method protected static userRights(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .parameter "userId"
    .parameter "purchaseType"

    .prologue
    .line 400
    new-instance v0, Ljava/lang/StringBuilder;

    if-nez p1, :cond_1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 401
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 402
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/rights.json?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 403
    if-eqz p1, :cond_0

    .line 405
    const-string v1, "&moviePurchaseType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 407
    :cond_0
    const-string v1, "&version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 408
    const-string v1, "&deviceType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Lnet/flixster/android/data/ApiBuilder;->getPlatformBasedDeviceType(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    const-string v1, "&deviceModel="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getDeviceModel()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    const-string v1, "&udt="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 412
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 400
    .end local v0           #urlBuilder:Ljava/lang/StringBuilder;
    :cond_1
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getSecureBaseUrl()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected static viewer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 545
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getBaseUrl()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 546
    .local v0, urlBuilder:Ljava/lang/StringBuilder;
    const-string v1, "users/viewer.json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 547
    const-string v1, "?version="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 548
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 549
    invoke-static {v0}, Lnet/flixster/android/data/ApiBuilder;->appendCountryParam(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static widevineLicense(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .parameter "right"
    .parameter "streamId"

    .prologue
    .line 74
    if-nez p0, :cond_0

    .line 75
    const-string v10, "FlxMain"

    const-string v11, "ApiBuilder.widevineLicense asset null"

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v3, ""

    .line 108
    :goto_0
    return-object v3

    .line 78
    :cond_0
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isEpisode()Z

    move-result v10

    if-eqz v10, :cond_2

    const-string v8, "episode_id/"

    .line 79
    .local v8, type:Ljava/lang/String;
    :goto_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->getSecureBaseUrl()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 80
    .local v9, urlBuilder:Ljava/lang/StringBuilder;
    const-string v10, "uv/widevine/license/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {v9, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v11, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 82
    const-string v10, "/mediaTransfer/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v10

    invoke-interface {v10}, Lcom/flixster/android/drm/PlaybackManager;->isStreamingMode()Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "streaming"

    :goto_2
    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const-string v10, "/udt/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getHashedUid()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v10

    invoke-interface {v10}, Lcom/flixster/android/drm/PlaybackManager;->isStreamingMode()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 85
    const-string v10, "/streamHandleId/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    :cond_1
    invoke-static {v9}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserParameters(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;

    move-result-object v9

    .line 90
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    const-string v11, "?"

    const-string v12, "/"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "&"

    const-string v12, "/"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "platform="

    const-string v12, "platform/"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    .line 91
    const-string v11, "user="

    const-string v12, "user/"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "session="

    const-string v12, "session/"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "token="

    const-string v12, "token/"

    invoke-virtual {v10, v11, v12}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 93
    .local v3, proxyUrl:Ljava/lang/String;
    :try_start_0
    const-string v10, "platform_session/"

    invoke-virtual {v3, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 94
    .local v4, sessionIndex:I
    const/4 v10, -0x1

    if-eq v4, v10, :cond_4

    .line 95
    add-int/lit8 v10, v4, 0x11

    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 96
    .local v5, sessionStr:Ljava/lang/String;
    const-string v10, "UTF-8"

    invoke-static {v5, v10}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 97
    .local v1, encodedSessionStr:Ljava/lang/String;
    invoke-virtual {v3, v5, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto/16 :goto_0

    .line 78
    .end local v1           #encodedSessionStr:Ljava/lang/String;
    .end local v3           #proxyUrl:Ljava/lang/String;
    .end local v4           #sessionIndex:I
    .end local v5           #sessionStr:Ljava/lang/String;
    .end local v8           #type:Ljava/lang/String;
    .end local v9           #urlBuilder:Ljava/lang/StringBuilder;
    :cond_2
    const-string v8, "movie_id/"

    goto/16 :goto_1

    .line 82
    .restart local v8       #type:Ljava/lang/String;
    .restart local v9       #urlBuilder:Ljava/lang/StringBuilder;
    :cond_3
    const-string v10, "download"

    goto :goto_2

    .line 99
    .restart local v3       #proxyUrl:Ljava/lang/String;
    .restart local v4       #sessionIndex:I
    :cond_4
    :try_start_1
    const-string v10, "auth_token/"

    invoke-virtual {v3, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 100
    .local v6, tokenIndex:I
    add-int/lit8 v10, v6, 0xb

    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 101
    .local v7, tokenStr:Ljava/lang/String;
    const-string v10, "UTF-8"

    invoke-static {v7, v10}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 102
    .local v2, encodedTokenStr:Ljava/lang/String;
    invoke-virtual {v3, v7, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v3

    goto/16 :goto_0

    .line 104
    .end local v2           #encodedTokenStr:Ljava/lang/String;
    .end local v4           #sessionIndex:I
    .end local v6           #tokenIndex:I
    .end local v7           #tokenStr:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 105
    .local v0, e:Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_0
.end method
