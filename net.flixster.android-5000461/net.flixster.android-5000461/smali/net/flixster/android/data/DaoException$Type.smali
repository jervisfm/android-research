.class public final enum Lnet/flixster/android/data/DaoException$Type;
.super Ljava/lang/Enum;
.source "DaoException.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/data/DaoException;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lnet/flixster/android/data/DaoException$Type;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lnet/flixster/android/data/DaoException$Type;

.field public static final enum NETWORK:Lnet/flixster/android/data/DaoException$Type;

.field public static final enum NETWORK_UNSTABLE:Lnet/flixster/android/data/DaoException$Type;

.field public static final enum NOT_LICENSED:Lnet/flixster/android/data/DaoException$Type;

.field public static final enum SERVER_API:Lnet/flixster/android/data/DaoException$Type;

.field public static final enum SERVER_DATA:Lnet/flixster/android/data/DaoException$Type;

.field public static final enum SERVER_ERROR_MSG:Lnet/flixster/android/data/DaoException$Type;

.field public static final enum STREAM_CREATE:Lnet/flixster/android/data/DaoException$Type;

.field public static final enum UNKNOWN:Lnet/flixster/android/data/DaoException$Type;

.field public static final enum USER_ACCT:Lnet/flixster/android/data/DaoException$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "NETWORK"

    invoke-direct {v0, v1, v3}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->NETWORK:Lnet/flixster/android/data/DaoException$Type;

    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "NETWORK_UNSTABLE"

    invoke-direct {v0, v1, v4}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->NETWORK_UNSTABLE:Lnet/flixster/android/data/DaoException$Type;

    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "SERVER_DATA"

    invoke-direct {v0, v1, v5}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_DATA:Lnet/flixster/android/data/DaoException$Type;

    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "SERVER_API"

    invoke-direct {v0, v1, v6}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_API:Lnet/flixster/android/data/DaoException$Type;

    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "SERVER_ERROR_MSG"

    invoke-direct {v0, v1, v7}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->SERVER_ERROR_MSG:Lnet/flixster/android/data/DaoException$Type;

    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "USER_ACCT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "STREAM_CREATE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->STREAM_CREATE:Lnet/flixster/android/data/DaoException$Type;

    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "NOT_LICENSED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->NOT_LICENSED:Lnet/flixster/android/data/DaoException$Type;

    new-instance v0, Lnet/flixster/android/data/DaoException$Type;

    const-string v1, "UNKNOWN"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/data/DaoException$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->UNKNOWN:Lnet/flixster/android/data/DaoException$Type;

    .line 24
    const/16 v0, 0x9

    new-array v0, v0, [Lnet/flixster/android/data/DaoException$Type;

    sget-object v1, Lnet/flixster/android/data/DaoException$Type;->NETWORK:Lnet/flixster/android/data/DaoException$Type;

    aput-object v1, v0, v3

    sget-object v1, Lnet/flixster/android/data/DaoException$Type;->NETWORK_UNSTABLE:Lnet/flixster/android/data/DaoException$Type;

    aput-object v1, v0, v4

    sget-object v1, Lnet/flixster/android/data/DaoException$Type;->SERVER_DATA:Lnet/flixster/android/data/DaoException$Type;

    aput-object v1, v0, v5

    sget-object v1, Lnet/flixster/android/data/DaoException$Type;->SERVER_API:Lnet/flixster/android/data/DaoException$Type;

    aput-object v1, v0, v6

    sget-object v1, Lnet/flixster/android/data/DaoException$Type;->SERVER_ERROR_MSG:Lnet/flixster/android/data/DaoException$Type;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->STREAM_CREATE:Lnet/flixster/android/data/DaoException$Type;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->NOT_LICENSED:Lnet/flixster/android/data/DaoException$Type;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->UNKNOWN:Lnet/flixster/android/data/DaoException$Type;

    aput-object v2, v0, v1

    sput-object v0, Lnet/flixster/android/data/DaoException$Type;->ENUM$VALUES:[Lnet/flixster/android/data/DaoException$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/flixster/android/data/DaoException$Type;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lnet/flixster/android/data/DaoException$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/data/DaoException$Type;

    return-object v0
.end method

.method public static values()[Lnet/flixster/android/data/DaoException$Type;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lnet/flixster/android/data/DaoException$Type;->ENUM$VALUES:[Lnet/flixster/android/data/DaoException$Type;

    array-length v1, v0

    new-array v2, v1, [Lnet/flixster/android/data/DaoException$Type;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
