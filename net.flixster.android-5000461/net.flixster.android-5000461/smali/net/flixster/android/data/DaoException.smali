.class public Lnet/flixster/android/data/DaoException;
.super Ljava/lang/Exception;
.source "DaoException.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/data/DaoException$Type;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = -0x1dbeebdf5a166b20L


# instance fields
.field private final type:Lnet/flixster/android/data/DaoException$Type;


# direct methods
.method private constructor <init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V
    .locals 0
    .parameter "t"
    .parameter "e"

    .prologue
    .line 33
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 34
    iput-object p1, p0, Lnet/flixster/android/data/DaoException;->type:Lnet/flixster/android/data/DaoException$Type;

    .line 35
    return-void
.end method

.method private constructor <init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)V
    .locals 0
    .parameter "t"
    .parameter "msg"

    .prologue
    .line 38
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 39
    iput-object p1, p0, Lnet/flixster/android/data/DaoException;->type:Lnet/flixster/android/data/DaoException$Type;

    .line 40
    return-void
.end method

.method public static create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;
    .locals 3
    .parameter "e"

    .prologue
    .line 63
    instance-of v2, p0, Lorg/json/JSONException;

    if-eqz v2, :cond_2

    .line 64
    invoke-virtual {p0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, msg:Ljava/lang/String;
    const-string v2, "End of input at character 0"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->NETWORK_UNSTABLE:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .line 87
    .end local v1           #msg:Ljava/lang/String;
    .local v0, de:Lnet/flixster/android/data/DaoException;
    :goto_0
    return-object v0

    .line 67
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    .restart local v1       #msg:Ljava/lang/String;
    :cond_0
    const-string v2, "Expected"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->SERVER_DATA:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    goto :goto_0

    .line 70
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_1
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->SERVER_DATA:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    goto :goto_0

    .line 72
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    .end local v1           #msg:Ljava/lang/String;
    :cond_2
    instance-of v2, p0, Ljava/io/IOException;

    if-eqz v2, :cond_9

    .line 73
    instance-of v2, p0, Lcom/flixster/android/net/HttpUnauthorizedException;

    if-eqz v2, :cond_3

    .line 74
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    goto :goto_0

    .line 75
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_3
    instance-of v2, p0, Ljava/net/SocketTimeoutException;

    if-eqz v2, :cond_4

    .line 76
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->NETWORK_UNSTABLE:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    goto :goto_0

    .line 77
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_4
    instance-of v2, p0, Ljava/net/UnknownHostException;

    if-nez v2, :cond_5

    instance-of v2, p0, Ljava/net/ConnectException;

    if-eqz v2, :cond_6

    .line 78
    :cond_5
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->NETWORK:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    goto :goto_0

    .line 79
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_6
    instance-of v2, p0, Ljava/io/FileNotFoundException;

    if-nez v2, :cond_7

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 80
    :cond_7
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->SERVER_API:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    goto :goto_0

    .line 82
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_8
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->NETWORK:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    goto :goto_0

    .line 85
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    :cond_9
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v2, Lnet/flixster/android/data/DaoException$Type;->UNKNOWN:Lnet/flixster/android/data/DaoException$Type;

    invoke-direct {v0, v2, p0}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/Exception;)V

    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    goto :goto_0
.end method

.method public static create(Ljava/lang/String;)Lnet/flixster/android/data/DaoException;
    .locals 2
    .parameter "apiResponse"

    .prologue
    .line 98
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Lorg/json/JSONObject;)Lnet/flixster/android/data/DaoException;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 100
    :goto_0
    return-object v1

    .line 99
    :catch_0
    move-exception v0

    .line 100
    .local v0, e:Lorg/json/JSONException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;
    .locals 1
    .parameter "t"
    .parameter "msg"

    .prologue
    .line 92
    new-instance v0, Lnet/flixster/android/data/DaoException;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)V

    return-object v0
.end method

.method private static create(Lorg/json/JSONObject;)Lnet/flixster/android/data/DaoException;
    .locals 3
    .parameter "jsonObj"

    .prologue
    .line 106
    new-instance v0, Lnet/flixster/android/data/DaoException;

    sget-object v1, Lnet/flixster/android/data/DaoException$Type;->SERVER_ERROR_MSG:Lnet/flixster/android/data/DaoException$Type;

    const-string v2, "error"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/data/DaoException;-><init>(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)V

    return-object v0
.end method

.method public static hasError(Ljava/lang/String;)Z
    .locals 2
    .parameter "apiResponse"

    .prologue
    .line 49
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->hasError(Lorg/json/JSONObject;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 51
    :goto_0
    return v1

    .line 50
    :catch_0
    move-exception v0

    .line 51
    .local v0, e:Lorg/json/JSONException;
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static hasError(Lorg/json/JSONObject;)Z
    .locals 1
    .parameter "jsonObj"

    .prologue
    .line 57
    const-string v0, "error"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public getType()Lnet/flixster/android/data/DaoException$Type;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lnet/flixster/android/data/DaoException;->type:Lnet/flixster/android/data/DaoException$Type;

    return-object v0
.end method
