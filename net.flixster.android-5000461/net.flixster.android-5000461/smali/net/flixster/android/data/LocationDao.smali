.class public Lnet/flixster/android/data/LocationDao;
.super Ljava/lang/Object;
.source "LocationDao.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLocations(DD)Ljava/util/ArrayList;
    .locals 1
    .parameter "latitude"
    .parameter "longitude"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DD)",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Location;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-static {p0, p1, p2, p3}, Lnet/flixster/android/data/ApiBuilder;->location(DD)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/data/LocationDao;->getLocationsByUrl(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public static getLocations(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 1
    .parameter "query"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Location;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 19
    invoke-static {p0}, Lnet/flixster/android/data/ApiBuilder;->location(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/data/LocationDao;->getLocationsByUrl(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method private static getLocationsByUrl(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 12
    .parameter "url"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Location;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 29
    :try_start_0
    new-instance v9, Ljava/net/URL;

    invoke-direct {v9, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 37
    .local v8, response:Ljava/lang/String;
    :try_start_1
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6, v8}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 38
    .local v6, locationsArray:Lorg/json/JSONArray;
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v5, locations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Location;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v9

    if-lt v0, v9, :cond_0

    .line 44
    return-object v5

    .line 30
    .end local v0           #i:I
    .end local v5           #locations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Location;>;"
    .end local v6           #locationsArray:Lorg/json/JSONArray;
    .end local v8           #response:Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 31
    .local v7, mue:Ljava/net/MalformedURLException;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 32
    .end local v7           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 33
    .local v1, ie:Ljava/io/IOException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9

    .line 40
    .end local v1           #ie:Ljava/io/IOException;
    .restart local v0       #i:I
    .restart local v5       #locations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Location;>;"
    .restart local v6       #locationsArray:Lorg/json/JSONArray;
    .restart local v8       #response:Ljava/lang/String;
    :cond_0
    :try_start_2
    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 41
    .local v4, locationObject:Lorg/json/JSONObject;
    invoke-static {v4}, Lnet/flixster/android/data/LocationDao;->parseLocation(Lorg/json/JSONObject;)Lnet/flixster/android/model/Location;

    move-result-object v3

    .line 42
    .local v3, location:Lnet/flixster/android/model/Location;
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 39
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    .end local v0           #i:I
    .end local v3           #location:Lnet/flixster/android/model/Location;
    .end local v4           #locationObject:Lorg/json/JSONObject;
    .end local v5           #locations:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Location;>;"
    .end local v6           #locationsArray:Lorg/json/JSONArray;
    :catch_2
    move-exception v2

    .line 46
    .local v2, je:Lorg/json/JSONException;
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9
.end method

.method private static parseLocation(Lorg/json/JSONObject;)Lnet/flixster/android/model/Location;
    .locals 4
    .parameter "locationObject"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 51
    new-instance v1, Lnet/flixster/android/model/Location;

    invoke-direct {v1}, Lnet/flixster/android/model/Location;-><init>()V

    .line 52
    .local v1, location:Lnet/flixster/android/model/Location;
    const-string v2, "city"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    const-string v2, "city"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/Location;->city:Ljava/lang/String;

    .line 55
    :cond_0
    const-string v2, "state"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 56
    const-string v2, "state"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/Location;->state:Ljava/lang/String;

    .line 58
    :cond_1
    const-string v2, "stateAbbrev"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 59
    const-string v2, "stateAbbrev"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/Location;->stateCode:Ljava/lang/String;

    .line 61
    :cond_2
    const-string v2, "country"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 62
    const-string v2, "country"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/Location;->country:Ljava/lang/String;

    .line 64
    :cond_3
    const-string v2, "zipcode"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 65
    const-string v2, "zipcode"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnet/flixster/android/model/Location;->zip:Ljava/lang/String;

    .line 67
    :cond_4
    const-string v2, "weight"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 68
    const-string v2, "weight"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lnet/flixster/android/model/Location;->weight:I

    .line 70
    :cond_5
    const-string v2, "center"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 71
    const-string v2, "center"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 72
    .local v0, centerObject:Lorg/json/JSONObject;
    const-string v2, "latitude"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 73
    const-string v2, "latitude"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, v1, Lnet/flixster/android/model/Location;->latitude:D

    .line 75
    :cond_6
    const-string v2, "longitude"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 76
    const-string v2, "longitude"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v2

    iput-wide v2, v1, Lnet/flixster/android/model/Location;->longitude:D

    .line 79
    .end local v0           #centerObject:Lorg/json/JSONObject;
    :cond_7
    return-object v1
.end method
