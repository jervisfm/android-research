.class Lnet/flixster/android/data/MiscDao$1;
.super Ljava/lang/Object;
.source "MiscDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/MiscDao;->fetchProperties(Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/MiscDao$1;->val$successHandler:Landroid/os/Handler;

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 49
    :try_start_0
    #calls: Lnet/flixster/android/data/MiscDao;->fetchProperties()Lnet/flixster/android/model/Property;
    invoke-static {}, Lnet/flixster/android/data/MiscDao;->access$0()Lnet/flixster/android/model/Property;

    move-result-object v1

    .line 50
    .local v1, property:Lnet/flixster/android/model/Property;
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/flixster/android/utils/Properties;->setProperties(Lnet/flixster/android/model/Property;)V

    .line 51
    iget-object v2, p0, Lnet/flixster/android/data/MiscDao$1;->val$successHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 55
    .end local v1           #property:Lnet/flixster/android/model/Property;
    :goto_0
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v2, "FlxMain"

    const-string v3, "MiscDao.fetchProperties"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
