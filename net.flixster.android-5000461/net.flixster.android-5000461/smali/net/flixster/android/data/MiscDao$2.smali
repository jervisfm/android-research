.class Lnet/flixster/android/data/MiscDao$2;
.super Ljava/lang/Object;
.source "MiscDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/MiscDao;->registerDevice()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 83
    const-string v5, "FlxMain"

    const-string v6, "MiscDao.registerDevice"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    new-instance v0, Lcom/flixster/android/net/RestClient;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->device()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Lcom/flixster/android/net/RestClient;-><init>(Ljava/lang/String;)V

    .line 85
    .local v0, client:Lcom/flixster/android/net/RestClient;
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->deviceParams()Ljava/util/ArrayList;

    move-result-object v4

    .line 86
    .local v4, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 87
    .local v2, itr:Ljava/util/Iterator;,"Ljava/util/Iterator<Lorg/apache/http/NameValuePair;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_0

    .line 91
    invoke-virtual {v0, v4}, Lcom/flixster/android/net/RestClient;->addParam(Ljava/util/ArrayList;)V

    .line 93
    :try_start_0
    sget-object v5, Lcom/flixster/android/net/RestClient$RequestMethod;->POST:Lcom/flixster/android/net/RestClient$RequestMethod;

    invoke-virtual {v0, v5}, Lcom/flixster/android/net/RestClient;->execute(Lcom/flixster/android/net/RestClient$RequestMethod;)V

    .line 94
    const-string v5, "FlxMain"

    .line 95
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "MiscDao.registerDevice response: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/flixster/android/net/RestClient;->getResponseCode()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v0}, Lcom/flixster/android/net/RestClient;->getResponse()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 94
    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_1
    return-void

    .line 88
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/http/NameValuePair;

    .line 89
    .local v3, param:Lorg/apache/http/NameValuePair;
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "MiscDao.registerDevice "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lorg/apache/http/NameValuePair;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v3}, Lorg/apache/http/NameValuePair;->getValue()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->sd(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 96
    .end local v3           #param:Lorg/apache/http/NameValuePair;
    :catch_0
    move-exception v1

    .line 97
    .local v1, e:Ljava/io/IOException;
    const-string v5, "FlxMain"

    const-string v6, "MiscDao.registerDevice"

    invoke-static {v5, v6, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
