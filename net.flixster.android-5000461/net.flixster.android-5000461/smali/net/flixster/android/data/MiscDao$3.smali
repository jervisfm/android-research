.class Lnet/flixster/android/data/MiscDao$3;
.super Ljava/lang/Object;
.source "MiscDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/MiscDao;->fetchFriends(Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/MiscDao$3;->val$successHandler:Landroid/os/Handler;

    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 22

    .prologue
    .line 109
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 111
    .local v17, friends:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Contact;>;"
    const/4 v1, 0x5

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "contact_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "data1"

    aput-object v2, v3, v1

    const/4 v1, 0x2

    .line 112
    const-string v2, "data3"

    aput-object v2, v3, v1

    const/4 v1, 0x3

    const-string v2, "data2"

    aput-object v2, v3, v1

    const/4 v1, 0x4

    const-string v2, "data5"

    aput-object v2, v3, v1

    .line 114
    .local v3, dataProjection:[Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 115
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 116
    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    .line 117
    const-string v4, "mimetype=\'vnd.android.cursor.item/name\'"

    const/4 v5, 0x0

    .line 118
    const-string v6, "data3 ASC,data2 ASC"

    .line 116
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 119
    .local v14, dataCursor:Landroid/database/Cursor;
    :cond_0
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 164
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 166
    move-object/from16 v0, p0

    iget-object v1, v0, Lnet/flixster/android/data/MiscDao$3;->val$successHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-static/range {v17 .. v17}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-static {v2, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 167
    return-void

    .line 120
    :cond_1
    const-string v1, "data1"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 121
    .local v20, name:Ljava/lang/String;
    if-eqz v20, :cond_0

    const-string v1, ""

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "#"

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 125
    const/4 v15, 0x0

    .line 126
    .local v15, foundName:Z
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_5

    .line 132
    :goto_1
    if-nez v15, :cond_0

    .line 136
    const-string v1, "contact_id"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v19

    .line 137
    .local v19, id:Ljava/lang/String;
    const-string v1, "data2"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 138
    .local v18, givenName:Ljava/lang/String;
    const-string v1, "data5"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 139
    .local v10, middleName:Ljava/lang/String;
    const-string v1, "data3"

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 141
    .local v11, familyName:Ljava/lang/String;
    const/4 v1, 0x1

    new-array v6, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "data1"

    aput-object v2, v6, v1

    .line 143
    .local v6, phoneProjection:[Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 144
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 145
    sget-object v5, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    .line 146
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "contact_id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    .line 145
    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v21

    .line 147
    .local v21, phoneCursor:Landroid/database/Cursor;
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 148
    const-string v1, "data1"

    move-object/from16 v0, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 150
    .local v12, number:Ljava/lang/String;
    const/16 v16, 0x0

    .line 151
    .local v16, foundNumber:Z
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_6

    .line 157
    :goto_2
    if-nez v16, :cond_4

    .line 158
    new-instance v7, Lnet/flixster/android/model/Contact;

    move-object/from16 v8, v20

    move-object/from16 v9, v18

    invoke-direct/range {v7 .. v12}, Lnet/flixster/android/model/Contact;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    .local v7, friend:Lnet/flixster/android/model/Contact;
    move-object/from16 v0, v17

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    .end local v7           #friend:Lnet/flixster/android/model/Contact;
    .end local v12           #number:Ljava/lang/String;
    .end local v16           #foundNumber:Z
    :cond_4
    invoke-interface/range {v21 .. v21}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 126
    .end local v6           #phoneProjection:[Ljava/lang/String;
    .end local v10           #middleName:Ljava/lang/String;
    .end local v11           #familyName:Ljava/lang/String;
    .end local v18           #givenName:Ljava/lang/String;
    .end local v19           #id:Ljava/lang/String;
    .end local v21           #phoneCursor:Landroid/database/Cursor;
    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lnet/flixster/android/model/Contact;

    .line 127
    .local v13, c:Lnet/flixster/android/model/Contact;
    iget-object v2, v13, Lnet/flixster/android/model/Contact;->name:Ljava/lang/String;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 128
    const/4 v15, 0x1

    .line 129
    goto/16 :goto_1

    .line 151
    .end local v13           #c:Lnet/flixster/android/model/Contact;
    .restart local v6       #phoneProjection:[Ljava/lang/String;
    .restart local v10       #middleName:Ljava/lang/String;
    .restart local v11       #familyName:Ljava/lang/String;
    .restart local v12       #number:Ljava/lang/String;
    .restart local v16       #foundNumber:Z
    .restart local v18       #givenName:Ljava/lang/String;
    .restart local v19       #id:Ljava/lang/String;
    .restart local v21       #phoneCursor:Landroid/database/Cursor;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lnet/flixster/android/model/Contact;

    .line 152
    .restart local v13       #c:Lnet/flixster/android/model/Contact;
    iget-object v2, v13, Lnet/flixster/android/model/Contact;->number:Ljava/lang/String;

    invoke-virtual {v2, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 153
    const/16 v16, 0x1

    .line 154
    goto :goto_2
.end method
