.class Lnet/flixster/android/data/MiscDao$4;
.super Ljava/lang/Object;
.source "MiscDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/MiscDao;->fetchCaptions(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$successHandler:Landroid/os/Handler;

.field private final synthetic val$urlString:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/MiscDao$4;->val$urlString:Ljava/lang/String;

    iput-object p2, p0, Lnet/flixster/android/data/MiscDao$4;->val$successHandler:Landroid/os/Handler;

    iput-object p3, p0, Lnet/flixster/android/data/MiscDao$4;->val$errorHandler:Landroid/os/Handler;

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 176
    const/4 v2, 0x0

    .line 178
    .local v2, is:Ljava/io/InputStream;
    :try_start_0
    iget-object v6, p0, Lnet/flixster/android/data/MiscDao$4;->val$urlString:Ljava/lang/String;

    const-string v7, "file://"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 179
    new-instance v1, Ljava/io/File;

    iget-object v6, p0, Lnet/flixster/android/data/MiscDao$4;->val$urlString:Ljava/lang/String;

    const-string v7, "file://"

    const-string v8, ""

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 180
    .local v1, file:Ljava/io/File;
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .end local v2           #is:Ljava/io/InputStream;
    .local v3, is:Ljava/io/InputStream;
    move-object v2, v3

    .line 184
    .end local v1           #file:Ljava/io/File;
    .end local v3           #is:Ljava/io/InputStream;
    .restart local v2       #is:Ljava/io/InputStream;
    :goto_0
    new-instance v4, Lcom/flixster/android/net/CaptionsXmlParser;

    invoke-direct {v4}, Lcom/flixster/android/net/CaptionsXmlParser;-><init>()V

    .line 185
    .local v4, parser:Lcom/flixster/android/net/CaptionsXmlParser;
    invoke-virtual {v4, v2}, Lcom/flixster/android/net/CaptionsXmlParser;->parse(Ljava/io/InputStream;)Ljava/util/List;

    move-result-object v5

    .line 192
    .local v5, ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    iget-object v6, p0, Lnet/flixster/android/data/MiscDao$4;->val$successHandler:Landroid/os/Handler;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    invoke-static {v7, v8, v9}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_2

    .line 200
    if-eqz v2, :cond_0

    .line 202
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 208
    .end local v4           #parser:Lcom/flixster/android/net/CaptionsXmlParser;
    .end local v5           #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :cond_0
    :goto_1
    return-void

    .line 182
    :cond_1
    :try_start_2
    new-instance v6, Ljava/net/URL;

    iget-object v7, p0, Lnet/flixster/android/data/MiscDao$4;->val$urlString:Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lnet/flixster/android/util/HttpHelper;->fetchUrlStream(Ljava/net/URL;)Ljava/io/InputStream;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v2

    goto :goto_0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    .local v0, e:Ljava/io/FileNotFoundException;
    :try_start_3
    iget-object v6, p0, Lnet/flixster/android/data/MiscDao$4;->val$errorHandler:Landroid/os/Handler;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v7, v8, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 195
    const-string v6, "FlxDrm"

    const-string v7, "MiscDao.fetchCaptions FileNotFoundException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 200
    if-eqz v2, :cond_0

    .line 202
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 203
    :catch_1
    move-exception v0

    .line 204
    .local v0, e:Ljava/io/IOException;
    const-string v6, "FlxDrm"

    const-string v7, "MiscDao.fetchCaptions IOException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 196
    .end local v0           #e:Ljava/io/IOException;
    :catch_2
    move-exception v0

    .line 197
    .local v0, e:Ljava/net/MalformedURLException;
    :try_start_5
    iget-object v6, p0, Lnet/flixster/android/data/MiscDao$4;->val$errorHandler:Landroid/os/Handler;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v7, v8, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 198
    const-string v6, "FlxDrm"

    const-string v7, "MiscDao.fetchCaptions MalformedURLException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 200
    if-eqz v2, :cond_0

    .line 202
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 203
    :catch_3
    move-exception v0

    .line 204
    .local v0, e:Ljava/io/IOException;
    const-string v6, "FlxDrm"

    const-string v7, "MiscDao.fetchCaptions IOException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 199
    .end local v0           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 200
    if-eqz v2, :cond_2

    .line 202
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    .line 207
    :cond_2
    :goto_2
    throw v6

    .line 203
    :catch_4
    move-exception v0

    .line 204
    .restart local v0       #e:Ljava/io/IOException;
    const-string v7, "FlxDrm"

    const-string v8, "MiscDao.fetchCaptions IOException"

    invoke-static {v7, v8, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 203
    .end local v0           #e:Ljava/io/IOException;
    .restart local v4       #parser:Lcom/flixster/android/net/CaptionsXmlParser;
    .restart local v5       #ttElements:Ljava/util/List;,"Ljava/util/List<Lcom/flixster/android/net/TimedTextElement;>;"
    :catch_5
    move-exception v0

    .line 204
    .restart local v0       #e:Ljava/io/IOException;
    const-string v6, "FlxDrm"

    const-string v7, "MiscDao.fetchCaptions IOException"

    invoke-static {v6, v7, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
