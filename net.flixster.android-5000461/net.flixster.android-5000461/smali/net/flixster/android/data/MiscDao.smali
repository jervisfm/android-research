.class public Lnet/flixster/android/data/MiscDao;
.super Ljava/lang/Object;
.source "MiscDao.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Lnet/flixster/android/model/Property;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 59
    invoke-static {}, Lnet/flixster/android/data/MiscDao;->fetchProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    return-object v0
.end method

.method public static fetchCaptions(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 3
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "urlString"

    .prologue
    .line 172
    const-string v0, "FlxDrm"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MiscDao.fetchCaptions "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/MiscDao$4;

    invoke-direct {v1, p2, p0, p1}, Lnet/flixster/android/data/MiscDao$4;-><init>(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 210
    return-void
.end method

.method public static fetchFriends(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 106
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/MiscDao$3;

    invoke-direct {v1, p0}, Lnet/flixster/android/data/MiscDao$3;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 169
    return-void
.end method

.method private static fetchProperties()Lnet/flixster/android/model/Property;
    .locals 9
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 62
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->properties()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v6, 0x0

    :cond_0
    const/4 v8, 0x1

    invoke-static {v7, v6, v8}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 70
    .local v5, response:Ljava/lang/String;
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 71
    .local v2, json:Lorg/json/JSONObject;
    new-instance v4, Lnet/flixster/android/model/Property;

    invoke-direct {v4, v2}, Lnet/flixster/android/model/Property;-><init>(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 72
    .local v4, property:Lnet/flixster/android/model/Property;
    return-object v4

    .line 63
    .end local v2           #json:Lorg/json/JSONObject;
    .end local v4           #property:Lnet/flixster/android/model/Property;
    .end local v5           #response:Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 64
    .local v3, mue:Ljava/net/MalformedURLException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 65
    .end local v3           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 66
    .local v0, ie:Ljava/io/IOException;
    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6

    .line 73
    .end local v0           #ie:Ljava/io/IOException;
    .restart local v5       #response:Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 74
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6
.end method

.method public static fetchProperties(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 45
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/MiscDao$1;

    invoke-direct {v1, p0}, Lnet/flixster/android/data/MiscDao$1;-><init>(Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 57
    return-void
.end method

.method public static registerDevice()V
    .locals 2

    .prologue
    .line 80
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/MiscDao$2;

    invoke-direct {v1}, Lnet/flixster/android/data/MiscDao$2;-><init>()V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 101
    return-void
.end method
