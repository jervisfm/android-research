.class Lnet/flixster/android/data/MovieDao$1;
.super Ljava/lang/Object;
.source "MovieDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/MovieDao;->getUserSeasonDetail(JLandroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$seasonId:J

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(JLandroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-wide p1, p0, Lnet/flixster/android/data/MovieDao$1;->val$seasonId:J

    iput-object p3, p0, Lnet/flixster/android/data/MovieDao$1;->val$successHandler:Landroid/os/Handler;

    iput-object p4, p0, Lnet/flixster/android/data/MovieDao$1;->val$errorHandler:Landroid/os/Handler;

    .line 292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 295
    iget-wide v0, p0, Lnet/flixster/android/data/MovieDao$1;->val$seasonId:J

    iget-object v2, p0, Lnet/flixster/android/data/MovieDao$1;->val$successHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/data/MovieDao$1;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v0, v1, v2, v3}, Lnet/flixster/android/data/MovieDao;->getUserSeasonDetailBg(JLandroid/os/Handler;Landroid/os/Handler;)V

    .line 296
    return-void
.end method
