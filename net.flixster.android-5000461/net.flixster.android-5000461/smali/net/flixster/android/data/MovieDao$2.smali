.class Lnet/flixster/android/data/MovieDao$2;
.super Ljava/lang/Object;
.source "MovieDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/MovieDao;->getSeasonDetail(JLandroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$seasonId:J

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(JLandroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-wide p1, p0, Lnet/flixster/android/data/MovieDao$2;->val$seasonId:J

    iput-object p3, p0, Lnet/flixster/android/data/MovieDao$2;->val$successHandler:Landroid/os/Handler;

    iput-object p4, p0, Lnet/flixster/android/data/MovieDao$2;->val$errorHandler:Landroid/os/Handler;

    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 372
    :try_start_0
    iget-wide v2, p0, Lnet/flixster/android/data/MovieDao$2;->val$seasonId:J

    invoke-static {v2, v3}, Lnet/flixster/android/data/MovieDao;->getSeasonDetailLegacy(J)Lnet/flixster/android/model/Season;

    move-result-object v1

    .line 373
    .local v1, seasonDetail:Lnet/flixster/android/model/Season;
    iget-object v2, p0, Lnet/flixster/android/data/MovieDao$2;->val$successHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v3, v4, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    .end local v1           #seasonDetail:Lnet/flixster/android/model/Season;
    :goto_0
    return-void

    .line 374
    :catch_0
    move-exception v0

    .line 375
    .local v0, de:Lnet/flixster/android/data/DaoException;
    iget-object v2, p0, Lnet/flixster/android/data/MovieDao$2;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v6, v5, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
