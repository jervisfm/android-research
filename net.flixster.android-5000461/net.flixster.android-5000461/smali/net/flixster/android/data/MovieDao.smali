.class public Lnet/flixster/android/data/MovieDao;
.super Ljava/lang/Object;
.source "MovieDao.java"


# static fields
.field public static final BOXOFFICE:I = 0x0

.field public static final BROWSE:I = 0x3

.field public static final DVD_COMING_SOON:I = 0x2

.field private static final MOVIE_CACHE:Ljava/util/Map; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field public static final UPCOMING:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    new-instance v0, Lnet/flixster/android/util/SoftHashMap;

    invoke-direct {v0}, Lnet/flixster/android/util/SoftHashMap;-><init>()V

    sput-object v0, Lnet/flixster/android/data/MovieDao;->MOVIE_CACHE:Ljava/util/Map;

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fetchBoxOffice(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Z)V
    .locals 11
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter "checkCache"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .local p0, boxOfficeFeatured:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .local p1, boxOfficeOtw:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .local p2, boxOfficeTbo:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .local p3, boxOfficeOthers:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    const/4 v10, 0x1

    .line 41
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->boxOffice()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, p4}, Lnet/flixster/android/data/MovieDao;->fetchJsonList(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 44
    .local v7, response:Ljava/lang/String;
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 46
    .local v5, movies:Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 47
    .local v6, moviesLength:I
    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 48
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 49
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 50
    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 52
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v6, :cond_0

    .line 98
    return-void

    .line 53
    :cond_0
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 54
    .local v8, tempMovieObj:Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 55
    .local v3, movieId:J
    invoke-static {v3, v4}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v2

    .line 57
    .local v2, movie:Lnet/flixster/android/model/Movie;
    const-string v9, "title"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Lnet/flixster/android/model/Movie;->hintTitle(Ljava/lang/String;)V

    .line 58
    const/4 v9, 0x1

    iput-boolean v9, v2, Lnet/flixster/android/model/Movie;->isMIT:Z

    .line 62
    invoke-virtual {v2, v8}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 64
    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_1

    .line 65
    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    :cond_1
    const-string v9, "Opening This Week"

    invoke-virtual {v2, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_5

    .line 70
    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 71
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    if-ne v9, v10, :cond_3

    const/4 v9, 0x0

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/Movie;

    invoke-virtual {v9}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 72
    const/4 v9, 0x0

    invoke-interface {p1, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 75
    :cond_3
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 52
    :cond_4
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_5
    const-string v9, "Top Box Office"

    invoke-virtual {v2, v9}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_8

    .line 78
    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 79
    :cond_6
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v9

    if-ne v9, v10, :cond_7

    const/4 v9, 0x0

    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/Movie;

    invoke-virtual {v9}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 80
    const/4 v9, 0x0

    invoke-interface {p2, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 83
    :cond_7
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 95
    .end local v0           #i:I
    .end local v2           #movie:Lnet/flixster/android/model/Movie;
    .end local v3           #movieId:J
    .end local v5           #movies:Lorg/json/JSONArray;
    .end local v6           #moviesLength:I
    .end local v8           #tempMovieObj:Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 96
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9

    .line 86
    .end local v1           #je:Lorg/json/JSONException;
    .restart local v0       #i:I
    .restart local v2       #movie:Lnet/flixster/android/model/Movie;
    .restart local v3       #movieId:J
    .restart local v5       #movies:Lorg/json/JSONArray;
    .restart local v6       #moviesLength:I
    .restart local v8       #tempMovieObj:Lorg/json/JSONObject;
    :cond_8
    :try_start_1
    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 87
    :cond_9
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v9

    if-ne v9, v10, :cond_a

    const/4 v9, 0x0

    invoke-interface {p3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/Movie;

    invoke-virtual {v9}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_a

    .line 88
    const/4 v9, 0x0

    invoke-interface {p3, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 91
    :cond_a
    invoke-interface {p3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method public static fetchBoxOfficeForWidget(Ljava/util/List;IZ)V
    .locals 10
    .parameter
    .parameter "numOfMovies"
    .parameter "checkCache"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;IZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 102
    .local p0, resultList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->boxOffice()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9, p2}, Lnet/flixster/android/data/MovieDao;->fetchJsonList(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 105
    .local v7, response:Ljava/lang/String;
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 107
    .local v3, jsonMovies:Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v9

    invoke-static {p1, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 108
    .local v8, size:I
    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 110
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v8, :cond_0

    .line 124
    return-void

    .line 111
    :cond_0
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 112
    .local v2, jsonMovie:Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 113
    .local v5, movieId:J
    invoke-static {v5, v6}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v4

    .line 115
    .local v4, movie:Lnet/flixster/android/model/Movie;
    const-string v9, "title"

    invoke-virtual {v2, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v4, v9}, Lnet/flixster/android/model/Movie;->hintTitle(Ljava/lang/String;)V

    .line 116
    const/4 v9, 0x1

    iput-boolean v9, v4, Lnet/flixster/android/model/Movie;->isMIT:Z

    .line 118
    invoke-virtual {v4, v2}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 119
    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    .end local v0           #i:I
    .end local v2           #jsonMovie:Lorg/json/JSONObject;
    .end local v3           #jsonMovies:Lorg/json/JSONArray;
    .end local v4           #movie:Lnet/flixster/android/model/Movie;
    .end local v5           #movieId:J
    .end local v8           #size:I
    :catch_0
    move-exception v1

    .line 122
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9
.end method

.method public static fetchDvdCategory(Ljava/util/List;Ljava/lang/String;Z)V
    .locals 3
    .parameter
    .parameter "params"
    .parameter "checkCache"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 142
    .local p0, resultList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-static {p1}, Lnet/flixster/android/data/ApiBuilder;->dvdCategory(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v0, v1, p0, p2, v2}, Lnet/flixster/android/data/MovieDao;->fetchMovies(Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZ)V

    .line 143
    return-void
.end method

.method public static fetchDvdComingSoon(Ljava/util/List;Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter "checkCache"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 138
    .local p0, featuredList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .local p1, resultList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->dvdComingSoon()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, p2, v1}, Lnet/flixster/android/data/MovieDao;->fetchMovies(Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZ)V

    .line 139
    return-void
.end method

.method public static fetchDvdNewRelease(Ljava/util/List;Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter "checkCache"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 133
    .local p0, featuredList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .local p1, resultList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->dvdNewRelease()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p0, p1, p2, v1}, Lnet/flixster/android/data/MovieDao;->fetchMovies(Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZ)V

    .line 134
    return-void
.end method

.method private static fetchJsonList(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 3
    .parameter "url"
    .parameter "cache"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 211
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {v2, p1, p1}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    return-object v2

    .line 212
    :catch_0
    move-exception v1

    .line 213
    .local v1, mue:Ljava/net/MalformedURLException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 214
    .end local v1           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 215
    .local v0, ie:Ljava/io/IOException;
    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v2

    throw v2
.end method

.method private static fetchMovies(Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZ)V
    .locals 11
    .parameter "url"
    .parameter
    .parameter
    .parameter "checkCache"
    .parameter "isUpcoming"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;ZZ)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 147
    .local p1, featuredList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .local p2, resultList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-static {p0, p3}, Lnet/flixster/android/data/MovieDao;->fetchJsonList(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 150
    .local v7, response:Ljava/lang/String;
    :try_start_0
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 151
    .local v5, movies:Lorg/json/JSONArray;
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 152
    .local v6, moviesLength:I
    invoke-interface {p2}, Ljava/util/List;->clear()V

    .line 153
    if-eqz p1, :cond_0

    .line 154
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 157
    :cond_0
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v6, :cond_1

    .line 179
    return-void

    .line 158
    :cond_1
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 159
    .local v8, tempMovieObj:Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 160
    .local v3, movieId:J
    invoke-static {v3, v4}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v2

    .line 161
    .local v2, movie:Lnet/flixster/android/model/Movie;
    invoke-virtual {v2, v8}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 162
    iput-boolean p4, v2, Lnet/flixster/android/model/Movie;->isUpcoming:Z

    .line 164
    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 165
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v9

    const/4 v10, 0x1

    if-ne v9, v10, :cond_3

    const/4 v9, 0x0

    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/Movie;

    invoke-virtual {v9}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 166
    const/4 v9, 0x0

    invoke-interface {p2, v9}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 169
    :cond_3
    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_4
    if-eqz p1, :cond_5

    invoke-virtual {v2}, Lnet/flixster/android/model/Movie;->isFeatured()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 173
    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 157
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 176
    .end local v0           #i:I
    .end local v2           #movie:Lnet/flixster/android/model/Movie;
    .end local v3           #movieId:J
    .end local v5           #movies:Lorg/json/JSONArray;
    .end local v6           #moviesLength:I
    .end local v8           #tempMovieObj:Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 177
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9
.end method

.method public static fetchUpcoming(Ljava/util/List;Ljava/util/List;Z)V
    .locals 2
    .parameter
    .parameter
    .parameter "checkCache"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;Z)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 128
    .local p0, featuredList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    .local p1, resultList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->upcoming()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p0, p1, p2, v1}, Lnet/flixster/android/data/MovieDao;->fetchMovies(Ljava/lang/String;Ljava/util/List;Ljava/util/List;ZZ)V

    .line 129
    return-void
.end method

.method public static getMovie(J)Lnet/flixster/android/model/Movie;
    .locals 3
    .parameter "id"

    .prologue
    .line 221
    sget-object v1, Lnet/flixster/android/data/MovieDao;->MOVIE_CACHE:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 222
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-nez v0, :cond_0

    .line 223
    new-instance v0, Lnet/flixster/android/model/Movie;

    .end local v0           #movie:Lnet/flixster/android/model/Movie;
    invoke-direct {v0, p0, p1}, Lnet/flixster/android/model/Movie;-><init>(J)V

    .line 224
    .restart local v0       #movie:Lnet/flixster/android/model/Movie;
    sget-object v1, Lnet/flixster/android/data/MovieDao;->MOVIE_CACHE:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    :cond_0
    return-object v0
.end method

.method public static getMovieDetail(J)Lnet/flixster/android/model/Movie;
    .locals 9
    .parameter "id"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 230
    invoke-static {p0, p1}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v3

    .line 237
    .local v3, movie:Lnet/flixster/android/model/Movie;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-static {p0, p1}, Lnet/flixster/android/data/ApiBuilder;->movie(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 245
    .local v5, response:Ljava/lang/String;
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 246
    .local v2, jsonMovie:Lorg/json/JSONObject;
    invoke-virtual {v3, v2}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 247
    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->setDetailsApiParsed()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 254
    return-object v3

    .line 238
    .end local v2           #jsonMovie:Lorg/json/JSONObject;
    .end local v5           #response:Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 239
    .local v4, mue:Ljava/net/MalformedURLException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 240
    .end local v4           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 241
    .local v0, ie:Ljava/io/IOException;
    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6

    .line 248
    .end local v0           #ie:Ljava/io/IOException;
    .restart local v5       #response:Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 249
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6
.end method

.method public static getSeason(J)Lnet/flixster/android/model/Season;
    .locals 5
    .parameter "seasonId"

    .prologue
    .line 274
    invoke-static {p0, p1}, Lnet/flixster/android/model/Season;->generateIdHash(J)J

    move-result-wide v1

    .line 275
    .local v1, seasonHash:J
    sget-object v3, Lnet/flixster/android/data/MovieDao;->MOVIE_CACHE:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Movie;

    .line 276
    .local v0, movie:Lnet/flixster/android/model/Movie;
    if-nez v0, :cond_1

    .line 277
    new-instance v0, Lnet/flixster/android/model/Season;

    .end local v0           #movie:Lnet/flixster/android/model/Movie;
    invoke-direct {v0, p0, p1}, Lnet/flixster/android/model/Season;-><init>(J)V

    .line 278
    .restart local v0       #movie:Lnet/flixster/android/model/Movie;
    sget-object v3, Lnet/flixster/android/data/MovieDao;->MOVIE_CACHE:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v3, v0

    .line 282
    check-cast v3, Lnet/flixster/android/model/Season;

    :goto_0
    return-object v3

    .line 279
    :cond_1
    instance-of v3, v0, Lnet/flixster/android/model/Season;

    if-nez v3, :cond_0

    .line 280
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getSeasonDetail(JLandroid/os/Handler;Landroid/os/Handler;)V
    .locals 3
    .parameter "seasonId"
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 363
    invoke-static {p0, p1}, Lnet/flixster/android/data/MovieDao;->getSeason(J)Lnet/flixster/android/model/Season;

    move-result-object v0

    .line 364
    .local v0, season:Lnet/flixster/android/model/Season;
    invoke-virtual {v0}, Lnet/flixster/android/model/Season;->isDetailsApiParsed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 365
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 379
    :goto_0
    return-void

    .line 368
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v1

    new-instance v2, Lnet/flixster/android/data/MovieDao$2;

    invoke-direct {v2, p0, p1, p2, p3}, Lnet/flixster/android/data/MovieDao$2;-><init>(JLandroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v1, v2}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static getSeasonDetailLegacy(J)Lnet/flixster/android/model/Season;
    .locals 9
    .parameter "seasonId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 382
    invoke-static {p0, p1}, Lnet/flixster/android/data/MovieDao;->getSeason(J)Lnet/flixster/android/model/Season;

    move-result-object v5

    .line 389
    .local v5, season:Lnet/flixster/android/model/Season;
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-static {p0, p1}, Lnet/flixster/android/data/ApiBuilder;->season(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v6, 0x0

    .line 390
    :cond_0
    const/4 v8, 0x1

    .line 389
    invoke-static {v7, v6, v8}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 398
    .local v4, response:Ljava/lang/String;
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 399
    .local v2, jsonMovie:Lorg/json/JSONObject;
    invoke-virtual {v5, v2}, Lnet/flixster/android/model/Season;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Season;

    .line 400
    invoke-virtual {v5}, Lnet/flixster/android/model/Season;->setDetailsApiParsed()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 407
    return-object v5

    .line 391
    .end local v2           #jsonMovie:Lorg/json/JSONObject;
    .end local v4           #response:Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 392
    .local v3, mue:Ljava/net/MalformedURLException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 393
    .end local v3           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 394
    .local v0, ie:Ljava/io/IOException;
    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6

    .line 401
    .end local v0           #ie:Ljava/io/IOException;
    .restart local v4       #response:Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 402
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6
.end method

.method public static getUnfulfillable(J)Lnet/flixster/android/model/Unfulfillable;
    .locals 5
    .parameter "id"

    .prologue
    .line 260
    invoke-static {p0, p1}, Lnet/flixster/android/model/Unfulfillable;->generateIdHash(J)J

    move-result-wide v0

    .line 261
    .local v0, hash:J
    sget-object v3, Lnet/flixster/android/data/MovieDao;->MOVIE_CACHE:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Unfulfillable;

    .line 262
    .local v2, movie:Lnet/flixster/android/model/Movie;
    if-nez v2, :cond_1

    .line 263
    new-instance v2, Lnet/flixster/android/model/Unfulfillable;

    .end local v2           #movie:Lnet/flixster/android/model/Movie;
    invoke-direct {v2, p0, p1}, Lnet/flixster/android/model/Unfulfillable;-><init>(J)V

    .line 264
    .restart local v2       #movie:Lnet/flixster/android/model/Movie;
    sget-object v3, Lnet/flixster/android/data/MovieDao;->MOVIE_CACHE:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v3, v2

    .line 268
    check-cast v3, Lnet/flixster/android/model/Unfulfillable;

    :goto_0
    return-object v3

    .line 265
    :cond_1
    instance-of v3, v2, Lnet/flixster/android/model/Unfulfillable;

    if-nez v3, :cond_0

    .line 266
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static getUserSeasonDetail(JLandroid/os/Handler;Landroid/os/Handler;)V
    .locals 3
    .parameter "seasonId"
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 287
    invoke-static {p0, p1}, Lnet/flixster/android/data/MovieDao;->getSeason(J)Lnet/flixster/android/model/Season;

    move-result-object v0

    .line 288
    .local v0, season:Lnet/flixster/android/model/Season;
    invoke-virtual {v0}, Lnet/flixster/android/model/Season;->isUserSeasonParsed()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 289
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 298
    :goto_0
    return-void

    .line 292
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v1

    new-instance v2, Lnet/flixster/android/data/MovieDao$1;

    invoke-direct {v2, p0, p1, p2, p3}, Lnet/flixster/android/data/MovieDao$1;-><init>(JLandroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v1, v2}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static getUserSeasonDetailBg(JLandroid/os/Handler;Landroid/os/Handler;)V
    .locals 16
    .parameter "seasonId"
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 302
    invoke-static/range {p0 .. p1}, Lnet/flixster/android/data/MovieDao;->getSeason(J)Lnet/flixster/android/model/Season;

    move-result-object v9

    .line 303
    .local v9, season:Lnet/flixster/android/model/Season;
    invoke-virtual {v9}, Lnet/flixster/android/model/Season;->isDetailsApiParsed()Z

    move-result v13

    if-nez v13, :cond_0

    .line 305
    :try_start_0
    invoke-static/range {p0 .. p1}, Lnet/flixster/android/data/MovieDao;->getSeasonDetailLegacy(J)Lnet/flixster/android/model/Season;
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 315
    :cond_0
    const/4 v12, 0x0

    .line 317
    .local v12, user:Lnet/flixster/android/model/User;
    :try_start_1
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->fetchUser()Lnet/flixster/android/model/User;
    :try_end_1
    .catch Lnet/flixster/android/data/DaoException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v12

    .line 328
    :try_start_2
    move-wide/from16 v0, p0

    invoke-virtual {v12, v0, v1}, Lnet/flixster/android/model/User;->getLockerRightId(J)J

    move-result-wide v13

    invoke-static {v13, v14}, Lnet/flixster/android/model/LockerRight;->getRawSeasonRightId(J)J

    move-result-wide v10

    .line 329
    .local v10, seasonRightId:J
    new-instance v14, Ljava/net/URL;

    invoke-static {v10, v11}, Lnet/flixster/android/data/ApiBuilder;->rightEpisodes(J)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v14, v13}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 330
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v13

    if-eqz v13, :cond_2

    const/4 v13, 0x0

    :goto_0
    const/4 v15, 0x1

    .line 329
    invoke-static {v14, v13, v15}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v7

    .line 340
    .local v7, response:Ljava/lang/String;
    invoke-static {v7}, Lnet/flixster/android/data/DaoException;->hasError(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 341
    if-eqz p3, :cond_1

    .line 342
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v7}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 360
    .end local v7           #response:Ljava/lang/String;
    .end local v10           #seasonRightId:J
    .end local v12           #user:Lnet/flixster/android/model/User;
    :cond_1
    :goto_1
    return-void

    .line 306
    :catch_0
    move-exception v2

    .line 307
    .local v2, de:Lnet/flixster/android/data/DaoException;
    if-eqz p3, :cond_1

    .line 308
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 318
    .end local v2           #de:Lnet/flixster/android/data/DaoException;
    .restart local v12       #user:Lnet/flixster/android/model/User;
    :catch_1
    move-exception v2

    .line 319
    .restart local v2       #de:Lnet/flixster/android/data/DaoException;
    if-eqz p3, :cond_1

    .line 320
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 330
    .end local v2           #de:Lnet/flixster/android/data/DaoException;
    .restart local v10       #seasonRightId:J
    :cond_2
    const/4 v13, 0x1

    goto :goto_0

    .line 331
    .end local v10           #seasonRightId:J
    :catch_2
    move-exception v6

    .line 332
    .local v6, mue:Ljava/net/MalformedURLException;
    new-instance v13, Ljava/lang/RuntimeException;

    invoke-direct {v13, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v13

    .line 333
    .end local v6           #mue:Ljava/net/MalformedURLException;
    :catch_3
    move-exception v3

    .line 334
    .local v3, ie:Ljava/io/IOException;
    if-eqz p3, :cond_1

    .line 335
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v3}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 348
    .end local v3           #ie:Ljava/io/IOException;
    .restart local v7       #response:Ljava/lang/String;
    .restart local v10       #seasonRightId:J
    :cond_3
    :try_start_3
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 349
    .local v5, jsonArray:Lorg/json/JSONArray;
    invoke-virtual {v9, v5}, Lnet/flixster/android/model/Season;->mergeEpisodes(Lorg/json/JSONArray;)Ljava/util/Collection;

    move-result-object v8

    .line 350
    .local v8, rights:Ljava/util/Collection;,"Ljava/util/Collection<Lnet/flixster/android/model/LockerRight;>;"
    invoke-virtual {v12, v8}, Lnet/flixster/android/model/User;->addLockerRights(Ljava/util/Collection;)V

    .line 351
    if-eqz p2, :cond_1

    .line 352
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14, v9}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_4

    goto :goto_1

    .line 354
    .end local v5           #jsonArray:Lorg/json/JSONArray;
    .end local v8           #rights:Ljava/util/Collection;,"Ljava/util/Collection<Lnet/flixster/android/model/LockerRight;>;"
    :catch_4
    move-exception v4

    .line 355
    .local v4, je:Lorg/json/JSONException;
    if-eqz p3, :cond_1

    .line 356
    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v4}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method

.method public static searchMovies(Ljava/lang/String;Ljava/util/List;)V
    .locals 12
    .parameter "query"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 182
    .local p1, resultList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Movie;>;"
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "MovieDao.searchMovies query:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    invoke-static {p0}, Lnet/flixster/android/data/ApiBuilder;->searchMovies(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Lnet/flixster/android/data/MovieDao;->fetchJsonList(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    .line 186
    .local v7, response:Ljava/lang/String;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 187
    .local v2, jsonMovies:Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 188
    .local v6, moviesLength:I
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 190
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "MovieDao.searchMovies moviesLength:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v6, :cond_0

    .line 201
    return-void

    .line 192
    :cond_0
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 193
    .local v8, tempMovieObj:Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 194
    .local v4, movieId:J
    invoke-static {v4, v5}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v3

    .line 195
    .local v3, movie:Lnet/flixster/android/model/Movie;
    invoke-virtual {v3, v8}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 196
    invoke-interface {p1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    .end local v0           #i:I
    .end local v2           #jsonMovies:Lorg/json/JSONArray;
    .end local v3           #movie:Lnet/flixster/android/model/Movie;
    .end local v4           #movieId:J
    .end local v6           #moviesLength:I
    .end local v8           #tempMovieObj:Lorg/json/JSONObject;
    :catch_0
    move-exception v1

    .line 199
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9
.end method
