.class Lnet/flixster/android/data/NetflixDao$1;
.super Ljava/lang/Object;
.source "NetflixDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/NetflixDao;->fetchQueue(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$qtype:Ljava/lang/String;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/NetflixDao$1;->val$qtype:Ljava/lang/String;

    iput-object p2, p0, Lnet/flixster/android/data/NetflixDao$1;->val$successHandler:Landroid/os/Handler;

    iput-object p3, p0, Lnet/flixster/android/data/NetflixDao$1;->val$errorHandler:Landroid/os/Handler;

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 156
    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lnet/flixster/android/data/NetflixDao$1;->val$qtype:Ljava/lang/String;

    invoke-static {v2, v3}, Lnet/flixster/android/data/NetflixDao;->fetchQueue(ILjava/lang/String;)Lnet/flixster/android/model/NetflixQueue;

    move-result-object v1

    .line 157
    .local v1, queue:Lnet/flixster/android/model/NetflixQueue;
    iget-object v2, p0, Lnet/flixster/android/data/NetflixDao$1;->val$successHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v3, v4, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    .end local v1           #queue:Lnet/flixster/android/model/NetflixQueue;
    :goto_0
    return-void

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, e:Ljava/lang/Exception;
    iget-object v3, p0, Lnet/flixster/android/data/NetflixDao$1;->val$errorHandler:Landroid/os/Handler;

    .line 160
    iget-object v2, p0, Lnet/flixster/android/data/NetflixDao$1;->val$qtype:Ljava/lang/String;

    const-string v4, "/queues/disc/available"

    if-ne v2, v4, :cond_0

    const/4 v2, 0x3

    .line 159
    :goto_1
    invoke-static {v5, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 161
    :cond_0
    const/4 v2, 0x4

    goto :goto_1
.end method
