.class public Lnet/flixster/android/data/NetflixDao;
.super Ljava/lang/Object;
.source "NetflixDao.java"


# static fields
.field public static final MAX_RESULTS:I = 0x19

.field public static final NETFLIX_ACCESS_TOKEN_URL:Ljava/lang/String; = "http://api-public.netflix.com/oauth/access_token"

.field public static final NETFLIX_APPLICATION_NAME:Ljava/lang/String; = "Movies"

.field public static final NETFLIX_AUTHORIZE_WEBSITE_URL:Ljava/lang/String; = "https://api-user.netflix.com/oauth/login"

.field public static final NETFLIX_CALLBACK_URL:Ljava/lang/String; = "flixster://netflix/queue"

.field public static final NETFLIX_CONSUMER_KEY:Ljava/lang/String; = "rtuhbkms7jmpsn3sft3va7y6"

.field public static final NETFLIX_CONSUMER_SECRET:Ljava/lang/String; = "YDPK6rDpPF"

.field public static final NETFLIX_REQUEST_TOKEN_URL:Ljava/lang/String; = "http://api-public.netflix.com/oauth/request_token"

.field public static final QTYPE_ATHOME:Ljava/lang/String; = "/at_home"

.field public static final QTYPE_DISC:Ljava/lang/String; = "/queues/disc/available"

.field public static final QTYPE_INSTANT:Ljava/lang/String; = "/queues/instant/available"

.field public static final QTYPE_SAVED:Ljava/lang/String; = "/queues/disc/saved"

.field private static final TIMEOUT_HTTP_GET:I = 0x1770

.field private static final TIMEOUT_HTTP_POST:I = 0x1f40

.field public static final URL_NEWACCOUNT:Ljava/lang/String; = "http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817"

.field private static final URL_NEWACCOUNT_IMPRESSION:Ljava/lang/String; = "http://gan.doubleclick.net/gan_impression?lid=41000000030512611&pubid=21000000000262817"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertFlxDbNetflixIdToNetflixUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "flxDbNetflixId"

    .prologue
    .line 373
    const-string v0, "api.netflix.com"

    const-string v1, "api-public.netflix.com"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static convertNetflixUrlToFlxDbNetflixId(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "netflixMovieUrl"

    .prologue
    .line 369
    const-string v0, "api-public.netflix.com"

    const-string v1, "api.netflix.com"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static deleteQueueItem(Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .parameter "netflixMovieId"
    .parameter "queueType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Loauth/signpost/exception/OAuthMessageSignerException;,
            Loauth/signpost/exception/OAuthExpectationFailedException;,
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Loauth/signpost/exception/OAuthNotAuthorizedException;,
            Loauth/signpost/exception/OAuthCommunicationException;
        }
    .end annotation

    .prologue
    const/16 v11, 0x1f40

    .line 224
    const-string v8, "FlxNf"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NetflixDao.deleteQueueItem "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    new-instance v2, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 226
    .local v2, httpClient:Lorg/apache/http/client/HttpClient;
    invoke-interface {v2}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 227
    .local v0, clientParameters:Lorg/apache/http/params/HttpParams;
    invoke-static {v0, v11}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 228
    invoke-static {v0, v11}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 230
    invoke-static {p1, p0}, Lnet/flixster/android/data/ApiBuilder;->netflixDeleteQueue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 231
    .local v7, url:Ljava/lang/String;
    const-string v8, "FlxNf"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NetflixDao.deleteQueueItem url:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    new-instance v4, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v4, v7}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 234
    .local v4, request:Lorg/apache/http/client/methods/HttpDelete;
    invoke-virtual {v4}, Lorg/apache/http/client/methods/HttpDelete;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v8

    const-string v9, "http.protocol.expect-continue"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    .line 236
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getCommonsHttpConsumer()Loauth/signpost/OAuthConsumer;

    move-result-object v1

    .line 237
    .local v1, consumer:Loauth/signpost/OAuthConsumer;
    invoke-interface {v1, v4}, Loauth/signpost/OAuthConsumer;->sign(Ljava/lang/Object;)Loauth/signpost/http/HttpRequest;

    .line 239
    invoke-interface {v2, v4}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v5

    .line 240
    .local v5, response:Lorg/apache/http/HttpResponse;
    const-string v8, "FlxNf"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NetflixDao.deleteQueueItem post response: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    .line 244
    .local v6, statusCode:I
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/StatusLine;->getReasonPhrase()Ljava/lang/String;

    move-result-object v3

    .line 246
    .local v3, reason:Ljava/lang/String;
    invoke-interface {v5}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v8

    invoke-interface {v8}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 247
    const/16 v8, 0xc8

    if-eq v6, v8, :cond_0

    .line 248
    const-string v8, "FlxNf"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NetflixDao.deleteQueueItem  statusCode:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " reason:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    new-instance v8, Loauth/signpost/exception/OAuthNotAuthorizedException;

    invoke-direct {v8}, Loauth/signpost/exception/OAuthNotAuthorizedException;-><init>()V

    throw v8

    .line 251
    :cond_0
    const-string v8, "FlxNf"

    const-string v9, "NetflixDao.deleteQueueItem deleted"

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    return-void
.end method

.method public static fetchInsertFlixsterMovies(Ljava/util/ArrayList;)V
    .locals 18
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/NetflixQueueItem;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 322
    .local p0, netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    const-string v14, "FlxNf"

    const-string v15, "NetflixDao.fetchInsertFlixsterMovies()"

    invoke-static {v14, v15}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 324
    .local v12, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v14, Lorg/apache/http/message/BasicNameValuePair;

    const-string v15, "view"

    const-string v16, "long"

    invoke-direct/range {v14 .. v16}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 327
    .local v2, idToItemMap:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lnet/flixster/android/model/NetflixQueueItem;>;"
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-nez v15, :cond_1

    .line 336
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->netflixMovies()Ljava/lang/String;

    move-result-object v14

    invoke-static {v14, v12}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v5

    .line 337
    .local v5, jsonStringResult:Ljava/lang/String;
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 338
    .local v10, netflixFlixsterMovies:Lorg/json/JSONArray;
    invoke-virtual {v10}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 339
    .local v6, length:I
    const-string v14, "FlxNf"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v16, "NetflixDao.fetchInsertFlixsterMovies() length:"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    if-lt v0, v6, :cond_2

    .line 362
    return-void

    .line 327
    .end local v0           #i:I
    .end local v5           #jsonStringResult:Ljava/lang/String;
    .end local v6           #length:I
    .end local v10           #netflixFlixsterMovies:Lorg/json/JSONArray;
    :cond_1
    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/NetflixQueueItem;

    .line 329
    .local v3, item:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-virtual {v3}, Lnet/flixster/android/model/NetflixQueueItem;->getNetflixUrlId()Ljava/lang/String;

    move-result-object v1

    .line 330
    .local v1, id:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 331
    invoke-virtual {v2, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 332
    new-instance v15, Lorg/apache/http/message/BasicNameValuePair;

    const-string v16, "id"

    invoke-static {v1}, Lnet/flixster/android/data/NetflixDao;->convertNetflixUrlToFlxDbNetflixId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    invoke-direct/range {v15 .. v17}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v12, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 344
    .end local v1           #id:Ljava/lang/String;
    .end local v3           #item:Lnet/flixster/android/model/NetflixQueueItem;
    .restart local v0       #i:I
    .restart local v5       #jsonStringResult:Ljava/lang/String;
    .restart local v6       #length:I
    .restart local v10       #netflixFlixsterMovies:Lorg/json/JSONArray;
    :cond_2
    invoke-virtual {v10, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 345
    .local v4, jsonMovie:Lorg/json/JSONObject;
    const-string v14, "id"

    invoke-virtual {v4, v14}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 346
    .local v8, movieId:J
    invoke-static {v8, v9}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v7

    .line 347
    .local v7, m:Lnet/flixster/android/model/Movie;
    invoke-virtual {v7, v4}, Lnet/flixster/android/model/Movie;->netflixParseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 348
    const-string v14, "netflix"

    invoke-virtual {v7, v14}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 349
    .local v11, netflixUrlId:Ljava/lang/String;
    invoke-static {v11}, Lnet/flixster/android/data/NetflixDao;->convertFlxDbNetflixIdToNetflixUrl(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 350
    const-string v14, "netflix"

    invoke-virtual {v7, v14, v11}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    invoke-virtual {v2, v11}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lnet/flixster/android/model/NetflixQueueItem;

    .line 352
    .local v13, tempItem:Lnet/flixster/android/model/NetflixQueueItem;
    if-eqz v13, :cond_3

    .line 353
    iput-object v7, v13, Lnet/flixster/android/model/NetflixQueueItem;->mMovie:Lnet/flixster/android/model/Movie;

    .line 343
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static fetchQueue(ILjava/lang/String;)Lnet/flixster/android/model/NetflixQueue;
    .locals 12
    .parameter "startIndex"
    .parameter "qtype"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/16 v11, 0x1770

    .line 168
    const-string v8, "FlxNf"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NetflixDao.fetchQueue "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    new-instance v7, Ljava/net/URL;

    const/16 v8, 0x19

    invoke-static {p1, p0, v8}, Lnet/flixster/android/data/ApiBuilder;->netflixGetQueue(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 171
    .local v7, url:Ljava/net/URL;
    const-string v8, "FlxNf"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NetflixDao.fetchQueue url:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 173
    invoke-virtual {v7}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v5

    check-cast v5, Ljava/net/HttpURLConnection;

    .line 174
    .local v5, request:Ljava/net/HttpURLConnection;
    invoke-virtual {v5, v11}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 175
    invoke-virtual {v5, v11}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 177
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getNewConsumer()Loauth/signpost/OAuthConsumer;

    move-result-object v1

    .line 178
    .local v1, consumer:Loauth/signpost/OAuthConsumer;
    invoke-interface {v1, v5}, Loauth/signpost/OAuthConsumer;->sign(Ljava/lang/Object;)Loauth/signpost/http/HttpRequest;

    .line 181
    :try_start_0
    invoke-static {v5}, Lnet/flixster/android/util/HttpHelper;->fetchUrlBytes(Ljava/net/HttpURLConnection;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 183
    .local v0, byteString:[B
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 201
    new-instance v6, Ljava/lang/String;

    const-string v8, "ISO-8859-1"

    invoke-direct {v6, v0, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 204
    .local v6, result:Ljava/lang/String;
    new-instance v4, Lorg/json/JSONObject;

    invoke-virtual {v6}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 206
    .local v4, queueObject:Lorg/json/JSONObject;
    const-string v8, "queue"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "at_home"

    invoke-virtual {v4, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 207
    :cond_0
    new-instance v3, Lnet/flixster/android/model/NetflixQueue;

    invoke-direct {v3}, Lnet/flixster/android/model/NetflixQueue;-><init>()V

    .line 208
    .local v3, queue:Lnet/flixster/android/model/NetflixQueue;
    const-string v8, "/queues/disc/available"

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-virtual {v3, v4, v8}, Lnet/flixster/android/model/NetflixQueue;->parseFromJson(Lorg/json/JSONObject;Z)Lnet/flixster/android/model/NetflixQueue;

    .line 210
    invoke-virtual {v3}, Lnet/flixster/android/model/NetflixQueue;->getNetflixQueueItemList()Ljava/util/ArrayList;

    move-result-object v2

    .line 211
    .local v2, netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    const-string v8, "FlxNf"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NetflixDao.fetchQueue fetched size:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const-string v8, "FlxNf"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "NetflixDao.fetchQueue total size:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lnet/flixster/android/model/NetflixQueue;->getNumberOfResults()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-static {v2}, Lnet/flixster/android/data/NetflixDao;->fetchInsertFlixsterMovies(Ljava/util/ArrayList;)V

    .line 215
    return-object v3

    .line 182
    .end local v0           #byteString:[B
    .end local v2           #netflixQueueItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NetflixQueueItem;>;"
    .end local v3           #queue:Lnet/flixster/android/model/NetflixQueue;
    .end local v4           #queueObject:Lorg/json/JSONObject;
    .end local v6           #result:Ljava/lang/String;
    :catchall_0
    move-exception v8

    .line 183
    invoke-virtual {v5}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 184
    throw v8

    .line 217
    .restart local v0       #byteString:[B
    .restart local v4       #queueObject:Lorg/json/JSONObject;
    .restart local v6       #result:Ljava/lang/String;
    :cond_1
    new-instance v8, Ljava/lang/Exception;

    const-string v9, "JSON object didn\'t have a \'queue\'"

    invoke-direct {v8, v9}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v8
.end method

.method public static fetchQueue(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "qtype"

    .prologue
    .line 152
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/NetflixDao$1;

    invoke-direct {v1, p2, p0, p1}, Lnet/flixster/android/data/NetflixDao$1;-><init>(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 165
    return-void
.end method

.method public static fetchTitleState(Ljava/lang/String;)Lnet/flixster/android/model/NetflixTitleState;
    .locals 13
    .parameter "movieId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Loauth/signpost/exception/OAuthMessageSignerException;,
            Loauth/signpost/exception/OAuthExpectationFailedException;,
            Lorg/json/JSONException;,
            Loauth/signpost/exception/OAuthCommunicationException;
        }
    .end annotation

    .prologue
    .line 118
    const-string v10, "FlxNf"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "NetflixDao.fetchTitleState "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    new-instance v9, Ljava/net/URL;

    invoke-static {p0}, Lnet/flixster/android/data/ApiBuilder;->netflixTitleState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 121
    .local v9, url:Ljava/net/URL;
    const-string v10, "FlxNf"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "NetflixDao.fetchTitleState url:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    invoke-virtual {v9}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v7

    check-cast v7, Ljava/net/HttpURLConnection;

    .line 124
    .local v7, request:Ljava/net/HttpURLConnection;
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getNewConsumer()Loauth/signpost/OAuthConsumer;

    move-result-object v0

    .line 125
    .local v0, consumer:Loauth/signpost/OAuthConsumer;
    invoke-interface {v0, v7}, Loauth/signpost/OAuthConsumer;->sign(Ljava/lang/Object;)Loauth/signpost/http/HttpRequest;

    .line 127
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 129
    .local v8, result:Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 131
    .local v1, in:Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .local v2, inputline:Ljava/lang/String;
    if-nez v2, :cond_1

    .line 135
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 138
    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v3, v10}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 139
    .local v3, jsonResultObject:Lorg/json/JSONObject;
    const/4 v6, 0x0

    .line 140
    .local v6, netflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    const-string v10, "title_states"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 141
    const-string v10, "title_states"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 142
    .local v5, jsonTitleStates:Lorg/json/JSONObject;
    const-string v10, "title_state"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 143
    const-string v10, "title_state"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 144
    .local v4, jsonTitleState:Lorg/json/JSONObject;
    new-instance v6, Lnet/flixster/android/model/NetflixTitleState;

    .end local v6           #netflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-direct {v6}, Lnet/flixster/android/model/NetflixTitleState;-><init>()V

    .line 145
    .restart local v6       #netflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    invoke-virtual {v6, v4}, Lnet/flixster/android/model/NetflixTitleState;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/NetflixTitleState;

    .line 148
    .end local v4           #jsonTitleState:Lorg/json/JSONObject;
    .end local v5           #jsonTitleStates:Lorg/json/JSONObject;
    :cond_0
    return-object v6

    .line 132
    .end local v3           #jsonResultObject:Lorg/json/JSONObject;
    .end local v6           #netflixTitleState:Lnet/flixster/android/model/NetflixTitleState;
    :cond_1
    :try_start_1
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 134
    .end local v1           #in:Ljava/io/BufferedReader;
    .end local v2           #inputline:Ljava/lang/String;
    :catchall_0
    move-exception v10

    .line 135
    invoke-virtual {v7}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 136
    throw v10
.end method

.method public static getCommonsHttpConsumer()Loauth/signpost/OAuthConsumer;
    .locals 3

    .prologue
    .line 101
    new-instance v0, Loauth/signpost/commonshttp/CommonsHttpOAuthConsumer;

    const-string v1, "rtuhbkms7jmpsn3sft3va7y6"

    const-string v2, "YDPK6rDpPF"

    invoke-direct {v0, v1, v2}, Loauth/signpost/commonshttp/CommonsHttpOAuthConsumer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    .local v0, consumer:Loauth/signpost/OAuthConsumer;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixOAuthToken()Ljava/lang/String;

    move-result-object v1

    .line 103
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixOAuthTokenSecret()Ljava/lang/String;

    move-result-object v2

    .line 102
    invoke-interface {v0, v1, v2}, Loauth/signpost/OAuthConsumer;->setTokenWithSecret(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    return-object v0
.end method

.method public static getCommonsHttpProvider()Loauth/signpost/OAuthProvider;
    .locals 4

    .prologue
    .line 108
    new-instance v0, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;

    const-string v1, "http://api-public.netflix.com/oauth/request_token"

    .line 109
    const-string v2, "http://api-public.netflix.com/oauth/access_token"

    const-string v3, "https://api-user.netflix.com/oauth/login"

    .line 108
    invoke-direct {v0, v1, v2, v3}, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    .local v0, provider:Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Loauth/signpost/commonshttp/CommonsHttpOAuthProvider;->setOAuth10a(Z)V

    .line 111
    return-object v0
.end method

.method public static getNewConsumer()Loauth/signpost/OAuthConsumer;
    .locals 3

    .prologue
    .line 87
    new-instance v0, Loauth/signpost/basic/DefaultOAuthConsumer;

    const-string v1, "rtuhbkms7jmpsn3sft3va7y6"

    const-string v2, "YDPK6rDpPF"

    invoke-direct {v0, v1, v2}, Loauth/signpost/basic/DefaultOAuthConsumer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .local v0, consumer:Loauth/signpost/OAuthConsumer;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixOAuthToken()Ljava/lang/String;

    move-result-object v1

    .line 89
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getNetflixOAuthTokenSecret()Ljava/lang/String;

    move-result-object v2

    .line 88
    invoke-interface {v0, v1, v2}, Loauth/signpost/OAuthConsumer;->setTokenWithSecret(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    return-object v0
.end method

.method public static getNewProvider()Loauth/signpost/OAuthProvider;
    .locals 4

    .prologue
    .line 94
    new-instance v0, Loauth/signpost/basic/DefaultOAuthProvider;

    const-string v1, "http://api-public.netflix.com/oauth/request_token"

    const-string v2, "http://api-public.netflix.com/oauth/access_token"

    .line 95
    const-string v3, "https://api-user.netflix.com/oauth/login"

    .line 94
    invoke-direct {v0, v1, v2, v3}, Loauth/signpost/basic/DefaultOAuthProvider;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    .local v0, provider:Loauth/signpost/OAuthProvider;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Loauth/signpost/OAuthProvider;->setOAuth10a(Z)V

    .line 97
    return-object v0
.end method

.method public static postQueueItem(Ljava/lang/String;ILjava/lang/String;)V
    .locals 19
    .parameter "netflixMovieUrlId"
    .parameter "nPosition"
    .parameter "queueType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Loauth/signpost/exception/OAuthMessageSignerException;,
            Lorg/json/JSONException;,
            Loauth/signpost/exception/OAuthExpectationFailedException;,
            Lorg/apache/http/client/ClientProtocolException;,
            Ljava/io/IOException;,
            Loauth/signpost/exception/OAuthNotAuthorizedException;,
            Loauth/signpost/exception/OAuthCommunicationException;
        }
    .end annotation

    .prologue
    .line 258
    const-string v16, "FlxNf"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "NetflixDao.postQueueItem "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, " "

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    new-instance v7, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v7}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V

    .line 261
    .local v7, httpClient:Lorg/apache/http/client/HttpClient;
    invoke-interface {v7}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v3

    .line 262
    .local v3, clientParameters:Lorg/apache/http/params/HttpParams;
    const/16 v16, 0x1f40

    move/from16 v0, v16

    invoke-static {v3, v0}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 263
    const/16 v16, 0x1f40

    move/from16 v0, v16

    invoke-static {v3, v0}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 265
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-static {v0, v1, v2}, Lnet/flixster/android/data/ApiBuilder;->netflixPostQueue(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v15

    .line 266
    .local v15, url:Ljava/lang/String;
    const-string v16, "FlxNf"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "NetflixDao.postQueueItem post url: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    new-instance v10, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v10, v15}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 274
    .local v10, post:Lorg/apache/http/client/methods/HttpPost;
    invoke-virtual {v10}, Lorg/apache/http/client/methods/HttpPost;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v16

    const-string v17, "http.protocol.expect-continue"

    const/16 v18, 0x0

    invoke-interface/range {v16 .. v18}, Lorg/apache/http/params/HttpParams;->setBooleanParameter(Ljava/lang/String;Z)Lorg/apache/http/params/HttpParams;

    .line 276
    invoke-static {}, Lnet/flixster/android/data/NetflixDao;->getCommonsHttpConsumer()Loauth/signpost/OAuthConsumer;

    move-result-object v4

    .line 277
    .local v4, consumer:Loauth/signpost/OAuthConsumer;
    invoke-interface {v4, v10}, Loauth/signpost/OAuthConsumer;->sign(Ljava/lang/Object;)Loauth/signpost/http/HttpRequest;

    .line 279
    invoke-interface {v7, v10}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v12

    .line 281
    .local v12, response:Lorg/apache/http/HttpResponse;
    const-string v16, "FlxNf"

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "NetflixDao.postQueueItem post response: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v14

    .line 285
    .local v14, statusCode:I
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 287
    .local v13, responseBulder:Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    .line 288
    .local v5, content:Ljava/io/InputStream;
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 289
    const/16 v16, 0xc9

    move/from16 v0, v16

    if-eq v14, v0, :cond_3

    .line 290
    invoke-interface {v12}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v6

    .line 291
    .local v6, entity:Lorg/apache/http/HttpEntity;
    if-eqz v6, :cond_0

    .line 292
    invoke-interface {v6}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v5

    .line 293
    new-instance v11, Ljava/io/BufferedReader;

    new-instance v16, Ljava/io/InputStreamReader;

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    move-object/from16 v0, v16

    invoke-direct {v11, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 296
    .local v11, reader:Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v11}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    .local v8, line:Ljava/lang/String;
    if-nez v8, :cond_1

    .line 303
    .end local v8           #line:Ljava/lang/String;
    .end local v11           #reader:Ljava/io/BufferedReader;
    :cond_0
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lnet/flixster/android/model/NetflixError;->parseFromJSONString(Ljava/lang/String;)Lnet/flixster/android/model/NetflixError;

    move-result-object v9

    .line 304
    .local v9, netflixError:Lnet/flixster/android/model/NetflixError;
    iget v0, v9, Lnet/flixster/android/model/NetflixError;->mStatusCode:I

    move/from16 v16, v0

    const/16 v17, 0x190

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    iget v0, v9, Lnet/flixster/android/model/NetflixError;->mSubCode:I

    move/from16 v16, v0

    const/16 v17, 0x28a

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 306
    new-instance v16, Lnet/flixster/android/model/NetflixException;

    iget-object v0, v9, Lnet/flixster/android/model/NetflixError;->message:Ljava/lang/String;

    move-object/from16 v17, v0

    invoke-direct/range {v16 .. v17}, Lnet/flixster/android/model/NetflixException;-><init>(Ljava/lang/String;)V

    throw v16

    .line 297
    .end local v9           #netflixError:Lnet/flixster/android/model/NetflixError;
    .restart local v8       #line:Ljava/lang/String;
    .restart local v11       #reader:Ljava/io/BufferedReader;
    :cond_1
    invoke-virtual {v13, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "\n"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 309
    .end local v8           #line:Ljava/lang/String;
    .end local v11           #reader:Ljava/io/BufferedReader;
    .restart local v9       #netflixError:Lnet/flixster/android/model/NetflixError;
    :cond_2
    new-instance v16, Loauth/signpost/exception/OAuthNotAuthorizedException;

    invoke-direct/range {v16 .. v16}, Loauth/signpost/exception/OAuthNotAuthorizedException;-><init>()V

    throw v16

    .line 311
    .end local v6           #entity:Lorg/apache/http/HttpEntity;
    .end local v9           #netflixError:Lnet/flixster/android/model/NetflixError;
    :cond_3
    return-void
.end method

.method public static trackNewAccountImpression()V
    .locals 3

    .prologue
    .line 377
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v0

    const-string v1, "http://gan.doubleclick.net/gan_impression?lid=41000000030512611&pubid=21000000000262817"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    .line 378
    return-void
.end method

.method public static trackNewAccountReferral()V
    .locals 3

    .prologue
    .line 381
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v0

    const-string v1, "http://gan.doubleclick.net/gan_click?lid=41000000030512611&pubid=21000000000262817"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    .line 382
    return-void
.end method
