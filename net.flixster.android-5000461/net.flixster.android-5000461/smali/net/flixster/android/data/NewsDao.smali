.class public Lnet/flixster/android/data/NewsDao;
.super Ljava/lang/Object;
.source "NewsDao.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final NEWS_API:Ljava/lang/String; = "http://api.flixster.com/api/v1/news.json?limit=10&period=10"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getNewsItemList()Ljava/util/ArrayList;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/NewsStory;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 23
    :try_start_0
    new-instance v10, Ljava/net/URL;

    const-string v11, "http://api.flixster.com/api/v1/news.json?limit=10&period=10"

    invoke-direct {v10, v11}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v9

    .line 32
    .local v9, response:Ljava/lang/String;
    :try_start_1
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v9}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 33
    .local v4, jsonNewsItems:Lorg/json/JSONArray;
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v8

    .line 34
    .local v8, newsItemsLength:I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 35
    .local v7, newsItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v8, :cond_0

    .line 41
    return-object v7

    .line 24
    .end local v0           #i:I
    .end local v4           #jsonNewsItems:Lorg/json/JSONArray;
    .end local v7           #newsItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    .end local v8           #newsItemsLength:I
    .end local v9           #response:Ljava/lang/String;
    :catch_0
    move-exception v5

    .line 25
    .local v5, mue:Ljava/net/MalformedURLException;
    new-instance v10, Ljava/lang/RuntimeException;

    invoke-direct {v10, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    .line 26
    .end local v5           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 27
    .local v1, ie:Ljava/io/IOException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v10

    throw v10

    .line 36
    .end local v1           #ie:Ljava/io/IOException;
    .restart local v0       #i:I
    .restart local v4       #jsonNewsItems:Lorg/json/JSONArray;
    .restart local v7       #newsItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    .restart local v8       #newsItemsLength:I
    .restart local v9       #response:Ljava/lang/String;
    :cond_0
    :try_start_2
    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 37
    .local v3, jsonNewsItem:Lorg/json/JSONObject;
    new-instance v6, Lnet/flixster/android/model/NewsStory;

    invoke-direct {v6}, Lnet/flixster/android/model/NewsStory;-><init>()V

    .line 38
    .local v6, newsItem:Lnet/flixster/android/model/NewsStory;
    invoke-virtual {v6, v3}, Lnet/flixster/android/model/NewsStory;->parseJSONObject(Lorg/json/JSONObject;)Lnet/flixster/android/model/NewsStory;

    .line 39
    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 42
    .end local v0           #i:I
    .end local v3           #jsonNewsItem:Lorg/json/JSONObject;
    .end local v4           #jsonNewsItems:Lorg/json/JSONArray;
    .end local v6           #newsItem:Lnet/flixster/android/model/NewsStory;
    .end local v7           #newsItemList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/NewsStory;>;"
    .end local v8           #newsItemsLength:I
    :catch_2
    move-exception v2

    .line 43
    .local v2, je:Lorg/json/JSONException;
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v10

    throw v10
.end method
