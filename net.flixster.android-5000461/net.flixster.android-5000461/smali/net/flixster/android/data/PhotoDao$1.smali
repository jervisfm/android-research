.class Lnet/flixster/android/data/PhotoDao$1;
.super Ljava/lang/Object;
.source "PhotoDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/PhotoDao;->getUserPhotos(Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$photos:Ljava/util/List;

.field private final synthetic val$successHandler:Landroid/os/Handler;

.field private final synthetic val$username:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/PhotoDao$1;->val$username:Ljava/lang/String;

    iput-object p2, p0, Lnet/flixster/android/data/PhotoDao$1;->val$photos:Ljava/util/List;

    iput-object p3, p0, Lnet/flixster/android/data/PhotoDao$1;->val$successHandler:Landroid/os/Handler;

    iput-object p4, p0, Lnet/flixster/android/data/PhotoDao$1;->val$errorHandler:Landroid/os/Handler;

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 56
    :try_start_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "&username="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lnet/flixster/android/data/PhotoDao$1;->val$username:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/data/PhotoDao$1;->val$photos:Ljava/util/List;

    #calls: Lnet/flixster/android/data/PhotoDao;->getPhotos(Ljava/lang/String;Ljava/util/List;)V
    invoke-static {v1, v2}, Lnet/flixster/android/data/PhotoDao;->access$0(Ljava/lang/String;Ljava/util/List;)V

    .line 57
    iget-object v1, p0, Lnet/flixster/android/data/PhotoDao$1;->val$successHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v1, "FlxMain"

    const-string v2, "PhotoDao.getUserPhotos DaoException"

    invoke-static {v1, v2, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 60
    iget-object v1, p0, Lnet/flixster/android/data/PhotoDao$1;->val$errorHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
