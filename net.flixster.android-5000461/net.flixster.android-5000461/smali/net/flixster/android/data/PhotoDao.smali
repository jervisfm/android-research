.class public Lnet/flixster/android/data/PhotoDao;
.super Ljava/lang/Object;
.source "PhotoDao.java"


# static fields
.field public static final FILTER_RANDOM:Ljava/lang/String; = "random"

.field public static final FILTER_TOP_ACTORS:Ljava/lang/String; = "top-actors"

.field public static final FILTER_TOP_ACTRESSES:Ljava/lang/String; = "top-actresses"

.field public static final FILTER_TOP_MOVIES:Ljava/lang/String; = "top-movies"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 66
    invoke-static {p0, p1}, Lnet/flixster/android/data/PhotoDao;->getPhotos(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.method public static getActorPhotos(J)Ljava/util/ArrayList;
    .locals 3
    .parameter "actorId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 39
    .local v0, photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "&actor="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lnet/flixster/android/data/PhotoDao;->getPhotos(Ljava/lang/String;Ljava/util/List;)V

    .line 40
    return-object v0
.end method

.method public static getMoviePhotos(J)Ljava/util/ArrayList;
    .locals 3
    .parameter "movieId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v0, photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "&movie="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lnet/flixster/android/data/PhotoDao;->getPhotos(Ljava/lang/String;Ljava/util/List;)V

    .line 46
    return-object v0
.end method

.method private static getPhotos(Ljava/lang/String;Ljava/util/List;)V
    .locals 9
    .parameter "photoArg"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 67
    .local p1, photos:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Photo;>;"
    invoke-static {p0}, Lnet/flixster/android/data/ApiBuilder;->photos(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 70
    .local v5, url:Ljava/lang/String;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    invoke-direct {v6, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v7, 0x1

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 79
    .local v4, response:Ljava/lang/String;
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v4}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 80
    .local v3, photosArray:Lorg/json/JSONArray;
    invoke-static {v3, p1}, Lnet/flixster/android/data/PhotoDao;->parsePhotos(Lorg/json/JSONArray;Ljava/util/List;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 84
    return-void

    .line 71
    .end local v3           #photosArray:Lorg/json/JSONArray;
    .end local v4           #response:Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 72
    .local v2, mue:Ljava/net/MalformedURLException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 73
    .end local v2           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 74
    .local v0, ie:Ljava/io/IOException;
    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6

    .line 81
    .end local v0           #ie:Ljava/io/IOException;
    .restart local v4       #response:Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 82
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6
.end method

.method public static getTopPhotos(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .parameter "filter"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 33
    .local v0, photos:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Photo;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "&filter="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lnet/flixster/android/data/PhotoDao;->getPhotos(Ljava/lang/String;Ljava/util/List;)V

    .line 34
    return-object v0
.end method

.method public static getUserPhotos(Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "username"
    .parameter
    .parameter "successHandler"
    .parameter "errorHandler"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;",
            "Landroid/os/Handler;",
            "Landroid/os/Handler;",
            ")V"
        }
    .end annotation

    .prologue
    .line 52
    .local p1, photos:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Photo;>;"
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/PhotoDao$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lnet/flixster/android/data/PhotoDao$1;-><init>(Ljava/lang/String;Ljava/util/List;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 64
    return-void
.end method

.method private static parsePhotos(Lorg/json/JSONArray;Ljava/util/List;)V
    .locals 11
    .parameter "photosArray"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 88
    .local p1, photos:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Photo;>;"
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-virtual {p0}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lt v2, v9, :cond_0

    .line 122
    return-void

    .line 90
    :cond_0
    new-instance v6, Lnet/flixster/android/model/Photo;

    invoke-direct {v6}, Lnet/flixster/android/model/Photo;-><init>()V

    .line 91
    .local v6, photo:Lnet/flixster/android/model/Photo;
    invoke-virtual {p0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 92
    .local v7, photoObject:Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v9

    iput-wide v9, v6, Lnet/flixster/android/model/Photo;->id:J

    .line 93
    const-string v9, "caption"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    .line 94
    const-string v9, "description"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    .line 95
    const-string v9, "images"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 96
    const-string v9, "images"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 97
    .local v3, imagesObject:Lorg/json/JSONObject;
    const-string v9, "thumbnail"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 98
    const-string v9, "thumbnail"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 99
    .local v8, thumbnailObject:Lorg/json/JSONObject;
    const-string v9, "url"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    .line 101
    .end local v8           #thumbnailObject:Lorg/json/JSONObject;
    :cond_1
    const-string v9, "origional"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 102
    const-string v9, "origional"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 103
    .local v5, origionalObject:Lorg/json/JSONObject;
    const-string v9, "url"

    invoke-virtual {v5, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/model/Photo;->origionalUrl:Ljava/lang/String;

    .line 105
    .end local v5           #origionalObject:Lorg/json/JSONObject;
    :cond_2
    const-string v9, "gallery"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 106
    const-string v9, "gallery"

    invoke-virtual {v3, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 107
    .local v1, galleryObject:Lorg/json/JSONObject;
    const-string v9, "url"

    invoke-virtual {v1, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/model/Photo;->galleryUrl:Ljava/lang/String;

    .line 110
    .end local v1           #galleryObject:Lorg/json/JSONObject;
    .end local v3           #imagesObject:Lorg/json/JSONObject;
    :cond_3
    const-string v9, "movie"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 111
    const-string v9, "movie"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 112
    .local v4, movieObject:Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v9

    iput-wide v9, v6, Lnet/flixster/android/model/Photo;->movieId:J

    .line 113
    const-string v9, "title"

    invoke-virtual {v4, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/model/Photo;->movieTitle:Ljava/lang/String;

    .line 115
    .end local v4           #movieObject:Lorg/json/JSONObject;
    :cond_4
    const-string v9, "actor"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 116
    const-string v9, "actor"

    invoke-virtual {v7, v9}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 117
    .local v0, actorObject:Lorg/json/JSONObject;
    const-string v9, "id"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v9

    iput-wide v9, v6, Lnet/flixster/android/model/Photo;->actorId:J

    .line 118
    const-string v9, "name"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v6, Lnet/flixster/android/model/Photo;->actorName:Ljava/lang/String;

    .line 120
    .end local v0           #actorObject:Lorg/json/JSONObject;
    :cond_5
    invoke-interface {p1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0
.end method
