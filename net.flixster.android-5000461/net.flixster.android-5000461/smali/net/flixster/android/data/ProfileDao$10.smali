.class Lnet/flixster/android/data/ProfileDao$10;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->endDownload(Landroid/os/Handler;Landroid/os/Handler;JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$rightId:J

.field private final synthetic val$sonicQueueId:Ljava/lang/String;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(JLjava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-wide p1, p0, Lnet/flixster/android/data/ProfileDao$10;->val$rightId:J

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$10;->val$sonicQueueId:Ljava/lang/String;

    iput-object p4, p0, Lnet/flixster/android/data/ProfileDao$10;->val$errorHandler:Landroid/os/Handler;

    iput-object p5, p0, Lnet/flixster/android/data/ProfileDao$10;->val$successHandler:Landroid/os/Handler;

    .line 634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 638
    iget-wide v0, p0, Lnet/flixster/android/data/ProfileDao$10;->val$rightId:J

    const-string v2, "stop"

    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$10;->val$sonicQueueId:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lnet/flixster/android/data/ApiBuilder;->rightDownload(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 640
    .local v6, apiUrl:Ljava/lang/String;
    :try_start_0
    invoke-static {v6}, Lnet/flixster/android/util/HttpHelper;->deleteFromUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 645
    iget-object v0, p0, Lnet/flixster/android/data/ProfileDao$10;->val$successHandler:Landroid/os/Handler;

    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$10;->val$sonicQueueId:Ljava/lang/String;

    invoke-static {v4, v5, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 646
    :goto_0
    return-void

    .line 641
    :catch_0
    move-exception v7

    .line 642
    .local v7, ie:Ljava/io/IOException;
    iget-object v0, p0, Lnet/flixster/android/data/ProfileDao$10;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v7}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v1

    invoke-static {v4, v5, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
