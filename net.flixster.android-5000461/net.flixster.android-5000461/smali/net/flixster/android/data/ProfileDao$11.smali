.class Lnet/flixster/android/data/ProfileDao$11;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->updatePlaybackPosition(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$playPosition:Ljava/lang/String;

.field private final synthetic val$right:Lnet/flixster/android/model/LockerRight;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$11;->val$right:Lnet/flixster/android/model/LockerRight;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$11;->val$playPosition:Ljava/lang/String;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$11;->val$errorHandler:Landroid/os/Handler;

    iput-object p4, p0, Lnet/flixster/android/data/ProfileDao$11;->val$successHandler:Landroid/os/Handler;

    .line 652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 655
    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$11;->val$right:Lnet/flixster/android/model/LockerRight;

    iget-wide v3, v3, Lnet/flixster/android/model/LockerRight;->rightId:J

    iget-object v5, p0, Lnet/flixster/android/data/ProfileDao$11;->val$playPosition:Ljava/lang/String;

    invoke-static {v3, v4, v5}, Lnet/flixster/android/data/ApiBuilder;->rightPosition(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 656
    .local v0, apiUrl:Ljava/lang/String;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 658
    .local v2, parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    :try_start_0
    invoke-static {v0, v2}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 664
    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$11;->val$successHandler:Landroid/os/Handler;

    iget-object v4, p0, Lnet/flixster/android/data/ProfileDao$11;->val$playPosition:Ljava/lang/String;

    invoke-static {v7, v6, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 665
    :goto_0
    return-void

    .line 659
    :catch_0
    move-exception v1

    .line 660
    .local v1, ie:Ljava/io/IOException;
    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$11;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v4

    invoke-static {v7, v6, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
