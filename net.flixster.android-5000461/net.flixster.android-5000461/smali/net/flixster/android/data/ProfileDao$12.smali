.class Lnet/flixster/android/data/ProfileDao$12;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->streamStart(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$right:Lnet/flixster/android/model/LockerRight;

.field private final synthetic val$successHandler:Landroid/os/Handler;

.field private final synthetic val$urlType:Ljava/lang/String;


# direct methods
.method constructor <init>(Lnet/flixster/android/model/LockerRight;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$12;->val$right:Lnet/flixster/android/model/LockerRight;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$12;->val$errorHandler:Landroid/os/Handler;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$12;->val$urlType:Ljava/lang/String;

    iput-object p4, p0, Lnet/flixster/android/data/ProfileDao$12;->val$successHandler:Landroid/os/Handler;

    .line 671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 26

    .prologue
    .line 675
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getStreamStopError()[Ljava/lang/String;

    move-result-object v25

    .line 676
    .local v25, streamStopError:[Ljava/lang/String;
    if-eqz v25, :cond_0

    move-object/from16 v0, v25

    array-length v4, v0

    const/4 v6, 0x2

    if-ne v4, v6, :cond_0

    .line 677
    const/4 v4, 0x0

    aget-object v4, v25, v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 678
    .local v2, rightId:J
    const/4 v4, 0x1

    aget-object v5, v25, v4

    .line 680
    .local v5, streamId:Ljava/lang/String;
    :try_start_0
    const-string v4, "FlxDrm"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Previous streamStop error detected: rightId="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " streamId="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 681
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 680
    invoke-static {v4, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    const-string v4, "stop"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lnet/flixster/android/data/ApiBuilder;->rightStream(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    .line 683
    .local v12, apiUrl:Ljava/lang/String;
    invoke-static {v12}, Lnet/flixster/android/util/HttpHelper;->deleteFromUrl(Ljava/lang/String;)Ljava/lang/String;

    .line 684
    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-static {v4, v6}, Lnet/flixster/android/FlixsterApplication;->setStreamStopError(Ljava/lang/Long;Ljava/lang/String;)V

    .line 685
    const-string v4, "FlxDrm"

    const-string v6, "Previous streamStop error corrected"

    invoke-static {v4, v6}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 691
    .end local v2           #rightId:J
    .end local v5           #streamId:Ljava/lang/String;
    .end local v12           #apiUrl:Ljava/lang/String;
    :cond_0
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$right:Lnet/flixster/android/model/LockerRight;

    iget-wide v6, v4, Lnet/flixster/android/model/LockerRight;->rightId:J

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v11

    invoke-static/range {v6 .. v11}, Lnet/flixster/android/data/ApiBuilder;->rightStream(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v12

    .line 692
    .restart local v12       #apiUrl:Ljava/lang/String;
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 693
    .local v21, parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    const/16 v23, 0x0

    .line 695
    .local v23, response:Ljava/lang/String;
    :try_start_1
    move-object/from16 v0, v21

    invoke-static {v12, v0}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v23

    .line 701
    invoke-static/range {v23 .. v23}, Lnet/flixster/android/data/DaoException;->hasError(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 702
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$errorHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v23 .. v23}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 741
    :goto_1
    return-void

    .line 686
    .end local v12           #apiUrl:Ljava/lang/String;
    .end local v21           #parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .end local v23           #response:Ljava/lang/String;
    .restart local v2       #rightId:J
    .restart local v5       #streamId:Ljava/lang/String;
    :catch_0
    move-exception v15

    .line 687
    .local v15, e:Ljava/lang/Exception;
    const-string v4, "FlxDrm"

    const-string v6, "Previous streamStop error correction failed"

    invoke-static {v4, v6, v15}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 696
    .end local v2           #rightId:J
    .end local v5           #streamId:Ljava/lang/String;
    .end local v15           #e:Ljava/lang/Exception;
    .restart local v12       #apiUrl:Ljava/lang/String;
    .restart local v21       #parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .restart local v23       #response:Ljava/lang/String;
    :catch_1
    move-exception v17

    .line 697
    .local v17, ie:Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$errorHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v17 .. v17}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 707
    .end local v17           #ie:Ljava/io/IOException;
    :cond_1
    :try_start_2
    new-instance v20, Lorg/json/JSONObject;

    move-object/from16 v0, v20

    move-object/from16 v1, v23

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 708
    .local v20, movieAvailObj:Lorg/json/JSONObject;
    const-string v4, "assets"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 709
    .local v13, assetsObj:Lorg/json/JSONObject;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$urlType:Ljava/lang/String;

    const-string v6, "adaptive"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, "streamingAdaptive"

    :goto_2
    invoke-virtual {v13, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v24

    .line 712
    .local v24, selectedAssetObj:Lorg/json/JSONObject;
    new-instance v22, Ljava/util/HashMap;

    invoke-direct/range {v22 .. v22}, Ljava/util/HashMap;-><init>()V

    .line 713
    .local v22, params:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    if-eqz v24, :cond_3

    .line 714
    const-string v4, "fileLocation"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 715
    .local v16, fileLocation:Ljava/lang/String;
    const-string v4, "?"

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 716
    const/4 v4, 0x0

    const-string v6, "?"

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    .line 718
    :cond_2
    const-string v4, "fileLocation"

    move-object/from16 v0, v22

    move-object/from16 v1, v16

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 719
    const-string v4, "closedCaptionFileLocation"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 720
    .local v14, captionsFileLocation:Ljava/lang/String;
    const-string v4, "closedCaptionFileLocation"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 721
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v4}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 722
    const-string v4, "streamId"

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 723
    .restart local v5       #streamId:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$right:Lnet/flixster/android/model/LockerRight;

    iget-wide v6, v4, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v4, v5}, Lnet/flixster/android/FlixsterApplication;->setStreamStopError(Ljava/lang/Long;Ljava/lang/String;)V

    .line 724
    const-string v4, "streamId"

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 726
    const-string v4, "drmServerUrl"

    const-string v6, "drmServerUrl"

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 727
    const-string v4, "deviceId"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "destinationUniqueId"

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, "|"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    .line 728
    const-string v7, "deviceTypeId"

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 727
    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 729
    const-string v4, "customData"

    const-string v6, "playbackParams"

    move-object/from16 v0, v24

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 733
    .end local v5           #streamId:Ljava/lang/String;
    .end local v14           #captionsFileLocation:Ljava/lang/String;
    .end local v16           #fileLocation:Ljava/lang/String;
    :cond_3
    const-string v4, "lockerRightParams"

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v19

    .line 734
    .local v19, lockerRightsObj:Lorg/json/JSONObject;
    const-string v4, "playPosition"

    const-string v6, "playPosition"

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 736
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$successHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v22

    invoke-static {v6, v7, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    .line 737
    .end local v13           #assetsObj:Lorg/json/JSONObject;
    .end local v19           #lockerRightsObj:Lorg/json/JSONObject;
    .end local v20           #movieAvailObj:Lorg/json/JSONObject;
    .end local v22           #params:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    .end local v24           #selectedAssetObj:Lorg/json/JSONObject;
    :catch_2
    move-exception v18

    .line 738
    .local v18, je:Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$errorHandler:Landroid/os/Handler;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v18 .. v18}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v8

    invoke-static {v6, v7, v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 710
    .end local v18           #je:Lorg/json/JSONException;
    .restart local v13       #assetsObj:Lorg/json/JSONObject;
    .restart local v20       #movieAvailObj:Lorg/json/JSONObject;
    :cond_4
    :try_start_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/data/ProfileDao$12;->val$urlType:Ljava/lang/String;

    const-string v6, "low"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "streamingFixedLow"

    goto/16 :goto_2

    :cond_5
    const-string v4, "streamingFixedHigh"
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_2
.end method
