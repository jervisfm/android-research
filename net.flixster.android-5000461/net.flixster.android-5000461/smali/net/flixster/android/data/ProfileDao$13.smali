.class Lnet/flixster/android/data/ProfileDao$13;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->streamStop(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$playPosition:Ljava/lang/String;

.field private final synthetic val$right:Lnet/flixster/android/model/LockerRight;

.field private final synthetic val$streamId:Ljava/lang/String;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$13;->val$right:Lnet/flixster/android/model/LockerRight;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$13;->val$streamId:Ljava/lang/String;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$13;->val$playPosition:Ljava/lang/String;

    iput-object p4, p0, Lnet/flixster/android/data/ProfileDao$13;->val$errorHandler:Landroid/os/Handler;

    iput-object p5, p0, Lnet/flixster/android/data/ProfileDao$13;->val$successHandler:Landroid/os/Handler;

    .line 747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    .line 750
    iget-object v0, p0, Lnet/flixster/android/data/ProfileDao$13;->val$right:Lnet/flixster/android/model/LockerRight;

    iget-wide v0, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    const-string v2, "stop"

    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$13;->val$streamId:Ljava/lang/String;

    iget-object v4, p0, Lnet/flixster/android/data/ProfileDao$13;->val$playPosition:Ljava/lang/String;

    .line 751
    iget-object v5, p0, Lnet/flixster/android/data/ProfileDao$13;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v5}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v5

    .line 750
    invoke-static/range {v0 .. v5}, Lnet/flixster/android/data/ApiBuilder;->rightStream(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v6

    .line 754
    .local v6, apiUrl:Ljava/lang/String;
    :try_start_0
    invoke-static {v6}, Lnet/flixster/android/util/HttpHelper;->deleteFromUrl(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 761
    invoke-static {v8, v8}, Lnet/flixster/android/FlixsterApplication;->setStreamStopError(Ljava/lang/Long;Ljava/lang/String;)V

    .line 762
    iget-object v0, p0, Lnet/flixster/android/data/ProfileDao$13;->val$successHandler:Landroid/os/Handler;

    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$13;->val$streamId:Ljava/lang/String;

    invoke-static {v8, v9, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 763
    :goto_0
    return-void

    .line 755
    :catch_0
    move-exception v7

    .line 756
    .local v7, ie:Ljava/io/IOException;
    iget-object v0, p0, Lnet/flixster/android/data/ProfileDao$13;->val$right:Lnet/flixster/android/model/LockerRight;

    iget-wide v0, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$13;->val$streamId:Ljava/lang/String;

    invoke-static {v0, v1}, Lnet/flixster/android/FlixsterApplication;->setStreamStopError(Ljava/lang/Long;Ljava/lang/String;)V

    .line 757
    iget-object v0, p0, Lnet/flixster/android/data/ProfileDao$13;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v7}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v1

    invoke-static {v8, v9, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
