.class Lnet/flixster/android/data/ProfileDao$14;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->trackLicense(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$isSuccess:Z

.field private final synthetic val$right:Lnet/flixster/android/model/LockerRight;

.field private final synthetic val$streamId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;ZLandroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$14;->val$right:Lnet/flixster/android/model/LockerRight;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$14;->val$streamId:Ljava/lang/String;

    iput-boolean p3, p0, Lnet/flixster/android/data/ProfileDao$14;->val$isSuccess:Z

    iput-object p4, p0, Lnet/flixster/android/data/ProfileDao$14;->val$errorHandler:Landroid/os/Handler;

    .line 769
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 772
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$14;->val$right:Lnet/flixster/android/model/LockerRight;

    iget-wide v2, v2, Lnet/flixster/android/model/LockerRight;->rightId:J

    iget-object v4, p0, Lnet/flixster/android/data/ProfileDao$14;->val$streamId:Ljava/lang/String;

    iget-boolean v5, p0, Lnet/flixster/android/data/ProfileDao$14;->val$isSuccess:Z

    .line 773
    iget-object v6, p0, Lnet/flixster/android/data/ProfileDao$14;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v6

    .line 772
    invoke-static {v2, v3, v4, v5, v6}, Lnet/flixster/android/data/ApiBuilder;->rightLicenseStats(JLjava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    .line 775
    .local v0, apiUrl:Ljava/lang/String;
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v2}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 780
    :goto_0
    return-void

    .line 776
    :catch_0
    move-exception v1

    .line 777
    .local v1, ie:Ljava/io/IOException;
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$14;->val$errorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
