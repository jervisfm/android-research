.class Lnet/flixster/android/data/ProfileDao$15;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->ackLicense(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$customData:Ljava/lang/String;

.field private final synthetic val$errorHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$15;->val$customData:Ljava/lang/String;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$15;->val$errorHandler:Landroid/os/Handler;

    .line 785
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 789
    :try_start_0
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->sonicAckLicense()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$15;->val$customData:Ljava/lang/String;

    invoke-static {v2}, Lnet/flixster/android/data/ApiBuilder;->sonicAckLicenseParams(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 794
    :goto_0
    return-void

    .line 790
    :catch_0
    move-exception v0

    .line 791
    .local v0, ie:Ljava/io/IOException;
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$15;->val$errorHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
