.class Lnet/flixster/android/data/ProfileDao$16;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->uvStreamCreate(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$right:Lnet/flixster/android/model/LockerRight;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lnet/flixster/android/model/LockerRight;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$16;->val$right:Lnet/flixster/android/model/LockerRight;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$16;->val$errorHandler:Landroid/os/Handler;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$16;->val$successHandler:Landroid/os/Handler;

    .line 799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 17

    .prologue
    .line 804
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getStreamDeleteError()[Ljava/lang/String;

    move-result-object v10

    .line 805
    .local v10, streamDeleteError:[Ljava/lang/String;
    if-eqz v10, :cond_0

    array-length v12, v10

    const/4 v13, 0x2

    if-ne v12, v13, :cond_0

    .line 806
    const/4 v12, 0x0

    aget-object v7, v10, v12

    .line 807
    .local v7, movieId:Ljava/lang/String;
    const/4 v12, 0x1

    aget-object v11, v10, v12

    .line 809
    .local v11, streamId:Ljava/lang/String;
    :try_start_0
    const-string v12, "FlxDrm"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "Previous streamDelete error detected: movieId="

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " streamId="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    .line 810
    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 809
    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 811
    new-instance v12, Ljava/net/URL;

    invoke-static {v11}, Lnet/flixster/android/data/ApiBuilder;->streamDelete(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;

    .line 812
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v12, v13}, Lnet/flixster/android/FlixsterApplication;->setStreamDeleteError(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    const-string v12, "FlxDrm"

    const-string v13, "Previous streamDelete error corrected"

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 820
    .end local v7           #movieId:Ljava/lang/String;
    .end local v11           #streamId:Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v9, 0x0

    .line 823
    .local v9, response:Ljava/lang/String;
    :try_start_1
    new-instance v1, Lcom/flixster/android/net/RestClient;

    move-object/from16 v0, p0

    iget-object v12, v0, Lnet/flixster/android/data/ProfileDao$16;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-static {v12}, Lnet/flixster/android/data/ApiBuilder;->streamCreate(Lnet/flixster/android/model/LockerRight;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v1, v12}, Lcom/flixster/android/net/RestClient;-><init>(Ljava/lang/String;)V

    .line 824
    .local v1, client:Lcom/flixster/android/net/RestClient;
    sget-object v12, Lcom/flixster/android/net/RestClient$RequestMethod;->GET:Lcom/flixster/android/net/RestClient$RequestMethod;

    invoke-virtual {v1, v12}, Lcom/flixster/android/net/RestClient;->execute(Lcom/flixster/android/net/RestClient$RequestMethod;)V

    .line 825
    invoke-virtual {v1}, Lcom/flixster/android/net/RestClient;->getResponse()Ljava/lang/String;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v9

    .line 837
    const/4 v11, 0x0

    .line 839
    .restart local v11       #streamId:Ljava/lang/String;
    :try_start_2
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 840
    .local v6, jsonStreamId:Lorg/json/JSONObject;
    const-string v12, "error"

    const/4 v13, 0x0

    invoke-virtual {v6, v12, v13}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 841
    .local v3, error:Ljava/lang/String;
    if-nez v3, :cond_1

    .line 842
    const-string v12, "streamHandleId"

    invoke-virtual {v6, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_3

    move-result-object v11

    .line 853
    const-string v12, "reserved"

    invoke-static {v12, v11}, Lnet/flixster/android/FlixsterApplication;->setStreamDeleteError(Ljava/lang/String;Ljava/lang/String;)V

    .line 854
    move-object/from16 v0, p0

    iget-object v12, v0, Lnet/flixster/android/data/ProfileDao$16;->val$successHandler:Landroid/os/Handler;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14, v11}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 855
    .end local v1           #client:Lcom/flixster/android/net/RestClient;
    .end local v3           #error:Ljava/lang/String;
    .end local v6           #jsonStreamId:Lorg/json/JSONObject;
    .end local v11           #streamId:Ljava/lang/String;
    :goto_1
    return-void

    .line 814
    .end local v9           #response:Ljava/lang/String;
    .restart local v7       #movieId:Ljava/lang/String;
    .restart local v11       #streamId:Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 815
    .local v2, e:Ljava/lang/Exception;
    const-string v12, "FlxDrm"

    const-string v13, "Previous streamDelete error correction failed"

    invoke-static {v12, v13, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 829
    .end local v2           #e:Ljava/lang/Exception;
    .end local v7           #movieId:Ljava/lang/String;
    .end local v11           #streamId:Ljava/lang/String;
    .restart local v9       #response:Ljava/lang/String;
    :catch_1
    move-exception v8

    .line 830
    .local v8, mue:Ljava/net/MalformedURLException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lnet/flixster/android/data/ProfileDao$16;->val$errorHandler:Landroid/os/Handler;

    const/4 v13, 0x0

    const/4 v14, 0x0

    new-instance v15, Ljava/lang/RuntimeException;

    invoke-direct {v15, v8}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v13, v14, v15}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 832
    .end local v8           #mue:Ljava/net/MalformedURLException;
    :catch_2
    move-exception v4

    .line 833
    .local v4, ie:Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lnet/flixster/android/data/ProfileDao$16;->val$errorHandler:Landroid/os/Handler;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v4}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 844
    .end local v4           #ie:Ljava/io/IOException;
    .restart local v1       #client:Lcom/flixster/android/net/RestClient;
    .restart local v3       #error:Ljava/lang/String;
    .restart local v6       #jsonStreamId:Lorg/json/JSONObject;
    .restart local v11       #streamId:Ljava/lang/String;
    :cond_1
    :try_start_3
    move-object/from16 v0, p0

    iget-object v12, v0, Lnet/flixster/android/data/ProfileDao$16;->val$errorHandler:Landroid/os/Handler;

    const/4 v13, 0x0

    const/4 v14, 0x0

    .line 845
    sget-object v15, Lnet/flixster/android/data/DaoException$Type;->STREAM_CREATE:Lnet/flixster/android/data/DaoException$Type;

    invoke-static {v3}, Lcom/flixster/android/utils/StringHelper;->appendPeriod(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lnet/flixster/android/data/DaoException;->create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v15

    .line 844
    invoke-static {v13, v14, v15}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_1

    .line 848
    .end local v3           #error:Ljava/lang/String;
    .end local v6           #jsonStreamId:Lorg/json/JSONObject;
    :catch_3
    move-exception v5

    .line 849
    .local v5, je:Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v12, v0, Lnet/flixster/android/data/ProfileDao$16;->val$errorHandler:Landroid/os/Handler;

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v5}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v15

    invoke-static {v13, v14, v15}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v13

    invoke-virtual {v12, v13}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method
