.class Lnet/flixster/android/data/ProfileDao$17;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->uvStreamDelete(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$streamId:Ljava/lang/String;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$17;->val$streamId:Ljava/lang/String;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$17;->val$errorHandler:Landroid/os/Handler;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$17;->val$successHandler:Landroid/os/Handler;

    .line 860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 864
    :try_start_0
    new-instance v2, Ljava/net/URL;

    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$17;->val$streamId:Ljava/lang/String;

    invoke-static {v3}, Lnet/flixster/android/data/ApiBuilder;->streamDelete(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 875
    invoke-static {v6, v6}, Lnet/flixster/android/FlixsterApplication;->setStreamDeleteError(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$17;->val$successHandler:Landroid/os/Handler;

    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$17;->val$streamId:Ljava/lang/String;

    invoke-static {v6, v5, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 877
    :goto_0
    return-void

    .line 865
    :catch_0
    move-exception v1

    .line 866
    .local v1, mue:Ljava/net/MalformedURLException;
    const-string v2, "reserved"

    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$17;->val$streamId:Ljava/lang/String;

    invoke-static {v2, v3}, Lnet/flixster/android/FlixsterApplication;->setStreamDeleteError(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$17;->val$errorHandler:Landroid/os/Handler;

    new-instance v3, Ljava/lang/RuntimeException;

    invoke-direct {v3, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v6, v5, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 869
    .end local v1           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 870
    .local v0, ie:Ljava/io/IOException;
    const-string v2, "reserved"

    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$17;->val$streamId:Ljava/lang/String;

    invoke-static {v2, v3}, Lnet/flixster/android/FlixsterApplication;->setStreamDeleteError(Ljava/lang/String;Ljava/lang/String;)V

    .line 871
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$17;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v3

    invoke-static {v6, v5, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
