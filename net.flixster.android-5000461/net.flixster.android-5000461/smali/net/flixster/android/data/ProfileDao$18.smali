.class Lnet/flixster/android/data/ProfileDao$18;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->trackMskFbRequest(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$requestId:Ljava/lang/String;

.field private final synthetic val$userId:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$18;->val$requestId:Ljava/lang/String;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$18;->val$userId:Ljava/lang/String;

    .line 883
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 886
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ProfileDao.trackMskFbRequest "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lnet/flixster/android/data/ProfileDao$18;->val$requestId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for user "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lnet/flixster/android/data/ProfileDao$18;->val$userId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 887
    new-instance v0, Lcom/flixster/android/net/RestClient;

    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$18;->val$userId:Ljava/lang/String;

    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$18;->val$requestId:Ljava/lang/String;

    invoke-static {v2, v3}, Lnet/flixster/android/data/ApiBuilder;->fbInvites(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/flixster/android/net/RestClient;-><init>(Ljava/lang/String;)V

    .line 889
    .local v0, client:Lcom/flixster/android/net/RestClient;
    :try_start_0
    sget-object v2, Lcom/flixster/android/net/RestClient$RequestMethod;->POST:Lcom/flixster/android/net/RestClient$RequestMethod;

    invoke-virtual {v0, v2}, Lcom/flixster/android/net/RestClient;->execute(Lcom/flixster/android/net/RestClient$RequestMethod;)V

    .line 891
    const-string v2, "FlxMain"

    .line 892
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ProfileDao.trackMskFbRequest response: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/flixster/android/net/RestClient;->getResponseCode()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 893
    invoke-virtual {v0}, Lcom/flixster/android/net/RestClient;->getResponse()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 892
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 890
    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 897
    :goto_0
    return-void

    .line 894
    :catch_0
    move-exception v1

    .line 895
    .local v1, e:Ljava/io/IOException;
    const-string v2, "FlxMain"

    const-string v3, "ProfileDao.trackMskFbRequest"

    invoke-static {v2, v3, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
