.class Lnet/flixster/android/data/ProfileDao$19;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->getQuickRateMovies(Landroid/os/Handler;Landroid/os/Handler;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$isWts:Z

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(ZLandroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-boolean p1, p0, Lnet/flixster/android/data/ProfileDao$19;->val$isWts:Z

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$19;->val$errorHandler:Landroid/os/Handler;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$19;->val$successHandler:Landroid/os/Handler;

    .line 902
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 905
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 906
    .local v8, movies:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    const/4 v10, 0x0

    .line 908
    .local v10, response:Ljava/lang/String;
    :try_start_0
    new-instance v11, Ljava/net/URL;

    iget-boolean v12, p0, Lnet/flixster/android/data/ProfileDao$19;->val$isWts:Z

    invoke-static {v12}, Lnet/flixster/android/data/ApiBuilder;->quickRate(Z)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v11, v12, v13}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v10

    .line 917
    invoke-static {v10}, Lnet/flixster/android/data/DaoException;->hasError(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 918
    iget-object v11, p0, Lnet/flixster/android/data/ProfileDao$19;->val$errorHandler:Landroid/os/Handler;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v10}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v14

    invoke-static {v12, v13, v14}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 937
    :goto_0
    return-void

    .line 909
    :catch_0
    move-exception v9

    .line 910
    .local v9, mue:Ljava/net/MalformedURLException;
    iget-object v11, p0, Lnet/flixster/android/data/ProfileDao$19;->val$errorHandler:Landroid/os/Handler;

    const/4 v12, 0x0

    const/4 v13, 0x0

    new-instance v14, Ljava/lang/RuntimeException;

    invoke-direct {v14, v9}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v12, v13, v14}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 912
    .end local v9           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v3

    .line 913
    .local v3, ie:Ljava/io/IOException;
    iget-object v11, p0, Lnet/flixster/android/data/ProfileDao$19;->val$errorHandler:Landroid/os/Handler;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v3}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v14

    invoke-static {v12, v13, v14}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 923
    .end local v3           #ie:Ljava/io/IOException;
    :cond_0
    :try_start_1
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6, v10}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 924
    .local v6, jsonMovies:Lorg/json/JSONArray;
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v11

    if-lt v0, v11, :cond_1

    .line 936
    iget-object v11, p0, Lnet/flixster/android/data/ProfileDao$19;->val$successHandler:Landroid/os/Handler;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v8}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v14

    invoke-static {v12, v13, v14}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 925
    :cond_1
    :try_start_2
    invoke-virtual {v6, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 926
    .local v5, jsonMovie:Lorg/json/JSONObject;
    const-string v11, "id"

    invoke-virtual {v5, v11}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 927
    .local v1, id:J
    invoke-static {v1, v2}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v7

    .line 928
    .local v7, movie:Lnet/flixster/android/model/Movie;
    invoke-virtual {v7, v5}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 929
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 924
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 931
    .end local v0           #i:I
    .end local v1           #id:J
    .end local v5           #jsonMovie:Lorg/json/JSONObject;
    .end local v6           #jsonMovies:Lorg/json/JSONArray;
    .end local v7           #movie:Lnet/flixster/android/model/Movie;
    :catch_2
    move-exception v4

    .line 932
    .local v4, je:Lorg/json/JSONException;
    iget-object v11, p0, Lnet/flixster/android/data/ProfileDao$19;->val$errorHandler:Landroid/os/Handler;

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v4}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v14

    invoke-static {v12, v13, v14}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
