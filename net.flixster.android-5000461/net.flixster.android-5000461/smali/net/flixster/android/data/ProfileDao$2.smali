.class Lnet/flixster/android/data/ProfileDao$2;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->getFacebookFriends(Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$2;->val$errorHandler:Landroid/os/Handler;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$2;->val$successHandler:Landroid/os/Handler;

    .line 209
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x0

    .line 214
    :try_start_0
    new-instance v9, Ljava/net/URL;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v10

    iget-object v10, v10, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-static {v10}, Lnet/flixster/android/data/ApiBuilder;->friendsIds(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 223
    .local v7, response:Ljava/lang/String;
    :try_start_1
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 224
    .local v5, jsonUsers:Lorg/json/JSONArray;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 225
    .local v2, ids:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lt v0, v9, :cond_1

    .line 232
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 233
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v9

    invoke-interface {v2}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v10

    iput-object v10, v9, Lnet/flixster/android/model/User;->friendsIds:[Ljava/lang/Object;

    .line 235
    :cond_0
    iget-object v9, p0, Lnet/flixster/android/data/ProfileDao$2;->val$successHandler:Landroid/os/Handler;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 240
    .end local v0           #i:I
    .end local v2           #ids:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v5           #jsonUsers:Lorg/json/JSONArray;
    .end local v7           #response:Ljava/lang/String;
    :goto_1
    return-void

    .line 215
    :catch_0
    move-exception v6

    .line 216
    .local v6, mue:Ljava/net/MalformedURLException;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 217
    .end local v6           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v3

    .line 218
    .local v3, ie:Ljava/io/IOException;
    iget-object v9, p0, Lnet/flixster/android/data/ProfileDao$2;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v3}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v10

    invoke-static {v13, v12, v10}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 226
    .end local v3           #ie:Ljava/io/IOException;
    .restart local v0       #i:I
    .restart local v2       #ids:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v5       #jsonUsers:Lorg/json/JSONArray;
    .restart local v7       #response:Ljava/lang/String;
    :cond_1
    :try_start_2
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 227
    .local v8, userObject:Lorg/json/JSONObject;
    const-string v9, "id"

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v10}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 228
    .local v1, id:Ljava/lang/String;
    if-eqz v1, :cond_2

    .line 229
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 225
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    .end local v0           #i:I
    .end local v1           #id:Ljava/lang/String;
    .end local v2           #ids:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v5           #jsonUsers:Lorg/json/JSONArray;
    .end local v8           #userObject:Lorg/json/JSONObject;
    :catch_2
    move-exception v4

    .line 237
    .local v4, je:Lorg/json/JSONException;
    iget-object v9, p0, Lnet/flixster/android/data/ProfileDao$2;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v4}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v10

    invoke-static {v13, v12, v10}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method
