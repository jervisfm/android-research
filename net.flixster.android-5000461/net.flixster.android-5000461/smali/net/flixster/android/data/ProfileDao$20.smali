.class Lnet/flixster/android/data/ProfileDao$20;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->getUserRatedReviews(Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$20;->val$successHandler:Landroid/os/Handler;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$20;->val$errorHandler:Landroid/os/Handler;

    .line 949
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 953
    :try_start_0
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v3, 0x32

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getUserRatedReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 954
    .local v1, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 955
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v2

    iput-object v1, v2, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    .line 957
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$20;->val$successHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 961
    .end local v1           #reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    :goto_0
    return-void

    .line 958
    :catch_0
    move-exception v0

    .line 959
    .local v0, e:Lnet/flixster/android/data/DaoException;
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$20;->val$errorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x2

    invoke-static {v3, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
