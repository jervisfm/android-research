.class Lnet/flixster/android/data/ProfileDao$22;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->getFriendsActivity(Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$22;->val$successHandler:Landroid/os/Handler;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$22;->val$errorHandler:Landroid/os/Handler;

    .line 1094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1098
    :try_start_0
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-static {v4}, Lnet/flixster/android/data/ProfileDao;->getFriendReviews(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 1099
    .local v3, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1100
    .local v1, positiveReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_2

    .line 1105
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 1106
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v4

    iput-object v1, v4, Lnet/flixster/android/model/User;->friendsActivity:Ljava/util/List;

    .line 1107
    iget-object v4, p0, Lnet/flixster/android/data/ProfileDao$22;->val$successHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1112
    .end local v1           #positiveReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .end local v3           #reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    :cond_1
    :goto_1
    return-void

    .line 1100
    .restart local v1       #positiveReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .restart local v3       #reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/Review;

    .line 1101
    .local v2, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v2}, Lnet/flixster/android/model/Review;->isNotInterested()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1102
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1109
    .end local v1           #positiveReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    .end local v2           #review:Lnet/flixster/android/model/Review;
    .end local v3           #reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    :catch_0
    move-exception v0

    .line 1110
    .local v0, de:Lnet/flixster/android/data/DaoException;
    iget-object v4, p0, Lnet/flixster/android/data/ProfileDao$22;->val$errorHandler:Landroid/os/Handler;

    const/4 v5, 0x0

    invoke-static {v5, v6, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method
