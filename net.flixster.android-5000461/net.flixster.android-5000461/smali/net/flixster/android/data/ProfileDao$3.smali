.class Lnet/flixster/android/data/ProfileDao$3;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->postUserMovieReview(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Review;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$flixsterId:Ljava/lang/String;

.field private final synthetic val$movieId:Ljava/lang/String;

.field private final synthetic val$review:Lnet/flixster/android/model/Review;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Review;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$3;->val$flixsterId:Ljava/lang/String;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$3;->val$movieId:Ljava/lang/String;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$3;->val$review:Lnet/flixster/android/model/Review;

    iput-object p4, p0, Lnet/flixster/android/data/ProfileDao$3;->val$errorHandler:Landroid/os/Handler;

    iput-object p5, p0, Lnet/flixster/android/data/ProfileDao$3;->val$successHandler:Landroid/os/Handler;

    .line 249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 253
    :try_start_0
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$3;->val$flixsterId:Ljava/lang/String;

    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$3;->val$movieId:Ljava/lang/String;

    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$3;->val$review:Lnet/flixster/android/model/Review;

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Lnet/flixster/android/data/ProfileDao;->postUserMovieReview(Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Review;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :goto_0
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v1

    iput-object v6, v1, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    .line 259
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v1

    iput-object v6, v1, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    .line 261
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$3;->val$successHandler:Landroid/os/Handler;

    invoke-virtual {v1, v5}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 262
    return-void

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, e:Ljava/lang/Exception;
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$3;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v6, v5, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
