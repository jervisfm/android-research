.class Lnet/flixster/android/data/ProfileDao$4;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->getUserMovieReview(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$movieId:Ljava/lang/String;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$4;->val$movieId:Ljava/lang/String;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$4;->val$successHandler:Landroid/os/Handler;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$4;->val$errorHandler:Landroid/os/Handler;

    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 302
    :try_start_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getFlixsterId()Ljava/lang/String;

    move-result-object v1

    .line 303
    .local v1, flixsterId:Ljava/lang/String;
    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$4;->val$movieId:Ljava/lang/String;

    invoke-static {v1, v3}, Lnet/flixster/android/data/ProfileDao;->getUserMovieReview(Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Review;

    move-result-object v2

    .line 304
    .local v2, userMovieReview:Lnet/flixster/android/model/Review;
    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$4;->val$successHandler:Landroid/os/Handler;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v4, v5, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 308
    .end local v1           #flixsterId:Ljava/lang/String;
    .end local v2           #userMovieReview:Lnet/flixster/android/model/Review;
    :goto_0
    return-void

    .line 305
    :catch_0
    move-exception v0

    .line 306
    .local v0, e:Lnet/flixster/android/data/DaoException;
    iget-object v3, p0, Lnet/flixster/android/data/ProfileDao$4;->val$errorHandler:Landroid/os/Handler;

    invoke-static {v7, v6, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
