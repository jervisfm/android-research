.class Lnet/flixster/android/data/ProfileDao$5;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->getFriends(Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$5;->val$successHandler:Landroid/os/Handler;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$5;->val$errorHandler:Landroid/os/Handler;

    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 343
    :try_start_0
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v2

    iget-object v2, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-static {v2, v3}, Lnet/flixster/android/data/ProfileDao;->getFriends(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 344
    .local v1, friends:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/User;>;"
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v2

    iput-object v1, v2, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    .line 345
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$5;->val$successHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_0
    .catch Lnet/flixster/android/data/DaoException; {:try_start_0 .. :try_end_0} :catch_0

    .line 349
    .end local v1           #friends:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/User;>;"
    :goto_0
    return-void

    .line 346
    :catch_0
    move-exception v0

    .line 347
    .local v0, e:Lnet/flixster/android/data/DaoException;
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$5;->val$errorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-static {v3, v4, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
