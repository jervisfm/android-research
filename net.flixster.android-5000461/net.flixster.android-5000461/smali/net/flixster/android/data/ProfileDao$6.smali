.class Lnet/flixster/android/data/ProfileDao$6;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$6;->val$errorHandler:Landroid/os/Handler;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$6;->val$successHandler:Landroid/os/Handler;

    .line 391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 21

    .prologue
    .line 394
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 395
    .local v14, movies:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Movie;>;"
    const/16 v16, 0x0

    .line 397
    .local v16, response:Ljava/lang/String;
    :try_start_0
    new-instance v17, Ljava/net/URL;

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v6

    iget-object v6, v6, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-static {v6, v0}, Lnet/flixster/android/data/ApiBuilder;->userRights(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v17

    invoke-direct {v0, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 398
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x0

    :goto_0
    const/16 v18, 0x1

    .line 397
    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v6, v1}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v16

    .line 407
    invoke-static/range {v16 .. v16}, Lnet/flixster/android/data/DaoException;->hasError(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 408
    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/data/ProfileDao$6;->val$errorHandler:Landroid/os/Handler;

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 409
    invoke-static/range {v16 .. v16}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v19

    .line 408
    invoke-static/range {v17 .. v19}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 480
    :cond_0
    :goto_1
    return-void

    .line 398
    :cond_1
    const/4 v6, 0x1

    goto :goto_0

    .line 399
    :catch_0
    move-exception v15

    .line 400
    .local v15, mue:Ljava/net/MalformedURLException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v15}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 401
    .end local v15           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v10

    .line 402
    .local v10, ie:Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/data/ProfileDao$6;->val$errorHandler:Landroid/os/Handler;

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 403
    invoke-static {v10}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v19

    .line 402
    invoke-static/range {v17 .. v19}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1

    .line 414
    .end local v10           #ie:Ljava/io/IOException;
    :cond_2
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v6

    invoke-virtual {v6}, Lnet/flixster/android/model/User;->isLockerRightsFetched()Z

    move-result v6

    if-nez v6, :cond_0

    .line 419
    :try_start_1
    new-instance v13, Lorg/json/JSONArray;

    move-object/from16 v0, v16

    invoke-direct {v13, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 420
    .local v13, jsonMovies:Lorg/json/JSONArray;
    const/4 v9, 0x0

    .local v9, i:I
    :goto_2
    invoke-virtual {v13}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v6

    if-lt v9, v6, :cond_5

    .line 458
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->hasMigratedToRights()Z

    move-result v6

    if-nez v6, :cond_4

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 459
    const/4 v6, 0x1

    invoke-static {v6}, Lnet/flixster/android/FlixsterApplication;->setMigratedToRights(Z)V

    .line 460
    const-string v6, "FlxMain"

    const-string v17, "ProfileDao.getUserLockerRights migration for rights api"

    move-object/from16 v0, v17

    invoke-static {v6, v0}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 461
    invoke-virtual {v14}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-nez v17, :cond_a

    .line 466
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v6

    invoke-static {v6}, Lcom/flixster/android/drm/MigrationHelper;->migrateDownloadedFilesForRightsApi(Lnet/flixster/android/model/User;)V

    .line 469
    invoke-static {}, Lcom/flixster/android/drm/Drm;->manager()Lcom/flixster/android/drm/PlaybackManager;

    move-result-object v6

    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Lnet/flixster/android/model/User;->getLockerRights()Ljava/util/List;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-interface {v6, v0}, Lcom/flixster/android/drm/PlaybackManager;->registerAllLocalAssets(Ljava/util/List;)V

    .line 479
    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/data/ProfileDao$6;->val$successHandler:Landroid/os/Handler;

    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 421
    :cond_5
    :try_start_2
    invoke-virtual {v13, v9}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 423
    .local v3, jsonRight:Lorg/json/JSONObject;
    const/4 v7, 0x0

    .line 425
    .local v7, movie:Lnet/flixster/android/model/Movie;
    const-string v6, "movie"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    .local v12, jsonMovie:Lorg/json/JSONObject;
    if-eqz v12, :cond_8

    .line 426
    const-string v6, "id"

    invoke-virtual {v12, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 427
    .local v4, id:J
    invoke-static {v4, v5}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v7

    .line 428
    invoke-virtual {v7, v12}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 429
    new-instance v2, Lnet/flixster/android/model/LockerRight;

    sget-object v6, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/LockerRight;-><init>(Lorg/json/JSONObject;JLnet/flixster/android/model/LockerRight$RightType;Lnet/flixster/android/model/VideoAsset;)V

    .line 430
    .local v2, right:Lnet/flixster/android/model/LockerRight;
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v6

    invoke-virtual {v6, v2}, Lnet/flixster/android/model/User;->addLockerRight(Lnet/flixster/android/model/LockerRight;)V

    .line 444
    .end local v2           #right:Lnet/flixster/android/model/LockerRight;
    .end local v4           #id:J
    :cond_6
    :goto_4
    if-eqz v7, :cond_7

    .line 445
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v6

    const-string v17, "isStreamingUnsupported"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v17

    move/from16 v0, v17

    invoke-interface {v6, v0}, Lcom/flixster/android/drm/PlaybackLogic;->setDeviceStreamBlacklisted(Z)V

    .line 446
    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v6

    .line 447
    const-string v17, "isNonWifiStreamingUnsupported"

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v17

    .line 446
    move/from16 v0, v17

    invoke-interface {v6, v0}, Lcom/flixster/android/drm/PlaybackLogic;->setDeviceNonWifiStreamBlacklisted(Z)V

    .line 448
    invoke-virtual {v14, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    :cond_7
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_2

    .line 431
    :cond_8
    const-string v6, "season"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    if-eqz v12, :cond_9

    .line 432
    const-string v6, "id"

    invoke-virtual {v12, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 433
    .restart local v4       #id:J
    invoke-static {v4, v5}, Lnet/flixster/android/data/MovieDao;->getSeason(J)Lnet/flixster/android/model/Season;

    move-result-object v7

    .line 434
    invoke-virtual {v7, v12}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 435
    new-instance v2, Lnet/flixster/android/model/LockerRight;

    sget-object v6, Lnet/flixster/android/model/LockerRight$RightType;->SEASON:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/LockerRight;-><init>(Lorg/json/JSONObject;JLnet/flixster/android/model/LockerRight$RightType;Lnet/flixster/android/model/VideoAsset;)V

    .line 436
    .restart local v2       #right:Lnet/flixster/android/model/LockerRight;
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v6

    invoke-virtual {v6, v2}, Lnet/flixster/android/model/User;->addLockerRight(Lnet/flixster/android/model/LockerRight;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_4

    .line 451
    .end local v2           #right:Lnet/flixster/android/model/LockerRight;
    .end local v3           #jsonRight:Lorg/json/JSONObject;
    .end local v4           #id:J
    .end local v7           #movie:Lnet/flixster/android/model/Movie;
    .end local v9           #i:I
    .end local v12           #jsonMovie:Lorg/json/JSONObject;
    .end local v13           #jsonMovies:Lorg/json/JSONArray;
    :catch_2
    move-exception v11

    .line 452
    .local v11, je:Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/data/ProfileDao$6;->val$errorHandler:Landroid/os/Handler;

    const/16 v17, 0x0

    const/16 v18, 0x0

    .line 453
    invoke-static {v11}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v19

    .line 452
    invoke-static/range {v17 .. v19}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_1

    .line 437
    .end local v11           #je:Lorg/json/JSONException;
    .restart local v3       #jsonRight:Lorg/json/JSONObject;
    .restart local v7       #movie:Lnet/flixster/android/model/Movie;
    .restart local v9       #i:I
    .restart local v12       #jsonMovie:Lorg/json/JSONObject;
    .restart local v13       #jsonMovies:Lorg/json/JSONArray;
    :cond_9
    :try_start_3
    const-string v6, "unfulfillable"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v12

    if-eqz v12, :cond_6

    .line 438
    const-string v6, "id"

    invoke-virtual {v12, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 439
    .restart local v4       #id:J
    invoke-static {v4, v5}, Lnet/flixster/android/data/MovieDao;->getUnfulfillable(J)Lnet/flixster/android/model/Unfulfillable;

    move-result-object v7

    .line 440
    invoke-virtual {v7, v12}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 441
    new-instance v2, Lnet/flixster/android/model/LockerRight;

    sget-object v6, Lnet/flixster/android/model/LockerRight$RightType;->UNFULFILLABLE:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/LockerRight;-><init>(Lorg/json/JSONObject;JLnet/flixster/android/model/LockerRight$RightType;Lnet/flixster/android/model/VideoAsset;)V

    .line 442
    .restart local v2       #right:Lnet/flixster/android/model/LockerRight;
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v6

    invoke-virtual {v6, v2}, Lnet/flixster/android/model/User;->addLockerRight(Lnet/flixster/android/model/LockerRight;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_4

    .line 461
    .end local v2           #right:Lnet/flixster/android/model/LockerRight;
    .end local v3           #jsonRight:Lorg/json/JSONObject;
    .end local v4           #id:J
    .end local v7           #movie:Lnet/flixster/android/model/Movie;
    .end local v12           #jsonMovie:Lorg/json/JSONObject;
    :cond_a
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lnet/flixster/android/model/Movie;

    .line 462
    .local v8, asset:Lnet/flixster/android/model/Movie;
    const-class v17, Lnet/flixster/android/model/Season;

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_3

    .line 463
    invoke-virtual {v8}, Lnet/flixster/android/model/Movie;->getId()J

    move-result-wide v17

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-static/range {v17 .. v20}, Lnet/flixster/android/data/MovieDao;->getUserSeasonDetailBg(JLandroid/os/Handler;Landroid/os/Handler;)V

    goto/16 :goto_3
.end method
