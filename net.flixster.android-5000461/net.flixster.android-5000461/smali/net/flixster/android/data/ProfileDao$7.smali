.class Lnet/flixster/android/data/ProfileDao$7;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->insertLockerRight(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$purchaseType:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$7;->val$purchaseType:Ljava/lang/String;

    .line 486
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 15

    .prologue
    .line 489
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v4

    iget-object v4, v4, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    iget-object v14, p0, Lnet/flixster/android/data/ProfileDao$7;->val$purchaseType:Ljava/lang/String;

    invoke-static {v4, v14}, Lnet/flixster/android/data/ApiBuilder;->userRights(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 490
    .local v6, apiUrl:Ljava/lang/String;
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 491
    .local v12, parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    const/4 v13, 0x0

    .line 493
    .local v13, response:Ljava/lang/String;
    :try_start_0
    invoke-static {v6, v12}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 501
    :try_start_1
    new-instance v11, Lorg/json/JSONArray;

    invoke-direct {v11, v13}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 502
    .local v11, jsonMovies:Lorg/json/JSONArray;
    const/4 v7, 0x0

    .local v7, i:I
    :goto_0
    invoke-virtual {v11}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    if-lt v7, v4, :cond_0

    .line 520
    .end local v7           #i:I
    .end local v11           #jsonMovies:Lorg/json/JSONArray;
    :goto_1
    return-void

    .line 494
    :catch_0
    move-exception v8

    .line 495
    .local v8, ie:Ljava/io/IOException;
    const/4 v4, 0x0

    iget-object v14, p0, Lnet/flixster/android/data/ProfileDao$7;->val$purchaseType:Ljava/lang/String;

    #calls: Lnet/flixster/android/data/ProfileDao;->toastLockerRightInsertion(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
    invoke-static {v4, v14}, Lnet/flixster/android/data/ProfileDao;->access$1(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    goto :goto_1

    .line 503
    .end local v8           #ie:Ljava/io/IOException;
    .restart local v7       #i:I
    .restart local v11       #jsonMovies:Lorg/json/JSONArray;
    :cond_0
    :try_start_2
    invoke-virtual {v11, v7}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 505
    .local v1, jsonRight:Lorg/json/JSONObject;
    const-string v4, "movie"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .local v10, jsonMovie:Lorg/json/JSONObject;
    if-eqz v10, :cond_1

    .line 506
    const-string v4, "id"

    invoke-virtual {v10, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 507
    .local v2, id:J
    invoke-static {v2, v3}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v5

    .line 508
    .local v5, movie:Lnet/flixster/android/model/Movie;
    invoke-virtual {v5, v10}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 509
    new-instance v0, Lnet/flixster/android/model/LockerRight;

    sget-object v4, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/LockerRight;-><init>(Lorg/json/JSONObject;JLnet/flixster/android/model/LockerRight$RightType;Lnet/flixster/android/model/VideoAsset;)V

    .line 510
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v4

    invoke-virtual {v4, v0}, Lnet/flixster/android/model/User;->addLockerRight(Lnet/flixster/android/model/LockerRight;)V

    .line 511
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->access$0()Lnet/flixster/android/model/User;

    move-result-object v4

    const/4 v14, 0x1

    iput-boolean v14, v4, Lnet/flixster/android/model/User;->refreshRequired:Z

    .line 512
    iget-object v4, p0, Lnet/flixster/android/data/ProfileDao$7;->val$purchaseType:Ljava/lang/String;

    #calls: Lnet/flixster/android/data/ProfileDao;->toastLockerRightInsertion(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
    invoke-static {v0, v4}, Lnet/flixster/android/data/ProfileDao;->access$1(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 502
    .end local v0           #right:Lnet/flixster/android/model/LockerRight;
    .end local v2           #id:J
    .end local v5           #movie:Lnet/flixster/android/model/Movie;
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 517
    .end local v1           #jsonRight:Lorg/json/JSONObject;
    .end local v7           #i:I
    .end local v10           #jsonMovie:Lorg/json/JSONObject;
    .end local v11           #jsonMovies:Lorg/json/JSONArray;
    :catch_1
    move-exception v9

    .line 518
    .local v9, je:Lorg/json/JSONException;
    const/4 v4, 0x0

    iget-object v14, p0, Lnet/flixster/android/data/ProfileDao$7;->val$purchaseType:Ljava/lang/String;

    #calls: Lnet/flixster/android/data/ProfileDao;->toastLockerRightInsertion(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
    invoke-static {v4, v14}, Lnet/flixster/android/data/ProfileDao;->access$1(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    goto :goto_1
.end method
