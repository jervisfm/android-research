.class Lnet/flixster/android/data/ProfileDao$8;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->toastLockerRightInsertion(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$activity:Landroid/app/Activity;

.field private final synthetic val$purchaseType:Ljava/lang/String;

.field private final synthetic val$right:Lnet/flixster/android/model/LockerRight;


# direct methods
.method constructor <init>(Lnet/flixster/android/model/LockerRight;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/data/ProfileDao$8;->val$right:Lnet/flixster/android/model/LockerRight;

    iput-object p2, p0, Lnet/flixster/android/data/ProfileDao$8;->val$activity:Landroid/app/Activity;

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$8;->val$purchaseType:Ljava/lang/String;

    .line 529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 531
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$8;->val$right:Lnet/flixster/android/model/LockerRight;

    if-eqz v1, :cond_1

    .line 532
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$8;->val$activity:Landroid/app/Activity;

    .line 533
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$8;->val$activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0158

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lnet/flixster/android/data/ProfileDao$8;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v6}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 532
    invoke-static {v1, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 534
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$8;->val$activity:Landroid/app/Activity;

    .line 537
    iget-object v2, p0, Lnet/flixster/android/data/ProfileDao$8;->val$activity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0159

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 536
    invoke-static {v1, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    .line 538
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 539
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 540
    .local v0, user:Lnet/flixster/android/model/User;
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$8;->val$purchaseType:Ljava/lang/String;

    const-string v2, "MKW"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 541
    iput-boolean v7, v0, Lnet/flixster/android/model/User;->isMskWtsEligible:Z

    goto :goto_0

    .line 542
    :cond_2
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$8;->val$purchaseType:Ljava/lang/String;

    const-string v2, "MKR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 543
    iput-boolean v7, v0, Lnet/flixster/android/model/User;->isMskRateEligible:Z

    goto :goto_0

    .line 544
    :cond_3
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$8;->val$purchaseType:Ljava/lang/String;

    const-string v2, "MT1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 545
    iput-boolean v7, v0, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    goto :goto_0

    .line 546
    :cond_4
    iget-object v1, p0, Lnet/flixster/android/data/ProfileDao$8;->val$purchaseType:Ljava/lang/String;

    const-string v2, "MPN"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    iput-boolean v7, v0, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    goto :goto_0
.end method
