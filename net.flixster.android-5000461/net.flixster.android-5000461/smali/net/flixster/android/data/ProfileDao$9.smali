.class Lnet/flixster/android/data/ProfileDao$9;
.super Ljava/lang/Object;
.source "ProfileDao.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/data/ProfileDao;->canDownload(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final synthetic val$errorHandler:Landroid/os/Handler;

.field private final synthetic val$right:Lnet/flixster/android/model/LockerRight;

.field private final synthetic val$rightId:J

.field private final synthetic val$successHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(JLnet/flixster/android/model/LockerRight;Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-wide p1, p0, Lnet/flixster/android/data/ProfileDao$9;->val$rightId:J

    iput-object p3, p0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    iput-object p4, p0, Lnet/flixster/android/data/ProfileDao$9;->val$errorHandler:Landroid/os/Handler;

    iput-object p5, p0, Lnet/flixster/android/data/ProfileDao$9;->val$successHandler:Landroid/os/Handler;

    .line 567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 27

    .prologue
    .line 570
    move-object/from16 v0, p0

    iget-wide v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$rightId:J

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v7}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v7

    invoke-static/range {v2 .. v7}, Lnet/flixster/android/data/ApiBuilder;->rightDownload(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v8

    .line 571
    .local v8, apiUrl:Ljava/lang/String;
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 572
    .local v23, parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    const/16 v26, 0x0

    .line 574
    .local v26, response:Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, v23

    invoke-static {v8, v0}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v26

    .line 580
    invoke-static/range {v26 .. v26}, Lnet/flixster/android/data/DaoException;->hasError(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 581
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$errorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static/range {v26 .. v26}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 628
    :goto_0
    return-void

    .line 575
    :catch_0
    move-exception v19

    .line 576
    .local v19, ie:Ljava/io/IOException;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$errorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static/range {v19 .. v19}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 586
    .end local v19           #ie:Ljava/io/IOException;
    :cond_0
    :try_start_1
    new-instance v22, Lorg/json/JSONObject;

    move-object/from16 v0, v22

    move-object/from16 v1, v26

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 587
    .local v22, movieAvailObj:Lorg/json/JSONObject;
    const-string v2, "assets"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 588
    .local v9, assetsObj:Lorg/json/JSONObject;
    const-string v2, "lockerRightParams"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v21

    .line 589
    .local v21, lockerRightsObj:Lorg/json/JSONObject;
    const-string v2, "downloadsRemaining"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v16

    .line 590
    .local v16, downloadsRemaining:I
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    move/from16 v0, v16

    invoke-virtual {v2, v0}, Lnet/flixster/android/model/LockerRight;->setDownloadsRemain(I)V

    .line 591
    if-lez v16, :cond_3

    .line 592
    const-string v2, "downloadLow"

    invoke-virtual {v9, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 593
    .local v11, downloadAssetObj:Lorg/json/JSONObject;
    if-eqz v11, :cond_2

    .line 594
    const-string v2, "fileLocation"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 595
    .local v18, fileLocation:Ljava/lang/String;
    new-instance v15, Lcom/flixster/android/drm/DownloadRight;

    move-object/from16 v0, p0

    iget-wide v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$rightId:J

    invoke-direct {v15, v2, v3}, Lcom/flixster/android/drm/DownloadRight;-><init>(J)V

    .line 596
    .local v15, downloadRight:Lcom/flixster/android/drm/DownloadRight;
    move-object/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/flixster/android/drm/DownloadRight;->setDownloadUri(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 597
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lnet/flixster/android/model/LockerRight;->setDownloadUri(Ljava/lang/String;)V

    .line 598
    const-string v2, "closedCaptionFileLocation"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 599
    .local v10, captionsFileLocation:Ljava/lang/String;
    invoke-virtual {v15, v10}, Lcom/flixster/android/drm/DownloadRight;->setCaptionsUri(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 600
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v2, v10}, Lnet/flixster/android/model/LockerRight;->setDownloadCaptionsUri(Ljava/lang/String;)V

    .line 601
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/LockerRight;->setDownloadAssetSize(I)V

    .line 602
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v2}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 603
    const-string v2, "drmServerUrl"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 604
    .local v13, downloadLicenseProxy:Ljava/lang/String;
    const-string v2, "sonicQueueId"

    invoke-virtual {v11, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v14

    .line 605
    .local v14, downloadQueueId:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "destinationUniqueId"

    invoke-virtual {v11, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "|"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 606
    const-string v3, "deviceTypeId"

    invoke-virtual {v11, v3}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 605
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 607
    .local v12, downloadDeviceId:Ljava/lang/String;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "start download: licenseProxy "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", queueId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 608
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", deviceId "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 607
    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 609
    invoke-virtual {v15, v13}, Lcom/flixster/android/drm/DownloadRight;->setLicenseProxy(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 610
    invoke-virtual {v15, v14}, Lcom/flixster/android/drm/DownloadRight;->setQueueId(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 611
    invoke-virtual {v15, v12}, Lcom/flixster/android/drm/DownloadRight;->setDeviceId(Ljava/lang/String;)Lcom/flixster/android/drm/DownloadRight;

    .line 613
    .end local v12           #downloadDeviceId:Ljava/lang/String;
    .end local v13           #downloadLicenseProxy:Ljava/lang/String;
    .end local v14           #downloadQueueId:Ljava/lang/String;
    :cond_1
    invoke-virtual {v15}, Lcom/flixster/android/drm/DownloadRight;->save()V

    .line 616
    .end local v10           #captionsFileLocation:Ljava/lang/String;
    .end local v15           #downloadRight:Lcom/flixster/android/drm/DownloadRight;
    .end local v18           #fileLocation:Ljava/lang/String;
    :cond_2
    const-string v2, "playPosition"

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    mul-int/lit16 v0, v2, 0x3e8

    move/from16 v24, v0

    .line 617
    .local v24, playPosition:I
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "restartTime"

    .line 618
    const/4 v4, 0x0

    .line 617
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v25

    .line 619
    .local v25, prefs:Landroid/content/SharedPreferences;
    invoke-interface/range {v25 .. v25}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v17

    .line 620
    .local v17, editor:Landroid/content/SharedPreferences$Editor;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    iget-wide v2, v2, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    move/from16 v1, v24

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 621
    invoke-interface/range {v17 .. v17}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 623
    .end local v11           #downloadAssetObj:Lorg/json/JSONObject;
    .end local v17           #editor:Landroid/content/SharedPreferences$Editor;
    .end local v24           #playPosition:I
    .end local v25           #prefs:Landroid/content/SharedPreferences;
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$successHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lnet/flixster/android/data/ProfileDao$9;->val$right:Lnet/flixster/android/model/LockerRight;

    invoke-static {v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 624
    .end local v9           #assetsObj:Lorg/json/JSONObject;
    .end local v16           #downloadsRemaining:I
    .end local v21           #lockerRightsObj:Lorg/json/JSONObject;
    .end local v22           #movieAvailObj:Lorg/json/JSONObject;
    :catch_1
    move-exception v20

    .line 625
    .local v20, je:Lorg/json/JSONException;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/data/ProfileDao$9;->val$errorHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static/range {v20 .. v20}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v5

    invoke-static {v3, v4, v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0
.end method
