.class public Lnet/flixster/android/data/ProfileDao;
.super Ljava/lang/Object;
.source "ProfileDao.java"


# static fields
.field public static final HTTPS_STREAM_CREATE:Z = true

.field private static currentUser:Lnet/flixster/android/model/User;

.field private static usersMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lnet/flixster/android/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    sput-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    .line 54
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0()Lnet/flixster/android/model/User;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 526
    invoke-static {p0, p1}, Lnet/flixster/android/data/ProfileDao;->toastLockerRightInsertion(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V

    return-void
.end method

.method public static ackLicense(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "customData"

    .prologue
    .line 785
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$15;

    invoke-direct {v1, p2, p1}, Lnet/flixster/android/data/ProfileDao$15;-><init>(Ljava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 796
    return-void
.end method

.method public static canDownload(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V
    .locals 9
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "right"

    .prologue
    .line 557
    iget-wide v1, p2, Lnet/flixster/android/model/LockerRight;->rightId:J

    .line 559
    .local v1, rightId:J
    new-instance v6, Lcom/flixster/android/drm/DownloadRight;

    invoke-direct {v6, v1, v2}, Lcom/flixster/android/drm/DownloadRight;-><init>(J)V

    .line 560
    .local v6, downloadRight:Lcom/flixster/android/drm/DownloadRight;
    invoke-virtual {p2}, Lnet/flixster/android/model/LockerRight;->getDownloadUri()Ljava/lang/String;

    move-result-object v7

    .local v7, downloadUri:Ljava/lang/String;
    if-nez v7, :cond_0

    const-string v0, ""

    invoke-virtual {v6}, Lcom/flixster/android/drm/DownloadRight;->getDownloadUri()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 561
    :cond_0
    invoke-virtual {p2, v7}, Lnet/flixster/android/model/LockerRight;->setDownloadUri(Ljava/lang/String;)V

    .line 562
    invoke-virtual {v6}, Lcom/flixster/android/drm/DownloadRight;->getCaptionsUri()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lnet/flixster/android/model/LockerRight;->setDownloadCaptionsUri(Ljava/lang/String;)V

    .line 563
    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v3, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 630
    :goto_0
    return-void

    .line 567
    :cond_1
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v8

    new-instance v0, Lnet/flixster/android/data/ProfileDao$9;

    move-object v3, p2

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/data/ProfileDao$9;-><init>(JLnet/flixster/android/model/LockerRight;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v8, v0}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static clearBitmapCache()V
    .locals 6

    .prologue
    .line 1336
    :try_start_0
    sget-object v4, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    invoke-static {v4}, Lnet/flixster/android/data/ProfileDao;->clearUserBitmaps(Lnet/flixster/android/model/User;)V

    .line 1337
    sget-object v4, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    if-eqz v4, :cond_0

    sget-object v4, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1338
    sget-object v4, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    .line 1339
    .local v3, users:Ljava/util/Collection;,"Ljava/util/Collection<Lnet/flixster/android/model/User;>;"
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, userIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lnet/flixster/android/model/User;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1347
    .end local v2           #userIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lnet/flixster/android/model/User;>;"
    :cond_0
    :goto_1
    return-void

    .line 1340
    .restart local v2       #userIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lnet/flixster/android/model/User;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/User;

    .line 1341
    .local v1, user:Lnet/flixster/android/model/User;
    invoke-static {v1}, Lnet/flixster/android/data/ProfileDao;->clearUserBitmaps(Lnet/flixster/android/model/User;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1344
    .end local v1           #user:Lnet/flixster/android/model/User;
    .end local v2           #userIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lnet/flixster/android/model/User;>;"
    :catch_0
    move-exception v0

    .line 1345
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "FlxMain"

    const-string v5, "problem clearing bitmap cache"

    invoke-static {v4, v5, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private static clearUserBitmaps(Lnet/flixster/android/model/User;)V
    .locals 3
    .parameter "user"

    .prologue
    .line 1351
    if-eqz p0, :cond_0

    .line 1352
    iget-object v2, p0, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1353
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    iget-object v2, p0, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v1, v2, :cond_1

    .line 1359
    .end local v1           #i:I
    :cond_0
    return-void

    .line 1354
    .restart local v1       #i:I
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/User;

    .line 1355
    .local v0, friend:Lnet/flixster/android/model/User;
    const/4 v2, 0x0

    iput-object v2, v0, Lnet/flixster/android/model/User;->bitmap:Landroid/graphics/Bitmap;

    .line 1353
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static createNewUser()V
    .locals 1

    .prologue
    .line 201
    new-instance v0, Lnet/flixster/android/model/User;

    invoke-direct {v0}, Lnet/flixster/android/model/User;-><init>()V

    sput-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    .line 202
    return-void
.end method

.method public static endDownload(Landroid/os/Handler;Landroid/os/Handler;JLjava/lang/String;)V
    .locals 7
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "rightId"
    .parameter "sonicQueueId"

    .prologue
    .line 634
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v6

    new-instance v0, Lnet/flixster/android/data/ProfileDao$10;

    move-wide v1, p2

    move-object v3, p4

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/data/ProfileDao$10;-><init>(JLjava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v6, v0}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 648
    return-void
.end method

.method public static declared-synchronized fetchUser()Lnet/flixster/android/model/User;
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    .line 166
    const-class v8, Lnet/flixster/android/data/ProfileDao;

    monitor-enter v8

    :try_start_0
    const-string v9, "FlxMain"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "ProfileDao.fetchUser currentUser="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v11, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v9, :cond_0

    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v9, v9, Lnet/flixster/android/model/User;->id:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v9, :cond_3

    .line 170
    :cond_0
    :try_start_1
    new-instance v9, Ljava/net/URL;

    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->viewer()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->isConnected()Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v7, 0x0

    :cond_1
    const/4 v10, 0x1

    invoke-static {v9, v7, v10}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v4

    .line 178
    .local v4, response:Ljava/lang/String;
    :try_start_2
    new-instance v6, Lorg/json/JSONObject;

    invoke-direct {v6, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 179
    .local v6, userObj:Lorg/json/JSONObject;
    const-string v7, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ProfileDao.fetchUser userObj:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    const-string v7, "error"

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    .local v0, error:Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 182
    const-string v7, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ProfileDao.fetchUser error:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v7, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v7, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    invoke-static {v7, v0}, Lnet/flixster/android/data/DaoException;->create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v7

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 189
    .end local v0           #error:Ljava/lang/String;
    .end local v6           #userObj:Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 190
    .local v2, je:Lorg/json/JSONException;
    :try_start_3
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v7

    throw v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 166
    .end local v2           #je:Lorg/json/JSONException;
    .end local v4           #response:Ljava/lang/String;
    .local v3, mue:Ljava/net/MalformedURLException;
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7

    .line 171
    .end local v3           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v3

    .line 172
    .restart local v3       #mue:Ljava/net/MalformedURLException;
    :try_start_4
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 173
    .end local v3           #mue:Ljava/net/MalformedURLException;
    :catch_2
    move-exception v1

    .line 174
    .local v1, ie:Ljava/io/IOException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v7

    throw v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 185
    .end local v1           #ie:Ljava/io/IOException;
    .restart local v0       #error:Ljava/lang/String;
    .restart local v4       #response:Ljava/lang/String;
    .restart local v6       #userObj:Lorg/json/JSONObject;
    :cond_2
    :try_start_5
    new-instance v5, Lnet/flixster/android/model/User;

    invoke-direct {v5}, Lnet/flixster/android/model/User;-><init>()V

    .line 186
    .local v5, user:Lnet/flixster/android/model/User;
    invoke-virtual {v5, v6}, Lnet/flixster/android/model/User;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/User;

    .line 187
    sput-object v5, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_0

    .line 193
    :cond_3
    :try_start_6
    sget-object v7, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    monitor-exit v8

    return-object v7
.end method

.method public static fetchUser(Ljava/lang/String;)Lnet/flixster/android/model/User;
    .locals 11
    .parameter "userId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 113
    sget-object v8, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    invoke-virtual {v8, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/User;

    .line 114
    .local v5, user:Lnet/flixster/android/model/User;
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ProfileDao.fetchUser id:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " user:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    if-nez v5, :cond_1

    .line 118
    :try_start_0
    new-instance v8, Ljava/net/URL;

    invoke-static {p0}, Lnet/flixster/android/data/ApiBuilder;->user(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v4

    .line 126
    .local v4, response:Ljava/lang/String;
    :try_start_1
    new-instance v7, Lorg/json/JSONObject;

    invoke-direct {v7, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 127
    .local v7, userObject:Lorg/json/JSONObject;
    const-string v8, "error"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 128
    .local v0, error:Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 129
    const-string v8, "FlxMain"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "ProfileDao.getUser error:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    sget-object v8, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    invoke-static {v8, v0}, Lnet/flixster/android/data/DaoException;->create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v8

    throw v8
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 136
    .end local v0           #error:Ljava/lang/String;
    .end local v7           #userObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 137
    .local v2, je:Lorg/json/JSONException;
    :goto_0
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v8

    throw v8

    .line 119
    .end local v2           #je:Lorg/json/JSONException;
    .end local v4           #response:Ljava/lang/String;
    :catch_1
    move-exception v3

    .line 120
    .local v3, mue:Ljava/net/MalformedURLException;
    new-instance v8, Ljava/lang/RuntimeException;

    invoke-direct {v8, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v8

    .line 121
    .end local v3           #mue:Ljava/net/MalformedURLException;
    :catch_2
    move-exception v1

    .line 122
    .local v1, ie:Ljava/io/IOException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v8

    throw v8

    .line 132
    .end local v1           #ie:Ljava/io/IOException;
    .restart local v0       #error:Ljava/lang/String;
    .restart local v4       #response:Ljava/lang/String;
    .restart local v7       #userObject:Lorg/json/JSONObject;
    :cond_0
    :try_start_2
    new-instance v6, Lnet/flixster/android/model/User;

    invoke-direct {v6}, Lnet/flixster/android/model/User;-><init>()V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 133
    .end local v5           #user:Lnet/flixster/android/model/User;
    .local v6, user:Lnet/flixster/android/model/User;
    :try_start_3
    invoke-virtual {v6, v7}, Lnet/flixster/android/model/User;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/User;

    .line 134
    sget-object v8, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    invoke-virtual {v8, p0, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    move-object v5, v6

    .line 140
    .end local v0           #error:Ljava/lang/String;
    .end local v4           #response:Ljava/lang/String;
    .end local v6           #user:Lnet/flixster/android/model/User;
    .end local v7           #userObject:Lorg/json/JSONObject;
    .restart local v5       #user:Lnet/flixster/android/model/User;
    :cond_1
    return-object v5

    .line 136
    .end local v5           #user:Lnet/flixster/android/model/User;
    .restart local v0       #error:Ljava/lang/String;
    .restart local v4       #response:Ljava/lang/String;
    .restart local v6       #user:Lnet/flixster/android/model/User;
    .restart local v7       #userObject:Lorg/json/JSONObject;
    :catch_3
    move-exception v2

    move-object v5, v6

    .end local v6           #user:Lnet/flixster/android/model/User;
    .restart local v5       #user:Lnet/flixster/android/model/User;
    goto :goto_0
.end method

.method public static fetchUser(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    const/4 v1, 0x0

    .line 148
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v0, :cond_0

    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 149
    const/4 v0, 0x0

    invoke-static {v1, v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 163
    :goto_0
    return-void

    .line 152
    :cond_0
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$1;

    invoke-direct {v1, p0, p1}, Lnet/flixster/android/data/ProfileDao$1;-><init>(Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static getFacebookFriends(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 205
    const-string v0, "FlxMain"

    const-string v1, "ProfileDao.getFacebookFriends"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v0, :cond_0

    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->friendsIds:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 207
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 245
    :goto_0
    return-void

    .line 208
    :cond_0
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v0, :cond_1

    .line 209
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$2;

    invoke-direct {v1, p1, p0}, Lnet/flixster/android/data/ProfileDao$2;-><init>(Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 243
    :cond_1
    const-string v0, "FlxMain"

    const-string v1, "ProfileDao.getFacebookFriends illegal state user null"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getFriendRatedReviews(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 1117
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v3, :cond_1

    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->friendRatedReviews:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 1118
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v3, Lnet/flixster/android/model/User;->friendRatedReviews:Ljava/util/List;

    .line 1130
    :cond_0
    :goto_0
    return-object v0

    .line 1120
    :cond_1
    invoke-static {p0}, Lnet/flixster/android/data/ProfileDao;->getFriendReviews(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 1121
    .local v2, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1122
    .local v0, ratedReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1127
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v3, :cond_0

    .line 1128
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iput-object v0, v3, Lnet/flixster/android/model/User;->friendRatedReviews:Ljava/util/List;

    goto :goto_0

    .line 1122
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Review;

    .line 1123
    .local v1, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v1}, Lnet/flixster/android/model/Review;->isWantToSee()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v1}, Lnet/flixster/android/model/Review;->isNotInterested()Z

    move-result v4

    if-nez v4, :cond_2

    .line 1124
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static getFriendReviews(Ljava/lang/String;)Ljava/util/List;
    .locals 12
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 1134
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v9, :cond_1

    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v9, v9, Lnet/flixster/android/model/User;->friendReviews:Ljava/util/List;

    if-eqz v9, :cond_1

    .line 1135
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v8, v9, Lnet/flixster/android/model/User;->friendReviews:Ljava/util/List;

    .line 1161
    :cond_0
    :goto_0
    return-object v8

    .line 1140
    :cond_1
    :try_start_0
    new-instance v9, Ljava/net/URL;

    invoke-static {p0}, Lnet/flixster/android/data/ApiBuilder;->friendReviews(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v6

    .line 1148
    .local v6, response:Ljava/lang/String;
    :try_start_1
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, v6}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1150
    .local v4, jsonReviews:Lorg/json/JSONArray;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1151
    .local v8, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    const/4 v1, 0x0

    .local v1, i:I
    :goto_1
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lt v1, v9, :cond_2

    .line 1158
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v9, :cond_0

    .line 1159
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iput-object v8, v9, Lnet/flixster/android/model/User;->friendReviews:Ljava/util/List;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 1162
    .end local v1           #i:I
    .end local v4           #jsonReviews:Lorg/json/JSONArray;
    .end local v8           #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :catch_0
    move-exception v3

    .line 1163
    .local v3, je:Lorg/json/JSONException;
    invoke-static {v3}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9

    .line 1141
    .end local v3           #je:Lorg/json/JSONException;
    .end local v6           #response:Ljava/lang/String;
    :catch_1
    move-exception v5

    .line 1142
    .local v5, mue:Ljava/net/MalformedURLException;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 1143
    .end local v5           #mue:Ljava/net/MalformedURLException;
    :catch_2
    move-exception v2

    .line 1144
    .local v2, ie:Ljava/io/IOException;
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9

    .line 1152
    .end local v2           #ie:Ljava/io/IOException;
    .restart local v1       #i:I
    .restart local v4       #jsonReviews:Lorg/json/JSONArray;
    .restart local v6       #response:Ljava/lang/String;
    .restart local v8       #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :cond_2
    :try_start_2
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 1153
    .local v0, activityObject:Lorg/json/JSONObject;
    const-string v9, "type"

    invoke-virtual {v0, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-string v9, "rating"

    const-string v10, "type"

    invoke-virtual {v0, v10}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1154
    invoke-static {v0}, Lnet/flixster/android/data/ProfileDao;->parseReview(Lorg/json/JSONObject;)Lnet/flixster/android/model/Review;

    move-result-object v7

    .line 1155
    .local v7, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 1151
    .end local v7           #review:Lnet/flixster/android/model/Review;
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static getFriendWantToSeeReviews(Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .parameter "userId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 1070
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v3, :cond_1

    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v3, v3, Lnet/flixster/android/model/User;->friendWantToSeeReviews:Ljava/util/List;

    if-eqz v3, :cond_1

    .line 1071
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v2, v3, Lnet/flixster/android/model/User;->friendWantToSeeReviews:Ljava/util/List;

    .line 1083
    :cond_0
    :goto_0
    return-object v2

    .line 1073
    :cond_1
    invoke-static {p0}, Lnet/flixster/android/data/ProfileDao;->getFriendReviews(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 1074
    .local v1, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1075
    .local v2, wantToSeeReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_3

    .line 1080
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v3, :cond_0

    .line 1081
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iput-object v2, v3, Lnet/flixster/android/model/User;->friendWantToSeeReviews:Ljava/util/List;

    goto :goto_0

    .line 1075
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Review;

    .line 1076
    .local v0, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v0}, Lnet/flixster/android/model/Review;->isWantToSee()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1077
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public static getFriends(Ljava/lang/String;I)Ljava/util/List;
    .locals 12
    .parameter "userId"
    .parameter "limit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/User;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 354
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v9, :cond_1

    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v9, v9, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    if-eqz v9, :cond_1

    .line 355
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v1, v9, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    .line 378
    :cond_0
    :goto_0
    return-object v1

    .line 360
    :cond_1
    :try_start_0
    new-instance v9, Ljava/net/URL;

    invoke-static {p0, p1}, Lnet/flixster/android/data/ApiBuilder;->friends(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v7

    .line 368
    .local v7, response:Ljava/lang/String;
    :try_start_1
    new-instance v5, Lorg/json/JSONArray;

    invoke-direct {v5, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 369
    .local v5, jsonUsers:Lorg/json/JSONArray;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 370
    .local v1, friends:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/User;>;"
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-lt v2, v9, :cond_2

    .line 375
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v9, :cond_0

    .line 376
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iput-object v1, v9, Lnet/flixster/android/model/User;->friends:Ljava/util/List;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 379
    .end local v1           #friends:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/User;>;"
    .end local v2           #i:I
    .end local v5           #jsonUsers:Lorg/json/JSONArray;
    :catch_0
    move-exception v4

    .line 380
    .local v4, je:Lorg/json/JSONException;
    invoke-static {v4}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9

    .line 361
    .end local v4           #je:Lorg/json/JSONException;
    .end local v7           #response:Ljava/lang/String;
    :catch_1
    move-exception v6

    .line 362
    .local v6, mue:Ljava/net/MalformedURLException;
    new-instance v9, Ljava/lang/RuntimeException;

    invoke-direct {v9, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v9

    .line 363
    .end local v6           #mue:Ljava/net/MalformedURLException;
    :catch_2
    move-exception v3

    .line 364
    .local v3, ie:Ljava/io/IOException;
    invoke-static {v3}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9

    .line 371
    .end local v3           #ie:Ljava/io/IOException;
    .restart local v1       #friends:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/User;>;"
    .restart local v2       #i:I
    .restart local v5       #jsonUsers:Lorg/json/JSONArray;
    .restart local v7       #response:Ljava/lang/String;
    :cond_2
    :try_start_2
    invoke-virtual {v5, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v8

    .line 372
    .local v8, userObject:Lorg/json/JSONObject;
    invoke-static {v8}, Lnet/flixster/android/data/ProfileDao;->parseUser(Lorg/json/JSONObject;)Lnet/flixster/android/model/User;

    move-result-object v0

    .line 373
    .local v0, friend:Lnet/flixster/android/model/User;
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 370
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static getFriends(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 332
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v0, :cond_0

    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->friends:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 335
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 339
    :cond_2
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$5;

    invoke-direct {v1, p0, p1}, Lnet/flixster/android/data/ProfileDao$5;-><init>(Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static getFriendsActivity(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 1088
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v0, :cond_0

    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1114
    :cond_0
    :goto_0
    return-void

    .line 1090
    :cond_1
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->friendsActivity:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1091
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 1094
    :cond_2
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$22;

    invoke-direct {v1, p0, p1}, Lnet/flixster/android/data/ProfileDao$22;-><init>(Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static getQuickRateMovies(Landroid/os/Handler;Landroid/os/Handler;Z)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "isWts"

    .prologue
    .line 902
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$19;

    invoke-direct {v1, p2, p1, p0}, Lnet/flixster/android/data/ProfileDao$19;-><init>(ZLandroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 939
    return-void
.end method

.method public static getUser()Lnet/flixster/android/model/User;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    return-object v0
.end method

.method public static getUserLockerRights(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 385
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v0, :cond_0

    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    invoke-virtual {v0}, Lnet/flixster/android/model/User;->isLockerRightsFetched()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 388
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 391
    :cond_2
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$6;

    invoke-direct {v1, p1, p0}, Lnet/flixster/android/data/ProfileDao$6;-><init>(Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static getUserMovieReview(Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Review;
    .locals 8
    .parameter "flixsterId"
    .parameter "movieId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 315
    :try_start_0
    new-instance v5, Ljava/net/URL;

    invoke-static {p0, p1}, Lnet/flixster/android/data/ApiBuilder;->userMovieReview(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 316
    .local v5, url:Ljava/net/URL;
    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 324
    .local v4, response:Ljava/lang/String;
    :try_start_1
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 325
    .local v3, ratingJSON:Lorg/json/JSONObject;
    invoke-static {v3}, Lnet/flixster/android/data/ProfileDao;->parseReview(Lorg/json/JSONObject;)Lnet/flixster/android/model/Review;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v6

    return-object v6

    .line 317
    .end local v3           #ratingJSON:Lorg/json/JSONObject;
    .end local v4           #response:Ljava/lang/String;
    .end local v5           #url:Ljava/net/URL;
    :catch_0
    move-exception v2

    .line 318
    .local v2, mue:Ljava/net/MalformedURLException;
    new-instance v6, Ljava/lang/RuntimeException;

    invoke-direct {v6, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v6

    .line 319
    .end local v2           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 320
    .local v0, ie:Ljava/io/IOException;
    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6

    .line 326
    .end local v0           #ie:Ljava/io/IOException;
    .restart local v4       #response:Ljava/lang/String;
    .restart local v5       #url:Ljava/net/URL;
    :catch_2
    move-exception v1

    .line 327
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v6

    throw v6
.end method

.method public static getUserMovieReview(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "movieId"

    .prologue
    .line 298
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$4;

    invoke-direct {v1, p2, p0, p1}, Lnet/flixster/android/data/ProfileDao$4;-><init>(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 310
    return-void
.end method

.method public static getUserRatedReviews(Ljava/lang/String;I)Ljava/util/List;
    .locals 6
    .parameter "userId"
    .parameter "limit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 966
    sget-object v4, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    invoke-virtual {v4, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnet/flixster/android/model/User;

    .line 967
    .local v3, user:Lnet/flixster/android/model/User;
    if-nez v3, :cond_0

    .line 968
    sget-object v4, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v4, :cond_1

    sget-object v4, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v4, v4, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 969
    sget-object v3, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    .line 974
    :cond_0
    :goto_0
    iget-object v4, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 975
    iget-object v0, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    .line 985
    :goto_1
    return-object v0

    .line 971
    :cond_1
    new-instance v3, Lnet/flixster/android/model/User;

    .end local v3           #user:Lnet/flixster/android/model/User;
    invoke-direct {v3}, Lnet/flixster/android/model/User;-><init>()V

    .restart local v3       #user:Lnet/flixster/android/model/User;
    goto :goto_0

    .line 977
    :cond_2
    invoke-static {p0, p1}, Lnet/flixster/android/data/ProfileDao;->getUserReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v2

    .line 978
    .local v2, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 979
    .local v0, ratedReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 984
    iput-object v0, v3, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    goto :goto_1

    .line 979
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Review;

    .line 980
    .local v1, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v1}, Lnet/flixster/android/model/Review;->isWantToSee()Z

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {v1}, Lnet/flixster/android/model/Review;->isNotInterested()Z

    move-result v5

    if-nez v5, :cond_3

    .line 981
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static getUserRatedReviews(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 942
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v0, :cond_0

    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 963
    :cond_0
    :goto_0
    return-void

    .line 944
    :cond_1
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->ratedReviews:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 945
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 949
    :cond_2
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$20;

    invoke-direct {v1, p0, p1}, Lnet/flixster/android/data/ProfileDao$20;-><init>(Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static getUserReviews(Ljava/lang/String;I)Ljava/util/List;
    .locals 13
    .parameter "userId"
    .parameter "limit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 1036
    sget-object v10, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    invoke-virtual {v10, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/model/User;

    .line 1037
    .local v9, user:Lnet/flixster/android/model/User;
    if-nez v9, :cond_0

    .line 1038
    sget-object v10, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v10, :cond_1

    sget-object v10, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v10, v10, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-virtual {p0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 1039
    sget-object v9, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    .line 1047
    :cond_0
    :goto_0
    :try_start_0
    new-instance v10, Ljava/net/URL;

    invoke-static {p0, p1}, Lnet/flixster/android/data/ApiBuilder;->userReviews(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v10, v11, v12}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v5

    .line 1055
    .local v5, response:Ljava/lang/String;
    :try_start_1
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v5}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1056
    .local v3, jsonReviews:Lorg/json/JSONArray;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1057
    .local v8, reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-lt v0, v10, :cond_2

    .line 1062
    iput-object v8, v9, Lnet/flixster/android/model/User;->reviews:Ljava/util/List;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1063
    return-object v8

    .line 1041
    .end local v0           #i:I
    .end local v3           #jsonReviews:Lorg/json/JSONArray;
    .end local v5           #response:Ljava/lang/String;
    .end local v8           #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :cond_1
    new-instance v9, Lnet/flixster/android/model/User;

    .end local v9           #user:Lnet/flixster/android/model/User;
    invoke-direct {v9}, Lnet/flixster/android/model/User;-><init>()V

    .restart local v9       #user:Lnet/flixster/android/model/User;
    goto :goto_0

    .line 1048
    :catch_0
    move-exception v4

    .line 1049
    .local v4, mue:Ljava/net/MalformedURLException;
    new-instance v10, Ljava/lang/RuntimeException;

    invoke-direct {v10, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v10

    .line 1050
    .end local v4           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v1

    .line 1051
    .local v1, ie:Ljava/io/IOException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v10

    throw v10

    .line 1058
    .end local v1           #ie:Ljava/io/IOException;
    .restart local v0       #i:I
    .restart local v3       #jsonReviews:Lorg/json/JSONArray;
    .restart local v5       #response:Ljava/lang/String;
    .restart local v8       #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :cond_2
    :try_start_2
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 1059
    .local v7, reviewObject:Lorg/json/JSONObject;
    invoke-static {v7}, Lnet/flixster/android/data/ProfileDao;->parseReview(Lorg/json/JSONObject;)Lnet/flixster/android/model/Review;

    move-result-object v6

    .line 1060
    .local v6, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1057
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1064
    .end local v0           #i:I
    .end local v3           #jsonReviews:Lorg/json/JSONArray;
    .end local v6           #review:Lnet/flixster/android/model/Review;
    .end local v7           #reviewObject:Lorg/json/JSONObject;
    .end local v8           #reviews:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Review;>;"
    :catch_2
    move-exception v2

    .line 1065
    .local v2, je:Lorg/json/JSONException;
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v10

    throw v10
.end method

.method public static getWantToSeeReviews(Ljava/lang/String;I)Ljava/util/List;
    .locals 6
    .parameter "userId"
    .parameter "limit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 1013
    sget-object v4, Lnet/flixster/android/data/ProfileDao;->usersMap:Ljava/util/HashMap;

    invoke-virtual {v4, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/model/User;

    .line 1014
    .local v2, user:Lnet/flixster/android/model/User;
    if-nez v2, :cond_0

    .line 1015
    sget-object v4, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v4, :cond_1

    sget-object v4, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v4, v4, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1016
    sget-object v2, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    .line 1021
    :cond_0
    :goto_0
    iget-object v4, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 1022
    iget-object v3, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    .line 1032
    :goto_1
    return-object v3

    .line 1018
    :cond_1
    new-instance v2, Lnet/flixster/android/model/User;

    .end local v2           #user:Lnet/flixster/android/model/User;
    invoke-direct {v2}, Lnet/flixster/android/model/User;-><init>()V

    .restart local v2       #user:Lnet/flixster/android/model/User;
    goto :goto_0

    .line 1024
    :cond_2
    invoke-static {p0, p1}, Lnet/flixster/android/data/ProfileDao;->getUserReviews(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    .line 1025
    .local v1, reviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1026
    .local v3, wantToSeeReviews:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Review;>;"
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_4

    .line 1031
    iput-object v3, v2, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    goto :goto_1

    .line 1026
    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Review;

    .line 1027
    .local v0, review:Lnet/flixster/android/model/Review;
    invoke-virtual {v0}, Lnet/flixster/android/model/Review;->isWantToSee()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1028
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static getWantToSeeReviews(Landroid/os/Handler;Landroid/os/Handler;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"

    .prologue
    .line 989
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    if-eqz v0, :cond_0

    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 1010
    :cond_0
    :goto_0
    return-void

    .line 991
    :cond_1
    sget-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v0, Lnet/flixster/android/model/User;->wantToSeeReviews:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 992
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    .line 996
    :cond_2
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$21;

    invoke-direct {v1, p0, p1}, Lnet/flixster/android/data/ProfileDao$21;-><init>(Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static insertLockerRight(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "purchaseType"

    .prologue
    .line 486
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$7;

    invoke-direct {v1, p2}, Lnet/flixster/android/data/ProfileDao$7;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 524
    return-void
.end method

.method public static loginUser(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/User;
    .locals 11
    .parameter "username"
    .parameter "password"
    .parameter "platform"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 62
    const-string v9, "FlxMain"

    const-string v10, "ProfileDao.loginUser"

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const/4 v7, 0x0

    .line 64
    .local v7, user:Lnet/flixster/android/model/User;
    const-string v9, "FLX"

    invoke-virtual {v9, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 65
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->tokens()Ljava/lang/String;

    move-result-object v6

    .line 66
    .local v6, url:Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 67
    .local v4, parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "login"

    invoke-direct {v9, v10, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string v10, "password"

    invoke-direct {v9, v10, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    const/4 v5, 0x0

    .line 73
    .local v5, response:Ljava/lang/String;
    :try_start_0
    invoke-static {v6, v4}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 80
    const/4 v8, 0x0

    .line 82
    .local v8, userObject:Lorg/json/JSONObject;
    :try_start_1
    new-instance v8, Lorg/json/JSONObject;

    .end local v8           #userObject:Lorg/json/JSONObject;
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 89
    .restart local v8       #userObject:Lorg/json/JSONObject;
    const-string v9, "error"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 90
    const-string v9, "error"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, error:Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-lez v9, :cond_1

    .line 92
    sget-object v9, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    invoke-static {v9, v1}, Lnet/flixster/android/data/DaoException;->create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9

    .line 74
    .end local v1           #error:Ljava/lang/String;
    .end local v8           #userObject:Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 75
    .local v2, ie:Ljava/io/IOException;
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v0

    .line 76
    .local v0, de:Lnet/flixster/android/data/DaoException;
    const-string v9, "FlxMain"

    const-string v10, "ProfileDao.loginUser DaoException"

    invoke-static {v9, v10, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    throw v0

    .line 83
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    .end local v2           #ie:Ljava/io/IOException;
    :catch_1
    move-exception v3

    .line 84
    .local v3, je:Lorg/json/JSONException;
    invoke-static {v3}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v0

    .line 85
    .restart local v0       #de:Lnet/flixster/android/data/DaoException;
    const-string v9, "FlxMain"

    const-string v10, "ProfileDao.loginUser DaoException"

    invoke-static {v9, v10, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 86
    throw v0

    .line 94
    .end local v0           #de:Lnet/flixster/android/data/DaoException;
    .end local v3           #je:Lorg/json/JSONException;
    .restart local v8       #userObject:Lorg/json/JSONObject;
    :cond_0
    const-string v9, "token"

    invoke-virtual {v8, v9}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 95
    invoke-static {}, Lnet/flixster/android/data/ProfileDao;->resetUser()V

    .line 96
    new-instance v7, Lnet/flixster/android/model/User;

    .end local v7           #user:Lnet/flixster/android/model/User;
    invoke-direct {v7}, Lnet/flixster/android/model/User;-><init>()V

    .line 97
    .restart local v7       #user:Lnet/flixster/android/model/User;
    invoke-virtual {v7, v8}, Lnet/flixster/android/model/User;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/User;

    .line 98
    const-string v9, "FLX"

    iput-object v9, v7, Lnet/flixster/android/model/User;->platform:Ljava/lang/String;

    .line 99
    const-string v9, "FLX"

    invoke-static {v9}, Lnet/flixster/android/FlixsterApplication;->setPlatform(Ljava/lang/String;)V

    .line 100
    iget-object v9, v7, Lnet/flixster/android/model/User;->userName:Ljava/lang/String;

    invoke-static {v9}, Lnet/flixster/android/FlixsterApplication;->setFlixsterUsername(Ljava/lang/String;)V

    .line 101
    iget-object v9, v7, Lnet/flixster/android/model/User;->token:Ljava/lang/String;

    invoke-static {v9}, Lnet/flixster/android/FlixsterApplication;->setFlixsterSessionKey(Ljava/lang/String;)V

    .line 102
    iget-object v9, v7, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    invoke-static {v9}, Lnet/flixster/android/FlixsterApplication;->setFlixsterId(Ljava/lang/String;)V

    .line 109
    .end local v4           #parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .end local v5           #response:Ljava/lang/String;
    .end local v6           #url:Ljava/lang/String;
    .end local v8           #userObject:Lorg/json/JSONObject;
    :cond_1
    return-object v7

    .line 105
    .restart local v4       #parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .restart local v5       #response:Ljava/lang/String;
    .restart local v6       #url:Ljava/lang/String;
    .restart local v8       #userObject:Lorg/json/JSONObject;
    :cond_2
    const-string v1, "Incorrect username/password combination. Please try again."

    .line 106
    .restart local v1       #error:Ljava/lang/String;
    sget-object v9, Lnet/flixster/android/data/DaoException$Type;->USER_ACCT:Lnet/flixster/android/data/DaoException$Type;

    invoke-static {v9, v1}, Lnet/flixster/android/data/DaoException;->create(Lnet/flixster/android/data/DaoException$Type;Ljava/lang/String;)Lnet/flixster/android/data/DaoException;

    move-result-object v9

    throw v9
.end method

.method private static parseReview(Lorg/json/JSONObject;)Lnet/flixster/android/model/Review;
    .locals 33
    .parameter "reviewObject"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1191
    new-instance v21, Lnet/flixster/android/model/Review;

    invoke-direct/range {v21 .. v21}, Lnet/flixster/android/model/Review;-><init>()V

    .line 1192
    .local v21, review:Lnet/flixster/android/model/Review;
    const/16 v30, 0x1

    move/from16 v0, v30

    move-object/from16 v1, v21

    iput v0, v1, Lnet/flixster/android/model/Review;->type:I

    .line 1194
    const-string v30, "user"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 1195
    const-string v30, "user"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v28

    .line 1196
    .local v28, userObject:Lorg/json/JSONObject;
    const-string v30, "id"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v30

    move-wide/from16 v0, v30

    move-object/from16 v2, v21

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->userId:J

    .line 1197
    const-string v30, "userName"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    iput-object v0, v1, Lnet/flixster/android/model/Review;->userName:Ljava/lang/String;

    .line 1198
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "firstName"

    move-object/from16 v0, v28

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v31, " "

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    .line 1199
    const-string v31, "lastName"

    move-object/from16 v0, v28

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-virtual/range {v30 .. v31}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    .line 1198
    invoke-static/range {v30 .. v30}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    iput-object v0, v1, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    .line 1200
    const-string v30, "images"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 1201
    const-string v30, "images"

    move-object/from16 v0, v28

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v13

    .line 1202
    .local v13, imagesObj:Lorg/json/JSONObject;
    const-string v30, "thumbnail"

    move-object/from16 v0, v30

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_0

    .line 1203
    const-string v30, "thumbnail"

    move-object/from16 v0, v30

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    iput-object v0, v1, Lnet/flixster/android/model/Review;->mugUrl:Ljava/lang/String;

    .line 1208
    .end local v13           #imagesObj:Lorg/json/JSONObject;
    .end local v28           #userObject:Lorg/json/JSONObject;
    :cond_0
    const-string v30, "movie"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_d

    .line 1209
    const-string v30, "movie"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v18

    .line 1210
    .local v18, movieObject:Lorg/json/JSONObject;
    const-string v30, "id"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 1211
    .local v16, movieId:J
    invoke-static/range {v16 .. v17}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v15

    .line 1212
    .local v15, movie:Lnet/flixster/android/model/Movie;
    const-string v30, "title"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v15, v0}, Lnet/flixster/android/model/Movie;->hintTitle(Ljava/lang/String;)V

    .line 1213
    const-string v30, "title"

    const-string v31, "title"

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    const-string v30, "mpaa"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_1

    .line 1215
    const-string v30, "mpaa"

    const-string v31, "mpaa"

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1217
    :cond_1
    const-string v30, "status"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_2

    .line 1218
    const-string v30, "status"

    const-string v31, "status"

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1220
    :cond_2
    const-string v30, "runningTime"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_3

    .line 1221
    const-string v30, "runningTime"

    const-string v31, "runningTime"

    move-object/from16 v0, v18

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v31 .. v31}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1223
    :cond_3
    const-string v30, "mpaa"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-nez v30, :cond_4

    const-string v30, "runningTime"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_5

    .line 1224
    :cond_4
    invoke-virtual {v15}, Lnet/flixster/android/model/Movie;->buildMeta()V

    .line 1226
    :cond_5
    const-string v30, "theaterReleaseDate"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_6

    .line 1227
    const-string v30, "theaterReleaseDate"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v24

    .line 1228
    .local v24, theaterReleaseObject:Lorg/json/JSONObject;
    const-string v30, "day"

    const/16 v31, -0x1

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    .line 1229
    .local v8, day:I
    if-lez v8, :cond_6

    .line 1230
    const-string v30, "year"

    const/16 v31, -0x1

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v29

    .line 1231
    .local v29, year:I
    const-string v30, "month"

    const/16 v31, -0x1

    move-object/from16 v0, v24

    move-object/from16 v1, v30

    move/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v14

    .line 1232
    .local v14, month:I
    new-instance v30, Ljava/util/GregorianCalendar;

    add-int/lit8 v31, v14, -0x1

    move-object/from16 v0, v30

    move/from16 v1, v29

    move/from16 v2, v31

    invoke-direct {v0, v1, v2, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual/range {v30 .. v30}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v20

    .line 1234
    .local v20, releaseDate:Ljava/util/Date;
    :try_start_0
    const-string v30, "theaterReleaseDate"

    invoke-static {}, Lcom/flixster/android/utils/DateTimeHelper;->mediumDateFormatter()Ljava/text/DateFormat;

    move-result-object v31

    .line 1235
    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v31

    .line 1234
    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1241
    .end local v8           #day:I
    .end local v14           #month:I
    .end local v20           #releaseDate:Ljava/util/Date;
    .end local v24           #theaterReleaseObject:Lorg/json/JSONObject;
    .end local v29           #year:I
    :cond_6
    :goto_0
    const-string v30, "dvdReleaseDate"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_7

    .line 1242
    const-string v30, "dvdReleaseDate"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 1243
    .local v9, dvdReleaseObject:Lorg/json/JSONObject;
    const-string v30, "day"

    const/16 v31, -0x1

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v9, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    .line 1244
    .restart local v8       #day:I
    if-lez v8, :cond_7

    .line 1245
    const-string v30, "year"

    const/16 v31, -0x1

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v9, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v29

    .line 1246
    .restart local v29       #year:I
    const-string v30, "month"

    const/16 v31, -0x1

    move-object/from16 v0, v30

    move/from16 v1, v31

    invoke-virtual {v9, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v14

    .line 1247
    .restart local v14       #month:I
    new-instance v30, Ljava/util/GregorianCalendar;

    add-int/lit8 v31, v14, -0x1

    move-object/from16 v0, v30

    move/from16 v1, v29

    move/from16 v2, v31

    invoke-direct {v0, v1, v2, v8}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual/range {v30 .. v30}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v20

    .line 1249
    .restart local v20       #releaseDate:Ljava/util/Date;
    :try_start_1
    const-string v30, "dvdReleaseDate"

    invoke-static {}, Lcom/flixster/android/utils/DateTimeHelper;->mediumDateFormatter()Ljava/text/DateFormat;

    move-result-object v31

    .line 1250
    move-object/from16 v0, v31

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v31

    .line 1249
    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1256
    .end local v8           #day:I
    .end local v9           #dvdReleaseObject:Lorg/json/JSONObject;
    .end local v14           #month:I
    .end local v20           #releaseDate:Ljava/util/Date;
    .end local v29           #year:I
    :cond_7
    :goto_1
    const-string v30, "poster"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_8

    .line 1257
    const-string v30, "poster"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v19

    .line 1258
    .local v19, posterObject:Lorg/json/JSONObject;
    const-string v30, "thumbnail"

    const-string v31, "thumbnail"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1259
    const-string v30, "profile"

    const-string v31, "profile"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1260
    const-string v30, "detailed"

    const-string v31, "detailed"

    move-object/from16 v0, v19

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    .end local v19           #posterObject:Lorg/json/JSONObject;
    :cond_8
    const-string v30, "actors"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_9

    .line 1263
    const-string v30, "actors"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 1264
    .local v3, actorArray:Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v26

    .line 1265
    .local v26, totalActors:I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 1266
    .local v6, actorsBuilder:Ljava/lang/StringBuilder;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 1267
    .local v7, actorsShortBuilder:Ljava/lang/StringBuilder;
    const/4 v12, 0x0

    .local v12, i:I
    :goto_2
    move/from16 v0, v26

    if-lt v12, v0, :cond_10

    .line 1282
    const-string v30, "MOVIE_ACTORS"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1283
    const-string v30, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    .end local v3           #actorArray:Lorg/json/JSONArray;
    .end local v6           #actorsBuilder:Ljava/lang/StringBuilder;
    .end local v7           #actorsShortBuilder:Ljava/lang/StringBuilder;
    .end local v12           #i:I
    .end local v26           #totalActors:I
    :cond_9
    const-string v30, "reviews"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1287
    const-string v30, "reviews"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v22

    .line 1288
    .local v22, reviewsObject:Lorg/json/JSONObject;
    const-string v30, "flixster"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_a

    .line 1289
    const-string v30, "flixster"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    .line 1290
    .local v11, flixsterReviewObject:Lorg/json/JSONObject;
    const-string v30, "numLiked"

    move-object/from16 v0, v30

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_14

    .line 1291
    const-string v30, "popcornScore"

    const-string v31, "numLiked"

    const/16 v32, -0x1

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setIntProperty(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1296
    :goto_3
    const-string v30, "numWantToSee"

    const-string v31, "numWantToSee"

    const/16 v32, -0x1

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setIntProperty(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1298
    .end local v11           #flixsterReviewObject:Lorg/json/JSONObject;
    :cond_a
    const-string v30, "rottenTomatoes"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_b

    .line 1299
    const-string v30, "rottenTomatoes"

    move-object/from16 v0, v22

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v25

    .line 1300
    .local v25, tomatoReviewObject:Lorg/json/JSONObject;
    const-string v30, "rottenTomatoes"

    const-string v31, "rating"

    const/16 v32, -0x1

    move-object/from16 v0, v25

    move-object/from16 v1, v31

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setIntProperty(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1303
    .end local v22           #reviewsObject:Lorg/json/JSONObject;
    .end local v25           #tomatoReviewObject:Lorg/json/JSONObject;
    :cond_b
    const-string v30, "trailer"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_c

    .line 1304
    const-string v30, "trailer"

    move-object/from16 v0, v18

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v27

    .line 1305
    .local v27, trailerObject:Lorg/json/JSONObject;
    const-string v30, "high"

    move-object/from16 v0, v27

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_c

    .line 1306
    const-string v30, "high"

    .line 1307
    const-string v31, "high"

    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v31

    .line 1306
    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 1310
    .end local v27           #trailerObject:Lorg/json/JSONObject;
    :cond_c
    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Lnet/flixster/android/model/Review;->setMovie(Lnet/flixster/android/model/Movie;)V

    .line 1313
    .end local v15           #movie:Lnet/flixster/android/model/Movie;
    .end local v16           #movieId:J
    .end local v18           #movieObject:Lorg/json/JSONObject;
    :cond_d
    const-string v30, "review"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_e

    .line 1314
    const-string v30, "review"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v21

    iput-object v0, v1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 1317
    :cond_e
    const-string v30, "score"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v30

    if-eqz v30, :cond_f

    .line 1318
    const-string v30, "score"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 1319
    .local v23, score:Ljava/lang/String;
    const-string v30, "+"

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v30

    if-eqz v30, :cond_15

    .line 1320
    const-wide/high16 v30, 0x4016

    move-wide/from16 v0, v30

    move-object/from16 v2, v21

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->stars:D

    .line 1329
    .end local v23           #score:Ljava/lang/String;
    :cond_f
    :goto_4
    return-object v21

    .line 1236
    .restart local v8       #day:I
    .restart local v14       #month:I
    .restart local v15       #movie:Lnet/flixster/android/model/Movie;
    .restart local v16       #movieId:J
    .restart local v18       #movieObject:Lorg/json/JSONObject;
    .restart local v20       #releaseDate:Ljava/util/Date;
    .restart local v24       #theaterReleaseObject:Lorg/json/JSONObject;
    .restart local v29       #year:I
    :catch_0
    move-exception v10

    .line 1237
    .local v10, e:Ljava/lang/Exception;
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "problem parsing movie release date "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1251
    .end local v10           #e:Ljava/lang/Exception;
    .end local v24           #theaterReleaseObject:Lorg/json/JSONObject;
    .restart local v9       #dvdReleaseObject:Lorg/json/JSONObject;
    :catch_1
    move-exception v10

    .line 1252
    .restart local v10       #e:Ljava/lang/Exception;
    new-instance v30, Ljava/lang/StringBuilder;

    const-string v31, "problem parsing dvd release date "

    invoke-direct/range {v30 .. v31}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v30

    invoke-virtual {v10}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v31

    invoke-static/range {v30 .. v31}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 1268
    .end local v8           #day:I
    .end local v9           #dvdReleaseObject:Lorg/json/JSONObject;
    .end local v10           #e:Ljava/lang/Exception;
    .end local v14           #month:I
    .end local v20           #releaseDate:Ljava/util/Date;
    .end local v29           #year:I
    .restart local v3       #actorArray:Lorg/json/JSONArray;
    .restart local v6       #actorsBuilder:Ljava/lang/StringBuilder;
    .restart local v7       #actorsShortBuilder:Ljava/lang/StringBuilder;
    .restart local v12       #i:I
    .restart local v26       #totalActors:I
    :cond_10
    invoke-virtual {v3, v12}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 1269
    .local v5, actorObject:Lorg/json/JSONObject;
    const-string v30, "name"

    move-object/from16 v0, v30

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1270
    .local v4, actorName:Ljava/lang/String;
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1271
    const/16 v30, 0x2

    move/from16 v0, v30

    if-ge v12, v0, :cond_12

    .line 1272
    const/16 v30, 0x1

    move/from16 v0, v30

    if-ne v12, v0, :cond_11

    .line 1273
    const-string v30, ", "

    move-object/from16 v0, v30

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1275
    :cond_11
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1277
    :cond_12
    add-int/lit8 v30, v26, -0x1

    move/from16 v0, v30

    if-ge v12, v0, :cond_13

    .line 1278
    const-string v30, ", "

    move-object/from16 v0, v30

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1267
    :cond_13
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_2

    .line 1293
    .end local v3           #actorArray:Lorg/json/JSONArray;
    .end local v4           #actorName:Ljava/lang/String;
    .end local v5           #actorObject:Lorg/json/JSONObject;
    .end local v6           #actorsBuilder:Ljava/lang/StringBuilder;
    .end local v7           #actorsShortBuilder:Ljava/lang/StringBuilder;
    .end local v12           #i:I
    .end local v26           #totalActors:I
    .restart local v11       #flixsterReviewObject:Lorg/json/JSONObject;
    .restart local v22       #reviewsObject:Lorg/json/JSONObject;
    :cond_14
    const-string v30, "popcornScore"

    .line 1294
    const-string v31, "popcornScore"

    const/16 v32, -0x1

    move-object/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v31

    invoke-static/range {v31 .. v31}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v31

    .line 1293
    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v15, v0, v1}, Lnet/flixster/android/model/Movie;->setIntProperty(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    .line 1321
    .end local v11           #flixsterReviewObject:Lorg/json/JSONObject;
    .end local v15           #movie:Lnet/flixster/android/model/Movie;
    .end local v16           #movieId:J
    .end local v18           #movieObject:Lorg/json/JSONObject;
    .end local v22           #reviewsObject:Lorg/json/JSONObject;
    .restart local v23       #score:Ljava/lang/String;
    :cond_15
    const-string v30, "-"

    move-object/from16 v0, v23

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v30

    if-eqz v30, :cond_16

    .line 1322
    const-wide/high16 v30, 0x4018

    move-wide/from16 v0, v30

    move-object/from16 v2, v21

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->stars:D

    goto/16 :goto_4

    .line 1323
    :cond_16
    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v30

    if-lez v30, :cond_17

    .line 1324
    invoke-static/range {v23 .. v23}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v30

    move-wide/from16 v0, v30

    move-object/from16 v2, v21

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->stars:D

    goto/16 :goto_4

    .line 1326
    :cond_17
    const-wide/16 v30, 0x0

    move-wide/from16 v0, v30

    move-object/from16 v2, v21

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->stars:D

    goto/16 :goto_4
.end method

.method private static parseUser(Lorg/json/JSONObject;)Lnet/flixster/android/model/User;
    .locals 5
    .parameter "userObject"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1168
    new-instance v2, Lnet/flixster/android/model/User;

    invoke-direct {v2}, Lnet/flixster/android/model/User;-><init>()V

    .line 1169
    .local v2, user:Lnet/flixster/android/model/User;
    const-string v3, "id"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    .line 1170
    const-string v3, "userName"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->userName:Ljava/lang/String;

    .line 1171
    const-string v3, "displayName"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    .line 1172
    const-string v3, "firstName"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->firstName:Ljava/lang/String;

    .line 1173
    const-string v3, "lastName"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    .line 1174
    const-string v3, "images"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1175
    const-string v3, "images"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 1176
    .local v0, imagesObject:Lorg/json/JSONObject;
    const-string v3, "thumbnail"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->thumbnailImage:Ljava/lang/String;

    .line 1177
    const-string v3, "small"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->smallImage:Ljava/lang/String;

    .line 1178
    const-string v3, "profile"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    .line 1180
    .end local v0           #imagesObject:Lorg/json/JSONObject;
    :cond_0
    const-string v3, "stats"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1181
    const-string v3, "stats"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 1182
    .local v1, statsObject:Lorg/json/JSONObject;
    const-string v3, "wts"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Lnet/flixster/android/model/User;->wtsCount:I

    .line 1183
    const-string v3, "ratings"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Lnet/flixster/android/model/User;->ratingCount:I

    .line 1184
    const-string v3, "reviews"

    invoke-virtual {v1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Lnet/flixster/android/model/User;->reviewCount:I

    .line 1185
    iget v3, v2, Lnet/flixster/android/model/User;->ratingCount:I

    iget v4, v2, Lnet/flixster/android/model/User;->wtsCount:I

    add-int/2addr v3, v4

    iput v3, v2, Lnet/flixster/android/model/User;->ratedCount:I

    .line 1187
    .end local v1           #statsObject:Lorg/json/JSONObject;
    :cond_1
    return-object v2
.end method

.method public static postUserMovieReview(Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Review;Z)Lorg/json/JSONObject;
    .locals 9
    .parameter "flixsterId"
    .parameter "movieId"
    .parameter "review"
    .parameter "postNewsFeed"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 269
    iget-wide v5, p2, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v7, 0x4016

    cmpl-double v5, v5, v7

    if-nez v5, :cond_1

    .line 270
    const-string v3, "+"

    .line 277
    .local v3, scoreString:Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 278
    .local v1, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "score"

    invoke-direct {v5, v6, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "review"

    iget-object v7, p2, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 280
    if-eqz p3, :cond_0

    .line 281
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "newsfeed"

    const-string v7, "true"

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    :cond_0
    invoke-static {v1}, Lnet/flixster/android/FlixsterApplication;->addCurrentUserPairs(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 284
    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "ver"

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionCode()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    invoke-static {p0, p1}, Lnet/flixster/android/data/ApiBuilder;->userMovieReviewShort(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 287
    .local v4, url:Ljava/lang/String;
    invoke-static {v4, v1}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v2

    .line 288
    .local v2, response:Ljava/lang/String;
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 290
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 293
    :goto_1
    return-object v5

    .line 271
    .end local v1           #params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .end local v2           #response:Ljava/lang/String;
    .end local v3           #scoreString:Ljava/lang/String;
    .end local v4           #url:Ljava/lang/String;
    :cond_1
    iget-wide v5, p2, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v7, 0x4018

    cmpl-double v5, v5, v7

    if-nez v5, :cond_2

    .line 272
    const-string v3, "-"

    .restart local v3       #scoreString:Ljava/lang/String;
    goto :goto_0

    .line 274
    .end local v3           #scoreString:Ljava/lang/String;
    :cond_2
    iget-wide v5, p2, Lnet/flixster/android/model/Review;->stars:D

    invoke-static {v5, v6}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v3

    .restart local v3       #scoreString:Ljava/lang/String;
    goto :goto_0

    .line 291
    .restart local v1       #params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .restart local v2       #response:Ljava/lang/String;
    .restart local v4       #url:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 292
    .local v0, e:Lorg/json/JSONException;
    const-string v5, "FlxMain"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "problem parsing json response "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 293
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    goto :goto_1
.end method

.method public static postUserMovieReview(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Review;)V
    .locals 7
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "flixsterId"
    .parameter "movieId"
    .parameter "review"

    .prologue
    .line 249
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v6

    new-instance v0, Lnet/flixster/android/data/ProfileDao$3;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/data/ProfileDao$3;-><init>(Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Review;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v6, v0}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 264
    return-void
.end method

.method public static resetUser()V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    sput-object v0, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    .line 198
    return-void
.end method

.method public static streamStart(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "right"
    .parameter "urlType"

    .prologue
    .line 671
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$12;

    invoke-direct {v1, p2, p1, p3, p0}, Lnet/flixster/android/data/ProfileDao$12;-><init>(Lnet/flixster/android/model/LockerRight;Landroid/os/Handler;Ljava/lang/String;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 743
    return-void
.end method

.method public static streamStop(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "right"
    .parameter "streamId"
    .parameter "playPosition"

    .prologue
    .line 747
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v6

    new-instance v0, Lnet/flixster/android/data/ProfileDao$13;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/data/ProfileDao$13;-><init>(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v6, v0}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 765
    return-void
.end method

.method private static toastLockerRightInsertion(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
    .locals 2
    .parameter "right"
    .parameter "purchaseType"

    .prologue
    .line 527
    invoke-static {}, Lcom/flixster/android/utils/ActivityHolder;->instance()Lcom/flixster/android/utils/ActivityHolder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/ActivityHolder;->getTopLevelActivity()Landroid/app/Activity;

    move-result-object v0

    .line 528
    .local v0, activity:Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 529
    new-instance v1, Lnet/flixster/android/data/ProfileDao$8;

    invoke-direct {v1, p0, v0, p1}, Lnet/flixster/android/data/ProfileDao$8;-><init>(Lnet/flixster/android/model/LockerRight;Landroid/app/Activity;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 554
    :cond_0
    return-void
.end method

.method public static trackLicense(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Z)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "right"
    .parameter "streamId"
    .parameter "isSuccess"

    .prologue
    .line 769
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$14;

    invoke-direct {v1, p2, p3, p4, p1}, Lnet/flixster/android/data/ProfileDao$14;-><init>(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;ZLandroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 782
    return-void
.end method

.method public static trackMskFbRequest(Ljava/lang/String;)V
    .locals 3
    .parameter "requestId"

    .prologue
    .line 882
    sget-object v1, Lnet/flixster/android/data/ProfileDao;->currentUser:Lnet/flixster/android/model/User;

    iget-object v0, v1, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    .line 883
    .local v0, userId:Ljava/lang/String;
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v1

    new-instance v2, Lnet/flixster/android/data/ProfileDao$18;

    invoke-direct {v2, p0, v0}, Lnet/flixster/android/data/ProfileDao$18;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 899
    return-void
.end method

.method public static updatePlaybackPosition(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;Ljava/lang/String;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "right"
    .parameter "playPosition"

    .prologue
    .line 652
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$11;

    invoke-direct {v1, p2, p3, p1, p0}, Lnet/flixster/android/data/ProfileDao$11;-><init>(Lnet/flixster/android/model/LockerRight;Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 667
    return-void
.end method

.method public static uvStreamCreate(Landroid/os/Handler;Landroid/os/Handler;Lnet/flixster/android/model/LockerRight;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "right"

    .prologue
    .line 799
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$16;

    invoke-direct {v1, p2, p1, p0}, Lnet/flixster/android/data/ProfileDao$16;-><init>(Lnet/flixster/android/model/LockerRight;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 857
    return-void
.end method

.method public static uvStreamDelete(Landroid/os/Handler;Landroid/os/Handler;Ljava/lang/String;)V
    .locals 2
    .parameter "successHandler"
    .parameter "errorHandler"
    .parameter "streamId"

    .prologue
    .line 860
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v0

    new-instance v1, Lnet/flixster/android/data/ProfileDao$17;

    invoke-direct {v1, p2, p1, p0}, Lnet/flixster/android/data/ProfileDao$17;-><init>(Ljava/lang/String;Landroid/os/Handler;Landroid/os/Handler;)V

    invoke-virtual {v0, v1}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 879
    return-void
.end method
