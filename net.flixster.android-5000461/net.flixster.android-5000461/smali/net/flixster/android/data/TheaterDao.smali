.class public Lnet/flixster/android/data/TheaterDao;
.super Ljava/lang/Object;
.source "TheaterDao.java"


# static fields
.field public static final THEATER_CACHE:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation
.end field

.field public static mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

.field public static mTheaterNameComparator:Lnet/flixster/android/model/TheaterNameComparator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lnet/flixster/android/util/SoftHashMap;

    invoke-direct {v0}, Lnet/flixster/android/util/SoftHashMap;-><init>()V

    sput-object v0, Lnet/flixster/android/data/TheaterDao;->THEATER_CACHE:Ljava/util/Map;

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findShowtimesByTheater(Ljava/util/Date;J)Ljava/util/HashMap;
    .locals 10
    .parameter "date"
    .parameter "theaterId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Date;",
            "J)",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Showtimes;",
            ">;>;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 140
    invoke-static {p1, p2}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v6

    .line 142
    .local v6, targetTheater:Lnet/flixster/android/model/Theater;
    invoke-virtual {v6}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;

    move-result-object v5

    .line 148
    .local v5, showtimesListByMovieHash:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Long;Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;>;"
    :try_start_0
    new-instance v7, Ljava/net/URL;

    invoke-static {p1, p2, p0}, Lnet/flixster/android/data/ApiBuilder;->theater(JLjava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 156
    .local v4, response:Ljava/lang/String;
    :try_start_1
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 157
    .local v2, jsonTheater:Lorg/json/JSONObject;
    invoke-virtual {v6, v2}, Lnet/flixster/android/model/Theater;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Theater;

    .line 158
    invoke-virtual {v6}, Lnet/flixster/android/model/Theater;->getShowtimesListByMovieHash()Ljava/util/HashMap;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v5

    .line 159
    return-object v5

    .line 149
    .end local v2           #jsonTheater:Lorg/json/JSONObject;
    .end local v4           #response:Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 150
    .local v3, mue:Ljava/net/MalformedURLException;
    new-instance v7, Ljava/lang/RuntimeException;

    invoke-direct {v7, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 151
    .end local v3           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v0

    .line 152
    .local v0, ie:Ljava/io/IOException;
    invoke-static {v0}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v7

    throw v7

    .line 160
    .end local v0           #ie:Ljava/io/IOException;
    .restart local v4       #response:Ljava/lang/String;
    :catch_2
    move-exception v1

    .line 161
    .local v1, je:Lorg/json/JSONException;
    invoke-static {v1}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v7

    throw v7
.end method

.method public static findTheaterByTheaterMoviePair(JJLjava/util/Date;)Lnet/flixster/android/model/Theater;
    .locals 2
    .parameter "theaterId"
    .parameter "movieId"
    .parameter "date"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 89
    invoke-static {p0, p1, p2, p3, p4}, Lnet/flixster/android/data/ApiBuilder;->theaterMoviePair(JJLjava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieUrl(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 92
    .local v0, listOfOne:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Theater;

    return-object v1
.end method

.method public static findTheaters(Ljava/util/List;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 1
    .parameter
    .parameter "postal"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 48
    .local p0, emptyTheaterList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    .local p2, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-static {p1}, Lnet/flixster/android/data/ApiBuilder;->theaters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2}, Lnet/flixster/android/data/TheaterDao;->getTheaters(Ljava/util/List;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 49
    return-void
.end method

.method public static findTheatersByMovieLocation(DDLjava/util/Date;JLjava/util/HashMap;)Ljava/util/List;
    .locals 8
    .parameter "latitude"
    .parameter "longitude"
    .parameter "date"
    .parameter "movieId"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(DD",
            "Ljava/util/Date;",
            "J",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 84
    .local p7, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    move-wide v0, p0

    move-wide v2, p2

    move-wide v4, p5

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lnet/flixster/android/data/ApiBuilder;->theatersByMovie(DDJLjava/util/Date;)Ljava/lang/String;

    move-result-object v7

    .line 85
    .local v7, url:Ljava/lang/String;
    invoke-static {v7, p7}, Lnet/flixster/android/data/TheaterDao;->findTheatersWithFavoritesByMoveUrl(Ljava/lang/String;Ljava/util/HashMap;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private static findTheatersByMovieUrl(Ljava/lang/String;)Ljava/util/List;
    .locals 17
    .parameter "url"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 105
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 106
    .local v8, results:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v1, favoriteTheaters:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    :try_start_0
    new-instance v14, Ljava/net/URL;

    move-object/from16 v0, p0

    invoke-direct {v14, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v7

    .line 118
    .local v7, response:Ljava/lang/String;
    :try_start_1
    new-instance v13, Lorg/json/JSONArray;

    invoke-direct {v13, v7}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 119
    .local v13, theaters:Lorg/json/JSONArray;
    invoke-virtual {v13}, Lorg/json/JSONArray;->length()I

    move-result v5

    .line 120
    .local v5, length:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-lt v2, v5, :cond_0

    .line 127
    new-instance v14, Lnet/flixster/android/model/TheaterDistanceComparator;

    invoke-direct {v14}, Lnet/flixster/android/model/TheaterDistanceComparator;-><init>()V

    sput-object v14, Lnet/flixster/android/data/TheaterDao;->mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

    .line 128
    sget-object v14, Lnet/flixster/android/data/TheaterDao;->mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

    invoke-static {v1, v14}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 129
    sget-object v14, Lnet/flixster/android/data/TheaterDao;->mTheaterDistanceComparator:Lnet/flixster/android/model/TheaterDistanceComparator;

    invoke-static {v8, v14}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 130
    invoke-interface {v1, v8}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    .line 131
    return-object v1

    .line 111
    .end local v2           #i:I
    .end local v5           #length:I
    .end local v7           #response:Ljava/lang/String;
    .end local v13           #theaters:Lorg/json/JSONArray;
    :catch_0
    move-exception v6

    .line 112
    .local v6, mue:Ljava/net/MalformedURLException;
    new-instance v14, Ljava/lang/RuntimeException;

    invoke-direct {v14, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v14

    .line 113
    .end local v6           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v3

    .line 114
    .local v3, ie:Ljava/io/IOException;
    invoke-static {v3}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v14

    throw v14

    .line 121
    .end local v3           #ie:Ljava/io/IOException;
    .restart local v2       #i:I
    .restart local v5       #length:I
    .restart local v7       #response:Ljava/lang/String;
    .restart local v13       #theaters:Lorg/json/JSONArray;
    :cond_0
    :try_start_2
    invoke-virtual {v13, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 122
    .local v9, tempTheaterObj:Lorg/json/JSONObject;
    const-string v14, "id"

    invoke-virtual {v9, v14}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    .line 123
    .local v11, theaterId:J
    invoke-static {v11, v12}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v10

    .line 124
    .local v10, theater:Lnet/flixster/android/model/Theater;
    invoke-virtual {v10, v9}, Lnet/flixster/android/model/Theater;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Theater;

    .line 125
    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 120
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 132
    .end local v2           #i:I
    .end local v5           #length:I
    .end local v9           #tempTheaterObj:Lorg/json/JSONObject;
    .end local v10           #theater:Lnet/flixster/android/model/Theater;
    .end local v11           #theaterId:J
    .end local v13           #theaters:Lorg/json/JSONArray;
    :catch_2
    move-exception v4

    .line 133
    .local v4, je:Lorg/json/JSONException;
    invoke-static {v4}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v14

    throw v14
.end method

.method public static findTheatersLocation(Ljava/util/List;DDLjava/util/HashMap;)V
    .locals 1
    .parameter
    .parameter "latitude"
    .parameter "longitude"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;DD",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 43
    .local p0, emptyTheaterList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    .local p5, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    invoke-static {p1, p2, p3, p4}, Lnet/flixster/android/data/ApiBuilder;->theaters(DD)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p5}, Lnet/flixster/android/data/TheaterDao;->getTheaters(Ljava/util/List;Ljava/lang/String;Ljava/util/HashMap;)V

    .line 44
    return-void
.end method

.method private static findTheatersWithFavoritesByMoveUrl(Ljava/lang/String;Ljava/util/HashMap;)Ljava/util/List;
    .locals 4
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 97
    .local p1, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    const-string v0, ""

    .line 98
    .local v0, favoritesURL:Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 99
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "&theater="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "&theater="

    invoke-virtual {p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/data/TheaterDao;->findTheatersByMovieUrl(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    return-object v1
.end method

.method public static getTheater(J)Lnet/flixster/android/model/Theater;
    .locals 3
    .parameter "theaterId"

    .prologue
    .line 33
    sget-object v1, Lnet/flixster/android/data/TheaterDao;->THEATER_CACHE:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/Theater;

    .line 34
    .local v0, theater:Lnet/flixster/android/model/Theater;
    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lnet/flixster/android/model/Theater;

    .end local v0           #theater:Lnet/flixster/android/model/Theater;
    invoke-direct {v0, p0, p1}, Lnet/flixster/android/model/Theater;-><init>(J)V

    .line 36
    .restart local v0       #theater:Lnet/flixster/android/model/Theater;
    sget-object v1, Lnet/flixster/android/data/TheaterDao;->THEATER_CACHE:Ljava/util/Map;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    :cond_0
    return-object v0
.end method

.method private static getTheaters(Ljava/util/List;Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 15
    .parameter
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Theater;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/data/DaoException;
        }
    .end annotation

    .prologue
    .line 53
    .local p0, emptyTheaterList:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/Theater;>;"
    .local p2, favorites:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Boolean;>;"
    const-string v0, ""

    .line 54
    .local v0, favoritesUrl:Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 55
    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, "&theater="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v13, "&theater="

    invoke-virtual/range {p2 .. p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 60
    :cond_0
    :try_start_0
    new-instance v12, Ljava/net/URL;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v12, v13, v14}, Lnet/flixster/android/util/HttpHelper;->fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 68
    .local v6, response:Ljava/lang/String;
    :try_start_1
    new-instance v8, Lorg/json/JSONArray;

    invoke-direct {v8, v6}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 69
    .local v8, theaterArray:Lorg/json/JSONArray;
    invoke-virtual {v8}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v4

    .line 70
    .local v4, length:I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-lt v1, v4, :cond_1

    .line 80
    return-void

    .line 61
    .end local v1           #i:I
    .end local v4           #length:I
    .end local v6           #response:Ljava/lang/String;
    .end local v8           #theaterArray:Lorg/json/JSONArray;
    :catch_0
    move-exception v5

    .line 62
    .local v5, mue:Ljava/net/MalformedURLException;
    new-instance v12, Ljava/lang/RuntimeException;

    invoke-direct {v12, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v12

    .line 63
    .end local v5           #mue:Ljava/net/MalformedURLException;
    :catch_1
    move-exception v2

    .line 64
    .local v2, ie:Ljava/io/IOException;
    invoke-static {v2}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v12

    throw v12

    .line 71
    .end local v2           #ie:Ljava/io/IOException;
    .restart local v1       #i:I
    .restart local v4       #length:I
    .restart local v6       #response:Ljava/lang/String;
    .restart local v8       #theaterArray:Lorg/json/JSONArray;
    :cond_1
    :try_start_2
    invoke-virtual {v8, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 72
    .local v11, theaterObject:Lorg/json/JSONObject;
    const-string v12, "id"

    invoke-virtual {v11, v12}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v9

    .line 73
    .local v9, theaterId:J
    invoke-static {v9, v10}, Lnet/flixster/android/data/TheaterDao;->getTheater(J)Lnet/flixster/android/model/Theater;

    move-result-object v7

    .line 74
    .local v7, theater:Lnet/flixster/android/model/Theater;
    invoke-virtual {v7, v11}, Lnet/flixster/android/model/Theater;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Theater;

    .line 75
    invoke-interface {p0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    .end local v1           #i:I
    .end local v4           #length:I
    .end local v7           #theater:Lnet/flixster/android/model/Theater;
    .end local v8           #theaterArray:Lorg/json/JSONArray;
    .end local v9           #theaterId:J
    .end local v11           #theaterObject:Lorg/json/JSONObject;
    :catch_2
    move-exception v3

    .line 78
    .local v3, je:Lorg/json/JSONException;
    invoke-static {v3}, Lnet/flixster/android/data/DaoException;->create(Ljava/lang/Exception;)Lnet/flixster/android/data/DaoException;

    move-result-object v12

    throw v12
.end method
