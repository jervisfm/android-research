.class public Lnet/flixster/android/data/TicketsDao;
.super Ljava/lang/Object;
.source "TicketsDao.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final CC_AMEX:I = 0x3

.field public static final CC_DISCOVER:I = 0x4

.field public static final CC_MASTERCARD:I = 0x2

.field public static final CC_UNKNOWN:I = 0x0

.field public static final CC_VISA:I = 0x1

.field public static final FIELD_STATE_BAD:I = 0x3

.field public static final FIELD_STATE_GOOD:I = 0x2

.field public static final FIELD_STATE_NEW:I = 0x0

.field public static final FIELD_STATE_PARTIAL:I = 0x1

.field public static final UNSELECTED_MONTH:I = -0x1

.field public static final UNSELECTED_YEAR:I = -0x64

.field private static final sAmexPattern:Ljava/util/regex/Pattern;

.field private static final sEmailPattern:Ljava/util/regex/Pattern;

.field private static final sMcPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-string v0, "^5[1-5][0-9]{0,14}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/data/TicketsDao;->sMcPattern:Ljava/util/regex/Pattern;

    .line 44
    const-string v0, "^((34)|(37))[0-9]{0,13}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/data/TicketsDao;->sAmexPattern:Ljava/util/regex/Pattern;

    .line 47
    const-string v0, "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/data/TicketsDao;->sEmailPattern:Ljava/util/regex/Pattern;

    .line 26
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static cardCheckSum(Ljava/lang/CharSequence;)I
    .locals 7
    .parameter "cardNumber"

    .prologue
    .line 328
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    .line 329
    .local v1, length:I
    const/4 v3, 0x0

    .line 333
    .local v3, sum:I
    add-int/lit8 v0, v1, -0x1

    .local v0, i:I
    :goto_0
    if-gez v0, :cond_0

    .line 344
    rem-int/lit8 v5, v3, 0xa

    if-nez v5, :cond_2

    .line 345
    const/4 v5, 0x2

    .line 347
    :goto_1
    return v5

    .line 334
    :cond_0
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    const/16 v6, 0xa

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    .line 335
    .local v4, value:I
    sub-int v5, v1, v0

    rem-int/lit8 v5, v5, 0x2

    if-nez v5, :cond_1

    .line 336
    mul-int/lit8 v2, v4, 0x2

    .line 337
    .local v2, miniValue:I
    rem-int/lit8 v5, v2, 0xa

    add-int/2addr v3, v5

    .line 338
    div-int/lit8 v5, v2, 0xa

    add-int/2addr v3, v5

    .line 333
    .end local v2           #miniValue:I
    :goto_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 340
    :cond_1
    add-int/2addr v3, v4

    goto :goto_2

    .line 347
    .end local v4           #value:I
    :cond_2
    const/4 v5, 0x3

    goto :goto_1
.end method

.method public static getMoviePerformance(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lnet/flixster/android/model/Performance;
    .locals 10
    .parameter "theater"
    .parameter "listing"
    .parameter "date"
    .parameter "time"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 126
    :try_start_0
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "TicketDao.getMoviePerformance theater:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " listing:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " date:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 127
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " time:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 126
    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-static {p0, p1, p2, p3}, Lnet/flixster/android/data/ApiBuilder;->ticketAvailability(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 131
    .local v6, url:Ljava/lang/String;
    new-instance v0, Lcom/flixster/android/net/RestClient;

    invoke-direct {v0, v6}, Lcom/flixster/android/net/RestClient;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    .local v0, client:Lcom/flixster/android/net/RestClient;
    :try_start_1
    sget-object v7, Lcom/flixster/android/net/RestClient$RequestMethod;->GET:Lcom/flixster/android/net/RestClient$RequestMethod;

    invoke-virtual {v0, v7}, Lcom/flixster/android/net/RestClient;->execute(Lcom/flixster/android/net/RestClient$RequestMethod;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 138
    :try_start_2
    invoke-virtual {v0}, Lcom/flixster/android/net/RestClient;->getResponse()Ljava/lang/String;

    move-result-object v5

    .line 140
    .local v5, responseBody:Ljava/lang/String;
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v5}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 143
    .local v3, jsonPerformance:Lorg/json/JSONObject;
    const-string v7, "error"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 144
    const-string v7, "error"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 145
    .local v2, error:Ljava/lang/String;
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "TicketsDao.getMoviePerformance error: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v7, Lnet/flixster/android/model/MovieTicketException;

    invoke-direct {v7, v2}, Lnet/flixster/android/model/MovieTicketException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    .line 158
    .end local v0           #client:Lcom/flixster/android/net/RestClient;
    .end local v2           #error:Ljava/lang/String;
    .end local v3           #jsonPerformance:Lorg/json/JSONObject;
    .end local v5           #responseBody:Ljava/lang/String;
    .end local v6           #url:Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 159
    .local v1, e:Lorg/json/JSONException;
    const-string v7, "FlxMain"

    const-string v8, "TicketsDao JSONException"

    invoke-static {v7, v8, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 162
    new-instance v7, Ljava/io/IOException;

    const-string v8, "TicketsDao.getMoviePerformance - JSONException becuase of an empty response?"

    invoke-direct {v7, v8}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 134
    .end local v1           #e:Lorg/json/JSONException;
    .restart local v0       #client:Lcom/flixster/android/net/RestClient;
    .restart local v6       #url:Ljava/lang/String;
    :catch_1
    move-exception v1

    .line 135
    .local v1, e:Ljava/io/IOException;
    :try_start_3
    const-string v7, "FlxMain"

    const-string v8, "TicketsDao.getMoviePerformance"

    invoke-static {v7, v8, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 136
    throw v1

    .line 149
    .end local v1           #e:Ljava/io/IOException;
    .restart local v3       #jsonPerformance:Lorg/json/JSONObject;
    .restart local v5       #responseBody:Ljava/lang/String;
    :cond_0
    new-instance v4, Lnet/flixster/android/model/Performance;

    invoke-direct {v4}, Lnet/flixster/android/model/Performance;-><init>()V

    .line 150
    .local v4, performance:Lnet/flixster/android/model/Performance;
    invoke-virtual {v4, v3}, Lnet/flixster/android/model/Performance;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Performance;

    .line 152
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "TicketDao.getMoviePerformance performace:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 154
    return-object v4
.end method

.method public static getReservation(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/model/Performance;[F)Lnet/flixster/android/model/Performance;
    .locals 10
    .parameter "theater"
    .parameter "listing"
    .parameter "date"
    .parameter "time"
    .parameter "cardType"
    .parameter "cardNumber"
    .parameter "cardCode"
    .parameter "cardPostal"
    .parameter "cardExpDate"
    .parameter "email"
    .parameter "performance"
    .parameter "ticketCounts"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 88
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v5, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "theater"

    invoke-direct {v7, v8, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "listing"

    invoke-direct {v7, v8, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "date"

    invoke-direct {v7, v8, p2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "time"

    invoke-direct {v7, v8, p3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "ct"

    invoke-direct {v7, v8, p4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "cn"

    invoke-direct {v7, v8, p5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "cv"

    move-object/from16 v0, p6

    invoke-direct {v7, v8, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "cz"

    move-object/from16 v0, p7

    invoke-direct {v7, v8, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 98
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "ce"

    move-object/from16 v0, p8

    invoke-direct {v7, v8, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "email"

    move-object/from16 v0, p9

    invoke-direct {v7, v8, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    invoke-virtual/range {p10 .. p10}, Lnet/flixster/android/model/Performance;->getCategoryTally()I

    move-result v4

    .line 102
    .local v4, length:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-lt v2, v4, :cond_0

    .line 106
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->ticketReservation()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7, v5}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v6

    .line 107
    .local v6, response:Ljava/lang/String;
    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    .line 109
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "TicketDao.getReservation(...) response:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v6}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 113
    .local v3, jsonPerformance:Lorg/json/JSONObject;
    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Lnet/flixster/android/model/Performance;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Performance;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    .end local v3           #jsonPerformance:Lorg/json/JSONObject;
    :goto_1
    return-object p10

    .line 103
    .end local v6           #response:Ljava/lang/String;
    :cond_0
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    invoke-virtual/range {p10 .. p10}, Lnet/flixster/android/model/Performance;->getCategoryIds()[Ljava/lang/String;

    move-result-object v8

    aget-object v8, v8, v2

    aget v9, p11, v2

    float-to-int v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 114
    .restart local v6       #response:Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 115
    .local v1, e:Ljava/lang/Exception;
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "TicketsSao.getReservation problem parsing json response "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public static makeCardTypeMap([Ljava/lang/String;)[I
    .locals 5
    .parameter "idList"

    .prologue
    .line 250
    array-length v2, p0

    .line 251
    .local v2, length:I
    new-array v0, v2, [I

    .line 252
    .local v0, cardMap:[I
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-lt v1, v2, :cond_0

    .line 266
    return-object v0

    .line 253
    :cond_0
    aget-object v3, p0, v1

    const-string v4, "VI"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 254
    const/4 v3, 0x1

    aput v3, v0, v1

    .line 252
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 255
    :cond_1
    aget-object v3, p0, v1

    const-string v4, "MC"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 256
    const/4 v3, 0x2

    aput v3, v0, v1

    goto :goto_1

    .line 257
    :cond_2
    aget-object v3, p0, v1

    const-string v4, "AX"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 258
    const/4 v3, 0x3

    aput v3, v0, v1

    goto :goto_1

    .line 259
    :cond_3
    aget-object v3, p0, v1

    const-string v4, "DS"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 260
    const/4 v3, 0x4

    aput v3, v0, v1

    goto :goto_1

    .line 262
    :cond_4
    const/4 v3, 0x0

    aput v3, v0, v1

    goto :goto_1
.end method

.method public static purchaseTickets(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .parameter "reservationId"
    .parameter "email"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnet/flixster/android/model/MovieTicketException;,
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 52
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 54
    .local v2, params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "email"

    invoke-direct {v4, v5, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "reservation"

    invoke-direct {v4, v5, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->ticketPurchase()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, Lnet/flixster/android/util/HttpHelper;->postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, response:Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    .line 60
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "TicketsDao.purchaseTickets response:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 64
    .local v1, jsonResponse:Lorg/json/JSONObject;
    const-string v4, "error"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 65
    const-string v4, "FlxMain"

    const-string v5, "TicketsDao. throw standard error"

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    new-instance v4, Lnet/flixster/android/model/MovieTicketException;

    const-string v5, "error"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Lnet/flixster/android/model/MovieTicketException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Lnet/flixster/android/model/MovieTicketException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 74
    .end local v1           #jsonResponse:Lorg/json/JSONObject;
    .end local v2           #params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .end local v3           #response:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 75
    .local v0, e:Lnet/flixster/android/model/MovieTicketException;
    throw v0

    .line 69
    .end local v0           #e:Lnet/flixster/android/model/MovieTicketException;
    .restart local v1       #jsonResponse:Lorg/json/JSONObject;
    .restart local v2       #params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .restart local v3       #response:Ljava/lang/String;
    :cond_0
    :try_start_1
    const-string v4, "confirmation"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 70
    const-string v4, "confirmation"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lnet/flixster/android/model/MovieTicketException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v4

    return-object v4

    .line 76
    .end local v1           #jsonResponse:Lorg/json/JSONObject;
    .end local v2           #params:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    .end local v3           #response:Ljava/lang/String;
    :catch_1
    move-exception v0

    .line 77
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "FlxMain"

    const-string v5, "Execption on purchse occured"

    invoke-static {v4, v5, v0}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 79
    .end local v0           #e:Ljava/lang/Exception;
    :cond_1
    new-instance v4, Lnet/flixster/android/model/MovieTicketPurchaseException;

    .line 80
    const-string v5, "An error has occured. Please call MovieTickets.com (1-888-440-8457) to confirm your purchase."

    .line 79
    invoke-direct {v4, v5}, Lnet/flixster/android/model/MovieTicketPurchaseException;-><init>(Ljava/lang/String;)V

    throw v4
.end method

.method public static validateCardCode(Ljava/lang/String;I)I
    .locals 3
    .parameter "code"
    .parameter "cardType"

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x3

    .line 197
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 198
    const/4 v0, 0x0

    .line 217
    :cond_0
    :goto_0
    return v0

    .line 200
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 217
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 204
    :pswitch_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 207
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_2

    move v0, v1

    .line 208
    goto :goto_0

    .line 212
    :pswitch_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static validateCreditCard(Ljava/lang/String;I)I
    .locals 7
    .parameter "cardNumber"
    .parameter "cardType"

    .prologue
    const/4 v3, 0x2

    const/16 v6, 0x10

    const/4 v4, 0x1

    const/4 v2, 0x3

    .line 270
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 271
    .local v0, length:I
    if-nez v0, :cond_1

    .line 272
    const/4 v2, 0x0

    .line 324
    :cond_0
    :goto_0
    return v2

    .line 275
    :cond_1
    packed-switch p1, :pswitch_data_0

    :cond_2
    move v2, v4

    .line 324
    goto :goto_0

    .line 277
    :pswitch_0
    const-string v5, "4"

    invoke-virtual {p0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 280
    const/16 v2, 0xd

    if-ne v0, v2, :cond_3

    invoke-static {p0}, Lnet/flixster/android/data/TicketsDao;->cardCheckSum(Ljava/lang/CharSequence;)I

    move-result v2

    if-ne v2, v3, :cond_3

    move v2, v3

    .line 281
    goto :goto_0

    .line 283
    :cond_3
    if-ne v0, v6, :cond_2

    .line 284
    invoke-static {p0}, Lnet/flixster/android/data/TicketsDao;->cardCheckSum(Ljava/lang/CharSequence;)I

    move-result v2

    goto :goto_0

    .line 288
    :pswitch_1
    if-le v0, v4, :cond_4

    .line 289
    sget-object v3, Lnet/flixster/android/data/TicketsDao;->sMcPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 290
    .local v1, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 294
    .end local v1           #matcher:Ljava/util/regex/Matcher;
    :cond_4
    if-ne v0, v6, :cond_2

    .line 295
    invoke-static {p0}, Lnet/flixster/android/data/TicketsDao;->cardCheckSum(Ljava/lang/CharSequence;)I

    move-result v2

    goto :goto_0

    .line 299
    :pswitch_2
    if-le v0, v2, :cond_5

    const-string v3, "6011"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 304
    :cond_5
    if-ne v0, v6, :cond_2

    .line 305
    invoke-static {p0}, Lnet/flixster/android/data/TicketsDao;->cardCheckSum(Ljava/lang/CharSequence;)I

    move-result v2

    goto :goto_0

    .line 309
    :pswitch_3
    if-le v0, v4, :cond_6

    .line 310
    sget-object v3, Lnet/flixster/android/data/TicketsDao;->sAmexPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 311
    .restart local v1       #matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 318
    .end local v1           #matcher:Ljava/util/regex/Matcher;
    :cond_6
    const/16 v2, 0xf

    if-ne v0, v2, :cond_2

    .line 319
    invoke-static {p0}, Lnet/flixster/android/data/TicketsDao;->cardCheckSum(Ljava/lang/CharSequence;)I

    move-result v2

    goto :goto_0

    .line 275
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public static validateDate(III)I
    .locals 6
    .parameter "monthIndex"
    .parameter "yearIndex"
    .parameter "thisMonth"

    .prologue
    const/4 v0, 0x3

    const/4 v5, -0x1

    const/16 v4, -0x64

    .line 224
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "MovieTicketDao.validateDate(monthIndex:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " yearIndex:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 225
    const-string v3, " thisMonth:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 224
    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    if-ne p0, v5, :cond_1

    if-ne p1, v4, :cond_1

    .line 227
    const/4 v0, 0x0

    .line 246
    :cond_0
    :goto_0
    return v0

    .line 231
    :cond_1
    const/16 v1, 0xb

    if-gt p0, v1, :cond_0

    if-gez p1, :cond_2

    if-ne p1, v4, :cond_0

    .line 232
    :cond_2
    if-gez p0, :cond_3

    if-ne p0, v5, :cond_0

    .line 237
    :cond_3
    if-eq p0, v5, :cond_4

    if-ne p1, v4, :cond_5

    .line 238
    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 242
    :cond_5
    if-gtz p1, :cond_6

    if-gt p2, p0, :cond_0

    .line 243
    :cond_6
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static validateEmail(Ljava/lang/String;)I
    .locals 2
    .parameter "email"

    .prologue
    .line 170
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 171
    const/4 v1, 0x0

    .line 178
    :goto_0
    return v1

    .line 173
    :cond_0
    sget-object v1, Lnet/flixster/android/data/TicketsDao;->sEmailPattern:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 174
    .local v0, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 175
    const/4 v1, 0x2

    goto :goto_0

    .line 178
    :cond_1
    const/4 v1, 0x3

    goto :goto_0
.end method

.method public static validatePostal(Ljava/lang/String;)I
    .locals 3
    .parameter "postal"

    .prologue
    .line 182
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 183
    const/4 v1, 0x0

    .line 193
    :goto_0
    return v1

    .line 185
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x5

    if-ge v1, v2, :cond_1

    .line 186
    const/4 v1, 0x1

    goto :goto_0

    .line 189
    :cond_1
    invoke-static {p0}, Lnet/flixster/android/util/PostalCodeUtils;->parseZipcodeForShowtimes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 190
    .local v0, cleanZip:Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 191
    const/4 v1, 0x2

    goto :goto_0

    .line 193
    :cond_2
    const/4 v1, 0x3

    goto :goto_0
.end method
