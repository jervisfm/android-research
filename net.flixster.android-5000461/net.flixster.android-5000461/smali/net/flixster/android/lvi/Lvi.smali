.class public abstract Lnet/flixster/android/lvi/Lvi;
.super Ljava/lang/Object;
.source "Lvi.java"


# static fields
.field public static final VIEW_TYPE_ACTOR:I

.field public static final VIEW_TYPE_ADVIEW:I

.field public static final VIEW_TYPE_CATEGORY:I

.field public static final VIEW_TYPE_DATEPANEL:I

.field public static final VIEW_TYPE_DETAILSCAP:I

.field public static final VIEW_TYPE_DIVIDER:I

.field public static final VIEW_TYPE_FOOTER:I

.field public static final VIEW_TYPE_ICONPANEL:I

.field public static final VIEW_TYPE_LOCATIONPANEL:I

.field public static final VIEW_TYPE_MESSAGE:I

.field public static final VIEW_TYPE_MOVIE:I

.field public static final VIEW_TYPE_NEWSITEM:I

.field public static final VIEW_TYPE_PAGEHEADER:I

.field public static final VIEW_TYPE_REFRESHBAR:I

.field public static final VIEW_TYPE_SHOWTIMES:I

.field public static final VIEW_TYPE_SUBHEADER:I

.field public static final VIEW_TYPE_THEATER:I

.field public static final VIEW_TYPE_USERPROFILE:I


# instance fields
.field private isSelected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->PAGEHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_PAGEHEADER:I

    .line 16
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->SUBHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_SUBHEADER:I

    .line 17
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->MOVIE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_MOVIE:I

    .line 18
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->LOCATION_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_LOCATIONPANEL:I

    .line 19
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->DATE_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_DATEPANEL:I

    .line 20
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->ICON_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_ICONPANEL:I

    .line 21
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->THEATER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_THEATER:I

    .line 22
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->DETAILSCAP:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_DETAILSCAP:I

    .line 23
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->SHOWTIMES:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_SHOWTIMES:I

    .line 24
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->NEWS_ITEM:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_NEWSITEM:I

    .line 25
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->CATEGORY:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_CATEGORY:I

    .line 26
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->MESSAGE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_MESSAGE:I

    .line 27
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->USER_PROFILE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_USERPROFILE:I

    .line 28
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->ACTOR:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_ACTOR:I

    .line 29
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->ADVIEW:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_ADVIEW:I

    .line 30
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->FOOTER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_FOOTER:I

    .line 31
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->DIVIDER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_DIVIDER:I

    .line 32
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->REFRESHBAR:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    sput v0, Lnet/flixster/android/lvi/Lvi;->VIEW_TYPE_REFRESHBAR:I

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/lvi/Lvi;->isSelected:Z

    .line 12
    return-void
.end method


# virtual methods
.method public abstract getItemViewType()I
.end method

.method public abstract getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lnet/flixster/android/lvi/Lvi;->isSelected:Z

    return v0
.end method

.method public setSelected(Z)V
    .locals 0
    .parameter "value"

    .prologue
    .line 48
    iput-boolean p1, p0, Lnet/flixster/android/lvi/Lvi;->isSelected:Z

    .line 49
    return-void
.end method
