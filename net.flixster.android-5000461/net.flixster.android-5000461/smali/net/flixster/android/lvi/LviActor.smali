.class public Lnet/flixster/android/lvi/LviActor;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviActor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/lvi/LviActor$ActorViewHolder;
    }
.end annotation


# instance fields
.field public mActor:Lnet/flixster/android/model/Actor;

.field public mActorClick:Landroid/view/View$OnClickListener;

.field private smallThumbnail:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 27
    return-void
.end method

.method public constructor <init>(Lnet/flixster/android/model/Actor;Z)V
    .locals 0
    .parameter "actor"
    .parameter "smallThumbnail"

    .prologue
    .line 29
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 30
    iput-object p1, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    .line 31
    iput-boolean p2, p0, Lnet/flixster/android/lvi/LviActor;->smallThumbnail:Z

    .line 32
    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 36
    sget v0, Lnet/flixster/android/lvi/LviActor;->VIEW_TYPE_ACTOR:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 13
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 41
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;

    if-nez v2, :cond_1

    .line 42
    :cond_0
    invoke-static/range {p4 .. p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    .line 43
    .local v8, li:Landroid/view/LayoutInflater;
    const v2, 0x7f030017

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v8, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 45
    .end local v8           #li:Landroid/view/LayoutInflater;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;

    .line 46
    .local v11, viewHolder:Lnet/flixster/android/lvi/LviActor$ActorViewHolder;
    if-nez v11, :cond_2

    .line 47
    new-instance v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;

    .end local v11           #viewHolder:Lnet/flixster/android/lvi/LviActor$ActorViewHolder;
    const/4 v2, 0x0

    invoke-direct {v11, v2}, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;-><init>(Lnet/flixster/android/lvi/LviActor$ActorViewHolder;)V

    .line 48
    .restart local v11       #viewHolder:Lnet/flixster/android/lvi/LviActor$ActorViewHolder;
    const v2, 0x7f070028

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    .line 49
    const v2, 0x7f07002c

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->nameView:Landroid/widget/TextView;

    .line 50
    const v2, 0x7f07002b

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    .line 51
    invoke-virtual {p2, v11}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 54
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    if-eqz v2, :cond_5

    .line 55
    iget-object v2, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    iget-object v10, v2, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    .line 56
    .local v10, name:Ljava/lang/String;
    iget-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->nameView:Landroid/widget/TextView;

    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 58
    iget-boolean v2, p0, Lnet/flixster/android/lvi/LviActor;->smallThumbnail:Z

    if-eqz v2, :cond_3

    .line 59
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0033

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 60
    .local v12, widthPx:I
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 61
    .local v9, marginPx:I
    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v7, v12, v12}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 62
    .local v7, layout:Landroid/widget/RelativeLayout$LayoutParams;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v7, v2, v3, v9, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 63
    iget-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v7}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 65
    .end local v7           #layout:Landroid/widget/RelativeLayout$LayoutParams;
    .end local v9           #marginPx:I
    .end local v12           #widthPx:I
    :cond_3
    iget-object v2, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    iget-object v2, v2, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_6

    .line 66
    iget-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    iget-object v3, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->bitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 78
    .end local p4
    :cond_4
    :goto_0
    iget-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    .line 79
    iget-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f030017

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setId(I)V

    .line 80
    iget-object v2, p0, Lnet/flixster/android/lvi/LviActor;->mActorClick:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_5

    .line 81
    iget-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->actorLayout:Landroid/widget/RelativeLayout;

    iget-object v3, p0, Lnet/flixster/android/lvi/LviActor;->mActorClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    .end local v10           #name:Ljava/lang/String;
    :cond_5
    return-object p2

    .line 67
    .restart local v10       #name:Ljava/lang/String;
    .restart local p4
    :cond_6
    iget-object v2, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    iget-object v2, v2, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 68
    iget-object v2, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    const v3, 0x7f020151

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 69
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/flixster/android/utils/ImageTask;

    if-eqz v2, :cond_7

    .line 70
    new-instance v1, Lnet/flixster/android/model/ImageOrder;

    const/4 v2, 0x4

    iget-object v3, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    iget-object v4, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    iget-object v4, v4, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    .line 71
    iget-object v5, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    move-object/from16 v6, p5

    .line 70
    invoke-direct/range {v1 .. v6}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 72
    .local v1, imageOrder:Lnet/flixster/android/model/ImageOrder;
    check-cast p4, Lcom/flixster/android/utils/ImageTask;

    .end local p4
    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Lcom/flixster/android/utils/ImageTask;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    goto :goto_0

    .line 74
    .end local v1           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    .restart local p4
    :cond_7
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    iget-object v3, v3, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    .line 75
    new-instance v4, Lcom/flixster/android/activity/ImageViewHandler;

    iget-object v5, v11, Lnet/flixster/android/lvi/LviActor$ActorViewHolder;->imageView:Landroid/widget/ImageView;

    iget-object v6, p0, Lnet/flixster/android/lvi/LviActor;->mActor:Lnet/flixster/android/model/Actor;

    iget-object v6, v6, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lcom/flixster/android/activity/ImageViewHandler;-><init>(Landroid/widget/ImageView;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v2, v3, v4}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    goto :goto_0
.end method
