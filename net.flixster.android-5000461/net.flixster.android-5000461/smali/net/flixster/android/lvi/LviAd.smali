.class public Lnet/flixster/android/lvi/LviAd;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviAd.java"


# instance fields
.field public dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

.field public mAdSlot:Ljava/lang/String;

.field private mAdView:Lnet/flixster/android/ads/AdView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public destroy()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lnet/flixster/android/lvi/LviAd;->mAdView:Lnet/flixster/android/ads/AdView;

    if-eqz v0, :cond_0

    .line 38
    iget-object v0, p0, Lnet/flixster/android/lvi/LviAd;->mAdView:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->destroy()V

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/lvi/LviAd;->mAdView:Lnet/flixster/android/ads/AdView;

    .line 41
    :cond_0
    return-void
.end method

.method public getItemViewType()I
    .locals 1

    .prologue
    .line 21
    sget v0, Lnet/flixster/android/lvi/LviAd;->VIEW_TYPE_ADVIEW:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 3
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 26
    const-string v0, "FlxAd"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LviAd.getView position:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    iget-object v0, p0, Lnet/flixster/android/lvi/LviAd;->mAdView:Lnet/flixster/android/ads/AdView;

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Lnet/flixster/android/ads/AdView;

    iget-object v1, p0, Lnet/flixster/android/lvi/LviAd;->mAdSlot:Ljava/lang/String;

    invoke-direct {v0, p4, v1}, Lnet/flixster/android/ads/AdView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/lvi/LviAd;->mAdView:Lnet/flixster/android/ads/AdView;

    .line 29
    iget-object v0, p0, Lnet/flixster/android/lvi/LviAd;->mAdView:Lnet/flixster/android/ads/AdView;

    iget-object v1, p0, Lnet/flixster/android/lvi/LviAd;->dfpTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    iput-object v1, v0, Lnet/flixster/android/ads/AdView;->dfpCustomTarget:Lnet/flixster/android/ads/model/DfpAd$DfpCustomTarget;

    .line 30
    iget-object v0, p0, Lnet/flixster/android/lvi/LviAd;->mAdView:Lnet/flixster/android/ads/AdView;

    invoke-virtual {v0}, Lnet/flixster/android/ads/AdView;->refreshAds()V

    .line 32
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/lvi/LviAd;->mAdView:Lnet/flixster/android/ads/AdView;

    return-object v0
.end method
