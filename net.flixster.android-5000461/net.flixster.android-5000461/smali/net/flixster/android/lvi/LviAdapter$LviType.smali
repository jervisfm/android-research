.class public final enum Lnet/flixster/android/lvi/LviAdapter$LviType;
.super Ljava/lang/Enum;
.source "LviAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/lvi/LviAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "LviType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lnet/flixster/android/lvi/LviAdapter$LviType;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACTOR:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum ADVIEW:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum CATEGORY:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum DATE_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum DETAILSCAP:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum DIVIDER:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field private static final synthetic ENUM$VALUES:[Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum FOOTER:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum ICON_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum LOAD_MORE:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum LOCATION_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum MESSAGE:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum MOVIE:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum NEWS_ITEM:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum PAGEHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum REFRESHBAR:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum SHOWTIMES:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum SUBHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum THEATER:Lnet/flixster/android/lvi/LviAdapter$LviType;

.field public static final enum USER_PROFILE:Lnet/flixster/android/lvi/LviAdapter$LviType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "MOVIE"

    invoke-direct {v0, v1, v3}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->MOVIE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "THEATER"

    invoke-direct {v0, v1, v4}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->THEATER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "DETAILSCAP"

    invoke-direct {v0, v1, v5}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->DETAILSCAP:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "SHOWTIMES"

    invoke-direct {v0, v1, v6}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->SHOWTIMES:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "NEWS_ITEM"

    invoke-direct {v0, v1, v7}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->NEWS_ITEM:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "CATEGORY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->CATEGORY:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "MESSAGE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->MESSAGE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "USER_PROFILE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->USER_PROFILE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "ACTOR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->ACTOR:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "ADVIEW"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->ADVIEW:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "LOCATION_PANEL"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->LOCATION_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "DATE_PANEL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->DATE_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "ICON_PANEL"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->ICON_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "PAGEHEADER"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->PAGEHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "SUBHEADER"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->SUBHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "FOOTER"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->FOOTER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "DIVIDER"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->DIVIDER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "REFRESHBAR"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->REFRESHBAR:Lnet/flixster/android/lvi/LviAdapter$LviType;

    new-instance v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    const-string v1, "LOAD_MORE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviAdapter$LviType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->LOAD_MORE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    .line 16
    const/16 v0, 0x13

    new-array v0, v0, [Lnet/flixster/android/lvi/LviAdapter$LviType;

    sget-object v1, Lnet/flixster/android/lvi/LviAdapter$LviType;->MOVIE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v1, v0, v3

    sget-object v1, Lnet/flixster/android/lvi/LviAdapter$LviType;->THEATER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v1, v0, v4

    sget-object v1, Lnet/flixster/android/lvi/LviAdapter$LviType;->DETAILSCAP:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v1, v0, v5

    sget-object v1, Lnet/flixster/android/lvi/LviAdapter$LviType;->SHOWTIMES:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v1, v0, v6

    sget-object v1, Lnet/flixster/android/lvi/LviAdapter$LviType;->NEWS_ITEM:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->CATEGORY:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->MESSAGE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->USER_PROFILE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->ACTOR:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->ADVIEW:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->LOCATION_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->DATE_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->ICON_PANEL:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->PAGEHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->SUBHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->FOOTER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->DIVIDER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->REFRESHBAR:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lnet/flixster/android/lvi/LviAdapter$LviType;->LOAD_MORE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    aput-object v2, v0, v1

    sput-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->ENUM$VALUES:[Lnet/flixster/android/lvi/LviAdapter$LviType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/flixster/android/lvi/LviAdapter$LviType;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/lvi/LviAdapter$LviType;

    return-object v0
.end method

.method public static values()[Lnet/flixster/android/lvi/LviAdapter$LviType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->ENUM$VALUES:[Lnet/flixster/android/lvi/LviAdapter$LviType;

    array-length v1, v0

    new-array v2, v1, [Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
