.class public Lnet/flixster/android/lvi/LviCategory;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviCategory.java"


# instance fields
.field public mCategory:Ljava/lang/String;

.field public mFilter:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 18
    sget v0, Lnet/flixster/android/lvi/LviCategory;->VIEW_TYPE_CATEGORY:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 3
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    const/16 v2, 0x14

    .line 24
    if-nez p2, :cond_0

    .line 25
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 26
    .local v0, categoryTextView:Landroid/widget/TextView;
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 27
    const v1, 0x7f020075

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 28
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 30
    const v1, 0x7f0d006f

    invoke-virtual {v0, p4, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 35
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/lvi/LviCategory;->mCategory:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 36
    return-object v0

    .end local v0           #categoryTextView:Landroid/widget/TextView;
    :cond_0
    move-object v0, p2

    .line 32
    check-cast v0, Landroid/widget/TextView;

    .restart local v0       #categoryTextView:Landroid/widget/TextView;
    goto :goto_0
.end method
