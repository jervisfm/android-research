.class public Lnet/flixster/android/lvi/LviDatePanel;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviDatePanel.java"


# instance fields
.field public mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lnet/flixster/android/lvi/LviDatePanel;->VIEW_TYPE_DATEPANEL:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 16
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 33
    const-string v12, "FlxMain"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "LviDatePanel.getView position:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " convertView:"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    if-nez p2, :cond_0

    .line 36
    invoke-static/range {p4 .. p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 37
    .local v5, li:Landroid/view/LayoutInflater;
    const v12, 0x7f030044

    const/4 v13, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v5, v12, v0, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 40
    .end local v5           #li:Landroid/view/LayoutInflater;
    :cond_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getShowtimesDate()Ljava/util/Date;

    move-result-object v2

    .line 41
    .local v2, d:Ljava/util/Date;
    const-string v12, "FlxMain"

    new-instance v13, Ljava/lang/StringBuilder;

    const-string v14, "LviDatePanel.getView showtimes date:"

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 43
    .local v1, c:Ljava/util/Calendar;
    const/4 v12, 0x0

    invoke-virtual {v1, v12}, Ljava/util/Calendar;->setLenient(Z)V

    .line 45
    const/16 v12, 0xb

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 46
    const/16 v12, 0xc

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 47
    const/16 v12, 0xd

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 48
    const/16 v12, 0xe

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 49
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    .line 51
    .local v10, todayEpoch:J
    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 52
    const/16 v12, 0xb

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 53
    const/16 v12, 0xc

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 54
    const/16 v12, 0xd

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 55
    const/16 v12, 0xe

    const/4 v13, 0x0

    invoke-virtual {v1, v12, v13}, Ljava/util/Calendar;->set(II)V

    .line 56
    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v7

    .line 57
    .local v7, targetEpoch:J
    sub-long v12, v7, v10

    const-wide/32 v14, 0xa4cb80

    add-long/2addr v12, v14

    const-wide/32 v14, 0x5265c00

    div-long v3, v12, v14

    .line 58
    .local v3, days:J
    invoke-static/range {p4 .. p4}, Lnet/flixster/android/util/DialogUtils;->getShowtimesDateOptions(Landroid/content/Context;)[Ljava/lang/CharSequence;

    move-result-object v6

    .line 60
    .local v6, showtimeOptions:[Ljava/lang/CharSequence;
    const v12, 0x7f0700f1

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 61
    .local v9, textView:Landroid/widget/TextView;
    const-wide/16 v12, 0x0

    cmp-long v12, v3, v12

    if-ltz v12, :cond_1

    array-length v12, v6

    int-to-long v12, v12

    cmp-long v12, v12, v3

    if-lez v12, :cond_1

    .line 62
    long-to-int v12, v3

    aget-object v12, v6, v12

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lnet/flixster/android/lvi/LviDatePanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p2

    invoke-virtual {v0, v12}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    return-object p2
.end method
