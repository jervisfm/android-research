.class public Lnet/flixster/android/lvi/LviFooter;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviFooter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;
    }
.end annotation


# instance fields
.field public mMovie:Lnet/flixster/android/model/Movie;

.field public mTrailerClick:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 22
    sget v0, Lnet/flixster/android/lvi/LviFooter;->VIEW_TYPE_FOOTER:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 6
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 28
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v2

    .line 31
    .local v2, movieRatingType:I
    if-eqz p2, :cond_0

    .line 32
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;

    .line 47
    .local v0, holder:Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;
    :goto_0
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 49
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 50
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerFresh:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 51
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerRotten:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 52
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerPopcorn:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 53
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerSpilled:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 54
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerWts:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 55
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerTitleFlixster:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    :goto_1
    return-object p2

    .line 34
    .end local v0           #holder:Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;
    :cond_0
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 35
    .local v1, li:Landroid/view/LayoutInflater;
    const v3, 0x7f030020

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 36
    new-instance v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;

    invoke-direct {v0}, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;-><init>()V

    .line 37
    .restart local v0       #holder:Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;
    const v3, 0x7f07005a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerTitle:Landroid/widget/TextView;

    .line 38
    const v3, 0x7f07005d

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerTitleFlixster:Landroid/widget/TextView;

    .line 39
    const v3, 0x7f07005b

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerFresh:Landroid/widget/TextView;

    .line 40
    const v3, 0x7f07005c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerRotten:Landroid/widget/TextView;

    .line 41
    const v3, 0x7f07005e

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerPopcorn:Landroid/widget/TextView;

    .line 42
    const v3, 0x7f07005f

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerSpilled:Landroid/widget/TextView;

    .line 43
    const v3, 0x7f070060

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerWts:Landroid/widget/TextView;

    .line 44
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 57
    .end local v1           #li:Landroid/view/LayoutInflater;
    :cond_1
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerTitleFlixster:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 58
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerWts:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 59
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerPopcorn:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 60
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerSpilled:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 61
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 62
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerFresh:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 63
    iget-object v3, v0, Lnet/flixster/android/lvi/LviFooter$FooterItemHolder;->footerRotten:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method
