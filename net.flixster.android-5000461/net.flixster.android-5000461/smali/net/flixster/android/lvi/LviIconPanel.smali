.class public Lnet/flixster/android/lvi/LviIconPanel;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviIconPanel.java"


# instance fields
.field public mDrawableResource:I

.field public mLabel:Ljava/lang/String;

.field public mOnClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 15
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    .line 13
    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 22
    sget v0, Lnet/flixster/android/lvi/LviIconPanel;->VIEW_TYPE_ICONPANEL:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 28
    if-nez p2, :cond_2

    .line 29
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 30
    .local v1, li:Landroid/view/LayoutInflater;
    const v3, 0x7f030046

    invoke-virtual {v1, v3, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 35
    .end local v1           #li:Landroid/view/LayoutInflater;
    .local v2, panelTextView:Landroid/widget/TextView;
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 36
    iget-object v3, p0, Lnet/flixster/android/lvi/LviIconPanel;->mLabel:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 39
    :cond_0
    iget v3, p0, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    if-eqz v3, :cond_1

    .line 40
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lnet/flixster/android/lvi/LviIconPanel;->mDrawableResource:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 41
    .local v0, icon:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 42
    invoke-virtual {v2, v0, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 45
    .end local v0           #icon:Landroid/graphics/drawable/Drawable;
    :cond_1
    iget-object v3, p0, Lnet/flixster/android/lvi/LviIconPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    return-object v2

    .end local v2           #panelTextView:Landroid/widget/TextView;
    :cond_2
    move-object v2, p2

    .line 32
    check-cast v2, Landroid/widget/TextView;

    .restart local v2       #panelTextView:Landroid/widget/TextView;
    goto :goto_0
.end method
