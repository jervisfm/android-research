.class public final enum Lnet/flixster/android/lvi/LviLoadMore$Type;
.super Ljava/lang/Enum;
.source "LviLoadMore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/lvi/LviLoadMore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Type"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lnet/flixster/android/lvi/LviLoadMore$Type;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ACTOR:Lnet/flixster/android/lvi/LviLoadMore$Type;

.field private static final synthetic ENUM$VALUES:[Lnet/flixster/android/lvi/LviLoadMore$Type;

.field public static final enum MOVIE:Lnet/flixster/android/lvi/LviLoadMore$Type;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lnet/flixster/android/lvi/LviLoadMore$Type;

    const-string v1, "MOVIE"

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/lvi/LviLoadMore$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviLoadMore$Type;->MOVIE:Lnet/flixster/android/lvi/LviLoadMore$Type;

    new-instance v0, Lnet/flixster/android/lvi/LviLoadMore$Type;

    const-string v1, "ACTOR"

    invoke-direct {v0, v1, v3}, Lnet/flixster/android/lvi/LviLoadMore$Type;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/lvi/LviLoadMore$Type;->ACTOR:Lnet/flixster/android/lvi/LviLoadMore$Type;

    .line 12
    const/4 v0, 0x2

    new-array v0, v0, [Lnet/flixster/android/lvi/LviLoadMore$Type;

    sget-object v1, Lnet/flixster/android/lvi/LviLoadMore$Type;->MOVIE:Lnet/flixster/android/lvi/LviLoadMore$Type;

    aput-object v1, v0, v2

    sget-object v1, Lnet/flixster/android/lvi/LviLoadMore$Type;->ACTOR:Lnet/flixster/android/lvi/LviLoadMore$Type;

    aput-object v1, v0, v3

    sput-object v0, Lnet/flixster/android/lvi/LviLoadMore$Type;->ENUM$VALUES:[Lnet/flixster/android/lvi/LviLoadMore$Type;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/flixster/android/lvi/LviLoadMore$Type;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lnet/flixster/android/lvi/LviLoadMore$Type;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/lvi/LviLoadMore$Type;

    return-object v0
.end method

.method public static values()[Lnet/flixster/android/lvi/LviLoadMore$Type;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lnet/flixster/android/lvi/LviLoadMore$Type;->ENUM$VALUES:[Lnet/flixster/android/lvi/LviLoadMore$Type;

    array-length v1, v0

    new-array v2, v1, [Lnet/flixster/android/lvi/LviLoadMore$Type;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
