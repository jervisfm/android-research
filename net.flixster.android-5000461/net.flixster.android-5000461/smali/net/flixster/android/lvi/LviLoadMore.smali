.class public Lnet/flixster/android/lvi/LviLoadMore;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviLoadMore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/lvi/LviLoadMore$Type;
    }
.end annotation


# instance fields
.field private final subtitleId:Ljava/lang/String;

.field private final titleId:Ljava/lang/String;

.field public final type:Lnet/flixster/android/lvi/LviLoadMore$Type;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lnet/flixster/android/lvi/LviLoadMore$Type;)V
    .locals 0
    .parameter "titleId"
    .parameter "subtitleId"
    .parameter "type"

    .prologue
    .line 19
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 20
    iput-object p1, p0, Lnet/flixster/android/lvi/LviLoadMore;->titleId:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lnet/flixster/android/lvi/LviLoadMore;->subtitleId:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lnet/flixster/android/lvi/LviLoadMore;->type:Lnet/flixster/android/lvi/LviLoadMore$Type;

    .line 23
    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->LOAD_MORE:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 4
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 27
    if-nez p2, :cond_0

    .line 28
    new-instance p2, Lcom/flixster/android/view/LoadMore;

    .end local p2
    invoke-direct {p2, p4}, Lcom/flixster/android/view/LoadMore;-><init>(Landroid/content/Context;)V

    .restart local p2
    :cond_0
    move-object v0, p2

    .line 30
    check-cast v0, Lcom/flixster/android/view/LoadMore;

    iget-object v1, p0, Lnet/flixster/android/lvi/LviLoadMore;->titleId:Ljava/lang/String;

    iget-object v2, p0, Lnet/flixster/android/lvi/LviLoadMore;->subtitleId:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/flixster/android/view/LoadMore;->load(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 31
    return-object p2
.end method
