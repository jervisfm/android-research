.class Lnet/flixster/android/lvi/LviLocationPanel$1;
.super Landroid/os/Handler;
.source "LviLocationPanel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/lvi/LviLocationPanel;->getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/lvi/LviLocationPanel;

.field private final synthetic val$activity:Landroid/content/Context;


# direct methods
.method constructor <init>(Lnet/flixster/android/lvi/LviLocationPanel;Landroid/content/Context;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/lvi/LviLocationPanel$1;->this$0:Lnet/flixster/android/lvi/LviLocationPanel;

    iput-object p2, p0, Lnet/flixster/android/lvi/LviLocationPanel$1;->val$activity:Landroid/content/Context;

    .line 40
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .parameter "msg"

    .prologue
    .line 42
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 43
    .local v0, currentLocation:Ljava/lang/String;
    iget-object v2, p0, Lnet/flixster/android/lvi/LviLocationPanel$1;->this$0:Lnet/flixster/android/lvi/LviLocationPanel;

    #getter for: Lnet/flixster/android/lvi/LviLocationPanel;->mConvertView:Landroid/view/View;
    invoke-static {v2}, Lnet/flixster/android/lvi/LviLocationPanel;->access$0(Lnet/flixster/android/lvi/LviLocationPanel;)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0700f8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 44
    .local v1, textView:Landroid/widget/TextView;
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 45
    iget-object v2, p0, Lnet/flixster/android/lvi/LviLocationPanel$1;->val$activity:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 46
    const v3, 0x7f0c00db

    .line 45
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    const/high16 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    const/high16 v2, -0x100

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
