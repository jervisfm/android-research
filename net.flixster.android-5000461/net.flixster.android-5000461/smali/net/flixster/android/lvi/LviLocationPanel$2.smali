.class Lnet/flixster/android/lvi/LviLocationPanel$2;
.super Ljava/lang/Object;
.source "LviLocationPanel.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/lvi/LviLocationPanel;->getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/lvi/LviLocationPanel;

.field private final synthetic val$getCurrentLocationHandler:Landroid/os/Handler;


# direct methods
.method constructor <init>(Lnet/flixster/android/lvi/LviLocationPanel;Landroid/os/Handler;)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/lvi/LviLocationPanel$2;->this$0:Lnet/flixster/android/lvi/LviLocationPanel;

    iput-object p2, p0, Lnet/flixster/android/lvi/LviLocationPanel$2;->val$getCurrentLocationHandler:Landroid/os/Handler;

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 58
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getCurrentLocationDisplay()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, currentLocation:Ljava/lang/String;
    iget-object v1, p0, Lnet/flixster/android/lvi/LviLocationPanel$2;->val$getCurrentLocationHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v2, v3, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 60
    return-void
.end method
