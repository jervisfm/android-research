.class public Lnet/flixster/android/lvi/LviLocationPanel;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviLocationPanel.java"


# instance fields
.field private mConvertView:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/lvi/LviLocationPanel;)Landroid/view/View;
    .locals 1
    .parameter

    .prologue
    .line 20
    iget-object v0, p0, Lnet/flixster/android/lvi/LviLocationPanel;->mConvertView:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lnet/flixster/android/lvi/LviLocationPanel;->VIEW_TYPE_LOCATIONPANEL:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    const/4 v5, 0x0

    .line 31
    if-nez p2, :cond_0

    .line 32
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 33
    .local v2, li:Landroid/view/LayoutInflater;
    const v4, 0x7f030047

    invoke-virtual {v2, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 35
    .end local v2           #li:Landroid/view/LayoutInflater;
    :cond_0
    iput-object p2, p0, Lnet/flixster/android/lvi/LviLocationPanel;->mConvertView:Landroid/view/View;

    .line 37
    const v4, 0x7f0700f8

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 39
    .local v3, textView:Landroid/widget/TextView;
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUseLocationServiceFlag()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 40
    new-instance v1, Lnet/flixster/android/lvi/LviLocationPanel$1;

    invoke-direct {v1, p0, p4}, Lnet/flixster/android/lvi/LviLocationPanel$1;-><init>(Lnet/flixster/android/lvi/LviLocationPanel;Landroid/content/Context;)V

    .line 54
    .local v1, getCurrentLocationHandler:Landroid/os/Handler;
    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    invoke-static {}, Lcom/flixster/android/utils/WorkerThreads;->instance()Lcom/flixster/android/utils/WorkerThreads;

    move-result-object v4

    new-instance v5, Lnet/flixster/android/lvi/LviLocationPanel$2;

    invoke-direct {v5, p0, v1}, Lnet/flixster/android/lvi/LviLocationPanel$2;-><init>(Lnet/flixster/android/lvi/LviLocationPanel;Landroid/os/Handler;)V

    invoke-virtual {v4, v5}, Lcom/flixster/android/utils/WorkerThreads;->invokeLater(Ljava/lang/Runnable;)V

    .line 71
    .end local v1           #getCurrentLocationHandler:Landroid/os/Handler;
    :goto_0
    :try_start_0
    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    .line 72
    const-string v5, "getStartSettingsClickListener"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, p4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View$OnClickListener;

    .line 71
    invoke-virtual {p2, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :goto_1
    return-object p2

    .line 62
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    const-string v4, ""

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 63
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    const/high16 v4, -0x100

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 66
    :cond_2
    const v4, 0x7f0c00db

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 67
    const/high16 v4, -0x1

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 73
    :catch_0
    move-exception v0

    .line 74
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "FlxMain"

    const-string v5, "getStartSettingsClickListener not found"

    invoke-static {v4, v5, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
