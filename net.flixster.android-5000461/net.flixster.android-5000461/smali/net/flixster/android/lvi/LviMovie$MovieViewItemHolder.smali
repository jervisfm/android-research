.class public Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;
.super Ljava/lang/Object;
.source "LviMovie.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/lvi/LviMovie;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MovieViewItemHolder"
.end annotation


# instance fields
.field friendFlixScore:Landroid/widget/TextView;

.field friendRtScore:Landroid/widget/TextView;

.field movieActors:Landroid/widget/TextView;

.field movieDetailsLayout:Landroid/widget/LinearLayout;

.field movieDownloadStatus:Landroid/widget/TextView;

.field movieFlixScore:Landroid/widget/TextView;

.field movieHeader:Landroid/widget/TextView;

.field movieItem:Landroid/widget/RelativeLayout;

.field movieMeta:Landroid/widget/TextView;

.field moviePlayIcon:Landroid/widget/ImageView;

.field moviePoster:Landroid/widget/ImageView;

.field movieRelease:Landroid/widget/TextView;

.field public movieReview:Lnet/flixster/android/model/Review;

.field movieRtScore:Landroid/widget/TextView;

.field movieTitle:Landroid/widget/TextView;

.field final synthetic this$0:Lnet/flixster/android/lvi/LviMovie;


# direct methods
.method public constructor <init>(Lnet/flixster/android/lvi/LviMovie;)V
    .locals 1
    .parameter

    .prologue
    .line 420
    iput-object p1, p0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->this$0:Lnet/flixster/android/lvi/LviMovie;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 437
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieReview:Lnet/flixster/android/model/Review;

    return-void
.end method
