.class public Lnet/flixster/android/lvi/LviMovie;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviMovie.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;
    }
.end annotation


# static fields
.field public static final ENABLE_SPILLED:Z = true

.field public static final FORM_CAP:I = 0x1

.field public static final FORM_ITEM:I


# instance fields
.field public mDetailsClick:Landroid/view/View$OnClickListener;

.field private mFlixScore:Ljava/lang/String;

.field public mForm:I

.field private mGross:Ljava/lang/String;

.field public mIsCollected:Z

.field public mIsFeatured:Z

.field private mIsReleased:Ljava/lang/Boolean;

.field public mMovie:Lnet/flixster/android/model/Movie;

.field private mResources:Landroid/content/res/Resources;

.field public mReview:Lnet/flixster/android/model/Review;

.field private mRtScore:Ljava/lang/String;

.field public mTrailerClick:Landroid/view/View$OnClickListener;

.field public right:Lnet/flixster/android/model/LockerRight;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 45
    iput-boolean v1, p0, Lnet/flixster/android/lvi/LviMovie;->mIsCollected:Z

    iput-boolean v1, p0, Lnet/flixster/android/lvi/LviMovie;->mIsFeatured:Z

    .line 46
    iput v1, p0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    .line 49
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mIsReleased:Ljava/lang/Boolean;

    .line 50
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    .line 51
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mFlixScore:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mRtScore:Ljava/lang/String;

    .line 53
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mGross:Ljava/lang/String;

    .line 56
    return-void
.end method

.method public constructor <init>(Lnet/flixster/android/model/Movie;)V
    .locals 2
    .parameter "movie"

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 45
    iput-boolean v1, p0, Lnet/flixster/android/lvi/LviMovie;->mIsCollected:Z

    iput-boolean v1, p0, Lnet/flixster/android/lvi/LviMovie;->mIsFeatured:Z

    .line 46
    iput v1, p0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    .line 49
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mIsReleased:Ljava/lang/Boolean;

    .line 50
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    .line 51
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mFlixScore:Ljava/lang/String;

    .line 52
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mRtScore:Ljava/lang/String;

    .line 53
    iput-object v0, p0, Lnet/flixster/android/lvi/LviMovie;->mGross:Ljava/lang/String;

    .line 59
    iput-object p1, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    .line 60
    return-void
.end method

.method private setDownloadControls(Landroid/widget/TextView;)V
    .locals 6
    .parameter "downloadPanel"

    .prologue
    const/4 v1, 0x0

    .line 405
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    if-nez v2, :cond_0

    .line 418
    :goto_0
    return-void

    .line 408
    :cond_0
    const/4 v0, 0x0

    .line 409
    .local v0, state:Ljava/lang/String;
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-static {v2}, Lcom/flixster/android/net/DownloadHelper;->isMovieDownloadInProgress(Lnet/flixster/android/model/LockerRight;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 410
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0c018f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 416
    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 417
    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v1, 0x8

    :cond_1
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 411
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-static {v2}, Lcom/flixster/android/net/DownloadHelper;->isDownloaded(Lnet/flixster/android/model/LockerRight;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 412
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0c0190

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v5}, Lnet/flixster/android/model/LockerRight;->getDownloadedAssetSize()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 414
    :cond_3
    const-string v0, ""

    goto :goto_1
.end method

.method private setFlixScore(Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;)V
    .locals 10
    .parameter "viewHolder"

    .prologue
    const/4 v9, 0x0

    const/4 v6, 0x0

    .line 351
    iget-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v8, "popcornScore"

    invoke-virtual {v7, v8}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 352
    iget-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v8, "popcornScore"

    invoke-virtual {v7, v8}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 353
    .local v1, flixScore:I
    const/16 v7, 0x3c

    if-ge v1, v7, :cond_1

    const/4 v3, 0x1

    .line 354
    .local v3, isSpilled:Z
    :goto_0
    iget-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mFlixScore:Ljava/lang/String;

    if-nez v7, :cond_0

    .line 355
    iget v7, p0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    if-nez v7, :cond_2

    .line 356
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mFlixScore:Ljava/lang/String;

    .line 363
    :cond_0
    :goto_1
    iget-object v7, p1, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieFlixScore:Landroid/widget/TextView;

    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mFlixScore:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 365
    iget-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mIsReleased:Ljava/lang/Boolean;

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_4

    const v2, 0x7f0200f6

    .line 367
    .local v2, iconId:I
    :goto_2
    iget-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 368
    .local v0, flixIcon:Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    invoke-virtual {v0, v6, v6, v7, v8}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 369
    iget-object v6, p1, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieFlixScore:Landroid/widget/TextView;

    invoke-virtual {v6, v0, v9, v9, v9}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 373
    .end local v0           #flixIcon:Landroid/graphics/drawable/Drawable;
    .end local v1           #flixScore:I
    .end local v2           #iconId:I
    .end local v3           #isSpilled:Z
    :goto_3
    return-void

    .restart local v1       #flixScore:I
    :cond_1
    move v3, v6

    .line 353
    goto :goto_0

    .line 358
    .restart local v3       #isSpilled:Z
    :cond_2
    iget-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0c0163

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 359
    .local v4, liked:Ljava/lang/String;
    iget-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    const v8, 0x7f0c0164

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 360
    .local v5, wts:Ljava/lang/String;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mIsReleased:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_3

    .end local v4           #liked:Ljava/lang/String;
    :goto_4
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lnet/flixster/android/lvi/LviMovie;->mFlixScore:Ljava/lang/String;

    goto :goto_1

    .restart local v4       #liked:Ljava/lang/String;
    :cond_3
    move-object v4, v5

    goto :goto_4

    .line 365
    .end local v4           #liked:Ljava/lang/String;
    .end local v5           #wts:Ljava/lang/String;
    :cond_4
    if-eqz v3, :cond_5

    const v2, 0x7f0200ef

    goto :goto_2

    .line 366
    :cond_5
    const v2, 0x7f0200ea

    goto :goto_2

    .line 371
    .end local v1           #flixScore:I
    .end local v3           #isSpilled:Z
    :cond_6
    iget-object v6, p1, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieFlixScore:Landroid/widget/TextView;

    const/16 v7, 0x8

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method

.method private setFriendScore(Landroid/widget/TextView;Landroid/content/Context;)V
    .locals 12
    .parameter "view"
    .parameter "activity"

    .prologue
    .line 295
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v2

    .line 296
    .local v2, movieRatingType:I
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatformUsername()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 297
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 298
    .local v4, resources:Landroid/content/res/Resources;
    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "FRIENDS_RATED_COUNT"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 299
    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "FRIENDS_RATED_COUNT"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-lez v8, :cond_1

    .line 301
    const v8, 0x7f0200eb

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 302
    .local v5, reviewIcon:Landroid/graphics/drawable/Drawable;
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    invoke-virtual {v5, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 303
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v5, v8, v9, v10}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 304
    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "FRIENDS_RATED_COUNT"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 305
    .local v0, friendsRatedCount:I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 306
    .local v3, ratingCountBuilder:Ljava/lang/StringBuilder;
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 307
    const/4 v8, 0x1

    if-le v0, v8, :cond_0

    .line 308
    const v8, 0x7f0c0090

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 312
    :goto_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 313
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 348
    .end local v0           #friendsRatedCount:I
    .end local v3           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v4           #resources:Landroid/content/res/Resources;
    .end local v5           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :goto_1
    return-void

    .line 310
    .restart local v0       #friendsRatedCount:I
    .restart local v3       #ratingCountBuilder:Ljava/lang/StringBuilder;
    .restart local v4       #resources:Landroid/content/res/Resources;
    .restart local v5       #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_0
    const v8, 0x7f0c008f

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 315
    .end local v0           #friendsRatedCount:I
    .end local v3           #ratingCountBuilder:Ljava/lang/StringBuilder;
    .end local v5           #reviewIcon:Landroid/graphics/drawable/Drawable;
    :cond_1
    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "FRIENDS_WTS_COUNT"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 316
    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "FRIENDS_WTS_COUNT"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    if-lez v8, :cond_5

    .line 317
    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v9, "FRIENDS_WTS_COUNT"

    invoke-virtual {v8, v9}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 318
    .local v1, friendsWantToSeeCount:I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 319
    .local v6, wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    iget-object v8, p0, Lnet/flixster/android/lvi/LviMovie;->mIsReleased:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_3

    if-nez v2, :cond_3

    .line 320
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {p1, v8, v9, v10, v11}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 321
    const-string v8, "("

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    const/4 v8, 0x1

    if-le v1, v8, :cond_2

    .line 323
    const v8, 0x7f0c008e

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 327
    :goto_2
    const-string v8, ")"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 340
    :goto_3
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 325
    :cond_2
    const v8, 0x7f0c008d

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 329
    :cond_3
    const v8, 0x7f0200f7

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 331
    .local v7, wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    invoke-virtual {v7}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v11

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 332
    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {p1, v7, v8, v9, v10}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 333
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 334
    const/4 v8, 0x1

    if-le v1, v8, :cond_4

    .line 335
    const v8, 0x7f0c0094

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 337
    :cond_4
    const v8, 0x7f0c0093

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 343
    .end local v1           #friendsWantToSeeCount:I
    .end local v6           #wantToSeeCountBuilder:Ljava/lang/StringBuilder;
    .end local v7           #wantToSeeIcon:Landroid/graphics/drawable/Drawable;
    :cond_5
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 346
    .end local v4           #resources:Landroid/content/res/Resources;
    :cond_6
    const/16 v8, 0x8

    invoke-virtual {p1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private setRtScore(Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;)V
    .locals 8
    .parameter "viewHolder"

    .prologue
    const/16 v7, 0x3c

    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 376
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v3, "rottenTomatoes"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 378
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v3, "rottenTomatoes"

    invoke-virtual {v2, v3}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 380
    .local v0, rottenScore:I
    if-ge v0, v7, :cond_0

    iget-boolean v2, p0, Lnet/flixster/android/lvi/LviMovie;->mIsFeatured:Z

    if-eqz v2, :cond_0

    .line 381
    iget-object v2, p1, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRtScore:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 402
    .end local v0           #rottenScore:I
    :goto_0
    return-void

    .line 383
    .restart local v0       #rottenScore:I
    :cond_0
    if-ge v0, v7, :cond_2

    .line 384
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0200ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 388
    .local v1, scoreIcon:Landroid/graphics/drawable/Drawable;
    :goto_1
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v1, v5, v5, v2, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 389
    iget-object v2, p1, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRtScore:Landroid/widget/TextView;

    invoke-virtual {v2, v1, v4, v4, v4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 390
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mRtScore:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 391
    iget v2, p0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    if-nez v2, :cond_3

    .line 392
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mRtScore:Ljava/lang/String;

    .line 397
    :cond_1
    :goto_2
    iget-object v2, p1, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRtScore:Landroid/widget/TextView;

    iget-object v3, p0, Lnet/flixster/android/lvi/LviMovie;->mRtScore:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 386
    .end local v1           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f0200de

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .restart local v1       #scoreIcon:Landroid/graphics/drawable/Drawable;
    goto :goto_1

    .line 394
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0c0162

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/lvi/LviMovie;->mRtScore:Ljava/lang/String;

    goto :goto_2

    .line 400
    .end local v0           #rottenScore:I
    .end local v1           #scoreIcon:Landroid/graphics/drawable/Drawable;
    :cond_4
    iget-object v2, p1, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRtScore:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 64
    sget v0, Lnet/flixster/android/lvi/LviMovie;->VIEW_TYPE_MOVIE:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 24
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 69
    const-class v3, Lcom/flixster/android/view/FriendActivity;

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    if-eqz p3, :cond_b

    const/4 v11, 0x0

    .line 70
    .local v11, isNonLvi:Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    if-nez v3, :cond_0

    .line 71
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mResources:Landroid/content/res/Resources;

    .line 73
    :cond_0
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getMovieRatingType()I

    move-result v14

    .line 74
    .local v14, movieRatingType:I
    if-nez p2, :cond_1

    .line 75
    invoke-static/range {p4 .. p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    .line 76
    .local v12, li:Landroid/view/LayoutInflater;
    move-object/from16 v0, p0

    iget v3, v0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    if-nez v3, :cond_c

    .line 77
    const v3, 0x7f03005c

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v12, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 82
    .end local v12           #li:Landroid/view/LayoutInflater;
    :cond_1
    :goto_1
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;

    .line 83
    .local v19, viewHolder:Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;
    if-nez v19, :cond_3

    .line 84
    new-instance v19, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;

    .end local v19           #viewHolder:Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;
    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;-><init>(Lnet/flixster/android/lvi/LviMovie;)V

    .line 86
    .restart local v19       #viewHolder:Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;
    const v3, 0x7f070106

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePoster:Landroid/widget/ImageView;

    .line 87
    const v3, 0x7f070108

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieTitle:Landroid/widget/TextView;

    .line 88
    const v3, 0x7f07010c

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieMeta:Landroid/widget/TextView;

    .line 89
    const v3, 0x7f07010d

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRelease:Landroid/widget/TextView;

    .line 90
    const v3, 0x7f07010b

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieActors:Landroid/widget/TextView;

    .line 91
    const v3, 0x7f07010a

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRtScore:Landroid/widget/TextView;

    .line 92
    const v3, 0x7f070109

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieFlixScore:Landroid/widget/TextView;

    .line 93
    const v3, 0x7f070196

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieDownloadStatus:Landroid/widget/TextView;

    .line 95
    move-object/from16 v0, p0

    iget v3, v0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    if-nez v3, :cond_2

    .line 96
    const v3, 0x7f07012f

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    .line 97
    const v3, 0x7f070195

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendRtScore:Landroid/widget/TextView;

    .line 98
    const v3, 0x7f070193

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendFlixScore:Landroid/widget/TextView;

    .line 99
    const v3, 0x7f070130

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieDetailsLayout:Landroid/widget/LinearLayout;

    .line 100
    const v3, 0x7f070105

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieItem:Landroid/widget/RelativeLayout;

    .line 102
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mDetailsClick:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_2

    .line 103
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mDetailsClick:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    :cond_2
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 109
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mReview:Lnet/flixster/android/model/Review;

    move-object/from16 v0, v19

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieReview:Lnet/flixster/android/model/Review;

    .line 114
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "Opening This Week"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_d

    .line 115
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRelease:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v7, "theaterReleaseDate"

    invoke-virtual {v4, v7}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRelease:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 131
    :goto_2
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v3

    if-eqz v3, :cond_10

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v3, :cond_10

    invoke-static {}, Lcom/flixster/android/drm/Drm;->logic()Lcom/flixster/android/drm/PlaybackLogic;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-interface {v3, v4}, Lcom/flixster/android/drm/PlaybackLogic;->isAssetDownloadable(Lnet/flixster/android/model/LockerRight;)Z

    move-result v3

    if-eqz v3, :cond_10

    .line 132
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieDownloadStatus:Landroid/widget/TextView;

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lnet/flixster/android/lvi/LviMovie;->setDownloadControls(Landroid/widget/TextView;)V

    .line 133
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieDownloadStatus:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 138
    :cond_4
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    if-eqz v3, :cond_23

    .line 139
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mIsReleased:Ljava/lang/Boolean;

    if-nez v3, :cond_5

    .line 140
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v3

    if-eqz v3, :cond_11

    .line 141
    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v4}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v3

    if-nez v3, :cond_11

    const/4 v3, 0x0

    :goto_4
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 140
    move-object/from16 v0, p0

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mIsReleased:Ljava/lang/Boolean;

    .line 144
    :cond_5
    move-object/from16 v0, v19

    iget-object v6, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePoster:Landroid/widget/ImageView;

    .line 145
    .local v6, iv:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v3, :cond_15

    .line 146
    move-object/from16 v0, p0

    iget v3, v0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_13

    .line 147
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v3}, Lnet/flixster/android/model/LockerRight;->getProfilePoster()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_12

    .line 148
    const v3, 0x7f02014f

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 205
    :cond_6
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    if-eqz v3, :cond_1d

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v3}, Lnet/flixster/android/model/LockerRight;->getTitle()Ljava/lang/String;

    move-result-object v17

    .line 206
    .local v17, title:Ljava/lang/String;
    :goto_6
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieTitle:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 209
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 210
    .local v8, actors:Ljava/lang/String;
    if-eqz v8, :cond_7

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_1e

    .line 211
    :cond_7
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieActors:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 218
    :goto_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "meta"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 219
    .local v13, meta:Ljava/lang/String;
    if-eqz v13, :cond_1f

    .line 220
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieMeta:Landroid/widget/TextView;

    invoke-virtual {v3, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieMeta:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 226
    :goto_8
    move-object/from16 v0, p0

    iget v3, v0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    if-nez v3, :cond_22

    .line 228
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "high"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 229
    .local v18, trailerUrlString:Ljava/lang/String;
    if-eqz v18, :cond_8

    const-string v3, "http"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lnet/flixster/android/lvi/LviMovie;->mIsCollected:Z

    if-eqz v3, :cond_21

    .line 230
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    if-eqz v3, :cond_21

    .line 231
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    iget-object v3, v3, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v3}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_20

    .line 232
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 233
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 234
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 235
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mTrailerClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 244
    :goto_9
    packed-switch v14, :pswitch_data_0

    .line 278
    .end local v6           #iv:Landroid/widget/ImageView;
    .end local v8           #actors:Ljava/lang/String;
    .end local v13           #meta:Ljava/lang/String;
    .end local v17           #title:Ljava/lang/String;
    .end local v18           #trailerUrlString:Ljava/lang/String;
    :goto_a
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 281
    invoke-virtual/range {p0 .. p0}, Lnet/flixster/android/lvi/LviMovie;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_24

    .line 282
    const v3, 0x7f020070

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 290
    :cond_a
    :goto_b
    return-object p2

    .line 69
    .end local v11           #isNonLvi:Z
    .end local v14           #movieRatingType:I
    .end local v19           #viewHolder:Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;
    :cond_b
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 79
    .restart local v11       #isNonLvi:Z
    .restart local v12       #li:Landroid/view/LayoutInflater;
    .restart local v14       #movieRatingType:I
    :cond_c
    const v3, 0x7f03004e

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v12, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_1

    .line 117
    .end local v12           #li:Landroid/view/LayoutInflater;
    .restart local v19       #viewHolder:Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;
    :cond_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "Top Box Office"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "boxOffice"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 119
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    const-string v4, "boxOffice"

    invoke-virtual {v3, v4}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->floatValue()F

    move-result v3

    const v4, 0x49742400

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    .line 121
    .local v10, f:Ljava/lang/Float;
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mGross:Ljava/lang/String;

    if-nez v3, :cond_e

    .line 122
    const-string v3, "$%.1fM"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v10, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mGross:Ljava/lang/String;

    .line 124
    :cond_e
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRelease:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mGross:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 125
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRelease:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 127
    .end local v10           #f:Ljava/lang/Float;
    :cond_f
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRelease:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 134
    :cond_10
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieDownloadStatus:Landroid/widget/TextView;

    if-eqz v3, :cond_4

    .line 135
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieDownloadStatus:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 141
    :cond_11
    const/4 v3, 0x1

    goto/16 :goto_4

    .line 150
    .restart local v6       #iv:Landroid/widget/ImageView;
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v3, v6}, Lnet/flixster/android/model/LockerRight;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 151
    .local v9, bitmap:Landroid/graphics/Bitmap;
    if-eqz v9, :cond_6

    .line 152
    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 156
    .end local v9           #bitmap:Landroid/graphics/Bitmap;
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v3}, Lnet/flixster/android/model/LockerRight;->getThumbnailPoster()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_14

    .line 157
    const v3, 0x7f02014f

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    .line 159
    :cond_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->right:Lnet/flixster/android/model/LockerRight;

    invoke-virtual {v3, v6}, Lnet/flixster/android/model/LockerRight;->getThumbnailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 160
    .restart local v9       #bitmap:Landroid/graphics/Bitmap;
    if-eqz v9, :cond_6

    .line 161
    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 166
    .end local v9           #bitmap:Landroid/graphics/Bitmap;
    :cond_15
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getThumbnailPoster()Ljava/lang/String;

    move-result-object v5

    .line 167
    .local v5, thumbnailUrl:Ljava/lang/String;
    move-object/from16 v0, p0

    iget v3, v0, Lnet/flixster/android/lvi/LviMovie;->mForm:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_17

    .line 168
    if-nez v5, :cond_16

    .line 169
    const v3, 0x7f02014f

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    .line 171
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3, v6}, Lnet/flixster/android/model/Movie;->getThumbnailBackedProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 172
    .restart local v9       #bitmap:Landroid/graphics/Bitmap;
    if-eqz v9, :cond_6

    .line 173
    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 177
    .end local v9           #bitmap:Landroid/graphics/Bitmap;
    :cond_17
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getSoftThumbnail()Landroid/graphics/Bitmap;

    move-result-object v9

    .line 178
    .restart local v9       #bitmap:Landroid/graphics/Bitmap;
    if-eqz v9, :cond_18

    .line 179
    invoke-virtual {v6, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_5

    .line 180
    :cond_18
    if-nez v5, :cond_19

    .line 181
    const v3, 0x7f02014f

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    .line 183
    :cond_19
    const v3, 0x7f020154

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 184
    if-eqz v11, :cond_1a

    .line 185
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v3, v5, v0}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    goto/16 :goto_5

    .line 187
    :cond_1a
    move-object/from16 v0, p4

    instance-of v3, v0, Lcom/flixster/android/utils/ImageTask;

    if-eqz v3, :cond_1c

    .line 189
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v15

    .line 190
    .local v15, timeStamp:J
    long-to-double v3, v15

    move-object/from16 v0, p0

    iget-object v7, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v7}, Lnet/flixster/android/model/Movie;->getThumbOrderStamp()J

    move-result-wide v20

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v20, v0

    const-wide v22, 0x40a7700000000000L

    add-double v20, v20, v22

    cmpl-double v3, v3, v20

    if-lez v3, :cond_1b

    .line 191
    new-instance v2, Lnet/flixster/android/model/ImageOrder;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    move-object/from16 v7, p5

    invoke-direct/range {v2 .. v7}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .local v2, imageOrder:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v3, p4

    .line 193
    check-cast v3, Lcom/flixster/android/utils/ImageTask;

    invoke-interface {v3, v2}, Lcom/flixster/android/utils/ImageTask;->orderImage(Lnet/flixster/android/model/ImageOrder;)V

    .line 195
    .end local v2           #imageOrder:Lnet/flixster/android/model/ImageOrder;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    move-wide v0, v15

    invoke-virtual {v3, v0, v1}, Lnet/flixster/android/model/Movie;->setThumbOrderStamp(J)V

    goto/16 :goto_5

    .line 197
    .end local v15           #timeStamp:J
    :cond_1c
    invoke-static {}, Lcom/flixster/android/utils/ImageGetter;->instance()Lcom/flixster/android/utils/ImageGetter;

    move-result-object v3

    new-instance v4, Lcom/flixster/android/activity/ImageViewHandler;

    invoke-direct {v4, v6, v5}, Lcom/flixster/android/activity/ImageViewHandler;-><init>(Landroid/widget/ImageView;Ljava/lang/String;)V

    invoke-virtual {v3, v5, v4}, Lcom/flixster/android/utils/ImageGetter;->get(Ljava/lang/String;Landroid/os/Handler;)V

    goto/16 :goto_5

    .line 205
    .end local v5           #thumbnailUrl:Ljava/lang/String;
    .end local v9           #bitmap:Landroid/graphics/Bitmap;
    :cond_1d
    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie;->mMovie:Lnet/flixster/android/model/Movie;

    invoke-virtual {v3}, Lnet/flixster/android/model/Movie;->getTitle()Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_6

    .line 213
    .restart local v8       #actors:Ljava/lang/String;
    .restart local v17       #title:Ljava/lang/String;
    :cond_1e
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieActors:Landroid/widget/TextView;

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 214
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieActors:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 223
    .restart local v13       #meta:Ljava/lang/String;
    :cond_1f
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieMeta:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_8

    .line 237
    .restart local v18       #trailerUrlString:Ljava/lang/String;
    :cond_20
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9

    .line 241
    :cond_21
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->moviePlayIcon:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9

    .line 246
    :pswitch_0
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieFlixScore:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 247
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRtScore:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 248
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendFlixScore:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 249
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendRtScore:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendFlixScore:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v3, v1}, Lnet/flixster/android/lvi/LviMovie;->setFriendScore(Landroid/widget/TextView;Landroid/content/Context;)V

    .line 251
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lnet/flixster/android/lvi/LviMovie;->setFlixScore(Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;)V

    goto/16 :goto_a

    .line 254
    :pswitch_1
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRtScore:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 255
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieFlixScore:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 256
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendFlixScore:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendRtScore:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 258
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendRtScore:Landroid/widget/TextView;

    move-object/from16 v0, p0

    move-object/from16 v1, p4

    invoke-direct {v0, v3, v1}, Lnet/flixster/android/lvi/LviMovie;->setFriendScore(Landroid/widget/TextView;Landroid/content/Context;)V

    .line 259
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lnet/flixster/android/lvi/LviMovie;->setRtScore(Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;)V

    goto/16 :goto_a

    .line 262
    :pswitch_2
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieRtScore:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 263
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->movieFlixScore:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendFlixScore:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 265
    move-object/from16 v0, v19

    iget-object v3, v0, Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;->friendRtScore:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_a

    .line 270
    .end local v18           #trailerUrlString:Ljava/lang/String;
    :cond_22
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lnet/flixster/android/lvi/LviMovie;->setFlixScore(Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;)V

    .line 271
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lnet/flixster/android/lvi/LviMovie;->setRtScore(Lnet/flixster/android/lvi/LviMovie$MovieViewItemHolder;)V

    goto/16 :goto_a

    .line 275
    .end local v6           #iv:Landroid/widget/ImageView;
    .end local v8           #actors:Ljava/lang/String;
    .end local v13           #meta:Ljava/lang/String;
    .end local v17           #title:Ljava/lang/String;
    :cond_23
    const-string v3, "FlxMain"

    const-string v4, "Bad stuff is happening here..."

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_a

    .line 285
    :cond_24
    const v3, 0x7f020075

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_b

    .line 244
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
