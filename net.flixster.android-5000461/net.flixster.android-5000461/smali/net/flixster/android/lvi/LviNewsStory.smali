.class public Lnet/flixster/android/lvi/LviNewsStory;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviNewsStory.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public mNewsStory:Lnet/flixster/android/model/NewsStory;

.field private mThumbOrderStamp:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 23
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lnet/flixster/android/lvi/LviNewsStory;->mThumbOrderStamp:J

    .line 19
    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lnet/flixster/android/lvi/LviNewsStory;->VIEW_TYPE_NEWSITEM:I

    return v0
.end method

.method public getThumbOrderStamp()J
    .locals 2

    .prologue
    .line 32
    iget-wide v0, p0, Lnet/flixster/android/lvi/LviNewsStory;->mThumbOrderStamp:J

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 17
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 42
    if-nez p2, :cond_1

    .line 43
    invoke-static/range {p4 .. p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    .line 44
    .local v8, li:Landroid/view/LayoutInflater;
    const v2, 0x7f03006b

    const/4 v3, 0x0

    invoke-virtual {v8, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout;

    .line 48
    .end local v8           #li:Landroid/view/LayoutInflater;
    .local v9, newsItemView:Landroid/widget/RelativeLayout;
    :goto_0
    const v2, 0x7f070205

    invoke-virtual {v9, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/TextView;

    .line 49
    .local v12, tv:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    iget-object v2, v2, Lnet/flixster/android/model/NewsStory;->mTitle:Ljava/lang/String;

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    const v2, 0x7f070206

    invoke-virtual {v9, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v12

    .end local v12           #tv:Landroid/widget/TextView;
    check-cast v12, Landroid/widget/TextView;

    .line 51
    .restart local v12       #tv:Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    iget-object v2, v2, Lnet/flixster/android/model/NewsStory;->mDescription:Ljava/lang/String;

    invoke-virtual {v12, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    const v2, 0x7f070203

    invoke-virtual {v9, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 54
    .local v5, iv:Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    iget-object v2, v2, Lnet/flixster/android/model/NewsStory;->mSoftPhoto:Ljava/lang/ref/SoftReference;

    invoke-virtual {v2}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/graphics/Bitmap;

    .line 55
    .local v7, b:Landroid/graphics/Bitmap;
    if-eqz v7, :cond_2

    .line 56
    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 74
    .end local p4
    :cond_0
    :goto_1
    return-object v9

    .end local v5           #iv:Landroid/widget/ImageView;
    .end local v7           #b:Landroid/graphics/Bitmap;
    .end local v9           #newsItemView:Landroid/widget/RelativeLayout;
    .end local v12           #tv:Landroid/widget/TextView;
    .restart local p4
    :cond_1
    move-object/from16 v9, p2

    .line 46
    check-cast v9, Landroid/widget/RelativeLayout;

    .restart local v9       #newsItemView:Landroid/widget/RelativeLayout;
    goto :goto_0

    .line 58
    .restart local v5       #iv:Landroid/widget/ImageView;
    .restart local v7       #b:Landroid/graphics/Bitmap;
    .restart local v12       #tv:Landroid/widget/TextView;
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    iget-object v4, v2, Lnet/flixster/android/model/NewsStory;->mThumbnailUrl:Ljava/lang/String;

    .line 59
    .local v4, thumbnailUrl:Ljava/lang/String;
    const v2, 0x7f02006c

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 60
    if-eqz v4, :cond_0

    const-string v2, "http"

    invoke-virtual {v4, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    .line 63
    .local v10, timeStamp:J
    long-to-double v2, v10

    move-object/from16 v0, p0

    iget-object v6, v0, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    invoke-virtual {v6}, Lnet/flixster/android/model/NewsStory;->getThumbOrderStamp()J

    move-result-wide v13

    long-to-double v13, v13

    const-wide v15, 0x41e65a0bc0000000L

    add-double/2addr v13, v15

    cmpl-double v2, v2, v13

    if-lez v2, :cond_0

    .line 64
    new-instance v1, Lnet/flixster/android/model/ImageOrder;

    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-object v3, v0, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    move-object/from16 v6, p5

    invoke-direct/range {v1 .. v6}, Lnet/flixster/android/model/ImageOrder;-><init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V

    .line 66
    .local v1, imageOrder:Lnet/flixster/android/model/ImageOrder;
    move-object/from16 v0, p4

    instance-of v2, v0, Lcom/flixster/android/utils/ImageTask;

    if-eqz v2, :cond_3

    .line 67
    check-cast p4, Lcom/flixster/android/utils/ImageTask;

    .end local p4
    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Lcom/flixster/android/utils/ImageTask;->orderImageFifo(Lnet/flixster/android/model/ImageOrder;)V

    .line 69
    :cond_3
    const v2, 0x7f02007b

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 70
    move-object/from16 v0, p0

    iget-object v2, v0, Lnet/flixster/android/lvi/LviNewsStory;->mNewsStory:Lnet/flixster/android/model/NewsStory;

    invoke-virtual {v2, v10, v11}, Lnet/flixster/android/model/NewsStory;->setThumbOrderStamp(J)V

    goto :goto_1
.end method

.method public setThumbOrderStamp(J)V
    .locals 0
    .parameter "stamp"

    .prologue
    .line 36
    iput-wide p1, p0, Lnet/flixster/android/lvi/LviNewsStory;->mThumbOrderStamp:J

    .line 37
    return-void
.end method
