.class public Lnet/flixster/android/lvi/LviPageHeader;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviPageHeader.java"


# instance fields
.field public mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 17
    sget v0, Lnet/flixster/android/lvi/LviPageHeader;->VIEW_TYPE_PAGEHEADER:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 4
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 23
    if-nez p2, :cond_0

    .line 24
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 25
    .local v0, li:Landroid/view/LayoutInflater;
    const v2, 0x7f03006c

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 29
    .end local v0           #li:Landroid/view/LayoutInflater;
    .local v1, tv:Landroid/widget/TextView;
    :goto_0
    iget-object v2, p0, Lnet/flixster/android/lvi/LviPageHeader;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 31
    return-object v1

    .end local v1           #tv:Landroid/widget/TextView;
    :cond_0
    move-object v1, p2

    .line 27
    check-cast v1, Landroid/widget/TextView;

    .restart local v1       #tv:Landroid/widget/TextView;
    goto :goto_0
.end method
