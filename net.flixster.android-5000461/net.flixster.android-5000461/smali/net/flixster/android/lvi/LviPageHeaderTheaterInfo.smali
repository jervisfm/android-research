.class public Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviPageHeaderTheaterInfo.java"


# instance fields
.field public mStarTheaterClick:Landroid/view/View$OnClickListener;

.field public mTheater:Lnet/flixster/android/model/Theater;

.field public mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 25
    sget v0, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;->VIEW_TYPE_PAGEHEADER:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 31
    if-nez p2, :cond_0

    .line 32
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 33
    .local v0, li:Landroid/view/LayoutInflater;
    const v3, 0x7f03004a

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .end local p2
    check-cast p2, Landroid/widget/RelativeLayout;

    .line 35
    .end local v0           #li:Landroid/view/LayoutInflater;
    .restart local p2
    :cond_0
    const v3, 0x7f0700fe

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 36
    .local v2, tv:Landroid/widget/TextView;
    iget-object v3, p0, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;->mTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 37
    const v3, 0x7f0700ff

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 38
    .local v1, star:Landroid/widget/CheckBox;
    iget-object v3, p0, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixsterApplication;->favoriteTheaterFlag(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 39
    iget-object v3, p0, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 40
    iget-object v3, p0, Lnet/flixster/android/lvi/LviPageHeaderTheaterInfo;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-object p2
.end method
