.class public Lnet/flixster/android/lvi/LviSearchPanel;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviSearchPanel.java"


# instance fields
.field public mHint:Ljava/lang/String;

.field public mOnClickListener:Landroid/view/View$OnClickListener;

.field public mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 23
    sget v0, Lnet/flixster/android/lvi/LviSearchPanel;->VIEW_TYPE_DATEPANEL:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 5
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 28
    if-nez p2, :cond_0

    .line 29
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 30
    .local v1, li:Landroid/view/LayoutInflater;
    const v3, 0x7f03004b

    const/4 v4, 0x0

    invoke-virtual {v1, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 33
    .end local v1           #li:Landroid/view/LayoutInflater;
    :cond_0
    const v3, 0x7f070102

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 34
    .local v2, searchQuery:Landroid/widget/EditText;
    const v3, 0x7f070101

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    .line 35
    .local v0, button:Landroid/widget/ImageButton;
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    .line 36
    iget-object v3, p0, Lnet/flixster/android/lvi/LviSearchPanel;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    iget-object v3, p0, Lnet/flixster/android/lvi/LviSearchPanel;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 38
    iget-object v3, p0, Lnet/flixster/android/lvi/LviSearchPanel;->mHint:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 39
    iget-object v3, p0, Lnet/flixster/android/lvi/LviSearchPanel;->mHint:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 41
    :cond_1
    return-object p2
.end method
