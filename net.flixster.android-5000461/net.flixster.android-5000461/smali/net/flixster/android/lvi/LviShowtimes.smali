.class public Lnet/flixster/android/lvi/LviShowtimes;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviShowtimes.java"


# instance fields
.field public mBuyClick:Landroid/view/View$OnClickListener;

.field public mShowtimePosition:I

.field public mShowtimes:Lnet/flixster/android/model/Showtimes;

.field public mShowtimesListSize:I

.field public mTheater:Lnet/flixster/android/model/Theater;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lnet/flixster/android/lvi/LviShowtimes;->VIEW_TYPE_SHOWTIMES:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 10
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    const/16 v9, 0x8

    const/4 v7, 0x0

    .line 33
    if-nez p2, :cond_0

    .line 34
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 35
    .local v1, li:Landroid/view/LayoutInflater;
    const v6, 0x7f03007e

    invoke-virtual {v1, v6, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .end local v1           #li:Landroid/view/LayoutInflater;
    :cond_0
    move-object v4, p2

    .line 38
    check-cast v4, Landroid/widget/RelativeLayout;

    .line 39
    .local v4, root:Landroid/widget/RelativeLayout;
    const v6, 0x7f07024f

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 40
    .local v2, movieTitle:Landroid/widget/TextView;
    const v6, 0x7f070251

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 41
    .local v5, timeList:Landroid/widget/TextView;
    const v6, 0x7f070250

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 44
    .local v0, buyButton:Landroid/widget/ImageView;
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0a002b

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 45
    .local v3, p:I
    iget v6, p0, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimePosition:I

    if-lez v6, :cond_1

    move v6, v7

    :goto_0
    invoke-virtual {v4, v3, v6, v3, v3}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 48
    iget-object v6, p0, Lnet/flixster/android/lvi/LviShowtimes;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v6}, Lnet/flixster/android/model/Theater;->hasTickets()Z

    move-result v6

    if-eqz v6, :cond_2

    iget-object v6, p0, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    if-eqz v6, :cond_2

    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v6

    invoke-virtual {v6}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v6

    if-nez v6, :cond_2

    .line 49
    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 50
    iget-object v6, p0, Lnet/flixster/android/lvi/LviShowtimes;->mBuyClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 57
    :goto_1
    iget-object v6, p0, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    if-nez v6, :cond_3

    .line 58
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0058

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 70
    :goto_2
    invoke-virtual {p2, p0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 71
    return-object p2

    :cond_1
    move v6, v3

    .line 45
    goto :goto_0

    .line 53
    :cond_2
    invoke-virtual {v0, v9}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 61
    :cond_3
    iget v6, p0, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimesListSize:I

    const/4 v8, 0x1

    if-le v6, v8, :cond_4

    .line 62
    iget-object v6, p0, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    iget-object v6, v6, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 67
    :goto_3
    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 68
    iget-object v6, p0, Lnet/flixster/android/lvi/LviShowtimes;->mShowtimes:Lnet/flixster/android/model/Showtimes;

    invoke-virtual {v6}, Lnet/flixster/android/model/Showtimes;->getTimesString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 65
    :cond_4
    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3
.end method
