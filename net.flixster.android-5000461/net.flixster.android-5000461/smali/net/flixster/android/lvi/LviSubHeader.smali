.class public Lnet/flixster/android/lvi/LviSubHeader;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviSubHeader.java"


# instance fields
.field public mTitle:Ljava/lang/String;

.field private titleId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 17
    return-void
.end method

.method public constructor <init>(ILandroid/app/Activity;)V
    .locals 0
    .parameter "titleId"
    .parameter "activity"

    .prologue
    .line 19
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 20
    iput p1, p0, Lnet/flixster/android/lvi/LviSubHeader;->titleId:I

    .line 22
    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lnet/flixster/android/lvi/LviAdapter$LviType;->SUBHEADER:Lnet/flixster/android/lvi/LviAdapter$LviType;

    invoke-virtual {v0}, Lnet/flixster/android/lvi/LviAdapter$LviType;->ordinal()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 2
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 31
    if-nez p2, :cond_0

    .line 32
    new-instance p2, Lcom/flixster/android/view/SubHeader;

    .end local p2
    invoke-direct {p2, p4}, Lcom/flixster/android/view/SubHeader;-><init>(Landroid/content/Context;)V

    .line 35
    .restart local p2
    :cond_0
    iget v0, p0, Lnet/flixster/android/lvi/LviSubHeader;->titleId:I

    if-eqz v0, :cond_1

    move-object v0, p2

    .line 36
    check-cast v0, Lcom/flixster/android/view/SubHeader;

    iget v1, p0, Lnet/flixster/android/lvi/LviSubHeader;->titleId:I

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubHeader;->setText(I)V

    .line 40
    :goto_0
    return-object p2

    :cond_1
    move-object v0, p2

    .line 38
    check-cast v0, Lcom/flixster/android/view/SubHeader;

    iget-object v1, p0, Lnet/flixster/android/lvi/LviSubHeader;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/flixster/android/view/SubHeader;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
