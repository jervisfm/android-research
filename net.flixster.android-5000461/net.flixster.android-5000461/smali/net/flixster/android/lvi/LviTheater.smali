.class public Lnet/flixster/android/lvi/LviTheater;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviTheater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;
    }
.end annotation


# instance fields
.field public mStarTheaterClick:Landroid/view/View$OnClickListener;

.field public mTheater:Lnet/flixster/android/model/Theater;

.field public mTheaterClick:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 27
    sget v0, Lnet/flixster/android/lvi/LviTheater;->VIEW_TYPE_THEATER:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 7
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    const/16 v6, 0x8

    const/4 v4, 0x0

    .line 34
    if-nez p2, :cond_0

    .line 35
    invoke-static {p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 36
    .local v0, li:Landroid/view/LayoutInflater;
    const v3, 0x7f03007d

    invoke-virtual {v0, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 37
    new-instance v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;

    invoke-direct {v2}, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;-><init>()V

    .line 38
    .local v2, viewHolder:Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;
    const v3, 0x7f07024b

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesTheater:Landroid/widget/TextView;

    .line 39
    const v3, 0x7f07024d

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesTheaterAddress:Landroid/widget/TextView;

    .line 40
    const v3, 0x7f07024a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesTicketIcon:Landroid/widget/ImageView;

    .line 41
    const v3, 0x7f07024c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesStar:Landroid/widget/CheckBox;

    .line 47
    .end local v0           #li:Landroid/view/LayoutInflater;
    :goto_0
    iget-object v3, p0, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v3}, Lnet/flixster/android/model/Theater;->hasTickets()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 48
    iget-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesTicketIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 53
    :goto_1
    iget-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesTheater:Landroid/widget/TextView;

    iget-object v4, p0, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v5, "name"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 54
    iget-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesTheaterAddress:Landroid/widget/TextView;

    iget-object v4, p0, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    const-string v5, "address"

    invoke-virtual {v4, v5}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v3

    invoke-virtual {v3}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 57
    iget-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesStar:Landroid/widget/CheckBox;

    invoke-virtual {v3, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 58
    iput-object p0, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->lviTheater:Lnet/flixster/android/lvi/LviTheater;

    .line 60
    invoke-virtual {p0}, Lnet/flixster/android/lvi/LviTheater;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 61
    const v3, 0x7f020070

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 65
    :goto_2
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0062

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 66
    .local v1, padding:I
    invoke-virtual {p2, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 75
    .end local v1           #padding:I
    :goto_3
    iget-object v3, p0, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    iput-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->theater:Lnet/flixster/android/model/Theater;

    .line 77
    const/4 v3, 0x1

    invoke-virtual {p2, v3}, Landroid/view/View;->setClickable(Z)V

    .line 78
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 80
    iget-object v3, p0, Lnet/flixster/android/lvi/LviTheater;->mTheaterClick:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    return-object p2

    .line 44
    .end local v2           #viewHolder:Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;

    .restart local v2       #viewHolder:Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;
    goto :goto_0

    .line 50
    :cond_1
    iget-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesTicketIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 63
    :cond_2
    const v3, 0x7f020075

    invoke-virtual {p2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    .line 68
    :cond_3
    iget-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesStar:Landroid/widget/CheckBox;

    .line 69
    iget-object v4, p0, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v4}, Lnet/flixster/android/model/Theater;->getId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lnet/flixster/android/FlixsterApplication;->favoriteTheaterFlag(Ljava/lang/String;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 70
    iget-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesStar:Landroid/widget/CheckBox;

    iget-object v4, p0, Lnet/flixster/android/lvi/LviTheater;->mTheater:Lnet/flixster/android/model/Theater;

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setTag(Ljava/lang/Object;)V

    .line 71
    iget-object v3, v2, Lnet/flixster/android/lvi/LviTheater$TheaterItemHolder;->showtimesStar:Landroid/widget/CheckBox;

    iget-object v4, p0, Lnet/flixster/android/lvi/LviTheater;->mStarTheaterClick:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method
