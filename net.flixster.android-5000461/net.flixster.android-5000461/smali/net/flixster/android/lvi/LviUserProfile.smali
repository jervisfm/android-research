.class public Lnet/flixster/android/lvi/LviUserProfile;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviUserProfile.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;
    }
.end annotation


# instance fields
.field public mUser:Lnet/flixster/android/model/User;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 23
    sget v0, Lnet/flixster/android/lvi/LviUserProfile;->VIEW_TYPE_USERPROFILE:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 14
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 29
    invoke-virtual/range {p4 .. p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 30
    .local v8, resources:Landroid/content/res/Resources;
    if-nez p2, :cond_0

    .line 31
    invoke-static/range {p4 .. p4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 32
    .local v5, li:Landroid/view/LayoutInflater;
    const v11, 0x7f03006e

    const/4 v12, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v5, v11, v0, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 35
    .end local v5           #li:Landroid/view/LayoutInflater;
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;

    .line 36
    .local v9, viewHolder:Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;
    if-nez v9, :cond_1

    .line 37
    new-instance v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;

    .end local v9           #viewHolder:Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;
    const/4 v11, 0x0

    invoke-direct {v9, v11}, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;-><init>(Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;)V

    .line 38
    .restart local v9       #viewHolder:Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;
    const v11, 0x7f07020a

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    .line 39
    const v11, 0x7f07020b

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    iput-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    .line 40
    const v11, 0x7f07020c

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    .line 41
    const v11, 0x7f0700a5

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->friendView:Landroid/widget/TextView;

    .line 42
    const v11, 0x7f0700a4

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->ratingView:Landroid/widget/TextView;

    .line 43
    const v11, 0x7f0700a3

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->wtsView:Landroid/widget/TextView;

    .line 44
    const v11, 0x7f0700a6

    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    iput-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    .line 45
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 49
    :cond_1
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getPlatform()Ljava/lang/String;

    move-result-object v6

    .line 50
    .local v6, platform:Ljava/lang/String;
    const-string v11, "FLX"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_6

    .line 51
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    const v12, 0x7f0200bc

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 56
    :cond_2
    :goto_0
    iget-object v11, p0, Lnet/flixster/android/lvi/LviUserProfile;->mUser:Lnet/flixster/android/model/User;

    iget-object v12, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v11, v12}, Lnet/flixster/android/model/User;->getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 57
    .local v1, bitmap:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_3

    .line 58
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v11, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 61
    :cond_3
    iget-object v11, p0, Lnet/flixster/android/lvi/LviUserProfile;->mUser:Lnet/flixster/android/model/User;

    iget-object v3, v11, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    .line 62
    .local v3, displayName:Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 63
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    invoke-virtual {v11, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->titleView:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 66
    :cond_4
    iget-object v11, p0, Lnet/flixster/android/lvi/LviUserProfile;->mUser:Lnet/flixster/android/model/User;

    iget v4, v11, Lnet/flixster/android/model/User;->friendCount:I

    .line 67
    .local v4, friendCount:I
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->friendView:Landroid/widget/TextView;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, ""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 68
    const v13, 0x7f0c0082

    invoke-virtual {v8, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 67
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v11, p0, Lnet/flixster/android/lvi/LviUserProfile;->mUser:Lnet/flixster/android/model/User;

    iget v7, v11, Lnet/flixster/android/model/User;->ratingCount:I

    .line 70
    .local v7, ratingCount:I
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->ratingView:Landroid/widget/TextView;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, ""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 71
    const v13, 0x7f0c0083

    invoke-virtual {v8, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 70
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v11, p0, Lnet/flixster/android/lvi/LviUserProfile;->mUser:Lnet/flixster/android/model/User;

    iget v10, v11, Lnet/flixster/android/model/User;->wtsCount:I

    .line 73
    .local v10, wtsCount:I
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->wtsView:Landroid/widget/TextView;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, ""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 74
    const v13, 0x7f0c0084

    invoke-virtual {v8, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 73
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v11, p0, Lnet/flixster/android/lvi/LviUserProfile;->mUser:Lnet/flixster/android/model/User;

    iget v2, v11, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 76
    .local v2, collectionsCount:I
    if-lez v2, :cond_5

    .line 77
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, ""

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 78
    const v13, 0x7f0c0085

    invoke-virtual {v8, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    .line 77
    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->collectionsView:Landroid/widget/TextView;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 81
    :cond_5
    return-object p2

    .line 52
    .end local v1           #bitmap:Landroid/graphics/Bitmap;
    .end local v2           #collectionsCount:I
    .end local v3           #displayName:Ljava/lang/String;
    .end local v4           #friendCount:I
    .end local v7           #ratingCount:I
    .end local v10           #wtsCount:I
    :cond_6
    const-string v11, "FBK"

    invoke-virtual {v11, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 53
    iget-object v11, v9, Lnet/flixster/android/lvi/LviUserProfile$ProfileViewHolder;->thumbnailFrameView:Landroid/widget/ImageView;

    const v12, 0x7f020149

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method
