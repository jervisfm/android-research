.class public Lnet/flixster/android/lvi/LviWrapper;
.super Lnet/flixster/android/lvi/Lvi;
.source "LviWrapper.java"


# instance fields
.field private final view:Landroid/view/View;

.field private final viewType:I


# direct methods
.method private constructor <init>(Landroid/view/View;I)V
    .locals 0
    .parameter "v"
    .parameter "viewType"

    .prologue
    .line 12
    invoke-direct {p0}, Lnet/flixster/android/lvi/Lvi;-><init>()V

    .line 13
    iput-object p1, p0, Lnet/flixster/android/lvi/LviWrapper;->view:Landroid/view/View;

    .line 14
    iput p2, p0, Lnet/flixster/android/lvi/LviWrapper;->viewType:I

    .line 15
    return-void
.end method

.method public static convertToLvi(Landroid/view/View;I)Lnet/flixster/android/lvi/Lvi;
    .locals 1
    .parameter "v"
    .parameter "viewType"

    .prologue
    .line 18
    new-instance v0, Lnet/flixster/android/lvi/LviWrapper;

    invoke-direct {v0, p0, p1}, Lnet/flixster/android/lvi/LviWrapper;-><init>(Landroid/view/View;I)V

    return-object v0
.end method


# virtual methods
.method public getItemViewType()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lnet/flixster/android/lvi/LviWrapper;->viewType:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;Landroid/content/Context;Landroid/os/Handler;)Landroid/view/View;
    .locals 1
    .parameter "position"
    .parameter "convertView"
    .parameter "parent"
    .parameter "activity"
    .parameter "refreshHandler"

    .prologue
    .line 23
    iget-object v0, p0, Lnet/flixster/android/lvi/LviWrapper;->view:Landroid/view/View;

    return-object v0
.end method
