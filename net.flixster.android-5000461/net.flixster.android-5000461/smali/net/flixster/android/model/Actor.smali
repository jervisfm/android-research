.class public Lnet/flixster/android/model/Actor;
.super Ljava/lang/Object;
.source "Actor.java"


# static fields
.field public static final KEY_ACTOR_ID:Ljava/lang/String; = "ACTOR_ID"

.field public static final KEY_ACTOR_NAME:Ljava/lang/String; = "ACTOR_NAME"


# instance fields
.field public biography:Ljava/lang/String;

.field public birthDate:Ljava/util/Date;

.field public birthDay:Ljava/lang/String;

.field public birthplace:Ljava/lang/String;

.field public bitmap:Landroid/graphics/Bitmap;

.field public chars:Ljava/lang/String;

.field public flixsterUrl:Ljava/lang/String;

.field public id:J

.field public largeUrl:Ljava/lang/String;

.field public movies:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Movie;",
            ">;"
        }
    .end annotation
.end field

.field public name:Ljava/lang/String;

.field public photoCount:I

.field public photos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation
.end field

.field public positionId:I

.field public thumbnailUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
