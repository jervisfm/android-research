.class public Lnet/flixster/android/model/Contact;
.super Ljava/lang/Object;
.source "Contact.java"


# instance fields
.field public familyName:Ljava/lang/String;

.field public givenName:Ljava/lang/String;

.field public middleName:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public number:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "name"

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lnet/flixster/android/model/Contact;->name:Ljava/lang/String;

    .line 10
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .parameter "name"
    .parameter "givenName"
    .parameter "middleName"
    .parameter "familyName"
    .parameter "number"

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lnet/flixster/android/model/Contact;->name:Ljava/lang/String;

    .line 14
    iput-object p2, p0, Lnet/flixster/android/model/Contact;->givenName:Ljava/lang/String;

    .line 15
    iput-object p3, p0, Lnet/flixster/android/model/Contact;->middleName:Ljava/lang/String;

    .line 16
    iput-object p4, p0, Lnet/flixster/android/model/Contact;->familyName:Ljava/lang/String;

    .line 17
    iput-object p5, p0, Lnet/flixster/android/model/Contact;->number:Ljava/lang/String;

    .line 18
    return-void
.end method
