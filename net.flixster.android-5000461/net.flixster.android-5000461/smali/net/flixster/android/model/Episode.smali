.class public Lnet/flixster/android/model/Episode;
.super Lnet/flixster/android/model/VideoAsset;
.source "Episode.java"


# static fields
.field public static final EDITION_ID:I = 0x3e8


# instance fields
.field private episodeNum:I

.field private synopsis:Ljava/lang/String;


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Lnet/flixster/android/model/VideoAsset;-><init>()V

    .line 19
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lnet/flixster/android/model/Episode;->editionId:J

    .line 20
    return-void
.end method

.method public constructor <init>(J)V
    .locals 0
    .parameter "assetId"

    .prologue
    .line 14
    invoke-direct {p0}, Lnet/flixster/android/model/Episode;-><init>()V

    .line 15
    iput-wide p1, p0, Lnet/flixster/android/model/Episode;->id:J

    .line 16
    return-void
.end method

.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 0
    .parameter "json"

    .prologue
    .line 23
    invoke-direct {p0}, Lnet/flixster/android/model/Episode;-><init>()V

    .line 24
    invoke-virtual {p0, p1}, Lnet/flixster/android/model/Episode;->merge(Lorg/json/JSONObject;)V

    .line 25
    return-void
.end method


# virtual methods
.method public getEpisodeNumber()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lnet/flixster/android/model/Episode;->episodeNum:I

    return v0
.end method

.method public getSynopsis()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lnet/flixster/android/model/Episode;->synopsis:Ljava/lang/String;

    return-object v0
.end method

.method protected merge(Lorg/json/JSONObject;)V
    .locals 3
    .parameter "json"

    .prologue
    .line 29
    const-string v0, "id"

    iget-wide v1, p0, Lnet/flixster/android/model/Episode;->id:J

    invoke-virtual {p1, v0, v1, v2}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lnet/flixster/android/model/Episode;->id:J

    .line 30
    const-string v0, "episode"

    iget v1, p0, Lnet/flixster/android/model/Episode;->episodeNum:I

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lnet/flixster/android/model/Episode;->episodeNum:I

    .line 31
    const-string v0, "title"

    iget-object v1, p0, Lnet/flixster/android/model/Episode;->title:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Episode;->title:Ljava/lang/String;

    .line 32
    const-string v0, "synopsis"

    iget-object v1, p0, Lnet/flixster/android/model/Episode;->synopsis:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Episode;->synopsis:Ljava/lang/String;

    .line 33
    return-void
.end method
