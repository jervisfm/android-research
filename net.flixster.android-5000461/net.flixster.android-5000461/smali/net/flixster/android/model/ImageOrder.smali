.class public Lnet/flixster/android/model/ImageOrder;
.super Ljava/lang/Object;
.source "ImageOrder.java"


# static fields
.field public static final TYPE_ACTOR_PHOTO:I = 0x4

.field public static final TYPE_GALLERY_PHOTO:I = 0x8

.field public static final TYPE_GALLERY_THUMB:I = 0x9

.field public static final TYPE_MOVIE_PHOTO:I = 0x3

.field public static final TYPE_MOVIE_THUMB:I = 0x0

.field public static final TYPE_NETFLIX_THUMB:I = 0x7

.field public static final TYPE_REVIEW_MUG:I = 0x1

.field public static final TYPE_STORY_THUMB:I = 0x6

.field public static final TYPE_USER_PHOTO:I = 0x5


# instance fields
.field public movieView:Landroid/view/View;

.field public refreshHandler:Landroid/os/Handler;

.field public tag:Ljava/lang/Object;

.field public type:I

.field public urlString:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILjava/lang/Object;Ljava/lang/String;Landroid/view/View;Landroid/os/Handler;)V
    .locals 0
    .parameter "type"
    .parameter "tag"
    .parameter "url"
    .parameter "view"
    .parameter "refreshHandler"

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput p1, p0, Lnet/flixster/android/model/ImageOrder;->type:I

    .line 28
    iput-object p2, p0, Lnet/flixster/android/model/ImageOrder;->tag:Ljava/lang/Object;

    .line 29
    iput-object p3, p0, Lnet/flixster/android/model/ImageOrder;->urlString:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Lnet/flixster/android/model/ImageOrder;->movieView:Landroid/view/View;

    .line 31
    iput-object p5, p0, Lnet/flixster/android/model/ImageOrder;->refreshHandler:Landroid/os/Handler;

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "url"

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lnet/flixster/android/model/ImageOrder;->urlString:Ljava/lang/String;

    .line 36
    return-void
.end method
