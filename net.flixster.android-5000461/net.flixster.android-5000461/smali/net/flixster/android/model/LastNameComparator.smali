.class public Lnet/flixster/android/model/LastNameComparator;
.super Ljava/lang/Object;
.source "LastNameComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lnet/flixster/android/model/User;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lnet/flixster/android/model/User;

    check-cast p2, Lnet/flixster/android/model/User;

    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/LastNameComparator;->compare(Lnet/flixster/android/model/User;Lnet/flixster/android/model/User;)I

    move-result v0

    return v0
.end method

.method public compare(Lnet/flixster/android/model/User;Lnet/flixster/android/model/User;)I
    .locals 3
    .parameter "user1"
    .parameter "user2"

    .prologue
    .line 6
    iget-object v0, p1, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    .line 7
    .local v0, lastName1:Ljava/lang/String;
    iget-object v1, p2, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    .line 8
    .local v1, lastName2:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 9
    const-string v0, ""

    .line 11
    :cond_0
    if-nez v1, :cond_1

    .line 12
    const-string v1, ""

    .line 14
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    return v2
.end method
