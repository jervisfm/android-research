.class public Lnet/flixster/android/model/Listing;
.super Ljava/lang/Object;
.source "Listing.java"


# static fields
.field private static final ASTERISK:Ljava/lang/String; = "*"

.field public static final CINEMASOURCE_LISTING:Ljava/lang/String; = "listing"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final CINEMASOURCE_THEATER:Ljava/lang/String; = "theater"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final DATE_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final DAY_IN_WEEK_FORMAT:Ljava/text/SimpleDateFormat;

.field private static final DISPLAY_DATE_FORMAT:Ljava/text/SimpleDateFormat;


# instance fields
.field private date:Ljava/util/Date;

.field public final dateString:Ljava/lang/String;

.field private dateTime:Ljava/util/Date;

.field public final displayTime:Ljava/lang/String;

.field public paramMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final ticketUrl:Ljava/lang/String;

.field public final timeString:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyyMMdd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lnet/flixster/android/model/Listing;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 27
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEEE, MMM d"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lnet/flixster/android/model/Listing;->DISPLAY_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    .line 28
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "EEEE"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lnet/flixster/android/model/Listing;->DAY_IN_WEEK_FORMAT:Ljava/text/SimpleDateFormat;

    .line 24
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 3
    .parameter "listing"

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "time"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Listing;->displayTime:Ljava/lang/String;

    .line 49
    const-string v0, "date"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Listing;->dateString:Ljava/lang/String;

    .line 50
    const-string v0, "ticket"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Listing;->ticketUrl:Ljava/lang/String;

    .line 51
    iget-object v0, p0, Lnet/flixster/android/model/Listing;->ticketUrl:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    iput-object v0, p0, Lnet/flixster/android/model/Listing;->timeString:Ljava/lang/String;

    .line 52
    return-void

    .line 51
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Listing;->ticketUrl:Ljava/lang/String;

    const-string v1, "perft"

    invoke-static {v0, v1}, Lcom/flixster/android/utils/UrlHelper;->getSingleQueryValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ":"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getDate()Ljava/util/Date;
    .locals 4

    .prologue
    .line 121
    iget-object v2, p0, Lnet/flixster/android/model/Listing;->date:Ljava/util/Date;

    if-nez v2, :cond_0

    .line 123
    :try_start_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    .line 124
    .local v1, tempCal:Ljava/util/Calendar;
    sget-object v2, Lnet/flixster/android/model/Listing;->DATE_FORMAT:Ljava/text/SimpleDateFormat;

    iget-object v3, p0, Lnet/flixster/android/model/Listing;->dateString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 125
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Listing;->date:Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 131
    .end local v1           #tempCal:Ljava/util/Calendar;
    :goto_0
    return-object v2

    .line 126
    :catch_0
    move-exception v0

    .line 127
    .local v0, e:Ljava/text/ParseException;
    const-string v2, "FlxMain"

    const-string v3, "Listing.getDate ParseException"

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 128
    const/4 v2, 0x0

    goto :goto_0

    .line 131
    .end local v0           #e:Ljava/text/ParseException;
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/model/Listing;->date:Ljava/util/Date;

    goto :goto_0
.end method

.method private getDateTime()Ljava/util/Date;
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 137
    iget-object v4, p0, Lnet/flixster/android/model/Listing;->dateTime:Ljava/util/Date;

    if-nez v4, :cond_1

    .line 138
    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->getDate()Ljava/util/Date;

    move-result-object v0

    .line 139
    .local v0, date:Ljava/util/Date;
    if-nez v0, :cond_0

    .line 140
    const/4 v4, 0x0

    .line 151
    .end local v0           #date:Ljava/util/Date;
    :goto_0
    return-object v4

    .line 142
    .restart local v0       #date:Ljava/util/Date;
    :cond_0
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 143
    .local v3, tempCal:Ljava/util/Calendar;
    invoke-virtual {v3, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 144
    iget-object v4, p0, Lnet/flixster/android/model/Listing;->timeString:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 145
    .local v1, hour:I
    iget-object v4, p0, Lnet/flixster/android/model/Listing;->timeString:Ljava/lang/String;

    const/4 v5, 0x4

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 146
    .local v2, minute:I
    const/16 v4, 0xb

    invoke-virtual {v3, v4, v1}, Ljava/util/Calendar;->set(II)V

    .line 147
    const/16 v4, 0xc

    invoke-virtual {v3, v4, v2}, Ljava/util/Calendar;->set(II)V

    .line 148
    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/model/Listing;->dateTime:Ljava/util/Date;

    goto :goto_0

    .line 151
    .end local v0           #date:Ljava/util/Date;
    .end local v1           #hour:I
    .end local v2           #minute:I
    .end local v3           #tempCal:Ljava/util/Calendar;
    :cond_1
    iget-object v4, p0, Lnet/flixster/android/model/Listing;->dateTime:Ljava/util/Date;

    goto :goto_0
.end method

.method private getDisplayDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 115
    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->getDate()Ljava/util/Date;

    move-result-object v0

    .line 116
    .local v0, date:Ljava/util/Date;
    if-nez v0, :cond_0

    const-string v1, ""

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lnet/flixster/android/model/Listing;->DISPLAY_DATE_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getDisplayTime()Ljava/lang/String;
    .locals 2

    .prologue
    .line 157
    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->isMidnight()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lnet/flixster/android/model/Listing;->displayTime:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "*"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 160
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Listing;->displayTime:Ljava/lang/String;

    goto :goto_0
.end method

.method private isMidnight()Z
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lnet/flixster/android/model/Listing;->ticketUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/model/Listing;->timeString:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    const/16 v1, 0x960

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDisplayTimeDate()Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->getDisplayTime()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->getDisplayDate()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMidnightDisclaimer()Ljava/lang/String;
    .locals 3

    .prologue
    .line 89
    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->isMidnight()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->getDateTime()Ljava/util/Date;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "*"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 91
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 92
    sget-object v1, Lnet/flixster/android/model/Listing;->DAY_IN_WEEK_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->getDate()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    const-string v1, " night / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 94
    sget-object v1, Lnet/flixster/android/model/Listing;->DAY_IN_WEEK_FORMAT:Ljava/text/SimpleDateFormat;

    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->getDateTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const-string v1, " morning"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 98
    .end local v0           #sb:Ljava/lang/StringBuilder;
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public hasElapsed()Z
    .locals 2

    .prologue
    .line 104
    invoke-direct {p0}, Lnet/flixster/android/model/Listing;->getDateTime()Ljava/util/Date;

    move-result-object v0

    .line 105
    .local v0, dateTime:Ljava/util/Date;
    if-eqz v0, :cond_0

    sget-object v1, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v1, v0}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method
