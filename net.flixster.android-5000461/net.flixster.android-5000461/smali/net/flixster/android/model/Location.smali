.class public Lnet/flixster/android/model/Location;
.super Ljava/lang/Object;
.source "Location.java"


# instance fields
.field public city:Ljava/lang/String;

.field public country:Ljava/lang/String;

.field public countryCode:Ljava/lang/String;

.field public latitude:D

.field public longitude:D

.field public state:Ljava/lang/String;

.field public stateCode:Ljava/lang/String;

.field public weight:I

.field public zip:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
