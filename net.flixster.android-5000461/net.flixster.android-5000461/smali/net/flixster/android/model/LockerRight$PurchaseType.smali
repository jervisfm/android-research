.class public final enum Lnet/flixster/android/model/LockerRight$PurchaseType;
.super Ljava/lang/Enum;
.source "LockerRight.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/model/LockerRight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "PurchaseType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lnet/flixster/android/model/LockerRight$PurchaseType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$PurchaseType;

.field public static final enum FB_INVITE:Lnet/flixster/android/model/LockerRight$PurchaseType;

.field public static final enum INSTALL_MOBILE:Lnet/flixster/android/model/LockerRight$PurchaseType;

.field public static final enum RATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

.field public static final enum SMS:Lnet/flixster/android/model/LockerRight$PurchaseType;

.field public static final enum UNKNOWN:Lnet/flixster/android/model/LockerRight$PurchaseType;

.field public static final enum UV_CREATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

.field public static final enum WTS:Lnet/flixster/android/model/LockerRight$PurchaseType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 55
    new-instance v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    const-string v1, "RATE"

    invoke-direct {v0, v1, v3}, Lnet/flixster/android/model/LockerRight$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->RATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    const-string v1, "WTS"

    invoke-direct {v0, v1, v4}, Lnet/flixster/android/model/LockerRight$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->WTS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v5}, Lnet/flixster/android/model/LockerRight$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->SMS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    const-string v1, "FB_INVITE"

    invoke-direct {v0, v1, v6}, Lnet/flixster/android/model/LockerRight$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->FB_INVITE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    const-string v1, "UV_CREATE"

    invoke-direct {v0, v1, v7}, Lnet/flixster/android/model/LockerRight$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->UV_CREATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    const-string v1, "INSTALL_MOBILE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/model/LockerRight$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->INSTALL_MOBILE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    const-string v1, "UNKNOWN"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/model/LockerRight$PurchaseType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->UNKNOWN:Lnet/flixster/android/model/LockerRight$PurchaseType;

    .line 54
    const/4 v0, 0x7

    new-array v0, v0, [Lnet/flixster/android/model/LockerRight$PurchaseType;

    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->RATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    aput-object v1, v0, v3

    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->WTS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    aput-object v1, v0, v4

    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->SMS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    aput-object v1, v0, v5

    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->FB_INVITE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    aput-object v1, v0, v6

    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->UV_CREATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lnet/flixster/android/model/LockerRight$PurchaseType;->INSTALL_MOBILE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lnet/flixster/android/model/LockerRight$PurchaseType;->UNKNOWN:Lnet/flixster/android/model/LockerRight$PurchaseType;

    aput-object v2, v0, v1

    sput-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$PurchaseType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method protected static match(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$PurchaseType;
    .locals 1
    .parameter "purchaseType"

    .prologue
    .line 58
    const-string v0, "MKR"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    sget-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->RATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    .line 72
    :goto_0
    return-object v0

    .line 60
    :cond_0
    const-string v0, "MKW"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    sget-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->WTS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    goto :goto_0

    .line 62
    :cond_1
    const-string v0, "MT"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    sget-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->SMS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    goto :goto_0

    .line 64
    :cond_2
    const-string v0, "MR"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 65
    sget-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->FB_INVITE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    goto :goto_0

    .line 66
    :cond_3
    const-string v0, "MSK"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "MAN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "MIP"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 67
    const-string v0, "MID"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 68
    :cond_4
    sget-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->UV_CREATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    goto :goto_0

    .line 69
    :cond_5
    const-string v0, "MPN"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "MPW"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 70
    :cond_6
    sget-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->INSTALL_MOBILE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    goto :goto_0

    .line 72
    :cond_7
    sget-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->UNKNOWN:Lnet/flixster/android/model/LockerRight$PurchaseType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$PurchaseType;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight$PurchaseType;

    return-object v0
.end method

.method public static values()[Lnet/flixster/android/model/LockerRight$PurchaseType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lnet/flixster/android/model/LockerRight$PurchaseType;->ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$PurchaseType;

    array-length v1, v0

    new-array v2, v1, [Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
