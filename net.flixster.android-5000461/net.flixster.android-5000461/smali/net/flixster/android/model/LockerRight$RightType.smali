.class public final enum Lnet/flixster/android/model/LockerRight$RightType;
.super Ljava/lang/Enum;
.source "LockerRight.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/model/LockerRight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RightType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lnet/flixster/android/model/LockerRight$RightType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$RightType;

.field public static final enum EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

.field public static final enum MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

.field public static final enum SEASON:Lnet/flixster/android/model/LockerRight$RightType;

.field public static final enum UNFULFILLABLE:Lnet/flixster/android/model/LockerRight$RightType;

.field public static final enum UNKNOWN:Lnet/flixster/android/model/LockerRight$RightType;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 27
    new-instance v0, Lnet/flixster/android/model/LockerRight$RightType;

    const-string v1, "MOVIE"

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/model/LockerRight$RightType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$RightType;

    const-string v1, "SEASON"

    invoke-direct {v0, v1, v3}, Lnet/flixster/android/model/LockerRight$RightType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$RightType;->SEASON:Lnet/flixster/android/model/LockerRight$RightType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$RightType;

    const-string v1, "EPISODE"

    invoke-direct {v0, v1, v4}, Lnet/flixster/android/model/LockerRight$RightType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$RightType;->EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$RightType;

    const-string v1, "UNFULFILLABLE"

    invoke-direct {v0, v1, v5}, Lnet/flixster/android/model/LockerRight$RightType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$RightType;->UNFULFILLABLE:Lnet/flixster/android/model/LockerRight$RightType;

    new-instance v0, Lnet/flixster/android/model/LockerRight$RightType;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6}, Lnet/flixster/android/model/LockerRight$RightType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$RightType;->UNKNOWN:Lnet/flixster/android/model/LockerRight$RightType;

    .line 26
    const/4 v0, 0x5

    new-array v0, v0, [Lnet/flixster/android/model/LockerRight$RightType;

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    aput-object v1, v0, v2

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->SEASON:Lnet/flixster/android/model/LockerRight$RightType;

    aput-object v1, v0, v3

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

    aput-object v1, v0, v4

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->UNFULFILLABLE:Lnet/flixster/android/model/LockerRight$RightType;

    aput-object v1, v0, v5

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->UNKNOWN:Lnet/flixster/android/model/LockerRight$RightType;

    aput-object v1, v0, v6

    sput-object v0, Lnet/flixster/android/model/LockerRight$RightType;->ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$RightType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method protected static match(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$RightType;
    .locals 1
    .parameter "type"

    .prologue
    .line 30
    sget-object v0, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight$RightType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 31
    sget-object v0, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    .line 35
    :goto_0
    return-object v0

    .line 32
    :cond_0
    sget-object v0, Lnet/flixster/android/model/LockerRight$RightType;->EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-virtual {v0}, Lnet/flixster/android/model/LockerRight$RightType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 33
    sget-object v0, Lnet/flixster/android/model/LockerRight$RightType;->EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

    goto :goto_0

    .line 35
    :cond_1
    sget-object v0, Lnet/flixster/android/model/LockerRight$RightType;->UNKNOWN:Lnet/flixster/android/model/LockerRight$RightType;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$RightType;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lnet/flixster/android/model/LockerRight$RightType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight$RightType;

    return-object v0
.end method

.method public static values()[Lnet/flixster/android/model/LockerRight$RightType;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lnet/flixster/android/model/LockerRight$RightType;->ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$RightType;

    array-length v1, v0

    new-array v2, v1, [Lnet/flixster/android/model/LockerRight$RightType;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
