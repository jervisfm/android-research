.class public final enum Lnet/flixster/android/model/LockerRight$Source;
.super Ljava/lang/Enum;
.source "LockerRight.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/model/LockerRight;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Source"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lnet/flixster/android/model/LockerRight$Source;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$Source;

.field public static final enum SONIC:Lnet/flixster/android/model/LockerRight$Source;

.field public static final enum UNKNOWN:Lnet/flixster/android/model/LockerRight$Source;

.field public static final enum WB:Lnet/flixster/android/model/LockerRight$Source;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 41
    new-instance v0, Lnet/flixster/android/model/LockerRight$Source;

    const-string v1, "WB"

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/model/LockerRight$Source;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$Source;->WB:Lnet/flixster/android/model/LockerRight$Source;

    new-instance v0, Lnet/flixster/android/model/LockerRight$Source;

    const-string v1, "SONIC"

    invoke-direct {v0, v1, v3}, Lnet/flixster/android/model/LockerRight$Source;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$Source;->SONIC:Lnet/flixster/android/model/LockerRight$Source;

    new-instance v0, Lnet/flixster/android/model/LockerRight$Source;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, Lnet/flixster/android/model/LockerRight$Source;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lnet/flixster/android/model/LockerRight$Source;->UNKNOWN:Lnet/flixster/android/model/LockerRight$Source;

    .line 40
    const/4 v0, 0x3

    new-array v0, v0, [Lnet/flixster/android/model/LockerRight$Source;

    sget-object v1, Lnet/flixster/android/model/LockerRight$Source;->WB:Lnet/flixster/android/model/LockerRight$Source;

    aput-object v1, v0, v2

    sget-object v1, Lnet/flixster/android/model/LockerRight$Source;->SONIC:Lnet/flixster/android/model/LockerRight$Source;

    aput-object v1, v0, v3

    sget-object v1, Lnet/flixster/android/model/LockerRight$Source;->UNKNOWN:Lnet/flixster/android/model/LockerRight$Source;

    aput-object v1, v0, v4

    sput-object v0, Lnet/flixster/android/model/LockerRight$Source;->ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$Source;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method protected static match(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$Source;
    .locals 1
    .parameter "source"

    .prologue
    .line 44
    const-string v0, "sonic"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    sget-object v0, Lnet/flixster/android/model/LockerRight$Source;->SONIC:Lnet/flixster/android/model/LockerRight$Source;

    .line 49
    :goto_0
    return-object v0

    .line 46
    :cond_0
    const-string v0, "wb"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    sget-object v0, Lnet/flixster/android/model/LockerRight$Source;->WB:Lnet/flixster/android/model/LockerRight$Source;

    goto :goto_0

    .line 49
    :cond_1
    sget-object v0, Lnet/flixster/android/model/LockerRight$Source;->UNKNOWN:Lnet/flixster/android/model/LockerRight$Source;

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$Source;
    .locals 1
    .parameter

    .prologue
    .line 1
    const-class v0, Lnet/flixster/android/model/LockerRight$Source;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight$Source;

    return-object v0
.end method

.method public static values()[Lnet/flixster/android/model/LockerRight$Source;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1
    sget-object v0, Lnet/flixster/android/model/LockerRight$Source;->ENUM$VALUES:[Lnet/flixster/android/model/LockerRight$Source;

    array-length v1, v0

    new-array v2, v1, [Lnet/flixster/android/model/LockerRight$Source;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
