.class public Lnet/flixster/android/model/LockerRight;
.super Ljava/lang/Object;
.source "LockerRight.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/model/LockerRight$PurchaseType;,
        Lnet/flixster/android/model/LockerRight$RightType;,
        Lnet/flixster/android/model/LockerRight$Source;
    }
.end annotation


# static fields
.field private static final POSTER_NONE_SUBSTRING:Ljava/lang/String; = "movie.none."

.field private static SEASON_RIGHT_ID_BASE:J


# instance fields
.field private asset:Lnet/flixster/android/model/VideoAsset;

.field public final assetId:J

.field private childAssetToRightIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private detailedImage:Lcom/flixster/android/model/Image;

.field protected final detailedUrl:Ljava/lang/String;

.field private downloadCaptionsUri:Ljava/lang/String;

.field private downloadSize:Ljava/lang/String;

.field private downloadSizeRaw:I

.field protected downloadUri:Ljava/lang/String;

.field private downloadedSize:Ljava/lang/String;

.field private downloadsRemain:I

.field public final isDownloadSupported:Z

.field public isRental:Z

.field public final isStreamingSupported:Z

.field private profileImage:Lcom/flixster/android/model/Image;

.field protected final profileUrl:Ljava/lang/String;

.field public purchaseType:Lnet/flixster/android/model/LockerRight$PurchaseType;

.field public rentalExpiration:Ljava/util/Date;

.field public final rightId:J

.field protected final rightTitle:Ljava/lang/String;

.field public source:Lnet/flixster/android/model/LockerRight$Source;

.field private thumbnailImage:Lcom/flixster/android/model/Image;

.field protected final thumbnailUrl:Ljava/lang/String;

.field protected final type:Lnet/flixster/android/model/LockerRight$RightType;

.field public viewingExpiration:Ljava/util/Date;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 440
    const-wide v0, 0xb1a2bc2ec500000L

    sput-wide v0, Lnet/flixster/android/model/LockerRight;->SEASON_RIGHT_ID_BASE:J

    .line 25
    return-void
.end method

.method private constructor <init>(JLjava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .parameter "rightId"
    .parameter "rightTitle"
    .parameter "rightType"
    .parameter "assetId"

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    iput v1, p0, Lnet/flixster/android/model/LockerRight;->downloadsRemain:I

    .line 150
    iput-wide p1, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    .line 151
    iput-object p3, p0, Lnet/flixster/android/model/LockerRight;->rightTitle:Ljava/lang/String;

    .line 152
    invoke-static {p4}, Lnet/flixster/android/model/LockerRight$RightType;->match(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$RightType;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    .line 153
    iput-wide p5, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    .line 154
    iput-object v2, p0, Lnet/flixster/android/model/LockerRight;->source:Lnet/flixster/android/model/LockerRight$Source;

    .line 155
    iput-boolean v1, p0, Lnet/flixster/android/model/LockerRight;->isStreamingSupported:Z

    .line 156
    iput-boolean v1, p0, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    .line 157
    iput-object v2, p0, Lnet/flixster/android/model/LockerRight;->detailedUrl:Ljava/lang/String;

    iput-object v2, p0, Lnet/flixster/android/model/LockerRight;->profileUrl:Ljava/lang/String;

    iput-object v2, p0, Lnet/flixster/android/model/LockerRight;->thumbnailUrl:Ljava/lang/String;

    .line 158
    return-void
.end method

.method public constructor <init>(Lorg/json/JSONObject;JLnet/flixster/android/model/LockerRight$RightType;Lnet/flixster/android/model/VideoAsset;)V
    .locals 9
    .parameter "right"
    .parameter "assetId"
    .parameter "type"
    .parameter "asset"

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 311
    const/4 v4, 0x1

    iput v4, p0, Lnet/flixster/android/model/LockerRight;->downloadsRemain:I

    .line 98
    const-string v4, "id"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8, p4}, Lnet/flixster/android/model/LockerRight;->getRightId(JLnet/flixster/android/model/LockerRight$RightType;)J

    move-result-wide v7

    iput-wide v7, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    .line 99
    const-string v4, "title"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/model/LockerRight;->rightTitle:Ljava/lang/String;

    .line 100
    iput-wide p2, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    .line 101
    iput-object p4, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    .line 102
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v4

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    iput-boolean v4, p0, Lnet/flixster/android/model/LockerRight;->isStreamingSupported:Z

    .line 103
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v4

    if-eqz v4, :cond_2

    :goto_1
    iput-boolean v5, p0, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    .line 104
    const-string v4, "source"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lnet/flixster/android/model/LockerRight$Source;->match(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$Source;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/model/LockerRight;->source:Lnet/flixster/android/model/LockerRight$Source;

    .line 105
    const-string v4, "purchaseType"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lnet/flixster/android/model/LockerRight$PurchaseType;->match(Ljava/lang/String;)Lnet/flixster/android/model/LockerRight$PurchaseType;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/model/LockerRight;->purchaseType:Lnet/flixster/android/model/LockerRight$PurchaseType;

    .line 106
    const-string v4, "1"

    const-string v5, "isRental"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, p0, Lnet/flixster/android/model/LockerRight;->isRental:Z

    .line 107
    iget-boolean v4, p0, Lnet/flixster/android/model/LockerRight;->isRental:Z

    if-eqz v4, :cond_0

    .line 108
    const-string v4, "viewingExpiration"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 109
    .local v3, viewingExpirationString:Ljava/lang/String;
    const-string v4, "rentalExpiration"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 110
    .local v1, rentalExpirationString:Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 111
    invoke-static {v3}, Lnet/flixster/android/model/LockerRight;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/model/LockerRight;->viewingExpiration:Ljava/util/Date;

    .line 124
    .end local v1           #rentalExpirationString:Ljava/lang/String;
    .end local v3           #viewingExpirationString:Ljava/lang/String;
    :cond_0
    :goto_2
    const-string v4, "poster"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 125
    .local v0, poster:Lorg/json/JSONObject;
    if-eqz v0, :cond_9

    .line 127
    const-string v4, "thumbnail"

    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 128
    .local v2, temp:Ljava/lang/String;
    if-eqz v2, :cond_6

    const-string v4, "movie.none."

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .end local v2           #temp:Ljava/lang/String;
    :goto_3
    iput-object v2, p0, Lnet/flixster/android/model/LockerRight;->thumbnailUrl:Ljava/lang/String;

    .line 129
    const-string v4, "profile"

    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 130
    .restart local v2       #temp:Ljava/lang/String;
    if-eqz v2, :cond_7

    const-string v4, "movie.none."

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_7

    .end local v2           #temp:Ljava/lang/String;
    :goto_4
    iput-object v2, p0, Lnet/flixster/android/model/LockerRight;->profileUrl:Ljava/lang/String;

    .line 131
    const-string v4, "detailed"

    invoke-virtual {v0, v4, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 132
    .restart local v2       #temp:Ljava/lang/String;
    if-eqz v2, :cond_8

    const-string v4, "movie.none."

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    .end local v2           #temp:Ljava/lang/String;
    :goto_5
    iput-object v2, p0, Lnet/flixster/android/model/LockerRight;->detailedUrl:Ljava/lang/String;

    .line 146
    :goto_6
    iput-object p5, p0, Lnet/flixster/android/model/LockerRight;->asset:Lnet/flixster/android/model/VideoAsset;

    .line 147
    return-void

    .line 102
    .end local v0           #poster:Lorg/json/JSONObject;
    :cond_1
    const-string v4, "streamingSupported"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v4

    goto/16 :goto_0

    .line 103
    :cond_2
    const-string v4, "downloadSupported"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v5

    goto/16 :goto_1

    .line 112
    .restart local v1       #rentalExpirationString:Ljava/lang/String;
    .restart local v3       #viewingExpirationString:Ljava/lang/String;
    :cond_3
    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 113
    invoke-static {v1}, Lnet/flixster/android/model/LockerRight;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/model/LockerRight;->rentalExpiration:Ljava/util/Date;

    goto :goto_2

    .line 114
    :cond_4
    const-string v4, ""

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 115
    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 116
    invoke-static {v1}, Lnet/flixster/android/model/LockerRight;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/model/LockerRight;->rentalExpiration:Ljava/util/Date;

    goto/16 :goto_2

    .line 118
    :cond_5
    invoke-static {v3}, Lnet/flixster/android/model/LockerRight;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v4

    iput-object v4, p0, Lnet/flixster/android/model/LockerRight;->viewingExpiration:Ljava/util/Date;

    goto/16 :goto_2

    .end local v1           #rentalExpirationString:Ljava/lang/String;
    .end local v3           #viewingExpirationString:Ljava/lang/String;
    .restart local v0       #poster:Lorg/json/JSONObject;
    .restart local v2       #temp:Ljava/lang/String;
    :cond_6
    move-object v2, v6

    .line 128
    goto :goto_3

    :cond_7
    move-object v2, v6

    .line 130
    goto :goto_4

    :cond_8
    move-object v2, v6

    .line 132
    goto :goto_5

    .line 134
    .end local v2           #temp:Ljava/lang/String;
    :cond_9
    iput-object v6, p0, Lnet/flixster/android/model/LockerRight;->detailedUrl:Ljava/lang/String;

    iput-object v6, p0, Lnet/flixster/android/model/LockerRight;->profileUrl:Ljava/lang/String;

    iput-object v6, p0, Lnet/flixster/android/model/LockerRight;->thumbnailUrl:Ljava/lang/String;

    goto :goto_6
.end method

.method private getAssetReference()Lnet/flixster/android/model/Movie;
    .locals 2

    .prologue
    .line 212
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isMovie()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-static {v0, v1}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isSeason()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-static {v0, v1}, Lnet/flixster/android/data/MovieDao;->getSeason(J)Lnet/flixster/android/model/Season;

    move-result-object v0

    goto :goto_0

    .line 213
    :cond_1
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isUnfulfillable()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-static {v0, v1}, Lnet/flixster/android/data/MovieDao;->getUnfulfillable(J)Lnet/flixster/android/model/Unfulfillable;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getMockInstance(JLjava/lang/String;Ljava/lang/String;J)Lnet/flixster/android/model/LockerRight;
    .locals 7
    .parameter "rightId"
    .parameter "rightTitle"
    .parameter "rightType"
    .parameter "assetId"

    .prologue
    .line 161
    new-instance v0, Lnet/flixster/android/model/LockerRight;

    move-wide v1, p0

    move-object v3, p2

    move-object v4, p3

    move-wide v5, p4

    invoke-direct/range {v0 .. v6}, Lnet/flixster/android/model/LockerRight;-><init>(JLjava/lang/String;Ljava/lang/String;J)V

    return-object v0
.end method

.method public static getRawSeasonRightId(J)J
    .locals 2
    .parameter "rightId"

    .prologue
    .line 447
    sget-wide v0, Lnet/flixster/android/model/LockerRight;->SEASON_RIGHT_ID_BASE:J

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    sget-wide v0, Lnet/flixster/android/model/LockerRight;->SEASON_RIGHT_ID_BASE:J

    sub-long/2addr p0, v0

    .end local p0
    :cond_0
    return-wide p0
.end method

.method private static getRightId(JLnet/flixster/android/model/LockerRight$RightType;)J
    .locals 2
    .parameter "rawRightId"
    .parameter "type"

    .prologue
    .line 443
    sget-object v0, Lnet/flixster/android/model/LockerRight$RightType;->SEASON:Lnet/flixster/android/model/LockerRight$RightType;

    if-ne p2, v0, :cond_0

    sget-wide v0, Lnet/flixster/android/model/LockerRight;->SEASON_RIGHT_ID_BASE:J

    add-long/2addr p0, v0

    .end local p0
    :cond_0
    return-wide p0
.end method

.method private static parse(Ljava/lang/String;)Ljava/util/Date;
    .locals 4
    .parameter "input"

    .prologue
    .line 451
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd HH:mm:ss z"

    invoke-direct {v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 452
    .local v2, sdf:Ljava/text/SimpleDateFormat;
    const/4 v0, 0x0

    .line 454
    .local v0, date:Ljava/util/Date;
    :try_start_0
    invoke-virtual {v2, p0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 458
    :goto_0
    return-object v0

    .line 455
    :catch_0
    move-exception v1

    .line 456
    .local v1, e:Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public addChildRightId(JJ)V
    .locals 3
    .parameter "rightId"
    .parameter "assetId"

    .prologue
    .line 186
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->childAssetToRightIdMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lnet/flixster/android/model/LockerRight;->childAssetToRightIdMap:Ljava/util/HashMap;

    .line 189
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->childAssetToRightIdMap:Ljava/util/HashMap;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    return-void
.end method

.method public getAsset()Lnet/flixster/android/model/Movie;
    .locals 1

    .prologue
    .line 208
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isEpisode()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->asset:Lnet/flixster/android/model/VideoAsset;

    check-cast v0, Lnet/flixster/android/model/Movie;

    goto :goto_0
.end method

.method public getChildRightId(J)J
    .locals 5
    .parameter "assetId"

    .prologue
    const-wide/16 v1, 0x0

    .line 198
    iget-object v3, p0, Lnet/flixster/android/model/LockerRight;->childAssetToRightIdMap:Ljava/util/HashMap;

    if-nez v3, :cond_1

    .line 202
    :cond_0
    :goto_0
    return-wide v1

    .line 201
    :cond_1
    iget-object v3, p0, Lnet/flixster/android/model/LockerRight;->childAssetToRightIdMap:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 202
    .local v0, rightId:Ljava/lang/Long;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    goto :goto_0
.end method

.method public getChildRightIds()Ljava/util/Collection;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->childAssetToRightIdMap:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->childAssetToRightIdMap:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public getDetailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 276
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->detailedImage:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_0

    .line 277
    new-instance v0, Lcom/flixster/android/model/Image;

    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getDetailPoster()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/LockerRight;->detailedImage:Lcom/flixster/android/model/Image;

    .line 279
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->detailedImage:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getDetailBitmap(Lcom/flixster/android/activity/ImageViewHandler;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter "callback"

    .prologue
    .line 284
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->detailedImage:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_0

    .line 285
    new-instance v0, Lcom/flixster/android/model/Image;

    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getDetailPoster()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/LockerRight;->detailedImage:Lcom/flixster/android/model/Image;

    .line 287
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->detailedImage:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Lcom/flixster/android/activity/ImageViewHandler;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getDetailPoster()Ljava/lang/String;
    .locals 2

    .prologue
    .line 236
    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->detailedUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 237
    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->detailedUrl:Ljava/lang/String;

    .line 243
    :goto_0
    return-object v1

    .line 239
    :cond_0
    invoke-direct {p0}, Lnet/flixster/android/model/LockerRight;->getAssetReference()Lnet/flixster/android/model/Movie;

    move-result-object v0

    .line 240
    .local v0, m:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_1

    .line 241
    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getDetailPoster()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 243
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDownloadAssetBitrate()J
    .locals 2

    .prologue
    .line 352
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isHoneycombTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->isSonicProxyMode()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0xf4240

    :goto_0
    int-to-long v0, v0

    return-wide v0

    :cond_0
    const v0, 0xaae60

    goto :goto_0
.end method

.method public getDownloadAssetSize()Ljava/lang/String;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->downloadSize:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadAssetSizeRaw()J
    .locals 2

    .prologue
    .line 327
    iget v0, p0, Lnet/flixster/android/model/LockerRight;->downloadSizeRaw:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getDownloadCaptionsUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->downloadCaptionsUri:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->downloadUri:Ljava/lang/String;

    return-object v0
.end method

.method public getDownloadedAssetSize()Ljava/lang/String;
    .locals 4

    .prologue
    .line 336
    iget-object v2, p0, Lnet/flixster/android/model/LockerRight;->downloadedSize:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 337
    iget-wide v2, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-static {v2, v3}, Lcom/flixster/android/net/DownloadHelper;->findDownloadedMovieSize(J)J

    move-result-wide v0

    .line 338
    .local v0, size:J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/flixster/android/net/DownloadHelper;->readableFileSize(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lnet/flixster/android/model/LockerRight;->downloadedSize:Ljava/lang/String;

    .line 340
    .end local v0           #size:J
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/model/LockerRight;->downloadedSize:Ljava/lang/String;

    return-object v2

    .line 338
    .restart local v0       #size:J
    :cond_1
    const-string v2, ""

    goto :goto_0
.end method

.method public getDownloadsRemain()I
    .locals 1

    .prologue
    .line 344
    iget v0, p0, Lnet/flixster/android/model/LockerRight;->downloadsRemain:I

    return v0
.end method

.method public getEstimatedDownloadFileSize()I
    .locals 14

    .prologue
    .line 356
    iget-object v10, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    sget-object v11, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    if-ne v10, v11, :cond_2

    .line 357
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getAsset()Lnet/flixster/android/model/Movie;

    move-result-object v4

    .line 358
    .local v4, m:Lnet/flixster/android/model/Movie;
    const-string v10, "runningTime"

    invoke-virtual {v4, v10}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 359
    .local v7, runningTime:Ljava/lang/String;
    const-wide/16 v2, 0x0

    .local v2, hours:J
    const-wide/16 v5, 0x0

    .line 360
    .local v5, minutes:J
    if-eqz v7, :cond_1

    .line 361
    const-string v10, "hr."

    invoke-virtual {v7, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 362
    const/4 v10, 0x0

    const-string v11, " "

    invoke-virtual {v7, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    int-to-long v2, v10

    .line 363
    const-string v10, "min."

    invoke-virtual {v7, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 364
    const-string v10, "."

    invoke-virtual {v7, v10}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v10

    add-int/lit8 v10, v10, 0x2

    invoke-virtual {v7, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    .line 367
    :cond_0
    const-string v10, "min."

    invoke-virtual {v7, v10}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 368
    const/4 v10, 0x0

    const-string v11, " "

    invoke-virtual {v7, v11}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v11

    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    int-to-long v5, v10

    .line 372
    :cond_1
    const-wide/16 v10, 0x3c

    mul-long/2addr v10, v2

    add-long/2addr v10, v5

    const-wide/16 v12, 0x3c

    mul-long v8, v10, v12

    .line 373
    .local v8, seconds:J
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetBitrate()J

    move-result-wide v10

    mul-long/2addr v10, v8

    long-to-double v10, v10

    const-wide v12, 0x3ff2b851eb851eb8L

    mul-double/2addr v10, v12

    const-wide/high16 v12, 0x4020

    div-double/2addr v10, v12

    double-to-long v0, v10

    .line 374
    .local v0, estimatedSize:J
    long-to-int v10, v0

    .line 387
    .end local v0           #estimatedSize:J
    .end local v2           #hours:J
    .end local v4           #m:Lnet/flixster/android/model/Movie;
    .end local v5           #minutes:J
    .end local v7           #runningTime:Ljava/lang/String;
    .end local v8           #seconds:J
    :goto_0
    return v10

    .line 375
    :cond_2
    iget-object v10, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    sget-object v11, Lnet/flixster/android/model/LockerRight$RightType;->EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

    if-ne v10, v11, :cond_3

    .line 376
    const-wide/16 v2, 0x0

    .restart local v2       #hours:J
    const-wide/16 v5, 0x2d

    .line 383
    .restart local v5       #minutes:J
    const-wide/16 v10, 0x3c

    mul-long/2addr v10, v2

    add-long/2addr v10, v5

    const-wide/16 v12, 0x3c

    mul-long v8, v10, v12

    .line 384
    .restart local v8       #seconds:J
    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getDownloadAssetBitrate()J

    move-result-wide v10

    mul-long/2addr v10, v8

    long-to-double v10, v10

    const-wide v12, 0x3ff2b851eb851eb8L

    mul-double/2addr v10, v12

    const-wide/high16 v12, 0x4020

    div-double/2addr v10, v12

    double-to-long v0, v10

    .line 385
    .restart local v0       #estimatedSize:J
    long-to-int v10, v0

    goto :goto_0

    .line 387
    .end local v0           #estimatedSize:J
    .end local v2           #hours:J
    .end local v5           #minutes:J
    .end local v8           #seconds:J
    :cond_3
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 269
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->profileImage:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_0

    .line 270
    new-instance v0, Lcom/flixster/android/model/Image;

    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getProfilePoster()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/LockerRight;->profileImage:Lcom/flixster/android/model/Image;

    .line 272
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->profileImage:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getProfilePoster()Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->profileUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 250
    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->profileUrl:Ljava/lang/String;

    .line 256
    :goto_0
    return-object v1

    .line 252
    :cond_0
    invoke-direct {p0}, Lnet/flixster/android/model/LockerRight;->getAssetReference()Lnet/flixster/android/model/Movie;

    move-result-object v0

    .line 253
    .local v0, m:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_1

    .line 254
    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getProfilePoster()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 256
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRentalExpirationString(Z)Ljava/lang/String;
    .locals 20
    .parameter "useShortened"

    .prologue
    .line 417
    const/4 v6, 0x0

    .line 418
    .local v6, expirationText:Ljava/lang/String;
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    invoke-direct {v1, v13, v14}, Ljava/util/Date;-><init>(J)V

    .line 419
    .local v1, currentDate:Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v13, v0, Lnet/flixster/android/model/LockerRight;->rentalExpiration:Ljava/util/Date;

    if-eqz v13, :cond_0

    .line 420
    move-object/from16 v0, p0

    iget-object v13, v0, Lnet/flixster/android/model/LockerRight;->rentalExpiration:Ljava/util/Date;

    invoke-virtual {v13}, Ljava/util/Date;->getTime()J

    move-result-wide v13

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v15

    sub-long v4, v13, v15

    .line 421
    .local v4, diff:J
    const-wide/16 v13, 0x0

    cmp-long v13, v4, v13

    if-lez v13, :cond_0

    .line 422
    const-wide/16 v13, 0x3e8

    div-long v11, v4, v13

    .line 423
    .local v11, seconds:J
    const-wide/16 v13, 0x3c

    div-long v9, v11, v13

    .line 424
    .local v9, minutes:J
    const-wide/16 v13, 0x3c

    div-long v7, v9, v13

    .line 425
    .local v7, hours:J
    const-wide/16 v13, 0x18

    div-long v2, v7, v13

    .line 428
    .local v2, days:J
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v13

    .line 429
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 430
    if-eqz p1, :cond_1

    const v13, 0x7f0c0181

    :goto_0
    invoke-virtual {v14, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v13, 0x1

    new-array v15, v13, [Ljava/lang/Object;

    const/16 v16, 0x0

    .line 431
    const-wide/16 v17, 0x0

    cmp-long v13, v2, v17

    if-nez v13, :cond_4

    const-wide/16 v17, 0x0

    cmp-long v13, v7, v17

    if-nez v13, :cond_3

    const-wide/16 v17, 0x0

    cmp-long v13, v9, v17

    if-nez v13, :cond_2

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v17, " sec"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 432
    :goto_1
    aput-object v13, v15, v16

    .line 427
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 435
    .end local v2           #days:J
    .end local v4           #diff:J
    .end local v7           #hours:J
    .end local v9           #minutes:J
    .end local v11           #seconds:J
    :cond_0
    return-object v6

    .line 430
    .restart local v2       #days:J
    .restart local v4       #diff:J
    .restart local v7       #hours:J
    .restart local v9       #minutes:J
    .restart local v11       #seconds:J
    :cond_1
    const v13, 0x7f0c0180

    goto :goto_0

    .line 431
    :cond_2
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v17, " min"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    :cond_3
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v13, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 432
    const-string v17, " hr"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    :cond_4
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, v17

    invoke-direct {v0, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v18, 0x1

    cmp-long v13, v2, v18

    if-nez v13, :cond_5

    const-string v13, " day"

    :goto_2
    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_1

    :cond_5
    const-string v13, " days"

    goto :goto_2
.end method

.method public getRightType()Lnet/flixster/android/model/LockerRight$RightType;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    return-object v0
.end method

.method public getThumbnailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->thumbnailImage:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_0

    .line 263
    new-instance v0, Lcom/flixster/android/model/Image;

    invoke-virtual {p0}, Lnet/flixster/android/model/LockerRight;->getThumbnailPoster()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/LockerRight;->thumbnailImage:Lcom/flixster/android/model/Image;

    .line 265
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->thumbnailImage:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getThumbnailPoster()Ljava/lang/String;
    .locals 2

    .prologue
    .line 223
    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->thumbnailUrl:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 224
    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->thumbnailUrl:Ljava/lang/String;

    .line 230
    :goto_0
    return-object v1

    .line 226
    :cond_0
    invoke-direct {p0}, Lnet/flixster/android/model/LockerRight;->getAssetReference()Lnet/flixster/android/model/Movie;

    move-result-object v0

    .line 227
    .local v0, m:Lnet/flixster/android/model/Movie;
    if-eqz v0, :cond_1

    .line 228
    invoke-virtual {v0}, Lnet/flixster/android/model/Movie;->getThumbnailPoster()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 230
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 219
    const-string v0, ""

    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->rightTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->rightTitle:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->asset:Lnet/flixster/android/model/VideoAsset;

    invoke-virtual {v0}, Lnet/flixster/android/model/VideoAsset;->getTitle()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewingExpirationString()Ljava/lang/String;
    .locals 17

    .prologue
    .line 400
    const/4 v4, 0x0

    .line 401
    .local v4, expirationText:Ljava/lang/String;
    new-instance v1, Ljava/util/Date;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    invoke-direct {v1, v11, v12}, Ljava/util/Date;-><init>(J)V

    .line 402
    .local v1, currentDate:Ljava/util/Date;
    move-object/from16 v0, p0

    iget-object v11, v0, Lnet/flixster/android/model/LockerRight;->viewingExpiration:Ljava/util/Date;

    if-eqz v11, :cond_0

    .line 403
    move-object/from16 v0, p0

    iget-object v11, v0, Lnet/flixster/android/model/LockerRight;->viewingExpiration:Ljava/util/Date;

    invoke-virtual {v11}, Ljava/util/Date;->getTime()J

    move-result-wide v11

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v13

    sub-long v2, v11, v13

    .line 404
    .local v2, diff:J
    const-wide/16 v11, 0x0

    cmp-long v11, v2, v11

    if-lez v11, :cond_0

    .line 405
    const-wide/16 v11, 0x3e8

    div-long v9, v2, v11

    .line 406
    .local v9, seconds:J
    const-wide/16 v11, 0x3c

    div-long v7, v9, v11

    .line 407
    .local v7, minutes:J
    const-wide/16 v11, 0x3c

    div-long v5, v7, v11

    .line 409
    .local v5, hours:J
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0c017f

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v11, 0x1

    new-array v13, v11, [Ljava/lang/Object;

    const/4 v14, 0x0

    .line 410
    const-wide/16 v15, 0x0

    cmp-long v11, v5, v15

    if-nez v11, :cond_2

    const-wide/16 v15, 0x0

    cmp-long v11, v7, v15

    if-nez v11, :cond_1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, " sec"

    invoke-virtual {v11, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :goto_0
    aput-object v11, v13, v14

    .line 408
    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 413
    .end local v2           #diff:J
    .end local v5           #hours:J
    .end local v7           #minutes:J
    .end local v9           #seconds:J
    :cond_0
    return-object v4

    .line 410
    .restart local v2       #diff:J
    .restart local v5       #hours:J
    .restart local v7       #minutes:J
    .restart local v9       #seconds:J
    :cond_1
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, " min"

    invoke-virtual {v11, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_0

    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v15}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v15, " hr"

    invoke-virtual {v11, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_0
.end method

.method public isEpisode()Z
    .locals 2

    .prologue
    .line 173
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMovie()Z
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->MOVIE:Lnet/flixster/android/model/LockerRight$RightType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSeason()Z
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->SEASON:Lnet/flixster/android/model/LockerRight$RightType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSonicProxyMode()Z
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->source:Lnet/flixster/android/model/LockerRight$Source;

    sget-object v1, Lnet/flixster/android/model/LockerRight$Source;->SONIC:Lnet/flixster/android/model/LockerRight$Source;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUnfulfillable()Z
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    sget-object v1, Lnet/flixster/android/model/LockerRight$RightType;->UNFULFILLABLE:Lnet/flixster/android/model/LockerRight$RightType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDownloadAssetSize(I)V
    .locals 3
    .parameter "size"

    .prologue
    .line 331
    if-lez p1, :cond_0

    move v0, p1

    :goto_0
    iput v0, p0, Lnet/flixster/android/model/LockerRight;->downloadSizeRaw:I

    .line 332
    if-lez p1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    int-to-long v1, p1

    invoke-static {v1, v2}, Lcom/flixster/android/net/DownloadHelper;->readableFileSize(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lnet/flixster/android/model/LockerRight;->downloadSize:Ljava/lang/String;

    .line 333
    return-void

    .line 331
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 332
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method public setDownloadCaptionsUri(Ljava/lang/String;)V
    .locals 0
    .parameter "captionsUri"

    .prologue
    .line 396
    iput-object p1, p0, Lnet/flixster/android/model/LockerRight;->downloadCaptionsUri:Ljava/lang/String;

    .line 397
    return-void
.end method

.method public setDownloadUri(Ljava/lang/String;)V
    .locals 0
    .parameter "uri"

    .prologue
    .line 319
    iput-object p1, p0, Lnet/flixster/android/model/LockerRight;->downloadUri:Ljava/lang/String;

    .line 320
    return-void
.end method

.method public setDownloadsRemain(I)V
    .locals 0
    .parameter "count"

    .prologue
    .line 348
    iput p1, p0, Lnet/flixster/android/model/LockerRight;->downloadsRemain:I

    .line 349
    return-void
.end method

.method public setSonicProxyMode()V
    .locals 1

    .prologue
    .line 304
    sget-object v0, Lnet/flixster/android/model/LockerRight$Source;->SONIC:Lnet/flixster/android/model/LockerRight$Source;

    iput-object v0, p0, Lnet/flixster/android/model/LockerRight;->source:Lnet/flixster/android/model/LockerRight$Source;

    .line 305
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{ rightId: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v1, p0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", assetId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v1, p0, Lnet/flixster/android/model/LockerRight;->assetId:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", source: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->source:Lnet/flixster/android/model/LockerRight$Source;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/LockerRight;->type:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 293
    const-string v1, ", isStreamingSupported: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/LockerRight;->isStreamingSupported:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isDownloadSupported: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/LockerRight;->isDownloadSupported:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 294
    const-string v1, " }"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 292
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
