.class public Lnet/flixster/android/model/Movie;
.super Lnet/flixster/android/model/VideoAsset;
.source "Movie.java"


# static fields
.field private static final BASIC_PROPERTY_ARRAY:[Ljava/lang/String; = null

.field public static final FRIENDS_AVERAGE:Ljava/lang/String; = "FRIENDS_AVERAGE"

.field public static final FRIENDS_NOT_INTERESTED_COUNT:Ljava/lang/String; = "FRIENDS_NOT_INTERESTED_COUNT"

.field public static final FRIENDS_RATED_COUNT:Ljava/lang/String; = "FRIENDS_RATED_COUNT"

.field public static final FRIENDS_WANT_TO_SEE_COUNT:Ljava/lang/String; = "FRIENDS_WTS_COUNT"

.field private static final FULL_PROPERTY_ARRAY:[Ljava/lang/String; = null

.field private static final FULL_PROPERTY_INT_ARRAY:[Ljava/lang/String; = null

.field public static final MOVIE_ACTORS:Ljava/lang/String; = "MOVIE_ACTORS"

.field public static final MOVIE_ACTORS_SHORT:Ljava/lang/String; = "MOVIE_ACTORS_SHORT"

.field public static final MOVIE_DETAILED_URL:Ljava/lang/String; = "detailed"

.field public static final MOVIE_DIRECTORS:Ljava/lang/String; = "directors"

.field public static final MOVIE_DVD_RELEASE_DATE_STRING:Ljava/lang/String; = "dvdReleaseDate"

.field public static final MOVIE_FEATUREID:Ljava/lang/String; = "featuredId"

.field public static final MOVIE_FLIXSTER_URL:Ljava/lang/String; = "flixster"

.field public static final MOVIE_GENRE:Ljava/lang/String; = "genre"

.field public static final MOVIE_GROSS_INT:Ljava/lang/String; = "boxOffice"

.field public static final MOVIE_ID:Ljava/lang/String; = "MOVIE_ID"

.field public static final MOVIE_IMDB_URL:Ljava/lang/String; = "imdb"

.field public static final MOVIE_META:Ljava/lang/String; = "meta"

.field public static final MOVIE_MPAA:Ljava/lang/String; = "mpaa"

.field public static final MOVIE_NETFLIX_URL:Ljava/lang/String; = "netflix"

.field private static final MOVIE_NONE_MOB:Ljava/lang/String; = "movie.none.mob.gif"

.field public static final MOVIE_NUM_REVIEWS:Ljava/lang/String; = "criticsNumReviews"

.field public static final MOVIE_PHOTO_URLS:Ljava/lang/String; = "photoUrls"

.field public static final MOVIE_POPCORN_SCORE_INT:Ljava/lang/String; = "popcornScore"

.field public static final MOVIE_POPCORN_SCORE_STRING:Ljava/lang/String; = "MOVIE_POPCORN_SCORE_STRING"

.field public static final MOVIE_PROFILE_URL:Ljava/lang/String; = "profile"

.field public static final MOVIE_RT_SCORE_INT:Ljava/lang/String; = "rottenTomatoes"

.field public static final MOVIE_RT_SCORE_STRING:Ljava/lang/String; = "MOVIE_RT_SCORE_STRING"

.field public static final MOVIE_RT_URL:Ljava/lang/String; = "rottentomatoes"

.field public static final MOVIE_RUNNINGTIME:Ljava/lang/String; = "runningTime"

.field public static final MOVIE_STATUS:Ljava/lang/String; = "status"

.field public static final MOVIE_SYNOPSIS:Ljava/lang/String; = "synopsis"

.field public static final MOVIE_THEATER_RELEASE_DATE_STRING:Ljava/lang/String; = "theaterReleaseDate"

.field public static final MOVIE_THUMBNAIL_URL:Ljava/lang/String; = "thumbnail"

.field public static final MOVIE_TITLE:Ljava/lang/String; = "title"

.field public static final MOVIE_TRAILER_HIGH_URL:Ljava/lang/String; = "high"

.field public static final MOVIE_TRAILER_LOW_URL:Ljava/lang/String; = "low"

.field public static final MOVIE_TRAILER_MED_URL:Ljava/lang/String; = "med"

.field public static final MOVIE_WTS_INT:Ljava/lang/String; = "numWantToSee"

.field private static final NETFLIX_BASIC_PROPERTY_ARRAY:[Ljava/lang/String; = null

.field public static final RATING_PG:Ljava/lang/String; = "PG"

.field public static final RATING_PG13:Ljava/lang/String; = "PG-13"

.field public static final RATING_R:Ljava/lang/String; = "R"

.field public static final RATING_UNRATED:Ljava/lang/String; = "Unrated"

.field public static final SQL_TABLE:Ljava/lang/String; = "movie_cache"

.field public static final STRING_ROTTEN_TOMATOES:Ljava/lang/String; = "Rotten Tomatoes"

.field public static final TAG_DRAMA:Ljava/lang/String; = "Drama"

.field public static final TAG_INACTIVE:Ljava/lang/String; = "Inactive"

.field public static final TAG_LIVE:Ljava/lang/String; = "Live"

.field public static final TAG_OPENING_THIS_WEEK:Ljava/lang/String; = "Opening This Week"

.field public static final TAG_PENDINGCONTENTSOURCE:Ljava/lang/String; = "Pending Content Source"

.field public static final TAG_ROMANCE:Ljava/lang/String; = "Romance"

.field public static final TAG_TOP_BOX_OFFICE:Ljava/lang/String; = "Top Box Office"

.field public static final THUMBNAIL_STATUS_INT:Ljava/lang/String; = "THUMBNAIL_STATUS_INT"

.field public static final TYPE_ROTTENTOMATOES:Ljava/lang/String; = "rottentomatoes"


# instance fields
.field private actorsHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private criticReviewsHash:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private criticReviewsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field private criticsNumReviews:I

.field private detailBitmap:Lcom/flixster/android/model/Image;

.field private directorsHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private dvdReleaseDate:Ljava/util/Date;

.field private friendReviewsHash:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private genres:[Ljava/lang/String;

.field private isCertifiedFresh:Z

.field public isMIT:Z

.field private isMovieDetailsApiParsed:Z

.field public isUpcoming:Z

.field public mActorPageChars:Ljava/lang/String;

.field public mActors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field public mDirectors:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Actor;",
            ">;"
        }
    .end annotation
.end field

.field private mFriendRatedReviews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field private mFriendWantToSeeReviews:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field private mIntPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public mPhotos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Photo;",
            ">;"
        }
    .end annotation
.end field

.field private mPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbOrderStamp:J

.field public photoCount:I

.field private profileBitmapNew:Lcom/flixster/android/model/Image;

.field private tagsParsed:Z

.field private theaterReleaseDate:Ljava/util/Date;

.field public thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private userNumRatings:I

.field private userReviewsHash:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private userReviewsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 94
    const/16 v0, 0x14

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "mpaa"

    aput-object v1, v0, v4

    const-string v1, "directors"

    aput-object v1, v0, v5

    const-string v1, "runningTime"

    aput-object v1, v0, v6

    .line 95
    const-string v1, "meta"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "MOVIE_ACTORS"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "MOVIE_ACTORS_SHORT"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "MOVIE_ACTORS_SHORT"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "status"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "genre"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    .line 96
    const-string v2, "thumbnail"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "flixster"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "imdb"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "rottentomatoes"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "profile"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    .line 97
    const-string v2, "theaterReleaseDate"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "dvdReleaseDate"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "high"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    .line 98
    const-string v2, "med"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "low"

    aput-object v2, v0, v1

    .line 94
    sput-object v0, Lnet/flixster/android/model/Movie;->FULL_PROPERTY_ARRAY:[Ljava/lang/String;

    .line 100
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "boxOffice"

    aput-object v1, v0, v3

    const-string v1, "popcornScore"

    aput-object v1, v0, v4

    .line 101
    const-string v1, "rottenTomatoes"

    aput-object v1, v0, v5

    const-string v1, "numWantToSee"

    aput-object v1, v0, v6

    const-string v1, "THUMBNAIL_STATUS_INT"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "featuredId"

    aput-object v2, v0, v1

    .line 100
    sput-object v0, Lnet/flixster/android/model/Movie;->FULL_PROPERTY_INT_ARRAY:[Ljava/lang/String;

    .line 103
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "mpaa"

    aput-object v1, v0, v4

    const-string v1, "runningTime"

    aput-object v1, v0, v5

    const-string v1, "synopsis"

    aput-object v1, v0, v6

    .line 104
    const-string v1, "status"

    aput-object v1, v0, v7

    .line 103
    sput-object v0, Lnet/flixster/android/model/Movie;->BASIC_PROPERTY_ARRAY:[Ljava/lang/String;

    .line 106
    new-array v0, v7, [Ljava/lang/String;

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "mpaa"

    aput-object v1, v0, v4

    const-string v1, "runningTime"

    aput-object v1, v0, v5

    .line 107
    const-string v1, "status"

    aput-object v1, v0, v6

    .line 106
    sput-object v0, Lnet/flixster/android/model/Movie;->NETFLIX_BASIC_PROPERTY_ARRAY:[Ljava/lang/String;

    .line 33
    return-void
.end method

.method public constructor <init>(J)V
    .locals 2
    .parameter "id"

    .prologue
    .line 148
    invoke-direct {p0}, Lnet/flixster/android/model/VideoAsset;-><init>()V

    .line 114
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lnet/flixster/android/model/Movie;->mThumbOrderStamp:J

    .line 118
    new-instance v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    .line 149
    iput-wide p1, p0, Lnet/flixster/android/model/Movie;->id:J

    .line 150
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    .line 152
    return-void
.end method

.method private arrayParseJSONObject(Lorg/json/JSONObject;[Ljava/lang/String;)V
    .locals 9
    .parameter "jsonObj"
    .parameter "stringArray"

    .prologue
    .line 289
    :try_start_0
    array-length v5, p2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v4, 0x0

    :goto_0
    if-lt v4, v5, :cond_0

    .line 340
    :goto_1
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->buildMeta()V

    .line 341
    return-void

    .line 289
    :cond_0
    :try_start_1
    aget-object v1, p2, v4

    .line 290
    .local v1, key:Ljava/lang/String;
    iget-object v6, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 291
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 292
    const-string v6, "synopsis"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 295
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 296
    .local v2, synopsis:Ljava/lang/String;
    iget-object v6, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v7, "&quot;"

    const-string v8, "\""

    invoke-virtual {v2, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v1, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 289
    .end local v2           #synopsis:Ljava/lang/String;
    :cond_1
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 300
    :cond_2
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 301
    .local v3, tempString:Ljava/lang/String;
    const-string v6, "status"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 302
    const-string v6, "Live"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 303
    const-string v3, "Live"

    .line 329
    :cond_3
    :goto_3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 330
    iget-object v6, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v6, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 337
    .end local v1           #key:Ljava/lang/String;
    .end local v3           #tempString:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 338
    .local v0, e:Lorg/json/JSONException;
    const-string v4, "FlxMain"

    const-string v5, "arrayParseError "

    invoke-static {v4, v5, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 304
    .end local v0           #e:Lorg/json/JSONException;
    .restart local v1       #key:Ljava/lang/String;
    .restart local v3       #tempString:Ljava/lang/String;
    :cond_4
    :try_start_2
    const-string v6, "Pending Content Source"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 305
    const-string v3, "Pending Content Source"

    goto :goto_3

    .line 306
    :cond_5
    const-string v6, "Inactive"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 307
    const-string v3, "Inactive"

    goto :goto_3

    .line 309
    :cond_6
    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 311
    :cond_7
    const-string v6, "mpaa"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 312
    const-string v6, "R"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 313
    const-string v3, "R"

    goto :goto_3

    .line 314
    :cond_8
    const-string v6, "Unrated"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 315
    const-string v3, "Unrated"

    goto :goto_3

    .line 316
    :cond_9
    const-string v6, "PG-13"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 317
    const-string v3, "PG-13"

    goto :goto_3

    .line 318
    :cond_a
    const-string v6, "PG"

    invoke-virtual {v3, v6}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 319
    const-string v3, "PG"

    goto :goto_3

    .line 321
    :cond_b
    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 324
    :cond_c
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 325
    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v3

    goto :goto_3
.end method

.method private netflixParseReviews(Lorg/json/JSONObject;)V
    .locals 8
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 896
    const-string v5, "reviews"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 897
    const-string v5, "reviews"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 898
    .local v1, jsonReviews:Lorg/json/JSONObject;
    const-string v5, "flixster"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 899
    const-string v5, "flixster"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 900
    .local v0, jsonFlixsterReviews:Lorg/json/JSONObject;
    const-string v5, "popcornScore"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 901
    .local v3, popcorn:I
    if-eqz v3, :cond_0

    .line 902
    iget-object v5, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v6, "popcornScore"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 904
    :cond_0
    iget-object v5, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v6, "numWantToSee"

    const-string v7, "numWantToSee"

    invoke-virtual {v0, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 906
    .end local v0           #jsonFlixsterReviews:Lorg/json/JSONObject;
    .end local v3           #popcorn:I
    :cond_1
    const-string v5, "rottenTomatoes"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 907
    const-string v5, "rottenTomatoes"

    invoke-virtual {v1, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 908
    .local v2, jsonRottenTomatoes:Lorg/json/JSONObject;
    const-string v5, "rating"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 909
    .local v4, rating:I
    if-eqz v4, :cond_2

    .line 910
    iget-object v5, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v6, "rottenTomatoes"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 915
    .end local v1           #jsonReviews:Lorg/json/JSONObject;
    .end local v2           #jsonRottenTomatoes:Lorg/json/JSONObject;
    .end local v4           #rating:I
    :cond_2
    return-void
.end method

.method private parseDirectors(Lorg/json/JSONObject;)V
    .locals 8
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 954
    iget-object v5, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v6, "directors"

    invoke-virtual {v5, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 955
    const-string v5, "directors"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 956
    const-string v5, "directors"

    invoke-virtual {p1, v5}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 957
    .local v1, directorList:Lorg/json/JSONArray;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 958
    .local v3, length:I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 959
    .local v0, directorBuilder:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 966
    iget-object v5, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v6, "directors"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 969
    .end local v0           #directorBuilder:Ljava/lang/StringBuilder;
    .end local v1           #directorList:Lorg/json/JSONArray;
    .end local v2           #i:I
    .end local v3           #length:I
    :cond_0
    return-void

    .line 960
    .restart local v0       #directorBuilder:Ljava/lang/StringBuilder;
    .restart local v1       #directorList:Lorg/json/JSONArray;
    .restart local v2       #i:I
    .restart local v3       #length:I
    :cond_1
    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 961
    .local v4, tempActorObj:Lorg/json/JSONObject;
    const-string v5, "name"

    invoke-virtual {v4, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 962
    add-int/lit8 v5, v2, 0x1

    if-ge v5, v3, :cond_2

    .line 963
    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 959
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private parseDvdReleaseDateString(Lorg/json/JSONObject;)V
    .locals 8
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 701
    iget-object v4, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v5, "dvdReleaseDate"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lnet/flixster/android/model/Movie;->dvdReleaseDate:Ljava/util/Date;

    if-nez v4, :cond_1

    .line 702
    :cond_0
    const-string v4, "dvdReleaseDate"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 703
    const-string v4, "dvdReleaseDate"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 704
    .local v1, jsonDvdRelease:Lorg/json/JSONObject;
    const-string v4, "year"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 705
    .local v3, year:Ljava/lang/String;
    const-string v4, "month"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 706
    .local v2, month:Ljava/lang/String;
    const-string v4, "day"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 710
    .local v0, day:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 711
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    .line 712
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 711
    iput-object v4, p0, Lnet/flixster/android/model/Movie;->dvdReleaseDate:Ljava/util/Date;

    .line 715
    iget-object v4, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v5, "dvdReleaseDate"

    .line 716
    invoke-static {}, Lcom/flixster/android/utils/DateTimeHelper;->mediumDateFormatter()Ljava/text/DateFormat;

    move-result-object v6

    iget-object v7, p0, Lnet/flixster/android/model/Movie;->dvdReleaseDate:Ljava/util/Date;

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 715
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 722
    .end local v0           #day:Ljava/lang/String;
    .end local v1           #jsonDvdRelease:Lorg/json/JSONObject;
    .end local v2           #month:Ljava/lang/String;
    .end local v3           #year:Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private parseFeatureId(Lorg/json/JSONObject;)V
    .locals 3
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 926
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "featuredId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 927
    const-string v0, "featuredId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v1, "featuredId"

    const-string v2, "featuredId"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 931
    :cond_0
    return-void
.end method

.method private parseFriends(Lorg/json/JSONObject;)V
    .locals 14
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 972
    const-string v10, "friends"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 973
    const-string v10, "friends"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 975
    .local v1, friendsObject:Lorg/json/JSONObject;
    if-eqz v1, :cond_3

    .line 976
    const-string v10, "stats"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 977
    const-string v10, "stats"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 978
    .local v8, statsObject:Lorg/json/JSONObject;
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v11, "FRIENDS_AVERAGE"

    const-string v12, "average"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 979
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v11, "FRIENDS_RATED_COUNT"

    const-string v12, "numRated"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 980
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v11, "FRIENDS_WTS_COUNT"

    const-string v12, "numWantToSee"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 981
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v11, "FRIENDS_NOT_INTERESTED_COUNT"

    const-string v12, "numNotInterested"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 983
    .end local v8           #statsObject:Lorg/json/JSONObject;
    :cond_0
    const-string v10, "ratings"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 984
    const-string v10, "ratings"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 986
    .local v0, friendReviewsArray:Lorg/json/JSONArray;
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mFriendWantToSeeReviews:Ljava/util/ArrayList;

    if-eqz v10, :cond_1

    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mFriendRatedReviews:Ljava/util/ArrayList;

    if-nez v10, :cond_2

    .line 987
    :cond_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lnet/flixster/android/model/Movie;->mFriendWantToSeeReviews:Ljava/util/ArrayList;

    .line 988
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    iput-object v10, p0, Lnet/flixster/android/model/Movie;->mFriendRatedReviews:Ljava/util/ArrayList;

    .line 989
    new-instance v10, Ljava/util/HashSet;

    invoke-direct {v10}, Ljava/util/HashSet;-><init>()V

    iput-object v10, p0, Lnet/flixster/android/model/Movie;->friendReviewsHash:Ljava/util/HashSet;

    .line 992
    :cond_2
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v10

    if-lt v2, v10, :cond_4

    .line 1037
    .end local v0           #friendReviewsArray:Lorg/json/JSONArray;
    .end local v1           #friendsObject:Lorg/json/JSONObject;
    .end local v2           #i:I
    :cond_3
    return-void

    .line 993
    .restart local v0       #friendReviewsArray:Lorg/json/JSONArray;
    .restart local v1       #friendsObject:Lorg/json/JSONObject;
    .restart local v2       #i:I
    :cond_4
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 994
    .local v6, reviewObject:Lorg/json/JSONObject;
    const-string v10, "id"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 995
    .local v3, id:Ljava/lang/String;
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->friendReviewsHash:Ljava/util/HashSet;

    invoke-virtual {v10, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_8

    .line 996
    new-instance v5, Lnet/flixster/android/model/Review;

    invoke-direct {v5}, Lnet/flixster/android/model/Review;-><init>()V

    .line 997
    .local v5, review:Lnet/flixster/android/model/Review;
    const/4 v10, 0x2

    iput v10, v5, Lnet/flixster/android/model/Review;->type:I

    .line 998
    iput-object v3, v5, Lnet/flixster/android/model/Review;->id:Ljava/lang/String;

    .line 999
    const-string v10, "user"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1000
    const-string v10, "user"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 1001
    .local v9, userObject:Lorg/json/JSONObject;
    const-string v10, "id"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    iput-wide v10, v5, Lnet/flixster/android/model/Review;->userId:J

    .line 1002
    const-string v10, "userName"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lnet/flixster/android/model/Review;->userName:Ljava/lang/String;

    .line 1003
    const-string v10, "firstName"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    .line 1004
    const-string v10, "lastName"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 1005
    iget-object v10, v5, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v11, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v12, " "

    invoke-direct {v10, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v12, "lastName"

    invoke-virtual {v9, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v11, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    .line 1007
    :cond_5
    const-string v10, "images"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1008
    const-string v10, "images"

    invoke-virtual {v9, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 1009
    .local v4, imagesObject:Lorg/json/JSONObject;
    const-string v10, "thumbnail"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 1011
    const-string v10, "thumbnail"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1010
    invoke-static {v10}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lnet/flixster/android/model/Review;->mugUrl:Ljava/lang/String;

    .line 1015
    .end local v4           #imagesObject:Lorg/json/JSONObject;
    .end local v9           #userObject:Lorg/json/JSONObject;
    :cond_6
    const-string v10, "review"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1016
    const-string v10, "review"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    iput-object v10, v5, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 1018
    :cond_7
    const-string v10, "score"

    invoke-virtual {v6, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 1019
    .local v7, score:Ljava/lang/String;
    const-string v10, "+"

    invoke-virtual {v7, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1020
    const-wide/high16 v10, 0x4016

    iput-wide v10, v5, Lnet/flixster/android/model/Review;->stars:D

    .line 1021
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mFriendWantToSeeReviews:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1031
    :goto_1
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->friendReviewsHash:Ljava/util/HashSet;

    invoke-virtual {v10, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 992
    .end local v5           #review:Lnet/flixster/android/model/Review;
    .end local v7           #score:Ljava/lang/String;
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 1022
    .restart local v5       #review:Lnet/flixster/android/model/Review;
    .restart local v7       #score:Ljava/lang/String;
    :cond_9
    const-string v10, "-"

    invoke-virtual {v7, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 1023
    const-wide/high16 v10, 0x4018

    iput-wide v10, v5, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_1

    .line 1024
    :cond_a
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_b

    .line 1025
    invoke-static {v7}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    iput-wide v10, v5, Lnet/flixster/android/model/Review;->stars:D

    .line 1026
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mFriendRatedReviews:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1028
    :cond_b
    const-wide/16 v10, 0x0

    iput-wide v10, v5, Lnet/flixster/android/model/Review;->stars:D

    .line 1029
    iget-object v10, p0, Lnet/flixster/android/model/Movie;->mFriendRatedReviews:Ljava/util/ArrayList;

    invoke-virtual {v10, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private parseGenre(Lorg/json/JSONObject;)V
    .locals 11
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 725
    iget-boolean v8, p0, Lnet/flixster/android/model/Movie;->tagsParsed:Z

    if-nez v8, :cond_0

    .line 726
    const-string v8, "tags"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 727
    const-string v8, "tags"

    invoke-virtual {p1, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 728
    .local v7, tagArray:Lorg/json/JSONArray;
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 729
    .local v3, length:I
    new-array v8, v3, [Ljava/lang/String;

    iput-object v8, p0, Lnet/flixster/android/model/Movie;->genres:[Ljava/lang/String;

    .line 730
    const/4 v0, 0x0

    .line 731
    .local v0, genresIndex:I
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 732
    .local v5, sb:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, i:I
    move v1, v0

    .end local v0           #genresIndex:I
    .local v1, genresIndex:I
    :goto_0
    if-lt v2, v3, :cond_1

    .line 748
    const/4 v8, 0x1

    iput-boolean v8, p0, Lnet/flixster/android/model/Movie;->tagsParsed:Z

    .line 751
    .end local v1           #genresIndex:I
    .end local v2           #i:I
    .end local v3           #length:I
    .end local v5           #sb:Ljava/lang/StringBuilder;
    .end local v7           #tagArray:Lorg/json/JSONArray;
    :cond_0
    return-void

    .line 733
    .restart local v1       #genresIndex:I
    .restart local v2       #i:I
    .restart local v3       #length:I
    .restart local v5       #sb:Ljava/lang/StringBuilder;
    .restart local v7       #tagArray:Lorg/json/JSONArray;
    :cond_1
    invoke-virtual {v7, v2}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 734
    .local v6, tag:Ljava/lang/String;
    const-string v8, "Top Box Office"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 735
    iget-object v8, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v9, "Top Box Office"

    const-string v10, "Top Box Office"

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 746
    .end local v1           #genresIndex:I
    .restart local v0       #genresIndex:I
    :goto_1
    iget-object v8, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v9, "genre"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 732
    add-int/lit8 v2, v2, 0x1

    move v1, v0

    .end local v0           #genresIndex:I
    .restart local v1       #genresIndex:I
    goto :goto_0

    .line 736
    :cond_2
    const-string v8, "Opening This Week"

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 737
    iget-object v8, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v9, "Opening This Week"

    const-string v10, "Opening This Week"

    invoke-virtual {v8, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .end local v1           #genresIndex:I
    .restart local v0       #genresIndex:I
    goto :goto_1

    .line 739
    .end local v0           #genresIndex:I
    .restart local v1       #genresIndex:I
    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_4

    .line 740
    const-string v8, ", "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 742
    :cond_4
    invoke-static {}, Lcom/flixster/android/utils/Translator;->instance()Lcom/flixster/android/utils/Translator;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/flixster/android/utils/Translator;->translateGenre(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 743
    .local v4, localizedGenre:Ljava/lang/String;
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 744
    iget-object v8, p0, Lnet/flixster/android/model/Movie;->genres:[Ljava/lang/String;

    add-int/lit8 v0, v1, 0x1

    .end local v1           #genresIndex:I
    .restart local v0       #genresIndex:I
    aput-object v4, v8, v1

    goto :goto_1
.end method

.method private parsePosterUrls(Lorg/json/JSONObject;)V
    .locals 4
    .parameter "poster"

    .prologue
    const/4 v3, 0x0

    .line 674
    if-eqz p1, :cond_0

    .line 675
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v1, "thumbnail"

    const-string v2, "thumbnail"

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 676
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v1, "profile"

    const-string v2, "profile"

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 677
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/utils/Properties;->isGoogleTv()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 678
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v1, "detailed"

    const-string v2, "detailed"

    invoke-virtual {p1, v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 681
    :cond_0
    return-void
.end method

.method private parseReviews(Lorg/json/JSONObject;)V
    .locals 28
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 755
    const-string v24, "reviews"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 756
    const-string v24, "reviews"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v9

    .line 757
    .local v9, jsonReviews:Lorg/json/JSONObject;
    const-string v24, "flixster"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 758
    const-string v24, "flixster"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 759
    .local v8, jsonFlixsterReviews:Lorg/json/JSONObject;
    const-string v24, "popcornScore"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v15

    .line 760
    .local v15, popcorn:I
    if-eqz v15, :cond_0

    .line 761
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    move-object/from16 v24, v0

    const-string v25, "popcornScore"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v24 .. v26}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 763
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    move-object/from16 v24, v0

    const-string v25, "numWantToSee"

    const-string v26, "numWantToSee"

    move-object/from16 v0, v26

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v26

    invoke-static/range {v26 .. v26}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v24 .. v26}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 764
    const-string v24, "numScores"

    move-object/from16 v0, v24

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v24

    const-string v25, "numWantToSee"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v25

    add-int v24, v24, v25

    .line 765
    const-string v25, "numNotInterested"

    move-object/from16 v0, v25

    invoke-virtual {v8, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v25

    add-int v24, v24, v25

    .line 764
    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lnet/flixster/android/model/Movie;->userNumRatings:I

    .line 767
    .end local v8           #jsonFlixsterReviews:Lorg/json/JSONObject;
    .end local v15           #popcorn:I
    :cond_1
    const-string v24, "rottenTomatoes"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 768
    const-string v24, "rottenTomatoes"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    .line 769
    .local v10, jsonRottenTomatoes:Lorg/json/JSONObject;
    const-string v24, "rating"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v17

    .line 770
    .local v17, rating:I
    if-eqz v17, :cond_2

    .line 771
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    move-object/from16 v24, v0

    const-string v25, "rottenTomatoes"

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    invoke-virtual/range {v24 .. v26}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 773
    :cond_2
    const-string v24, "certifiedFresh"

    move-object/from16 v0, v24

    invoke-virtual {v10, v0}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lnet/flixster/android/model/Movie;->isCertifiedFresh:Z

    .line 775
    .end local v10           #jsonRottenTomatoes:Lorg/json/JSONObject;
    .end local v17           #rating:I
    :cond_3
    const-string v24, "criticsNumReviews"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lnet/flixster/android/model/Movie;->criticsNumReviews:I

    .line 776
    const-string v24, "critics"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_7

    .line 777
    const-string v24, "critics"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 778
    .local v7, jsonCriticsReviews:Lorg/json/JSONArray;
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v13

    .line 780
    .local v13, length:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->criticReviewsList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    if-nez v24, :cond_4

    .line 781
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->criticReviewsList:Ljava/util/ArrayList;

    .line 783
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->criticReviewsList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    if-eqz v24, :cond_5

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->criticReviewsHash:Ljava/util/HashSet;

    move-object/from16 v24, v0

    if-nez v24, :cond_6

    .line 784
    :cond_5
    new-instance v24, Ljava/util/HashSet;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->criticReviewsHash:Ljava/util/HashSet;

    .line 787
    :cond_6
    const/4 v3, 0x0

    .local v3, i:I
    :goto_0
    if-lt v3, v13, :cond_a

    .line 840
    .end local v3           #i:I
    .end local v7           #jsonCriticsReviews:Lorg/json/JSONArray;
    .end local v13           #length:I
    :cond_7
    const-string v24, "recent"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_9

    .line 841
    const-string v24, "recent"

    move-object/from16 v0, v24

    invoke-virtual {v9, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v12

    .line 842
    .local v12, jsonUserReviews:Lorg/json/JSONArray;
    invoke-virtual {v12}, Lorg/json/JSONArray;->length()I

    move-result v13

    .line 844
    .restart local v13       #length:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->userReviewsList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    if-nez v24, :cond_8

    .line 845
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->userReviewsList:Ljava/util/ArrayList;

    .line 846
    new-instance v24, Ljava/util/HashSet;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashSet;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->userReviewsHash:Ljava/util/HashSet;

    .line 849
    :cond_8
    const/4 v3, 0x0

    .restart local v3       #i:I
    :goto_1
    if-lt v3, v13, :cond_10

    .line 892
    .end local v3           #i:I
    .end local v9           #jsonReviews:Lorg/json/JSONObject;
    .end local v12           #jsonUserReviews:Lorg/json/JSONArray;
    .end local v13           #length:I
    :cond_9
    return-void

    .line 788
    .restart local v3       #i:I
    .restart local v7       #jsonCriticsReviews:Lorg/json/JSONArray;
    .restart local v9       #jsonReviews:Lorg/json/JSONObject;
    .restart local v13       #length:I
    :cond_a
    invoke-virtual {v7, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 789
    .local v6, jsonCritic:Lorg/json/JSONObject;
    const-string v24, "name"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 792
    .local v14, name:Ljava/lang/String;
    const-string v24, "Rotten Tomatoes"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_e

    .line 793
    const-string v14, "Rotten Tomatoes"

    .line 799
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->criticReviewsHash:Ljava/util/HashSet;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_d

    const-string v24, "Rotten Tomatoes"

    move-object/from16 v0, v24

    invoke-virtual {v14, v0}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v24

    if-nez v24, :cond_d

    .line 800
    new-instance v16, Lnet/flixster/android/model/Review;

    invoke-direct/range {v16 .. v16}, Lnet/flixster/android/model/Review;-><init>()V

    .line 802
    .local v16, r:Lnet/flixster/android/model/Review;
    const-string v24, "source"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 804
    .local v21, source:Ljava/lang/String;
    const-string v24, "Rotten Tomatoes"

    move-object/from16 v0, v21

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_f

    .line 805
    const-string v21, "Rotten Tomatoes"

    .line 810
    :goto_3
    const-string v24, "review"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 811
    .local v18, review:Ljava/lang/String;
    const-string v24, "<em>"

    const-string v25, ""

    move-object/from16 v0, v18

    move-object/from16 v1, v24

    move-object/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    const-string v25, "</em>"

    const-string v26, ""

    invoke-virtual/range {v24 .. v26}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 812
    const/16 v22, 0x0

    .line 813
    .local v22, url:Ljava/lang/String;
    const-string v24, "url"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_b

    .line 814
    const-string v24, "url"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 816
    :cond_b
    const-string v24, "images"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 817
    const-string v24, "images"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 818
    .local v5, imagesObj:Lorg/json/JSONObject;
    const-string v24, "thumbnail"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 819
    const-string v24, "thumbnail"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/model/Review;->mugUrl:Ljava/lang/String;

    .line 823
    .end local v5           #imagesObj:Lorg/json/JSONObject;
    :cond_c
    const-string v24, "rating"

    move-object/from16 v0, v24

    invoke-virtual {v6, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v20

    .line 824
    .local v20, score:I
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, v16

    iput v0, v1, Lnet/flixster/android/model/Review;->type:I

    .line 825
    move-object/from16 v0, v16

    iput-object v14, v0, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    .line 827
    move-object/from16 v0, v21

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/model/Review;->source:Ljava/lang/String;

    .line 828
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 830
    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Lnet/flixster/android/model/Review;->score:I

    .line 831
    move-object/from16 v0, v22

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/model/Review;->url:Ljava/lang/String;

    .line 832
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->criticReviewsHash:Ljava/util/HashSet;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v14}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 833
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->criticReviewsList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    .end local v16           #r:Lnet/flixster/android/model/Review;
    .end local v18           #review:Ljava/lang/String;
    .end local v20           #score:I
    .end local v21           #source:Ljava/lang/String;
    .end local v22           #url:Ljava/lang/String;
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 795
    :cond_e
    invoke-static {v14}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_2

    .line 807
    .restart local v16       #r:Lnet/flixster/android/model/Review;
    .restart local v21       #source:Ljava/lang/String;
    :cond_f
    invoke-static/range {v21 .. v21}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    goto/16 :goto_3

    .line 850
    .end local v6           #jsonCritic:Lorg/json/JSONObject;
    .end local v7           #jsonCriticsReviews:Lorg/json/JSONArray;
    .end local v14           #name:Ljava/lang/String;
    .end local v16           #r:Lnet/flixster/android/model/Review;
    .end local v21           #source:Ljava/lang/String;
    .restart local v12       #jsonUserReviews:Lorg/json/JSONArray;
    :cond_10
    invoke-virtual {v12, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v11

    .line 851
    .local v11, jsonUserReview:Lorg/json/JSONObject;
    const-string v24, "id"

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 853
    .local v4, id:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->userReviewsHash:Ljava/util/HashSet;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v24

    if-nez v24, :cond_15

    .line 854
    const-string v24, "user"

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v23

    .line 855
    .local v23, userObj:Lorg/json/JSONObject;
    new-instance v16, Lnet/flixster/android/model/Review;

    invoke-direct/range {v16 .. v16}, Lnet/flixster/android/model/Review;-><init>()V

    .line 856
    .restart local v16       #r:Lnet/flixster/android/model/Review;
    const/16 v24, 0x1

    move/from16 v0, v24

    move-object/from16 v1, v16

    iput v0, v1, Lnet/flixster/android/model/Review;->type:I

    .line 857
    const-string v24, "id"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_11

    .line 858
    const-string v24, "id"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, v16

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->userId:J

    .line 860
    :cond_11
    const-string v24, "userName"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/model/Review;->userName:Ljava/lang/String;

    .line 861
    const-string v24, "%s %s"

    const/16 v25, 0x2

    move/from16 v0, v25

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v25, v0

    const/16 v26, 0x0

    const-string v27, "firstName"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    const/16 v26, 0x1

    const-string v27, "lastName"

    move-object/from16 v0, v23

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    aput-object v27, v25, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    .line 863
    const-string v24, "images"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_12

    .line 864
    const-string v24, "images"

    invoke-virtual/range {v23 .. v24}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 865
    .restart local v5       #imagesObj:Lorg/json/JSONObject;
    const-string v24, "thumbnail"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_12

    .line 866
    const-string v24, "thumbnail"

    move-object/from16 v0, v24

    invoke-virtual {v5, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/model/Review;->mugUrl:Ljava/lang/String;

    .line 870
    .end local v5           #imagesObj:Lorg/json/JSONObject;
    :cond_12
    const-string v24, "review"

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_13

    .line 871
    const-string v24, "review"

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 872
    const-string v25, "&quot;"

    const-string v26, "\""

    invoke-virtual/range {v24 .. v26}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v24

    .line 871
    move-object/from16 v0, v24

    move-object/from16 v1, v16

    iput-object v0, v1, Lnet/flixster/android/model/Review;->comment:Ljava/lang/String;

    .line 874
    :cond_13
    const-string v24, "score"

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_14

    .line 875
    const-string v24, "score"

    move-object/from16 v0, v24

    invoke-virtual {v11, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 876
    .local v19, s:Ljava/lang/String;
    const-string v24, "+"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_16

    .line 877
    const-wide/high16 v24, 0x4016

    move-wide/from16 v0, v24

    move-object/from16 v2, v16

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->stars:D

    .line 886
    .end local v19           #s:Ljava/lang/String;
    :cond_14
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->userReviewsHash:Ljava/util/HashSet;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 887
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->userReviewsList:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 849
    .end local v16           #r:Lnet/flixster/android/model/Review;
    .end local v23           #userObj:Lorg/json/JSONObject;
    :cond_15
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 878
    .restart local v16       #r:Lnet/flixster/android/model/Review;
    .restart local v19       #s:Ljava/lang/String;
    .restart local v23       #userObj:Lorg/json/JSONObject;
    :cond_16
    const-string v24, "-"

    move-object/from16 v0, v19

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v24

    if-eqz v24, :cond_17

    .line 879
    const-wide/high16 v24, 0x4018

    move-wide/from16 v0, v24

    move-object/from16 v2, v16

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_4

    .line 880
    :cond_17
    invoke-virtual/range {v19 .. v19}, Ljava/lang/String;->length()I

    move-result v24

    if-lez v24, :cond_18

    .line 881
    invoke-static/range {v19 .. v19}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, v16

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_4

    .line 883
    :cond_18
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, v16

    iput-wide v0, v2, Lnet/flixster/android/model/Review;->stars:D

    goto :goto_4
.end method

.method private parseTheaterReleaseDateString(Lorg/json/JSONObject;)V
    .locals 8
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 684
    iget-object v4, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v5, "theaterReleaseDate"

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lnet/flixster/android/model/Movie;->theaterReleaseDate:Ljava/util/Date;

    if-nez v4, :cond_1

    .line 685
    :cond_0
    const-string v4, "theaterReleaseDate"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 686
    const-string v4, "theaterReleaseDate"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 687
    .local v1, jsonTheaterRelease:Lorg/json/JSONObject;
    const-string v4, "year"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 688
    .local v3, year:Ljava/lang/String;
    const-string v4, "month"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 689
    .local v2, month:Ljava/lang/String;
    const-string v4, "day"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 690
    .local v0, day:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 691
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    .line 692
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    .line 691
    iput-object v4, p0, Lnet/flixster/android/model/Movie;->theaterReleaseDate:Ljava/util/Date;

    .line 693
    iget-object v4, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v5, "theaterReleaseDate"

    .line 694
    invoke-static {}, Lcom/flixster/android/utils/DateTimeHelper;->mediumDateFormatter()Ljava/text/DateFormat;

    move-result-object v6

    iget-object v7, p0, Lnet/flixster/android/model/Movie;->theaterReleaseDate:Ljava/util/Date;

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 693
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 698
    .end local v0           #day:Ljava/lang/String;
    .end local v1           #jsonTheaterRelease:Lorg/json/JSONObject;
    .end local v2           #month:Ljava/lang/String;
    .end local v3           #year:Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private parseTrailer(Lorg/json/JSONObject;)V
    .locals 4
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 935
    const-string v1, "trailer"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 936
    const-string v1, "trailer"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 937
    .local v0, jsonTrailerObj:Lorg/json/JSONObject;
    const-string v1, "high"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 938
    iget-object v1, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v2, "high"

    .line 939
    const-string v3, "high"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 938
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 941
    :cond_0
    const-string v1, "med"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 942
    iget-object v1, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v2, "med"

    .line 943
    const-string v3, "med"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 942
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 945
    :cond_1
    const-string v1, "low"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 946
    iget-object v1, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v2, "low"

    .line 947
    const-string v3, "low"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 946
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 951
    .end local v0           #jsonTrailerObj:Lorg/json/JSONObject;
    :cond_2
    return-void
.end method


# virtual methods
.method public IsDoubleclickDvd()Z
    .locals 6

    .prologue
    .line 219
    const/4 v2, 0x0

    .line 220
    .local v2, isDvd:Z
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getDvdReleaseDate()Ljava/util/Date;

    move-result-object v1

    .line 221
    .local v1, dvdDate:Ljava/util/Date;
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v3

    .line 222
    .local v3, theaterDate:Ljava/util/Date;
    if-eqz v1, :cond_1

    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v4, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v2, 0x1

    .line 223
    :goto_0
    if-nez v2, :cond_0

    if-eqz v3, :cond_0

    .line 224
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-direct {v0}, Ljava/util/GregorianCalendar;-><init>()V

    .line 225
    .local v0, c:Ljava/util/Calendar;
    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 226
    const/4 v4, 0x2

    const/4 v5, 0x3

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 227
    sget-object v4, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    .line 229
    .end local v0           #c:Ljava/util/Calendar;
    :cond_0
    return v2

    .line 222
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public buildMeta()V
    .locals 4

    .prologue
    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 156
    .local v0, metaList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "mpaa"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 157
    .local v1, prop:Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    .line 158
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 160
    :cond_0
    const-string v2, "runningTime"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 161
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    .line 162
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_1
    const-string v2, ", "

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    .line 165
    iget-object v2, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v3, "meta"

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 166
    return-void
.end method

.method public checkIntProperty(Ljava/lang/String;)Z
    .locals 1
    .parameter "propkey"

    .prologue
    .line 191
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public checkProperty(Ljava/lang/String;)Z
    .locals 1
    .parameter "propkey"

    .prologue
    .line 178
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getAudienceScore()I
    .locals 2

    .prologue
    .line 1115
    const-string v1, "popcornScore"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 1116
    .local v0, score:Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getAudienceScoreIconId()I
    .locals 1

    .prologue
    .line 1120
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->isReleased()Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0200f6

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->isSpilled()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0200ef

    goto :goto_0

    .line 1121
    :cond_1
    const v0, 0x7f0200ea

    goto :goto_0
.end method

.method public getCastShort()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1086
    const-string v0, "MOVIE_ACTORS_SHORT"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCriticReviewsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->criticReviewsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getDetailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 1203
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->detailBitmap:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_0

    .line 1204
    new-instance v0, Lcom/flixster/android/model/Image;

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getDetailPoster()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/Movie;->detailBitmap:Lcom/flixster/android/model/Image;

    .line 1206
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->detailBitmap:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getDetailBitmap(Lcom/flixster/android/activity/ImageViewHandler;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter "callback"

    .prologue
    .line 1211
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->detailBitmap:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_0

    .line 1212
    new-instance v0, Lcom/flixster/android/model/Image;

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getDetailPoster()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/Movie;->detailBitmap:Lcom/flixster/android/model/Image;

    .line 1214
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->detailBitmap:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Lcom/flixster/android/activity/ImageViewHandler;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getDetailPoster()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1141
    const-string v0, "detailed"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDvdReleaseDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->dvdReleaseDate:Ljava/util/Date;

    return-object v0
.end method

.method public getFeaturedId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 1172
    const-string v0, "featuredId"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getFriendRatedReviewsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .prologue
    .line 249
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mFriendRatedReviews:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFriendRatedReviewsListSize()I
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mFriendRatedReviews:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mFriendRatedReviews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getFriendStat(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 5
    .parameter "resources"

    .prologue
    const/4 v4, 0x1

    .line 1160
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 1161
    .local v1, sb:Ljava/lang/StringBuilder;
    const-string v2, "FRIENDS_RATED_COUNT"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .local v0, count:Ljava/lang/Integer;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_2

    .line 1162
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1163
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v4, :cond_1

    const v2, 0x7f0c0090

    :goto_0
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1168
    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_4

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_2
    return-object v2

    .line 1163
    :cond_1
    const v2, 0x7f0c008f

    goto :goto_0

    .line 1164
    :cond_2
    const-string v2, "FRIENDS_WTS_COUNT"

    invoke-virtual {p0, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_0

    .line 1165
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1166
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-le v2, v4, :cond_3

    const v2, 0x7f0c0094

    :goto_3
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_3
    const v2, 0x7f0c0093

    goto :goto_3

    .line 1168
    :cond_4
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public getFriendWantToSeeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .prologue
    .line 241
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mFriendWantToSeeReviews:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getFriendWantToSeeListSize()I
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mFriendWantToSeeReviews:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mFriendWantToSeeReviews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getGenres()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 1180
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->genres:[Ljava/lang/String;

    return-object v0
.end method

.method public getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;
    .locals 1
    .parameter "propkey"

    .prologue
    .line 183
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public getMpaa()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1063
    const-string v0, "mpaa"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMpaaRuntime()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1078
    const-string v0, "meta"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumCriticReviews()I
    .locals 1

    .prologue
    .line 1090
    iget v0, p0, Lnet/flixster/android/model/Movie;->criticsNumReviews:I

    return v0
.end method

.method public getNumUserRatings()I
    .locals 1

    .prologue
    .line 1111
    iget v0, p0, Lnet/flixster/android/model/Movie;->userNumRatings:I

    return v0
.end method

.method public getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 1196
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->profileBitmapNew:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_0

    .line 1197
    new-instance v0, Lcom/flixster/android/model/Image;

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getProfilePoster()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/Movie;->profileBitmapNew:Lcom/flixster/android/model/Image;

    .line 1199
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->profileBitmapNew:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getProfilePoster()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1137
    const-string v0, "profile"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "propkey"

    .prologue
    .line 170
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getReleaseYear()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1150
    const/4 v1, 0x0

    .line 1151
    .local v1, result:Ljava/lang/String;
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v0

    .line 1152
    .local v0, releaseDate:Ljava/util/Date;
    if-eqz v0, :cond_0

    .line 1153
    invoke-virtual {v0}, Ljava/util/Date;->getYear()I

    move-result v2

    add-int/lit16 v2, v2, 0x76c

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 1155
    :cond_0
    return-object v1
.end method

.method public getSoftThumbnail()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 1219
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getSynopsis()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1082
    const-string v0, "synopsis"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTheaterReleaseDate()Ljava/util/Date;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->theaterReleaseDate:Ljava/util/Date;

    return-object v0
.end method

.method public getThumbOrderStamp()J
    .locals 2

    .prologue
    .line 205
    iget-wide v0, p0, Lnet/flixster/android/model/Movie;->mThumbOrderStamp:J

    return-wide v0
.end method

.method public getThumbnailBackedProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 3
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 1185
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    iget-object v1, p0, Lnet/flixster/android/model/Movie;->profileBitmapNew:Lcom/flixster/android/model/Image;

    if-nez v1, :cond_0

    .line 1186
    new-instance v1, Lcom/flixster/android/model/Image;

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getProfilePoster()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lnet/flixster/android/model/Movie;->profileBitmapNew:Lcom/flixster/android/model/Image;

    .line 1188
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/model/Movie;->profileBitmapNew:Lcom/flixster/android/model/Image;

    invoke-virtual {v1, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1189
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 1190
    iget-object v1, p0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 1192
    .restart local v0       #bitmap:Landroid/graphics/Bitmap;
    :cond_1
    return-object v0
.end method

.method public getThumbnailPoster()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1129
    const-string v1, "thumbnail"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1130
    .local v0, url:Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "movie.none.mob.gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1131
    const/4 v0, 0x0

    .line 1133
    :cond_0
    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1043
    const-string v0, "title"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTomatometer()I
    .locals 2

    .prologue
    .line 1094
    const-string v1, "rottenTomatoes"

    invoke-virtual {p0, v1}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 1095
    .local v0, score:Ljava/lang/Integer;
    if-nez v0, :cond_0

    const/4 v1, -0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0
.end method

.method public getTomatometerIconId()I
    .locals 2

    .prologue
    .line 1099
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTomatometer()I

    move-result v0

    const/16 v1, 0x3c

    if-ge v0, v1, :cond_0

    const v0, 0x7f0200ed

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0200de

    goto :goto_0
.end method

.method public getTrailerHigh()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1047
    const-string v0, "high"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTrailerLow()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1055
    const-string v0, "low"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTrailerMed()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1051
    const-string v0, "med"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserReviewsList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->userReviewsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public hasTrailer()Z
    .locals 1

    .prologue
    .line 1059
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerHigh()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerMed()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTrailerLow()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hintTitle(Ljava/lang/String;)V
    .locals 2
    .parameter "title"

    .prologue
    .line 213
    if-eqz p1, :cond_0

    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v1, "title"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    const-string v0, "title"

    invoke-static {p1}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    :cond_0
    return-void
.end method

.method public isCertifiedFresh()Z
    .locals 1

    .prologue
    .line 1107
    iget-boolean v0, p0, Lnet/flixster/android/model/Movie;->isCertifiedFresh:Z

    return v0
.end method

.method public isDetailsApiParsed()Z
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lnet/flixster/android/model/Movie;->isMovieDetailsApiParsed:Z

    return v0
.end method

.method public isFeatured()Z
    .locals 1

    .prologue
    .line 1176
    const-string v0, "featuredId"

    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFresh()Z
    .locals 2

    .prologue
    .line 1103
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTomatometer()I

    move-result v0

    const/16 v1, 0x3c

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReleased()Z
    .locals 2

    .prologue
    .line 1145
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lnet/flixster/android/FlixsterApplication;->sToday:Ljava/util/Date;

    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getTheaterReleaseDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Date;->after(Ljava/util/Date;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSpilled()Z
    .locals 2

    .prologue
    .line 1125
    invoke-virtual {p0}, Lnet/flixster/android/model/Movie;->getAudienceScore()I

    move-result v0

    const/16 v1, 0x3c

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public merge(Lnet/flixster/android/model/Movie;)V
    .locals 6
    .parameter "mergeMovie"

    .prologue
    const/4 v1, 0x0

    .line 265
    if-ne p0, p1, :cond_1

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    sget-object v3, Lnet/flixster/android/model/Movie;->FULL_PROPERTY_ARRAY:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-lt v2, v4, :cond_2

    .line 275
    sget-object v2, Lnet/flixster/android/model/Movie;->FULL_PROPERTY_INT_ARRAY:[Ljava/lang/String;

    array-length v3, v2

    :goto_2
    if-lt v1, v3, :cond_4

    .line 282
    iget-object v1, p0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p1, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 283
    new-instance v2, Ljava/lang/ref/SoftReference;

    iget-object v1, p1, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v1}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-direct {v2, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lnet/flixster/android/model/Movie;->thumbnailSoftBitmap:Ljava/lang/ref/SoftReference;

    goto :goto_0

    .line 268
    :cond_2
    aget-object v0, v3, v2

    .line 269
    .local v0, prop:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 270
    invoke-virtual {p1, v0}, Lnet/flixster/android/model/Movie;->checkProperty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 271
    invoke-virtual {p1, v0}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v0, v5}, Lnet/flixster/android/model/Movie;->setProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 275
    .end local v0           #prop:Ljava/lang/String;
    :cond_4
    aget-object v0, v2, v1

    .line 276
    .restart local v0       #prop:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 277
    invoke-virtual {p1, v0}, Lnet/flixster/android/model/Movie;->checkIntProperty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 278
    invoke-virtual {p1, v0}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, v0, v4}, Lnet/flixster/android/model/Movie;->setIntProperty(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 275
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public netflixParseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;
    .locals 14
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 603
    const-string v11, "id"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 604
    .local v7, tempId:Ljava/lang/Long;
    if-eqz v7, :cond_0

    .line 605
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    iput-wide v11, p0, Lnet/flixster/android/model/Movie;->id:J

    .line 608
    :cond_0
    sget-object v11, Lnet/flixster/android/model/Movie;->NETFLIX_BASIC_PROPERTY_ARRAY:[Ljava/lang/String;

    invoke-direct {p0, p1, v11}, Lnet/flixster/android/model/Movie;->arrayParseJSONObject(Lorg/json/JSONObject;[Ljava/lang/String;)V

    .line 610
    const-string v11, "playing"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 611
    const-string v11, "playing"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v11

    iput-boolean v11, p0, Lnet/flixster/android/model/Movie;->isMIT:Z

    .line 614
    :cond_1
    const-string v11, "poster"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v11

    invoke-direct {p0, v11}, Lnet/flixster/android/model/Movie;->parsePosterUrls(Lorg/json/JSONObject;)V

    .line 617
    iget-object v11, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v12, "MOVIE_ACTORS"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 618
    const-string v11, "actors"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 619
    const-string v11, "actors"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 620
    .local v0, actorList:Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v11

    const/4 v12, 0x2

    invoke-static {v11, v12}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 621
    .local v5, length:I
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 623
    .local v1, actorsShort:Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .local v4, i:I
    :goto_0
    if-lt v4, v5, :cond_5

    .line 633
    iget-object v11, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v12, "MOVIE_ACTORS_SHORT"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 639
    .end local v0           #actorList:Lorg/json/JSONArray;
    .end local v1           #actorsShort:Ljava/lang/StringBuilder;
    .end local v4           #i:I
    .end local v5           #length:I
    :cond_2
    invoke-direct {p0, p1}, Lnet/flixster/android/model/Movie;->netflixParseReviews(Lorg/json/JSONObject;)V

    .line 643
    const-string v11, "urls"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 645
    :try_start_0
    const-string v11, "urls"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v9

    .line 646
    .local v9, urlArray:Lorg/json/JSONArray;
    invoke-virtual {v9}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    .line 649
    .restart local v5       #length:I
    const/4 v4, 0x0

    .restart local v4       #i:I
    :goto_1
    if-lt v4, v5, :cond_7

    .line 661
    .end local v4           #i:I
    .end local v5           #length:I
    .end local v9           #urlArray:Lorg/json/JSONArray;
    :cond_3
    :goto_2
    iget-object v11, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v12, "boxOffice"

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 662
    const-string v11, "boxOffice"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 663
    const-string v11, "boxOffice"

    invoke-virtual {p1, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 664
    .local v3, grossString:Ljava/lang/String;
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v11

    if-lez v11, :cond_4

    .line 666
    iget-object v11, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    const-string v12, "boxOffice"

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 670
    .end local v3           #grossString:Ljava/lang/String;
    :cond_4
    return-object p0

    .line 624
    .restart local v0       #actorList:Lorg/json/JSONArray;
    .restart local v1       #actorsShort:Ljava/lang/StringBuilder;
    .restart local v4       #i:I
    .restart local v5       #length:I
    :cond_5
    invoke-virtual {v0, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    .line 625
    .local v6, tempActorObj:Lorg/json/JSONObject;
    if-eqz v4, :cond_6

    .line 627
    const-string v11, ", "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 629
    :cond_6
    const-string v11, "name"

    invoke-virtual {v6, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 623
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 650
    .end local v0           #actorList:Lorg/json/JSONArray;
    .end local v1           #actorsShort:Ljava/lang/StringBuilder;
    .end local v6           #tempActorObj:Lorg/json/JSONObject;
    .restart local v9       #urlArray:Lorg/json/JSONArray;
    :cond_7
    :try_start_1
    invoke-virtual {v9, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v10

    .line 651
    .local v10, urlObj:Lorg/json/JSONObject;
    const-string v11, "type"

    invoke-virtual {v10, v11}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 652
    .local v8, type:Ljava/lang/String;
    const-string v11, "netflix"

    invoke-virtual {v8, v11}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 653
    iget-object v11, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    const-string v12, "netflix"

    const-string v13, "url"

    invoke-virtual {v10, v13}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 649
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 656
    .end local v4           #i:I
    .end local v5           #length:I
    .end local v8           #type:Ljava/lang/String;
    .end local v9           #urlArray:Lorg/json/JSONArray;
    .end local v10           #urlObj:Lorg/json/JSONObject;
    :catch_0
    move-exception v2

    .line 657
    .local v2, e:Lorg/json/JSONException;
    const-string v11, "FlxMain"

    const-string v12, "flixsterUrl JSON parse"

    invoke-static {v11, v12, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;
    .locals 41
    .parameter "jsonMovie"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 347
    const-string v38, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v38

    invoke-static/range {v38 .. v39}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v34

    .line 348
    .local v34, tempId:Ljava/lang/Long;
    if-eqz v34, :cond_0

    .line 349
    invoke-virtual/range {v34 .. v34}, Ljava/lang/Long;->longValue()J

    move-result-wide v38

    move-wide/from16 v0, v38

    move-object/from16 v2, p0

    iput-wide v0, v2, Lnet/flixster/android/model/Movie;->id:J

    .line 352
    :cond_0
    sget-object v38, Lnet/flixster/android/model/Movie;->BASIC_PROPERTY_ARRAY:[Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v38

    invoke-direct {v0, v1, v2}, Lnet/flixster/android/model/Movie;->arrayParseJSONObject(Lorg/json/JSONObject;[Ljava/lang/String;)V

    .line 354
    const-string v38, "playing"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_1

    .line 355
    const-string v38, "playing"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v38

    move/from16 v0, v38

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lnet/flixster/android/model/Movie;->isMIT:Z

    .line 358
    :cond_1
    invoke-direct/range {p0 .. p1}, Lnet/flixster/android/model/Movie;->parseFeatureId(Lorg/json/JSONObject;)V

    .line 359
    const-string v38, "poster"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v38

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    invoke-direct {v0, v1}, Lnet/flixster/android/model/Movie;->parsePosterUrls(Lorg/json/JSONObject;)V

    .line 361
    invoke-direct/range {p0 .. p1}, Lnet/flixster/android/model/Movie;->parseTrailer(Lorg/json/JSONObject;)V

    .line 362
    invoke-direct/range {p0 .. p1}, Lnet/flixster/android/model/Movie;->parseDirectors(Lorg/json/JSONObject;)V

    .line 365
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "MOVIE_ACTORS"

    invoke-virtual/range {v38 .. v39}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v38

    if-nez v38, :cond_2

    .line 366
    const-string v38, "actors"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_2

    .line 367
    const-string v38, "actors"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 368
    .local v6, actorList:Lorg/json/JSONArray;
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v29

    .line 371
    .local v29, length:I
    const-string v9, ""

    .line 372
    .local v9, actorsShort:Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 374
    .local v5, actorBuilder:Ljava/lang/StringBuilder;
    const/4 v15, 0x0

    .local v15, i:I
    :goto_0
    move/from16 v0, v29

    if-lt v15, v0, :cond_10

    .line 386
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 388
    .local v8, actors:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "MOVIE_ACTORS"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "MOVIE_ACTORS_SHORT"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 393
    .end local v5           #actorBuilder:Ljava/lang/StringBuilder;
    .end local v6           #actorList:Lorg/json/JSONArray;
    .end local v8           #actors:Ljava/lang/String;
    .end local v9           #actorsShort:Ljava/lang/String;
    .end local v15           #i:I
    .end local v29           #length:I
    :cond_2
    invoke-direct/range {p0 .. p1}, Lnet/flixster/android/model/Movie;->parseTheaterReleaseDateString(Lorg/json/JSONObject;)V

    .line 394
    invoke-direct/range {p0 .. p1}, Lnet/flixster/android/model/Movie;->parseDvdReleaseDateString(Lorg/json/JSONObject;)V

    .line 395
    invoke-direct/range {p0 .. p1}, Lnet/flixster/android/model/Movie;->parseReviews(Lorg/json/JSONObject;)V

    .line 396
    invoke-direct/range {p0 .. p1}, Lnet/flixster/android/model/Movie;->parseFriends(Lorg/json/JSONObject;)V

    .line 399
    const-string v38, "urls"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_3

    .line 401
    :try_start_0
    const-string v38, "urls"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v36

    .line 402
    .local v36, urlArray:Lorg/json/JSONArray;
    invoke-virtual/range {v36 .. v36}, Lorg/json/JSONArray;->length()I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v29

    .line 405
    .restart local v29       #length:I
    const/4 v15, 0x0

    .restart local v15       #i:I
    :goto_1
    move/from16 v0, v29

    if-lt v15, v0, :cond_13

    .line 432
    .end local v15           #i:I
    .end local v29           #length:I
    .end local v36           #urlArray:Lorg/json/JSONArray;
    :cond_3
    :goto_2
    const-string v38, "photoCount"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_4

    .line 433
    const-string v38, "photoCount"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v38

    move/from16 v0, v38

    move-object/from16 v1, p0

    iput v0, v1, Lnet/flixster/android/model/Movie;->photoCount:I

    .line 436
    :cond_4
    const-string v38, "photos"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_5

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    if-nez v38, :cond_18

    .line 438
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v38

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    .line 445
    :goto_3
    :try_start_1
    const-string v38, "photos"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v28

    .line 446
    .local v28, jsonPhotos:Lorg/json/JSONArray;
    const/4 v15, 0x0

    .restart local v15       #i:I
    :goto_4
    invoke-virtual/range {v28 .. v28}, Lorg/json/JSONArray;->length()I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v38

    move/from16 v0, v38

    if-lt v15, v0, :cond_19

    .line 457
    .end local v15           #i:I
    .end local v28           #jsonPhotos:Lorg/json/JSONArray;
    :cond_5
    :goto_5
    const-string v38, "directors"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_9

    .line 458
    const-string v38, "directors"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v26

    .line 459
    .local v26, jsonDirectors:Lorg/json/JSONArray;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    if-nez v38, :cond_6

    .line 460
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v38

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    .line 462
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    if-eqz v38, :cond_7

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->directorsHash:Ljava/util/HashMap;

    move-object/from16 v38, v0

    if-nez v38, :cond_8

    .line 463
    :cond_7
    new-instance v38, Ljava/util/HashMap;

    invoke-direct/range {v38 .. v38}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v38

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->directorsHash:Ljava/util/HashMap;

    .line 468
    :cond_8
    const/4 v15, 0x0

    .restart local v15       #i:I
    :goto_6
    :try_start_2
    invoke-virtual/range {v26 .. v26}, Lorg/json/JSONArray;->length()I
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v38

    move/from16 v0, v38

    if-lt v15, v0, :cond_1a

    .line 498
    .end local v15           #i:I
    .end local v26           #jsonDirectors:Lorg/json/JSONArray;
    :cond_9
    :goto_7
    const-string v38, "actors"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_d

    .line 499
    const-string v38, "actors"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v22

    .line 500
    .local v22, jsonActors:Lorg/json/JSONArray;
    invoke-virtual/range {v22 .. v22}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 501
    .local v3, aLength:I
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    if-nez v38, :cond_a

    .line 502
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v38

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    .line 504
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    if-eqz v38, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->actorsHash:Ljava/util/HashMap;

    move-object/from16 v38, v0

    if-nez v38, :cond_c

    .line 505
    :cond_b
    new-instance v38, Ljava/util/HashMap;

    invoke-direct/range {v38 .. v38}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v38

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->actorsHash:Ljava/util/HashMap;

    .line 513
    :cond_c
    const/4 v15, 0x0

    .restart local v15       #i:I
    :goto_8
    if-lt v15, v3, :cond_20

    .line 565
    .end local v3           #aLength:I
    .end local v15           #i:I
    .end local v22           #jsonActors:Lorg/json/JSONArray;
    :cond_d
    :goto_9
    const-string v38, "characters"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_e

    .line 567
    const-string v38, "characters"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v24

    .line 569
    .local v24, jsonCharacters:Lorg/json/JSONArray;
    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONArray;->length()I

    move-result v29

    .line 570
    .restart local v29       #length:I
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 571
    .local v7, actorPageCharBuilder:Ljava/lang/StringBuilder;
    const/4 v15, 0x0

    .restart local v15       #i:I
    :goto_a
    move/from16 v0, v29

    if-lt v15, v0, :cond_2a

    .line 580
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Movie;->mActorPageChars:Ljava/lang/String;

    .line 583
    .end local v7           #actorPageCharBuilder:Ljava/lang/StringBuilder;
    .end local v15           #i:I
    .end local v24           #jsonCharacters:Lorg/json/JSONArray;
    .end local v29           #length:I
    :cond_e
    invoke-direct/range {p0 .. p1}, Lnet/flixster/android/model/Movie;->parseGenre(Lorg/json/JSONObject;)V

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "boxOffice"

    invoke-virtual/range {v38 .. v39}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v38

    if-nez v38, :cond_f

    .line 586
    const-string v38, "boxOffice"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_f

    .line 587
    const-string v38, "boxOffice"

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 588
    .local v14, grossString:Ljava/lang/String;
    if-eqz v14, :cond_f

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v38

    if-lez v38, :cond_f

    .line 590
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "boxOffice"

    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v40

    invoke-static/range {v40 .. v40}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 595
    .end local v14           #grossString:Ljava/lang/String;
    :cond_f
    return-object p0

    .line 375
    .restart local v5       #actorBuilder:Ljava/lang/StringBuilder;
    .restart local v6       #actorList:Lorg/json/JSONArray;
    .restart local v9       #actorsShort:Ljava/lang/String;
    .restart local v15       #i:I
    .restart local v29       #length:I
    :cond_10
    invoke-virtual {v6, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v33

    .line 376
    .local v33, tempActorObj:Lorg/json/JSONObject;
    const-string v38, "name"

    move-object/from16 v0, v33

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    const/16 v39, 0x20

    const/16 v40, 0xa0

    invoke-virtual/range {v38 .. v40}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v32

    .line 377
    .local v32, tempActor:Ljava/lang/String;
    move-object/from16 v0, v32

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 379
    const/16 v38, 0x2

    move/from16 v0, v38

    if-ge v15, v0, :cond_11

    .line 380
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 382
    :cond_11
    add-int/lit8 v38, v15, 0x1

    move/from16 v0, v38

    move/from16 v1, v29

    if-ge v0, v1, :cond_12

    .line 383
    const-string v38, ", "

    move-object/from16 v0, v38

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 374
    :cond_12
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    .line 406
    .end local v5           #actorBuilder:Ljava/lang/StringBuilder;
    .end local v6           #actorList:Lorg/json/JSONArray;
    .end local v9           #actorsShort:Ljava/lang/String;
    .end local v32           #tempActor:Ljava/lang/String;
    .end local v33           #tempActorObj:Lorg/json/JSONObject;
    .restart local v36       #urlArray:Lorg/json/JSONArray;
    :cond_13
    :try_start_3
    move-object/from16 v0, v36

    invoke-virtual {v0, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v37

    .line 407
    .local v37, urlObj:Lorg/json/JSONObject;
    const-string v38, "type"

    invoke-virtual/range {v37 .. v38}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 409
    .local v35, type:Ljava/lang/String;
    const-string v38, "flixster"

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v38

    if-eqz v38, :cond_15

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "flixster"

    invoke-virtual/range {v38 .. v39}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v38

    if-nez v38, :cond_14

    .line 411
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "flixster"

    const-string v40, "url"

    move-object/from16 v0, v37

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 405
    :cond_14
    :goto_b
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_1

    .line 413
    :cond_15
    const-string v38, "imdb"

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v38

    if-eqz v38, :cond_16

    .line 414
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "imdb"

    invoke-virtual/range {v38 .. v39}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v38

    if-nez v38, :cond_14

    .line 415
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "imdb"

    const-string v40, "url"

    move-object/from16 v0, v37

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_b

    .line 427
    .end local v15           #i:I
    .end local v29           #length:I
    .end local v35           #type:Ljava/lang/String;
    .end local v36           #urlArray:Lorg/json/JSONArray;
    .end local v37           #urlObj:Lorg/json/JSONObject;
    :catch_0
    move-exception v13

    .line 428
    .local v13, e:Lorg/json/JSONException;
    const-string v38, "FlxMain"

    const-string v39, "flixsterUrl JSON parse"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-static {v0, v1, v13}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 417
    .end local v13           #e:Lorg/json/JSONException;
    .restart local v15       #i:I
    .restart local v29       #length:I
    .restart local v35       #type:Ljava/lang/String;
    .restart local v36       #urlArray:Lorg/json/JSONArray;
    .restart local v37       #urlObj:Lorg/json/JSONObject;
    :cond_16
    :try_start_4
    const-string v38, "rottentomatoes"

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v38

    if-eqz v38, :cond_17

    .line 418
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "rottentomatoes"

    invoke-virtual/range {v38 .. v39}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v38

    if-nez v38, :cond_14

    .line 419
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "rottentomatoes"

    const-string v40, "url"

    move-object/from16 v0, v37

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_b

    .line 421
    :cond_17
    const-string v38, "netflix"

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v38

    if-eqz v38, :cond_14

    .line 422
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "netflix"

    invoke-virtual/range {v38 .. v39}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v38

    if-nez v38, :cond_14

    .line 423
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v38, v0

    const-string v39, "netflix"

    const-string v40, "url"

    move-object/from16 v0, v37

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-static/range {v40 .. v40}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_b

    .line 440
    .end local v15           #i:I
    .end local v29           #length:I
    .end local v35           #type:Ljava/lang/String;
    .end local v36           #urlArray:Lorg/json/JSONArray;
    .end local v37           #urlObj:Lorg/json/JSONObject;
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_3

    .line 447
    .restart local v15       #i:I
    .restart local v28       #jsonPhotos:Lorg/json/JSONArray;
    :cond_19
    :try_start_5
    move-object/from16 v0, v28

    invoke-virtual {v0, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v27

    .line 448
    .local v27, jsonPhoto:Lorg/json/JSONObject;
    new-instance v30, Lnet/flixster/android/model/Photo;

    invoke-direct/range {v30 .. v30}, Lnet/flixster/android/model/Photo;-><init>()V

    .line 449
    .local v30, photo:Lnet/flixster/android/model/Photo;
    const-string v38, "url"

    move-object/from16 v0, v27

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v38 .. v38}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    move-object/from16 v1, v30

    iput-object v0, v1, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    .line 450
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mPhotos:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    .line 446
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_4

    .line 452
    .end local v15           #i:I
    .end local v27           #jsonPhoto:Lorg/json/JSONObject;
    .end local v28           #jsonPhotos:Lorg/json/JSONArray;
    .end local v30           #photo:Lnet/flixster/android/model/Photo;
    :catch_1
    move-exception v13

    .line 453
    .restart local v13       #e:Lorg/json/JSONException;
    const-string v38, "FlxMain"

    const-string v39, "problem parsing movie photos"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-static {v0, v1, v13}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_5

    .line 469
    .end local v13           #e:Lorg/json/JSONException;
    .restart local v15       #i:I
    .restart local v26       #jsonDirectors:Lorg/json/JSONArray;
    :cond_1a
    :try_start_6
    move-object/from16 v0, v26

    invoke-virtual {v0, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v25

    .line 470
    .local v25, jsonDirector:Lorg/json/JSONObject;
    const-string v38, "id"

    move-object/from16 v0, v25

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 472
    .local v16, id:J
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->directorsHash:Ljava/util/HashMap;

    move-object/from16 v38, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lnet/flixster/android/model/Actor;

    .line 473
    .local v12, director:Lnet/flixster/android/model/Actor;
    if-nez v12, :cond_1f

    const/16 v19, 0x1

    .line 474
    .local v19, isNewDirector:Z
    :goto_c
    if-eqz v19, :cond_1b

    .line 475
    new-instance v12, Lnet/flixster/android/model/Actor;

    .end local v12           #director:Lnet/flixster/android/model/Actor;
    invoke-direct {v12}, Lnet/flixster/android/model/Actor;-><init>()V

    .line 477
    .restart local v12       #director:Lnet/flixster/android/model/Actor;
    :cond_1b
    move-wide/from16 v0, v16

    iput-wide v0, v12, Lnet/flixster/android/model/Actor;->id:J

    .line 478
    const-string v38, "name"

    move-object/from16 v0, v25

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v38 .. v38}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    iput-object v0, v12, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    .line 479
    const-string v38, "photo"

    move-object/from16 v0, v25

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_1d

    .line 480
    const-string v38, "photo"

    move-object/from16 v0, v25

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v31

    .line 481
    .local v31, photoObject:Lorg/json/JSONObject;
    const-string v38, "thumbnail"

    move-object/from16 v0, v31

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_1c

    .line 482
    const-string v38, "thumbnail"

    move-object/from16 v0, v31

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v38 .. v38}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    iput-object v0, v12, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    .line 484
    :cond_1c
    const-string v38, "lthumbnail"

    move-object/from16 v0, v31

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_1d

    .line 485
    const-string v38, "lthumbnail"

    move-object/from16 v0, v31

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v38 .. v38}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    iput-object v0, v12, Lnet/flixster/android/model/Actor;->largeUrl:Ljava/lang/String;

    .line 488
    .end local v31           #photoObject:Lorg/json/JSONObject;
    :cond_1d
    if-eqz v19, :cond_1e

    .line 489
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mDirectors:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 490
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->directorsHash:Ljava/util/HashMap;

    move-object/from16 v38, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Lorg/json/JSONException; {:try_start_6 .. :try_end_6} :catch_2

    .line 468
    :cond_1e
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_6

    .line 473
    .end local v19           #isNewDirector:Z
    :cond_1f
    const/16 v19, 0x0

    goto/16 :goto_c

    .line 493
    .end local v12           #director:Lnet/flixster/android/model/Actor;
    .end local v16           #id:J
    .end local v25           #jsonDirector:Lorg/json/JSONObject;
    :catch_2
    move-exception v13

    .line 494
    .restart local v13       #e:Lorg/json/JSONException;
    const-string v38, "FlxMain"

    const-string v39, "problem parsing movie directors"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-static {v0, v1, v13}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 514
    .end local v13           #e:Lorg/json/JSONException;
    .end local v26           #jsonDirectors:Lorg/json/JSONArray;
    .restart local v3       #aLength:I
    .restart local v22       #jsonActors:Lorg/json/JSONArray;
    :cond_20
    :try_start_7
    move-object/from16 v0, v22

    invoke-virtual {v0, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v21

    .line 515
    .local v21, jsonActor:Lorg/json/JSONObject;
    const-string v38, "id"

    move-object/from16 v0, v21

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v16

    .line 517
    .restart local v16       #id:J
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->actorsHash:Ljava/util/HashMap;

    move-object/from16 v38, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    invoke-virtual/range {v38 .. v39}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lnet/flixster/android/model/Actor;

    .line 518
    .local v4, actor:Lnet/flixster/android/model/Actor;
    if-nez v4, :cond_26

    const/16 v18, 0x1

    .line 519
    .local v18, isNewActor:Z
    :goto_d
    if-eqz v18, :cond_21

    .line 520
    new-instance v4, Lnet/flixster/android/model/Actor;

    .end local v4           #actor:Lnet/flixster/android/model/Actor;
    invoke-direct {v4}, Lnet/flixster/android/model/Actor;-><init>()V

    .line 522
    .restart local v4       #actor:Lnet/flixster/android/model/Actor;
    :cond_21
    move-wide/from16 v0, v16

    iput-wide v0, v4, Lnet/flixster/android/model/Actor;->id:J

    .line 523
    const-string v38, "name"

    move-object/from16 v0, v21

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v38 .. v38}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    iput-object v0, v4, Lnet/flixster/android/model/Actor;->name:Ljava/lang/String;

    .line 525
    const-string v38, "characters"

    move-object/from16 v0, v21

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_22

    .line 526
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 527
    .local v11, charList:Ljava/lang/StringBuilder;
    const-string v38, "characters"

    move-object/from16 v0, v21

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v24

    .line 528
    .restart local v24       #jsonCharacters:Lorg/json/JSONArray;
    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONArray;->length()I

    move-result v38

    if-lez v38, :cond_22

    .line 529
    invoke-virtual/range {v24 .. v24}, Lorg/json/JSONArray;->length()I

    move-result v10

    .line 530
    .local v10, cLength:I
    const/16 v20, 0x0

    .local v20, j:I
    :goto_e
    move/from16 v0, v20

    if-lt v0, v10, :cond_27

    .line 541
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    iput-object v0, v4, Lnet/flixster/android/model/Actor;->chars:Ljava/lang/String;

    .line 545
    .end local v10           #cLength:I
    .end local v11           #charList:Ljava/lang/StringBuilder;
    .end local v20           #j:I
    .end local v24           #jsonCharacters:Lorg/json/JSONArray;
    :cond_22
    const-string v38, "photo"

    move-object/from16 v0, v21

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_24

    .line 546
    const-string v38, "photo"

    move-object/from16 v0, v21

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v31

    .line 547
    .restart local v31       #photoObject:Lorg/json/JSONObject;
    const-string v38, "thumbnail"

    move-object/from16 v0, v31

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_23

    .line 548
    const-string v38, "thumbnail"

    move-object/from16 v0, v31

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v38 .. v38}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    iput-object v0, v4, Lnet/flixster/android/model/Actor;->thumbnailUrl:Ljava/lang/String;

    .line 550
    :cond_23
    const-string v38, "lthumbnail"

    move-object/from16 v0, v31

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_24

    .line 551
    const-string v38, "lthumbnail"

    move-object/from16 v0, v31

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    invoke-static/range {v38 .. v38}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v38

    iput-object v0, v4, Lnet/flixster/android/model/Actor;->largeUrl:Ljava/lang/String;

    .line 554
    .end local v31           #photoObject:Lorg/json/JSONObject;
    :cond_24
    if-eqz v18, :cond_25

    .line 555
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->mActors:Ljava/util/ArrayList;

    move-object/from16 v38, v0

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 556
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Movie;->actorsHash:Ljava/util/HashMap;

    move-object/from16 v38, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v39

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-virtual {v0, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 513
    :cond_25
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_8

    .line 518
    .end local v18           #isNewActor:Z
    :cond_26
    const/16 v18, 0x0

    goto/16 :goto_d

    .line 531
    .restart local v10       #cLength:I
    .restart local v11       #charList:Ljava/lang/StringBuilder;
    .restart local v18       #isNewActor:Z
    .restart local v20       #j:I
    .restart local v24       #jsonCharacters:Lorg/json/JSONArray;
    :cond_27
    move-object/from16 v0, v24

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v23

    .line 532
    .local v23, jsonCharObj:Lorg/json/JSONObject;
    const-string v38, "character"

    move-object/from16 v0, v23

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_29

    .line 533
    if-eqz v20, :cond_28

    .line 534
    const-string v38, ", "

    move-object/from16 v0, v38

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 536
    :cond_28
    const-string v38, "character"

    move-object/from16 v0, v23

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_3

    .line 530
    :cond_29
    add-int/lit8 v20, v20, 0x1

    goto/16 :goto_e

    .line 559
    .end local v4           #actor:Lnet/flixster/android/model/Actor;
    .end local v10           #cLength:I
    .end local v11           #charList:Ljava/lang/StringBuilder;
    .end local v16           #id:J
    .end local v18           #isNewActor:Z
    .end local v20           #j:I
    .end local v21           #jsonActor:Lorg/json/JSONObject;
    .end local v23           #jsonCharObj:Lorg/json/JSONObject;
    .end local v24           #jsonCharacters:Lorg/json/JSONArray;
    :catch_3
    move-exception v13

    .line 560
    .restart local v13       #e:Lorg/json/JSONException;
    const-string v38, "FlxMain"

    const-string v39, "problem parsing movie actors"

    move-object/from16 v0, v38

    move-object/from16 v1, v39

    invoke-static {v0, v1, v13}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_9

    .line 572
    .end local v3           #aLength:I
    .end local v13           #e:Lorg/json/JSONException;
    .end local v22           #jsonActors:Lorg/json/JSONArray;
    .restart local v7       #actorPageCharBuilder:Ljava/lang/StringBuilder;
    .restart local v24       #jsonCharacters:Lorg/json/JSONArray;
    .restart local v29       #length:I
    :cond_2a
    move-object/from16 v0, v24

    invoke-virtual {v0, v15}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v23

    .line 573
    .restart local v23       #jsonCharObj:Lorg/json/JSONObject;
    const-string v38, "character"

    move-object/from16 v0, v23

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v38

    if-eqz v38, :cond_2c

    .line 574
    if-eqz v15, :cond_2b

    .line 575
    const-string v38, ", "

    move-object/from16 v0, v38

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 577
    :cond_2b
    const-string v38, "character"

    move-object/from16 v0, v23

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 571
    :cond_2c
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_a
.end method

.method public setDetailsApiParsed()V
    .locals 1

    .prologue
    .line 261
    const/4 v0, 0x1

    iput-boolean v0, p0, Lnet/flixster/android/model/Movie;->isMovieDetailsApiParsed:Z

    .line 262
    return-void
.end method

.method public setIntProperty(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 1
    .parameter "propkey"
    .parameter "value"

    .prologue
    .line 187
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mIntPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "propkey"
    .parameter "value"

    .prologue
    .line 174
    iget-object v0, p0, Lnet/flixster/android/model/Movie;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    return-void
.end method

.method public setThumbOrderStamp(J)V
    .locals 0
    .parameter "stamp"

    .prologue
    .line 209
    iput-wide p1, p0, Lnet/flixster/android/model/Movie;->mThumbOrderStamp:J

    .line 210
    return-void
.end method
