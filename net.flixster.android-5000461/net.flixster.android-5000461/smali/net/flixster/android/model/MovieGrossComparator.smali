.class public Lnet/flixster/android/model/MovieGrossComparator;
.super Ljava/lang/Object;
.source "MovieGrossComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lnet/flixster/android/model/Movie;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lnet/flixster/android/model/Movie;

    check-cast p2, Lnet/flixster/android/model/Movie;

    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/MovieGrossComparator;->compare(Lnet/flixster/android/model/Movie;Lnet/flixster/android/model/Movie;)I

    move-result v0

    return v0
.end method

.method public compare(Lnet/flixster/android/model/Movie;Lnet/flixster/android/model/Movie;)I
    .locals 4
    .parameter "movie1"
    .parameter "movie2"

    .prologue
    const/4 v3, 0x0

    .line 5
    const-string v2, "boxOffice"

    invoke-virtual {p1, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 6
    .local v0, iM1:Ljava/lang/Integer;
    const-string v2, "boxOffice"

    invoke-virtual {p2, v2}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 7
    .local v1, iM2:Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 10
    :cond_0
    if-nez v1, :cond_1

    .line 11
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 14
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int/2addr v2, v3

    return v2
.end method
