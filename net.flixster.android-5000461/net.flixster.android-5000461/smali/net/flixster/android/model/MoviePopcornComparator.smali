.class public Lnet/flixster/android/model/MoviePopcornComparator;
.super Ljava/lang/Object;
.source "MoviePopcornComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lnet/flixster/android/model/Movie;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lnet/flixster/android/model/Movie;

    check-cast p2, Lnet/flixster/android/model/Movie;

    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/MoviePopcornComparator;->compare(Lnet/flixster/android/model/Movie;Lnet/flixster/android/model/Movie;)I

    move-result v0

    return v0
.end method

.method public compare(Lnet/flixster/android/model/Movie;Lnet/flixster/android/model/Movie;)I
    .locals 5
    .parameter "movie1"
    .parameter "movie2"

    .prologue
    const/4 v2, 0x0

    .line 5
    const-string v3, "popcornScore"

    invoke-virtual {p1, v3}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    .line 6
    .local v0, iM1:Ljava/lang/Integer;
    const-string v3, "popcornScore"

    invoke-virtual {p2, v3}, Lnet/flixster/android/model/Movie;->getIntProperty(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    .line 7
    .local v1, iM2:Ljava/lang/Integer;
    if-nez v0, :cond_0

    .line 8
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 10
    :cond_0
    if-nez v1, :cond_1

    .line 11
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 14
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v3, v4, :cond_3

    .line 15
    const/4 v2, -0x1

    .line 20
    :cond_2
    :goto_0
    return v2

    .line 17
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 18
    const/4 v2, 0x1

    goto :goto_0
.end method
