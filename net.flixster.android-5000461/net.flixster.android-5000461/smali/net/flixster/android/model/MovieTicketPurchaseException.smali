.class public Lnet/flixster/android/model/MovieTicketPurchaseException;
.super Lnet/flixster/android/model/MovieTicketException;
.source "MovieTicketPurchaseException.java"


# static fields
.field private static final serialVersionUID:J = -0x489a9d0b06bb4f9fL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lnet/flixster/android/model/MovieTicketException;-><init>()V

    .line 23
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "reason"

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lnet/flixster/android/model/MovieTicketException;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method
