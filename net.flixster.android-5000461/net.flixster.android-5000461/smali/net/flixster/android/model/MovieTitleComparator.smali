.class public Lnet/flixster/android/model/MovieTitleComparator;
.super Ljava/lang/Object;
.source "MovieTitleComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lnet/flixster/android/model/Movie;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lnet/flixster/android/model/Movie;

    check-cast p2, Lnet/flixster/android/model/Movie;

    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/MovieTitleComparator;->compare(Lnet/flixster/android/model/Movie;Lnet/flixster/android/model/Movie;)I

    move-result v0

    return v0
.end method

.method public compare(Lnet/flixster/android/model/Movie;Lnet/flixster/android/model/Movie;)I
    .locals 3
    .parameter "movie1"
    .parameter "movie2"

    .prologue
    .line 5
    const-string v2, "title"

    invoke-virtual {p1, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 6
    .local v0, title1:Ljava/lang/String;
    const-string v2, "title"

    invoke-virtual {p2, v2}, Lnet/flixster/android/model/Movie;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 7
    .local v1, title2:Ljava/lang/String;
    if-nez v0, :cond_0

    .line 8
    const-string v0, ""

    .line 10
    :cond_0
    if-nez v1, :cond_1

    .line 11
    const-string v1, ""

    .line 13
    :cond_1
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    return v2
.end method
