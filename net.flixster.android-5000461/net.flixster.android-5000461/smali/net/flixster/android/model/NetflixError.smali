.class public Lnet/flixster/android/model/NetflixError;
.super Ljava/lang/Object;
.source "NetflixError.java"


# instance fields
.field public mStatusCode:I

.field public mSubCode:I

.field public message:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    iput v0, p0, Lnet/flixster/android/model/NetflixError;->mStatusCode:I

    .line 8
    iput v0, p0, Lnet/flixster/android/model/NetflixError;->mSubCode:I

    .line 6
    return-void
.end method

.method public static parseFromJSONString(Ljava/lang/String;)Lnet/flixster/android/model/NetflixError;
    .locals 4
    .parameter "jsonString"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 12
    new-instance v1, Lnet/flixster/android/model/NetflixError;

    invoke-direct {v1}, Lnet/flixster/android/model/NetflixError;-><init>()V

    .line 14
    .local v1, netflixError:Lnet/flixster/android/model/NetflixError;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 16
    .local v0, jsonNetflixError:Lorg/json/JSONObject;
    const-string v3, "status"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 17
    const-string v3, "status"

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 18
    .local v2, statusJSON:Lorg/json/JSONObject;
    const-string v3, "sub_code"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 19
    const-string v3, "sub_code"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v1, Lnet/flixster/android/model/NetflixError;->mSubCode:I

    .line 21
    :cond_0
    const-string v3, "status_code"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 22
    const-string v3, "status_code"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    iput v3, v1, Lnet/flixster/android/model/NetflixError;->mStatusCode:I

    .line 24
    :cond_1
    const-string v3, "message"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 25
    const-string v3, "message"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lnet/flixster/android/model/NetflixError;->message:Ljava/lang/String;

    .line 28
    .end local v2           #statusJSON:Lorg/json/JSONObject;
    :cond_2
    return-object v1
.end method
