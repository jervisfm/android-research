.class public Lnet/flixster/android/model/NetflixException;
.super Ljava/io/IOException;
.source "NetflixException.java"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field public mMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/io/IOException;-><init>()V

    .line 26
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .parameter "message"

    .prologue
    .line 20
    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p1, p0, Lnet/flixster/android/model/NetflixException;->mMessage:Ljava/lang/String;

    .line 22
    return-void
.end method
