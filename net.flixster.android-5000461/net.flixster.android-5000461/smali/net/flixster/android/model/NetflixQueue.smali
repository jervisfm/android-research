.class public Lnet/flixster/android/model/NetflixQueue;
.super Ljava/lang/Object;
.source "NetflixQueue.java"


# static fields
.field public static final NF_ATHOME:Ljava/lang/String; = "at_home"

.field public static final NF_ATHOMEITEM:Ljava/lang/String; = "at_home_item"

.field public static final NF_ETAG:Ljava/lang/String; = "etag"

.field public static final NF_NUMBER_OF_RESULTS:Ljava/lang/String; = "number_of_results"

.field public static final NF_QUEUE:Ljava/lang/String; = "queue"

.field public static final NF_QUEUEITEM:Ljava/lang/String; = "queue_item"

.field public static final NF_RESULTS_PER_PAGE:Ljava/lang/String; = "results_per_page"

.field public static final NF_START_INDEX:Ljava/lang/String; = "start_index"


# instance fields
.field private mNetflixQueueItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/NetflixQueueItem;",
            ">;"
        }
    .end annotation
.end field

.field private numberOfResults:I

.field private resultsPerPage:I

.field private startIndex:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput v0, p0, Lnet/flixster/android/model/NetflixQueue;->startIndex:I

    .line 29
    iput v0, p0, Lnet/flixster/android/model/NetflixQueue;->resultsPerPage:I

    .line 30
    iput v0, p0, Lnet/flixster/android/model/NetflixQueue;->numberOfResults:I

    .line 15
    return-void
.end method


# virtual methods
.method public getNetflixQueueItemList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/NetflixQueueItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lnet/flixster/android/model/NetflixQueue;->mNetflixQueueItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getNumberOfResults()I
    .locals 1

    .prologue
    .line 43
    iget v0, p0, Lnet/flixster/android/model/NetflixQueue;->numberOfResults:I

    return v0
.end method

.method public getResultsPerPage()I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lnet/flixster/android/model/NetflixQueue;->resultsPerPage:I

    return v0
.end method

.method public getStartIndex()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lnet/flixster/android/model/NetflixQueue;->startIndex:I

    return v0
.end method

.method public parseFromJson(Lorg/json/JSONObject;Z)Lnet/flixster/android/model/NetflixQueue;
    .locals 11
    .parameter "jsonResponse"
    .parameter "ignoreSavedItem"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/high16 v10, -0x8000

    .line 54
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lnet/flixster/android/model/NetflixQueue;->mNetflixQueueItemList:Ljava/util/ArrayList;

    .line 55
    const-string v7, "FlxMain"

    const-string v8, "NetflixQueue.parseFromJson"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v7, "queue"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 57
    const-string v7, "queue"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 59
    .local v2, jsonQueue:Lorg/json/JSONObject;
    const-string v7, "etag"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 60
    const-string v7, "etag"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, etag:Ljava/lang/String;
    const-string v7, "etag"

    invoke-virtual {v2, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 62
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setNetflixEtag(Ljava/lang/String;)V

    .line 64
    .end local v0           #etag:Ljava/lang/String;
    :cond_0
    const-string v7, "start_index"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 65
    const-string v7, "start_index"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/NetflixQueue;->startIndex:I

    .line 67
    :cond_1
    const-string v7, "results_per_page"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 68
    const-string v7, "results_per_page"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/NetflixQueue;->resultsPerPage:I

    .line 70
    :cond_2
    const-string v7, "number_of_results"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 71
    const-string v7, "number_of_results"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/NetflixQueue;->numberOfResults:I

    .line 74
    :cond_3
    const-string v7, "queue_item"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 75
    const-string v7, "queue_item"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 76
    .local v4, jsonQueueList:Lorg/json/JSONArray;
    if-eqz v4, :cond_c

    .line 78
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    .line 79
    .local v5, length:I
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "NetflixQueue.parseFromJson length:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    const/4 v6, 0x0

    .line 81
    .local v6, netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const/4 v1, 0x0

    .local v1, i:I
    :goto_0
    if-lt v1, v5, :cond_a

    .line 108
    .end local v1           #i:I
    .end local v2           #jsonQueue:Lorg/json/JSONObject;
    .end local v4           #jsonQueueList:Lorg/json/JSONArray;
    .end local v5           #length:I
    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_4
    :goto_1
    const-string v7, "at_home"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 109
    const-string v7, "at_home"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 111
    .restart local v2       #jsonQueue:Lorg/json/JSONObject;
    const-string v7, "etag"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 112
    const-string v7, "etag"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 113
    .restart local v0       #etag:Ljava/lang/String;
    const-string v7, "etag"

    invoke-virtual {v2, v7, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 114
    invoke-static {v0}, Lnet/flixster/android/FlixsterApplication;->setNetflixEtag(Ljava/lang/String;)V

    .line 116
    .end local v0           #etag:Ljava/lang/String;
    :cond_5
    const-string v7, "start_index"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 117
    const-string v7, "start_index"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/NetflixQueue;->startIndex:I

    .line 119
    :cond_6
    const-string v7, "results_per_page"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 120
    const-string v7, "results_per_page"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/NetflixQueue;->resultsPerPage:I

    .line 122
    :cond_7
    const-string v7, "number_of_results"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 123
    const-string v7, "number_of_results"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/NetflixQueue;->numberOfResults:I

    .line 126
    :cond_8
    const-string v7, "at_home_item"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 127
    const-string v7, "FlxMain"

    const-string v8, "NetflixQueue.parsing jsonQueue.has(NF_ATHOMEITEM)"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    const-string v7, "at_home_item"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 129
    .restart local v4       #jsonQueueList:Lorg/json/JSONArray;
    if-eqz v4, :cond_10

    .line 131
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    .line 132
    .restart local v5       #length:I
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "NetflixQueue.parsing jsonQueue.has(NF_ATHOMEITEM) length:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const/4 v6, 0x0

    .line 134
    .restart local v6       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const/4 v1, 0x0

    .restart local v1       #i:I
    :goto_2
    if-lt v1, v5, :cond_e

    .line 163
    .end local v1           #i:I
    .end local v2           #jsonQueue:Lorg/json/JSONObject;
    .end local v4           #jsonQueueList:Lorg/json/JSONArray;
    .end local v5           #length:I
    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_9
    :goto_3
    return-object p0

    .line 83
    .restart local v1       #i:I
    .restart local v2       #jsonQueue:Lorg/json/JSONObject;
    .restart local v4       #jsonQueueList:Lorg/json/JSONArray;
    .restart local v5       #length:I
    .restart local v6       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_a
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 84
    .local v3, jsonQueueItem:Lorg/json/JSONObject;
    new-instance v6, Lnet/flixster/android/model/NetflixQueueItem;

    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-direct {v6}, Lnet/flixster/android/model/NetflixQueueItem;-><init>()V

    .line 85
    .restart local v6       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-virtual {v6, v3}, Lnet/flixster/android/model/NetflixQueueItem;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/NetflixQueueItem;

    .line 86
    if-eqz p2, :cond_b

    invoke-virtual {v6}, Lnet/flixster/android/model/NetflixQueueItem;->isSaved()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 88
    iput v10, p0, Lnet/flixster/android/model/NetflixQueue;->numberOfResults:I

    goto/16 :goto_1

    .line 91
    :cond_b
    iget-object v7, p0, Lnet/flixster/android/model/NetflixQueue;->mNetflixQueueItemList:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 95
    .end local v1           #i:I
    .end local v3           #jsonQueueItem:Lorg/json/JSONObject;
    .end local v5           #length:I
    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_c
    const/4 v6, 0x0

    .line 96
    .restart local v6       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const-string v7, "queue_item"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 97
    .restart local v3       #jsonQueueItem:Lorg/json/JSONObject;
    new-instance v6, Lnet/flixster/android/model/NetflixQueueItem;

    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-direct {v6}, Lnet/flixster/android/model/NetflixQueueItem;-><init>()V

    .line 98
    .restart local v6       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-virtual {v6, v3}, Lnet/flixster/android/model/NetflixQueueItem;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/NetflixQueueItem;

    .line 99
    if-eqz p2, :cond_d

    invoke-virtual {v6}, Lnet/flixster/android/model/NetflixQueueItem;->isSaved()Z

    move-result v7

    if-eqz v7, :cond_d

    .line 101
    iput v10, p0, Lnet/flixster/android/model/NetflixQueue;->numberOfResults:I

    goto/16 :goto_1

    .line 103
    :cond_d
    iget-object v7, p0, Lnet/flixster/android/model/NetflixQueue;->mNetflixQueueItemList:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 136
    .end local v3           #jsonQueueItem:Lorg/json/JSONObject;
    .restart local v1       #i:I
    .restart local v5       #length:I
    :cond_e
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 137
    .restart local v3       #jsonQueueItem:Lorg/json/JSONObject;
    new-instance v6, Lnet/flixster/android/model/NetflixQueueItem;

    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-direct {v6}, Lnet/flixster/android/model/NetflixQueueItem;-><init>()V

    .line 138
    .restart local v6       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-virtual {v6, v3}, Lnet/flixster/android/model/NetflixQueueItem;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/NetflixQueueItem;

    .line 139
    if-eqz p2, :cond_f

    invoke-virtual {v6}, Lnet/flixster/android/model/NetflixQueueItem;->isSaved()Z

    move-result v7

    if-eqz v7, :cond_f

    .line 141
    iput v10, p0, Lnet/flixster/android/model/NetflixQueue;->numberOfResults:I

    goto :goto_3

    .line 144
    :cond_f
    iget-object v7, p0, Lnet/flixster/android/model/NetflixQueue;->mNetflixQueueItemList:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 148
    .end local v1           #i:I
    .end local v3           #jsonQueueItem:Lorg/json/JSONObject;
    .end local v5           #length:I
    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    :cond_10
    const-string v7, "FlxMain"

    const-string v8, "NetflixQueue.parsing jsonQueue.has(NF_ATHOMEITEM) single"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const/4 v6, 0x0

    .line 150
    .restart local v6       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    const-string v7, "at_home_item"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 151
    .restart local v3       #jsonQueueItem:Lorg/json/JSONObject;
    new-instance v6, Lnet/flixster/android/model/NetflixQueueItem;

    .end local v6           #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-direct {v6}, Lnet/flixster/android/model/NetflixQueueItem;-><init>()V

    .line 152
    .restart local v6       #netflixQueueItem:Lnet/flixster/android/model/NetflixQueueItem;
    invoke-virtual {v6, v3}, Lnet/flixster/android/model/NetflixQueueItem;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/NetflixQueueItem;

    .line 153
    if-eqz p2, :cond_11

    invoke-virtual {v6}, Lnet/flixster/android/model/NetflixQueueItem;->isSaved()Z

    move-result v7

    if-eqz v7, :cond_11

    .line 155
    iput v10, p0, Lnet/flixster/android/model/NetflixQueue;->numberOfResults:I

    goto/16 :goto_3

    .line 157
    :cond_11
    iget-object v7, p0, Lnet/flixster/android/model/NetflixQueue;->mNetflixQueueItemList:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3
.end method
