.class public Lnet/flixster/android/model/NetflixQueueItem;
.super Ljava/lang/Object;
.source "NetflixQueueItem.java"


# static fields
.field public static final NF_CATEGORY:Ljava/lang/String; = "category"

.field public static final NF_CAT_AVAILBLE_NOW:Ljava/lang/String; = "available now"

.field public static final NF_CAT_SAVED:Ljava/lang/String; = "saved"

.field public static final NF_ID:Ljava/lang/String; = "id"

.field public static final NF_POSITION:Ljava/lang/String; = "position"

.field public static final NF_TITLE:Ljava/lang/String; = "title"

.field public static final NF_URL_ID_BASE:Ljava/lang/String; = "http://api-public.netflix.com/catalog/titles/movies/"

.field public static final STRING_THUMBNAIL_URL:Ljava/lang/String; = "box_art"


# instance fields
.field public mMovie:Lnet/flixster/android/model/Movie;

.field private mPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mThumbOrderStamp:J

.field private position:I

.field public thumbnailBitmap:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object v2, p0, Lnet/flixster/android/model/NetflixQueueItem;->mMovie:Lnet/flixster/android/model/Movie;

    .line 32
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lnet/flixster/android/model/NetflixQueueItem;->mThumbOrderStamp:J

    .line 33
    iput-object v2, p0, Lnet/flixster/android/model/NetflixQueueItem;->thumbnailBitmap:Landroid/graphics/Bitmap;

    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    .line 39
    return-void
.end method


# virtual methods
.method public getNetflixUrlId()Ljava/lang/String;
    .locals 3

    .prologue
    .line 59
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "http://api-public.netflix.com/catalog/titles/movies/"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    const-string v2, "id"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lnet/flixster/android/model/NetflixQueueItem;->position:I

    return v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "propkey"

    .prologue
    .line 42
    iget-object v0, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 45
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getThumbOrderStamp()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lnet/flixster/android/model/NetflixQueueItem;->mThumbOrderStamp:J

    return-wide v0
.end method

.method public isSaved()Z
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    const-string v1, "saved"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/NetflixQueueItem;
    .locals 13
    .parameter "jsonQueueItem"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 64
    const-string v10, "title"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 65
    const-string v10, "title"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v8

    .line 66
    .local v8, titleJsonObj:Lorg/json/JSONObject;
    const-string v10, "regular"

    invoke-virtual {v8, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    .line 67
    iget-object v10, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    const-string v11, "title"

    const-string v12, "regular"

    invoke-virtual {v8, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    .end local v8           #titleJsonObj:Lorg/json/JSONObject;
    :cond_0
    const-string v10, "id"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 72
    const-string v10, "id"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 73
    .local v9, urlId:Ljava/lang/String;
    const/16 v10, 0x2f

    invoke-virtual {v9, v10}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v6

    .line 74
    .local v6, startIndex:I
    add-int/lit8 v10, v6, 0x1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 75
    .local v5, number:Ljava/lang/String;
    iget-object v10, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    const-string v11, "id"

    invoke-static {v5}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    .end local v5           #number:Ljava/lang/String;
    .end local v6           #startIndex:I
    .end local v9           #urlId:Ljava/lang/String;
    :cond_1
    const-string v10, "box_art"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 80
    const-string v10, "box_art"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 81
    .local v1, jsonBoxArt:Lorg/json/JSONObject;
    const-string v10, "medium"

    invoke-virtual {v1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 82
    iget-object v10, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    const-string v11, "box_art"

    const-string v12, "medium"

    invoke-virtual {v1, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-static {v12}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    .end local v1           #jsonBoxArt:Lorg/json/JSONObject;
    :cond_2
    const-string v10, "category"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 86
    const-string v10, "category"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 87
    .local v3, jsonCategoryList:Lorg/json/JSONArray;
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    .line 90
    .local v4, length:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v4, :cond_4

    .line 102
    .end local v0           #i:I
    .end local v3           #jsonCategoryList:Lorg/json/JSONArray;
    .end local v4           #length:I
    :cond_3
    const-string v10, "position"

    const/4 v11, 0x0

    invoke-virtual {p1, v10, v11}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v10

    iput v10, p0, Lnet/flixster/android/model/NetflixQueueItem;->position:I

    .line 103
    return-object p0

    .line 91
    .restart local v0       #i:I
    .restart local v3       #jsonCategoryList:Lorg/json/JSONArray;
    .restart local v4       #length:I
    :cond_4
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 93
    .local v2, jsonCat:Lorg/json/JSONObject;
    const-string v10, "term"

    invoke-virtual {v2, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 94
    .local v7, term:Ljava/lang/String;
    const-string v10, "available now"

    invoke-virtual {v7, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 95
    iget-object v10, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    const-string v11, "available now"

    const-string v12, "available now"

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    :cond_5
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_6
    const-string v10, "saved"

    invoke-virtual {v7, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 97
    iget-object v10, p0, Lnet/flixster/android/model/NetflixQueueItem;->mPropertyMap:Ljava/util/HashMap;

    const-string v11, "saved"

    const-string v12, "saved"

    invoke-virtual {v10, v11, v12}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public setThumbOrderStamp(J)V
    .locals 0
    .parameter "stamp"

    .prologue
    .line 55
    iput-wide p1, p0, Lnet/flixster/android/model/NetflixQueueItem;->mThumbOrderStamp:J

    .line 56
    return-void
.end method
