.class public Lnet/flixster/android/model/NetflixTitleState;
.super Ljava/lang/Object;
.source "NetflixTitleState.java"


# static fields
.field public static final ACT_DVD_ADD:I = 0x2

.field public static final ACT_DVD_IN_QUEUE:I = 0x1

.field public static final ACT_DVD_SAVE:I = 0x4

.field public static final ACT_INSTANT_ADD:I = 0x10

.field public static final ACT_INSTANT_IN_QUEUE:I = 0x8

.field public static final CAT_ADD:Ljava/lang/String; = "Add"

.field public static final CAT_BLURAY:Ljava/lang/String; = "Blu-ray"

.field public static final CAT_DVD:Ljava/lang/String; = "DVD"

.field public static final CAT_INSTANT:Ljava/lang/String; = "Instant"

.field public static final CAT_IN_QUEUE:Ljava/lang/String; = "In Queue"

.field public static final CAT_SAVE:Ljava/lang/String; = "Save"


# instance fields
.field public mCategoriesMask:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    .line 7
    return-void
.end method

.method private parseTitleStateItem(Lorg/json/JSONObject;)V
    .locals 11
    .parameter "jsonTitleStateItem"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 53
    const-string v10, "format"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 55
    const-string v10, "format"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 56
    .local v5, jsonTitleStateItemFormatArray:Lorg/json/JSONArray;
    if-nez v5, :cond_0

    .line 57
    new-instance v5, Lorg/json/JSONArray;

    .end local v5           #jsonTitleStateItemFormatArray:Lorg/json/JSONArray;
    invoke-direct {v5}, Lorg/json/JSONArray;-><init>()V

    .line 58
    .restart local v5       #jsonTitleStateItemFormatArray:Lorg/json/JSONArray;
    const-string v10, "format"

    invoke-virtual {p1, v10}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v10

    invoke-virtual {v5, v10}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 61
    :cond_0
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 63
    .local v7, lengthFormatArray:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v7, :cond_2

    .line 101
    .end local v0           #i:I
    .end local v5           #jsonTitleStateItemFormatArray:Lorg/json/JSONArray;
    .end local v7           #lengthFormatArray:I
    :cond_1
    return-void

    .line 64
    .restart local v0       #i:I
    .restart local v5       #jsonTitleStateItemFormatArray:Lorg/json/JSONArray;
    .restart local v7       #lengthFormatArray:I
    :cond_2
    invoke-virtual {v5, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 65
    .local v4, jsonFormat:Lorg/json/JSONObject;
    const-string v10, "category"

    invoke-virtual {v4, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 66
    .local v2, jsonCategories:Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 69
    .local v6, lengthCat:I
    const/4 v9, 0x0

    .line 70
    .local v9, typeState:Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, j:I
    :goto_1
    if-lt v1, v6, :cond_3

    .line 63
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 71
    :cond_3
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 72
    .local v3, jsonCategory:Lorg/json/JSONObject;
    const-string v10, "term"

    invoke-virtual {v3, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 74
    .local v8, term:Ljava/lang/String;
    const-string v10, "DVD"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_4

    const-string v10, "Blu-ray"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 75
    :cond_4
    const-string v9, "DVD"

    .line 70
    :cond_5
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 76
    :cond_6
    const-string v10, "Instant"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 77
    const-string v9, "Instant"

    goto :goto_2

    .line 78
    :cond_7
    const-string v10, "DVD"

    if-ne v9, v10, :cond_a

    .line 80
    const-string v10, "In Queue"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 81
    iget v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    or-int/lit8 v10, v10, 0x1

    iput v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    goto :goto_2

    .line 82
    :cond_8
    const-string v10, "Add"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 83
    iget v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    or-int/lit8 v10, v10, 0x2

    iput v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    goto :goto_2

    .line 84
    :cond_9
    const-string v10, "Save"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 85
    iget v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    or-int/lit8 v10, v10, 0x4

    iput v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    goto :goto_2

    .line 89
    :cond_a
    const-string v10, "In Queue"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_b

    .line 90
    iget v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    or-int/lit8 v10, v10, 0x8

    iput v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    goto :goto_2

    .line 91
    :cond_b
    const-string v10, "Add"

    invoke-virtual {v8, v10}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 92
    iget v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    or-int/lit8 v10, v10, 0x10

    iput v10, p0, Lnet/flixster/android/model/NetflixTitleState;->mCategoriesMask:I

    goto :goto_2
.end method


# virtual methods
.method public parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/NetflixTitleState;
    .locals 5
    .parameter "jsontTitleState"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 34
    const-string v4, "title_state_item"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    const-string v4, "title_state_item"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 37
    .local v1, jsonTitleStateItem:Lorg/json/JSONObject;
    if-eqz v1, :cond_1

    .line 39
    invoke-direct {p0, v1}, Lnet/flixster/android/model/NetflixTitleState;->parseTitleStateItem(Lorg/json/JSONObject;)V

    .line 49
    .end local v1           #jsonTitleStateItem:Lorg/json/JSONObject;
    :cond_0
    return-object p0

    .line 41
    .restart local v1       #jsonTitleStateItem:Lorg/json/JSONObject;
    :cond_1
    const-string v4, "title_state_item"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 42
    .local v2, jsonTitleStateItemArray:Lorg/json/JSONArray;
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 43
    .local v3, length:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 44
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 45
    invoke-direct {p0, v1}, Lnet/flixster/android/model/NetflixTitleState;->parseTitleStateItem(Lorg/json/JSONObject;)V

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
