.class public Lnet/flixster/android/model/NewsStory;
.super Ljava/lang/Object;
.source "NewsStory.java"


# instance fields
.field public final DESCRIPTION:Ljava/lang/String;

.field public final SOURCE:Ljava/lang/String;

.field public final THUMBNAIL_URL:Ljava/lang/String;

.field public final TITLE:Ljava/lang/String;

.field public mDescription:Ljava/lang/String;

.field public mSoftPhoto:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public mSource:Ljava/lang/String;

.field private mThumbOrderStamp:J

.field public mThumbnailUrl:Ljava/lang/String;

.field public mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    const-string v0, "title"

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->TITLE:Ljava/lang/String;

    .line 15
    const-string v0, "description"

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->DESCRIPTION:Ljava/lang/String;

    .line 16
    const-string v0, "source"

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->SOURCE:Ljava/lang/String;

    .line 17
    const-string v0, "thumbnail"

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->THUMBNAIL_URL:Ljava/lang/String;

    .line 23
    new-instance v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->mSoftPhoto:Ljava/lang/ref/SoftReference;

    .line 25
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lnet/flixster/android/model/NewsStory;->mThumbOrderStamp:J

    .line 12
    return-void
.end method


# virtual methods
.method public getThumbOrderStamp()J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lnet/flixster/android/model/NewsStory;->mThumbOrderStamp:J

    return-wide v0
.end method

.method public parseJSONObject(Lorg/json/JSONObject;)Lnet/flixster/android/model/NewsStory;
    .locals 1
    .parameter "jsonObject"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 38
    const-string v0, "title"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "title"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->mTitle:Ljava/lang/String;

    .line 41
    :cond_0
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 42
    const-string v0, "description"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->mDescription:Ljava/lang/String;

    .line 44
    :cond_1
    const-string v0, "source"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 45
    const-string v0, "source"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->mSource:Ljava/lang/String;

    .line 47
    :cond_2
    const-string v0, "thumbnail"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 48
    const-string v0, "thumbnail"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/NewsStory;->mThumbnailUrl:Ljava/lang/String;

    .line 50
    :cond_3
    return-object p0
.end method

.method public setThumbOrderStamp(J)V
    .locals 0
    .parameter "stamp"

    .prologue
    .line 32
    iput-wide p1, p0, Lnet/flixster/android/model/NewsStory;->mThumbOrderStamp:J

    .line 33
    return-void
.end method
