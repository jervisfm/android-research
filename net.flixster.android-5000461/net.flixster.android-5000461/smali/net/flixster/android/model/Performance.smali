.class public Lnet/flixster/android/model/Performance;
.super Ljava/lang/Object;
.source "Performance.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CATEGORIES:Ljava/lang/String; = "categories"

.field public static final CATEGORY_ID:Ljava/lang/String; = "id"

.field public static final CATEGORY_LABEL:Ljava/lang/String; = "description"

.field public static final CATEGORY_PRICE:Ljava/lang/String; = "amount"

.field public static final CURRENCY:Ljava/lang/String; = "currency"

.field public static final ERROR:Ljava/lang/String; = "error"

.field public static final METHODS:Ljava/lang/String; = "methods"

.field public static final METHOD_ID:Ljava/lang/String; = "id"

.field public static final METHOD_NAME:Ljava/lang/String; = "name"

.field public static final PERFORMANCE:Ljava/lang/String; = "performance"

.field public static final RESERVATION:Ljava/lang/String; = "reservation"

.field public static final SURCHARGE:Ljava/lang/String; = "surcharge"

.field public static final TICKETS:Ljava/lang/String; = "tickets"


# instance fields
.field private mCategoryIds:[Ljava/lang/String;

.field private mCategoryLabels:[Ljava/lang/String;

.field private mCategoryPrices:[F

.field private mCategoryTally:I

.field private mCurrency:Ljava/lang/String;

.field private mError:Ljava/lang/String;

.field private mMethodIds:[Ljava/lang/String;

.field private mMethodNames:[Ljava/lang/String;

.field private mMethodsTally:I

.field private mReservationId:Ljava/lang/String;

.field private mSurcharge:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput v0, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    .line 39
    iput v0, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/model/Performance;->mSurcharge:F

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .parameter "in"

    .prologue
    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput v0, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    .line 39
    iput v0, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    .line 45
    const/4 v0, 0x0

    iput v0, p0, Lnet/flixster/android/model/Performance;->mSurcharge:F

    .line 59
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Performance;->mCurrency:Ljava/lang/String;

    .line 60
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    .line 61
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryIds:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryLabels:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 63
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryPrices:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readFloatArray([F)V

    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    .line 66
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mMethodIds:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mMethodNames:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readStringArray([Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Performance;->mReservationId:Ljava/lang/String;

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readFloat()F

    move-result v0

    iput v0, p0, Lnet/flixster/android/model/Performance;->mSurcharge:F

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Performance;->mError:Ljava/lang/String;

    .line 72
    return-void
.end method


# virtual methods
.method public clearError()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/model/Performance;->mError:Ljava/lang/String;

    .line 116
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    return v0
.end method

.method public getCategoryIds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getCategoryTally()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    return v0
.end method

.method public getError()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mError:Ljava/lang/String;

    return-object v0
.end method

.method public getLabels()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryLabels:[Ljava/lang/String;

    return-object v0
.end method

.method public getMethodIds()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mMethodIds:[Ljava/lang/String;

    return-object v0
.end method

.method public getMethodNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mMethodNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getMethodsTally()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    return v0
.end method

.method public getPrices()[F
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryPrices:[F

    return-object v0
.end method

.method public getReservationId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mReservationId:Ljava/lang/String;

    return-object v0
.end method

.method public getSurcharge()F
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lnet/flixster/android/model/Performance;->mSurcharge:F

    return v0
.end method

.method public parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Performance;
    .locals 10
    .parameter "jsonPerformance"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 120
    const-string v7, "FlxMain"

    const-string v8, "Performance.parseFromJSON"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v7, "error"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 123
    const-string v7, "error"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lnet/flixster/android/model/Performance;->mError:Ljava/lang/String;

    .line 177
    :cond_0
    :goto_0
    return-object p0

    .line 130
    :cond_1
    const-string v7, "tickets"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 131
    const-string v7, "tickets"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v6

    .line 132
    .local v6, jsonTickets:Lorg/json/JSONObject;
    const-string v7, "currency"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lnet/flixster/android/model/Performance;->mCurrency:Ljava/lang/String;

    .line 135
    const-string v7, "surcharge"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 136
    const-string v7, "surcharge"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Float;->floatValue()F

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/Performance;->mSurcharge:F

    .line 140
    :cond_2
    const-string v7, "categories"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 141
    .local v1, jsonCategories:Lorg/json/JSONArray;
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    .line 144
    iget v7, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    new-array v7, v7, [Ljava/lang/String;

    iput-object v7, p0, Lnet/flixster/android/model/Performance;->mCategoryIds:[Ljava/lang/String;

    .line 145
    iget v7, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    new-array v7, v7, [Ljava/lang/String;

    iput-object v7, p0, Lnet/flixster/android/model/Performance;->mCategoryLabels:[Ljava/lang/String;

    .line 146
    iget v7, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    new-array v7, v7, [F

    iput-object v7, p0, Lnet/flixster/android/model/Performance;->mCategoryPrices:[F

    .line 148
    const/4 v0, 0x0

    .local v0, i:I
    :goto_1
    iget v7, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    if-lt v0, v7, :cond_4

    .line 155
    const-string v7, "FlxMain"

    const-string v8, "Performance.parseFromJSON doing methods"

    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    const-string v7, "methods"

    invoke-virtual {v6, v7}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 158
    .local v4, jsonMethods:Lorg/json/JSONArray;
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    iput v7, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    .line 161
    iget v7, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    new-array v7, v7, [Ljava/lang/String;

    iput-object v7, p0, Lnet/flixster/android/model/Performance;->mMethodIds:[Ljava/lang/String;

    .line 162
    iget v7, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    new-array v7, v7, [Ljava/lang/String;

    iput-object v7, p0, Lnet/flixster/android/model/Performance;->mMethodNames:[Ljava/lang/String;

    .line 163
    const/4 v0, 0x0

    :goto_2
    iget v7, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    if-lt v0, v7, :cond_5

    .line 172
    .end local v0           #i:I
    .end local v1           #jsonCategories:Lorg/json/JSONArray;
    .end local v4           #jsonMethods:Lorg/json/JSONArray;
    .end local v6           #jsonTickets:Lorg/json/JSONObject;
    :cond_3
    const-string v7, "reservation"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 173
    const-string v7, "reservation"

    invoke-virtual {p1, v7}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v5

    .line 174
    .local v5, jsonReservation:Lorg/json/JSONObject;
    const-string v7, "id"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lnet/flixster/android/model/Performance;->mReservationId:Ljava/lang/String;

    goto/16 :goto_0

    .line 149
    .end local v5           #jsonReservation:Lorg/json/JSONObject;
    .restart local v0       #i:I
    .restart local v1       #jsonCategories:Lorg/json/JSONArray;
    .restart local v6       #jsonTickets:Lorg/json/JSONObject;
    :cond_4
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 150
    .local v2, jsonCategory:Lorg/json/JSONObject;
    iget-object v7, p0, Lnet/flixster/android/model/Performance;->mCategoryIds:[Ljava/lang/String;

    const-string v8, "id"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v0

    .line 151
    iget-object v7, p0, Lnet/flixster/android/model/Performance;->mCategoryLabels:[Ljava/lang/String;

    const-string v8, "description"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v0

    .line 152
    iget-object v7, p0, Lnet/flixster/android/model/Performance;->mCategoryPrices:[F

    const-string v8, "amount"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Float;->floatValue()F

    move-result v8

    aput v8, v7, v0

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 164
    .end local v2           #jsonCategory:Lorg/json/JSONObject;
    .restart local v4       #jsonMethods:Lorg/json/JSONArray;
    :cond_5
    invoke-virtual {v4, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 165
    .local v3, jsonMethod:Lorg/json/JSONObject;
    iget-object v7, p0, Lnet/flixster/android/model/Performance;->mMethodIds:[Ljava/lang/String;

    const-string v8, "id"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v0

    .line 166
    iget-object v7, p0, Lnet/flixster/android/model/Performance;->mMethodNames:[Ljava/lang/String;

    const-string v8, "name"

    invoke-virtual {v3, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v0

    .line 167
    const-string v7, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Performance.parseFromJSON doing methods mMethodIds[i]:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, p0, Lnet/flixster/android/model/Performance;->mMethodIds:[Ljava/lang/String;

    aget-object v9, v9, v0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    .line 168
    const-string v9, " mMethodNames[i]:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lnet/flixster/android/model/Performance;->mMethodNames:[Ljava/lang/String;

    aget-object v9, v9, v0

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 167
    invoke-static {v7, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .parameter "dest"
    .parameter "flags"

    .prologue
    .line 186
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCurrency:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 187
    iget v0, p0, Lnet/flixster/android/model/Performance;->mCategoryTally:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 188
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryIds:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 189
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryLabels:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 190
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mCategoryPrices:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 192
    iget v0, p0, Lnet/flixster/android/model/Performance;->mMethodsTally:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 193
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mMethodIds:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 194
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mMethodNames:[Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    .line 195
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mReservationId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 196
    iget v0, p0, Lnet/flixster/android/model/Performance;->mSurcharge:F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloat(F)V

    .line 197
    iget-object v0, p0, Lnet/flixster/android/model/Performance;->mError:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 198
    return-void
.end method
