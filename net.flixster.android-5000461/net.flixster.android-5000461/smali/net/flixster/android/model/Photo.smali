.class public Lnet/flixster/android/model/Photo;
.super Ljava/lang/Object;
.source "Photo.java"


# instance fields
.field public actorId:J

.field public actorName:Ljava/lang/String;

.field public bitmap:Landroid/graphics/Bitmap;

.field public caption:Ljava/lang/String;

.field public description:Ljava/lang/String;

.field public galleryBitmap:Ljava/lang/ref/SoftReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/SoftReference",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public galleryUrl:Ljava/lang/String;

.field public id:J

.field public movieId:J

.field public movieTitle:Ljava/lang/String;

.field public origionalUrl:Ljava/lang/String;

.field private thumbnailBitmap:Lcom/flixster/android/model/Image;

.field public thumbnailUrl:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lnet/flixster/android/model/Photo;->galleryBitmap:Ljava/lang/ref/SoftReference;

    .line 10
    return-void
.end method


# virtual methods
.method public getThumbnailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 28
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    iget-object v0, p0, Lnet/flixster/android/model/Photo;->thumbnailBitmap:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lcom/flixster/android/model/Image;

    iget-object v1, p0, Lnet/flixster/android/model/Photo;->thumbnailUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/Photo;->thumbnailBitmap:Lcom/flixster/android/model/Image;

    .line 31
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Photo;->thumbnailBitmap:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
