.class public Lnet/flixster/android/model/Property;
.super Ljava/lang/Object;
.source "Property.java"


# static fields
.field private static final ELEVEN_BUTTON_TEXT:Ljava/lang/String; = "elevenButtonText"

.field private static final ELEVEN_CREATIVE_URL:Ljava/lang/String; = "elevenCreativeUrl"

.field private static final IS_CLIENT_CAPPED:Ljava/lang/String; = "isClientCapped"

.field private static final IS_ELEVEN_ENABLED:Ljava/lang/String; = "isElevenEnabled"

.field private static final IS_GLUE_ENABLED:Ljava/lang/String; = "isGlueEnabled"

.field private static final IS_MSK_ENABLED:Ljava/lang/String; = "isMskEnabled"

.field private static final IS_MSK_FIRST_LAUNCH_ENABLED:Ljava/lang/String; = "isMskFirstLaunchEnabled"

.field private static final MSK_ANON_ENTRY:Ljava/lang/String; = "msk.anon.entry"

.field private static final MSK_ANON_PROMO_URL:Ljava/lang/String; = "mskAnonPromoUrl"

.field private static final MSK_FB_ENTRY:Ljava/lang/String; = "msk.fb.entry"

.field private static final MSK_FB_LOGIN_EXIT:Ljava/lang/String; = "msk.fb.login.exit"

.field private static final MSK_FB_REQUEST_BODY:Ljava/lang/String; = "msk.fb.request.body"

.field private static final MSK_FB_REQUEST_EXIT:Ljava/lang/String; = "msk.fb.request.exit"

.field private static final MSK_FIRST_LAUNCH_ENTRY:Ljava/lang/String; = "msk.first.launch.entry"

.field private static final MSK_FLX_ENTRY:Ljava/lang/String; = "msk.flx.entry"

.field private static final MSK_FLX_LOGIN_EXIT:Ljava/lang/String; = "msk.flx.login.exit"

.field public static final MSK_SELECTION_PICK_EXIT:Ljava/lang/String; = "msk.selection.pick.exit"

.field public static final MSK_SELECTION_SEND_EXIT:Ljava/lang/String; = "msk.selection.send.exit"

.field public static final MSK_SELECTION_SKIP_EXIT:Ljava/lang/String; = "msk.selection.skip.exit"

.field private static final MSK_TEXT_REQUEST_BODY:Ljava/lang/String; = "msk.text.request.body"

.field private static final MSK_TEXT_REQUEST_EXIT:Ljava/lang/String; = "msk.text.request.exit"

.field private static final MSK_TEXT_REQUEST_REWARDS:Ljava/lang/String; = "msk.text.request.rewards"

.field private static final MSK_TOOLTIP_URL:Ljava/lang/String; = "mskTooltipUrl"

.field private static final MSK_USER_PROMO_URL:Ljava/lang/String; = "mskUserPromoUrl"

.field private static final MSK_VARIATION:Ljava/lang/String; = "msk.variation"

.field private static final REWARDS_FBINVITE:Ljava/lang/String; = "rewardsFbInvite"

.field private static final REWARDS_FBINVITE_ENABLED:Ljava/lang/String; = "rewardsFbInviteEnabled"

.field private static final REWARDS_QUICKRATE:Ljava/lang/String; = "rewardsQuickRate"

.field private static final REWARDS_QUICKRATE_ENABLED:Ljava/lang/String; = "rewardsQuickRateEnabled"

.field private static final REWARDS_QUICKWTS:Ljava/lang/String; = "rewardsQuickWts"

.field private static final REWARDS_QUICKWTS_ENABLED:Ljava/lang/String; = "rewardsQuickWtsEnabled"

.field private static final REWARDS_SMS:Ljava/lang/String; = "rewardsSms"

.field private static final REWARDS_SMS_BODY:Ljava/lang/String; = "rewardsSmsBody"

.field private static final REWARDS_SMS_ENABLED:Ljava/lang/String; = "rewardsSmsEnabled"


# instance fields
.field public final elevenButtonText:Ljava/lang/String;

.field public final elevenCreativeUrl:Ljava/lang/String;

.field public final is7ElevenEnabled:Z

.field public final isClientCapped:Z

.field public final isFbInviteRewardEnabled:Z

.field public final isGetGlueEnabled:Z

.field public final isMskEnabled:Z

.field public final isMskFirstLaunchEnabled:Z

.field public final isQuickRateRewardEnabled:Z

.field public final isQuickWtsRewardEnabled:Z

.field public final isSmsRewardEnabled:Z

.field private final json:Lorg/json/JSONObject;

.field public final mskAnonEntry:Ljava/lang/String;

.field public final mskAnonPromoUrl:Ljava/lang/String;

.field public final mskFbEntry:Ljava/lang/String;

.field public final mskFbLoginExit:Ljava/lang/String;

.field public final mskFbRequestBody:Ljava/lang/String;

.field public final mskFbRequestExit:Ljava/lang/String;

.field public final mskFirstLaunchEntry:Ljava/lang/String;

.field public final mskFlxEntry:Ljava/lang/String;

.field public final mskFlxLoginExit:Ljava/lang/String;

.field public final mskTextRequestBody:Ljava/lang/String;

.field public final mskTextRequestExit:Ljava/lang/String;

.field public final mskTextRequestRewards:Ljava/lang/String;

.field public final mskTextRequestRewardsArray:[I

.field public final mskTooltipUrl:Ljava/lang/String;

.field public final mskUserPromoUrl:Ljava/lang/String;

.field public final mskVariation:Ljava/lang/String;

.field public final rewardsFbInvite:Ljava/lang/String;

.field public final rewardsFbInviteArray:[I

.field public final rewardsQuickRate:I

.field public final rewardsQuickWts:I

.field public final rewardsSms:Ljava/lang/String;

.field public final rewardsSmsArray:[I

.field public final rewardsSmsBody:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 9
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x4

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput-object p1, p0, Lnet/flixster/android/model/Property;->json:Lorg/json/JSONObject;

    .line 75
    const-string v2, "isMskEnabled"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->isMskEnabled:Z

    .line 76
    const-string v2, "isMskFirstLaunchEnabled"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->isMskFirstLaunchEnabled:Z

    .line 77
    const-string v2, "mskAnonPromoUrl"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskAnonPromoUrl:Ljava/lang/String;

    .line 78
    const-string v2, "mskUserPromoUrl"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskUserPromoUrl:Ljava/lang/String;

    .line 79
    const-string v2, "mskTooltipUrl"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskTooltipUrl:Ljava/lang/String;

    .line 80
    const-string v2, "rewardsQuickRateEnabled"

    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->isQuickRateRewardEnabled:Z

    .line 81
    const-string v2, "rewardsQuickRate"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/model/Property;->rewardsQuickRate:I

    .line 82
    const-string v2, "rewardsQuickWtsEnabled"

    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->isQuickWtsRewardEnabled:Z

    .line 83
    const-string v2, "rewardsQuickWts"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/model/Property;->rewardsQuickWts:I

    .line 84
    const-string v2, "rewardsSmsEnabled"

    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->isSmsRewardEnabled:Z

    .line 85
    const-string v2, "rewardsSmsBody"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->rewardsSmsBody:Ljava/lang/String;

    .line 86
    const-string v2, "rewardsSms"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->rewardsSms:Ljava/lang/String;

    .line 87
    iget-object v2, p0, Lnet/flixster/android/model/Property;->rewardsSms:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 88
    new-array v2, v7, [I

    iput-object v2, p0, Lnet/flixster/android/model/Property;->rewardsSmsArray:[I

    .line 89
    iget-object v2, p0, Lnet/flixster/android/model/Property;->rewardsSmsArray:[I

    aput v5, v2, v5

    .line 90
    iget-object v2, p0, Lnet/flixster/android/model/Property;->rewardsSms:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 91
    .local v1, rewardsArray:[Ljava/lang/String;
    const/4 v0, 0x1

    .local v0, i:I
    :goto_0
    if-lt v0, v7, :cond_0

    .line 97
    .end local v0           #i:I
    .end local v1           #rewardsArray:[Ljava/lang/String;
    :goto_1
    const-string v2, "rewardsFbInviteEnabled"

    invoke-virtual {p1, v2, v6}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->isFbInviteRewardEnabled:Z

    .line 98
    const-string v2, "rewardsFbInvite"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->rewardsFbInvite:Ljava/lang/String;

    .line 99
    iget-object v2, p0, Lnet/flixster/android/model/Property;->rewardsFbInvite:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 100
    new-array v2, v8, [I

    iput-object v2, p0, Lnet/flixster/android/model/Property;->rewardsFbInviteArray:[I

    .line 101
    iget-object v2, p0, Lnet/flixster/android/model/Property;->rewardsFbInvite:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 102
    .restart local v1       #rewardsArray:[Ljava/lang/String;
    const/4 v0, 0x0

    .restart local v0       #i:I
    :goto_2
    if-lt v0, v8, :cond_2

    .line 109
    .end local v0           #i:I
    .end local v1           #rewardsArray:[Ljava/lang/String;
    :goto_3
    const-string v2, "msk.anon.entry"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskAnonEntry:Ljava/lang/String;

    .line 110
    const-string v2, "msk.fb.entry"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskFbEntry:Ljava/lang/String;

    .line 111
    const-string v2, "msk.flx.entry"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskFlxEntry:Ljava/lang/String;

    .line 112
    const-string v2, "msk.fb.login.exit"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskFbLoginExit:Ljava/lang/String;

    .line 113
    const-string v2, "msk.flx.login.exit"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskFlxLoginExit:Ljava/lang/String;

    .line 114
    const-string v2, "msk.fb.request.exit"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskFbRequestExit:Ljava/lang/String;

    .line 115
    const-string v2, "msk.text.request.exit"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskTextRequestExit:Ljava/lang/String;

    .line 116
    const-string v2, "msk.variation"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    .line 117
    const-string v2, "msk.fb.request.body"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskFbRequestBody:Ljava/lang/String;

    .line 118
    const-string v2, "msk.text.request.body"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskTextRequestBody:Ljava/lang/String;

    .line 119
    const-string v2, "msk.text.request.rewards"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskTextRequestRewards:Ljava/lang/String;

    .line 120
    iget-object v2, p0, Lnet/flixster/android/model/Property;->mskTextRequestRewards:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 121
    new-array v2, v7, [I

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskTextRequestRewardsArray:[I

    .line 122
    iget-object v2, p0, Lnet/flixster/android/model/Property;->mskTextRequestRewardsArray:[I

    aput v5, v2, v5

    .line 123
    iget-object v2, p0, Lnet/flixster/android/model/Property;->mskTextRequestRewards:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 124
    .restart local v1       #rewardsArray:[Ljava/lang/String;
    const/4 v0, 0x1

    .restart local v0       #i:I
    :goto_4
    if-lt v0, v7, :cond_4

    .line 130
    .end local v0           #i:I
    .end local v1           #rewardsArray:[Ljava/lang/String;
    :goto_5
    const-string v2, "msk.first.launch.entry"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->mskFirstLaunchEntry:Ljava/lang/String;

    .line 131
    const-string v2, "isGlueEnabled"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->isGetGlueEnabled:Z

    .line 132
    const-string v2, "isElevenEnabled"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->is7ElevenEnabled:Z

    .line 133
    const-string v2, "elevenCreativeUrl"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->elevenCreativeUrl:Ljava/lang/String;

    .line 134
    const-string v2, "elevenButtonText"

    invoke-virtual {p1, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lnet/flixster/android/model/Property;->elevenButtonText:Ljava/lang/String;

    .line 135
    const-string v2, "isClientCapped"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lnet/flixster/android/model/Property;->isClientCapped:Z

    .line 136
    return-void

    .line 92
    .restart local v0       #i:I
    .restart local v1       #rewardsArray:[Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/model/Property;->rewardsSmsArray:[I

    add-int/lit8 v3, v0, -0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 91
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 95
    .end local v0           #i:I
    .end local v1           #rewardsArray:[Ljava/lang/String;
    :cond_1
    iput-object v4, p0, Lnet/flixster/android/model/Property;->rewardsSmsArray:[I

    goto/16 :goto_1

    .line 103
    .restart local v0       #i:I
    .restart local v1       #rewardsArray:[Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/model/Property;->rewardsFbInviteArray:[I

    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    .line 106
    .end local v0           #i:I
    .end local v1           #rewardsArray:[Ljava/lang/String;
    :cond_3
    iput-object v4, p0, Lnet/flixster/android/model/Property;->rewardsFbInviteArray:[I

    goto/16 :goto_3

    .line 125
    .restart local v0       #i:I
    .restart local v1       #rewardsArray:[Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lnet/flixster/android/model/Property;->mskTextRequestRewardsArray:[I

    add-int/lit8 v3, v0, -0x1

    aget-object v3, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    .line 124
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 128
    .end local v0           #i:I
    .end local v1           #rewardsArray:[Ljava/lang/String;
    :cond_5
    iput-object v4, p0, Lnet/flixster/android/model/Property;->mskTextRequestRewardsArray:[I

    goto :goto_5
.end method


# virtual methods
.method public getMskEntry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    invoke-static {}, Lcom/flixster/android/data/AccountFacade;->getSocialUser()Lnet/flixster/android/model/User;

    move-result-object v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lnet/flixster/android/model/Property;->mskAnonEntry:Ljava/lang/String;

    .line 168
    :goto_0
    return-object v0

    .line 165
    :cond_0
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/flixster/android/data/AccountManager;->isPlatformFacebook()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 166
    iget-object v0, p0, Lnet/flixster/android/model/Property;->mskFbEntry:Ljava/lang/String;

    goto :goto_0

    .line 168
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/model/Property;->mskFlxEntry:Ljava/lang/String;

    goto :goto_0
.end method

.method public optString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .parameter "name"

    .prologue
    .line 140
    iget-object v0, p0, Lnet/flixster/android/model/Property;->json:Lorg/json/JSONObject;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{ isMskEnabled="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->isMskEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isMskFirstLaunchEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->isMskFirstLaunchEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 146
    const-string v1, ", mskAnonPromoUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskAnonPromoUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskUserPromoUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskUserPromoUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskTooltipUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskTooltipUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isQuickRateRewardEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->isQuickRateRewardEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rewardsQuickRate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 148
    iget v1, p0, Lnet/flixster/android/model/Property;->rewardsQuickRate:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isQuickWtsRewardEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->isQuickWtsRewardEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rewardsQuickWts="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 149
    iget v1, p0, Lnet/flixster/android/model/Property;->rewardsQuickWts:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isSmsRewardEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->isSmsRewardEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rewardsSmsBody="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->rewardsSmsBody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 150
    const-string v1, ", rewardsSms="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->rewardsSms:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isFbInviteRewardEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->isFbInviteRewardEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 151
    const-string v1, ", rewardsFbInvite="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->rewardsFbInvite:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskAnonEntry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskAnonEntry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskFbEntry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskFbEntry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskFlxEntry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskFlxEntry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskFbLoginExit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskFbLoginExit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 153
    const-string v1, ", mskFlxLoginExit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskFlxLoginExit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskFbRequestExit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskFbRequestExit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 154
    const-string v1, ", mskTextRequestExit="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskTextRequestExit:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskVariation="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskVariation:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 155
    const-string v1, ", mskFbRequestBody="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskFbRequestBody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskTextRequestBody="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskTextRequestBody:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 156
    const-string v1, ", mskTextRequestRewards="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskTextRequestRewards:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mskFirstLaunchEntry="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->mskFirstLaunchEntry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 157
    const-string v1, ", isGetGlueEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->isGetGlueEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isClientCapEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->isClientCapped:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 158
    const-string v1, ", is7ElevenEnabled="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lnet/flixster/android/model/Property;->is7ElevenEnabled:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", elevenCreativeUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->elevenCreativeUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 159
    const-string v1, ", elevenButtonText="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lnet/flixster/android/model/Property;->elevenButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 145
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
