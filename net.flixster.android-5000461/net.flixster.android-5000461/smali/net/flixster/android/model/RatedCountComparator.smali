.class public Lnet/flixster/android/model/RatedCountComparator;
.super Ljava/lang/Object;
.source "RatedCountComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lnet/flixster/android/model/User;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lnet/flixster/android/model/User;

    check-cast p2, Lnet/flixster/android/model/User;

    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/RatedCountComparator;->compare(Lnet/flixster/android/model/User;Lnet/flixster/android/model/User;)I

    move-result v0

    return v0
.end method

.method public compare(Lnet/flixster/android/model/User;Lnet/flixster/android/model/User;)I
    .locals 2
    .parameter "user1"
    .parameter "user2"

    .prologue
    .line 6
    iget v0, p2, Lnet/flixster/android/model/User;->ratedCount:I

    iget v1, p1, Lnet/flixster/android/model/User;->ratedCount:I

    sub-int/2addr v0, v1

    return v0
.end method
