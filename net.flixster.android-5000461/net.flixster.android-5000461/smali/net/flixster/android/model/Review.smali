.class public Lnet/flixster/android/model/Review;
.super Ljava/lang/Object;
.source "Review.java"


# static fields
.field private static final CRITIC_NONE_TMB:Ljava/lang/String; = "critic_default_icon.gif"

.field public static final REVIEWTYPE_CRITIC:I = 0x0

.field public static final REVIEWTYPE_FRIEND:I = 0x2

.field public static final REVIEWTYPE_USER:I = 0x1


# instance fields
.field public comment:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field private movie:Lnet/flixster/android/model/Movie;

.field public mugUrl:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field private profileBitmap:Lcom/flixster/android/model/Image;

.field public score:I

.field public source:Ljava/lang/String;

.field public stars:D

.field public type:I

.field public url:Ljava/lang/String;

.field public userId:J

.field public userName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x0

    return v0
.end method

.method public getActionId()I
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0}, Lnet/flixster/android/model/Review;->isWantToSee()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0c0170

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0c0171

    goto :goto_0
.end method

.method public getMovie()Lnet/flixster/android/model/Movie;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lnet/flixster/android/model/Review;->movie:Lnet/flixster/android/model/Movie;

    return-object v0
.end method

.method public getReviewerBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    iget-object v0, p0, Lnet/flixster/android/model/Review;->mugUrl:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lnet/flixster/android/model/Review;->mugUrl:Ljava/lang/String;

    const-string v1, "user.none.tmb.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/model/Review;->mugUrl:Ljava/lang/String;

    const-string v1, "critic_default_icon.gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    :cond_0
    const/4 v0, 0x0

    .line 71
    :goto_0
    return-object v0

    .line 68
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/model/Review;->profileBitmap:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_2

    .line 69
    new-instance v0, Lcom/flixster/android/model/Image;

    iget-object v1, p0, Lnet/flixster/android/model/Review;->mugUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/Review;->profileBitmap:Lcom/flixster/android/model/Image;

    .line 71
    :cond_2
    iget-object v0, p0, Lnet/flixster/android/model/Review;->profileBitmap:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lnet/flixster/android/model/Review;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 80
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    invoke-virtual {p0}, Lnet/flixster/android/model/Review;->isWantToSee()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    const-string v1, "wants to see..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    :cond_0
    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 83
    :cond_1
    invoke-virtual {p0}, Lnet/flixster/android/model/Review;->isNotInterested()Z

    move-result v1

    if-nez v1, :cond_0

    .line 84
    const-string v1, "rated a movie..."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public isNotInterested()Z
    .locals 4

    .prologue
    .line 46
    iget-wide v0, p0, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v2, 0x4000

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4028

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWantToSee()Z
    .locals 4

    .prologue
    .line 42
    iget-wide v0, p0, Lnet/flixster/android/model/Review;->stars:D

    const-wide/high16 v2, 0x4000

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4026

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setMovie(Lnet/flixster/android/model/Movie;)V
    .locals 0
    .parameter "movie"

    .prologue
    .line 54
    iput-object p1, p0, Lnet/flixster/android/model/Review;->movie:Lnet/flixster/android/model/Movie;

    .line 55
    return-void
.end method
