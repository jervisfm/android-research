.class public Lnet/flixster/android/model/Season;
.super Lnet/flixster/android/model/Movie;
.source "Season.java"


# static fields
.field private static SEASON_HASH_BASE:J


# instance fields
.field private final episodesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lnet/flixster/android/model/Episode;",
            ">;"
        }
    .end annotation
.end field

.field private network:Ljava/lang/String;

.field private numOfEpisodes:I

.field private seasonNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 109
    const-wide v0, 0x16345785d8a0000L

    sput-wide v0, Lnet/flixster/android/model/Season;->SEASON_HASH_BASE:J

    .line 16
    return-void
.end method

.method public constructor <init>(J)V
    .locals 1
    .parameter "id"

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/model/Movie;-><init>(J)V

    .line 25
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/Season;->episodesMap:Ljava/util/Map;

    .line 26
    return-void
.end method

.method public static generateIdHash(J)J
    .locals 2
    .parameter "seasonId"

    .prologue
    .line 106
    sget-wide v0, Lnet/flixster/android/model/Season;->SEASON_HASH_BASE:J

    add-long/2addr v0, p0

    return-wide v0
.end method


# virtual methods
.method public getEpisodes()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Lnet/flixster/android/model/Episode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    iget-object v0, p0, Lnet/flixster/android/model/Season;->episodesMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getNetwork()Ljava/lang/String;
    .locals 2

    .prologue
    .line 92
    const-string v0, ""

    iget-object v1, p0, Lnet/flixster/android/model/Season;->network:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/Season;->network:Ljava/lang/String;

    goto :goto_0
.end method

.method public getNumOfEpisodes()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lnet/flixster/android/model/Season;->numOfEpisodes:I

    return v0
.end method

.method public getSeasonNum()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lnet/flixster/android/model/Season;->seasonNum:I

    return v0
.end method

.method public isUserSeasonParsed()Z
    .locals 3

    .prologue
    .line 100
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v0

    .line 101
    .local v0, user:Lnet/flixster/android/model/User;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lnet/flixster/android/model/Season;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lnet/flixster/android/model/User;->hasEpisodeRightsForSeason(J)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public mergeEpisodes(Lorg/json/JSONArray;)Ljava/util/Collection;
    .locals 12
    .parameter "array"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/json/JSONArray;",
            ")",
            "Ljava/util/Collection",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-static {}, Lcom/flixster/android/data/AccountManager;->instance()Lcom/flixster/android/data/AccountManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/flixster/android/data/AccountManager;->getUser()Lnet/flixster/android/model/User;

    move-result-object v4

    iget-wide v10, p0, Lnet/flixster/android/model/Season;->id:J

    invoke-virtual {v4, v10, v11}, Lnet/flixster/android/model/User;->getLockerRightFromAssetId(J)Lnet/flixster/android/model/LockerRight;

    move-result-object v9

    .line 61
    .local v9, seasonRight:Lnet/flixster/android/model/LockerRight;
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 62
    .local v8, rights:Ljava/util/Collection;,"Ljava/util/Collection<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v6, 0x0

    .local v6, i:I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lt v6, v4, :cond_0

    .line 78
    return-object v8

    .line 63
    :cond_0
    invoke-virtual {p1, v6}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 64
    .local v1, json:Lorg/json/JSONObject;
    if-eqz v1, :cond_1

    .line 65
    const-string v4, "episode"

    invoke-virtual {v1, v4}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 66
    .local v7, jsonEpisode:Lorg/json/JSONObject;
    if-eqz v7, :cond_1

    .line 67
    const-string v4, "id"

    invoke-virtual {v7, v4}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 68
    .local v2, epId:J
    iget-object v4, p0, Lnet/flixster/android/model/Season;->episodesMap:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-interface {v4, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Episode;

    .line 69
    .local v5, ep:Lnet/flixster/android/model/Episode;
    if-eqz v5, :cond_1

    .line 70
    invoke-virtual {v5, v7}, Lnet/flixster/android/model/Episode;->merge(Lorg/json/JSONObject;)V

    .line 71
    new-instance v0, Lnet/flixster/android/model/LockerRight;

    sget-object v4, Lnet/flixster/android/model/LockerRight$RightType;->EPISODE:Lnet/flixster/android/model/LockerRight$RightType;

    invoke-direct/range {v0 .. v5}, Lnet/flixster/android/model/LockerRight;-><init>(Lorg/json/JSONObject;JLnet/flixster/android/model/LockerRight$RightType;Lnet/flixster/android/model/VideoAsset;)V

    .line 72
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    invoke-interface {v8, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 73
    iget-wide v10, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    invoke-virtual {v9, v10, v11, v2, v3}, Lnet/flixster/android/model/LockerRight;->addChildRightId(JJ)V

    .line 62
    .end local v0           #right:Lnet/flixster/android/model/LockerRight;
    .end local v2           #epId:J
    .end local v5           #ep:Lnet/flixster/android/model/Episode;
    .end local v7           #jsonEpisode:Lorg/json/JSONObject;
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method

.method public bridge synthetic parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lnet/flixster/android/model/Season;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Season;

    move-result-object v0

    return-object v0
.end method

.method public parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Season;
    .locals 9
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 30
    invoke-super {p0, p1}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 32
    const-string v6, "season"

    iget v7, p0, Lnet/flixster/android/model/Season;->seasonNum:I

    invoke-virtual {p1, v6, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/model/Season;->seasonNum:I

    .line 33
    const-string v6, "numEpisodes"

    iget v7, p0, Lnet/flixster/android/model/Season;->numOfEpisodes:I

    invoke-virtual {p1, v6, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/model/Season;->numOfEpisodes:I

    .line 34
    const-string v6, "network"

    iget-object v7, p0, Lnet/flixster/android/model/Season;->network:Ljava/lang/String;

    invoke-virtual {p1, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/Season;->network:Ljava/lang/String;

    .line 36
    const-string v6, "episodes"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 37
    .local v0, array:Lorg/json/JSONArray;
    if-eqz v0, :cond_0

    .line 38
    const/4 v2, 0x0

    .local v2, i:I
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v6

    if-lt v2, v6, :cond_1

    .line 51
    .end local v2           #i:I
    :cond_0
    return-object p0

    .line 39
    .restart local v2       #i:I
    :cond_1
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 40
    .local v5, obj:Lorg/json/JSONObject;
    if-eqz v5, :cond_2

    .line 41
    const-string v6, "id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v3

    .line 42
    .local v3, id:J
    iget-object v6, p0, Lnet/flixster/android/model/Season;->episodesMap:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/Episode;

    .line 43
    .local v1, ep:Lnet/flixster/android/model/Episode;
    if-nez v1, :cond_3

    .line 44
    iget-object v6, p0, Lnet/flixster/android/model/Season;->episodesMap:Ljava/util/Map;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    new-instance v8, Lnet/flixster/android/model/Episode;

    invoke-direct {v8, v5}, Lnet/flixster/android/model/Episode;-><init>(Lorg/json/JSONObject;)V

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    .end local v1           #ep:Lnet/flixster/android/model/Episode;
    .end local v3           #id:J
    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 46
    .restart local v1       #ep:Lnet/flixster/android/model/Episode;
    .restart local v3       #id:J
    :cond_3
    invoke-virtual {v1, v5}, Lnet/flixster/android/model/Episode;->merge(Lorg/json/JSONObject;)V

    goto :goto_1
.end method
