.class public Lnet/flixster/android/model/Showtimes;
.super Ljava/lang/Object;
.source "Showtimes.java"


# static fields
.field public static final NAME:Ljava/lang/String; = "SHOWTIMES_NAME"

.field public static final TIMES:Ljava/lang/String; = "SHOWTIMES_TIMES"


# instance fields
.field public mListings:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Listing;",
            ">;"
        }
    .end annotation
.end field

.field public mMovieId:J

.field public mShowtimesTitle:Ljava/lang/String;

.field public mTheaterId:J

.field private mTimesString:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    .line 25
    return-void
.end method


# virtual methods
.method public getTimesString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 29
    iget-object v5, p0, Lnet/flixster/android/model/Showtimes;->mTimesString:Ljava/lang/String;

    if-nez v5, :cond_0

    .line 31
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 32
    .local v4, timesString:Ljava/lang/StringBuilder;
    iget-object v5, p0, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 33
    .local v1, length:I
    const/4 v0, 0x0

    .local v0, i:I
    :goto_0
    if-lt v0, v1, :cond_1

    .line 41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, " PM"

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lnet/flixster/android/model/Showtimes;->mTimesString:Ljava/lang/String;

    .line 43
    .end local v0           #i:I
    .end local v1           #length:I
    .end local v4           #timesString:Ljava/lang/StringBuilder;
    :cond_0
    iget-object v5, p0, Lnet/flixster/android/model/Showtimes;->mTimesString:Ljava/lang/String;

    return-object v5

    .line 34
    .restart local v0       #i:I
    .restart local v1       #length:I
    .restart local v4       #timesString:Ljava/lang/StringBuilder;
    :cond_1
    iget-object v5, p0, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lnet/flixster/android/model/Listing;

    iget-object v3, v5, Lnet/flixster/android/model/Listing;->displayTime:Ljava/lang/String;

    .line 35
    .local v3, originalTime:Ljava/lang/String;
    invoke-static {v3}, Lcom/flixster/android/utils/DateTimeHelper;->shortTimeFormat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 36
    .local v2, newTime:Ljava/lang/String;
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 37
    add-int/lit8 v5, v1, -0x1

    if-ge v0, v5, :cond_2

    .line 38
    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 33
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
