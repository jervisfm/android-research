.class public Lnet/flixster/android/model/ShowtimesTitleComparator;
.super Ljava/lang/Object;
.source "ShowtimesTitleComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lnet/flixster/android/model/Showtimes;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lnet/flixster/android/model/Showtimes;

    check-cast p2, Lnet/flixster/android/model/Showtimes;

    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/ShowtimesTitleComparator;->compare(Lnet/flixster/android/model/Showtimes;Lnet/flixster/android/model/Showtimes;)I

    move-result v0

    return v0
.end method

.method public compare(Lnet/flixster/android/model/Showtimes;Lnet/flixster/android/model/Showtimes;)I
    .locals 2
    .parameter "showtimes1"
    .parameter "showtimes2"

    .prologue
    .line 5
    iget-object v0, p1, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    iget-object v1, p2, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
