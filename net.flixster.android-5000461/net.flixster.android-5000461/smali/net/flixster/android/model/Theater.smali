.class public Lnet/flixster/android/model/Theater;
.super Ljava/lang/Object;
.source "Theater.java"


# static fields
.field public static final THEATER_ADDRESS:Ljava/lang/String; = "address"

.field public static final THEATER_CITY:Ljava/lang/String; = "city"

.field public static final THEATER_ID:Ljava/lang/String; = "id"

.field public static final THEATER_MAP:Ljava/lang/String; = "map"

.field public static final THEATER_NAME:Ljava/lang/String; = "name"

.field public static final THEATER_PHONE:Ljava/lang/String; = "phone"

.field public static final THEATER_STATE:Ljava/lang/String; = "state"

.field public static final THEATER_STREET:Ljava/lang/String; = "street"

.field public static final THEATER_TICKET:Ljava/lang/String; = "ticket"

.field public static final THEATER_TICKETS:Ljava/lang/String; = "tickets"

.field public static final THEATER_ZIP:Ljava/lang/String; = "zip"


# instance fields
.field private id:J

.field public latitude:D

.field public longitude:D

.field private mHasTickets:Z

.field private mMiles:Ljava/lang/Double;

.field private mPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mShowtimesListByMovieHash:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Showtimes;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(J)V
    .locals 1
    .parameter "id"

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lnet/flixster/android/model/Theater;->mHasTickets:Z

    .line 36
    iput-wide p1, p0, Lnet/flixster/android/model/Theater;->id:J

    .line 37
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    .line 38
    return-void
.end method


# virtual methods
.method public checkProperty(Ljava/lang/String;)Z
    .locals 1
    .parameter "propkey"

    .prologue
    .line 61
    iget-object v0, p0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lnet/flixster/android/model/Theater;->id:J

    return-wide v0
.end method

.method public getMiles()D
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lnet/flixster/android/model/Theater;->mMiles:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "propkey"

    .prologue
    .line 49
    iget-object v0, p0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 52
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getShowtimesListByMovieHash()Ljava/util/HashMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/ArrayList",
            "<",
            "Lnet/flixster/android/model/Showtimes;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lnet/flixster/android/model/Theater;->mShowtimesListByMovieHash:Ljava/util/HashMap;

    return-object v0
.end method

.method public hasTickets()Z
    .locals 1

    .prologue
    .line 77
    iget-boolean v0, p0, Lnet/flixster/android/model/Theater;->mHasTickets:Z

    return v0
.end method

.method public invalidateShowtimesHash()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/model/Theater;->mShowtimesListByMovieHash:Ljava/util/HashMap;

    .line 70
    return-void
.end method

.method public parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Theater;
    .locals 28
    .parameter "jsonTheater"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 81
    const-string v24, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 83
    const-string v24, "id"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    .line 84
    .local v18, tempId:Ljava/lang/Long;
    if-eqz v18, :cond_0

    .line 85
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Long;->longValue()J

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lnet/flixster/android/model/Theater;->id:J

    .line 89
    .end local v18           #tempId:Ljava/lang/Long;
    :cond_0
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v24, 0x0

    const-string v25, "name"

    aput-object v25, v20, v24

    const/16 v24, 0x1

    const-string v25, "map"

    aput-object v25, v20, v24

    const/16 v24, 0x2

    const-string v25, "phone"

    aput-object v25, v20, v24

    const/16 v24, 0x3

    const-string v25, "id"

    aput-object v25, v20, v24

    .line 90
    .local v20, theaterProps:[Ljava/lang/String;
    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v25, v0

    const/16 v24, 0x0

    :goto_0
    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_6

    .line 97
    const-string v24, "tickets"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 100
    const-string v24, "tickets"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v24

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lnet/flixster/android/model/Theater;->mHasTickets:Z

    .line 102
    :cond_1
    const-string v24, "address"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 103
    const-string v24, "address"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v17

    .line 105
    .local v17, tempAddress:Lorg/json/JSONObject;
    const/16 v24, 0x4

    move/from16 v0, v24

    new-array v4, v0, [Ljava/lang/String;

    const/16 v24, 0x0

    const-string v25, "street"

    aput-object v25, v4, v24

    const/16 v24, 0x1

    const-string v25, "city"

    aput-object v25, v4, v24

    const/16 v24, 0x2

    const-string v25, "state"

    aput-object v25, v4, v24

    const/16 v24, 0x3

    const-string v25, "zip"

    aput-object v25, v4, v24

    .line 106
    .local v4, addressProps:[Ljava/lang/String;
    array-length v0, v4

    move/from16 v25, v0

    const/16 v24, 0x0

    :goto_1
    move/from16 v0, v24

    move/from16 v1, v25

    if-lt v0, v1, :cond_8

    .line 112
    new-instance v25, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v24, v0

    const-string v26, "street"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    invoke-static/range {v24 .. v24}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v24, ", "

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v24, v0

    const-string v26, "city"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v25

    .line 113
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v24, v0

    const-string v26, "zip"

    move-object/from16 v0, v24

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    move-object/from16 v0, v25

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    .line 112
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 115
    .local v3, address:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v24, v0

    const-string v25, "address"

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    const-string v24, "distance"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 118
    const-string v24, "distance"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v24

    invoke-static/range {v24 .. v25}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v24

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Theater;->mMiles:Ljava/lang/Double;

    .line 120
    :cond_2
    const-string v24, "latitude"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 121
    const-string v24, "latitude"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lnet/flixster/android/model/Theater;->latitude:D

    .line 123
    :cond_3
    const-string v24, "longitude"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_4

    .line 124
    const-string v24, "longitude"

    move-object/from16 v0, v17

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v24

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Lnet/flixster/android/model/Theater;->longitude:D

    .line 128
    .end local v3           #address:Ljava/lang/String;
    .end local v4           #addressProps:[Ljava/lang/String;
    .end local v17           #tempAddress:Lorg/json/JSONObject;
    :cond_4
    const-string v24, "showtimes"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v24

    if-eqz v24, :cond_5

    .line 133
    new-instance v24, Ljava/util/HashMap;

    invoke-direct/range {v24 .. v24}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, v24

    move-object/from16 v1, p0

    iput-object v0, v1, Lnet/flixster/android/model/Theater;->mShowtimesListByMovieHash:Ljava/util/HashMap;

    .line 137
    const-string v24, "showtimes"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v15

    .line 139
    .local v15, showtimesJSONArray:Lorg/json/JSONArray;
    invoke-virtual {v15}, Lorg/json/JSONArray;->length()I

    move-result v16

    .line 141
    .local v16, showtimesLength:I
    const/4 v5, 0x0

    .local v5, i:I
    :goto_2
    move/from16 v0, v16

    if-lt v5, v0, :cond_a

    .line 176
    .end local v5           #i:I
    .end local v15           #showtimesJSONArray:Lorg/json/JSONArray;
    .end local v16           #showtimesLength:I
    :cond_5
    return-object p0

    .line 90
    :cond_6
    aget-object v12, v20, v24

    .line 91
    .local v12, prop:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_7

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_7

    .line 94
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    :cond_7
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_0

    .line 106
    .end local v12           #prop:Ljava/lang/String;
    .restart local v4       #addressProps:[Ljava/lang/String;
    .restart local v17       #tempAddress:Lorg/json/JSONObject;
    :cond_8
    aget-object v12, v4, v24

    .line 107
    .restart local v12       #prop:Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    invoke-virtual {v0, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v26

    if-nez v26, :cond_9

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v26

    if-eqz v26, :cond_9

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    move-object/from16 v26, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-static/range {v27 .. v27}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    invoke-virtual {v0, v12, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    :cond_9
    add-int/lit8 v24, v24, 0x1

    goto/16 :goto_1

    .line 144
    .end local v4           #addressProps:[Ljava/lang/String;
    .end local v12           #prop:Ljava/lang/String;
    .end local v17           #tempAddress:Lorg/json/JSONObject;
    .restart local v5       #i:I
    .restart local v15       #showtimesJSONArray:Lorg/json/JSONArray;
    .restart local v16       #showtimesLength:I
    :cond_a
    invoke-virtual {v15, v5}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v13

    .line 145
    .local v13, showtimeObj:Lorg/json/JSONObject;
    const-string v24, "movie"

    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v7

    .line 147
    .local v7, jsonMovieObj:Lorg/json/JSONObject;
    const-string v24, "id"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 148
    .local v10, movieId:J
    new-instance v14, Lnet/flixster/android/model/Showtimes;

    invoke-direct {v14}, Lnet/flixster/android/model/Showtimes;-><init>()V

    .line 149
    .local v14, showtimes:Lnet/flixster/android/model/Showtimes;
    move-object/from16 v0, p0

    iget-wide v0, v0, Lnet/flixster/android/model/Theater;->id:J

    move-wide/from16 v24, v0

    move-wide/from16 v0, v24

    iput-wide v0, v14, Lnet/flixster/android/model/Showtimes;->mTheaterId:J

    .line 150
    iput-wide v10, v14, Lnet/flixster/android/model/Showtimes;->mMovieId:J

    .line 152
    invoke-static {v10, v11}, Lnet/flixster/android/data/MovieDao;->getMovie(J)Lnet/flixster/android/model/Movie;

    move-result-object v9

    .line 153
    .local v9, movie:Lnet/flixster/android/model/Movie;
    invoke-virtual {v9, v7}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 155
    const-string v24, "title"

    move-object/from16 v0, v24

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    iput-object v0, v14, Lnet/flixster/android/model/Showtimes;->mShowtimesTitle:Ljava/lang/String;

    .line 159
    const-string v24, "times"

    move-object/from16 v0, v24

    invoke-virtual {v13, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v22

    .line 160
    .local v22, timesArray:Lorg/json/JSONArray;
    invoke-virtual/range {v22 .. v22}, Lorg/json/JSONArray;->length()I

    move-result v23

    .line 161
    .local v23, times_length:I
    const/4 v6, 0x0

    .local v6, j:I
    :goto_3
    move/from16 v0, v23

    if-lt v6, v0, :cond_b

    .line 167
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mShowtimesListByMovieHash:Ljava/util/HashMap;

    move-object/from16 v24, v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_c

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mShowtimesListByMovieHash:Ljava/util/HashMap;

    move-object/from16 v24, v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Ljava/util/ArrayList;

    .line 173
    .local v19, tempShowtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    :goto_4
    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_2

    .line 162
    .end local v19           #tempShowtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    :cond_b
    move-object/from16 v0, v22

    invoke-virtual {v0, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v21

    .line 163
    .local v21, timeObj:Lorg/json/JSONObject;
    new-instance v8, Lnet/flixster/android/model/Listing;

    move-object/from16 v0, v21

    invoke-direct {v8, v0}, Lnet/flixster/android/model/Listing;-><init>(Lorg/json/JSONObject;)V

    .line 164
    .local v8, listing:Lnet/flixster/android/model/Listing;
    iget-object v0, v14, Lnet/flixster/android/model/Showtimes;->mListings:Ljava/util/ArrayList;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 170
    .end local v8           #listing:Lnet/flixster/android/model/Listing;
    .end local v21           #timeObj:Lorg/json/JSONObject;
    :cond_c
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 171
    .restart local v19       #tempShowtimesList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lnet/flixster/android/model/Showtimes;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lnet/flixster/android/model/Theater;->mShowtimesListByMovieHash:Ljava/util/HashMap;

    move-object/from16 v24, v0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v25

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4
.end method

.method public setId(J)V
    .locals 0
    .parameter "id"

    .prologue
    .line 45
    iput-wide p1, p0, Lnet/flixster/android/model/Theater;->id:J

    .line 46
    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .parameter "propkey"
    .parameter "value"

    .prologue
    .line 57
    iget-object v0, p0, Lnet/flixster/android/model/Theater;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method
