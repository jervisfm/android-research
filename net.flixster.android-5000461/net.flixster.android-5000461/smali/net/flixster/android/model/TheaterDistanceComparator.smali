.class public Lnet/flixster/android/model/TheaterDistanceComparator;
.super Ljava/lang/Object;
.source "TheaterDistanceComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lnet/flixster/android/model/Theater;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lnet/flixster/android/model/Theater;

    check-cast p2, Lnet/flixster/android/model/Theater;

    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/TheaterDistanceComparator;->compare(Lnet/flixster/android/model/Theater;Lnet/flixster/android/model/Theater;)I

    move-result v0

    return v0
.end method

.method public compare(Lnet/flixster/android/model/Theater;Lnet/flixster/android/model/Theater;)I
    .locals 4
    .parameter "theater1"
    .parameter "theater2"

    .prologue
    .line 6
    invoke-virtual {p2}, Lnet/flixster/android/model/Theater;->getMiles()D

    move-result-wide v0

    invoke-virtual {p1}, Lnet/flixster/android/model/Theater;->getMiles()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 7
    const/4 v0, -0x1

    .line 12
    :goto_0
    return v0

    .line 9
    :cond_0
    invoke-virtual {p2}, Lnet/flixster/android/model/Theater;->getMiles()D

    move-result-wide v0

    invoke-virtual {p1}, Lnet/flixster/android/model/Theater;->getMiles()D

    move-result-wide v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_1

    .line 10
    const/4 v0, 0x1

    goto :goto_0

    .line 12
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
