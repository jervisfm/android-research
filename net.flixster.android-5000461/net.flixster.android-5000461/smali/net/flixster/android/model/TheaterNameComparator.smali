.class public Lnet/flixster/android/model/TheaterNameComparator;
.super Ljava/lang/Object;
.source "TheaterNameComparator.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lnet/flixster/android/model/Theater;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 1
    check-cast p1, Lnet/flixster/android/model/Theater;

    check-cast p2, Lnet/flixster/android/model/Theater;

    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/TheaterNameComparator;->compare(Lnet/flixster/android/model/Theater;Lnet/flixster/android/model/Theater;)I

    move-result v0

    return v0
.end method

.method public compare(Lnet/flixster/android/model/Theater;Lnet/flixster/android/model/Theater;)I
    .locals 2
    .parameter "theater1"
    .parameter "theater2"

    .prologue
    .line 5
    const-string v0, "name"

    invoke-virtual {p1, v0}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {p2, v1}, Lnet/flixster/android/model/Theater;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
