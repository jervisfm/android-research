.class Lnet/flixster/android/model/TouchInterceptor$1;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "TouchInterceptor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/model/TouchInterceptor;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/model/TouchInterceptor;


# direct methods
.method constructor <init>(Lnet/flixster/android/model/TouchInterceptor;)V
    .locals 0
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/model/TouchInterceptor$1;->this$0:Lnet/flixster/android/model/TouchInterceptor;

    .line 86
    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4
    .parameter "e1"
    .parameter "e2"
    .parameter "velocityX"
    .parameter "velocityY"

    .prologue
    const/4 v1, 0x1

    .line 89
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor$1;->this$0:Lnet/flixster/android/model/TouchInterceptor;

    #getter for: Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;
    invoke-static {v2}, Lnet/flixster/android/model/TouchInterceptor;->access$0(Lnet/flixster/android/model/TouchInterceptor;)Landroid/widget/ImageView;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 90
    const/high16 v2, 0x447a

    cmpl-float v2, p3, v2

    if-lez v2, :cond_0

    .line 91
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor$1;->this$0:Lnet/flixster/android/model/TouchInterceptor;

    #getter for: Lnet/flixster/android/model/TouchInterceptor;->mTempRect:Landroid/graphics/Rect;
    invoke-static {v2}, Lnet/flixster/android/model/TouchInterceptor;->access$1(Lnet/flixster/android/model/TouchInterceptor;)Landroid/graphics/Rect;

    move-result-object v0

    .line 92
    .local v0, r:Landroid/graphics/Rect;
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor$1;->this$0:Lnet/flixster/android/model/TouchInterceptor;

    #getter for: Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;
    invoke-static {v2}, Lnet/flixster/android/model/TouchInterceptor;->access$0(Lnet/flixster/android/model/TouchInterceptor;)Landroid/widget/ImageView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 93
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v3, v3, 0x2

    div-int/lit8 v3, v3, 0x3

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 96
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor$1;->this$0:Lnet/flixster/android/model/TouchInterceptor;

    #calls: Lnet/flixster/android/model/TouchInterceptor;->stopDragging()V
    invoke-static {v2}, Lnet/flixster/android/model/TouchInterceptor;->access$2(Lnet/flixster/android/model/TouchInterceptor;)V

    .line 97
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor$1;->this$0:Lnet/flixster/android/model/TouchInterceptor;

    #getter for: Lnet/flixster/android/model/TouchInterceptor;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;
    invoke-static {v2}, Lnet/flixster/android/model/TouchInterceptor;->access$3(Lnet/flixster/android/model/TouchInterceptor;)Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    move-result-object v2

    iget-object v3, p0, Lnet/flixster/android/model/TouchInterceptor$1;->this$0:Lnet/flixster/android/model/TouchInterceptor;

    #getter for: Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I
    invoke-static {v3}, Lnet/flixster/android/model/TouchInterceptor;->access$4(Lnet/flixster/android/model/TouchInterceptor;)I

    move-result v3

    invoke-interface {v2, v3}, Lnet/flixster/android/model/TouchInterceptor$RemoveListener;->remove(I)V

    .line 98
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor$1;->this$0:Lnet/flixster/android/model/TouchInterceptor;

    #calls: Lnet/flixster/android/model/TouchInterceptor;->unExpandViews(Z)V
    invoke-static {v2, v1}, Lnet/flixster/android/model/TouchInterceptor;->access$5(Lnet/flixster/android/model/TouchInterceptor;Z)V

    .line 104
    .end local v0           #r:Landroid/graphics/Rect;
    :cond_0
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
