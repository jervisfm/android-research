.class public Lnet/flixster/android/model/TouchInterceptor;
.super Landroid/widget/ListView;
.source "TouchInterceptor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/model/TouchInterceptor$DragListener;,
        Lnet/flixster/android/model/TouchInterceptor$DropListener;,
        Lnet/flixster/android/model/TouchInterceptor$RemoveListener;
    }
.end annotation


# static fields
.field private static final FLING:I = 0x0

.field private static final SLIDE:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCoordOffset:I

.field private mDragBitmap:Landroid/graphics/Bitmap;

.field private mDragListener:Lnet/flixster/android/model/TouchInterceptor$DragListener;

.field private mDragPoint:I

.field private mDragPos:I

.field private mDragView:Landroid/widget/ImageView;

.field private mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

.field private mFirstDragPos:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHeight:I

.field private mItemHeightExpanded:I

.field private mItemHeightNormal:I

.field private mLowerBound:I

.field private mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

.field private mRemoveMode:I

.field private mTempRect:Landroid/graphics/Rect;

.field private final mTouchSlop:I

.field private mUpperBound:I

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWindowParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .parameter "context"
    .parameter "attrs"

    .prologue
    const/4 v4, -0x1

    .line 69
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    iput v4, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveMode:I

    .line 62
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mTempRect:Landroid/graphics/Rect;

    .line 70
    iput-object p1, p0, Lnet/flixster/android/model/TouchInterceptor;->mContext:Landroid/content/Context;

    .line 71
    const-string v2, "Music"

    const/4 v3, 0x3

    invoke-virtual {p1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 72
    .local v0, pref:Landroid/content/SharedPreferences;
    const-string v2, "deletemode"

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveMode:I

    .line 73
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v2

    iput v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mTouchSlop:I

    .line 74
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 76
    .local v1, res:Landroid/content/res/Resources;
    const v2, 0x7f0a0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mItemHeightNormal:I

    .line 77
    iget v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mItemHeightNormal:I

    mul-int/lit8 v2, v2, 0x2

    iput v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mItemHeightExpanded:I

    .line 78
    return-void
.end method

.method static synthetic access$0(Lnet/flixster/android/model/TouchInterceptor;)Landroid/widget/ImageView;
    .locals 1
    .parameter

    .prologue
    .line 43
    iget-object v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$1(Lnet/flixster/android/model/TouchInterceptor;)Landroid/graphics/Rect;
    .locals 1
    .parameter

    .prologue
    .line 62
    iget-object v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mTempRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic access$2(Lnet/flixster/android/model/TouchInterceptor;)V
    .locals 0
    .parameter

    .prologue
    .line 399
    invoke-direct {p0}, Lnet/flixster/android/model/TouchInterceptor;->stopDragging()V

    return-void
.end method

.method static synthetic access$3(Lnet/flixster/android/model/TouchInterceptor;)Lnet/flixster/android/model/TouchInterceptor$RemoveListener;
    .locals 1
    .parameter

    .prologue
    .line 54
    iget-object v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    return-object v0
.end method

.method static synthetic access$4(Lnet/flixster/android/model/TouchInterceptor;)I
    .locals 1
    .parameter

    .prologue
    .line 47
    iget v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I

    return v0
.end method

.method static synthetic access$5(Lnet/flixster/android/model/TouchInterceptor;Z)V
    .locals 0
    .parameter
    .parameter

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lnet/flixster/android/model/TouchInterceptor;->unExpandViews(Z)V

    return-void
.end method

.method private adjustScrollBounds(I)V
    .locals 1
    .parameter "y"

    .prologue
    .line 201
    iget v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    div-int/lit8 v0, v0, 0x3

    if-lt p1, v0, :cond_0

    .line 202
    iget v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mUpperBound:I

    .line 204
    :cond_0
    iget v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    if-gt p1, v0, :cond_1

    .line 205
    iget v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lnet/flixster/android/model/TouchInterceptor;->mLowerBound:I

    .line 207
    :cond_1
    return-void
.end method

.method private doExpansion()V
    .locals 9

    .prologue
    .line 245
    iget v7, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getFirstVisiblePosition()I

    move-result v8

    sub-int v0, v7, v8

    .line 246
    .local v0, childnum:I
    iget v7, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    iget v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I

    if-le v7, v8, :cond_0

    .line 247
    add-int/lit8 v0, v0, 0x1

    .line 250
    :cond_0
    iget v7, p0, Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I

    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getFirstVisiblePosition()I

    move-result v8

    sub-int/2addr v7, v8

    invoke-virtual {p0, v7}, Lnet/flixster/android/model/TouchInterceptor;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 252
    .local v1, first:Landroid/view/View;
    const/4 v3, 0x0

    .line 253
    .local v3, i:I
    :goto_0
    invoke-virtual {p0, v3}, Lnet/flixster/android/model/TouchInterceptor;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 254
    .local v6, vv:Landroid/view/View;
    if-nez v6, :cond_1

    .line 278
    return-void

    .line 257
    :cond_1
    iget v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mItemHeightNormal:I

    .line 258
    .local v2, height:I
    const/4 v5, 0x0

    .line 259
    .local v5, visibility:I
    invoke-virtual {v6, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 261
    iget v7, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    iget v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I

    if-ne v7, v8, :cond_3

    .line 263
    const/4 v5, 0x4

    .line 273
    :cond_2
    :goto_1
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 274
    .local v4, params:Landroid/view/ViewGroup$LayoutParams;
    iput v2, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 275
    invoke-virtual {v6, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 276
    invoke-virtual {v6, v5}, Landroid/view/View;->setVisibility(I)V

    .line 252
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 266
    .end local v4           #params:Landroid/view/ViewGroup$LayoutParams;
    :cond_3
    const/4 v2, 0x1

    goto :goto_1

    .line 268
    :cond_4
    if-ne v3, v0, :cond_2

    .line 269
    iget v7, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v7, v8, :cond_2

    .line 270
    iget v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mItemHeightExpanded:I

    goto :goto_1
.end method

.method private dragView(II)V
    .locals 5
    .parameter "x"
    .parameter "y"

    .prologue
    .line 387
    iget v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 388
    const/high16 v0, 0x3f80

    .line 389
    .local v0, alpha:F
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v1

    .line 390
    .local v1, width:I
    div-int/lit8 v2, v1, 0x2

    if-le p1, v2, :cond_0

    .line 391
    sub-int v2, v1, p1

    int-to-float v2, v2

    div-int/lit8 v3, v1, 0x2

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 393
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v0, v2, Landroid/view/WindowManager$LayoutParams;->alpha:F

    .line 395
    .end local v0           #alpha:F
    .end local v1           #width:I
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPoint:I

    sub-int v3, p2, v3

    iget v4, p0, Lnet/flixster/android/model/TouchInterceptor;->mCoordOffset:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 396
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    iget-object v4, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 397
    return-void
.end method

.method private getItemForPosition(I)I
    .locals 3
    .parameter "y"

    .prologue
    .line 188
    iget v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPoint:I

    sub-int v2, p1, v2

    add-int/lit8 v0, v2, -0x20

    .line 189
    .local v0, adjustedy:I
    const/4 v2, 0x0

    invoke-direct {p0, v2, v0}, Lnet/flixster/android/model/TouchInterceptor;->myPointToPosition(II)I

    move-result v1

    .line 190
    .local v1, pos:I
    if-ltz v1, :cond_1

    .line 191
    iget v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I

    if-gt v1, v2, :cond_0

    .line 192
    add-int/lit8 v1, v1, 0x1

    .line 197
    :cond_0
    :goto_0
    return v1

    .line 194
    :cond_1
    if-gez v0, :cond_0

    .line 195
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private myPointToPosition(II)I
    .locals 5
    .parameter "x"
    .parameter "y"

    .prologue
    .line 174
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mTempRect:Landroid/graphics/Rect;

    .line 175
    .local v2, frame:Landroid/graphics/Rect;
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getChildCount()I

    move-result v1

    .line 176
    .local v1, count:I
    add-int/lit8 v3, v1, -0x1

    .local v3, i:I
    :goto_0
    if-gez v3, :cond_0

    .line 183
    const/4 v4, -0x1

    :goto_1
    return v4

    .line 177
    :cond_0
    invoke-virtual {p0, v3}, Lnet/flixster/android/model/TouchInterceptor;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 178
    .local v0, child:Landroid/view/View;
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 179
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 180
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getFirstVisiblePosition()I

    move-result v4

    add-int/2addr v4, v3

    goto :goto_1

    .line 176
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0
.end method

.method private startDragging(Landroid/graphics/Bitmap;I)V
    .locals 7
    .parameter "bm"
    .parameter "y"

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x2

    .line 352
    const-string v2, "FlxMain"

    const-string v3, "TouchInterceptor.startDragging(.)"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-direct {p0}, Lnet/flixster/android/model/TouchInterceptor;->stopDragging()V

    .line 355
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    .line 356
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x33

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 357
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 358
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPoint:I

    sub-int v3, p2, v3

    iget v4, p0, Lnet/flixster/android/model/TouchInterceptor;->mCoordOffset:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 360
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 362
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 365
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 366
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x198

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 369
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v3, -0x3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 370
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 372
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 376
    .local v1, v:Landroid/widget/ImageView;
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f09000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 377
    .local v0, backGroundColor:I
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 378
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 379
    iput-object p1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 381
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mContext:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowManager:Landroid/view/WindowManager;

    .line 382
    iget-object v2, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lnet/flixster/android/model/TouchInterceptor;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v1, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 383
    iput-object v1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    .line 384
    return-void
.end method

.method private stopDragging()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 400
    iget-object v1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 401
    iget-object v1, p0, Lnet/flixster/android/model/TouchInterceptor;->mContext:Landroid/content/Context;

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 402
    .local v0, wm:Landroid/view/WindowManager;
    iget-object v1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 403
    iget-object v1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 404
    iput-object v3, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    .line 406
    .end local v0           #wm:Landroid/view/WindowManager;
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 407
    iget-object v1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 408
    iput-object v3, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 410
    :cond_1
    return-void
.end method

.method private unExpandViews(Z)V
    .locals 7
    .parameter "deletion"

    .prologue
    const/4 v6, 0x0

    .line 213
    const/4 v0, 0x0

    .line 214
    .local v0, i:I
    :goto_0
    invoke-virtual {p0, v0}, Lnet/flixster/android/model/TouchInterceptor;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 215
    .local v3, v:Landroid/view/View;
    if-nez v3, :cond_1

    .line 216
    if-eqz p1, :cond_0

    .line 218
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getFirstVisiblePosition()I

    move-result v2

    .line 219
    .local v2, position:I
    invoke-virtual {p0, v6}, Lnet/flixster/android/model/TouchInterceptor;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v4

    .line 220
    .local v4, y:I
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-virtual {p0, v5}, Lnet/flixster/android/model/TouchInterceptor;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 221
    invoke-virtual {p0, v2, v4}, Lnet/flixster/android/model/TouchInterceptor;->setSelectionFromTop(II)V

    .line 224
    .end local v2           #position:I
    .end local v4           #y:I
    :cond_0
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->layoutChildren()V

    .line 225
    invoke-virtual {p0, v0}, Lnet/flixster/android/model/TouchInterceptor;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 226
    if-nez v3, :cond_1

    .line 235
    return-void

    .line 230
    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 231
    .local v1, params:Landroid/view/ViewGroup$LayoutParams;
    iget v5, p0, Lnet/flixster/android/model/TouchInterceptor;->mItemHeightNormal:I

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 232
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 233
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .parameter "ev"

    .prologue
    .line 84
    iget-object v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    if-eqz v8, :cond_0

    iget-object v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mGestureDetector:Landroid/view/GestureDetector;

    if-nez v8, :cond_0

    .line 85
    iget v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveMode:I

    if-nez v8, :cond_0

    .line 86
    new-instance v8, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getContext()Landroid/content/Context;

    move-result-object v9

    new-instance v10, Lnet/flixster/android/model/TouchInterceptor$1;

    invoke-direct {v10, p0}, Lnet/flixster/android/model/TouchInterceptor$1;-><init>(Lnet/flixster/android/model/TouchInterceptor;)V

    invoke-direct {v8, v9, v10}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mGestureDetector:Landroid/view/GestureDetector;

    .line 109
    :cond_0
    iget-object v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragListener:Lnet/flixster/android/model/TouchInterceptor$DragListener;

    if-nez v8, :cond_1

    iget-object v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    if-eqz v8, :cond_2

    .line 110
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 166
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/ListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    :goto_1
    return v8

    .line 112
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v6, v8

    .line 113
    .local v6, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v7, v8

    .line 114
    .local v7, y:I
    invoke-virtual {p0, v6, v7}, Lnet/flixster/android/model/TouchInterceptor;->pointToPosition(II)I

    move-result v3

    .line 115
    .local v3, itemnum:I
    const/4 v8, -0x1

    if-eq v3, v8, :cond_2

    .line 129
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getFirstVisiblePosition()I

    move-result v8

    sub-int v8, v3, v8

    invoke-virtual {p0, v8}, Lnet/flixster/android/model/TouchInterceptor;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 130
    .local v2, item:Landroid/view/ViewGroup;
    const v8, 0x7f0701fd

    invoke-virtual {v2, v8}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 132
    .local v1, dragger:Landroid/view/View;
    if-eqz v1, :cond_3

    .line 134
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTop()I

    move-result v8

    sub-int v8, v7, v8

    iput v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPoint:I

    .line 135
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v8

    float-to-int v8, v8

    sub-int/2addr v8, v7

    iput v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mCoordOffset:I

    .line 137
    iget-object v4, p0, Lnet/flixster/android/model/TouchInterceptor;->mTempRect:Landroid/graphics/Rect;

    .line 138
    .local v4, r:Landroid/graphics/Rect;
    invoke-virtual {v1, v4}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 145
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v8

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int/2addr v8, v9

    if-le v6, v8, :cond_3

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_3

    .line 148
    const/4 v8, 0x1

    invoke-virtual {v2, v8}, Landroid/view/ViewGroup;->setDrawingCacheEnabled(Z)V

    .line 151
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-static {v8}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 152
    .local v0, bitmap:Landroid/graphics/Bitmap;
    invoke-direct {p0, v0, v7}, Lnet/flixster/android/model/TouchInterceptor;->startDragging(Landroid/graphics/Bitmap;I)V

    .line 153
    iput v3, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    .line 154
    iget v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    iput v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I

    .line 155
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getHeight()I

    move-result v8

    iput v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    .line 156
    iget v5, p0, Lnet/flixster/android/model/TouchInterceptor;->mTouchSlop:I

    .line 157
    .local v5, touchSlop:I
    sub-int v8, v7, v5

    iget v9, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    div-int/lit8 v9, v9, 0x3

    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    iput v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mUpperBound:I

    .line 158
    add-int v8, v7, v5

    iget v9, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    mul-int/lit8 v9, v9, 0x2

    div-int/lit8 v9, v9, 0x3

    invoke-static {v8, v9}, Ljava/lang/Math;->max(II)I

    move-result v8

    iput v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mLowerBound:I

    .line 159
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 162
    .end local v0           #bitmap:Landroid/graphics/Bitmap;
    .end local v4           #r:Landroid/graphics/Rect;
    .end local v5           #touchSlop:I
    :cond_3
    const/4 v8, 0x0

    iput-object v8, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    goto/16 :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14
    .parameter "ev"

    .prologue
    const/4 v9, 0x1

    const/4 v13, 0x0

    .line 283
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v10, :cond_0

    .line 284
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v10, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 286
    :cond_0
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragListener:Lnet/flixster/android/model/TouchInterceptor$DragListener;

    if-nez v10, :cond_1

    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    if-eqz v10, :cond_e

    :cond_1
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    if-eqz v10, :cond_e

    .line 287
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 288
    .local v0, action:I
    packed-switch v0, :pswitch_data_0

    .line 348
    .end local v0           #action:I
    :cond_2
    :goto_0
    return v9

    .line 291
    .restart local v0       #action:I
    :pswitch_0
    iget-object v3, p0, Lnet/flixster/android/model/TouchInterceptor;->mTempRect:Landroid/graphics/Rect;

    .line 292
    .local v3, r:Landroid/graphics/Rect;
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v10, v3}, Landroid/widget/ImageView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 293
    invoke-direct {p0}, Lnet/flixster/android/model/TouchInterceptor;->stopDragging()V

    .line 294
    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveMode:I

    if-ne v10, v9, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    iget v11, v3, Landroid/graphics/Rect;->right:I

    mul-int/lit8 v11, v11, 0x3

    div-int/lit8 v11, v11, 0x4

    int-to-float v11, v11

    cmpl-float v10, v10, v11

    if-lez v10, :cond_4

    .line 295
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    if-eqz v10, :cond_3

    .line 296
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    iget v11, p0, Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I

    invoke-interface {v10, v11}, Lnet/flixster/android/model/TouchInterceptor$RemoveListener;->remove(I)V

    .line 298
    :cond_3
    invoke-direct {p0, v9}, Lnet/flixster/android/model/TouchInterceptor;->unExpandViews(Z)V

    goto :goto_0

    .line 300
    :cond_4
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    if-eqz v10, :cond_5

    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    if-ltz v10, :cond_5

    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getCount()I

    move-result v11

    if-ge v10, v11, :cond_5

    .line 301
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    iget v11, p0, Lnet/flixster/android/model/TouchInterceptor;->mFirstDragPos:I

    iget v12, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    invoke-interface {v10, v11, v12}, Lnet/flixster/android/model/TouchInterceptor$DropListener;->drop(II)V

    .line 303
    :cond_5
    invoke-direct {p0, v13}, Lnet/flixster/android/model/TouchInterceptor;->unExpandViews(Z)V

    goto :goto_0

    .line 309
    .end local v3           #r:Landroid/graphics/Rect;
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v10

    float-to-int v7, v10

    .line 310
    .local v7, x:I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v10

    float-to-int v8, v10

    .line 311
    .local v8, y:I
    invoke-direct {p0, v7, v8}, Lnet/flixster/android/model/TouchInterceptor;->dragView(II)V

    .line 312
    invoke-direct {p0, v8}, Lnet/flixster/android/model/TouchInterceptor;->getItemForPosition(I)I

    move-result v1

    .line 313
    .local v1, itemnum:I
    if-ltz v1, :cond_2

    .line 314
    if-eqz v0, :cond_6

    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    if-eq v1, v10, :cond_8

    .line 315
    :cond_6
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragListener:Lnet/flixster/android/model/TouchInterceptor$DragListener;

    if-eqz v10, :cond_7

    .line 316
    iget-object v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragListener:Lnet/flixster/android/model/TouchInterceptor$DragListener;

    iget v11, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    invoke-interface {v10, v11, v1}, Lnet/flixster/android/model/TouchInterceptor$DragListener;->drag(II)V

    .line 318
    :cond_7
    iput v1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragPos:I

    .line 319
    invoke-direct {p0}, Lnet/flixster/android/model/TouchInterceptor;->doExpansion()V

    .line 321
    :cond_8
    const/4 v5, 0x0

    .line 322
    .local v5, speed:I
    invoke-direct {p0, v8}, Lnet/flixster/android/model/TouchInterceptor;->adjustScrollBounds(I)V

    .line 323
    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mLowerBound:I

    if-le v8, v10, :cond_c

    .line 325
    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    iget v11, p0, Lnet/flixster/android/model/TouchInterceptor;->mLowerBound:I

    add-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x2

    if-le v8, v10, :cond_b

    const/16 v5, 0x10

    .line 330
    :cond_9
    :goto_1
    if-eqz v5, :cond_2

    .line 331
    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    div-int/lit8 v10, v10, 0x2

    invoke-virtual {p0, v13, v10}, Lnet/flixster/android/model/TouchInterceptor;->pointToPosition(II)I

    move-result v4

    .line 332
    .local v4, ref:I
    const/4 v10, -0x1

    if-ne v4, v10, :cond_a

    .line 335
    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mHeight:I

    div-int/lit8 v10, v10, 0x2

    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getDividerHeight()I

    move-result v11

    add-int/2addr v10, v11

    add-int/lit8 v10, v10, 0x40

    invoke-virtual {p0, v13, v10}, Lnet/flixster/android/model/TouchInterceptor;->pointToPosition(II)I

    move-result v4

    .line 337
    :cond_a
    invoke-virtual {p0}, Lnet/flixster/android/model/TouchInterceptor;->getFirstVisiblePosition()I

    move-result v10

    sub-int v10, v4, v10

    invoke-virtual {p0, v10}, Lnet/flixster/android/model/TouchInterceptor;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 338
    .local v6, v:Landroid/view/View;
    if-eqz v6, :cond_2

    .line 339
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v2

    .line 340
    .local v2, pos:I
    sub-int v10, v2, v5

    invoke-virtual {p0, v4, v10}, Lnet/flixster/android/model/TouchInterceptor;->setSelectionFromTop(II)V

    goto/16 :goto_0

    .line 325
    .end local v2           #pos:I
    .end local v4           #ref:I
    .end local v6           #v:Landroid/view/View;
    :cond_b
    const/4 v5, 0x4

    goto :goto_1

    .line 326
    :cond_c
    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mUpperBound:I

    if-ge v8, v10, :cond_9

    .line 328
    iget v10, p0, Lnet/flixster/android/model/TouchInterceptor;->mUpperBound:I

    div-int/lit8 v10, v10, 0x2

    if-ge v8, v10, :cond_d

    const/16 v5, -0x10

    :goto_2
    goto :goto_1

    :cond_d
    const/4 v5, -0x4

    goto :goto_2

    .line 348
    .end local v0           #action:I
    .end local v1           #itemnum:I
    .end local v5           #speed:I
    .end local v7           #x:I
    .end local v8           #y:I
    :cond_e
    invoke-super {p0, p1}, Landroid/widget/ListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v9

    goto/16 :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public setDragListener(Lnet/flixster/android/model/TouchInterceptor$DragListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 413
    iput-object p1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDragListener:Lnet/flixster/android/model/TouchInterceptor$DragListener;

    .line 414
    return-void
.end method

.method public setDropListener(Lnet/flixster/android/model/TouchInterceptor$DropListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 417
    iput-object p1, p0, Lnet/flixster/android/model/TouchInterceptor;->mDropListener:Lnet/flixster/android/model/TouchInterceptor$DropListener;

    .line 418
    return-void
.end method

.method public setRemoveListener(Lnet/flixster/android/model/TouchInterceptor$RemoveListener;)V
    .locals 0
    .parameter "l"

    .prologue
    .line 421
    iput-object p1, p0, Lnet/flixster/android/model/TouchInterceptor;->mRemoveListener:Lnet/flixster/android/model/TouchInterceptor$RemoveListener;

    .line 422
    return-void
.end method
