.class public Lnet/flixster/android/model/Unfulfillable;
.super Lnet/flixster/android/model/Movie;
.source "Unfulfillable.java"


# static fields
.field private static UNFULFILLABLE_HASH_BASE:J


# instance fields
.field private alternateDownloadUrl:Ljava/lang/String;

.field private alternateStreamUrl:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-wide v0, 0x2c68af0bb140000L

    sput-wide v0, Lnet/flixster/android/model/Unfulfillable;->UNFULFILLABLE_HASH_BASE:J

    .line 6
    return-void
.end method

.method public constructor <init>(J)V
    .locals 0
    .parameter "id"

    .prologue
    .line 10
    invoke-direct {p0, p1, p2}, Lnet/flixster/android/model/Movie;-><init>(J)V

    .line 11
    return-void
.end method

.method public static generateIdHash(J)J
    .locals 4
    .parameter "unfulfillableId"

    .prologue
    .line 27
    sget-wide v0, Lnet/flixster/android/model/Unfulfillable;->UNFULFILLABLE_HASH_BASE:J

    add-long/2addr v0, p0

    const-wide v2, 0x7fffffffffffffffL

    add-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public getAlternateUrl()Ljava/lang/String;
    .locals 2

    .prologue
    .line 21
    const-string v0, ""

    iget-object v1, p0, Lnet/flixster/android/model/Unfulfillable;->alternateStreamUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/model/Unfulfillable;->alternateStreamUrl:Ljava/lang/String;

    :goto_0
    return-object v0

    .line 22
    :cond_0
    const-string v0, ""

    iget-object v1, p0, Lnet/flixster/android/model/Unfulfillable;->alternateDownloadUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lnet/flixster/android/model/Unfulfillable;->alternateDownloadUrl:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const-string v0, "http://www.uvvu.com"

    goto :goto_0
.end method

.method public bridge synthetic parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1
    invoke-virtual {p0, p1}, Lnet/flixster/android/model/Unfulfillable;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Unfulfillable;

    move-result-object v0

    return-object v0
.end method

.method public parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Unfulfillable;
    .locals 1
    .parameter "json"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 14
    invoke-super {p0, p1}, Lnet/flixster/android/model/Movie;->parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/Movie;

    .line 15
    const-string v0, "streamUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Unfulfillable;->alternateStreamUrl:Ljava/lang/String;

    .line 16
    const-string v0, "downloadUrl"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnet/flixster/android/model/Unfulfillable;->alternateDownloadUrl:Ljava/lang/String;

    .line 17
    return-object p0
.end method
