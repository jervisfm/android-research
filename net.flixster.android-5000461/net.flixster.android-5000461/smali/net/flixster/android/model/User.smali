.class public Lnet/flixster/android/model/User;
.super Ljava/lang/Object;
.source "User.java"


# static fields
.field private static synthetic $SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType:[I = null

.field private static final BASE_PROPERTY_ARRAY:[Ljava/lang/String; = null

.field private static final EMPTY_RIGHTS:Ljava/util/List; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation
.end field

.field public static final USER_DISPLAYNAME_STRING:Ljava/lang/String; = "displayName"

.field public static final USER_FIRSTNAME_STRING:Ljava/lang/String; = "firstName"

.field public static final USER_GENDER_STRING:Ljava/lang/String; = "gender"

.field public static final USER_LASTNAME_STRING:Ljava/lang/String; = "lastName"

.field protected static final USER_NONE_PRO:Ljava/lang/String; = "user.none.pro.jpg"

.field protected static final USER_NONE_TMB:Ljava/lang/String; = "user.none.tmb.jpg"

.field public static final USER_PLATFORM_STRING:Ljava/lang/String; = "platform"

.field public static final USER_USERNAME_STRING:Ljava/lang/String; = "userName"


# instance fields
.field public age:I

.field public bitmap:Landroid/graphics/Bitmap;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public collectionsCount:I

.field public displayName:Ljava/lang/String;

.field public firstName:Ljava/lang/String;

.field public friendCount:I

.field public friendRatedReviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field public friendReviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field public friendWantToSeeReviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field public friends:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public friendsActivity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field public friendsIds:[Ljava/lang/Object;

.field public gender:Ljava/lang/String;

.field public id:Ljava/lang/String;

.field public isMskEligible:Z

.field public isMskFbInviteEligible:Z

.field public isMskInstallMobileEligible:Z

.field public isMskRateEligible:Z

.field public isMskSmsEligible:Z

.field public isMskUvCreateEligible:Z

.field public isMskWtsEligible:Z

.field public lastName:Ljava/lang/String;

.field private lockerRights:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation
.end field

.field private mPropertyMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public platform:Ljava/lang/String;

.field private profileBitmap:Lcom/flixster/android/model/Image;

.field public profileImage:Ljava/lang/String;

.field public ratedCount:I

.field public ratedReviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field public ratingCount:I

.field public refreshRequired:Z

.field public reviewCount:I

.field public reviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field public smallImage:Ljava/lang/String;

.field private thumbnailBitmap:Lcom/flixster/android/model/Image;

.field public thumbnailImage:Ljava/lang/String;

.field public token:Ljava/lang/String;

.field public userName:Ljava/lang/String;

.field public wantToSeeReviews:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/Review;",
            ">;"
        }
    .end annotation
.end field

.field public wtsCount:I


# direct methods
.method static synthetic $SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType()[I
    .locals 3

    .prologue
    .line 25
    sget-object v0, Lnet/flixster/android/model/User;->$SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lnet/flixster/android/model/LockerRight$PurchaseType;->values()[Lnet/flixster/android/model/LockerRight$PurchaseType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->FB_INVITE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_6

    :goto_1
    :try_start_1
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->INSTALL_MOBILE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_5

    :goto_2
    :try_start_2
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->RATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_4

    :goto_3
    :try_start_3
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->SMS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_3

    :goto_4
    :try_start_4
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->UNKNOWN:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_2

    :goto_5
    :try_start_5
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->UV_CREATE:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_1

    :goto_6
    :try_start_6
    sget-object v1, Lnet/flixster/android/model/LockerRight$PurchaseType;->WTS:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_0

    :goto_7
    sput-object v0, Lnet/flixster/android/model/User;->$SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :catch_3
    move-exception v1

    goto :goto_4

    :catch_4
    move-exception v1

    goto :goto_3

    :catch_5
    move-exception v1

    goto :goto_2

    :catch_6
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 33
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "platform"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string v2, "userName"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    .line 34
    const-string v2, "displayName"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "firstName"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "lastName"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "gender"

    aput-object v2, v0, v1

    .line 33
    sput-object v0, Lnet/flixster/android/model/User;->BASE_PROPERTY_ARRAY:[Ljava/lang/String;

    .line 224
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lnet/flixster/android/model/User;->EMPTY_RIGHTS:Ljava/util/List;

    .line 25
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/User;->mPropertyMap:Ljava/util/HashMap;

    .line 86
    return-void
.end method

.method private arrayParseJSONObject(Lorg/json/JSONObject;[Ljava/lang/String;)V
    .locals 6
    .parameter "jsonObj"
    .parameter "stringArray"

    .prologue
    .line 98
    :try_start_0
    array-length v3, p2

    const/4 v2, 0x0

    :goto_0
    if-lt v2, v3, :cond_0

    .line 108
    :goto_1
    return-void

    .line 98
    :cond_0
    aget-object v1, p2, v2

    .line 99
    .local v1, key:Ljava/lang/String;
    iget-object v4, p0, Lnet/flixster/android/model/User;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 100
    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 101
    iget-object v4, p0, Lnet/flixster/android/model/User;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 105
    .end local v1           #key:Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 106
    .local v0, e:Lorg/json/JSONException;
    const-string v2, "FlxMain"

    const-string v3, "User.arrayParse error "

    invoke-static {v2, v3, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method


# virtual methods
.method public addLockerRight(Lnet/flixster/android/model/LockerRight;)V
    .locals 1
    .parameter "right"

    .prologue
    .line 293
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-nez v0, :cond_0

    .line 294
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    .line 296
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    return-void
.end method

.method public addLockerRights(Ljava/util/Collection;)V
    .locals 1
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 300
    .local p1, rights:Ljava/util/Collection;,"Ljava/util/Collection<Lnet/flixster/android/model/LockerRight;>;"
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-nez v0, :cond_0

    .line 301
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    .line 303
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 304
    return-void
.end method

.method public fetchProfileBitmap()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 112
    iget-object v1, p0, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 113
    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 114
    .local v0, url:Ljava/net/URL;
    invoke-static {v0}, Lnet/flixster/android/util/HttpImageHelper;->fetchImage(Ljava/net/URL;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lnet/flixster/android/model/User;->bitmap:Landroid/graphics/Bitmap;

    .line 118
    .end local v0           #url:Ljava/net/URL;
    :goto_0
    return-void

    .line 116
    :cond_0
    const-string v1, "FlxMain"

    const-string v2, "No profileImage url!"

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getAssetId(J)J
    .locals 5
    .parameter "rightId"

    .prologue
    .line 326
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "User.getAssetId matching right id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v1, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    .line 333
    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    .line 327
    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 328
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    const-string v2, "FlxMain"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "User.getAssetId "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-wide v2, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 330
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->assetId:J

    goto :goto_0
.end method

.method public getGiftMoviesEarnedCount()I
    .locals 11

    .prologue
    .line 355
    const/4 v0, 0x0

    .line 356
    .local v0, count:I
    iget-object v8, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-eqz v8, :cond_0

    .line 357
    const/4 v3, 0x0

    .local v3, earnedMoviesRate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v6, 0x0

    .local v6, earnedMoviesWts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v4, 0x0

    .local v4, earnedMoviesSms:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v1, 0x0

    .local v1, earnedMoviesFbInvite:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v5, 0x0

    .local v5, earnedMoviesUvCreate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    const/4 v2, 0x0

    .line 358
    .local v2, earnedMoviesInstallMobile:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    iget-object v8, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-nez v9, :cond_1

    .line 405
    .end local v1           #earnedMoviesFbInvite:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .end local v2           #earnedMoviesInstallMobile:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .end local v3           #earnedMoviesRate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .end local v4           #earnedMoviesSms:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .end local v5           #earnedMoviesUvCreate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .end local v6           #earnedMoviesWts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_0
    return v0

    .line 358
    .restart local v1       #earnedMoviesFbInvite:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .restart local v2       #earnedMoviesInstallMobile:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .restart local v3       #earnedMoviesRate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .restart local v4       #earnedMoviesSms:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .restart local v5       #earnedMoviesUvCreate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    .restart local v6       #earnedMoviesWts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_1
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lnet/flixster/android/model/LockerRight;

    .line 359
    .local v7, r:Lnet/flixster/android/model/LockerRight;
    invoke-static {}, Lnet/flixster/android/model/User;->$SWITCH_TABLE$net$flixster$android$model$LockerRight$PurchaseType()[I

    move-result-object v9

    iget-object v10, v7, Lnet/flixster/android/model/LockerRight;->purchaseType:Lnet/flixster/android/model/LockerRight$PurchaseType;

    invoke-virtual {v10}, Lnet/flixster/android/model/LockerRight$PurchaseType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    goto :goto_0

    .line 361
    :pswitch_0
    if-nez v3, :cond_2

    .line 362
    new-instance v3, Ljava/util/ArrayList;

    .end local v3           #earnedMoviesRate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 364
    .restart local v3       #earnedMoviesRate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_2
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 365
    add-int/lit8 v0, v0, 0x1

    .line 366
    goto :goto_0

    .line 368
    :pswitch_1
    if-nez v6, :cond_3

    .line 369
    new-instance v6, Ljava/util/ArrayList;

    .end local v6           #earnedMoviesWts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 371
    .restart local v6       #earnedMoviesWts:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_3
    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 372
    add-int/lit8 v0, v0, 0x1

    .line 373
    goto :goto_0

    .line 375
    :pswitch_2
    if-nez v4, :cond_4

    .line 376
    new-instance v4, Ljava/util/ArrayList;

    .end local v4           #earnedMoviesSms:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 378
    .restart local v4       #earnedMoviesSms:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_4
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 379
    add-int/lit8 v0, v0, 0x1

    .line 380
    goto :goto_0

    .line 382
    :pswitch_3
    if-nez v1, :cond_5

    .line 383
    new-instance v1, Ljava/util/ArrayList;

    .end local v1           #earnedMoviesFbInvite:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 385
    .restart local v1       #earnedMoviesFbInvite:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_5
    invoke-interface {v1, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    add-int/lit8 v0, v0, 0x1

    .line 387
    goto :goto_0

    .line 389
    :pswitch_4
    if-nez v5, :cond_6

    .line 390
    new-instance v5, Ljava/util/ArrayList;

    .end local v5           #earnedMoviesUvCreate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 392
    .restart local v5       #earnedMoviesUvCreate:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_6
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 393
    add-int/lit8 v0, v0, 0x1

    .line 394
    goto :goto_0

    .line 396
    :pswitch_5
    if-nez v2, :cond_7

    .line 397
    new-instance v2, Ljava/util/ArrayList;

    .end local v2           #earnedMoviesInstallMobile:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 399
    .restart local v2       #earnedMoviesInstallMobile:Ljava/util/List;,"Ljava/util/List<Lnet/flixster/android/model/LockerRight;>;"
    :cond_7
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 400
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 359
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getLockerNonEpisodesCount()I
    .locals 4

    .prologue
    .line 260
    const/4 v0, 0x0

    .line 261
    .local v0, count:I
    iget-object v2, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 262
    iget-object v2, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 268
    :cond_1
    return v0

    .line 262
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnet/flixster/android/model/LockerRight;

    .line 263
    .local v1, right:Lnet/flixster/android/model/LockerRight;
    invoke-virtual {v1}, Lnet/flixster/android/model/LockerRight;->isEpisode()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, v1, Lnet/flixster/android/model/LockerRight;->isRental:Z

    if-nez v3, :cond_0

    .line 264
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public getLockerRightFromAssetId(J)Lnet/flixster/android/model/LockerRight;
    .locals 5
    .parameter "assetId"

    .prologue
    const/4 v1, 0x0

    .line 274
    iget-object v2, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-nez v2, :cond_0

    .line 275
    const-string v2, "FlxMain"

    const-string v3, "User.getLockerRight lockerRights null"

    invoke-static {v2, v3}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    .line 283
    :goto_0
    return-object v0

    .line 278
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    move-object v0, v1

    .line 283
    goto :goto_0

    .line 278
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 279
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    iget-wide v3, v0, Lnet/flixster/android/model/LockerRight;->assetId:J

    cmp-long v3, v3, p1

    if-nez v3, :cond_1

    goto :goto_0
.end method

.method public getLockerRightFromRightId(J)Lnet/flixster/android/model/LockerRight;
    .locals 5
    .parameter "rightId"

    .prologue
    const/4 v1, 0x0

    .line 313
    iget-object v2, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-nez v2, :cond_0

    move-object v0, v1

    .line 321
    :goto_0
    return-object v0

    .line 316
    :cond_0
    iget-object v2, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_2

    move-object v0, v1

    .line 321
    goto :goto_0

    .line 316
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/model/LockerRight;

    .line 317
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    iget-wide v3, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    cmp-long v3, v3, p1

    if-nez v3, :cond_1

    goto :goto_0
.end method

.method public getLockerRightId(J)J
    .locals 3
    .parameter "assetId"

    .prologue
    .line 288
    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/User;->getLockerRightFromAssetId(J)Lnet/flixster/android/model/LockerRight;

    move-result-object v0

    .line 289
    .local v0, right:Lnet/flixster/android/model/LockerRight;
    if-nez v0, :cond_0

    const-wide/16 v1, 0x0

    :goto_0
    return-wide v1

    :cond_0
    iget-wide v1, v0, Lnet/flixster/android/model/LockerRight;->rightId:J

    goto :goto_0
.end method

.method public getLockerRights()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lnet/flixster/android/model/LockerRight;",
            ">;"
        }
    .end annotation

    .prologue
    .line 237
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-nez v0, :cond_0

    sget-object v0, Lnet/flixster/android/model/User;->EMPTY_RIGHTS:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    goto :goto_0
.end method

.method public getLockerRightsCount()I
    .locals 4

    .prologue
    .line 242
    iget-object v1, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 243
    .local v0, count:I
    :goto_0
    const-string v1, "FlxMain"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "User.getLockerRightsCount "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    return v0

    .line 242
    .end local v0           #count:I
    :cond_0
    iget-object v1, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getProfileBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 4
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    const/4 v1, 0x0

    .line 122
    invoke-virtual {p0}, Lnet/flixster/android/model/User;->isProfileUrlAvailable()Z

    move-result v2

    if-nez v2, :cond_1

    move-object v0, v1

    .line 132
    :cond_0
    :goto_0
    return-object v0

    .line 125
    :cond_1
    iget-object v2, p0, Lnet/flixster/android/model/User;->profileBitmap:Lcom/flixster/android/model/Image;

    if-nez v2, :cond_2

    .line 126
    new-instance v2, Lcom/flixster/android/model/Image;

    iget-object v3, p0, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lnet/flixster/android/model/User;->profileBitmap:Lcom/flixster/android/model/Image;

    .line 128
    :cond_2
    iget-object v2, p0, Lnet/flixster/android/model/User;->profileBitmap:Lcom/flixster/android/model/Image;

    invoke-virtual {v2, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 129
    .local v0, bitmap:Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    iget-object v2, p0, Lnet/flixster/android/model/User;->thumbnailBitmap:Lcom/flixster/android/model/Image;

    if-eqz v2, :cond_0

    .line 130
    iget-object v2, p0, Lnet/flixster/android/model/User;->thumbnailBitmap:Lcom/flixster/android/model/Image;

    invoke-virtual {v2, v1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .parameter "propkey"

    .prologue
    .line 89
    iget-object v0, p0, Lnet/flixster/android/model/User;->mPropertyMap:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getRewardsEligibleCount()I
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 346
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 347
    .local v0, p:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lnet/flixster/android/model/User;->isMskRateEligible:Z

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lnet/flixster/android/model/Property;->isQuickRateRewardEnabled:Z

    if-eqz v1, :cond_1

    move v1, v2

    .line 348
    :goto_0
    iget-boolean v4, p0, Lnet/flixster/android/model/User;->isMskWtsEligible:Z

    if-eqz v4, :cond_2

    iget-boolean v4, v0, Lnet/flixster/android/model/Property;->isQuickWtsRewardEnabled:Z

    if-eqz v4, :cond_2

    move v4, v2

    :goto_1
    add-int/2addr v4, v1

    .line 349
    iget-boolean v1, p0, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    if-eqz v1, :cond_3

    iget-boolean v1, v0, Lnet/flixster/android/model/Property;->isSmsRewardEnabled:Z

    if-eqz v1, :cond_3

    move v1, v5

    :goto_2
    add-int/2addr v1, v4

    .line 350
    iget-boolean v4, p0, Lnet/flixster/android/model/User;->isMskFbInviteEligible:Z

    if-eqz v4, :cond_4

    iget-boolean v4, v0, Lnet/flixster/android/model/Property;->isFbInviteRewardEnabled:Z

    if-eqz v4, :cond_4

    :goto_3
    add-int/2addr v1, v5

    iget-boolean v4, p0, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    if-eqz v4, :cond_5

    :goto_4
    add-int v3, v1, v2

    .line 347
    :cond_0
    return v3

    :cond_1
    move v1, v3

    goto :goto_0

    :cond_2
    move v4, v3

    .line 348
    goto :goto_1

    :cond_3
    move v1, v3

    .line 349
    goto :goto_2

    :cond_4
    move v5, v3

    .line 350
    goto :goto_3

    :cond_5
    move v2, v3

    goto :goto_4
.end method

.method public getThumbnailBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;
    .locals 2
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/widget/ImageView;",
            ">(TT;)",
            "Landroid/graphics/Bitmap;"
        }
    .end annotation

    .prologue
    .line 136
    .local p1, imageView:Landroid/widget/ImageView;,"TT;"
    invoke-virtual {p0}, Lnet/flixster/android/model/User;->isThumbnailUrlAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    const/4 v0, 0x0

    .line 142
    :goto_0
    return-object v0

    .line 139
    :cond_0
    iget-object v0, p0, Lnet/flixster/android/model/User;->thumbnailBitmap:Lcom/flixster/android/model/Image;

    if-nez v0, :cond_1

    .line 140
    new-instance v0, Lcom/flixster/android/model/Image;

    iget-object v1, p0, Lnet/flixster/android/model/User;->thumbnailImage:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/flixster/android/model/Image;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lnet/flixster/android/model/User;->thumbnailBitmap:Lcom/flixster/android/model/Image;

    .line 142
    :cond_1
    iget-object v0, p0, Lnet/flixster/android/model/User;->thumbnailBitmap:Lcom/flixster/android/model/Image;

    invoke-virtual {v0, p1}, Lcom/flixster/android/model/Image;->getBitmap(Landroid/widget/ImageView;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public hasEpisodeRightsForSeason(J)Z
    .locals 6
    .parameter "seasonId"

    .prologue
    .line 252
    invoke-virtual {p0, p1, p2}, Lnet/flixster/android/model/User;->getLockerRightId(J)J

    move-result-wide v3

    invoke-static {v3, v4}, Lnet/flixster/android/model/LockerRight;->getRawSeasonRightId(J)J

    move-result-wide v0

    .line 253
    .local v0, rawSeasonRightId:J
    invoke-virtual {p0, v0, v1}, Lnet/flixster/android/model/User;->getLockerRightFromRightId(J)Lnet/flixster/android/model/LockerRight;

    move-result-object v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 254
    .local v2, result:Z
    :goto_0
    const-string v3, "FlxMain"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "User.hasEpisodeRightsForSeason "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    return v2

    .line 253
    .end local v2           #result:Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isLockerRightsFetched()Z
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isProfileUrlAvailable()Z
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    const-string v1, "user.none.pro.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRewardsEligible()Z
    .locals 2

    .prologue
    .line 337
    invoke-static {}, Lcom/flixster/android/utils/Properties;->instance()Lcom/flixster/android/utils/Properties;

    move-result-object v1

    invoke-virtual {v1}, Lcom/flixster/android/utils/Properties;->getProperties()Lnet/flixster/android/model/Property;

    move-result-object v0

    .line 338
    .local v0, p:Lnet/flixster/android/model/Property;
    if-eqz v0, :cond_5

    .line 339
    iget-boolean v1, p0, Lnet/flixster/android/model/User;->isMskRateEligible:Z

    if-eqz v1, :cond_0

    iget-boolean v1, v0, Lnet/flixster/android/model/Property;->isQuickRateRewardEnabled:Z

    if-nez v1, :cond_4

    .line 340
    :cond_0
    iget-boolean v1, p0, Lnet/flixster/android/model/User;->isMskWtsEligible:Z

    if-eqz v1, :cond_1

    iget-boolean v1, v0, Lnet/flixster/android/model/Property;->isQuickWtsRewardEnabled:Z

    if-nez v1, :cond_4

    .line 341
    :cond_1
    iget-boolean v1, p0, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    if-eqz v1, :cond_2

    iget-boolean v1, v0, Lnet/flixster/android/model/Property;->isSmsRewardEnabled:Z

    if-nez v1, :cond_4

    .line 342
    :cond_2
    iget-boolean v1, p0, Lnet/flixster/android/model/User;->isMskFbInviteEligible:Z

    if-eqz v1, :cond_3

    iget-boolean v1, v0, Lnet/flixster/android/model/Property;->isFbInviteRewardEnabled:Z

    if-nez v1, :cond_4

    :cond_3
    iget-boolean v1, p0, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    if-eqz v1, :cond_5

    :cond_4
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_5
    const/4 v1, 0x0

    .line 338
    goto :goto_0
.end method

.method public isThumbnailUrlAvailable()Z
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lnet/flixster/android/model/User;->thumbnailImage:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lnet/flixster/android/model/User;->thumbnailImage:Ljava/lang/String;

    const-string v1, "user.none.tmb.jpg"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public parseFromJSON(Lorg/json/JSONObject;)Lnet/flixster/android/model/User;
    .locals 8
    .parameter "jsonUser"

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 158
    :try_start_0
    sget-object v6, Lnet/flixster/android/model/User;->BASE_PROPERTY_ARRAY:[Ljava/lang/String;

    invoke-direct {p0, p1, v6}, Lnet/flixster/android/model/User;->arrayParseJSONObject(Lorg/json/JSONObject;[Ljava/lang/String;)V

    .line 159
    const-string v6, "id"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 160
    const-string v6, "id"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->id:Ljava/lang/String;

    .line 162
    :cond_0
    const-string v6, "token"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 163
    const-string v6, "token"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->token:Ljava/lang/String;

    .line 165
    :cond_1
    const-string v6, "platform"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 166
    const-string v6, "platform"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->platform:Ljava/lang/String;

    .line 168
    :cond_2
    const-string v6, "userName"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 169
    const-string v6, "userName"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->userName:Ljava/lang/String;

    .line 171
    :cond_3
    const-string v6, "firstName"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 172
    const-string v6, "firstName"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->firstName:Ljava/lang/String;

    .line 174
    :cond_4
    const-string v6, "lastName"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 175
    const-string v6, "lastName"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    .line 177
    :cond_5
    const-string v6, "FLX"

    iget-object v7, p0, Lnet/flixster/android/model/User;->platform:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 178
    iget-object v6, p0, Lnet/flixster/android/model/User;->firstName:Ljava/lang/String;

    iput-object v6, p0, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    .line 179
    iget-object v6, p0, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    if-eqz v6, :cond_6

    .line 180
    iget-object v6, p0, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v7, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, " "

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lnet/flixster/android/model/User;->lastName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;

    .line 185
    :cond_6
    :goto_0
    const-string v6, "gender"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 186
    const-string v6, "gender"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->gender:Ljava/lang/String;

    .line 188
    :cond_7
    const-string v6, "age"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 189
    const-string v6, "age"

    const/4 v7, -0x1

    invoke-virtual {p1, v6, v7}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/model/User;->age:I

    .line 191
    :cond_8
    const-string v6, "images"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 192
    const-string v6, "images"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 193
    .local v1, imagesObject:Lorg/json/JSONObject;
    const-string v6, "profile"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->profileImage:Ljava/lang/String;

    .line 194
    const-string v6, "thumbnail"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->thumbnailImage:Ljava/lang/String;

    .line 195
    const-string v6, "small"

    invoke-virtual {v1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->smallImage:Ljava/lang/String;

    .line 197
    .end local v1           #imagesObject:Lorg/json/JSONObject;
    :cond_9
    const-string v6, "stats"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 198
    const-string v6, "stats"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 199
    .local v3, statsObject:Lorg/json/JSONObject;
    const-string v6, "wts"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/model/User;->wtsCount:I

    .line 200
    const-string v6, "ratings"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/model/User;->ratingCount:I

    .line 201
    const-string v6, "reviews"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/model/User;->reviewCount:I

    .line 202
    const-string v6, "friends"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/model/User;->friendCount:I

    .line 203
    const-string v6, "collected"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p0, Lnet/flixster/android/model/User;->collectionsCount:I

    .line 205
    .end local v3           #statsObject:Lorg/json/JSONObject;
    :cond_a
    const-string v6, "permissions"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 206
    const-string v6, "permissions"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    .line 207
    .local v2, permissionsObject:Lorg/json/JSONObject;
    const-string v6, "isMskEligible"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_d

    move v6, v4

    :goto_1
    iput-boolean v6, p0, Lnet/flixster/android/model/User;->isMskEligible:Z

    .line 208
    const-string v6, "isMskRateEligible"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_e

    move v6, v4

    :goto_2
    iput-boolean v6, p0, Lnet/flixster/android/model/User;->isMskRateEligible:Z

    .line 209
    const-string v6, "isMskWtsEligible"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_f

    move v6, v4

    :goto_3
    iput-boolean v6, p0, Lnet/flixster/android/model/User;->isMskWtsEligible:Z

    .line 210
    const-string v6, "isMskSmsEligible"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_10

    move v6, v4

    :goto_4
    iput-boolean v6, p0, Lnet/flixster/android/model/User;->isMskSmsEligible:Z

    .line 211
    const-string v6, "isMskFbInviteEligible"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_11

    move v6, v4

    :goto_5
    iput-boolean v6, p0, Lnet/flixster/android/model/User;->isMskFbInviteEligible:Z

    .line 212
    const-string v6, "isMskUvCreateEligible"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_12

    move v6, v4

    :goto_6
    iput-boolean v6, p0, Lnet/flixster/android/model/User;->isMskUvCreateEligible:Z

    .line 213
    const-string v6, "isMskInstallMobileEligible"

    invoke-virtual {v2, v6}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;)I

    move-result v6

    if-eqz v6, :cond_13

    :goto_7
    iput-boolean v4, p0, Lnet/flixster/android/model/User;->isMskInstallMobileEligible:Z

    .line 219
    .end local v2           #permissionsObject:Lorg/json/JSONObject;
    :cond_b
    :goto_8
    return-object p0

    .line 182
    :cond_c
    const-string v6, "displayName"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 183
    const-string v6, "displayName"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lnet/flixster/android/FlixUtils;->copyString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lnet/flixster/android/model/User;->displayName:Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 215
    :catch_0
    move-exception v0

    .line 217
    .local v0, e:Lorg/json/JSONException;
    const-string v4, "FlxMain"

    const-string v5, "User object parse error "

    invoke-static {v4, v5, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_8

    .end local v0           #e:Lorg/json/JSONException;
    .restart local v2       #permissionsObject:Lorg/json/JSONObject;
    :cond_d
    move v6, v5

    .line 207
    goto :goto_1

    :cond_e
    move v6, v5

    .line 208
    goto :goto_2

    :cond_f
    move v6, v5

    .line 209
    goto :goto_3

    :cond_10
    move v6, v5

    .line 210
    goto :goto_4

    :cond_11
    move v6, v5

    .line 211
    goto :goto_5

    :cond_12
    move v6, v5

    .line 212
    goto :goto_6

    :cond_13
    move v4, v5

    .line 213
    goto :goto_7
.end method

.method public removeLockerRight(Lnet/flixster/android/model/LockerRight;)V
    .locals 1
    .parameter "right"

    .prologue
    .line 307
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 310
    :cond_0
    return-void
.end method

.method public resetLockerRights()V
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x0

    iput-object v0, p0, Lnet/flixster/android/model/User;->lockerRights:Ljava/util/List;

    .line 228
    return-void
.end method
