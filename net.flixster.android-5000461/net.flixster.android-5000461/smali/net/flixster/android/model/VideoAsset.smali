.class public abstract Lnet/flixster/android/model/VideoAsset;
.super Ljava/lang/Object;
.source "VideoAsset.java"


# instance fields
.field protected downloadUri:Ljava/lang/String;

.field protected editionId:J

.field protected id:J

.field protected isCollected:Z

.field protected isDownloadSupported:Z

.field protected isStreamingSupported:Z

.field protected title:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 0
    .parameter "assetId"
    .parameter "edition"

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide p1, p0, Lnet/flixster/android/model/VideoAsset;->id:J

    .line 16
    iput-wide p3, p0, Lnet/flixster/android/model/VideoAsset;->editionId:J

    .line 17
    return-void
.end method


# virtual methods
.method public getId()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lnet/flixster/android/model/VideoAsset;->id:J

    return-wide v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lnet/flixster/android/model/VideoAsset;->title:Ljava/lang/String;

    return-object v0
.end method
