.class public Lnet/flixster/android/util/DialogUtils;
.super Ljava/lang/Object;
.source "DialogUtils.java"


# static fields
.field private static sShowtimesDateOptions:[Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const/4 v0, 0x0

    sput-object v0, Lnet/flixster/android/util/DialogUtils;->sShowtimesDateOptions:[Ljava/lang/CharSequence;

    .line 11
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getLocationOptions()[Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 28
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/CharSequence;

    .line 29
    .local v0, locations:[Ljava/lang/CharSequence;
    const/4 v1, 0x0

    const-string v2, "Near me"

    aput-object v2, v0, v1

    .line 30
    const/4 v1, 0x1

    const-string v2, "( change postal code )"

    aput-object v2, v0, v1

    .line 31
    const/4 v1, 0x2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserLocation()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 32
    return-object v0
.end method

.method public static getShowtimesDateOptions(Landroid/content/Context;)[Ljava/lang/CharSequence;
    .locals 6
    .parameter "context"

    .prologue
    .line 15
    sget-object v2, Lnet/flixster/android/util/DialogUtils;->sShowtimesDateOptions:[Ljava/lang/CharSequence;

    if-nez v2, :cond_0

    .line 16
    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/CharSequence;

    sput-object v2, Lnet/flixster/android/util/DialogUtils;->sShowtimesDateOptions:[Ljava/lang/CharSequence;

    .line 17
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 18
    .local v0, date:Ljava/util/Calendar;
    sget-object v2, Lnet/flixster/android/util/DialogUtils;->sShowtimesDateOptions:[Ljava/lang/CharSequence;

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0167

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 19
    const/4 v1, 0x1

    .local v1, i:I
    :goto_0
    sget-object v2, Lnet/flixster/android/util/DialogUtils;->sShowtimesDateOptions:[Ljava/lang/CharSequence;

    array-length v2, v2

    if-lt v1, v2, :cond_1

    .line 24
    .end local v0           #date:Ljava/util/Calendar;
    .end local v1           #i:I
    :cond_0
    sget-object v2, Lnet/flixster/android/util/DialogUtils;->sShowtimesDateOptions:[Ljava/lang/CharSequence;

    return-object v2

    .line 20
    .restart local v0       #date:Ljava/util/Calendar;
    .restart local v1       #i:I
    :cond_1
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 21
    sget-object v2, Lnet/flixster/android/util/DialogUtils;->sShowtimesDateOptions:[Ljava/lang/CharSequence;

    sget-object v3, Lcom/flixster/android/utils/DateTimeHelper;->DAY_IN_WEEK_FORMATTER:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    .line 19
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
