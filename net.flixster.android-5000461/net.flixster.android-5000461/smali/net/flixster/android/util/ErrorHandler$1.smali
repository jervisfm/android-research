.class Lnet/flixster/android/util/ErrorHandler$1;
.super Ljava/lang/Object;
.source "ErrorHandler.java"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lnet/flixster/android/util/ErrorHandler;->setDefaultUncaughtExceptionHandler(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lnet/flixster/android/util/ErrorHandler;

.field private final synthetic val$appContext:Landroid/content/Context;

.field private final synthetic val$osUeh:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method constructor <init>(Lnet/flixster/android/util/ErrorHandler;Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0
    .parameter
    .parameter
    .parameter

    .prologue
    .line 1
    iput-object p1, p0, Lnet/flixster/android/util/ErrorHandler$1;->this$0:Lnet/flixster/android/util/ErrorHandler;

    iput-object p2, p0, Lnet/flixster/android/util/ErrorHandler$1;->val$appContext:Landroid/content/Context;

    iput-object p3, p0, Lnet/flixster/android/util/ErrorHandler$1;->val$osUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 7
    .parameter "thread"
    .parameter "ex"

    .prologue
    .line 39
    #calls: Lnet/flixster/android/util/ErrorHandler;->buildDiagnosticMessage(Ljava/lang/Thread;Ljava/lang/Throwable;)Ljava/lang/String;
    invoke-static {p1, p2}, Lnet/flixster/android/util/ErrorHandler;->access$0(Ljava/lang/Thread;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 40
    .local v0, diagnostics:Ljava/lang/String;
    const-string v4, "FlxMain"

    invoke-static {v4, v0}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const/4 v3, 0x0

    .line 45
    .local v3, fos:Ljava/io/FileOutputStream;
    :try_start_0
    const-string v2, "flixster.stacktrace"

    .line 46
    .local v2, filename:Ljava/lang/String;
    iget-object v4, p0, Lnet/flixster/android/util/ErrorHandler$1;->val$appContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v3

    .line 47
    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/FileOutputStream;->write([B)V

    .line 48
    const-string v4, "FlxMain"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Diagnostics saved to "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 54
    if-eqz v3, :cond_0

    .line 56
    :try_start_1
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5

    .line 63
    .end local v2           #filename:Ljava/lang/String;
    :cond_0
    :goto_0
    iget-object v4, p0, Lnet/flixster/android/util/ErrorHandler$1;->val$osUeh:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v4, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 64
    return-void

    .line 49
    :catch_0
    move-exception v1

    .line 50
    .local v1, e:Ljava/io/FileNotFoundException;
    :try_start_2
    const-string v4, "FlxMain"

    const-string v5, "FileNotFoundException"

    invoke-static {v4, v5, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 54
    if-eqz v3, :cond_0

    .line 56
    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    .line 57
    :catch_1
    move-exception v1

    .line 58
    .local v1, e:Ljava/io/IOException;
    const-string v4, "FlxMain"

    const-string v5, "IOException"

    invoke-static {v4, v5, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 51
    .end local v1           #e:Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 52
    .restart local v1       #e:Ljava/io/IOException;
    :try_start_4
    const-string v4, "FlxMain"

    const-string v5, "IOException"

    invoke-static {v4, v5, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 54
    if-eqz v3, :cond_0

    .line 56
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    .line 57
    :catch_3
    move-exception v1

    .line 58
    const-string v4, "FlxMain"

    const-string v5, "IOException"

    invoke-static {v4, v5, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 53
    .end local v1           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v4

    .line 54
    if-eqz v3, :cond_1

    .line 56
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 60
    :cond_1
    :goto_1
    throw v4

    .line 57
    :catch_4
    move-exception v1

    .line 58
    .restart local v1       #e:Ljava/io/IOException;
    const-string v5, "FlxMain"

    const-string v6, "IOException"

    invoke-static {v5, v6, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 57
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #filename:Ljava/lang/String;
    :catch_5
    move-exception v1

    .line 58
    .restart local v1       #e:Ljava/io/IOException;
    const-string v4, "FlxMain"

    const-string v5, "IOException"

    invoke-static {v4, v5, v1}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
