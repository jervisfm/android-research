.class public Lnet/flixster/android/util/ErrorHandler;
.super Ljava/lang/Object;
.source "ErrorHandler.java"


# static fields
.field private static final DIAGNOSTICS_FILE:Ljava/lang/String; = "flixster.stacktrace"

.field private static final EMAIL_SUBJECT:Ljava/lang/String; = "Android Diagnostic Report v"

.field private static final INSTANCE:Lnet/flixster/android/util/ErrorHandler;


# instance fields
.field private hasChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lnet/flixster/android/util/ErrorHandler;

    invoke-direct {v0}, Lnet/flixster/android/util/ErrorHandler;-><init>()V

    sput-object v0, Lnet/flixster/android/util/ErrorHandler;->INSTANCE:Lnet/flixster/android/util/ErrorHandler;

    .line 18
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method static synthetic access$0(Ljava/lang/Thread;Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 1
    .parameter
    .parameter

    .prologue
    .line 124
    invoke-static {p0, p1}, Lnet/flixster/android/util/ErrorHandler;->buildDiagnosticMessage(Ljava/lang/Thread;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static buildDiagnosticMessage(Ljava/lang/Thread;Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 3
    .parameter "thread"
    .parameter "ex"

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 126
    .local v0, sb:Ljava/lang/StringBuilder;
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Uncaught exception thrown on thread "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 128
    invoke-static {p1}, Lnet/flixster/android/util/ErrorHandler;->buildStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static buildEmailIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 4
    .parameter "recipient"
    .parameter "subject"
    .parameter "body"

    .prologue
    .line 90
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.SEND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 91
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.intent.extra.EMAIL"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 92
    const-string v1, "android.intent.extra.SUBJECT"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 93
    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    const-string v1, "message/rfc822"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    return-object v0
.end method

.method private static buildStackTrace(Ljava/lang/Throwable;)Ljava/lang/String;
    .locals 8
    .parameter "ex"

    .prologue
    .line 133
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 134
    .local v3, sb:Ljava/lang/StringBuilder;
    move-object v0, p0

    .line 135
    .local v0, currEx:Ljava/lang/Throwable;
    const/4 v1, 0x0

    .line 136
    .local v1, isNestedEx:Z
    :goto_0
    if-nez v0, :cond_0

    .line 151
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5

    .line 137
    :cond_0
    if-eqz v1, :cond_1

    .line 138
    const-string v5, "Caused by: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v5, "\n"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    invoke-virtual {v0}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    .line 143
    .local v4, stack:[Ljava/lang/StackTraceElement;
    array-length v6, v4

    const/4 v5, 0x0

    :goto_1
    if-lt v5, v6, :cond_2

    .line 148
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 149
    const/4 v1, 0x1

    goto :goto_0

    .line 143
    :cond_2
    aget-object v2, v4, v5

    .line 144
    .local v2, s:Ljava/lang/StackTraceElement;
    const-string v7, "\t"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    invoke-virtual {v2}, Ljava/lang/StackTraceElement;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 146
    const-string v7, "\n"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    add-int/lit8 v5, v5, 0x1

    goto :goto_1
.end method

.method private static checkForDiagnosticMessage(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .parameter "appContext"

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 100
    .local v0, br:Ljava/io/BufferedReader;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 102
    .local v4, sb:Ljava/lang/StringBuilder;
    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v5, Ljava/io/InputStreamReader;

    const-string v6, "flixster.stacktrace"

    invoke-virtual {p0, v6}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_7
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 103
    .end local v0           #br:Ljava/io/BufferedReader;
    .local v1, br:Ljava/io/BufferedReader;
    :try_start_1
    const-string v5, "FlxMain"

    const-string v6, "Diagnostics found"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_0
    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v3

    .local v3, line:Ljava/lang/String;
    if-nez v3, :cond_1

    .line 108
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6

    move-result-object v5

    .line 114
    if-eqz v1, :cond_0

    .line 116
    :try_start_2
    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_0
    :goto_1
    move-object v0, v1

    .line 121
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v3           #line:Ljava/lang/String;
    .restart local v0       #br:Ljava/io/BufferedReader;
    :goto_2
    return-object v5

    .line 106
    .end local v0           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    .restart local v3       #line:Ljava/lang/String;
    :cond_1
    :try_start_3
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_6

    goto :goto_0

    .line 109
    .end local v3           #line:Ljava/lang/String;
    :catch_0
    move-exception v2

    move-object v0, v1

    .line 110
    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v0       #br:Ljava/io/BufferedReader;
    .local v2, e:Ljava/io/FileNotFoundException;
    :goto_3
    :try_start_4
    const-string v5, "FlxMain"

    const-string v6, "No diagnostics found"

    invoke-static {v5, v6}, Lcom/flixster/android/utils/Logger;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 114
    if-eqz v0, :cond_2

    .line 116
    :try_start_5
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    .line 121
    .end local v2           #e:Ljava/io/FileNotFoundException;
    :cond_2
    :goto_4
    const/4 v5, 0x0

    goto :goto_2

    .line 117
    .end local v0           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    .restart local v3       #line:Ljava/lang/String;
    :catch_1
    move-exception v2

    .line 118
    .local v2, e:Ljava/io/IOException;
    const-string v6, "FlxMain"

    const-string v7, "IOException"

    invoke-static {v6, v7, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 117
    .end local v1           #br:Ljava/io/BufferedReader;
    .end local v3           #line:Ljava/lang/String;
    .restart local v0       #br:Ljava/io/BufferedReader;
    .local v2, e:Ljava/io/FileNotFoundException;
    :catch_2
    move-exception v2

    .line 118
    .local v2, e:Ljava/io/IOException;
    const-string v5, "FlxMain"

    const-string v6, "IOException"

    invoke-static {v5, v6, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 111
    .end local v2           #e:Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 112
    .restart local v2       #e:Ljava/io/IOException;
    :goto_5
    :try_start_6
    const-string v5, "FlxMain"

    const-string v6, "IOException"

    invoke-static {v5, v6, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 114
    if-eqz v0, :cond_2

    .line 116
    :try_start_7
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    goto :goto_4

    .line 117
    :catch_4
    move-exception v2

    .line 118
    const-string v5, "FlxMain"

    const-string v6, "IOException"

    invoke-static {v5, v6, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 113
    .end local v2           #e:Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 114
    :goto_6
    if-eqz v0, :cond_3

    .line 116
    :try_start_8
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 120
    :cond_3
    :goto_7
    throw v5

    .line 117
    :catch_5
    move-exception v2

    .line 118
    .restart local v2       #e:Ljava/io/IOException;
    const-string v6, "FlxMain"

    const-string v7, "IOException"

    invoke-static {v6, v7, v2}, Lcom/flixster/android/utils/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_7

    .line 113
    .end local v0           #br:Ljava/io/BufferedReader;
    .end local v2           #e:Ljava/io/IOException;
    .restart local v1       #br:Ljava/io/BufferedReader;
    :catchall_1
    move-exception v5

    move-object v0, v1

    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v0       #br:Ljava/io/BufferedReader;
    goto :goto_6

    .line 111
    .end local v0           #br:Ljava/io/BufferedReader;
    .restart local v1       #br:Ljava/io/BufferedReader;
    :catch_6
    move-exception v2

    move-object v0, v1

    .end local v1           #br:Ljava/io/BufferedReader;
    .restart local v0       #br:Ljava/io/BufferedReader;
    goto :goto_5

    .line 109
    :catch_7
    move-exception v2

    goto :goto_3
.end method

.method public static instance()Lnet/flixster/android/util/ErrorHandler;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lnet/flixster/android/util/ErrorHandler;->INSTANCE:Lnet/flixster/android/util/ErrorHandler;

    return-object v0
.end method


# virtual methods
.method public checkForAppCrash(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6
    .parameter "appContext"

    .prologue
    .line 70
    iget-boolean v4, p0, Lnet/flixster/android/util/ErrorHandler;->hasChecked:Z

    if-eqz v4, :cond_1

    .line 71
    const/4 v2, 0x0

    .line 86
    :cond_0
    :goto_0
    return-object v2

    .line 73
    :cond_1
    const/4 v4, 0x1

    iput-boolean v4, p0, Lnet/flixster/android/util/ErrorHandler;->hasChecked:Z

    .line 74
    const/4 v2, 0x0

    .line 75
    .local v2, intent:Landroid/content/Intent;
    invoke-static {p1}, Lnet/flixster/android/util/ErrorHandler;->checkForDiagnosticMessage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 76
    .local v1, diagnostics:Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 79
    const-string v4, "flixster.stacktrace"

    invoke-virtual {p1, v4}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 82
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Android Diagnostic Report v"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getVersionName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 83
    .local v3, subject:Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserAgent()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 84
    .local v0, body:Ljava/lang/String;
    invoke-static {}, Lnet/flixster/android/data/ApiBuilder;->supportEmail()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v3, v0}, Lnet/flixster/android/util/ErrorHandler;->buildEmailIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    goto :goto_0
.end method

.method public setDefaultUncaughtExceptionHandler(Landroid/content/Context;)V
    .locals 2
    .parameter "appContext"

    .prologue
    .line 35
    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    .line 36
    .local v0, osUeh:Ljava/lang/Thread$UncaughtExceptionHandler;
    new-instance v1, Lnet/flixster/android/util/ErrorHandler$1;

    invoke-direct {v1, p0, p1, v0}, Lnet/flixster/android/util/ErrorHandler$1;-><init>(Lnet/flixster/android/util/ErrorHandler;Landroid/content/Context;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 66
    return-void
.end method
