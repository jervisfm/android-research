.class public Lnet/flixster/android/util/HttpHelper;
.super Ljava/lang/Object;
.source "HttpHelper.java"


# static fields
.field private static final BUFFERSIZE:I = 0x400

.field private static final CHUNKSIZE:I = 0x2000

.field private static final TIMEOUT_CONNECTION:I = 0xfa0

.field private static final TIMEOUT_HTTP_POST:I = 0x1d4c0

.field private static final TIMEOUT_READ:I = 0xea60


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static deleteFromUrl(Ljava/lang/String;)Ljava/lang/String;
    .locals 12
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 150
    const-string v9, "FlxApi"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "HttpHelper.deleteFromUrl url:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    const/4 v1, 0x0

    .line 153
    .local v1, content:Ljava/io/InputStream;
    :try_start_0
    invoke-static {p0}, Lnet/flixster/android/data/ApiBuilder;->isSecureFlxUrl(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    new-instance v4, Lcom/flixster/android/net/ssl/SecureFlxHttpClient;

    invoke-direct {v4}, Lcom/flixster/android/net/ssl/SecureFlxHttpClient;-><init>()V

    .line 155
    .local v4, httpClient:Lorg/apache/http/client/HttpClient;
    :goto_0
    invoke-interface {v4}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 156
    .local v0, clientParameters:Lorg/apache/http/params/HttpParams;
    const v9, 0x1d4c0

    invoke-static {v0, v9}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 157
    const v9, 0x1d4c0

    invoke-static {v0, v9}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 158
    new-instance v2, Lorg/apache/http/client/methods/HttpDelete;

    invoke-direct {v2, p0}, Lorg/apache/http/client/methods/HttpDelete;-><init>(Ljava/lang/String;)V

    .line 159
    .local v2, delete:Lorg/apache/http/client/methods/HttpDelete;
    invoke-interface {v4, v2}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v7

    .line 160
    .local v7, response:Lorg/apache/http/HttpResponse;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 161
    .local v8, responseBuilder:Ljava/lang/StringBuilder;
    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 162
    .local v3, entity:Lorg/apache/http/HttpEntity;
    if-eqz v3, :cond_0

    .line 163
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 164
    new-instance v6, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/InputStreamReader;

    invoke-direct {v9, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v6, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 166
    .local v6, reader:Ljava/io/BufferedReader;
    :goto_1
    invoke-virtual {v6}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, line:Ljava/lang/String;
    if-nez v5, :cond_4

    .line 170
    .end local v5           #line:Ljava/lang/String;
    .end local v6           #reader:Ljava/io/BufferedReader;
    :cond_0
    const-string v9, "FlxApi"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "HttpHelper.deleteFromUrl response code:"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v7}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v11

    invoke-interface {v11}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 173
    if-eqz v1, :cond_1

    .line 175
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 171
    :cond_1
    :goto_2
    return-object v9

    .line 154
    .end local v0           #clientParameters:Lorg/apache/http/params/HttpParams;
    .end local v2           #delete:Lorg/apache/http/client/methods/HttpDelete;
    .end local v3           #entity:Lorg/apache/http/HttpEntity;
    .end local v4           #httpClient:Lorg/apache/http/client/HttpClient;
    .end local v7           #response:Lorg/apache/http/HttpResponse;
    .end local v8           #responseBuilder:Ljava/lang/StringBuilder;
    :cond_2
    :try_start_2
    new-instance v4, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v4}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 172
    :catchall_0
    move-exception v9

    .line 173
    if-eqz v1, :cond_3

    .line 175
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 180
    :cond_3
    :goto_3
    throw v9

    .line 167
    .restart local v0       #clientParameters:Lorg/apache/http/params/HttpParams;
    .restart local v2       #delete:Lorg/apache/http/client/methods/HttpDelete;
    .restart local v3       #entity:Lorg/apache/http/HttpEntity;
    .restart local v4       #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v5       #line:Ljava/lang/String;
    .restart local v6       #reader:Ljava/io/BufferedReader;
    .restart local v7       #response:Lorg/apache/http/HttpResponse;
    .restart local v8       #responseBuilder:Ljava/lang/StringBuilder;
    :cond_4
    :try_start_4
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 176
    .end local v5           #line:Ljava/lang/String;
    .end local v6           #reader:Ljava/io/BufferedReader;
    :catch_0
    move-exception v10

    goto :goto_2

    .end local v0           #clientParameters:Lorg/apache/http/params/HttpParams;
    .end local v2           #delete:Lorg/apache/http/client/methods/HttpDelete;
    .end local v3           #entity:Lorg/apache/http/HttpEntity;
    .end local v4           #httpClient:Lorg/apache/http/client/HttpClient;
    .end local v7           #response:Lorg/apache/http/HttpResponse;
    .end local v8           #responseBuilder:Ljava/lang/StringBuilder;
    :catch_1
    move-exception v10

    goto :goto_3
.end method

.method public static fetchUrl(Ljava/net/URL;ZZ)Ljava/lang/String;
    .locals 3
    .parameter "url"
    .parameter "checkCache"
    .parameter "shouldCache"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 38
    const/4 v2, 0x0

    invoke-static {p0, p1, p2, v2}, Lnet/flixster/android/util/HttpHelper;->fetchUrlBytes(Ljava/net/URL;ZZZ)[B

    move-result-object v0

    .line 41
    .local v0, responseBody:[B
    new-instance v1, Ljava/lang/String;

    const-string v2, "windows-1252"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 42
    .local v1, responseString:Ljava/lang/String;
    return-object v1
.end method

.method public static fetchUrlBytes(Ljava/net/HttpURLConnection;)[B
    .locals 1
    .parameter "connection"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lnet/flixster/android/util/HttpHelper;->fetchUrlBytes(Ljava/net/HttpURLConnection;Z)[B

    move-result-object v0

    return-object v0
.end method

.method private static fetchUrlBytes(Ljava/net/HttpURLConnection;Z)[B
    .locals 8
    .parameter "connection"
    .parameter "useFlixsterUa"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 83
    invoke-static {}, Lnet/flixster/android/FlixsterApplication;->getUserAgent()Ljava/lang/String;

    move-result-object v5

    .line 84
    .local v5, userAgent:Ljava/lang/String;
    if-eqz p1, :cond_0

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Flixster"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 85
    :cond_0
    const-string v6, "User-Agent"

    invoke-virtual {p0, v6, v5}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 86
    const/16 v6, 0xfa0

    invoke-virtual {p0, v6}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 87
    const v6, 0xea60

    invoke-virtual {p0, v6}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 89
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, contentEncoding:Ljava/lang/String;
    const/4 v4, 0x0

    .line 92
    .local v4, tempStream:Ljava/io/InputStream;
    :try_start_0
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    .line 103
    if-nez v4, :cond_2

    .line 104
    new-instance v6, Ljava/io/IOException;

    const-string v7, "HttpHelper.fetchUrlBytes connection stream null"

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 93
    :catch_0
    move-exception v1

    .line 94
    .local v1, ioe:Ljava/io/IOException;
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v3

    .line 95
    .local v3, responseCode:I
    const/16 v6, 0x191

    if-ne v3, v6, :cond_1

    .line 96
    new-instance v6, Lcom/flixster/android/net/HttpUnauthorizedException;

    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7, v1}, Lcom/flixster/android/net/HttpUnauthorizedException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v6

    .line 98
    :cond_1
    throw v1

    .line 100
    .end local v1           #ioe:Ljava/io/IOException;
    .end local v3           #responseCode:I
    :catch_1
    move-exception v2

    .line 101
    .local v2, ise:Ljava/lang/IllegalStateException;
    new-instance v6, Ljava/io/IOException;

    invoke-virtual {v2}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 106
    .end local v2           #ise:Ljava/lang/IllegalStateException;
    :cond_2
    const-string v6, "gzip"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    new-instance v6, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v6, v4}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v4, v6

    .line 107
    :cond_3
    invoke-static {v4}, Lnet/flixster/android/util/HttpHelper;->streamToByteArray(Ljava/io/InputStream;)[B

    move-result-object v6

    return-object v6
.end method

.method protected static fetchUrlBytes(Ljava/net/URL;ZZZ)[B
    .locals 6
    .parameter "url"
    .parameter "checkCache"
    .parameter "shouldCache"
    .parameter "useFlixsterUa"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 48
    const-string v4, "FlxApi"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v3, "HttpHelper.fetchUrlBytes "

    invoke-direct {v5, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz p1, :cond_0

    const-string v3, "checkCache "

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    .line 49
    if-eqz p2, :cond_1

    const-string v3, "shouldCache "

    :goto_1
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 48
    invoke-static {v4, v3}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    if-eqz p1, :cond_3

    .line 52
    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sFlixsterCacheManager:Lnet/flixster/android/FlixsterCacheManager;

    invoke-virtual {p0}, Ljava/net/URL;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4}, Lnet/flixster/android/FlixsterCacheManager;->get(I)[B

    move-result-object v1

    .line 53
    .local v1, result:[B
    if-eqz v1, :cond_2

    array-length v3, v1

    if-lez v3, :cond_2

    .line 54
    const-string v3, "FlxApi"

    const-string v4, "HttpHelper.fetchUrlBytes cache hit"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v1

    .line 71
    .end local v1           #result:[B
    .local v2, result:[B
    :goto_2
    return-object v2

    .line 48
    .end local v2           #result:[B
    :cond_0
    const-string v3, ""

    goto :goto_0

    .line 49
    :cond_1
    const-string v3, ""

    goto :goto_1

    .line 56
    .restart local v1       #result:[B
    :cond_2
    if-eqz v1, :cond_5

    array-length v3, v1

    if-nez v3, :cond_5

    .line 57
    const-string v3, "FlxApi"

    const-string v4, "HttpHelper.fetchUrlBytes cache empty"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .end local v1           #result:[B
    :cond_3
    :goto_3
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 64
    .local v0, connection:Ljava/net/HttpURLConnection;
    :try_start_0
    invoke-static {v0, p3}, Lnet/flixster/android/util/HttpHelper;->fetchUrlBytes(Ljava/net/HttpURLConnection;Z)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 66
    .restart local v1       #result:[B
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 68
    if-eqz p2, :cond_4

    .line 69
    sget-object v3, Lnet/flixster/android/FlixsterApplication;->sFlixsterCacheManager:Lnet/flixster/android/FlixsterCacheManager;

    invoke-virtual {p0}, Ljava/net/URL;->hashCode()I

    move-result v4

    invoke-virtual {v3, v4, v1}, Lnet/flixster/android/FlixsterCacheManager;->put(I[B)V

    :cond_4
    move-object v2, v1

    .line 71
    .end local v1           #result:[B
    .restart local v2       #result:[B
    goto :goto_2

    .line 59
    .end local v0           #connection:Ljava/net/HttpURLConnection;
    .end local v2           #result:[B
    .restart local v1       #result:[B
    :cond_5
    const-string v3, "FlxApi"

    const-string v4, "HttpHelper.fetchUrlBytes cache miss"

    invoke-static {v3, v4}, Lcom/flixster/android/utils/Logger;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 65
    .end local v1           #result:[B
    .restart local v0       #connection:Ljava/net/HttpURLConnection;
    :catchall_0
    move-exception v3

    .line 66
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 67
    throw v3
.end method

.method public static fetchUrlStream(Ljava/net/URL;)Ljava/io/InputStream;
    .locals 3
    .parameter "url"

    .prologue
    .line 191
    :try_start_0
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 192
    .local v0, connection:Ljava/net/HttpURLConnection;
    const/16 v2, 0xfa0

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 193
    const v2, 0xea60

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 194
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    .line 195
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 198
    .end local v0           #connection:Ljava/net/HttpURLConnection;
    :goto_0
    return-object v2

    .line 196
    :catch_0
    move-exception v1

    .line 197
    .local v1, e:Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 198
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getRemoteFileSize(Ljava/net/URL;)I
    .locals 2
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 184
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    .line 185
    .local v0, urlConnection:Ljava/net/URLConnection;
    invoke-virtual {v0}, Ljava/net/URLConnection;->connect()V

    .line 186
    invoke-virtual {v0}, Ljava/net/URLConnection;->getContentLength()I

    move-result v1

    return v1
.end method

.method public static postToUrl(Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 13
    .parameter "url"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 111
    .local p1, parameters:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lorg/apache/http/NameValuePair;>;"
    invoke-static {p0}, Lnet/flixster/android/data/ApiBuilder;->isSecureFlxUrl(Ljava/lang/String;)Z

    move-result v4

    .line 112
    .local v4, isHttps:Z
    if-eqz v4, :cond_2

    .line 113
    const-string v10, "FlxApi"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "HttpHelper.postToUrl url:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " postValues:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->sd(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    :goto_0
    const/4 v1, 0x0

    .line 119
    .local v1, content:Ljava/io/InputStream;
    if-eqz v4, :cond_3

    :try_start_0
    new-instance v3, Lcom/flixster/android/net/ssl/SecureFlxHttpClient;

    invoke-direct {v3}, Lcom/flixster/android/net/ssl/SecureFlxHttpClient;-><init>()V

    .line 120
    .local v3, httpClient:Lorg/apache/http/client/HttpClient;
    :goto_1
    invoke-interface {v3}, Lorg/apache/http/client/HttpClient;->getParams()Lorg/apache/http/params/HttpParams;

    move-result-object v0

    .line 121
    .local v0, clientParameters:Lorg/apache/http/params/HttpParams;
    const v10, 0x1d4c0

    invoke-static {v0, v10}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 122
    const v10, 0x1d4c0

    invoke-static {v0, v10}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 123
    new-instance v6, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v6, p0}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 124
    .local v6, post:Lorg/apache/http/client/methods/HttpPost;
    new-instance v10, Lorg/apache/http/client/entity/UrlEncodedFormEntity;

    invoke-direct {v10, p1}, Lorg/apache/http/client/entity/UrlEncodedFormEntity;-><init>(Ljava/util/List;)V

    invoke-virtual {v6, v10}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 125
    invoke-interface {v3, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v8

    .line 126
    .local v8, response:Lorg/apache/http/HttpResponse;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 127
    .local v9, responseBuilder:Ljava/lang/StringBuilder;
    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 128
    .local v2, entity:Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_0

    .line 129
    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    .line 130
    new-instance v7, Ljava/io/BufferedReader;

    new-instance v10, Ljava/io/InputStreamReader;

    invoke-direct {v10, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v7, v10}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 132
    .local v7, reader:Ljava/io/BufferedReader;
    :goto_2
    invoke-virtual {v7}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .local v5, line:Ljava/lang/String;
    if-nez v5, :cond_5

    .line 136
    .end local v5           #line:Ljava/lang/String;
    .end local v7           #reader:Ljava/io/BufferedReader;
    :cond_0
    const-string v10, "FlxApi"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "HttpHelper.postToUrl response code:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v8}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v12

    invoke-interface {v12}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v10

    .line 139
    if-eqz v1, :cond_1

    .line 141
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 137
    :cond_1
    :goto_3
    return-object v10

    .line 115
    .end local v0           #clientParameters:Lorg/apache/http/params/HttpParams;
    .end local v1           #content:Ljava/io/InputStream;
    .end local v2           #entity:Lorg/apache/http/HttpEntity;
    .end local v3           #httpClient:Lorg/apache/http/client/HttpClient;
    .end local v6           #post:Lorg/apache/http/client/methods/HttpPost;
    .end local v8           #response:Lorg/apache/http/HttpResponse;
    .end local v9           #responseBuilder:Ljava/lang/StringBuilder;
    :cond_2
    const-string v10, "FlxApi"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "HttpHelper.postToUrl url:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " postValues:"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 119
    .restart local v1       #content:Ljava/io/InputStream;
    :cond_3
    :try_start_2
    new-instance v3, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v3}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 138
    :catchall_0
    move-exception v10

    .line 139
    if-eqz v1, :cond_4

    .line 141
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    .line 146
    :cond_4
    :goto_4
    throw v10

    .line 133
    .restart local v0       #clientParameters:Lorg/apache/http/params/HttpParams;
    .restart local v2       #entity:Lorg/apache/http/HttpEntity;
    .restart local v3       #httpClient:Lorg/apache/http/client/HttpClient;
    .restart local v5       #line:Ljava/lang/String;
    .restart local v6       #post:Lorg/apache/http/client/methods/HttpPost;
    .restart local v7       #reader:Ljava/io/BufferedReader;
    .restart local v8       #response:Lorg/apache/http/HttpResponse;
    .restart local v9       #responseBuilder:Ljava/lang/StringBuilder;
    :cond_5
    :try_start_4
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\n"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 142
    .end local v5           #line:Ljava/lang/String;
    .end local v7           #reader:Ljava/io/BufferedReader;
    :catch_0
    move-exception v11

    goto :goto_3

    .end local v0           #clientParameters:Lorg/apache/http/params/HttpParams;
    .end local v2           #entity:Lorg/apache/http/HttpEntity;
    .end local v3           #httpClient:Lorg/apache/http/client/HttpClient;
    .end local v6           #post:Lorg/apache/http/client/methods/HttpPost;
    .end local v8           #response:Lorg/apache/http/HttpResponse;
    .end local v9           #responseBuilder:Ljava/lang/StringBuilder;
    :catch_1
    move-exception v11

    goto :goto_4
.end method

.method public static streamToByteArray(Ljava/io/InputStream;)[B
    .locals 14
    .parameter "is"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/16 v13, 0x2000

    const/4 v12, 0x0

    .line 207
    const/4 v3, 0x0

    .line 208
    .local v3, bytesRead:I
    const/16 v10, 0x400

    new-array v2, v10, [B

    .line 209
    .local v2, buffer:[B
    new-array v5, v13, [B

    .line 210
    .local v5, fixedChunk:[B
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 211
    .local v0, BufferChunkList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<[B>;"
    const/16 v9, 0x2000

    .line 212
    .local v9, spaceLeft:I
    const/4 v4, 0x0

    .line 214
    .local v4, chunkIndex:I
    :goto_0
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v10, -0x1

    if-ne v3, v10, :cond_0

    .line 232
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 234
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v10

    mul-int/lit16 v10, v10, 0x2000

    add-int v8, v10, v4

    .line 235
    .local v8, responseSize:I
    new-array v7, v8, [B

    .line 236
    .local v7, responseBody:[B
    const/4 v6, 0x0

    .line 237
    .local v6, index:I
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_2

    .line 241
    invoke-static {v5, v12, v7, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 242
    return-object v7

    .line 216
    .end local v6           #index:I
    .end local v7           #responseBody:[B
    .end local v8           #responseSize:I
    :cond_0
    if-le v3, v9, :cond_1

    .line 218
    invoke-static {v2, v12, v5, v4, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 219
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    new-array v5, v13, [B

    .line 223
    sub-int v4, v3, v9

    .line 224
    invoke-static {v2, v9, v5, v12, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 230
    :goto_2
    rsub-int v9, v4, 0x2000

    goto :goto_0

    .line 227
    :cond_1
    invoke-static {v2, v12, v5, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 228
    add-int/2addr v4, v3

    goto :goto_2

    .line 237
    .restart local v6       #index:I
    .restart local v7       #responseBody:[B
    .restart local v8       #responseSize:I
    :cond_2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 238
    .local v1, b:[B
    invoke-static {v1, v12, v7, v6, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 239
    add-int/lit16 v6, v6, 0x2000

    goto :goto_1
.end method
