.class public Lnet/flixster/android/util/HttpImageHelper;
.super Ljava/lang/Object;
.source "HttpImageHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static fetchImage(Ljava/net/URL;)Landroid/graphics/Bitmap;
    .locals 5
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 14
    const/4 v0, 0x0

    .line 15
    .local v0, bitmap:Landroid/graphics/Bitmap;
    invoke-static {p0, v3, v3, v4}, Lnet/flixster/android/util/HttpHelper;->fetchUrlBytes(Ljava/net/URL;ZZZ)[B

    move-result-object v1

    .line 16
    .local v1, bitmapByteArray:[B
    array-length v3, v1

    invoke-static {v1, v4, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 17
    if-nez v0, :cond_0

    .line 19
    new-instance v2, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v2}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 20
    .local v2, options:Landroid/graphics/BitmapFactory$Options;
    const/4 v3, 0x4

    iput v3, v2, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 21
    array-length v3, v1

    invoke-static {v1, v4, v3, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 23
    .end local v2           #options:Landroid/graphics/BitmapFactory$Options;
    :cond_0
    return-object v0
.end method

.method public static fetchImageDoNotCache(Ljava/net/URL;)V
    .locals 3
    .parameter "url"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-virtual {p0}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lnet/flixster/android/data/ApiBuilder;->isAdTrack(Ljava/lang/String;)Z

    move-result v0

    .line 29
    .local v0, useFlixsterUserAgent:Z
    invoke-static {p0, v2, v2, v0}, Lnet/flixster/android/util/HttpHelper;->fetchUrlBytes(Ljava/net/URL;ZZZ)[B

    .line 30
    return-void
.end method
