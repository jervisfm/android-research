.class public Lnet/flixster/android/util/MediaHelper;
.super Ljava/lang/Object;
.source "MediaHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static savePhoto(Lnet/flixster/android/model/Photo;Landroid/content/Context;)Z
    .locals 10
    .parameter "photo"
    .parameter "context"

    .prologue
    const/4 v7, 0x1

    .line 13
    const/4 v5, 0x0

    .line 14
    .local v5, wasSuccessfull:Z
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "photo"

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v8, p0, Lnet/flixster/android/model/Photo;->id:J

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, ".png"

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 15
    .local v4, title:Ljava/lang/String;
    const-string v0, "Flixster Movies Photo"

    .line 16
    .local v0, description:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 18
    .local v1, descriptionBuilder:Ljava/lang/StringBuilder;
    iget-object v6, p0, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v7, :cond_0

    .line 19
    iget-object v4, p0, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    .line 20
    const-string v6, " "

    const-string v8, "_"

    invoke-virtual {v4, v6, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    .line 21
    iget-object v6, p0, Lnet/flixster/android/model/Photo;->caption:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 23
    :cond_0
    iget-object v6, p0, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, p0, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-le v6, v7, :cond_2

    .line 24
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_1

    .line 25
    const-string v6, " - "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    :cond_1
    iget-object v6, p0, Lnet/flixster/android/model/Photo;->description:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-lez v6, :cond_3

    .line 30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 33
    :cond_3
    :try_start_0
    const-string v6, "FlxMain"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "FlixsterActivity savePhoto title:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " desc:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/flixster/android/utils/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 35
    iget-object v6, p0, Lnet/flixster/android/model/Photo;->galleryBitmap:Ljava/lang/ref/SoftReference;

    invoke-virtual {v6}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    .line 34
    invoke-static {v8, v6, v4, v0}, Landroid/provider/MediaStore$Images$Media;->insertImage(Landroid/content/ContentResolver;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 36
    .local v3, status:Ljava/lang/String;
    if-eqz v3, :cond_4

    move v5, v7

    .line 40
    .end local v3           #status:Ljava/lang/String;
    :goto_0
    return v5

    .line 36
    .restart local v3       #status:Ljava/lang/String;
    :cond_4
    const/4 v5, 0x0

    goto :goto_0

    .line 37
    .end local v3           #status:Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 38
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "FlxMain"

    const-string v7, "Problem saving image"

    invoke-static {v6, v7, v2}, Lcom/flixster/android/utils/Logger;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
