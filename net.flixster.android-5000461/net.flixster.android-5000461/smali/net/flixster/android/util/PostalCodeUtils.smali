.class public Lnet/flixster/android/util/PostalCodeUtils;
.super Ljava/lang/Object;
.source "PostalCodeUtils.java"


# static fields
.field protected static ALL_NUMERIC:Ljava/util/regex/Pattern; = null

.field public static AU_POSTAL_CODE_PATTERN:Ljava/util/regex/Pattern; = null

.field public static CANADIAN_POSTAL_CODE_PATTERN:Ljava/util/regex/Pattern; = null

.field protected static CONTAINS_DIGIT:Ljava/util/regex/Pattern; = null

.field public static final COUNTRY_AU:I = 0x4

.field public static final COUNTRY_CA:I = 0x3

.field public static final COUNTRY_UK:I = 0x2

.field public static final COUNTRY_US:I = 0x1

.field public static UK_POSTCODE_PATTERN:Ljava/util/regex/Pattern;

.field public static US_ZIP_CODE_PATTERN:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-string v0, "[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]\\d[ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz][\\s-]?\\d[ABCEGHJKLMNPRSTVWXYZabceghjklmnprstvwxyz]\\d"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 16
    sput-object v0, Lnet/flixster/android/util/PostalCodeUtils;->CANADIAN_POSTAL_CODE_PATTERN:Ljava/util/regex/Pattern;

    .line 19
    const-string v0, "\\d{5}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/util/PostalCodeUtils;->US_ZIP_CODE_PATTERN:Ljava/util/regex/Pattern;

    .line 21
    const-string v0, "\\d{4}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/util/PostalCodeUtils;->AU_POSTAL_CODE_PATTERN:Ljava/util/regex/Pattern;

    .line 22
    const-string v0, "[A-Z]{1,2}\\d[A-Z\\d]?( )?(\\d[ABD-HJLNP-UW-Z]{2})?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/util/PostalCodeUtils;->UK_POSTCODE_PATTERN:Ljava/util/regex/Pattern;

    .line 29
    const-string v0, ".*\\d.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/util/PostalCodeUtils;->CONTAINS_DIGIT:Ljava/util/regex/Pattern;

    .line 30
    const-string v0, "\\d+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lnet/flixster/android/util/PostalCodeUtils;->ALL_NUMERIC:Ljava/util/regex/Pattern;

    .line 9
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCanonicalCanadianPostalCode(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "postalCode"

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 69
    if-nez p0, :cond_0

    .line 70
    const/4 p0, 0x0

    .line 85
    :goto_0
    return-object p0

    .line 73
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 83
    :cond_1
    :goto_1
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    .line 85
    goto :goto_0

    .line 77
    :cond_2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public static isAllNumeric(Ljava/lang/String;)Z
    .locals 1
    .parameter "value"

    .prologue
    .line 58
    sget-object v0, Lnet/flixster/android/util/PostalCodeUtils;->ALL_NUMERIC:Ljava/util/regex/Pattern;

    invoke-static {v0, p0}, Lnet/flixster/android/util/PostalCodeUtils;->isValueMatchesPattern(Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isProbablePostalCode(Ljava/lang/String;)Z
    .locals 1
    .parameter "value"

    .prologue
    .line 54
    sget-object v0, Lnet/flixster/android/util/PostalCodeUtils;->CONTAINS_DIGIT:Ljava/util/regex/Pattern;

    invoke-static {v0, p0}, Lnet/flixster/android/util/PostalCodeUtils;->isValueMatchesPattern(Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isValidAustraliaPostcode(Ljava/lang/String;)Z
    .locals 1
    .parameter "value"

    .prologue
    .line 50
    sget-object v0, Lnet/flixster/android/util/PostalCodeUtils;->AU_POSTAL_CODE_PATTERN:Ljava/util/regex/Pattern;

    invoke-static {v0, p0}, Lnet/flixster/android/util/PostalCodeUtils;->isValueMatchesPattern(Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isValidCanadianPostalCode(Ljava/lang/String;)Z
    .locals 1
    .parameter "value"

    .prologue
    .line 42
    sget-object v0, Lnet/flixster/android/util/PostalCodeUtils;->CANADIAN_POSTAL_CODE_PATTERN:Ljava/util/regex/Pattern;

    invoke-static {v0, p0}, Lnet/flixster/android/util/PostalCodeUtils;->isValueMatchesPattern(Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isValidUnitedKingdomPostcode(Ljava/lang/String;)Z
    .locals 1
    .parameter "value"

    .prologue
    .line 46
    sget-object v0, Lnet/flixster/android/util/PostalCodeUtils;->UK_POSTCODE_PATTERN:Ljava/util/regex/Pattern;

    invoke-static {v0, p0}, Lnet/flixster/android/util/PostalCodeUtils;->isValueMatchesPattern(Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isValidUnitedStatesZipCode(Ljava/lang/String;)Z
    .locals 1
    .parameter "value"

    .prologue
    .line 38
    sget-object v0, Lnet/flixster/android/util/PostalCodeUtils;->US_ZIP_CODE_PATTERN:Ljava/util/regex/Pattern;

    invoke-static {v0, p0}, Lnet/flixster/android/util/PostalCodeUtils;->isValueMatchesPattern(Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static isValueMatchesPattern(Ljava/util/regex/Pattern;Ljava/lang/String;)Z
    .locals 2
    .parameter "pattern"
    .parameter "value"

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 34
    .local v0, matcher:Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    return v1
.end method

.method public static parseZipcodeForShowtimes(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .parameter "value"

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 133
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-object v0

    .line 135
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 139
    .local v1, postal:Ljava/lang/String;
    invoke-static {v1}, Lnet/flixster/android/util/PostalCodeUtils;->isValidUnitedStatesZipCode(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 140
    const/4 v2, 0x1

    invoke-static {v2, v1}, Lnet/flixster/android/util/PostalCodeUtils;->splitPostalCode(ILjava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v3

    .local v0, code:Ljava/lang/String;
    goto :goto_0

    .line 141
    .end local v0           #code:Ljava/lang/String;
    :cond_2
    invoke-static {v1}, Lnet/flixster/android/util/PostalCodeUtils;->isValidAustraliaPostcode(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 142
    const/4 v2, 0x4

    invoke-static {v2, v1}, Lnet/flixster/android/util/PostalCodeUtils;->splitPostalCode(ILjava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v3

    .restart local v0       #code:Ljava/lang/String;
    goto :goto_0

    .line 143
    .end local v0           #code:Ljava/lang/String;
    :cond_3
    invoke-static {v1}, Lnet/flixster/android/util/PostalCodeUtils;->isValidCanadianPostalCode(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 144
    const/4 v2, 0x3

    invoke-static {v2, v1}, Lnet/flixster/android/util/PostalCodeUtils;->splitPostalCode(ILjava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v3

    .restart local v0       #code:Ljava/lang/String;
    goto :goto_0

    .line 145
    .end local v0           #code:Ljava/lang/String;
    :cond_4
    invoke-static {v1}, Lnet/flixster/android/util/PostalCodeUtils;->isValidUnitedKingdomPostcode(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    const/4 v2, 0x2

    invoke-static {v2, v1}, Lnet/flixster/android/util/PostalCodeUtils;->splitPostalCode(ILjava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v3

    .restart local v0       #code:Ljava/lang/String;
    goto :goto_0
.end method

.method public static splitPostalCode(ILjava/lang/String;)[Ljava/lang/String;
    .locals 10
    .parameter "country"
    .parameter "code"

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x1

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 89
    if-eqz p0, :cond_0

    if-le p0, v9, :cond_1

    .line 90
    :cond_0
    new-array v3, v6, [Ljava/lang/String;

    aput-object p1, v3, v5

    .line 124
    :goto_0
    return-object v3

    .line 92
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    .line 93
    move-object v0, p1

    .line 94
    .local v0, first:Ljava/lang/String;
    const/4 v1, 0x0

    .line 95
    .local v1, last:Ljava/lang/String;
    if-ne p0, v8, :cond_4

    .line 96
    const-string v3, "-"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_3

    .line 97
    const-string v3, "-"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 98
    const-string v3, "-"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 124
    :cond_2
    :goto_1
    new-array v3, v6, [Ljava/lang/String;

    aput-object v0, v3, v5

    aput-object v1, v3, v8

    goto :goto_0

    .line 99
    :cond_3
    sget-object v3, Lnet/flixster/android/util/PostalCodeUtils;->ALL_NUMERIC:Ljava/util/regex/Pattern;

    invoke-static {v3, p1}, Lnet/flixster/android/util/PostalCodeUtils;->isValueMatchesPattern(Ljava/util/regex/Pattern;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x5

    if-le v3, v4, :cond_2

    .line 100
    const/4 v3, 0x5

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 101
    const/4 v3, 0x6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 104
    :cond_4
    if-ne p0, v6, :cond_5

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    if-gt v3, v9, :cond_5

    .line 105
    move-object v0, p1

    .line 106
    const/4 v1, 0x0

    goto :goto_1

    .line 107
    :cond_5
    if-ne p0, v6, :cond_6

    const-string v3, " "

    invoke-virtual {p1, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 108
    const-string v3, " "

    invoke-virtual {p1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 109
    .local v2, pair:[Ljava/lang/String;
    aget-object v0, v2, v5

    .line 110
    aget-object v1, v2, v8

    goto :goto_1

    .line 111
    .end local v2           #pair:[Ljava/lang/String;
    :cond_6
    if-ne p0, v6, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x5

    if-lt v3, v4, :cond_7

    .line 115
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    invoke-virtual {p1, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 116
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x3

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 117
    :cond_7
    if-ne p0, v7, :cond_2

    .line 118
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_8

    .line 119
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x6

    invoke-virtual {p1, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 120
    :cond_8
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x7

    if-ne v3, v4, :cond_2

    const-string v3, "-"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_2

    .line 121
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {p1, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x7

    invoke-virtual {p1, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method
