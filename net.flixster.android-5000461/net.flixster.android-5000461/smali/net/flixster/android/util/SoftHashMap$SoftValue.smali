.class Lnet/flixster/android/util/SoftHashMap$SoftValue;
.super Ljava/lang/ref/SoftReference;
.source "SoftHashMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lnet/flixster/android/util/SoftHashMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SoftValue"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/ref/SoftReference",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final key:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TK;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TT;",
            "Ljava/lang/ref/ReferenceQueue",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p0, this:Lnet/flixster/android/util/SoftHashMap$SoftValue;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>.SoftValue<TK;TT;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    .local p2, value:Ljava/lang/Object;,"TT;"
    .local p3, queue:Ljava/lang/ref/ReferenceQueue;,"Ljava/lang/ref/ReferenceQueue<TT;>;"
    invoke-direct {p0, p2, p3}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    .line 75
    iput-object p1, p0, Lnet/flixster/android/util/SoftHashMap$SoftValue;->key:Ljava/lang/Object;

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;Lnet/flixster/android/util/SoftHashMap$SoftValue;)V
    .locals 0
    .parameter
    .parameter
    .parameter
    .parameter

    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lnet/flixster/android/util/SoftHashMap$SoftValue;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;)V

    return-void
.end method

.method static synthetic access$1(Lnet/flixster/android/util/SoftHashMap$SoftValue;)Ljava/lang/Object;
    .locals 1
    .parameter

    .prologue
    .line 71
    iget-object v0, p0, Lnet/flixster/android/util/SoftHashMap$SoftValue;->key:Ljava/lang/Object;

    return-object v0
.end method
