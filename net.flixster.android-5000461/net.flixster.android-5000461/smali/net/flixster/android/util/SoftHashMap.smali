.class public final Lnet/flixster/android/util/SoftHashMap;
.super Ljava/util/AbstractMap;
.source "SoftHashMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnet/flixster/android/util/SoftHashMap$SoftValue;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/AbstractMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private final hashMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<TK;",
            "Lnet/flixster/android/util/SoftHashMap$SoftValue",
            "<TK;TV;>;>;"
        }
    .end annotation
.end field

.field private final refQueue:Ljava/lang/ref/ReferenceQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/ReferenceQueue",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    .local p0, this:Lnet/flixster/android/util/SoftHashMap;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>;"
    invoke-direct {p0}, Ljava/util/AbstractMap;-><init>()V

    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/util/SoftHashMap;->hashMap:Ljava/util/Map;

    .line 15
    new-instance v0, Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v0}, Ljava/lang/ref/ReferenceQueue;-><init>()V

    iput-object v0, p0, Lnet/flixster/android/util/SoftHashMap;->refQueue:Ljava/lang/ref/ReferenceQueue;

    .line 18
    return-void
.end method

.method private processQueue()V
    .locals 3

    .prologue
    .line 61
    .local p0, this:Lnet/flixster/android/util/SoftHashMap;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>;"
    :goto_0
    iget-object v1, p0, Lnet/flixster/android/util/SoftHashMap;->refQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v1}, Ljava/lang/ref/ReferenceQueue;->poll()Ljava/lang/ref/Reference;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/util/SoftHashMap$SoftValue;

    .line 62
    .local v0, softValue:Lnet/flixster/android/util/SoftHashMap$SoftValue;,"Lnet/flixster/android/util/SoftHashMap$SoftValue<TK;TV;>;"
    if-eqz v0, :cond_0

    .line 63
    iget-object v1, p0, Lnet/flixster/android/util/SoftHashMap;->hashMap:Ljava/util/Map;

    #getter for: Lnet/flixster/android/util/SoftHashMap$SoftValue;->key:Ljava/lang/Object;
    invoke-static {v0}, Lnet/flixster/android/util/SoftHashMap$SoftValue;->access$1(Lnet/flixster/android/util/SoftHashMap$SoftValue;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 65
    :cond_0
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 45
    .local p0, this:Lnet/flixster/android/util/SoftHashMap;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>;"
    invoke-direct {p0}, Lnet/flixster/android/util/SoftHashMap;->processQueue()V

    .line 46
    iget-object v0, p0, Lnet/flixster/android/util/SoftHashMap;->hashMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 47
    return-void
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;>;"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, this:Lnet/flixster/android/util/SoftHashMap;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, this:Lnet/flixster/android/util/SoftHashMap;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>;"
    const/4 v1, 0x0

    .line 22
    .local v1, value:Ljava/lang/Object;,"TV;"
    iget-object v2, p0, Lnet/flixster/android/util/SoftHashMap;->hashMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/util/SoftHashMap$SoftValue;

    .line 23
    .local v0, sr:Lnet/flixster/android/util/SoftHashMap$SoftValue;,"Lnet/flixster/android/util/SoftHashMap$SoftValue<TK;TV;>;"
    if-eqz v0, :cond_0

    .line 24
    invoke-virtual {v0}, Lnet/flixster/android/util/SoftHashMap$SoftValue;->get()Ljava/lang/Object;

    move-result-object v1

    .line 25
    if-nez v1, :cond_0

    .line 26
    iget-object v2, p0, Lnet/flixster/android/util/SoftHashMap;->hashMap:Ljava/util/Map;

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    .end local v1           #value:Ljava/lang/Object;,"TV;"
    :cond_0
    return-object v1
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    .prologue
    .local p0, this:Lnet/flixster/android/util/SoftHashMap;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>;"
    .local p1, key:Ljava/lang/Object;,"TK;"
    .local p2, value:Ljava/lang/Object;,"TV;"
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lnet/flixster/android/util/SoftHashMap;->processQueue()V

    .line 34
    iget-object v2, p0, Lnet/flixster/android/util/SoftHashMap;->hashMap:Ljava/util/Map;

    new-instance v3, Lnet/flixster/android/util/SoftHashMap$SoftValue;

    iget-object v4, p0, Lnet/flixster/android/util/SoftHashMap;->refQueue:Ljava/lang/ref/ReferenceQueue;

    invoke-direct {v3, p1, p2, v4, v1}, Lnet/flixster/android/util/SoftHashMap$SoftValue;-><init>(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/ref/ReferenceQueue;Lnet/flixster/android/util/SoftHashMap$SoftValue;)V

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/util/SoftHashMap$SoftValue;

    .line 35
    .local v0, oldValue:Lnet/flixster/android/util/SoftHashMap$SoftValue;,"Lnet/flixster/android/util/SoftHashMap$SoftValue<TK;TV;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lnet/flixster/android/util/SoftHashMap$SoftValue;->get()Ljava/lang/Object;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .parameter "key"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    .prologue
    .line 39
    .local p0, this:Lnet/flixster/android/util/SoftHashMap;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>;"
    invoke-direct {p0}, Lnet/flixster/android/util/SoftHashMap;->processQueue()V

    .line 40
    iget-object v1, p0, Lnet/flixster/android/util/SoftHashMap;->hashMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnet/flixster/android/util/SoftHashMap$SoftValue;

    .line 41
    .local v0, oldValue:Lnet/flixster/android/util/SoftHashMap$SoftValue;,"Lnet/flixster/android/util/SoftHashMap$SoftValue<TK;TV;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lnet/flixster/android/util/SoftHashMap$SoftValue;->get()Ljava/lang/Object;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 50
    .local p0, this:Lnet/flixster/android/util/SoftHashMap;,"Lnet/flixster/android/util/SoftHashMap<TK;TV;>;"
    invoke-direct {p0}, Lnet/flixster/android/util/SoftHashMap;->processQueue()V

    .line 51
    iget-object v0, p0, Lnet/flixster/android/util/SoftHashMap;->hashMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
